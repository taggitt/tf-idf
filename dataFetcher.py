import string
import unicodedata
import wikipedia
import sys
import requests
import os
import errno

listsDone = ["Wikipedia:WikiProject_Lists_of_topics"]
skipped = []
#initialList = "Wikipedia:WikiProject_Lists_of_topics"
#initialList = "Lists of mathematics topics"
initialList = "Outline of machines"

def createDir(path): #Makes sure the directory can be created
	try:
		os.makedirs(path)
	except OSError as exception:
		if exception.errno != errno.EEXIST:
			raise

def linkFormat(link): #Removes extra characters
	links = link
	links = string.replace(links, ' ', '_')
	links = string.replace(links, '/', '__')
	links = string.replace(links, "'", '__')
	return links

def listExplorer(currentList, path): #Creates all directories that will be used in the crawl
	os.chdir(path)
	visited = []
	pathQueue = []
	linkQueue = []
	wikiPage = wikipedia.page(currentList)
	links = wikiPage.links
	if('List of Albania-related articles' in links):
		links.remove('List of Albania-related articles')
	if('Lists of topics' in links):
		links.remove('Lists of topics')
	for link in links:
		linkQueue.append(link)
		pathQueue.append(os.path.abspath("."))
	while pathQueue:
		linkElement = linkQueue[0]
		pathElement = pathQueue[0]
		linkQueue.pop(0)
		pathQueue.pop(0)
		print(pathElement)
		if((linkElement not in visited) and ('List of' in linkElement or 'Glossary of' in linkElement or 'Catalog of' in linkElement or 'Outline of' in linkElement)):
			visited.append(linkElement)
			if(not linkFormat(linkElement) == pathElement):
				pathToAdd = pathElement + '/' + linkFormat(linkElement)
			else:
				pathToAdd = pathElement
			try:
				wikiPage = wikipedia.page(linkElement)
				for link in wikiPage.links:
					if link not in visited:
						if(('List of' in link or 'Glossary of' in link or 'Catalog of' in link or 'Outline of' in link) and ('List of' in linkElement or 'Glossary of' in linkElement or 'Catalog of' in linkElement or 'Outline of' in linkElement)):
							if(pathToAdd.count('/') <= 2):
								try:
									createDir(pathToAdd)
								except:
									skipped.append(linkElement)
								linkQueue.append(link)
								pathQueue.append(pathToAdd + '/' + link)
			except (wikipedia.exceptions.PageError, requests.exceptions.ConnectionError, wikipedia.exceptions.DisambiguationError, wikipedia.exceptions.WikipediaException):
				skipped.append(linkedElement)
				print(linkedElement + " was skipped")
				continue
		else:
			print("download branch")
			print(linkElement)
			download(linkElement, pathElement)

def download(line, path):
	temp = line
	temp = temp.rstrip()
	try:
		text = wikipedia.page(temp).content
		text = text.encode('ascii','ignore')
		text = text.lower()
		text = text.translate(None, string.punctuation)
		text = text.translate(None, string.digits)
		text = string.replace(text, '\n', ' ')
		temp = linkFormat(temp)
		file = open(path + '/' + temp + '.txt', 'w')
		file.write(text)
		file.close()
	except (wikipedia.exceptions.PageError, requests.exceptions.ConnectionError, wikipedia.exceptions.DisambiguationError, wikipedia.exceptions.WikipediaException):
		print temp + ' not found, added to skipped array, will be logged into the file log'
		skipped.append(temp)

pathToOutput = os.path.abspath('.')
pathToOutput = string.rstrip(pathToOutput, '/')
pathToOutput += '/database'

listExplorer(initialList, pathToOutput)


