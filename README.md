# README #

Taggitt is a website that tags your text. By Dany and Karely

These are instruction for runnning on Windows.

Copy all the files inside the folder "COPY...." and paste inside htdocs in XAMPP.

Install python version 3.6. 

Install nltk. This can be done by opening the terminal. Write "set path=%PATH%;C:\...\Python\Python36-32\Scripts" where "..." is the path to where you put python. Then write: "pip install nltk".

Create 'nltk_data' folder in root (ie: C:\nltk_data)
Download with nltk.download(): Open terminal in the folder (open cmd, cd to C:\nltk_data). Set path to python.exe with "set path=%PATH%; C:\...\Python\Python36-32" where "..." is the path. Run python by typing "python" in the terminal. Write "import nltk" then "nltk.download()". After a few seconds/minutes a window will come out. Select download "all corpora" and inside packages download "punkt".

Inside the python files, change the first line #! "..." replacing the "..." to the path it can take to access python.exe (usually in ...\Python\Python36-32).

Copy the folder "tfidf files" inside the folder "data" and paste it in the htdocs folder.

