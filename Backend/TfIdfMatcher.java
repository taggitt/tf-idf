package tfidfmatcher;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.TreeMap;

/**
 *
 * @author Dany Lyth
 */
public class TfIdfMatcher {

    /**
     * @param args the command line arguments
     */
    static final String BASE_LEVEL = "test"; //path to top level (where summary files are "data/summary tfidf")
    static ArrayList<TreeMap<String, Double>> basetfidfScores = new ArrayList<TreeMap<String, Double>>();
    static ArrayList<String> basenames;
    static ArrayList<TreeMap<String, Double>> tfidfScores = new ArrayList<TreeMap<String, Double>>();
    static ArrayList<String> names;
    static Scanner inputScanner = new Scanner(System.in);

    public static void createScoresForDirectory(String path) {
        try {
            tfidfScores = new ArrayList<TreeMap<String, Double>>();
            names = new ArrayList<String>();
            File folder = new File(path);
            File[] listOfFiles = folder.listFiles();
            for (int i = 0; i < listOfFiles.length; i++) {
                if (listOfFiles[i].isFile()) {
                    names.add(listOfFiles[i].getName());
                    //System.out.println(names.get(i));
                    TreeMap<String, Double> scores = new TreeMap<>();
                    Scanner fileInput = new Scanner(listOfFiles[i]);
                    while (fileInput.hasNextLine()) {
                        String line = fileInput.nextLine();
                        //System.out.println(line);
                        Scanner lineReader = new Scanner(line);
                        lineReader.useDelimiter(",");
                        String wordValue = lineReader.next();
                        double score = lineReader.nextDouble();
                        scores.put(wordValue, score);
                    }
                    tfidfScores.add(scores);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        // TODO code application logic here

        //Open folder in which tf-idfDatabase is and create metaFiles as arrayList of treemap
        try {
            basetfidfScores = new ArrayList<TreeMap<String, Double>>();
            basenames = new ArrayList<String>();
            File folder = new File(BASE_LEVEL);
            File[] listOfFiles = folder.listFiles();
            //for (File f : listOfFiles) {
            //    System.out.println(f.getName());
            //}
            for (int i = 0; i < listOfFiles.length; i++) {
                if (listOfFiles[i].isFile()) {
                    basenames.add(listOfFiles[i].getName());
                    TreeMap<String, Double> scores = new TreeMap<>();
                    Scanner fileInput = new Scanner(listOfFiles[i]);
                    while (fileInput.hasNextLine()) {
                        String line = fileInput.nextLine();
                        //System.out.println(line);
                        Scanner lineReader = new Scanner(line);
                        lineReader.useDelimiter(",");
                        String wordValue = lineReader.next();
                        double score = lineReader.nextDouble();
                        scores.put(wordValue, score);
                    }
                    basetfidfScores.add(scores);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

            //Get best match from metaFiles
            //System.out.println("Input please.");
            ArrayList<String> toReturn = new ArrayList<>(); //Contains the topics, first is the metaTopic (first level), second is a level under, etc
            TreeMap<String, Double> score = new TreeMap<>(); //Contains the tfidf scores for the input
            ArrayList<Double> metaScores = new ArrayList<>(); //Contains the comparison score
            while (inputScanner.hasNextLine()) {
                String line = inputScanner.nextLine();
                if (line.equals("exit")) {
                    break;
                }
                Scanner inputLineReader = new Scanner(line);
                inputLineReader.useDelimiter(",");
                String word = inputLineReader.next();
                double value = inputLineReader.nextDouble();
                score.put(word, value);
            }
            //cosine similarity
            for (TreeMap<String, Double> scores : basetfidfScores) {
                double totalScore = 0;
                double toDivideBy1 = 0;
                double toDivideBy2 = 0;
                for(String i : scores.keySet()) {
                    toDivideBy1 += scores.get(i)*scores.get(i);
                }
                for (String i : score.keySet()) {
                    toDivideBy2 += score.get(i)*score.get(i);
                    if (scores.containsKey(i)) {
                        totalScore += scores.get(i) * score.get(i);
                    }
                }
                totalScore /= Math.sqrt(toDivideBy1*toDivideBy2);
                metaScores.add(totalScore);
            }
            //Jaccard index attempts were less than satisfactory
//            for (TreeMap<String, Double> scores : basetfidfScores) {
//                double totalScore;
//                double top = 0;
//                double bottom = 0;
//                for (String i : score.keySet()) {
//                    if (scores.containsKey(i)) {
//                        top += Math.min(scores.get(i), score.get(i));
//                        bottom += Math.max(scores.get(i), score.get(i));
//                    }
//                }
//                totalScore = top / bottom;
//                metaScores.add(totalScore);
//            }
            double temp = 0;
            String tempBestMatch = "";
            for (int i = 0; i < metaScores.size(); i++) {
                if (metaScores.get(i) > temp) {
                    temp = metaScores.get(i);
                    tempBestMatch = basenames.get(i).split("_tfidf")[0];
                }
            }
            toReturn.add(tempBestMatch); //Add best matching metaFile
            //System.out.println(tempBestMatch);

            //Go to the best match folder
            //toReturn = new ArrayList<>(); //Contains the topics, first is the metaTopic (first level), second is a level under, etc
            metaScores = new ArrayList<Double>(); //Contains the comparison score
            createScoresForDirectory(BASE_LEVEL + "/" + tempBestMatch);
            //Cosine similarity
            for (TreeMap<String, Double> scores : tfidfScores) {
                double totalScore = 0;
                double toDivideBy1 = 0;
                double toDivideBy2 = 0;
                for(String i : scores.keySet()) {
                    toDivideBy1 += scores.get(i)*scores.get(i);
                }
                for (String i : score.keySet()) {
                    toDivideBy2 += score.get(i)*score.get(i);
                    if (scores.containsKey(i)) {
                        totalScore += scores.get(i) * score.get(i);
                    }
                }
                totalScore /= Math.sqrt(toDivideBy1*toDivideBy2);
                metaScores.add(totalScore);
                
            }
//            //Jaccard index similarity
//            for (TreeMap<String, Double> scores : tfidfScores) {
//                double totalScore;
//                double top = 0;
//                double bottom = 0;
//                for (String i : score.keySet()) {
//                    if (scores.containsKey(i)) {
//                        top += Math.min(scores.get(i), score.get(i));
//                        bottom += Math.max(scores.get(i), score.get(i));
//                    }
//                }
//                totalScore = top / bottom;
//                metaScores.add(totalScore);
//            }
            temp = 0;
            tempBestMatch = "";
            for (int i = 0; i < metaScores.size(); i++) {
                if (metaScores.get(i) > temp) {
                    temp = metaScores.get(i);
                    tempBestMatch = names.get(i).split("_tfidf")[0];
                }
            }
            toReturn.add(tempBestMatch);
            System.out.println(toReturn.get(0).substring(9).split("_tfidf")[0] + "\n" + toReturn.get(1).split("_tfidf")[0]);

    }
}
