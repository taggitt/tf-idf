import string
import unicodedata
import wikipedia
import sys
import requests
import os
import errno

skipped = []
initialList = "Wikipedia:WikiProject_Lists_of_topics"
#initialList = "Lists of mathematics topics"
#initialList = "Outline of machines"

def createDir(path): #Makes sure the directory can be created
	try:
		os.makedirs(path)
	except OSError as exception:
		if exception.errno != errno.EEXIST:
			raise

def linkFormat(link): #Removes extra characters
	links = link
	links = string.replace(links, ' ', '_')
	links = string.replace(links, '/', '__')
	links = string.replace(links, "'", '__')
	links = links.encode('ascii', 'ignore')
	links = links.rstrip()
	return links

def listExplorer(currentList, path): #Creates all directories that will be used in the crawl
	count = 0
	os.chdir(path + "/database")
	pathQueue = []
	linkQueue = []
	visited = []
	try:
		wikiPage = wikipedia.page(currentList)
		links = wikiPage.links
		#print(links)
		if('List of Albania-related articles' in links):
			links.remove('List of Albania-related articles')
		if('Lists of topics' in links):
			links.remove('Lists of topics')
		for link in links:
			if('List of' in link or 'Glossary of' in link or 'Catalog of' in link or 'Outline of' in link):
				linkQueue.append(link)
				pathToAddToQueue = os.path.join(os.path.abspath("."), linkFormat(link))
				pathQueue.append(pathToAddToQueue)
				createDir(pathToAddToQueue)
		while pathQueue:
			try:
				linkElement = linkQueue[0]
				pathElement = pathQueue[0]
				linkQueue.pop(0)
				pathQueue.pop(0)
				wikiPage = wikipedia.page(linkElement)
				for link in wikiPage.links:
					download(link, pathElement)
					print("Downloading" + linkElement)
			except (wikipedia.exceptions.PageError, requests.exceptions.ConnectionError, wikipedia.exceptions.DisambiguationError, wikipedia.exceptions.WikipediaException):
				skipped.append(linkElement)
				print(linkElement + " was skipped")
				continue
	except:
		print("nothing")
def download(line, path):
	temp = line
	temp = temp.rstrip()
	try:
		text = wikipedia.page(temp).content
		text = text.encode('ascii','ignore')
		text = text.lower()
		text = text.translate(None, string.punctuation)
		text = text.translate(None, string.digits)
		text = string.replace(text, '\n', ' ')
		temp = linkFormat(temp)
		file = open(path + '/' + temp + '.txt', 'w')
		file.write(text)
		file.close()
	except (wikipedia.exceptions.PageError, requests.exceptions.ConnectionError, wikipedia.exceptions.DisambiguationError, wikipedia.exceptions.WikipediaException):
		print(temp + ' not found, added to skipped array, will be logged into the file log')
		skipped.append(temp)

pathToOutput = os.path.abspath('.')
pathToOutput = string.rstrip(pathToOutput, '/')

listExplorer(initialList, pathToOutput)
