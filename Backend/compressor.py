#Takes 2 arguments (1 is optional):
#	The first is the file containing the corpus,
#	The second can take 2 forms:
#	-f, an optional arg that stops the recreation of the tf_idf files from the corpus, saving hours when it is already constructed
#	Or an integer representing the number of digits you want to save of your tf_idf value (after the decimal) (default is 6)
#	The outputs are:
#	A file for each file in the corpus, in the format word_int_value,tf_idf_score\n
#	Four dictionaries containing:
#		String_to_int, a python dictionary of all the numerical values associated to each word
#		Int_to_string, the reverse dictionary mentioned above
#		Int_to_idf, a dictionary of all the numerical values associated with each words and their corresponding idf scores
#		word_base, a simple list of all the words in the cleaned corpus, useful for other parts of the sequence

import nltk
import string
import os
import sys
import numpy

from sklearn.feature_extraction.text import TfidfVectorizer
from nltk.stem.porter import PorterStemmer

stemmer = PorterStemmer()

option = 'xx'
optionPrecision = 8
if len(sys.argv) == 2:
	path = sys.argv[1]
if len(sys.argv) == 3:
	path = sys.argv[1]
	if sys.argv[2] == '-f':
		option = sys.argv[2]
	elif (int(sys.argv[2]) in range(20)):
		optionPrecision = 2 + int(sys.argv[2])


#From a word base, create two files: from word -> integer and int -> word (in alphabetical order)
def dictionaryBuilder(path):
	counter = 0
	wordFile = open(path + '/dictionaries/word_base', 'r')
	inverseMapFile = open(path + '/dictionaries/string_to_int', 'w')
	mapFile = open(path + '/dictionaries/string_to_int', 'w')
	dictStringToInt = {}
	dictIntToString = {}
	for x in wordFile:
		line = string.strip(x, '\n')
		dictStringToInt[line] = counter
		dictIntToString[counter] = line
		counter += 1
	mapFile.write(str(dictStringToInt))
	inverseMapFile.write(str(dictIntToString))
	wordFile.close()
	mapFile.close()


def stemTokens(tokens, stemmer):
	stemmed = []
	for items in tokens:
		stemmed.append(stemmer.stem(items))
	return stemmed


def tokenizeAndStem(text):
	tokens = nltk.word_tokenize(text)
	stems = stemTokens(tokens, stemmer)
	return stems

def wordBaseBuilder(listOfFeatures, path):
	if not os.path.exists(path + '/dictionaries'):
		os.makedirs(path + '/dictionaries')
	wordBase = open(path + '/dictionaries/word_base', 'w')
	for features in (listOfFeatures):
		wordBase.write(str(features) + '\n')
	wordBase.close()

def tfidfMaker():
	tokenDictionary = {}
	numberOfFiles = 0
	for subdir, dirs, files in os.walk(path):
		for file in files:
			numberOfFiles += 1#To show progress
			filePath = subdir + os.path.sep + file
			subjectFile = open(filePath, 'r')
			text = subjectFile.read()
			text = text.lower()
			text = text.translate(None, string.punctuation)
			tokenDictionary[filePath] = text
			if numberOfFiles%10 == 0:
				print str(file)
	pageNames = tokenDictionary.keys()
	print('Creating tfidf database, this may take some time')
	tfidf = TfidfVectorizer(tokenizer = tokenizeAndStem, lowercase = False, stop_words='english')
	tfs = tfidf.fit_transform(tokenDictionary.values())
	print('Tfidf sparse matrix created. Soon will be writing this matrix into files.')
	count = len(tfs[0].toarray()[0])
	print('Creating word base')
	wordBaseBuilder(tfidf.get_feature_names(), path)
	print('Creating map files')
	dictionaryBuilder(path)
	print('Writing the matrix to files')
	print('Number of tokens: ' + str(count))
	print('Number of files: ' + str(numberOfFiles))
	if not os.path.exists('./tfidf_text'):
    		os.makedirs('./tfidf_text')
	if option != '-f':
		featureNames = tfidf.get_feature_names()
    		for col in range(numberOfFiles):
			tf_idf_page = open('./Partial_tfidf' + pageNames[col].split("PartialDB", 1)[1].split(".")[0] + "_tfidf.txt", 'w')
			print('Doing the tf-idf for the file ' + pageNames[col])
			row = tfs[col].toarray()[0]
			toWriteToFile = ""
			for x in range(count):
				temp = row[x]
				if temp != 0:
					toWriteToFile = toWriteToFile + (str(featureNames[x]) + ',' + str(row[x])[:optionPrecision] + '\n') #change for number of sig figs in tfidf files
			tf_idf_page.write(toWriteToFile)
			tf_idf_page.close()
		print('Done writing the tf-idf values')
	print('Creating idf database, a list of the idf values of every word.')
	file = open('./dictionaries/int_to_idf', 'w')
	idf = tfidf.idf_
	for x in range(len(idf)):
		file.write(str(x) + ',' + str(idf[x]) + '/n')
	file.close()

tfidfMaker()
