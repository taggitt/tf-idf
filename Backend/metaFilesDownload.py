import string
import unicodedata
import wikipedia
import sys
import requests
import os
import errno

initialList = "Wikipedia:WikiProject_Lists_of_topics"

def createDir(path):
	try:
		os.makedirs(path)
	except OSError as exception:
		if exception.errno != errno.EEXIST:
				raise

def linkFormat(link): #Removes extra characters
	links = link
	links = string.replace(links, ' ', '_')
	links = string.replace(links, '/', '__')
	links = string.replace(links, "'", '__')
	links = links.encode('ascii', 'ignore')
	links = links.rstrip()
	return links

def listExplorer(currentList, path): #Creates all directories that will be used in the crawl
	pathQueue = []
	linkQueue = []
	try:
		wikiPage = wikipedia.page(currentList)
		links = wikiPage.links
		#print(links)
		if('List of Albania-related articles' in links):
			links.remove('List of Albania-related articles')
		if('Lists of topics' in links):
			links.remove('Lists of topics')
		for link in links:
			if('List of' in link or 'Glossary of' in link or 'Catalog of' in link or 'Outline of' in link):
				linkQueue.append(link)
		while linkQueue:
			linkElement = linkQueue[0]
			linkQueue.pop(0)
			try:
				wikiPage = wikipedia.page(linkElement)
				linkElement2 = linkElement.split("_topics")[0]
				linkElement2 = linkElement2[len("List_of_"):]
				wikiPage2 = wikipedia.page(linkElement2)
				text = wikiPage.content + " " + wikiPage2.content
				file = open(linkformat(linkElement) + ".txt", "w")
				file.write(text)
				file.close()
			except:
				print("Skipped: " + linkElement)