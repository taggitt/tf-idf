To run these files, a few dependencies are needed.

From python:
Nltk with all packages (as opposed to only punkt) http://www.nltk.org/
Wikipedia https://pypi.python.org/pypi/wikipedia/
Scipy and numpy
Sklearn http://scikit-learn.org/stable/install.html

These are all the outside libraries that are used


Java doesn't use anything outside the standard libraries


The order these are run is first the data_fetcher_small_version. This requires a database folder in the directory from which it is run
it is recommended to trim a little to select only topics that are of interest (in our case, 22 high level topics)
Then, folderCopy which copies over the folder structure of the data that was fetched
Summaryfilebuilder will create metalevel files that are summary of the subjects
Compressor will create an enormour sparse matrix of all the information, which is why the trimming above is important
Finally, the java program takes the database and an input file of the form output by the tokenizer (in the site folder)