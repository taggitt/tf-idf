abandon,0.008161
abil,0.006011
abolit,0.011476
abov,0.005593
abroad,0.018976
absolut,0.014822
absorb,0.008734
academ,0.006544
academi,0.008417
accept,0.010971
accord,0.013107
acknowledg,0.016850
action,0.005368
activ,0.013079
addit,0.030942
address,0.006579
advanc,0.005396
advoc,0.067503
affect,0.011419
affirm,0.010546
afterward,0.009624
age,0.010943
aid,0.006554
aim,0.024957
akiko,0.021690
aman,0.018902
american,0.009901
amitabha,0.077062
analyt,0.023898
anarch,0.012742
anatomi,0.010261
ancient,0.044314
ando,0.019077
ani,0.003850
anim,0.006115
ankoku,0.020836
anoth,0.004202
anticip,0.009717
apex,0.013605
appear,0.010121
argu,0.058126
arinori,0.045972
aros,0.008677
arriv,0.014067
articl,0.005192
artifici,0.007378
ascet,0.028229
ask,0.007223
assassin,0.010341
associ,0.017073
assum,0.030265
atsutan,0.020497
attain,0.008844
attempt,0.029904
attent,0.007124
author,0.009839
authoris,0.012960
awaken,0.012368
awar,0.023716
away,0.006655
background,0.015008
baigan,0.022986
bamboo,0.013462
ban,0.008803
base,0.015228
basic,0.005300
becam,0.033710
becaus,0.011898
befor,0.004499
began,0.015782
behaviour,0.008122
belief,0.007052
believ,0.010898
benevol,0.013094
big,0.007977
bodi,0.010573
book,0.010014
bourgeoi,0.026655
bring,0.006549
british,0.017647
broken,0.008784
brought,0.012988
bud,0.013235
buddhahood,0.017229
buddhism,0.386231
buddhist,0.116568
build,0.005575
bukkyo,0.022986
bushido,0.038531
cabinet,0.008864
came,0.017307
capit,0.022955
carri,0.005688
catch,0.010622
central,0.009892
challeng,0.006303
chant,0.027005
charact,0.007291
characterist,0.012164
child,0.008474
china,0.027117
chines,0.068264
choei,0.022986
chomei,0.022986
chomin,0.022986
christian,0.075223
citizen,0.007221
civil,0.024212
civilian,0.009173
civilis,0.012042
class,0.029263
classic,0.005830
clearli,0.007934
climat,0.007534
close,0.014165
code,0.006828
collect,0.010295
coloni,0.007297
combin,0.005032
commerc,0.008793
common,0.004267
complet,0.009748
compuls,0.014592
concept,0.004925
concern,0.004954
condit,0.009876
confucian,0.179674
confuciu,0.042657
connect,0.005810
conscious,0.008990
consid,0.008231
consider,0.011862
consist,0.004723
constitut,0.016717
construct,0.005465
contact,0.022217
contemporari,0.006903
contin,0.009094
continent,0.018798
continu,0.004276
contradict,0.018854
contrast,0.011923
conveni,0.009423
cooper,0.006745
cope,0.011327
core,0.007494
correct,0.014214
countri,0.032511
court,0.006694
creat,0.008633
crimin,0.017879
critic,0.031148
criticis,0.010388
cultur,0.074293
current,0.004518
daibutsu,0.022986
danc,0.011318
darwin,0.019753
day,0.026629
death,0.006181
decis,0.005950
declar,0.006816
declin,0.013401
dedic,0.008477
deep,0.007857
deepli,0.019930
democraci,0.007861
democrat,0.013830
demonstr,0.006343
deni,0.016363
depend,0.009313
depict,0.008803
deriv,0.021465
design,0.005183
desir,0.006642
desper,0.012473
develop,0.035296
did,0.005239
diet,0.020871
differ,0.011224
difficult,0.006155
diplomat,0.008384
direct,0.014059
discrimin,0.009800
disillus,0.014853
disord,0.009097
distinguish,0.006265
dogen,0.017710
domin,0.011615
doshisha,0.021690
doubl,0.007813
draft,0.009108
drew,0.009726
drive,0.007583
dure,0.024376
dutch,0.034672
dynasti,0.009177
eagerli,0.016059
earli,0.021249
earnest,0.013403
earnestli,0.018302
earthli,0.013669
earthquak,0.010762
eastern,0.014105
edo,0.110237
educ,0.021652
effect,0.004127
eisai,0.039862
elabor,0.008841
elect,0.018801
emori,0.016351
emot,0.008728
emperor,0.074969
emphas,0.007457
emphasi,0.007707
emphasis,0.011449
encourag,0.013967
encyclopedia,0.007318
end,0.004645
engag,0.013457
england,0.007373
english,0.011835
enhanc,0.007870
enlighten,0.055668
entir,0.005511
environ,0.005632
epistemolog,0.010098
era,0.114128
erect,0.023044
esoter,0.038980
essay,0.008234
essenc,0.009598
establish,0.040811
ethic,0.031281
european,0.005292
evalu,0.007362
evidenc,0.011964
excav,0.011422
excis,0.013543
exclus,0.007258
execut,0.006436
exist,0.012675
expans,0.007169
experi,0.005225
express,0.005505
expuls,0.012460
extern,0.006645
faith,0.025483
famili,0.017650
familylik,0.022986
famou,0.007019
farm,0.008477
fascist,0.012597
father,0.007470
feel,0.007943
femal,0.008531
feminin,0.013605
feminist,0.011137
feudal,0.054882
final,0.016487
firmli,0.011106
fittest,0.015105
focu,0.006233
folklor,0.012757
folklorist,0.016826
follow,0.010903
forc,0.004554
forefront,0.013365
foreign,0.023659
form,0.038203
formerli,0.008511
fought,0.009637
franc,0.013009
freedom,0.021518
french,0.018344
frequent,0.006387
fukuzawa,0.043380
fusa,0.021690
fusion,0.021181
futur,0.005533
gather,0.007920
gave,0.013223
gender,0.009740
gener,0.006529
genpaku,0.021690
german,0.006416
giri,0.020198
given,0.004557
goal,0.006249
gone,0.010082
govern,0.069131
grace,0.011975
gradual,0.007303
great,0.010241
ha,0.006059
haibutsu,0.022986
hajim,0.017710
hakko,0.022986
handl,0.008262
happi,0.010046
hard,0.007457
hardi,0.013308
harmon,0.022088
harmoni,0.010597
hayashi,0.017060
heaven,0.010881
heavili,0.007325
heian,0.104467
help,0.004974
hermit,0.014501
hi,0.081478
high,0.009201
highest,0.013915
highli,0.011962
hirata,0.019931
hiratsuka,0.022986
hiromatsu,0.045972
hiroyuki,0.020497
histor,0.005103
hitoshi,0.019931
hokkekei,0.022986
holi,0.009620
honen,0.037170
honesti,0.013847
howev,0.036874
human,0.035019
ichikawa,0.043380
ichiu,0.022986
idea,0.005099
ideal,0.022179
ideolog,0.017165
ii,0.005946
ikki,0.020198
illus,0.011218
imag,0.006996
imperi,0.053107
import,0.011625
inazo,0.022986
incant,0.017818
incid,0.017980
includ,0.003154
independ,0.009333
indian,0.007521
indigen,0.009059
individu,0.023432
industri,0.005186
influenc,0.060053
initi,0.014810
insist,0.055622
instead,0.010643
intellectu,0.023384
intent,0.015292
intern,0.003981
interpret,0.012123
introduc,0.031979
ippen,0.021690
irifuji,0.022986
ishida,0.020497
isol,0.007376
itagaki,0.039862
ito,0.016752
itsukushimi,0.022986
japan,0.097677
japanes,0.320387
jd,0.014623
jdo,0.020198
jeanjacqu,0.013543
jesu,0.011129
jianzhen,0.021690
jinsai,0.021227
jishu,0.022986
jodo,0.040995
jomin,0.022986
joseph,0.007487
journey,0.010558
js,0.015182
just,0.010595
kamakura,0.054517
kami,0.017506
kamo,0.019265
kamono,0.022986
kansei,0.020198
kanto,0.022986
kanzo,0.022986
kara,0.016290
karatani,0.022986
kato,0.017818
katsunan,0.022986
kawakami,0.020836
kazan,0.017411
kenko,0.020836
kishaku,0.022986
kita,0.020198
kitaro,0.019931
knowledg,0.005714
known,0.007592
kojiki,0.038155
kojin,0.022986
kokubunji,0.022986
kokubunniji,0.022986
kokugaku,0.141390
kokutai,0.022986
kotoku,0.022986
kuga,0.021690
kukai,0.020497
kumagusu,0.022986
kunio,0.020836
ky,0.016980
kya,0.019077
kyoho,0.022986
kyoto,0.028028
land,0.028746
languag,0.005709
late,0.042074
later,0.009035
law,0.013728
leader,0.006642
leagu,0.009320
learn,0.051049
led,0.019610
legal,0.006268
legislatur,0.009293
legitim,0.009435
liber,0.006848
life,0.014648
lifestyl,0.010817
lifetim,0.009726
link,0.006605
literari,0.009526
literatur,0.006786
live,0.004911
local,0.010289
look,0.006271
lotu,0.029775
love,0.009017
ma,0.009594
mabuchi,0.021227
maeno,0.022986
magazin,0.008793
mainli,0.006305
mainstream,0.009217
major,0.004029
make,0.015450
mani,0.010299
manner,0.007330
mantra,0.014922
manyoshu,0.045972
mapp,0.019689
maruyama,0.022986
marxism,0.011878
marxist,0.010312
masahiro,0.019077
masao,0.018439
masculin,0.013502
mass,0.006228
mastermind,0.016544
masuraoburi,0.022986
mean,0.004183
medicin,0.007241
mediev,0.016202
medit,0.023081
meiji,0.179496
meirokusha,0.039862
member,0.004846
membership,0.008366
menciu,0.017411
merchant,0.009021
merleauponti,0.015471
middl,0.018787
mikkyo,0.041672
militari,0.024577
militarismfasc,0.022986
militarist,0.015030
minakata,0.022986
mind,0.007162
minist,0.006853
minob,0.022986
mission,0.007573
mitogaku,0.022256
model,0.004731
modern,0.031891
monarchi,0.009736
monism,0.013365
monist,0.014356
monitor,0.008213
monk,0.044939
mono,0.016544
mononob,0.021690
moral,0.007598
mori,0.028385
morioka,0.020497
motherhood,0.017060
motoori,0.019931
motoyoshi,0.022986
mountain,0.008171
movement,0.062850
multisubject,0.021690
mumeo,0.022986
muneyoshi,0.022986
myh,0.019265
nagai,0.019265
naka,0.019265
namu,0.019931
namuamidabutsu,0.022986
nara,0.098483
narrowmind,0.019468
nation,0.058922
nationalist,0.029544
nationwid,0.011540
natur,0.020274
neesima,0.022986
neg,0.006586
neoconfucian,0.142177
new,0.016963
newlyrisen,0.022986
newspap,0.009403
nichiren,0.052233
nihon,0.017411
nihonjinron,0.022256
ninomiya,0.021690
nishi,0.020836
nishida,0.037477
nitob,0.021690
nobl,0.009876
nonchurch,0.020836
nonoffici,0.017606
norinaga,0.019931
noya,0.022986
obedi,0.013235
object,0.015576
occupi,0.007614
occur,0.004825
octopu,0.016230
offici,0.011532
ogyu,0.022986
oku,0.020198
old,0.006284
oligarchi,0.014063
omori,0.039379
oneself,0.011888
onli,0.013985
onlin,0.006647
ontolog,0.020811
open,0.005199
oppos,0.012694
opposit,0.006249
oppress,0.022404
order,0.008371
ordin,0.011202
organ,0.004142
organis,0.007191
origin,0.025255
orikuchi,0.022986
osugi,0.022986
overcam,0.014273
paid,0.007744
paper,0.006267
paradis,0.040816
parliamentari,0.017396
parti,0.011739
particip,0.005991
particular,0.009508
peac,0.021975
peopl,0.046064
perform,0.005339
period,0.022565
person,0.019880
pessim,0.015301
phenomenolog,0.011485
philosoph,0.020000
philosophi,0.122864
plan,0.011504
platform,0.009301
play,0.010967
point,0.013217
polic,0.016894
polici,0.011241
polit,0.071505
poor,0.007509
popul,0.005404
popular,0.054340
posit,0.013504
positivist,0.012912
possibl,0.004442
pot,0.013327
poverti,0.017919
power,0.022285
practic,0.041939
practis,0.011458
practition,0.009140
pray,0.013543
prayer,0.011476
preach,0.049735
press,0.005126
preval,0.009589
prewar,0.013043
priest,0.010312
princ,0.028197
principl,0.010456
privat,0.012490
problem,0.009316
profit,0.007525
promot,0.006212
promulg,0.011106
protect,0.005804
public,0.004386
publish,0.009668
pupil,0.045445
pure,0.048209
pursu,0.023762
quaker,0.014818
radic,0.023952
raicho,0.068958
rais,0.006424
rangaku,0.074955
rapidli,0.007342
ration,0.007506
razan,0.020198
reach,0.011227
reaction,0.007082
real,0.005733
realiz,0.007723
rebellion,0.009862
recommend,0.008142
refer,0.005524
reform,0.013513
regard,0.015898
regent,0.013271
reincarn,0.013669
relat,0.011173
relationship,0.005272
reli,0.006655
relief,0.019696
reliev,0.011559
religi,0.007055
religion,0.035176
remodel,0.030603
ren,0.021649
reng,0.019265
repay,0.011674
represent,0.006968
rescript,0.037477
resist,0.007157
resourc,0.005592
respect,0.021012
restor,0.054682
rever,0.012330
revis,0.008254
revolut,0.013685
rich,0.007920
right,0.034697
rightist,0.016752
rinzai,0.038531
rise,0.005558
rissho,0.022986
ron,0.013522
rose,0.008382
rousseau,0.025824
routledg,0.009843
rule,0.005042
russojapanes,0.081756
ryotaku,0.022986
safe,0.036125
safeguard,0.011846
saicho,0.020836
said,0.005675
saka,0.017606
sake,0.011540
sakoku,0.057796
sakuma,0.021690
sakuzo,0.022986
salvat,0.011931
samurai,0.097740
satsuma,0.021690
save,0.007844
say,0.005984
scholar,0.014029
school,0.078678
secretarygener,0.014140
sect,0.080982
secur,0.017605
seen,0.005419
seido,0.022986
seitosha,0.022986
selfawaken,0.020497
selfish,0.012944
selfsuffici,0.011606
senior,0.009155
serv,0.005498
sever,0.004007
shame,0.014356
share,0.005382
shigeki,0.022986
shingon,0.017818
shinobu,0.021227
shinran,0.038155
shinto,0.166156
shoeki,0.022986
shogun,0.143091
shoki,0.019265
shomu,0.022986
shotoku,0.061492
shozan,0.022986
shozo,0.045972
shusui,0.022986
simpl,0.006108
sin,0.010932
sinc,0.003844
singl,0.005144
sino,0.080794
sit,0.010162
situat,0.005918
social,0.078246
socialist,0.025800
societi,0.028898
sociopolit,0.013543
soga,0.041672
soho,0.016752
soko,0.021690
solips,0.016611
solv,0.014149
sontoku,0.022986
soon,0.014098
sorai,0.021227
soto,0.015471
sovereign,0.018376
sovereignti,0.027811
space,0.005926
spearhead,0.013423
special,0.004807
spirit,0.052116
spiritu,0.008857
split,0.007900
spoke,0.010609
spread,0.027180
st,0.006197
stabil,0.007169
state,0.030272
statesmen,0.014442
statism,0.017060
stimul,0.008603
straightforward,0.012170
strengthen,0.008695
strictli,0.009066
strike,0.008551
strongli,0.007409
structur,0.004491
struggl,0.008058
studi,0.065002
style,0.007920
subject,0.004890
substanti,0.007136
substitut,0.008542
succeed,0.007859
success,0.005098
suffrag,0.022603
sugita,0.021690
suiko,0.021690
suitabl,0.008533
support,0.018283
suppress,0.017606
supremaci,0.012569
surviv,0.006630
sutra,0.027169
synthes,0.009990
tafel,0.022986
taish,0.018172
taisho,0.019468
taisuk,0.021690
takaaki,0.043380
takano,0.021690
tang,0.012420
taoism,0.013146
tatsukichi,0.022986
taught,0.008571
tdaiji,0.020497
teach,0.037654
teacher,0.008809
technolog,0.005542
templ,0.040649
tendai,0.017318
tetsuro,0.021227
text,0.006631
themselv,0.005702
theolog,0.009371
theoret,0.006303
theori,0.022530
therefor,0.005225
thi,0.025878
thinker,0.018586
thoroughli,0.012182
thought,0.152552
threat,0.008063
threw,0.014089
thrift,0.014958
tie,0.014997
time,0.010204
todaiji,0.021690
today,0.005522
tokugawa,0.174325
tokutomi,0.022986
toler,0.009726
took,0.006005
tool,0.006155
tradit,0.019363
translat,0.013236
treason,0.027338
treat,0.006731
treatis,0.009274
trend,0.029371
truth,0.008179
uchimura,0.022986
ueki,0.022986
ultranation,0.020836
underlin,0.013482
understand,0.005218
unfold,0.012640
unif,0.010932
union,0.005712
unionist,0.014219
uniqu,0.006554
unit,0.011632
univers,0.003981
upper,0.007763
use,0.009423
utilitarian,0.024365
variou,0.013129
verbal,0.011059
veri,0.004390
virtu,0.019481
vorilong,0.022986
wa,0.119777
wabisabi,0.022986
wang,0.012447
war,0.032765
watanab,0.019077
wataru,0.043380
watsuji,0.021690
way,0.004110
west,0.013201
western,0.075174
wherea,0.006670
whisk,0.018585
wide,0.009902
widespread,0.007385
william,0.006160
window,0.010803
woman,0.009598
women,0.021948
work,0.007537
world,0.031179
worldli,0.027979
wrestl,0.014531
wrote,0.006549
xi,0.059233
yamaga,0.021690
yamato,0.016980
yamatodamashii,0.022986
yanagi,0.022986
yanagita,0.020836
yangm,0.017930
yasuo,0.037477
yosano,0.022986
yoshida,0.017710
yoshimoto,0.043380
yoshino,0.019931
yuasa,0.045972
yukichi,0.021690
yushima,0.021690
zaibatsu,0.019077
zazen,0.018738
zen,0.097092
zhu,0.073276
