abba,0.018420
abdul,0.018554
abil,0.008297
abov,0.007720
abraham,0.028326
accept,0.007571
accord,0.012060
account,0.007102
achiev,0.007586
action,0.007409
activ,0.012035
activist,0.014386
adher,0.012198
admiel,0.031725
adopt,0.016644
advoc,0.020704
afghan,0.020140
afghanistan,0.015472
age,0.015104
agre,0.009092
ali,0.014950
alleg,0.024939
almond,0.022165
altern,0.007749
american,0.040996
analog,0.010775
analysi,0.007269
ancient,0.008737
andrew,0.012247
angel,0.028924
anglican,0.018025
ani,0.015944
anthropoet,0.031725
antimuslim,0.031725
anton,0.017486
anyon,0.026295
ap,0.018217
apart,0.011051
appealand,0.031725
appear,0.013970
applebi,0.055018
appli,0.032234
approach,0.013880
appropri,0.009666
arabia,0.014752
archbishop,0.037614
armstrong,0.018608
aros,0.011976
arthur,0.012302
articl,0.007167
ashkenazi,0.023662
asia,0.010354
aspect,0.015852
assembl,0.009414
assert,0.020719
associ,0.041238
atheist,0.082268
athena,0.021943
aton,0.021735
attach,0.011417
attack,0.019446
attempt,0.013757
attitud,0.012380
attribut,0.009374
audienc,0.013612
aug,0.020315
avoid,0.009007
ayatollah,0.022568
b,0.015975
backer,0.021735
ballantin,0.024443
baptist,0.052824
barr,0.020694
barri,0.015967
base,0.005254
bassam,0.026331
battl,0.011389
bbc,0.027439
beach,0.015753
bead,0.018749
becam,0.013293
becaus,0.016421
becom,0.011642
belief,0.048670
believ,0.007520
benjamin,0.013788
bert,0.022165
bibl,0.058329
biblic,0.031243
billi,0.020954
bin,0.016765
biola,0.063451
birth,0.010714
bodili,0.015890
book,0.027644
born,0.010272
bottl,0.017778
branch,0.007733
brasher,0.031725
brenda,0.022743
brief,0.011525
broader,0.012124
broadli,0.012172
brother,0.012194
brown,0.012338
bruce,0.014940
buddhism,0.029615
buddhist,0.073131
burma,0.018072
came,0.015925
canada,0.010431
caplan,0.024163
carol,0.018049
carri,0.015702
case,0.006089
catholic,0.016322
centuri,0.050031
certain,0.019997
challeng,0.008699
chapel,0.018692
character,0.036949
chicago,0.047479
choos,0.010476
christ,0.015540
christian,0.103823
christma,0.051484
church,0.022008
citat,0.014504
claim,0.044358
classic,0.008046
classifi,0.009851
coalit,0.011802
coin,0.020171
collect,0.007104
combat,0.012784
come,0.006966
committe,0.010449
common,0.011779
commun,0.006369
complet,0.013454
comprehend,0.017253
comprehens,0.010729
compromis,0.014003
concept,0.013595
concern,0.006838
condemn,0.027439
confer,0.019176
conflict,0.018144
conglomer,0.018193
connot,0.032199
conserv,0.038676
consid,0.005680
consider,0.008186
contamin,0.015540
contemporari,0.009528
context,0.008568
continu,0.005902
contrari,0.012737
contrast,0.008228
controversi,0.038395
convent,0.009432
council,0.008882
counter,0.013171
countercultur,0.021063
countri,0.006410
creat,0.005958
creation,0.008866
creed,0.018778
crisi,0.010052
critic,0.057321
cross,0.010631
cultur,0.021972
curti,0.019308
dalai,0.022483
danger,0.011442
dawkin,0.019480
day,0.007350
death,0.017062
decemb,0.008939
declar,0.009408
deem,0.013182
defend,0.021797
defici,0.015449
defin,0.026740
deiti,0.015586
delus,0.019274
demand,0.008696
denomin,0.043539
depend,0.012854
describ,0.011991
descript,0.008904
desir,0.009167
develop,0.009743
did,0.007230
differ,0.005164
dimens,0.011208
discern,0.016006
dismiss,0.012978
display,0.011070
distil,0.017446
distinct,0.007502
distort,0.013754
divers,0.009368
divin,0.012956
doctrin,0.035045
doe,0.006591
dogma,0.066611
dogmat,0.036485
domin,0.008015
dorff,0.055756
dorj,0.053181
dr,0.012085
draw,0.009985
dure,0.005607
dynam,0.009132
e,0.015787
earli,0.005865
earnest,0.018500
earth,0.009207
eclect,0.018895
econom,0.025672
economist,0.022020
ed,0.032840
edinburgh,0.015954
editor,0.012484
eerdman,0.023547
effervesc,0.027509
elitist,0.021476
elliot,0.041000
emmanuel,0.019241
emphasi,0.010637
encyclopedia,0.010100
end,0.006412
enemi,0.026214
enthusiasm,0.017888
eschatolog,0.020315
especi,0.006809
essenti,0.016304
establish,0.006258
ethnic,0.011572
evangel,0.055262
evangelist,0.022016
event,0.007631
evid,0.007997
evil,0.014265
exagger,0.016350
exampl,0.010504
exclus,0.020037
exemplifi,0.014940
exist,0.005831
experi,0.057694
experienc,0.010528
explain,0.007894
express,0.007598
expuls,0.017198
extern,0.004585
extract,0.011288
extrem,0.025237
face,0.008802
facil,0.011241
faith,0.046896
fallibl,0.020361
famili,0.024361
featur,0.015862
fidel,0.017778
fierc,0.016126
fight,0.011004
fighter,0.015928
financ,0.009292
flexibl,0.012983
fluid,0.012361
follow,0.015049
footnot,0.015633
form,0.004793
formul,0.010042
franc,0.008977
francisco,0.027067
fraud,0.014445
free,0.021489
freedom,0.009899
fund,0.008646
fundament,0.554116
fundamentalist,0.442162
fundamentalistmodernist,0.029936
furthermor,0.010746
gabriel,0.015827
galicia,0.021234
gelugpa,0.026590
gener,0.009012
georg,0.018076
gershom,0.027878
gershon,0.029298
god,0.076831
goe,0.022571
goldstein,0.020796
gorenberg,0.031725
govern,0.005963
graham,0.015980
grand,0.012181
group,0.062225
groupsmainli,0.031725
growth,0.007937
ha,0.079449
haaretz,0.026089
hallucin,0.020796
handl,0.011403
hardlin,0.021353
haredi,0.024748
harper,0.016153
harvard,0.012252
hasid,0.022834
headcov,0.029936
headscarv,0.026870
higher,0.014910
hijab,0.026089
hime,0.028290
hinderi,0.058596
hindu,0.043616
histor,0.014088
histori,0.010446
hold,0.015107
holi,0.013277
hostag,0.018608
howard,0.029095
howev,0.010178
human,0.024167
hungari,0.015529
ian,0.015483
ideal,0.010203
ident,0.008979
identifi,0.007580
ideolog,0.047383
ideologu,0.023662
ignatiu,0.022834
impli,0.009749
import,0.005348
imposit,0.018025
imposs,0.010603
imprison,0.014911
includ,0.013059
inde,0.010662
indic,0.007714
indisput,0.021234
indoctrin,0.023122
inextric,0.021669
infal,0.021008
influenti,0.010085
ingroup,0.021873
inquisit,0.019208
insight,0.011632
inspir,0.010375
instanc,0.008492
instig,0.017125
institut,0.013271
intellig,0.019569
intern,0.005495
interpret,0.016732
interview,0.025934
intim,0.015693
intoler,0.018267
invent,0.010203
investig,0.008787
involv,0.006079
iran,0.014225
iranian,0.031630
irredentist,0.023903
irreduc,0.018292
isbn,0.088959
islam,0.146149
issu,0.006729
jame,0.008844
japan,0.010370
jesu,0.046084
jewish,0.065001
jim,0.016574
john,0.007132
journal,0.008065
judaism,0.044997
judith,0.018986
kadampa,0.028758
karen,0.017956
karl,0.011794
keat,0.020848
key,0.007979
kharijit,0.046656
khomeini,0.023328
kind,0.008353
known,0.010479
kosman,0.031725
label,0.022564
laden,0.020140
laissezfair,0.018049
lama,0.021476
lampoon,0.024592
languag,0.015761
late,0.036294
law,0.018947
lawrenc,0.014026
lead,0.012511
led,0.006766
lee,0.014257
leftw,0.017291
legend,0.014770
level,0.006269
liber,0.009452
like,0.005599
link,0.009117
lionel,0.020271
list,0.005829
listen,0.014807
liter,0.036167
literalmind,0.031725
live,0.006778
lo,0.028757
lock,0.013579
london,0.008827
lord,0.012408
lower,0.007746
lustik,0.031725
lyman,0.023223
m,0.007497
macmillan,0.014321
mahayana,0.019241
main,0.006636
mainstream,0.012722
maintain,0.021929
major,0.011123
make,0.010662
malis,0.027878
mani,0.009476
manzar,0.031725
mark,0.016834
markedli,0.017933
market,0.015694
marsden,0.042952
marti,0.023328
martin,0.011096
materi,0.007346
materialist,0.016949
mattingli,0.026089
mean,0.011546
meanwhil,0.012676
measur,0.006866
media,0.019231
meet,0.008482
mellen,0.024592
member,0.033443
merg,0.012626
messag,0.013028
method,0.006492
milit,0.016379
milton,0.015563
mind,0.049430
minor,0.009701
miracl,0.016365
mistak,0.014428
mix,0.010083
modern,0.006288
modernist,0.018778
morgan,0.014883
mount,0.012221
movement,0.050601
moyer,0.023328
mujahiddin,0.028758
muslim,0.070344
myanmar,0.039033
n,0.018828
nagata,0.025261
nation,0.011617
nativ,0.032506
natten,0.031725
natur,0.005596
necessit,0.016881
need,0.012604
neg,0.018181
neutral,0.011474
new,0.032778
news,0.011442
niagara,0.047807
nichiren,0.024031
nihil,0.019973
nineteenth,0.013113
noll,0.023436
nonreligi,0.020183
nonsens,0.018072
norm,0.012159
northern,0.009800
note,0.006044
notion,0.009918
novemb,0.009402
object,0.021498
observ,0.006918
occasion,0.011825
onli,0.004825
onlin,0.009175
open,0.014353
opinion,0.011142
oppon,0.013022
order,0.005776
organ,0.005718
origin,0.011619
orthodox,0.028233
osama,0.022927
outgroup,0.022242
oxford,0.029549
p,0.008002
pakistan,0.014329
paranorm,0.020796
particip,0.008269
particularli,0.007351
partner,0.011460
pejor,0.054877
peopl,0.005779
perceiv,0.010542
perfect,0.011712
perform,0.007369
perilof,0.031725
perspect,0.018761
pessimist,0.019480
philosophi,0.008925
play,0.022706
point,0.006080
polaris,0.019376
polici,0.007757
polit,0.065795
posit,0.018638
pp,0.009800
practic,0.012863
prais,0.014056
preconvent,0.026590
prefigur,0.022016
presbyterian,0.040112
present,0.006010
press,0.077825
pretens,0.020406
previou,0.009065
primari,0.007935
princeton,0.013271
principl,0.021648
privat,0.008619
problem,0.006429
process,0.005682
professor,0.021487
program,0.007561
project,0.014876
promin,0.009426
promot,0.008575
propos,0.007416
proselyt,0.021063
protector,0.029747
protest,0.022343
psycholog,0.020493
public,0.006054
publish,0.026688
puriti,0.034036
purpos,0.007819
q,0.013463
qatar,0.018720
qualiti,0.008956
queen,0.012616
quiet,0.017608
quiggin,0.025651
r,0.016088
ra,0.017291
rabbi,0.019973
radic,0.033059
rahman,0.020361
ralli,0.015827
rapid,0.010417
rawa,0.031725
reaction,0.009775
reaffirm,0.018096
real,0.007912
realiti,0.010370
recent,0.013203
recommend,0.011238
reduc,0.007222
refer,0.019061
reflect,0.008193
refus,0.011125
regardless,0.011525
reject,0.018797
relat,0.005140
religi,0.194769
religion,0.067970
religionneutr,0.031725
religionthat,0.031725
religios,0.021063
religiouslik,0.031725
remov,0.008802
repres,0.006253
research,0.006184
restrict,0.017672
result,0.015931
resurrect,0.016914
retir,0.012972
return,0.007715
revolt,0.013880
revolut,0.028333
revolutionari,0.012133
rhetor,0.014761
richard,0.009418
right,0.006841
rightw,0.017587
riot,0.015233
rise,0.015343
robert,0.008382
roderick,0.040454
root,0.038522
rosenberg,0.019893
rosett,0.026590
routledg,0.013585
row,0.014743
ruthven,0.025863
s,0.027035
said,0.015666
saidi,0.028758
sampl,0.011361
san,0.024214
saudi,0.015540
say,0.008259
scene,0.013733
scholar,0.029045
school,0.023269
scientif,0.007671
scott,0.028218
scripp,0.023436
scriptur,0.030679
search,0.009758
sect,0.047902
secta,0.028758
sectarian,0.019737
secular,0.026389
seen,0.007480
selfdecept,0.023436
seminari,0.020315
sens,0.031378
sephard,0.022834
servic,0.007428
set,0.017717
sever,0.005531
shakubuku,0.031725
shape,0.009086
share,0.007429
shariati,0.027878
shia,0.036005
shimon,0.023662
shugden,0.095176
signifi,0.015383
similar,0.012671
similarli,0.010052
simon,0.012748
sin,0.030177
sinc,0.005305
singapor,0.013981
situat,0.008169
sivan,0.027878
skeptic,0.013974
snow,0.016438
social,0.013499
societi,0.013295
sociologist,0.015058
sole,0.010770
solv,0.009764
sometim,0.006907
soon,0.009729
sourc,0.006674
soviet,0.011492
space,0.008180
specif,0.012696
spread,0.009378
spring,0.011967
standard,0.007146
start,0.006716
state,0.032497
staterun,0.019893
steve,0.016307
stewart,0.016847
stray,0.020227
strict,0.024658
strident,0.023547
strong,0.015419
struggl,0.011122
studi,0.016821
stylebook,0.045486
subject,0.006749
substanc,0.011155
suni,0.020694
sunni,0.033497
superstiti,0.022320
support,0.006308
sword,0.017387
sy,0.021063
symbol,0.009632
takfir,0.029936
taliban,0.021293
target,0.009879
teach,0.010394
templ,0.014026
tendenc,0.023828
tenet,0.046865
tension,0.024103
term,0.065177
terri,0.017933
testimoni,0.017071
tex,0.025450
text,0.018305
th,0.042588
theolog,0.129341
theologian,0.015693
theravada,0.020098
therefor,0.007211
thi,0.011905
thing,0.008442
think,0.009201
thought,0.022559
thu,0.006202
thurman,0.026331
tibetan,0.018500
tibi,0.028758
time,0.018778
told,0.013456
torrey,0.023662
trace,0.020348
tradit,0.026725
translat,0.009134
transmit,0.013000
tree,0.010929
true,0.008590
truth,0.022577
turn,0.007335
twelv,0.013996
twentieth,0.012518
typic,0.007118
uk,0.009725
ultranationalist,0.024911
unbeliev,0.022089
understand,0.028808
unequivoc,0.019625
unfett,0.021414
unit,0.032109
univers,0.043962
unwav,0.025081
urban,0.011128
usa,0.011845
usag,0.011128
use,0.039019
usual,0.012770
vajrayana,0.021873
valu,0.006800
van,0.011606
variant,0.012853
variou,0.018120
veri,0.006059
version,0.037125
view,0.020401
violenc,0.025272
virgin,0.016251
vital,0.011849
volum,0.045443
wa,0.026102
wahhabi,0.023122
wake,0.014155
wale,0.029541
want,0.009795
watchmanexamin,0.031725
watson,0.015815
way,0.017019
wear,0.028757
websit,0.010033
western,0.015962
wherebi,0.011988
willing,0.016209
wilson,0.013253
winterv,0.031725
women,0.040391
word,0.036825
worldwid,0.010764
worthi,0.016732
write,0.008295
wwwblessedquietnesscom,0.031725
xv,0.019662
yahya,0.021943
year,0.005354
yeasti,0.029936
york,0.032687
young,0.009851
zealotri,0.027175
zionism,0.022834
