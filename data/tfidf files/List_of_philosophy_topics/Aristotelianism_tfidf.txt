abbasid,0.015453
abil,0.006950
abridg,0.017212
accept,0.006342
accord,0.015155
account,0.017849
activ,0.015122
actual,0.012931
addit,0.005110
adher,0.010218
adopt,0.013943
advanc,0.006239
aim,0.007213
alasdair,0.053512
albertu,0.052796
alexand,0.009679
alexandrian,0.017837
alfarabi,0.034425
alhakam,0.023699
alkindi,0.033532
allow,0.005023
allud,0.015431
almamun,0.019821
alrashid,0.020130
analysi,0.006089
analyt,0.018420
ancient,0.007319
ani,0.008904
annot,0.013833
aphrodisia,0.020601
apolit,0.036087
appar,0.008624
appear,0.023405
appli,0.021602
aquina,0.042637
arab,0.055985
arabian,0.014044
arabicspeak,0.020476
area,0.005017
argument,0.016975
aristot,0.021161
aristotelian,0.537354
aristotelicum,0.044549
aristotl,0.541540
armstrong,0.015588
aros,0.010032
arristteelinizm,0.026576
assum,0.006998
attempt,0.005762
author,0.005688
authorit,0.014099
avail,0.018526
averro,0.115081
avicenn,0.022509
avicenna,0.057025
bad,0.010363
baghdad,0.014030
ban,0.010178
base,0.004401
basi,0.006292
becam,0.022272
becaus,0.004585
began,0.006082
belong,0.008607
bertrand,0.013269
best,0.040282
bibliographi,0.008859
birth,0.008975
block,0.009074
blur,0.014183
bodi,0.006112
boethiuss,0.024090
book,0.028946
brentano,0.039842
build,0.006446
built,0.007593
c,0.034978
caliph,0.027591
came,0.013340
capella,0.020356
capit,0.026540
caus,0.005445
centr,0.008388
central,0.005719
centuri,0.047149
chapter,0.009508
character,0.015476
charg,0.007873
christian,0.039533
church,0.009218
circul,0.010674
citizen,0.016698
claim,0.018579
class,0.006766
clayton,0.017645
come,0.005835
comment,0.039092
commentari,0.105860
comparison,0.008556
complet,0.011271
concentr,0.007985
conceptu,0.009821
concern,0.005728
conclud,0.008345
concordist,0.025732
condemn,0.011492
consequ,0.006938
conserv,0.016199
consist,0.005461
constantinopl,0.014113
constitut,0.012885
construct,0.006318
contemporari,0.039908
continu,0.004944
contrast,0.027571
conveni,0.010894
convent,0.015803
copi,0.009837
cordoba,0.019206
corpu,0.041424
correct,0.008217
corrupt,0.010018
cosmolog,0.011929
countri,0.005369
crdoba,0.017692
creativ,0.010543
cremona,0.019821
critic,0.042016
critiqu,0.020734
d,0.013211
david,0.007248
day,0.006157
declin,0.007747
deduct,0.011806
defect,0.012045
defend,0.027389
defin,0.005600
despit,0.006919
dethron,0.019632
develop,0.004081
did,0.006057
differ,0.004325
difficult,0.007117
digest,0.012467
disastr,0.014271
discipl,0.027274
discuss,0.006658
distinct,0.018854
distinguish,0.007244
doctrin,0.019571
domin,0.006714
dominican,0.014681
dozen,0.011414
duarum,0.026576
dure,0.009394
earli,0.014741
earlier,0.007259
ecclesiast,0.014631
edward,0.008937
effect,0.009545
effort,0.020411
eleventh,0.030522
emphat,0.017094
empir,0.020565
employ,0.006516
encyclopedia,0.008461
end,0.026856
enquiri,0.014874
entir,0.012744
era,0.015524
erron,0.014071
error,0.009341
especi,0.017114
essenti,0.013658
et,0.008444
ethic,0.054251
europ,0.033307
european,0.006119
eventu,0.006982
everi,0.006147
ex,0.013290
exampl,0.026399
excommun,0.018043
exist,0.009770
extern,0.003841
factor,0.006229
fall,0.006705
fals,0.009963
famou,0.032464
famous,0.012760
far,0.026646
figur,0.007275
flourish,0.010812
follow,0.025213
forbid,0.014127
foreign,0.006838
form,0.004015
formal,0.013147
format,0.007165
framework,0.007526
franklin,0.012017
fred,0.027250
furthermor,0.009002
gadam,0.079684
gener,0.003774
gerard,0.015475
glean,0.016871
golden,0.053223
good,0.023362
great,0.011841
greatest,0.018489
greek,0.044634
guid,0.007427
ha,0.010508
harmon,0.012769
harun,0.019921
heaven,0.012581
hegel,0.057382
heidegg,0.031708
hellenist,0.013354
help,0.011502
heterodoxi,0.021665
hi,0.117756
highest,0.016089
histori,0.013126
homo,0.013770
hous,0.007047
howev,0.008526
huge,0.009769
human,0.020244
hume,0.027345
hunayn,0.020601
hursthous,0.046707
hylomorph,0.019206
ibn,0.011436
idea,0.023584
ident,0.015044
identifi,0.012700
ii,0.006875
impact,0.014398
import,0.035843
imposs,0.008882
includ,0.025527
incorpor,0.021641
independ,0.005395
ineffect,0.013290
influenc,0.034716
influenti,0.008448
ingredi,0.013795
initi,0.005707
insinu,0.020476
inspir,0.008691
instead,0.012306
institut,0.011117
intellectu,0.027036
intern,0.013810
internet,0.008617
interpret,0.028033
introduc,0.012324
introduct,0.006964
ishaq,0.038573
isidor,0.016732
islam,0.052469
jame,0.014818
jesuit,0.014715
jewish,0.010890
jr,0.022316
juxtapos,0.018567
kant,0.038124
kierkegaard,0.016203
know,0.007959
knowledg,0.039641
known,0.026335
lampsacu,0.022274
larg,0.013886
later,0.010446
latin,0.074294
lead,0.010480
learn,0.014755
legaci,0.011043
legitim,0.021817
librari,0.008251
life,0.005645
lifetim,0.011246
linger,0.015957
link,0.003818
littl,0.006721
logic,0.067999
macintyr,0.124178
macintyrean,0.026576
magnu,0.076618
maimonid,0.016409
main,0.005559
major,0.009318
make,0.017863
malet,0.022764
man,0.007770
manageri,0.028571
mani,0.027785
mankind,0.012673
margin,0.009624
martianu,0.020356
marx,0.024549
massiv,0.009344
mathemat,0.013807
mcdowel,0.079684
mediev,0.009366
mediterranean,0.011487
member,0.028015
merit,0.012531
metaphys,0.056179
meteorolog,0.012877
method,0.005438
michael,0.008081
midtwelfth,0.024090
miller,0.024519
modern,0.031605
moerbek,0.021665
moral,0.008785
mose,0.014330
motion,0.008861
mountain,0.047240
mover,0.017788
mumford,0.018442
muslim,0.029463
natur,0.018752
nearli,0.007648
neoplaton,0.030563
neoplatonist,0.071350
new,0.003922
newli,0.018478
nietzsch,0.014838
nietzschethat,0.026576
ninth,0.013746
nisi,0.024543
non,0.012656
nonaristotelian,0.019725
nonetheless,0.010984
note,0.015189
number,0.004393
object,0.006003
onc,0.006561
onli,0.012127
openli,0.013300
oppos,0.007338
opposit,0.007225
organon,0.036885
origin,0.019466
overvalu,0.016664
paraphras,0.017253
pari,0.019206
particip,0.013855
particular,0.010993
pave,0.012420
penalti,0.013136
percept,0.009347
perfect,0.009811
perficitur,0.026576
period,0.005217
peripatet,0.072389
peripatetic,0.025077
permeat,0.015804
permit,0.008944
perplex,0.018097
person,0.005746
philosoph,0.092496
philosophi,0.216822
philosophia,0.016631
philosophiarum,0.026576
phronesi,0.020868
physic,0.017239
place,0.009932
plato,0.069409
platon,0.014016
platoni,0.023353
point,0.005093
polit,0.044093
popul,0.006248
popular,0.006282
porphyri,0.018632
portion,0.008824
posterior,0.031759
postmodernist,0.034506
practic,0.026938
preced,0.009540
predic,0.055591
premis,0.011812
premiss,0.021665
present,0.005035
preserv,0.008487
prime,0.008090
probabl,0.007225
produc,0.020958
product,0.005248
progress,0.006863
prohibit,0.010275
project,0.012461
promin,0.007896
proof,0.010267
propon,0.010552
public,0.015214
publica,0.041202
pupil,0.013136
quod,0.020601
rd,0.009017
read,0.016640
realism,0.012459
recent,0.011060
recept,0.012639
rediscoveri,0.015588
regard,0.006127
reign,0.022086
reject,0.047238
renaiss,0.010560
republican,0.022816
repudi,0.015730
research,0.005181
respons,0.011078
restrict,0.007401
result,0.004448
retreat,0.024670
reveal,0.016801
review,0.007155
revis,0.028631
reviv,0.030448
revolutionari,0.020327
rise,0.006426
rival,0.021104
roman,0.017133
rosalind,0.033743
rsttilinzm,0.026576
rule,0.011659
russel,0.033582
said,0.013123
saw,0.007815
scholar,0.064884
scholast,0.013833
school,0.058479
scia,0.025077
scienc,0.010742
scientia,0.018504
scientif,0.006426
scot,0.016598
sea,0.008148
search,0.008174
second,0.010483
secret,0.009853
sens,0.006571
sever,0.004633
sevil,0.033815
sinc,0.004444
social,0.022617
societi,0.005568
son,0.008510
sought,0.017317
sourc,0.016772
spain,0.009445
special,0.005558
spent,0.019648
sphere,0.019893
state,0.019445
stephen,0.009873
strato,0.021855
strict,0.010328
studi,0.004697
subject,0.005654
substanti,0.008251
suffici,0.008217
sufism,0.018208
suspect,0.011877
syllog,0.069684
synthesi,0.009923
syriac,0.016836
systemat,0.016983
teach,0.008707
teacher,0.010185
teeth,0.013990
teleolog,0.029676
tempor,0.023425
text,0.007667
th,0.015289
theolog,0.032504
theophrastu,0.015635
theoret,0.029151
theori,0.046888
theorist,0.020718
therefor,0.024165
thi,0.039894
thoma,0.031802
thomism,0.019052
thoroughli,0.028170
thought,0.037795
thousand,0.007838
thu,0.005195
time,0.035393
titl,0.007978
total,0.012403
tr,0.015804
tradit,0.067163
traditionsinclud,0.026576
translat,0.130082
transmit,0.021780
travel,0.007848
treatis,0.010722
trendelenburg,0.046707
truth,0.018913
unaristotelian,0.025077
understood,0.008341
undertaken,0.011453
undertook,0.013859
unfortun,0.012814
unimport,0.016766
univers,0.004603
use,0.010895
venic,0.014565
veri,0.010151
view,0.017090
viewpoint,0.023400
virtu,0.067572
virtuou,0.031364
wa,0.102042
wasnt,0.014733
wast,0.010661
way,0.004752
welcom,0.012267
werent,0.018043
west,0.007631
western,0.053487
wheel,0.013386
wholli,0.012933
wide,0.011449
william,0.007122
wisdom,0.011768
wish,0.009827
wolff,0.032127
word,0.006169
work,0.095862
world,0.018024
write,0.041697
wrote,0.022718
year,0.008971
