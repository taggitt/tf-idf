abhidharma,0.010526
abov,0.003451
abstract,0.014231
academ,0.044417
academi,0.010386
academia,0.006952
accept,0.010153
access,0.003730
accid,0.006499
accord,0.005391
accordingli,0.006067
achenbach,0.011466
acquaint,0.007890
act,0.003081
action,0.019874
activ,0.021520
actual,0.003450
adam,0.005471
address,0.008119
adher,0.005453
admit,0.005685
adopt,0.003720
advaita,0.009273
aesthet,0.058845
affect,0.010568
african,0.066152
africana,0.010336
africanamerican,0.017293
afterlif,0.008246
age,0.006752
ahimsa,0.010088
aid,0.008088
aim,0.007699
ajivika,0.011561
alghazali,0.009716
algonquian,0.010863
align,0.005824
alkindi,0.008947
allegori,0.008529
allow,0.002680
amawtakuna,0.012462
america,0.003778
american,0.027490
amo,0.008529
analysi,0.006499
analyt,0.009830
analyz,0.027189
anatta,0.010292
ancient,0.050777
ancilla,0.011770
andean,0.008740
andit,0.012462
ani,0.011879
anicca,0.010927
anim,0.007546
anoth,0.010371
anselm,0.008910
antichristian,0.010336
antiqu,0.011157
antir,0.009061
anton,0.007816
anyth,0.005327
appear,0.009367
appli,0.005764
applic,0.006439
appoint,0.004431
appreci,0.005982
approach,0.015513
aquina,0.007584
arab,0.004979
arabia,0.006594
area,0.002677
arendt,0.010207
aret,0.010685
argu,0.021518
argument,0.022646
aris,0.004398
aristotelian,0.013988
aristotl,0.016672
aros,0.010707
arriv,0.004339
art,0.032994
arthashastra,0.009367
articl,0.006408
artist,0.016973
asia,0.013886
asian,0.030815
ask,0.013369
aspect,0.007086
assault,0.006911
associ,0.010534
assumpt,0.004593
astronomi,0.024211
atman,0.009716
atom,0.004714
attribut,0.004190
audienc,0.012170
auditor,0.009122
augustin,0.007239
aurobindo,0.010993
author,0.009106
avail,0.003295
averro,0.008773
avicenna,0.007607
awaken,0.007631
award,0.004752
away,0.008213
axiolog,0.009942
ayn,0.018734
aztec,0.042302
b,0.014283
babylonia,0.008947
babylonian,0.007328
bacon,0.007227
bad,0.005530
balanc,0.008533
bantu,0.009102
base,0.009396
basic,0.006540
baudrillard,0.020415
bc,0.033665
bce,0.023861
beauti,0.030882
beauvoir,0.010630
becam,0.023770
becaus,0.007341
becom,0.013011
began,0.016229
begin,0.012678
behavior,0.003813
belief,0.030460
berggruen,0.012148
berkeley,0.006348
best,0.014331
better,0.003665
bhattacharya,0.010801
bia,0.006198
bibliographi,0.004727
bimal,0.011466
biolog,0.007710
biomed,0.007137
bird,0.005549
birth,0.009579
black,0.004387
bodi,0.016309
boethiu,0.009657
book,0.027805
boundari,0.004538
brahman,0.017173
brain,0.005201
branch,0.031112
breadth,0.007721
brewer,0.009492
bring,0.008082
british,0.003629
broad,0.013098
broader,0.005420
broadli,0.005441
bryer,0.012012
buddha,0.007623
buddhism,0.052956
buddhist,0.091537
bullshit,0.011886
burma,0.008079
busi,0.007328
butterfli,0.007976
c,0.031110
ca,0.005979
cambodian,0.009657
came,0.003559
canadian,0.005526
candid,0.005014
canon,0.017734
carli,0.011662
case,0.002722
categor,0.005534
categori,0.012462
causat,0.015706
cave,0.006546
ce,0.005965
center,0.003472
central,0.006103
centuri,0.053118
certain,0.008939
chain,0.009727
challeng,0.007778
chalmer,0.008910
champion,0.006758
chanakya,0.009466
chandra,0.009061
chang,0.004909
chanzen,0.011466
character,0.012388
characterist,0.007505
charl,0.004169
chemistri,0.004771
children,0.004477
china,0.012548
chines,0.032759
christian,0.016877
church,0.004919
cicero,0.008111
cinema,0.007309
circl,0.005246
citat,0.006484
citizen,0.004455
civil,0.007469
ck,0.019257
claim,0.009914
clan,0.007137
class,0.003611
classic,0.007194
classifi,0.013212
clausewitz,0.010292
close,0.008739
codic,0.010050
cofound,0.007480
cognit,0.005161
coherent,0.010742
coin,0.004508
colin,0.007494
colleg,0.004726
coloni,0.004502
come,0.006228
comedian,0.020254
common,0.007898
commonli,0.003616
commun,0.008541
compani,0.003438
compet,0.004489
complementari,0.006352
complet,0.003007
complex,0.006372
comprehens,0.004796
compris,0.004454
comput,0.010824
conceiv,0.005717
concept,0.024310
conceptu,0.005241
concern,0.018342
conclud,0.004453
conclus,0.019407
concret,0.005965
conduct,0.003732
confucian,0.015836
confuciu,0.008773
connect,0.014340
conquest,0.011734
consequ,0.003702
consequenti,0.008601
consid,0.012697
consist,0.002914
constantli,0.006070
constru,0.008356
construct,0.003372
contact,0.004569
contain,0.003123
contemporan,0.008111
contemporari,0.012778
content,0.008593
continent,0.011598
continu,0.007915
contrast,0.007356
contribut,0.015980
convers,0.004492
correspond,0.003953
cosmic,0.006661
cosmogoni,0.010050
cosmolog,0.031831
counsel,0.007009
countri,0.005731
court,0.008260
cover,0.007170
creation,0.003963
crescent,0.016244
critic,0.025624
critiqu,0.005532
crvka,0.010476
cubist,0.011377
cultur,0.036016
current,0.008363
curti,0.008631
custom,0.004403
cynic,0.008875
cyren,0.009716
daoism,0.009978
darana,0.011292
darklight,0.012462
date,0.003639
deal,0.010523
debat,0.024324
decad,0.003958
decis,0.003671
deduct,0.012600
deep,0.004847
deeper,0.006507
defin,0.005976
definit,0.003443
degre,0.003566
demon,0.007704
deontolog,0.009545
depart,0.003818
depend,0.002873
descart,0.006814
descartess,0.010863
descend,0.005270
describ,0.002680
develop,0.065334
dewey,0.008079
dharma,0.008381
dialogu,0.006079
diaspora,0.007890
die,0.008344
differ,0.006925
dignga,0.010630
dimens,0.005010
ding,0.009978
direct,0.008674
directli,0.003498
directori,0.007309
disappear,0.011072
discern,0.007155
discipl,0.007277
disciplin,0.052162
discours,0.011925
discov,0.003834
discuss,0.021318
disput,0.008760
distinct,0.013415
distort,0.006148
divid,0.010359
divis,0.007548
dmoz,0.005767
doctor,0.005196
document,0.004204
dogen,0.010927
domin,0.025083
doubt,0.005619
dream,0.018860
drei,0.012148
drew,0.006001
dual,0.006139
dualism,0.015153
dualist,0.008394
dukkha,0.020584
dure,0.017546
dynasti,0.028311
earli,0.026221
earliest,0.008764
easier,0.005613
east,0.028215
eastern,0.004351
ecolog,0.004940
econom,0.008607
educ,0.036739
effect,0.007640
effort,0.007261
egypt,0.005536
egyptian,0.005908
element,0.006668
elit,0.005821
embrac,0.005914
emot,0.005385
emphasi,0.009510
empir,0.021948
empiric,0.007097
employ,0.003477
empti,0.006250
encompass,0.019632
encyclopedia,0.009030
end,0.014332
energi,0.007340
engag,0.012454
engin,0.003778
english,0.003651
enjoy,0.004991
enlighten,0.017173
enter,0.003832
entertain,0.006446
entir,0.003400
entireti,0.007494
entiti,0.008896
epicurean,0.008910
epistem,0.008331
epistemolog,0.087230
epistemologist,0.019683
era,0.028995
especi,0.006088
essay,0.005080
essenc,0.005922
essenti,0.007288
establish,0.005595
ethic,0.091677
ethicsappli,0.012462
ethiopian,0.008258
ethnophilosophi,0.012297
etho,0.008756
eudaimonia,0.010014
europ,0.003554
european,0.009796
event,0.003411
eventu,0.003726
evid,0.010725
evil,0.025508
examin,0.008206
exampl,0.021131
exclus,0.008957
exemplifi,0.006678
exercis,0.013513
exhaust,0.006130
exist,0.020856
existenti,0.035517
experi,0.019343
experiment,0.004379
explain,0.003529
explod,0.007712
explor,0.039945
express,0.003396
extens,0.003540
extern,0.002049
extrem,0.003760
f,0.003861
facebook,0.008211
fact,0.003328
faith,0.005241
fall,0.007157
falsafa,0.036892
fame,0.006901
famili,0.003630
familiar,0.005546
famou,0.004331
fantasi,0.007423
far,0.003554
featur,0.007091
femal,0.010527
femin,0.015651
feminist,0.013743
fertil,0.010990
feynman,0.007696
field,0.028629
figur,0.011647
film,0.031902
filmmak,0.027245
fiorina,0.012297
fix,0.004161
fl,0.007966
flourish,0.017309
focu,0.007692
focus,0.007298
follow,0.006727
forc,0.005619
foremost,0.007322
forev,0.007472
form,0.014999
formal,0.010523
formul,0.004489
foucault,0.008111
foundat,0.018278
foundation,0.009942
fourth,0.004891
framework,0.004016
frankfurt,0.007746
free,0.003202
fundament,0.017949
furthermor,0.009608
game,0.004810
gandhi,0.017579
gautama,0.009102
gender,0.036059
genderproportion,0.012462
gener,0.026187
geniu,0.007631
genr,0.006619
geonim,0.011770
gerd,0.009686
german,0.007918
germani,0.008442
gervai,0.011886
gitch,0.012012
given,0.008435
gnostic,0.008724
goal,0.003856
god,0.014719
golden,0.011361
good,0.021817
govern,0.005331
gradual,0.004506
graduat,0.005253
great,0.003159
grecoroman,0.032108
greec,0.005299
greek,0.043667
grew,0.004684
ground,0.004079
group,0.007586
growth,0.003548
gupta,0.008947
ha,0.044863
hadot,0.060060
han,0.005602
hannah,0.009251
happi,0.006198
harass,0.015247
harmoni,0.006538
harri,0.005895
haskalah,0.010993
healthi,0.006120
hegel,0.022966
held,0.003314
help,0.003069
henc,0.008748
hermeneut,0.008557
heterodox,0.016446
hew,0.010927
hi,0.007540
higher,0.003332
highereduc,0.011662
highli,0.014762
himalayan,0.010050
hindu,0.038996
hinduism,0.013481
hipparchia,0.011886
hiriyanna,0.012012
histor,0.015745
histori,0.032689
historian,0.004723
historic,0.009545
hobb,0.015742
hold,0.013506
home,0.004003
hope,0.004773
hopeless,0.009416
howev,0.009100
hu,0.008586
human,0.037812
humanist,0.006809
hume,0.007296
hundr,0.004259
ibn,0.024411
icon,0.007546
idea,0.025171
ideal,0.013684
ident,0.016056
identifi,0.006777
ignor,0.004862
ii,0.007337
illuminationist,0.011377
imag,0.004316
imbal,0.007271
immateri,0.008407
impact,0.003841
imparti,0.008543
imperi,0.005461
impli,0.004358
implic,0.014653
import,0.019127
impuls,0.014370
inca,0.017262
includ,0.077842
incorpor,0.003849
increas,0.002695
increasingli,0.008265
independ,0.005758
india,0.017167
indian,0.041765
indiana,0.007081
indic,0.006897
indigen,0.022357
individu,0.008674
indonesian,0.008132
industri,0.003199
infer,0.010774
infinit,0.005489
influenc,0.055578
influenti,0.031560
inform,0.005574
innov,0.009711
inquiri,0.027958
insid,0.004701
insist,0.005719
institut,0.002966
instruct,0.005277
intellectu,0.033665
intend,0.013019
interact,0.003497
interdepend,0.007036
interlocutor,0.010207
intern,0.002456
internet,0.004598
interpret,0.014960
interv,0.005906
introduct,0.014865
intuit,0.005998
invent,0.004561
investig,0.023569
involv,0.005435
ipod,0.010927
iran,0.012718
iranian,0.014139
iranianislam,0.012462
irrelev,0.006962
irrespect,0.008143
islam,0.042000
issu,0.009024
jain,0.017417
jainism,0.008501
jame,0.003953
japanes,0.026713
jayarama,0.012462
jennif,0.008487
jewish,0.029057
john,0.006377
joseon,0.009628
journal,0.025239
jr,0.005954
js,0.009367
judaic,0.010526
judaism,0.013410
judeochristian,0.009185
judg,0.015530
judgment,0.005940
june,0.008168
jurisprud,0.007862
just,0.009806
justic,0.023688
justif,0.019650
justifi,0.022550
kalam,0.010381
kama,0.010685
kant,0.013563
kantian,0.008875
karl,0.005272
karma,0.008420
key,0.021403
khaldun,0.017613
khmer,0.009143
kierkegaard,0.008646
king,0.004464
know,0.008494
knowhow,0.009492
knowledg,0.081092
known,0.004684
korean,0.013014
krishna,0.017646
kyoto,0.008646
la,0.004678
langer,0.009600
languag,0.010568
lanka,0.007480
larg,0.004940
larri,0.007671
later,0.027873
latin,0.004405
law,0.019764
learn,0.003937
leav,0.003969
lectur,0.010192
led,0.006049
left,0.003736
legal,0.007734
leibniz,0.013988
li,0.007264
life,0.018076
light,0.003863
like,0.007510
limit,0.002862
linguist,0.016528
link,0.004075
list,0.013030
lit,0.023423
liter,0.010778
literari,0.011755
literatur,0.020936
live,0.024242
lock,0.012140
logic,0.081647
logo,0.014592
long,0.003043
lose,0.004929
loui,0.010576
love,0.016691
luther,0.008155
m,0.006703
machiavelli,0.008789
magazin,0.010851
mahadeva,0.011770
mahatma,0.010249
mahayana,0.008601
maimonid,0.008756
main,0.005933
mainli,0.003890
major,0.027349
make,0.009532
male,0.005256
malefemal,0.009716
malest,0.012297
malesth,0.011886
malick,0.012148
mani,0.023300
manicha,0.010088
manit,0.011561
mao,0.008572
map,0.004287
maroneia,0.011886
martin,0.009921
marx,0.013100
marxism,0.007328
marxist,0.012725
masintin,0.012462
materi,0.003284
math,0.007264
mathemat,0.058946
matil,0.011561
matrix,0.011765
matter,0.007050
maxim,0.005329
mazdak,0.011212
mcginn,0.011212
mean,0.010323
meant,0.009613
meanwhil,0.005666
media,0.004298
medicin,0.013404
mediev,0.029990
medit,0.007120
medium,0.005342
meirokusha,0.012297
memori,0.004874
men,0.004345
mendelssohn,0.010742
mental,0.010142
merg,0.005644
mesoamerica,0.009273
messian,0.009185
metaeth,0.009518
metainvestig,0.012462
metaphilosophi,0.010801
metaphys,0.107926
metaphysika,0.012462
method,0.029024
michael,0.004312
michel,0.012848
middl,0.011591
militari,0.003791
milliondollar,0.011886
mind,0.026516
ming,0.008027
minist,0.004228
misogyni,0.010993
misrepresent,0.009492
mm,0.006857
modal,0.007437
model,0.002919
moder,0.005368
modern,0.053408
modernist,0.008394
modif,0.005570
modu,0.009518
moksha,0.009600
monism,0.008246
monoth,0.008460
monotheist,0.008318
moral,0.028130
mortal,0.012527
mose,0.007647
mostli,0.007996
movement,0.019389
movi,0.012818
multipl,0.003604
multipli,0.006041
music,0.015451
musician,0.008177
muslim,0.010482
mutual,0.004943
mysteri,0.006185
myth,0.005984
nagarjuna,0.010088
nahda,0.012297
nahua,0.010801
napoleon,0.006391
nation,0.005193
nativ,0.019375
natur,0.072553
navyanyya,0.011886
near,0.003853
necessari,0.003734
need,0.002817
neg,0.008127
neoconfucian,0.009747
neoplaton,0.008155
neovedanta,0.010801
new,0.012559
newton,0.011833
ngritud,0.012148
nietzsch,0.007918
nihil,0.008929
nobl,0.006093
nonacadem,0.019749
nonclass,0.009122
nonhuman,0.014382
nonpeerreview,0.012297
nonphilosoph,0.011136
nonprofession,0.011212
norm,0.005435
note,0.002701
notself,0.011212
novelist,0.008079
nstika,0.011136
number,0.009377
numer,0.010415
nyaya,0.009466
object,0.035239
oblig,0.005175
observ,0.003092
obsolet,0.007309
occup,0.009766
occupi,0.004698
occur,0.005954
oldest,0.005069
ometeotl,0.012297
onli,0.006471
onlin,0.004101
ontolog,0.012840
open,0.003208
oppos,0.003916
opposit,0.003856
optic,0.005637
order,0.002582
orenda,0.012462
organ,0.002556
origin,0.012985
ornitholog,0.009492
orthodox,0.025242
otherwis,0.004457
outsid,0.010725
overlap,0.005208
overview,0.004695
overwhelm,0.006283
overwhelmingli,0.007599
panafrican,0.010336
pancanana,0.012462
pantheism,0.008789
pariti,0.006745
particular,0.008799
particularand,0.012462
paul,0.004073
peerreview,0.007494
peopl,0.010335
percent,0.004706
percept,0.014964
perceptu,0.007494
perform,0.003294
period,0.030629
peripatet,0.009657
person,0.006132
peter,0.004273
phenomena,0.004474
phenomenolog,0.028346
phenomenon,0.004826
philosoph,0.341408
philosopherscholar,0.012462
philosophi,0.722163
philosophia,0.017750
philosophischen,0.012012
philpap,0.008543
physi,0.018686
physic,0.024531
physiologoi,0.011886
pierr,0.006032
plant,0.004068
plastic,0.006300
plato,0.018520
platon,0.014960
play,0.003383
playwright,0.009102
point,0.005436
polar,0.005741
polit,0.058825
ponen,0.010476
pope,0.006590
popul,0.003334
populac,0.007416
popular,0.023469
pose,0.005708
posit,0.005554
positiv,0.007631
possess,0.004211
possibl,0.010963
postcoloni,0.008079
potenti,0.003479
power,0.002750
practic,0.034501
pragmat,0.013100
praktisch,0.012148
prama,0.011662
pramana,0.011212
praxi,0.009600
precis,0.004426
preislam,0.009122
premis,0.031519
present,0.010747
preserv,0.009058
presidenti,0.005495
presocrat,0.008200
prestigi,0.007639
preuv,0.012297
price,0.003978
primari,0.007094
primarili,0.007346
principl,0.019354
priori,0.014406
prize,0.005011
probabl,0.003856
problem,0.014369
process,0.002540
produc,0.005592
profess,0.011231
profession,0.060961
professionalis,0.010685
professor,0.028816
professorsunivers,0.012462
profound,0.018390
program,0.003380
progress,0.021975
project,0.003325
promin,0.004213
promot,0.019166
properti,0.013014
propon,0.005631
propos,0.003315
proposit,0.018529
prove,0.008294
proverb,0.018593
provid,0.002384
prussia,0.007957
prussian,0.008132
psycholog,0.027483
public,0.013531
publish,0.008948
punatamakara,0.012462
pupil,0.007009
purpos,0.003495
pursu,0.004887
pursuer,0.011770
pursuit,0.006058
puzzl,0.007155
pythagora,0.016244
quasipsychotherapeut,0.012297
question,0.040495
race,0.005279
radhakrishnan,0.010685
raghunatha,0.011886
rand,0.023842
rangaku,0.011561
rare,0.008844
ration,0.018524
read,0.005920
reader,0.011623
real,0.007074
realism,0.013297
realiti,0.023179
realli,0.005483
reason,0.039763
recent,0.014756
recogniz,0.007928
recur,0.007277
red,0.004723
refer,0.008521
reflect,0.018312
reform,0.016675
region,0.009155
regress,0.006722
rein,0.008616
relat,0.013787
relationship,0.016265
relev,0.004457
reli,0.004106
religi,0.013060
religion,0.039066
religiophilosoph,0.012148
remain,0.011274
ren,0.013357
renaiss,0.011270
repercuss,0.009251
report,0.003275
requisit,0.008282
research,0.008294
resembl,0.005376
respect,0.003241
respond,0.004596
respons,0.002956
result,0.009496
resurg,0.007290
retain,0.008963
retali,0.007480
review,0.007637
revolv,0.020841
richard,0.004210
ricki,0.011770
right,0.015291
rise,0.027436
road,0.004950
robust,0.012346
roger,0.005715
role,0.002985
roman,0.013714
root,0.004305
rousseau,0.007966
rule,0.003111
rushd,0.010476
s,0.012085
sacr,0.006526
said,0.003501
samkhya,0.009874
samsara,0.009391
sandel,0.010801
sanger,0.009207
sanskrit,0.007058
saul,0.016446
saw,0.037535
say,0.003692
scheme,0.005129
scholast,0.007382
scholastic,0.008166
school,0.062413
scienc,0.103190
scientif,0.017147
scientist,0.011691
scriptur,0.006857
search,0.004362
sebayt,0.012297
second,0.002797
secular,0.005898
seek,0.003824
seen,0.013376
seinfeld,0.011136
selfknowledg,0.010127
seller,0.006809
seneca,0.009185
sens,0.014027
sensori,0.013214
sensoriemot,0.012297
sentiment,0.006623
separ,0.006227
seri,0.013580
seriou,0.009548
set,0.013200
sever,0.002472
sexual,0.016800
shaman,0.008501
shape,0.004062
share,0.006642
sheffield,0.009081
shih,0.010742
shili,0.012012
shinto,0.018639
shunyata,0.011292
sich,0.010863
signific,0.003027
silk,0.007081
similar,0.008496
simon,0.005698
simplic,0.006862
simpson,0.007947
simul,0.005153
simulacra,0.010801
sinc,0.007115
siouan,0.012462
siromani,0.011886
situat,0.003651
sixth,0.006297
skeptic,0.018741
skinner,0.008166
slipperi,0.010166
smith,0.005025
social,0.024138
societi,0.017830
sociolog,0.016859
socrat,0.051256
someon,0.005540
someth,0.022801
sometim,0.009263
soteriolog,0.010014
sought,0.013862
soul,0.006124
sound,0.009988
sourc,0.017901
south,0.003714
southeast,0.011613
space,0.003656
special,0.017798
specialist,0.011770
specif,0.011351
specul,0.004934
sphere,0.005308
spinoza,0.008111
spiritu,0.027325
split,0.009749
sport,0.011266
spread,0.012577
sri,0.006901
srivijaya,0.010336
st,0.007647
standard,0.009584
stanford,0.005509
star,0.005260
start,0.003002
state,0.016602
statecraft,0.009908
statement,0.017417
stephen,0.005268
steve,0.007290
stika,0.011212
stoicism,0.009102
stori,0.004808
strategi,0.004418
strictli,0.005593
strive,0.006800
strong,0.006892
structur,0.002771
stuart,0.006578
student,0.024222
studi,0.050133
subbranch,0.018204
subcontin,0.014618
subdivis,0.006967
subfield,0.024423
subject,0.012069
suffer,0.004329
sufi,0.008789
sun,0.005261
support,0.002820
suprem,0.004857
surround,0.004343
surviv,0.008181
susan,0.007252
susann,0.010577
sutra,0.008381
swell,0.007834
symbol,0.004306
syncret,0.008460
synthesi,0.005295
systemat,0.004531
taken,0.003473
talbot,0.009747
talmud,0.009391
tanakh,0.009908
tanka,0.011212
tantra,0.009809
tao,0.008515
tast,0.013314
taught,0.005288
taylor,0.005968
taymiyyah,0.010742
teach,0.013939
teacher,0.010871
technic,0.008436
technolog,0.003419
televis,0.010692
teotl,0.012012
term,0.017930
terrenc,0.009841
testimoni,0.007631
text,0.032733
th,0.048955
thale,0.007908
thatstil,0.012297
thcenturi,0.005469
theme,0.010506
theolog,0.017345
theologia,0.010476
theoret,0.007778
theori,0.075065
theosophi,0.009778
therapeut,0.006999
therapi,0.006041
theravada,0.008984
therefor,0.006447
thi,0.026611
thing,0.011322
thinker,0.074541
thoma,0.008485
thomism,0.010166
thought,0.060508
thth,0.007918
thu,0.002772
tibet,0.016921
tibetan,0.008270
tie,0.004626
time,0.012591
timelin,0.005583
timesnot,0.012462
titl,0.004257
tlamatiliztli,0.012462
tlamatini,0.012462
today,0.017037
togeth,0.006541
tolstoy,0.009492
tool,0.003797
topic,0.041396
total,0.003309
tradit,0.071683
transcend,0.006709
transcendent,0.022730
transcultur,0.010742
transform,0.011647
transienc,0.010993
translat,0.004083
transmiss,0.005859
transregion,0.012297
trend,0.004530
true,0.011521
truth,0.015139
turn,0.003279
tv,0.006558
type,0.002851
typic,0.003182
typifi,0.008677
tzu,0.009367
ubuntu,0.010927
ujamaa,0.011377
uk,0.004347
ultim,0.008204
unavoid,0.008529
uncommon,0.007271
und,0.006978
undergradu,0.006317
underli,0.004504
underpin,0.013833
understand,0.012878
unifi,0.004964
uniqu,0.016177
unit,0.004784
univers,0.022109
unjust,0.008368
unlik,0.003950
unorthodox,0.018593
unproven,0.008984
unsolv,0.007501
upanishad,0.008910
use,0.015504
usher,0.007264
utilitarian,0.015033
utopia,0.008356
vaisheshika,0.009809
valid,0.004529
valu,0.036480
vari,0.003496
varieti,0.010684
variou,0.024301
vast,0.004762
vasubandhu,0.010685
vat,0.008515
veda,0.008037
vedanta,0.017681
vedic,0.008111
veri,0.008126
versu,0.005536
vice,0.005491
vietnames,0.007937
view,0.018240
vijnaptimatra,0.011886
virtu,0.012019
vision,0.005137
vivekananda,0.010526
vocabulari,0.007092
volum,0.008125
vortrg,0.011662
wa,0.062233
wakan,0.012012
wang,0.007679
war,0.010108
way,0.012680
weaver,0.008692
weber,0.007252
wellliv,0.012012
western,0.071357
white,0.009006
whiteley,0.011561
whitest,0.012297
wide,0.006109
wider,0.005400
wikipedia,0.007623
wikipediaget,0.012462
wilhelm,0.005970
william,0.003800
wisdom,0.050241
women,0.036112
work,0.039529
world,0.024047
worldview,0.021832
write,0.003708
writer,0.004868
written,0.011265
wrong,0.016736
x,0.008399
xiong,0.010927
yacob,0.012012
yanantin,0.012148
yang,0.007825
yangm,0.011063
yashovijaya,0.012462
year,0.004787
yield,0.004633
yin,0.008646
yoga,0.008661
york,0.003653
zedong,0.008965
zera,0.011770
zhou,0.008433
zhuangzi,0.019683
zoroast,0.010381
zur,0.008474
zurvan,0.011136
