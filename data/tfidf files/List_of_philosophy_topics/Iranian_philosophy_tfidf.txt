abd,0.015822
abdolkarim,0.025224
abdolrahman,0.026731
abdulbah,0.021285
access,0.007030
accid,0.012250
accord,0.015243
accus,0.010190
addin,0.018807
adurbad,0.024686
adurfarnbag,0.025882
aesthet,0.024647
afdal,0.026731
affect,0.006640
africa,0.008252
afterward,0.011192
age,0.006363
agha,0.020989
ahmad,0.014699
aidin,0.026731
albertu,0.017701
aldin,0.163263
alfarabi,0.034626
ali,0.062984
aljurjani,0.023837
allama,0.022897
allamah,0.024231
allot,0.015434
almuqaffa,0.023179
alqahir,0.024231
alrazi,0.018371
anacharsi,0.024686
analyt,0.009264
analyz,0.008541
ancient,0.080984
answer,0.009047
anyth,0.020084
appar,0.008674
appear,0.023542
appropri,0.008145
aquina,0.014295
arab,0.018770
ardakani,0.026731
aristotelian,0.013182
articl,0.012078
ashraf,0.021285
ashtiyani,0.025882
asia,0.008724
associ,0.004963
astral,0.020248
atmospher,0.009954
attar,0.045795
augustin,0.013645
author,0.005721
auvergn,0.020359
avesta,0.020595
avestan,0.020595
avicenn,0.090564
avicenna,0.071697
azar,0.020595
backgammon,0.020359
bad,0.010423
bagheri,0.025224
bah,0.065657
bakhtshooa,0.025882
balkhi,0.026731
base,0.008855
basi,0.006329
bc,0.018129
bce,0.011243
becam,0.011201
becom,0.004904
befor,0.005232
belief,0.008201
believ,0.012674
belong,0.017314
bodi,0.006148
book,0.023292
borzouy,0.051765
bought,0.011707
branch,0.006515
burzo,0.024231
case,0.005130
cathol,0.009907
ce,0.011243
censur,0.017566
central,0.005752
centuri,0.042155
challeng,0.007330
chancellor,0.014553
chang,0.009252
character,0.015566
china,0.007884
christian,0.031811
chronolog,0.024471
claim,0.018687
classic,0.013560
cnidu,0.020140
colot,0.023489
combin,0.011704
comment,0.009830
commun,0.010733
compar,0.006070
concentr,0.008031
concept,0.011455
concern,0.005762
concupisc,0.024686
conduct,0.007034
consid,0.004786
consider,0.006897
contemporari,0.016056
continent,0.010930
continu,0.014919
contradict,0.010963
contrast,0.006933
convert,0.008366
cosmic,0.025112
cosmologytheolog,0.026731
countri,0.005401
cours,0.014306
court,0.007784
creat,0.010040
creation,0.007470
creator,0.012301
cultur,0.012342
current,0.005254
d,0.006644
daftar,0.026731
damad,0.025224
dark,0.010613
dashtaki,0.026731
date,0.006859
davari,0.026731
davudi,0.026731
deal,0.006612
debat,0.007641
deed,0.014237
definit,0.006489
deism,0.017155
democritu,0.015565
denounc,0.013294
depart,0.007196
describ,0.015155
desir,0.007724
develop,0.016419
dictionari,0.029852
did,0.018278
die,0.007864
differ,0.004351
discours,0.011238
distinct,0.006321
divers,0.007894
divid,0.006508
doctrin,0.019685
domin,0.006754
dualism,0.014281
dualist,0.015822
dure,0.009449
earli,0.004942
east,0.007597
eastern,0.008201
educ,0.012590
eighteenth,0.012629
elder,0.024286
element,0.006284
emedan,0.025882
emperor,0.010898
empir,0.006895
encyclopedia,0.008510
endeavor,0.013027
english,0.006882
enlighten,0.010789
enter,0.007224
er,0.015610
era,0.023422
err,0.019746
espous,0.014031
essenti,0.006869
ethic,0.018189
eudoxu,0.036628
europ,0.013400
event,0.006430
eventu,0.007023
evil,0.024039
exampl,0.004425
exist,0.004913
existenceess,0.026731
existenti,0.013389
expans,0.008337
explain,0.006652
extend,0.006517
extent,0.008130
extern,0.003863
extort,0.016970
faith,0.009878
fakhr,0.041979
falsafeh,0.053463
falsafehth,0.026731
famou,0.008163
far,0.006700
fardid,0.026731
farid,0.019656
farroxzadan,0.025882
fatalist,0.042570
featur,0.006682
femal,0.009921
field,0.010792
figur,0.007318
flute,0.018043
form,0.004038
format,0.007206
foucault,0.015288
foundat,0.006890
founder,0.009261
freemason,0.020140
fromsouth,0.026731
gain,0.006428
gatha,0.021444
gener,0.003796
geniu,0.014384
ghazali,0.021792
god,0.009248
goftarenik,0.026731
golden,0.010706
gondishapuri,0.025882
good,0.041122
great,0.017865
grecopersian,0.021133
greek,0.052377
group,0.009532
gundishapur,0.022404
ha,0.028186
hadi,0.021444
hamedani,0.026731
hand,0.006547
happi,0.011683
hayyan,0.021982
heavenli,0.014961
heidegg,0.031893
hi,0.061590
high,0.005350
hikmat,0.022186
hippo,0.016103
histori,0.017604
howev,0.004288
human,0.005090
huxwadh,0.026731
ian,0.013046
ibn,0.057515
idea,0.023721
illumin,0.024829
illumination,0.024231
illuminationist,0.021444
imam,0.048892
impact,0.007241
implic,0.009206
import,0.013519
inclin,0.013075
includ,0.007336
inconceiv,0.018430
incorpor,0.007255
indoiranian,0.060744
influenc,0.093118
influenti,0.033992
innov,0.018305
insight,0.009801
institut,0.011182
intellectu,0.036259
interact,0.006592
intern,0.004630
invas,0.019270
inventor,0.013075
iqbal,0.019840
iran,0.107876
iranian,0.293164
iranianpersian,0.026731
iranshahri,0.025224
isfahan,0.037352
islam,0.096755
issu,0.005670
jabir,0.019656
jahangir,0.021982
jala,0.025224
jalal,0.020989
jamasp,0.048462
jami,0.018612
journal,0.020388
judaism,0.025276
kalam,0.019568
kashani,0.024686
kateb,0.025224
kavadh,0.024686
kayvan,0.025224
kerdarenik,0.026731
kermani,0.024686
khajeh,0.026731
khatami,0.022897
khayyam,0.018807
khomeini,0.019656
khorasan,0.020248
khosrau,0.021444
khosrow,0.021285
khrad,0.023837
khurasan,0.023179
kind,0.007038
king,0.008415
kluge,0.023179
know,0.016011
knowledg,0.013291
known,0.017659
kripk,0.018371
kshathra,0.026731
lahiji,0.026731
late,0.012232
later,0.010507
latin,0.008303
lead,0.005270
learnt,0.017117
left,0.007041
life,0.005678
light,0.014565
like,0.009436
limit,0.005394
line,0.006322
linger,0.016050
link,0.003841
list,0.004911
literatur,0.007892
littl,0.006760
local,0.011965
lost,0.007676
love,0.010486
magic,0.012316
magician,0.017892
magnu,0.015413
mahmoud,0.036191
main,0.016776
mainli,0.007333
major,0.009372
male,0.009907
man,0.023446
mani,0.023955
manich,0.023837
manicha,0.095078
maragheh,0.022897
mardanfarrux,0.025882
marif,0.026731
materi,0.006189
materialist,0.042843
mazdak,0.211337
mazdayasna,0.048462
mediev,0.009421
menogi,0.023837
mention,0.017103
metaphys,0.022602
middl,0.029131
mir,0.017194
miraclework,0.024231
mirdamad,0.026731
miskawayh,0.023179
mix,0.008496
mixtur,0.011044
mobad,0.025882
modarr,0.025224
modern,0.005298
mohammad,0.031996
mongol,0.013876
monotheist,0.015679
moral,0.008836
movement,0.012181
mowlana,0.026731
mozart,0.019088
muhammad,0.012688
mulla,0.129679
murad,0.020359
mysteri,0.011659
mystic,0.013027
myth,0.011280
namag,0.024686
naqd,0.026731
narrat,0.014683
nasir,0.035311
natur,0.018862
nazar,0.022186
nematollah,0.025224
neoplaton,0.030741
neutral,0.009668
new,0.015782
neyshabor,0.026731
nietzsch,0.014925
night,0.010834
nishapuri,0.025224
nobleman,0.018430
noor,0.021613
north,0.007013
note,0.005092
noth,0.037388
nowaday,0.013367
number,0.004418
o,0.009314
occur,0.005611
offer,0.006568
ohrmazd,0.025882
ohrmazddadan,0.025882
old,0.036542
oldest,0.009555
opera,0.015188
opposit,0.007268
optimist,0.014889
order,0.004867
origin,0.009790
ormuzd,0.025882
orthodox,0.011894
ostan,0.023179
osthan,0.026731
owe,0.010750
oxford,0.024897
pahlavi,0.019239
panchatantra,0.020852
pari,0.009659
particularli,0.012389
passion,0.013263
path,0.008955
paul,0.007678
pendarenik,0.026731
peopl,0.004869
perhap,0.008211
period,0.015745
persia,0.078917
persian,0.210858
philosoph,0.170566
philosophi,0.488816
physician,0.011142
pivot,0.013162
plagiar,0.017748
planet,0.010021
plato,0.023271
platon,0.028197
plini,0.029075
point,0.005123
polar,0.010821
polit,0.011087
popper,0.014475
popular,0.012638
possess,0.007937
possibl,0.005166
postclass,0.017941
postislam,0.023179
prais,0.011843
prefer,0.007756
preislam,0.017194
preordain,0.041442
previou,0.007638
priest,0.011992
primaci,0.015129
primordi,0.014237
principl,0.030400
problem,0.005417
program,0.006371
promot,0.007225
prophet,0.013294
proscrib,0.018095
protest,0.009413
protosoci,0.053463
protosocialist,0.023179
provid,0.008987
psycholog,0.008633
publish,0.039353
pythagora,0.015308
qazi,0.021285
qazwini,0.049372
qom,0.067923
queen,0.010630
question,0.012721
qumi,0.026731
quot,0.010596
qutb,0.019840
qutbaldin,0.026731
rais,0.007470
rajab,0.024686
rank,0.025655
ration,0.026187
razi,0.083410
rd,0.009069
reader,0.010953
realism,0.012532
realiti,0.008737
reason,0.005765
receiv,0.006137
reconcili,0.013443
refer,0.006424
reform,0.007857
regard,0.012326
reign,0.011107
rel,0.005759
releas,0.008028
religi,0.041027
religion,0.040908
remark,0.009852
repres,0.010538
republ,0.007539
research,0.010422
result,0.004474
revolut,0.007957
reza,0.018807
role,0.005627
roman,0.008616
root,0.008114
rumi,0.020037
sabzevari,0.026731
sadr,0.019239
sadra,0.117449
sage,0.012663
said,0.006600
saint,0.010741
sarastro,0.026731
sassanian,0.038798
sassanid,0.016384
saw,0.007861
say,0.013919
scholar,0.016315
scholast,0.013914
school,0.137247
scienc,0.005402
scythian,0.018202
search,0.008222
searl,0.016474
seminari,0.017117
semnani,0.026731
sever,0.018642
shabestari,0.026731
shah,0.014871
shahab,0.065947
sham,0.017843
shape,0.007656
share,0.006259
shiraz,0.040950
shirazi,0.019746
signific,0.005706
similar,0.005338
sina,0.034311
sinc,0.004470
situat,0.006883
social,0.017061
socrat,0.013801
son,0.008559
soroush,0.023489
soul,0.011542
sourc,0.005623
sovereignti,0.010781
spectrum,0.010572
sphere,0.010004
start,0.011318
state,0.011735
stoic,0.015798
strong,0.012992
student,0.007609
studi,0.014173
style,0.009211
subject,0.011374
subtli,0.017843
success,0.005928
successor,0.010042
sufism,0.018314
suhrawardi,0.106425
tabatabai,0.023489
tablighat,0.026731
tabrizi,0.023837
taken,0.006546
tansar,0.023837
teach,0.026273
tehran,0.068321
term,0.012673
text,0.007712
th,0.025631
theologian,0.013222
theori,0.005240
theosophi,0.036860
therefor,0.006076
thi,0.043470
thinker,0.010807
thoma,0.007997
thought,0.050688
time,0.019777
today,0.012845
topic,0.007093
trace,0.008572
tradit,0.039407
tragedi,0.015035
transcend,0.050585
transit,0.007783
translat,0.023089
treat,0.007828
trip,0.012353
truth,0.019023
tusi,0.040496
ultim,0.007732
umar,0.034790
understood,0.008390
undifferenti,0.017479
univers,0.032412
use,0.007305
va,0.015543
vali,0.022641
variant,0.010830
varieti,0.006712
variou,0.005089
view,0.022920
vizier,0.018807
vohu,0.024231
voltair,0.015946
wa,0.076978
way,0.009560
welfar,0.010297
west,0.007676
western,0.033624
whatev,0.011005
wide,0.005758
william,0.007163
wisdom,0.047349
wittgenstein,0.015434
word,0.012411
work,0.013148
worship,0.012115
worthi,0.014098
write,0.020970
writer,0.027526
wrote,0.007616
zaehner,0.021792
zakaria,0.022186
zakariya,0.043226
zarathushtra,0.024231
zarathustra,0.164766
zauberflt,0.023179
zoroast,0.097840
zoroastrian,0.141537
zoroastrianismrel,0.026731
zurvan,0.272867
