abil,0.005272
abl,0.009681
abov,0.001635
absenc,0.002348
absorpt,0.003109
ac,0.003479
academ,0.001913
acceler,0.014741
accent,0.004342
accept,0.001603
accord,0.002554
account,0.003009
accur,0.002176
accuraci,0.005541
achiev,0.009642
acmr,0.005905
acoust,0.003568
act,0.002920
action,0.001569
activ,0.002549
actual,0.001634
actuat,0.079493
adapt,0.008136
add,0.002435
addit,0.003877
adjust,0.007335
advanc,0.012622
advantag,0.009914
aerial,0.003597
affect,0.001669
afterschool,0.011265
age,0.001599
agent,0.002178
agricultur,0.003797
agrobot,0.005905
ai,0.003209
aibo,0.005209
aid,0.003832
aim,0.005472
air,0.015699
aircraft,0.002837
airlin,0.003251
algorithm,0.017927
allianc,0.002389
allow,0.010163
alloy,0.003554
alreadi,0.007444
altern,0.004924
amber,0.004028
anaerob,0.004149
analyz,0.002147
anderson,0.003329
andor,0.002235
andrew,0.002594
android,0.004134
ani,0.009006
anim,0.001788
annual,0.001962
anoth,0.006143
anybot,0.005827
anyth,0.002524
anywher,0.003126
apek,0.015271
appar,0.002180
appear,0.005918
appli,0.006828
applic,0.012205
approach,0.016172
appropri,0.006143
approxim,0.001740
aqua,0.013945
area,0.005075
arena,0.003530
arm,0.006110
armsleg,0.005905
array,0.005575
art,0.001954
articl,0.001518
artifici,0.032358
asimo,0.026565
asimov,0.016994
ask,0.002111
aspect,0.008394
assembl,0.007977
assign,0.002294
associ,0.001247
assum,0.005309
astound,0.009423
astrict,0.005905
atmospher,0.002502
attain,0.002585
attempt,0.002914
author,0.002876
auto,0.003885
autom,0.008575
automat,0.007823
autonom,0.020665
autonomi,0.016016
autopilot,0.005209
avail,0.006246
averag,0.001908
avoid,0.005724
bachelor,0.003326
background,0.002193
balanc,0.020218
balashek,0.005905
ball,0.012618
ballbot,0.005756
ballip,0.005905
base,0.005565
basi,0.001591
basic,0.006198
batteri,0.027684
batterypow,0.005827
baua,0.005905
bayesian,0.003551
becaus,0.004638
becom,0.008631
befor,0.002631
began,0.001538
begin,0.004505
behav,0.002739
behavior,0.010841
benefactor,0.004562
benefit,0.001869
best,0.006790
better,0.015633
bicep,0.005242
biddulph,0.005905
bioinspir,0.004941
biolog,0.003653
biomimet,0.009490
bionic,0.014288
bionickangaroo,0.005827
biped,0.004836
bl,0.004394
board,0.002222
boat,0.003070
bodi,0.007728
bomb,0.005832
bori,0.003853
bot,0.008767
botbal,0.005827
bound,0.002304
branch,0.001638
brother,0.002583
brush,0.004048
brushless,0.005905
build,0.011410
built,0.017282
busi,0.001736
cabl,0.003358
calcul,0.015725
california,0.002362
camera,0.006669
camp,0.008373
cancel,0.002922
capabl,0.008126
capac,0.002138
capuchin,0.004745
car,0.010904
carangiform,0.011810
carbon,0.002514
care,0.001938
career,0.005061
carnegi,0.007451
carpet,0.004405
case,0.005159
cast,0.002620
catch,0.003105
caterpillar,0.018780
caus,0.005507
ceil,0.003823
celebr,0.002658
center,0.001645
centuri,0.002649
certain,0.007060
certif,0.011994
chain,0.002304
challeng,0.003685
chang,0.005815
cheapli,0.004205
chef,0.004798
chemic,0.001964
children,0.002121
china,0.001982
circl,0.002485
circuit,0.002910
circumv,0.003833
cite,0.002157
clapham,0.004964
classic,0.001704
classif,0.004552
clean,0.006040
cleanup,0.004618
climb,0.013966
climber,0.005147
close,0.004141
cloud,0.002950
cnc,0.004836
coat,0.006112
code,0.001996
cognit,0.012230
coin,0.006409
cold,0.002459
collabor,0.009479
collaps,0.002272
collis,0.009506
combat,0.002708
combin,0.007356
combust,0.007130
come,0.010329
command,0.015152
commandlin,0.005313
commerci,0.009883
commiss,0.002054
common,0.006238
commonli,0.001713
commun,0.005396
compact,0.003044
compani,0.001629
compar,0.007630
compet,0.002127
competit,0.016231
complet,0.012825
complex,0.010568
compon,0.015992
compos,0.001957
compress,0.009556
comput,0.032484
concept,0.010079
concern,0.001448
condit,0.001443
conduct,0.003536
confer,0.002031
confin,0.008311
confirm,0.002224
connect,0.005096
connector,0.004877
consist,0.001381
constantli,0.005752
construct,0.015978
consum,0.002054
contain,0.002960
contamin,0.003291
continu,0.006251
contract,0.004139
contribut,0.001514
control,0.035644
convert,0.006310
convey,0.003115
cook,0.002909
cooper,0.003944
copi,0.004975
core,0.010956
correct,0.004155
correspond,0.001873
counterbal,0.004510
countri,0.005431
courier,0.004498
cours,0.001798
coursework,0.004322
cpo,0.004941
cradl,0.004149
crane,0.004149
crazylook,0.005905
creat,0.010097
creativ,0.002666
creator,0.003092
creatur,0.003146
criteria,0.002584
critic,0.001517
cruis,0.003654
curi,0.004111
current,0.009246
curriculum,0.006392
cyberflora,0.005756
cybernet,0.003687
cycl,0.002115
czech,0.003306
d,0.001670
daili,0.002413
danger,0.009695
darpa,0.004427
data,0.008233
date,0.001724
davi,0.002991
day,0.003114
dc,0.005714
deactiv,0.004383
deal,0.003324
deceler,0.004248
decid,0.004021
decis,0.001739
decisionmak,0.002974
decreas,0.002032
defect,0.003045
defenc,0.002511
deform,0.006737
defus,0.004663
degre,0.003379
delft,0.009635
demonstr,0.012983
densiti,0.002490
depart,0.001809
depend,0.001361
depth,0.002709
deriv,0.001568
describ,0.002540
descript,0.001886
design,0.037889
desir,0.001942
desktop,0.004069
despit,0.001749
destroy,0.002157
detect,0.006798
determin,0.001432
develop,0.022703
devic,0.016142
dexter,0.009237
di,0.003211
dialog,0.004069
diamet,0.003371
dickmann,0.005827
dictionari,0.005003
did,0.003063
die,0.001977
differ,0.014220
difficult,0.007198
digest,0.003152
digit,0.004608
dinosaur,0.004069
direct,0.015072
directli,0.001657
dirt,0.008514
dirti,0.007943
disabl,0.003005
disciplin,0.003802
discret,0.002433
discuss,0.001683
dishwash,0.005827
dissip,0.003670
distinct,0.001589
disturb,0.002897
divers,0.001984
diversifi,0.003124
divid,0.001636
dmoz,0.002732
doctor,0.002462
doe,0.002792
domest,0.004276
dr,0.007680
drawback,0.004015
drawn,0.002435
drive,0.008868
driverless,0.005242
dull,0.008283
durat,0.002938
dure,0.001187
dusti,0.004877
duti,0.002455
dynam,0.029018
eap,0.005632
earlystag,0.005147
earth,0.003900
easier,0.002659
educ,0.003165
educationalrel,0.005905
effect,0.003620
effector,0.032980
effici,0.013616
elast,0.018669
elastomer,0.005577
elderli,0.003479
electr,0.034920
electroact,0.005827
electrod,0.007550
electromagnet,0.002785
electron,0.004157
element,0.004739
elsewher,0.002561
embed,0.002889
emot,0.012760
employ,0.006591
employe,0.002603
emul,0.003561
enabl,0.005910
encompass,0.004651
encount,0.002446
end,0.005433
endeffector,0.005827
endur,0.003032
energi,0.015652
engin,0.014322
engineeringbas,0.005827
english,0.003460
enstabretagn,0.005905
ensur,0.002040
entir,0.003222
entomopt,0.005756
entri,0.002402
environ,0.026348
environment,0.001997
epam,0.005478
epson,0.005756
equip,0.006663
ergonom,0.004416
ernst,0.003011
especi,0.002885
essenc,0.002806
essenti,0.005180
essex,0.009125
establish,0.001325
estim,0.003496
etymolog,0.005289
euosha,0.005433
europ,0.001684
european,0.003094
event,0.003233
eventu,0.003531
everi,0.001554
evolut,0.006051
evolutionari,0.007342
evolv,0.002039
exactli,0.004867
exampl,0.020026
excel,0.002687
excess,0.002400
exchang,0.001756
excit,0.003028
exhibit,0.002345
exist,0.002470
expand,0.001757
expandtyp,0.005905
expect,0.005120
expens,0.002218
experiment,0.002075
explain,0.003344
explor,0.005678
explos,0.002753
exposur,0.002760
express,0.012876
extend,0.001638
extens,0.001677
extern,0.000971
extract,0.002391
extrem,0.001782
eye,0.004967
face,0.001864
facial,0.036914
facil,0.004762
factor,0.001575
factori,0.013591
fall,0.011870
fantasi,0.003517
far,0.005053
fashion,0.002614
faster,0.002591
fear,0.002331
feder,0.001977
feedback,0.009050
feel,0.002322
feet,0.003033
festo,0.011513
fiction,0.021197
field,0.008139
fight,0.002331
figur,0.005519
filament,0.003869
film,0.002519
filter,0.003096
final,0.003213
fine,0.002655
finger,0.007217
fingertip,0.015271
firefight,0.004604
firstyear,0.005012
fish,0.009271
fissur,0.004450
fit,0.002261
fix,0.003943
flame,0.003687
flat,0.005682
flexibl,0.008250
flexinol,0.005905
flexur,0.005118
fli,0.016637
flight,0.002721
flipper,0.004964
fll,0.005692
float,0.002941
floor,0.015109
flow,0.001956
fluid,0.005237
flywheel,0.005313
focu,0.001822
focus,0.001729
follow,0.001062
foot,0.005772
forc,0.018640
forev,0.003541
form,0.013200
format,0.001811
formul,0.002127
fourth,0.002317
fourwheel,0.005209
frame,0.002538
freedom,0.002097
freeland,0.005577
frequent,0.003734
friction,0.013091
frighten,0.004119
frubber,0.005905
fuel,0.002461
fullbodi,0.005037
fulli,0.006008
function,0.009520
fundament,0.001701
furthermor,0.002276
fusion,0.003096
futur,0.012943
g,0.001797
gain,0.001616
gakuin,0.005756
garbag,0.004257
gase,0.002961
gear,0.003371
gecko,0.019050
gener,0.015272
generalis,0.003833
gentl,0.004083
georgia,0.003175
german,0.001876
germani,0.002000
gestur,0.033074
given,0.001332
glass,0.005856
globe,0.003025
glossari,0.003238
goal,0.003654
good,0.004430
govern,0.001263
gp,0.003619
grand,0.002580
grass,0.003347
graviti,0.005792
great,0.004491
greater,0.005019
greatest,0.002337
grip,0.007992
gripper,0.051809
ground,0.003866
group,0.001198
grow,0.005110
guarante,0.002436
guid,0.007512
guidanc,0.002995
gyroscop,0.004695
ha,0.015058
hal,0.004197
half,0.003657
hall,0.002520
han,0.002654
hand,0.026335
handl,0.007246
hanson,0.004633
happen,0.004199
happi,0.002937
haptic,0.004877
harder,0.003301
hardwar,0.006215
hazard,0.006067
health,0.005555
healthcar,0.003128
heat,0.004684
heavi,0.013775
heavyduti,0.005632
height,0.002648
held,0.001570
helicopt,0.003612
help,0.002908
helper,0.004416
herd,0.003544
hi,0.007146
high,0.002690
higher,0.001579
highlevel,0.003507
highli,0.005246
highlight,0.002735
hill,0.002642
histori,0.002212
hit,0.002750
hiu,0.005632
hobbi,0.008330
hold,0.003200
home,0.005690
honda,0.004633
hop,0.008462
hospi,0.005905
hospit,0.007858
hot,0.002925
household,0.002642
howev,0.008624
hu,0.004069
human,0.070391
humanlik,0.004231
humanoid,0.021155
humanrobot,0.015533
hundr,0.006055
huosheng,0.005905
hurdl,0.004222
hybrid,0.008326
hydraul,0.007425
ibm,0.003416
idea,0.002981
identifi,0.001605
ieee,0.003619
ifrem,0.005632
imag,0.010227
imagin,0.002655
immedi,0.001952
imped,0.003579
impedancemeasur,0.005905
implement,0.001849
implic,0.002314
import,0.003398
improv,0.011442
incap,0.003579
includ,0.016598
incorpor,0.001824
increas,0.006387
increasingli,0.003916
index,0.002110
indic,0.001634
indoor,0.004075
industri,0.018195
inerti,0.007676
inertia,0.003747
infer,0.002552
inform,0.013206
infrar,0.003430
initi,0.001443
innov,0.002300
input,0.002406
insid,0.006683
inspect,0.003319
inspir,0.004395
instal,0.002621
instead,0.003111
institut,0.007028
instruct,0.002500
instrument,0.002045
integr,0.003206
intellectu,0.002278
intellig,0.022799
intens,0.002206
interact,0.026518
interdisciplinari,0.002730
interfac,0.005783
intern,0.004656
interpret,0.001772
introduc,0.001558
intuit,0.002842
invers,0.005610
invert,0.003590
invest,0.001891
investig,0.005584
involv,0.001287
isaac,0.005750
isbn,0.001570
isplashi,0.005905
isplashii,0.005905
israel,0.002720
jame,0.001873
japan,0.002196
japanes,0.002531
jaw,0.011843
jcm,0.005478
jelli,0.009795
jellyfish,0.004461
job,0.013763
joint,0.007017
josef,0.004090
journey,0.003087
joy,0.003601
judg,0.002453
jump,0.009201
just,0.001548
k,0.002013
kangaroo,0.004798
karel,0.008971
keyboard,0.008746
kid,0.003965
kind,0.003538
kinemat,0.022199
kismet,0.005756
know,0.002012
knowledg,0.005012
known,0.005549
lab,0.008705
label,0.002389
labor,0.002180
laboratori,0.004507
labour,0.002248
land,0.003361
languag,0.003338
larg,0.007022
lavatori,0.005905
law,0.001337
leadacid,0.010553
leagu,0.002724
learn,0.014925
learningbas,0.005478
leav,0.001880
led,0.001433
leg,0.029671
lego,0.014341
length,0.004406
let,0.002488
letter,0.002192
level,0.011952
leverag,0.003427
li,0.006884
liar,0.009046
lidar,0.008923
lifetim,0.002843
lift,0.005929
light,0.009154
lighter,0.006970
lighterthanair,0.005632
like,0.015421
likewis,0.002630
limb,0.006951
line,0.001589
linear,0.009451
link,0.000965
liquid,0.002380
list,0.001234
littl,0.003399
live,0.001435
lizard,0.004002
load,0.012190
local,0.001504
locomot,0.022651
logist,0.003204
long,0.005767
longer,0.001831
lot,0.002735
lower,0.001640
lowinvas,0.005905
lowlevel,0.004002
machin,0.030018
machineri,0.002738
magnitud,0.002739
main,0.001405
mainli,0.001843
maintain,0.004645
mainten,0.002664
major,0.002356
make,0.010163
malfunct,0.004126
manag,0.006361
maneuv,0.007617
mani,0.020074
manifest,0.002554
manipul,0.023777
manmad,0.003568
manrobot,0.005905
manta,0.005526
manu,0.004342
manufactur,0.016942
map,0.006094
marc,0.003666
march,0.001841
market,0.001662
mass,0.003642
master,0.002369
materi,0.009337
mathemat,0.003491
maximum,0.002414
mayb,0.004021
mean,0.002445
meaning,0.002886
measur,0.008727
mechan,0.027878
mechatron,0.004711
medic,0.002045
medium,0.002531
mediumtohighlevel,0.005905
mein,0.004919
mellon,0.008166
memori,0.002309
mention,0.002149
merger,0.003217
metal,0.011556
method,0.015128
methodolog,0.002350
micro,0.003635
middl,0.001830
midlevel,0.004728
militari,0.005389
militarili,0.003813
million,0.001744
mimic,0.022327
mimick,0.004373
miniatur,0.003907
minut,0.002650
missil,0.003460
mission,0.002214
mistaken,0.003666
mit,0.008094
mix,0.002135
mm,0.003249
mobil,0.007076
mobot,0.005905
mode,0.002500
model,0.004149
modern,0.003996
modifi,0.002166
moment,0.007579
momentum,0.002919
monitor,0.002401
monkey,0.003643
monoton,0.003971
month,0.001961
moravec,0.018846
mostli,0.007578
motion,0.024649
motor,0.054417
mount,0.005177
mous,0.003646
movement,0.013781
movi,0.009111
ms,0.003337
mud,0.003880
muddi,0.004856
multiag,0.004342
multimod,0.004536
multipl,0.001708
muscl,0.027717
museum,0.002677
nanomet,0.004322
nanorobot,0.005351
nanotub,0.018846
nasa,0.006705
nation,0.003691
natur,0.008298
navig,0.030502
near,0.001825
nearli,0.001934
necessari,0.003539
necessarili,0.002173
need,0.018689
new,0.015870
nexi,0.005577
ni,0.003907
nitinol,0.005756
nois,0.003166
noisi,0.004134
nonindustri,0.004897
norbert,0.004111
normal,0.001770
notabl,0.001703
novemb,0.001991
nuclear,0.006807
number,0.004443
numer,0.001645
nut,0.003833
object,0.015180
observ,0.004396
obstacl,0.009614
obviou,0.002805
occasion,0.002505
occup,0.006941
ocean,0.002285
offer,0.004953
offic,0.003574
oil,0.002216
old,0.001837
onboard,0.004663
onc,0.004977
oneallow,0.005905
onewheel,0.017716
onli,0.009199
open,0.003040
oper,0.018211
operatorassist,0.005905
opportun,0.002097
oppos,0.001855
opposit,0.001827
optic,0.002671
optim,0.007066
orb,0.010127
order,0.007342
organ,0.001211
organis,0.002102
orient,0.002313
origin,0.004922
osh,0.010355
otherwis,0.002112
outdoor,0.008005
outer,0.002837
outjump,0.005905
outlin,0.002161
outperform,0.003869
outrun,0.005209
oxford,0.004172
oxid,0.002946
pace,0.002996
pack,0.003240
packag,0.002815
pad,0.004149
paddl,0.004762
page,0.002177
panel,0.003272
paper,0.001832
paradigm,0.002690
paramet,0.002560
particular,0.006949
particularli,0.007786
passeng,0.003304
passiv,0.006119
pasta,0.004798
pastri,0.005012
path,0.004503
patient,0.005409
pattern,0.003789
pendulum,0.004197
penguin,0.010019
peopl,0.009794
percent,0.002230
percentag,0.002474
percept,0.002363
perform,0.035905
perhap,0.010321
period,0.001319
person,0.010171
petrol,0.013119
phase,0.004270
phd,0.002869
physic,0.004359
physiolog,0.002615
piano,0.004394
pick,0.008671
piec,0.002452
piezo,0.023309
piezoceram,0.005905
pilot,0.003289
piscin,0.005905
place,0.007535
plan,0.005045
plane,0.002674
planner,0.003524
plant,0.001927
plastic,0.002985
plate,0.002883
platform,0.005438
play,0.006413
plen,0.005526
pleo,0.005756
pneumat,0.013316
pogo,0.005478
point,0.003864
pole,0.003076
polic,0.004939
polym,0.006674
poor,0.002195
poorli,0.002940
popul,0.004739
popular,0.003177
portabl,0.003704
porter,0.003747
posit,0.007896
possibl,0.003896
posterior,0.004015
pot,0.003896
potenti,0.009891
power,0.024759
powerpol,0.005905
practic,0.004087
precis,0.002097
predat,0.002826
predict,0.007452
predomin,0.002904
preexist,0.006933
prefer,0.001949
prehens,0.005827
prentic,0.003442
preprogram,0.004964
prescrib,0.003109
present,0.002546
preset,0.009456
previou,0.001920
primarili,0.001740
princip,0.001989
principl,0.003057
print,0.002391
prior,0.001923
probabl,0.001827
problem,0.004085
process,0.012037
produc,0.003974
product,0.003981
prof,0.003891
professor,0.004551
program,0.033638
programm,0.004895
progress,0.003471
project,0.003151
promis,0.007178
promot,0.001816
propag,0.002837
propel,0.007137
properti,0.001541
proport,0.002105
propos,0.001571
propuls,0.011933
prosthesi,0.005090
prosthet,0.008569
protect,0.001697
protrus,0.004919
prove,0.001965
provid,0.009038
psychosoci,0.004149
public,0.002564
publish,0.002826
purpos,0.006625
push,0.002421
quadrup,0.004988
quantum,0.002568
question,0.001599
quicker,0.004111
r,0.001703
radar,0.007109
radiat,0.002550
radioact,0.003124
raibert,0.005391
rais,0.001878
rang,0.007642
rapid,0.002206
rapidli,0.004293
raw,0.002634
ray,0.008275
rc,0.004205
rcsa,0.005756
rd,0.002280
reach,0.001641
reaction,0.004141
reactiv,0.003042
read,0.001402
readili,0.002763
real,0.011733
reallif,0.004104
realtim,0.003385
reason,0.002898
receiv,0.007714
recent,0.008390
receptionist,0.005351
receptor,0.003439
recogn,0.006926
recognit,0.004427
reduc,0.004589
redund,0.003572
refer,0.008883
reflect,0.003471
rel,0.002896
relev,0.004224
reli,0.001945
reliabl,0.004829
remot,0.016404
remov,0.003729
repeat,0.004662
repetit,0.006588
replac,0.008228
replic,0.005701
report,0.001552
repres,0.001324
requir,0.012713
research,0.022272
resembl,0.005095
resolut,0.002470
respect,0.001535
respons,0.001400
result,0.004499
revis,0.002413
richard,0.001995
right,0.002898
rigid,0.006010
rise,0.001625
risk,0.003974
riski,0.003424
rmp,0.005756
road,0.004691
roboeth,0.005433
robonaut,0.011384
robot,0.937001
robota,0.005756
roboticsrel,0.005905
robotproject,0.005905
robust,0.008775
rocki,0.007718
roll,0.006026
rollersk,0.005905
roomba,0.005526
rossum,0.005209
rotari,0.004438
rotat,0.010694
rough,0.006405
round,0.002558
rubber,0.003504
rudder,0.004856
ruin,0.003211
ruixiang,0.005905
run,0.008873
runaround,0.011513
rur,0.005037
russel,0.002830
s,0.003436
sad,0.003924
safe,0.005280
safeti,0.023427
said,0.001659
sail,0.006109
sailboat,0.020254
sailbot,0.005905
satisfactori,0.003674
sauc,0.004695
save,0.004586
scale,0.001827
school,0.006572
schunk,0.005391
scienc,0.019015
scientif,0.001625
scientist,0.003693
scoop,0.004988
screw,0.004352
seam,0.004510
search,0.002067
second,0.009278
sector,0.002117
secur,0.001715
seen,0.003169
segway,0.017716
selfcontrol,0.004284
sens,0.011632
sensor,0.050212
sensori,0.009392
sequenc,0.004484
seri,0.003217
serv,0.004822
servic,0.001573
servo,0.010295
set,0.002502
sever,0.015232
shadow,0.006293
shape,0.007699
share,0.003147
shelf,0.003941
shell,0.002718
shipwreck,0.004075
shock,0.002760
short,0.005165
shown,0.001947
signal,0.006971
signific,0.001434
significantli,0.002072
silvercadmium,0.005905
similar,0.004026
simpl,0.003571
simplest,0.002866
simpli,0.001833
simplic,0.003251
simplifi,0.002617
simul,0.007326
sinc,0.005619
singl,0.001504
singular,0.002961
situat,0.001730
sixwheel,0.011810
size,0.001750
skate,0.019677
skateboard,0.005577
skill,0.006495
skin,0.005701
skull,0.003590
slavic,0.003936
slightli,0.002435
slope,0.003244
small,0.008251
smaller,0.003822
smarthand,0.005905
smooth,0.014903
snake,0.017844
social,0.008578
societi,0.001408
soft,0.002869
softwar,0.009525
solar,0.005361
solidst,0.004009
solut,0.001922
solv,0.004136
somersault,0.005692
someth,0.004321
sometim,0.001463
sonar,0.008746
soon,0.004122
sophist,0.005171
sound,0.009465
sourc,0.009896
space,0.008664
speaker,0.005772
special,0.005622
specif,0.006723
specifi,0.006855
speech,0.014529
speed,0.011492
speedi,0.004877
sphere,0.002515
spheric,0.006564
spin,0.006234
spirit,0.002539
spoken,0.006046
sport,0.005338
spread,0.001986
sprint,0.004745
stabil,0.002096
stabl,0.002211
stack,0.003504
stage,0.001962
stair,0.004817
standard,0.004541
stanford,0.002610
star,0.002492
start,0.002845
state,0.002950
static,0.005750
stationari,0.003258
statu,0.001877
stay,0.002460
steadili,0.002964
stem,0.005052
step,0.003811
stick,0.003379
sticki,0.004083
stickybot,0.005905
stop,0.002220
storag,0.008023
store,0.004554
stori,0.004556
straight,0.003117
strain,0.002793
strategi,0.002093
streamlin,0.004197
stride,0.004097
strive,0.003222
structur,0.002626
student,0.003826
studi,0.004751
subfield,0.002893
submarin,0.003396
suboptim,0.004405
subsequ,0.003507
substanti,0.004172
substitut,0.004995
subsurfac,0.004205
success,0.004471
suction,0.004663
suit,0.002612
suitabl,0.002495
summer,0.010400
sun,0.002493
supervisori,0.004205
suppli,0.007329
supplier,0.003164
surfac,0.018316
surgeri,0.006337
surround,0.002058
surveil,0.003547
surviv,0.001938
survivor,0.003491
swim,0.021905
swing,0.003708
switch,0.002759
synthet,0.002787
tactil,0.017451
takeoff,0.004576
tank,0.003113
target,0.004185
task,0.060465
tasklevel,0.005827
teach,0.004403
technic,0.001998
techniqu,0.017272
technolog,0.016205
teleoper,0.011155
tell,0.002531
term,0.006372
terrain,0.013400
territori,0.001967
test,0.001837
tether,0.004648
texa,0.006346
th,0.002577
themselv,0.001667
theoret,0.001842
theori,0.002634
therefor,0.003055
thi,0.026901
thing,0.001788
think,0.001949
thought,0.003185
thousand,0.001982
thu,0.002627
thunniform,0.005905
tight,0.003430
time,0.010938
tini,0.003032
today,0.008073
toe,0.004205
togeth,0.001549
tohoku,0.005478
toil,0.004648
told,0.002850
tool,0.001799
topic,0.001783
total,0.003136
touch,0.008899
toxic,0.003107
toy,0.003679
track,0.019894
tracker,0.004817
traction,0.008551
tradit,0.002830
train,0.007763
transfer,0.003846
translat,0.001934
transmiss,0.002776
transmit,0.002753
transport,0.003853
trap,0.002944
travel,0.003969
travers,0.003654
tread,0.004988
trek,0.004055
tri,0.012383
trot,0.005177
tu,0.004248
tube,0.003105
tuna,0.004055
tune,0.003650
turn,0.003107
turntabl,0.005478
tv,0.003107
twowheel,0.010957
type,0.018918
typic,0.010555
u,0.002622
uav,0.010074
ultrason,0.004633
unawar,0.003654
understand,0.001525
underwat,0.003623
uneven,0.007569
unforeseen,0.004342
unhealthi,0.004157
unim,0.005632
uninhabit,0.003551
unit,0.002267
univers,0.011640
unman,0.004303
unnatur,0.004009
unpleas,0.004015
unpow,0.011155
unsaf,0.004373
unstabl,0.002998
unsupervis,0.004590
upright,0.004002
upsidedown,0.005012
urban,0.002357
urbi,0.005526
usa,0.002509
use,0.076225
user,0.002493
usual,0.001352
util,0.004066
vacuum,0.008798
vacuumclean,0.005905
vaimo,0.005905
valu,0.004321
vari,0.001656
variabl,0.002054
variat,0.002106
varieti,0.005062
variou,0.010236
vast,0.002256
vehicl,0.002552
veloc,0.008866
verbal,0.003233
veri,0.016686
vertic,0.005815
vibrat,0.009839
video,0.004947
view,0.002881
visibl,0.002484
vision,0.021910
vocat,0.003770
voic,0.013394
volum,0.003850
wa,0.018431
waiter,0.005478
walk,0.074644
walker,0.003541
wall,0.009519
wallbot,0.005905
wallclimb,0.005905
want,0.002075
warn,0.002702
wast,0.002696
water,0.003467
waveform,0.004604
way,0.013219
waypoint,0.010867
weaponri,0.003853
wear,0.003045
weight,0.006796
weld,0.016756
wheel,0.037236
wherea,0.001950
wherebi,0.002539
whi,0.002068
wholli,0.003270
wide,0.001447
wider,0.002559
widespread,0.002159
width,0.003779
wiener,0.003880
wind,0.002670
windscreen,0.005756
winner,0.003094
wire,0.022729
word,0.017161
work,0.009916
worker,0.004184
workshop,0.003326
workspac,0.005209
world,0.003418
worldwid,0.002280
worst,0.003100
write,0.001757
writer,0.004613
wrote,0.001914
wrsc,0.005905
y,0.002393
year,0.002268
yeung,0.005351
york,0.001731
young,0.002086
youth,0.005669
zero,0.004727
zhang,0.003813
zhuhai,0.005577
zmp,0.017481
