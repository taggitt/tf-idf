abbrevi,0.055258
achiev,0.028540
ai,0.341963
aim,0.032396
allen,0.314280
allow,0.022561
appoint,0.037294
aristo,0.099057
articl,0.080890
artifici,0.229871
attempt,0.025878
avail,0.027732
avoid,0.033887
base,0.039536
becaus,0.020592
better,0.030848
brain,0.043775
breakthrough,0.058377
capabl,0.036079
capit,0.029797
carri,0.029535
cell,0.039403
citat,0.054566
cnn,0.079884
coauthor,0.062827
cofound,0.062948
compani,0.028934
compos,0.034765
comput,0.060728
construct,0.028377
cours,0.031938
cover,0.030170
coverag,0.055124
creat,0.044829
current,0.023460
decemb,0.033628
deriv,0.027863
design,0.026916
develop,0.018327
diagram,0.050411
diagrambas,0.119351
digress,0.090901
direct,0.024334
directli,0.029444
discoveri,0.034322
discuss,0.029901
engin,0.031795
entir,0.028616
etzioni,0.188716
euclid,0.067212
examin,0.069062
explain,0.029700
extern,0.017251
extract,0.042468
field,0.024093
flagship,0.075142
focu,0.032366
focus,0.061422
fund,0.032528
geekwir,0.119351
geometri,0.048115
given,0.023661
goal,0.032450
grade,0.165071
groundbreak,0.067212
guid,0.133425
ha,0.015731
halo,0.074839
imag,0.072653
import,0.020121
incub,0.147661
indepth,0.068442
inform,0.070362
inspir,0.039033
institut,0.149786
integr,0.028469
intellig,0.220861
key,0.030020
knowledg,0.089013
languag,0.059293
launch,0.119168
learn,0.066266
link,0.017149
list,0.021931
literatur,0.035238
mathemat,0.031003
media,0.036174
mental,0.085351
mention,0.038181
metaphor,0.055463
microsoft,0.062471
model,0.049132
natur,0.021054
new,0.035232
novemb,0.035372
offer,0.029326
offici,0.059881
oper,0.024879
ordinari,0.043005
oren,0.098148
paper,0.065081
pass,0.030638
paul,0.068561
philosophi,0.033576
plato,0.103903
prepar,0.036677
problem,0.072558
produc,0.023530
project,0.279825
read,0.024910
reason,0.025741
refer,0.014342
refin,0.044992
research,0.093071
result,0.019978
scholar,0.072847
school,0.029180
scienc,0.072366
scientif,0.057722
search,0.073420
seattlebas,0.110219
seek,0.032183
semant,0.159495
septemb,0.034587
servic,0.027944
similar,0.023835
solv,0.036735
solver,0.075609
startup,0.138795
state,0.017465
stateoftheart,0.078043
static,0.051060
studi,0.063284
subject,0.025392
survey,0.038368
technolog,0.028779
text,0.034433
textbook,0.044992
textual,0.062707
th,0.068664
thi,0.029860
thing,0.031761
topic,0.031670
trend,0.038126
tri,0.031418
type,0.023998
understand,0.108378
user,0.044281
verg,0.070535
video,0.043932
visual,0.042114
vulcan,0.163038
wa,0.065465
wealth,0.041105
websit,0.037744
word,0.027707
xconomi,0.112621
