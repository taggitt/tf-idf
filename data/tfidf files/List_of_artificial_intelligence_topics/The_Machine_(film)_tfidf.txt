access,0.011186
accident,0.020912
achiev,0.010171
action,0.009934
adapt,0.012874
afterward,0.017808
agent,0.013785
aggreg,0.017097
agre,0.012189
ai,0.020310
aid,0.012129
alert,0.022631
allmovi,0.034976
allow,0.008040
altern,0.010388
ambit,0.021087
ampute,0.034673
android,0.026164
anna,0.023081
antagon,0.024766
apolog,0.024626
appar,0.013802
appeal,0.015057
april,0.037106
arm,0.012891
arrang,0.013180
artifici,0.027306
ask,0.013365
atlant,0.017132
atmospher,0.015838
attempt,0.009222
audaci,0.030371
ava,0.157843
averag,0.024152
avoid,0.012076
award,0.042760
b,0.010708
bafta,0.034673
base,0.049313
beauti,0.018523
becom,0.015608
befor,0.024978
beg,0.025496
best,0.010744
blockbust,0.031722
book,0.009265
brain,0.062401
braindamag,0.033867
brim,0.036432
britain,0.013733
british,0.043540
brood,0.029924
budget,0.015259
caiti,0.085065
capabl,0.012857
caradog,0.170131
cast,0.016585
caus,0.008714
chines,0.042105
chip,0.021920
citi,0.022552
cognit,0.015480
come,0.018678
commun,0.008538
compar,0.009658
comput,0.032462
consciou,0.019310
conscious,0.016635
consensu,0.016289
consol,0.025977
corner,0.021071
countercultur,0.028239
coupl,0.015111
creat,0.007987
cri,0.024104
critic,0.019212
cybernet,0.070009
cyborg,0.144384
cymru,0.036024
dalton,0.023523
databas,0.015278
daughter,0.035846
dawson,0.237725
dead,0.016372
deal,0.010520
debut,0.025977
defenc,0.015897
demand,0.023318
demonstr,0.023476
deni,0.015139
denni,0.022389
deriv,0.009929
design,0.009592
despit,0.011073
destroy,0.027312
develop,0.006531
devic,0.014595
dialect,0.019211
die,0.012513
direct,0.008671
director,0.015702
disabl,0.038037
disord,0.016834
distress,0.021675
download,0.023056
dub,0.021006
dure,0.007517
effici,0.012310
effort,0.010888
empathi,0.025622
empir,0.010970
employ,0.010428
entertain,0.019332
entranc,0.022139
entri,0.015208
escap,0.015785
espino,0.039278
excis,0.025060
execut,0.011910
expect,0.010802
explor,0.011979
extern,0.006147
failsaf,0.032394
fall,0.010732
fan,0.022118
fantasi,0.022262
favor,0.012985
felperin,0.042532
festiv,0.039052
fiction,0.033538
film,0.159458
final,0.010169
flaw,0.020043
fluent,0.028962
free,0.009603
function,0.008607
fund,0.011592
futur,0.010239
futurenoir,0.041182
game,0.014427
gave,0.012233
gaze,0.028468
genr,0.019851
glasbi,0.042532
goe,0.015129
griev,0.029818
gross,0.016438
ground,0.012233
grow,0.021561
guardian,0.020773
ha,0.016818
half,0.011572
hand,0.020834
harvey,0.021320
head,0.010853
heft,0.036024
help,0.027612
hemispherectomi,0.040134
hi,0.075382
highli,0.022135
hollywood,0.054118
home,0.012005
hostil,0.017436
human,0.032399
idea,0.018872
ignor,0.014583
imperson,0.025977
implant,0.045538
increasingli,0.012394
independ,0.008635
influx,0.021693
insist,0.034307
intellig,0.026235
internet,0.013791
jame,0.059287
jon,0.023469
june,0.012248
kill,0.069392
killer,0.025332
kim,0.023496
kingdom,0.033548
lack,0.010717
late,0.009731
latest,0.037548
laudabl,0.036432
lead,0.008386
leav,0.023806
left,0.011204
lesli,0.023747
leverag,0.021693
life,0.018070
like,0.007507
limit,0.008583
link,0.006111
littl,0.010757
lobotom,0.041182
london,0.011834
look,0.011605
lose,0.014782
loss,0.012293
lost,0.012214
lotz,0.069347
love,0.016685
machin,0.339262
magazin,0.016272
major,0.007456
malaysia,0.020536
map,0.012857
march,0.023309
mari,0.064949
match,0.014946
matt,0.025840
mccarthi,0.586412
mechan,0.010378
metacrit,0.065540
method,0.008704
militari,0.011369
million,0.011039
ministri,0.015111
mix,0.013518
modern,0.008430
modestli,0.028792
moral,0.028120
mostli,0.011991
mother,0.097182
movi,0.038443
murder,0.017849
nav,0.033397
necessari,0.011200
need,0.008448
neurolog,0.021269
new,0.025111
newman,0.025977
obey,0.040112
ocean,0.014462
odd,0.039654
offer,0.010450
offic,0.011312
offici,0.010669
onli,0.019408
oper,0.008866
orang,0.021006
order,0.030979
outsid,0.010721
overrid,0.024523
paul,0.012216
perform,0.009880
place,0.007948
plan,0.010643
play,0.010147
plot,0.017224
polit,0.008820
posit,0.008329
predict,0.011791
premier,0.020175
prepar,0.013070
prize,0.015030
produc,0.016770
product,0.008399
programm,0.015490
project,0.019944
promis,0.030287
protect,0.010741
prove,0.024875
provid,0.014300
qateel,0.040134
quantum,0.016254
raindanc,0.042532
rate,0.089885
read,0.017754
rebel,0.018570
recept,0.020228
recipi,0.019728
record,0.010773
recruit,0.018844
refer,0.005111
refus,0.014915
regain,0.019632
regret,0.026512
regularli,0.017399
rehabilit,0.022038
releas,0.063869
relent,0.060743
remov,0.011800
reneg,0.029515
report,0.019647
request,0.031070
research,0.016583
rest,0.011974
rett,0.035300
review,0.034357
road,0.014846
robot,0.018946
rotten,0.058648
routin,0.018347
ruthless,0.026833
sacrific,0.020819
said,0.010501
sallyann,0.042532
scan,0.081632
scene,0.018411
scienc,0.025789
scientist,0.058438
scifi,0.060064
score,0.018734
secret,0.031539
selfawar,0.024626
sens,0.010517
sentient,0.024358
septemb,0.012325
seri,0.020364
set,0.007917
sham,0.028390
shoot,0.043350
shot,0.019877
shown,0.012325
signifi,0.020623
site,0.011981
slow,0.014685
smart,0.022346
smith,0.015070
soldier,0.080927
solid,0.015287
solips,0.030737
son,0.013619
speech,0.015325
stabl,0.013999
star,0.094650
start,0.009004
state,0.006224
stay,0.015570
stephen,0.031602
sternli,0.036024
strength,0.014427
stunt,0.027744
stylish,0.034673
subterranean,0.029419
suffer,0.012983
sunset,0.026944
superhuman,0.028709
suri,0.097735
survey,0.013673
suspici,0.048586
syfi,0.036880
sympathi,0.023310
syndrom,0.021675
tablet,0.022676
taipei,0.027677
taiwan,0.020017
talk,0.015306
technolog,0.010256
test,0.034886
themat,0.024802
theyoungfolkscom,0.042532
thi,0.005320
thomson,0.243086
thoughtprovok,0.034120
threaten,0.016174
thrill,0.029614
thriller,0.088844
time,0.006293
tobi,0.030612
tomato,0.048781
took,0.011112
total,0.009925
treatment,0.013085
tribeca,0.041182
troubl,0.037352
ture,0.021512
turn,0.019668
tv,0.039336
uk,0.026076
underground,0.019716
unit,0.028698
unless,0.015940
unlimit,0.021286
unsubtl,0.042532
upset,0.024042
use,0.017436
varieti,0.010680
video,0.031312
vincent,0.022542
violat,0.015738
virtualis,0.042532
volunt,0.019608
w,0.023870
wa,0.034994
wale,0.019802
want,0.026265
warn,0.034209
websit,0.013450
week,0.014950
weight,0.014337
whisper,0.032394
won,0.014399
work,0.013947
worth,0.016106
wound,0.078059
writerdirector,0.072048
written,0.011262
wrote,0.036358
xlrator,0.042532
york,0.021911
