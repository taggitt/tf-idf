access,0.013348
adjectivenp,0.050751
alchemyapi,0.050751
ambigu,0.022668
anchovi,0.082067
api,0.247970
appli,0.010313
applic,0.080649
approach,0.011102
araya,0.047889
assist,0.014384
automat,0.078776
bar,0.021807
base,0.016811
becaus,0.008756
bilingu,0.162495
biomed,0.051085
biotex,0.050751
board,0.016781
built,0.014501
candid,0.053836
capabl,0.015341
card,0.022136
categori,0.014865
centr,0.016019
chart,0.022192
chunk,0.031051
classifi,0.015760
cluster,0.021376
collect,0.011365
colloc,0.042121
combin,0.011110
commun,0.030565
compar,0.011524
compound,0.017558
comput,0.012911
concept,0.010874
conceptu,0.018754
constitut,0.012303
construct,0.012066
contain,0.011178
cooccurr,0.040713
corpora,0.106372
corpu,0.052737
crawler,0.041373
creation,0.014183
credit,0.015079
crossplatform,0.042985
customiptc,0.050751
dandelion,0.040713
dataset,0.028652
dedic,0.018717
describ,0.009591
develop,0.007793
director,0.018736
document,0.030088
domain,0.091313
domainrelev,0.050751
domainspecif,0.035336
editor,0.019971
engin,0.013520
english,0.026131
englishgermandutch,0.050751
enterpris,0.017658
entiti,0.047753
entityclassifiereu,0.050751
entri,0.018146
era,0.014822
essenti,0.013041
europ,0.012721
extern,0.007335
extract,0.541763
extractor,0.162854
file,0.020940
filter,0.070149
format,0.013682
free,0.080211
freeopensourc,0.050751
french,0.013500
frequent,0.014102
furthermor,0.017191
gabor,0.036988
gener,0.014417
given,0.010061
glossari,0.097811
glossml,0.050751
goal,0.013798
grow,0.012863
heartsom,0.101502
high,0.010157
human,0.009665
hypernym,0.050751
ibm,0.025797
identifi,0.012126
implement,0.013966
import,0.008555
includ,0.013927
index,0.015936
industri,0.011450
info,0.031277
inform,0.029919
internet,0.016456
interoper,0.033185
introduct,0.013298
keyword,0.131176
knowledg,0.037850
known,0.008381
languag,0.037819
learn,0.014089
lexic,0.031952
lexterm,0.050751
like,0.008958
linguist,0.059148
link,0.014585
list,0.009325
literaci,0.024608
literatur,0.014984
local,0.011358
low,0.012853
machin,0.032385
make,0.008528
manag,0.048043
manifest,0.019292
map,0.015341
markup,0.033025
measur,0.010984
melli,0.043472
method,0.031158
mind,0.015814
model,0.020892
mono,0.036527
multilingu,0.191324
nation,0.009292
natur,0.008952
need,0.010081
network,0.014019
new,0.007490
ngram,0.043472
noun,0.026392
np,0.030324
number,0.008389
obtain,0.013034
offic,0.013498
onc,0.012530
onlin,0.029355
ontolog,0.045950
open,0.022961
opensourc,0.030941
packag,0.042519
page,0.016440
parallel,0.017376
particularli,0.011760
phrase,0.040412
plausibl,0.025065
point,0.009727
prepositionalnp,0.050751
process,0.009090
processor,0.027140
rank,0.016235
recognit,0.016716
recommend,0.017977
refer,0.006098
relev,0.015951
research,0.009894
saa,0.076476
semant,0.045214
semantria,0.050751
servic,0.035647
sever,0.017697
similar,0.020270
simplif,0.027908
sketch,0.027224
softwar,0.017982
sourc,0.010676
spanish,0.018355
specif,0.010155
speech,0.018287
start,0.021488
statist,0.041775
step,0.014392
strong,0.012333
subject,0.010797
subtask,0.038442
summar,0.043237
support,0.010091
surfac,0.015368
syntact,0.029724
tag,0.027570
taxonomi,0.073256
tbxtool,0.050751
technic,0.015095
term,0.120304
termfind,0.050751
termin,0.019887
terminolog,0.459687
texlexan,0.050751
text,0.146418
textcav,0.050751
theme,0.018798
tmx,0.044596
tool,0.054363
topicdriven,0.050751
tourist,0.022619
translat,0.073061
type,0.010204
typic,0.011387
uk,0.015557
ultim,0.014680
use,0.034676
veri,0.009693
virtual,0.016785
visual,0.017908
vocabulari,0.025379
warehous,0.029947
web,0.202419
webbas,0.097933
xml,0.034456
