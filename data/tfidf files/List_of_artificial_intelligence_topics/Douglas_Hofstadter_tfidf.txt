abhijit,0.032794
abil,0.004945
abstract,0.031627
academ,0.005384
academi,0.006924
accent,0.012219
account,0.004233
ach,0.040058
act,0.004108
action,0.004416
activ,0.025107
addit,0.003636
adjunct,0.011834
admit,0.007580
ai,0.009030
aim,0.005133
al,0.006165
alan,0.015241
alexand,0.006887
alic,0.010995
allen,0.008299
allus,0.011979
alphabet,0.008655
altern,0.004618
alway,0.014501
amazoncom,0.013123
ambigram,0.056731
ambigrammi,0.018910
ambigrammist,0.018910
american,0.036655
anagram,0.014751
analog,0.077078
analogybas,0.018910
analogymak,0.132373
andrew,0.007300
angel,0.008620
ani,0.003168
ann,0.033676
anoth,0.006914
ant,0.009754
appoint,0.017727
approach,0.008273
april,0.021997
arbor,0.023440
architectur,0.040682
art,0.038494
arthur,0.007332
artifici,0.018210
artist,0.022632
artwork,0.011011
assert,0.006175
assum,0.004979
atlant,0.007617
attend,0.006858
attitud,0.007379
audienc,0.008113
audio,0.018868
autobiographi,0.010859
autoportrait,0.018910
avail,0.004394
avenu,0.010399
avoid,0.005369
award,0.019011
axelrod,0.014484
b,0.009522
bach,0.062134
baofen,0.018910
barenboim,0.017141
base,0.009396
battl,0.006788
beau,0.045511
beauti,0.041178
becom,0.003469
begin,0.004226
bias,0.008981
bibliograph,0.012191
bibliographi,0.006303
bicentenni,0.013401
bite,0.011335
bloch,0.023763
bloomington,0.049833
bloop,0.018910
blue,0.007725
bob,0.010082
boicho,0.017844
bolognabound,0.018910
bon,0.013304
bongard,0.032794
book,0.094747
booksmit,0.018910
born,0.012245
bound,0.006485
bradford,0.012109
braid,0.039240
brain,0.034680
brainstem,0.014248
brian,0.008865
broadcast,0.008410
brush,0.022784
built,0.005403
buri,0.008570
burk,0.010903
butterfli,0.021272
byrdmus,0.018910
c,0.004148
caianiello,0.016198
calligraphi,0.013452
cambridg,0.005749
campu,0.010284
canon,0.007882
career,0.014242
carol,0.032275
carri,0.009359
categor,0.007379
caught,0.008940
caus,0.003874
cd,0.009993
center,0.013890
certain,0.003973
chacha,0.016617
chair,0.008153
chalmerstoward,0.018910
chamad,0.018910
chandra,0.012083
chang,0.006545
chapter,0.020297
character,0.005506
children,0.023881
choos,0.006244
chopin,0.015849
christof,0.014848
citi,0.005013
claim,0.004406
clark,0.008405
class,0.004815
classic,0.004796
clear,0.005403
clement,0.010422
clment,0.014174
clossmana,0.018910
coauthor,0.029863
cobound,0.018910
codevelop,0.014104
coedit,0.014324
cognit,0.117008
coher,0.008002
collect,0.012704
colleg,0.012604
coloni,0.012006
column,0.065985
columnist,0.012657
come,0.012456
comment,0.006954
committe,0.006228
compani,0.004584
compar,0.008588
compos,0.005508
composit,0.006019
comput,0.086597
concept,0.044570
conceptcent,0.018910
conceptu,0.006988
concern,0.004076
concret,0.023862
connectionist,0.013257
conscious,0.044377
consequ,0.009874
consider,0.004879
consist,0.007772
constantli,0.008094
constrain,0.008442
constraint,0.014418
contain,0.004165
content,0.005729
cooper,0.005549
copycat,0.030579
core,0.012331
cours,0.010121
cousin,0.009548
coxet,0.014248
creat,0.014206
creation,0.005285
creativ,0.037512
creativit,0.018310
crossreferenc,0.015170
cube,0.010815
cultur,0.013097
cur,0.013257
current,0.003717
d,0.004700
dafna,0.018910
dal,0.013257
damag,0.006265
daniel,0.007221
danni,0.013123
david,0.010315
dawn,0.027567
dblp,0.015416
death,0.005085
decept,0.010207
dedic,0.027898
dedr,0.017141
deep,0.006463
defeat,0.006869
defin,0.003984
degre,0.009509
della,0.011354
dellalba,0.018910
democratico,0.016397
dennett,0.012005
depart,0.010181
descend,0.007027
describ,0.007147
design,0.004264
develop,0.011615
devot,0.013726
dialogu,0.024317
die,0.005563
dilemma,0.019704
direct,0.003855
director,0.006981
discours,0.007950
discov,0.005113
discoveri,0.038067
discuss,0.009475
dissert,0.009888
distinct,0.004471
distinguish,0.005154
distribut,0.009581
divers,0.005584
doctor,0.006929
docudrama,0.017844
doesnt,0.008681
domain,0.005670
don,0.009636
donald,0.008186
doubt,0.007493
dougla,0.017161
dr,0.007203
drake,0.012219
draw,0.005952
dream,0.008382
drhjj,0.018910
driven,0.006759
drop,0.006372
dutch,0.014262
earli,0.003496
ed,0.014681
edit,0.021885
editor,0.007441
eduardo,0.012056
educ,0.004453
effect,0.003395
effort,0.004841
egbert,0.014848
ekbiaai,0.018910
elect,0.010311
electron,0.011699
emerg,0.013440
emmanuel,0.011469
empathi,0.011392
energi,0.009787
engin,0.005037
english,0.009737
enigma,0.012692
entitl,0.021650
equal,0.004760
eric,0.008303
ernest,0.008993
err,0.013969
error,0.013294
errormak,0.018910
escher,0.067008
especi,0.004059
essay,0.027096
essenc,0.015793
establish,0.003730
etern,0.026512
eugen,0.017092
event,0.004549
exampl,0.009392
exemplifi,0.008905
exert,0.016042
exhibit,0.006598
expect,0.004802
explain,0.009411
explan,0.006153
explor,0.010652
express,0.009058
extend,0.004610
extern,0.005466
faculti,0.008074
fall,0.004771
fantasi,0.009898
farg,0.068567
fascin,0.010422
father,0.006145
fault,0.009390
favor,0.005773
featur,0.009455
februari,0.005679
feedback,0.016977
feel,0.006534
fellow,0.007903
ferromagnet,0.012490
field,0.007634
figur,0.005177
floop,0.018910
fluenci,0.013969
fluentli,0.014402
fluid,0.029473
focus,0.004866
follow,0.005980
followup,0.011092
foot,0.016243
foreign,0.004866
forese,0.010933
foreword,0.068238
form,0.011428
formal,0.004677
forth,0.007598
foundali,0.018910
foundalisphaeaco,0.018910
fractal,0.011698
fraction,0.007316
francisco,0.016133
frank,0.007319
franois,0.013556
frdric,0.012555
french,0.025152
frenchtabletop,0.018910
fuel,0.006927
fugu,0.016617
function,0.003827
fundament,0.004786
futur,0.009105
galilean,0.011570
galleri,0.009612
galoi,0.012276
game,0.019243
gardner,0.011141
gari,0.018651
garri,0.013167
gdel,0.064301
geb,0.075642
gebstadt,0.018910
gener,0.002686
geneva,0.009935
gentner,0.016617
geometri,0.022870
georg,0.005387
german,0.005279
gitanjali,0.018910
givan,0.018910
given,0.003749
glioblastoma,0.016397
golden,0.022722
grace,0.009852
graduat,0.021013
gray,0.009325
great,0.008425
grew,0.006246
grid,0.009464
gridfont,0.037821
group,0.006743
h,0.005192
ha,0.052342
hal,0.011810
half,0.005145
hamid,0.012877
han,0.007470
harri,0.015722
head,0.004825
held,0.004419
hi,0.130711
high,0.003784
highest,0.005724
highlevel,0.039482
highli,0.004920
highlytout,0.018910
hindi,0.013167
hire,0.008161
histori,0.006226
hodg,0.011570
hoenderdo,0.018910
hofstadt,0.795323
hofstadtermbiu,0.018910
holland,0.009737
holyoak,0.014569
home,0.005337
honor,0.007732
human,0.032411
humor,0.010123
hypothet,0.008405
idea,0.020976
ideal,0.012164
illus,0.009229
im,0.009879
imag,0.005755
imit,0.008843
implement,0.015612
includ,0.023353
incomplet,0.008145
independ,0.003839
india,0.005722
indiana,0.066092
infinit,0.007319
influenc,0.008234
initi,0.004061
inquiri,0.007455
insid,0.006269
inspir,0.012369
intellig,0.034994
intens,0.006208
interact,0.004663
intern,0.003275
interview,0.015458
introduc,0.004384
introduct,0.004955
invent,0.012164
invit,0.007703
involv,0.010871
irrat,0.009628
isbn,0.083957
ise,0.012657
issu,0.004011
itali,0.006726
italian,0.021041
iter,0.009354
j,0.004694
jackson,0.009669
jame,0.015815
jane,0.009852
jason,0.011449
jastrow,0.013610
jauch,0.017844
jersey,0.009256
jim,0.009879
jm,0.011676
john,0.017006
jokingli,0.014324
jone,0.008299
joseph,0.006159
joy,0.010133
just,0.004358
kanerva,0.017844
kasparov,0.014569
keith,0.009404
kelli,0.010703
kevin,0.009807
key,0.004756
kind,0.004978
king,0.005953
known,0.003123
kokinov,0.017844
koza,0.017141
kurzweil,0.012365
la,0.018715
label,0.006724
lanalogi,0.018910
languag,0.061065
laradamm,0.018910
laradammermodel,0.018910
larg,0.009880
larger,0.004932
later,0.003716
launch,0.006293
laura,0.011489
law,0.018823
le,0.022333
learn,0.005249
lectur,0.013590
led,0.004033
legaci,0.007857
legal,0.010313
leitmotif,0.016617
letter,0.012337
level,0.014947
levelcross,0.018910
lever,0.011227
life,0.024103
like,0.003337
likelihood,0.008666
limit,0.011449
lin,0.011742
link,0.002717
lipogram,0.017463
list,0.003474
listen,0.008826
literari,0.007836
literatur,0.011166
live,0.004040
lo,0.017141
londonnew,0.016863
long,0.004057
longer,0.005153
longtim,0.010649
look,0.005159
loop,0.068842
love,0.007418
lowerlevel,0.013304
lugowski,0.018910
m,0.008938
ma,0.015786
machin,0.006033
mad,0.030558
magazin,0.007234
magnet,0.014671
mahab,0.018910
mahabalseqse,0.018910
make,0.006355
man,0.011057
mandarin,0.012996
mani,0.022595
marek,0.013167
marot,0.083086
marri,0.023500
marsha,0.015057
marshal,0.008282
marshallmetacat,0.018910
martin,0.006614
master,0.006668
masteri,0.011263
mathemat,0.034386
mathur,0.016397
mbiu,0.013123
mcgraw,0.011765
mcgrawlett,0.018910
mean,0.003441
mechan,0.009229
melani,0.026904
member,0.011960
memori,0.013000
mental,0.013523
mention,0.006049
meredithseekwh,0.018910
merkl,0.016198
met,0.012780
meta,0.012219
metacat,0.018910
metamag,0.081986
michigan,0.036072
microcosmo,0.016397
microdomain,0.016863
mignonn,0.018910
mind,0.041249
minim,0.006595
mit,0.007592
mitchel,0.010471
mitchellcopycat,0.018910
model,0.070062
moder,0.007158
modifi,0.006097
molli,0.027938
moment,0.007109
mommi,0.017463
monica,0.012955
moravec,0.013257
moser,0.013211
mot,0.015551
mother,0.007201
motiv,0.006104
movi,0.008546
multiform,0.014174
muse,0.011549
music,0.041205
musicinspir,0.018910
nagel,0.012622
nanci,0.010583
nation,0.003462
necker,0.014569
neighbor,0.007049
nerd,0.015849
neuron,0.009130
new,0.002791
newman,0.011549
nf,0.013452
nicholsmusicat,0.018910
nj,0.009169
nobel,0.014547
nomic,0.016397
nomin,0.006522
nondeterminist,0.012426
nonfict,0.010903
notat,0.008396
note,0.003602
noth,0.006612
notion,0.005911
novel,0.027663
novelinvers,0.018910
novemb,0.005604
number,0.006252
numer,0.009258
observ,0.004124
obtain,0.004856
odyssey,0.022022
oligoglot,0.018910
onegin,0.029697
oneself,0.009780
onli,0.002876
onlin,0.005469
onset,0.009612
optic,0.007517
order,0.003443
oregon,0.010636
organ,0.017041
origin,0.003462
oscil,0.009042
outlin,0.006082
outsid,0.004766
overal,0.005719
overarch,0.010571
page,0.006125
panel,0.009209
panelist,0.016016
paper,0.020623
paperback,0.010520
paradox,0.015982
parallel,0.012949
particip,0.004929
particular,0.003911
particularli,0.004382
partito,0.017141
partyadversari,0.018910
pass,0.004854
passion,0.009382
pattern,0.031988
pei,0.026515
pens,0.014248
pentti,0.015695
percept,0.059860
perform,0.008785
person,0.008177
perspect,0.005591
pervas,0.019650
peter,0.005697
phaeaco,0.018910
phd,0.016149
phenomena,0.005966
phi,0.010229
philosoph,0.016454
philosophi,0.021280
phrase,0.007529
physic,0.012266
physicist,0.007396
pianist,0.013969
piano,0.037096
piec,0.013800
piet,0.013969
pilingu,0.018910
pit,0.009898
platonia,0.017141
pleas,0.009464
pleasantli,0.037821
poem,0.018445
poet,0.017281
poetri,0.017436
point,0.003624
polish,0.008442
popular,0.004470
possibl,0.003654
postscript,0.027007
power,0.003666
pp,0.017524
prais,0.025135
precept,0.011210
precis,0.005902
predict,0.005242
prefac,0.020568
present,0.003582
presidenti,0.007327
press,0.008434
pressbradford,0.018910
presum,0.007998
primarili,0.004897
principl,0.004301
prison,0.007604
prize,0.013365
prizewin,0.011611
problem,0.007664
proceed,0.006557
process,0.010162
product,0.003734
profession,0.005806
professor,0.044827
profil,0.007735
profound,0.008173
program,0.004507
project,0.017734
promis,0.006733
proof,0.007305
prose,0.010133
prototyp,0.009242
provok,0.009169
pseudonym,0.011611
psycholog,0.012215
publ,0.013610
public,0.010825
publish,0.047725
pulitz,0.012458
puriti,0.010144
pursuit,0.008078
push,0.006813
pushkin,0.028969
quanta,0.012005
quarterli,0.008760
quip,0.013782
r,0.004794
rabbitduck,0.018910
racism,0.010533
racist,0.011654
radic,0.006568
ralph,0.009636
random,0.006835
rang,0.004301
rapidli,0.006040
ration,0.006175
ray,0.007762
reach,0.004618
read,0.007893
reader,0.007749
real,0.004716
realiz,0.006354
reason,0.008157
receiv,0.008683
recent,0.003935
refer,0.004544
reflect,0.014650
regul,0.005379
rehl,0.018910
rehlinglett,0.018910
relat,0.006128
render,0.007452
repli,0.009347
report,0.004367
research,0.025806
reson,0.017394
respect,0.008643
retir,0.007732
return,0.004599
rev,0.010154
review,0.030550
revis,0.006790
revuls,0.028969
rhapsodi,0.015170
richard,0.005614
rid,0.010964
robert,0.019986
robot,0.008423
rotat,0.007523
roughli,0.006374
row,0.008787
royal,0.005996
runaway,0.011881
russian,0.013306
safir,0.017141
sagan,0.012426
said,0.014007
salavon,0.018910
sander,0.010318
satir,0.021116
save,0.006453
scholarship,0.008125
school,0.004623
scienc,0.068796
scientif,0.022864
scope,0.006680
scoperta,0.017844
seckel,0.017844
seek,0.005099
seen,0.004458
seeth,0.017463
select,0.004941
self,0.015619
selfmodifi,0.016198
selfrefer,0.013723
selfreferenti,0.014035
selfreinforc,0.015849
selfwatch,0.018910
seminar,0.010329
sens,0.014027
septemb,0.005480
seqse,0.018910
sequel,0.011158
sequenc,0.012617
seri,0.004527
server,0.010744
set,0.003520
sever,0.016485
sexism,0.012916
sexist,0.030115
shape,0.005416
share,0.008856
short,0.004845
similar,0.003776
simpli,0.005160
simultan,0.006627
sinc,0.009487
singular,0.025002
siobhan,0.017844
sister,0.026186
site,0.005327
skeptic,0.016659
slippag,0.013905
small,0.003870
social,0.004023
societi,0.007924
sold,0.006678
sole,0.006419
someon,0.014775
somer,0.014174
someth,0.006080
sometim,0.004117
son,0.006055
sonic,0.012083
sort,0.006633
soul,0.016331
sound,0.006658
space,0.009752
spanish,0.006839
spark,0.008369
spars,0.009533
speak,0.018193
speech,0.006813
speechstuff,0.018910
spirit,0.021438
spiritu,0.007287
spoken,0.008507
spontan,0.008173
spread,0.005590
stanford,0.044078
state,0.002767
stochast,0.008777
strang,0.046914
strategi,0.005891
stress,0.006485
strict,0.007349
strive,0.009067
structur,0.007390
student,0.043064
studi,0.020054
studio,0.010888
sture,0.017844
stylist,0.011299
suber,0.014848
sublimin,0.013723
subsequ,0.004935
succeed,0.006465
sudden,0.009054
suggest,0.004427
sum,0.006308
summit,0.009249
superration,0.018910
surfac,0.005726
sweden,0.007900
swedish,0.008625
symbol,0.005741
symposium,0.030369
tabletop,0.014751
taken,0.004630
talk,0.006805
tanner,0.014035
teach,0.024782
teaneck,0.018910
technolog,0.018239
temperatur,0.006393
tend,0.005193
tension,0.007183
term,0.008965
teuscher,0.018910
thcenturi,0.007292
thema,0.074752
theme,0.014008
theorem,0.007089
theori,0.014828
thesi,0.007826
thi,0.007096
think,0.016454
thinker,0.007645
thought,0.008964
thoughtstuff,0.018910
thousand,0.005577
time,0.013991
titl,0.011353
togeth,0.004361
ton,0.025726
tongu,0.010571
took,0.004940
topic,0.010035
total,0.004412
tournament,0.023396
trader,0.015571
translat,0.065336
triangl,0.009354
tude,0.013782
tumor,0.010154
ture,0.019129
twice,0.007538
typefac,0.031390
typic,0.004242
uncomfort,0.011834
underli,0.006006
understand,0.017171
uniform,0.007332
univers,0.052409
unpromis,0.016863
uppsala,0.012838
use,0.015505
usual,0.003805
valu,0.004053
vari,0.004662
variou,0.010801
vegetarian,0.012247
veltroni,0.034927
vers,0.009311
versif,0.015289
victim,0.008498
video,0.006960
vision,0.006850
visual,0.033363
voic,0.007538
vol,0.013594
volum,0.005417
wa,0.049270
walgreen,0.016016
walker,0.009964
walter,0.015088
wang,0.010240
wangnonaxiomat,0.018910
watersh,0.010165
wave,0.006378
way,0.003381
whirli,0.018910
william,0.015203
won,0.012804
word,0.008780
work,0.018602
world,0.019238
write,0.014834
writer,0.012981
written,0.015021
wrote,0.021553
x,0.005599
xxviii,0.015849
year,0.003191
york,0.004871
yorkaesthet,0.018910
young,0.005872
younger,0.007857
