abil,0.007734
accolad,0.021460
ace,0.018397
acquir,0.009155
action,0.013813
actor,0.025915
actual,0.007194
addit,0.011373
advocaci,0.015213
agent,0.019168
aggreg,0.011887
ahead,0.013122
ai,0.056485
aim,0.008026
albuquerqu,0.020323
alcon,0.136545
alreadi,0.016378
amateurish,0.025985
amazoncom,0.020521
ambiti,0.014550
america,0.015755
american,0.006368
anamorph,0.023546
angel,0.013480
ani,0.004954
anni,0.018936
antitechnolog,0.024785
april,0.034398
artifici,0.018984
artilleri,0.015364
aspect,0.007387
atkinson,0.019152
attack,0.018126
attempt,0.006411
australia,0.009770
austria,0.013005
averag,0.025188
awar,0.010170
award,0.019819
banal,0.023546
base,0.014693
basic,0.006818
beauti,0.012878
becaus,0.005102
befor,0.005788
began,0.006768
belen,0.024785
believ,0.007010
benefit,0.008225
berkeley,0.013237
best,0.014940
bettani,0.107223
bigger,0.015213
biggest,0.012725
biolog,0.008038
black,0.009147
blackout,0.040273
blame,0.013464
blind,0.013872
bloom,0.016801
bluray,0.022166
bmovi,0.027904
bob,0.015766
bodi,0.020404
bold,0.016653
box,0.038119
bree,0.083712
brightwood,0.059143
bro,0.021116
buchanan,0.037160
budget,0.010609
build,0.007172
bullet,0.016758
busi,0.007640
cage,0.017697
canada,0.009723
capabl,0.017878
carri,0.014636
cast,0.069186
caster,0.072322
cd,0.015626
center,0.007240
charact,0.009380
cheesi,0.029571
chicago,0.011063
china,0.034886
chines,0.029274
choic,0.008919
chose,0.012769
christoph,0.035691
cillian,0.085897
cinematograph,0.071728
cinematographi,0.022522
civil,0.007787
claim,0.013782
cleans,0.017814
clifton,0.021948
closer,0.011233
code,0.008784
cole,0.031596
collabor,0.020856
collaps,0.010000
collin,0.016030
come,0.006493
compani,0.007169
complet,0.006270
compos,0.008613
compromis,0.013053
comput,0.045139
conclud,0.009285
connect,0.022426
conscious,0.046263
consensu,0.011325
consid,0.015885
continu,0.005501
contrari,0.011873
contribut,0.006664
control,0.006032
coproduct,0.021744
cori,0.021283
countri,0.005975
crater,0.017074
creat,0.027768
critic,0.060108
d,0.022051
damag,0.009798
damn,0.019432
danna,0.027309
databas,0.010622
david,0.008065
day,0.006851
death,0.007952
debut,0.054182
defend,0.010158
delet,0.016261
demand,0.008106
denbi,0.029571
depp,0.131849
desert,0.012911
desper,0.016047
despit,0.007699
destroy,0.028484
destruct,0.010746
develop,0.022704
dialogu,0.012675
did,0.033700
die,0.017399
digit,0.030417
direct,0.012058
director,0.032751
directori,0.045722
disabl,0.013223
disappoint,0.015581
diseas,0.009511
dismiss,0.012096
display,0.010319
distinct,0.006993
distribut,0.022475
dmg,0.093516
donald,0.025601
downtown,0.018978
dr,0.011265
drama,0.015336
drop,0.009965
dvd,0.018060
dystopian,0.041760
ecosystem,0.012398
edmund,0.013908
effect,0.005310
element,0.006952
emma,0.018813
empir,0.007627
end,0.005976
endang,0.015187
energi,0.007652
engin,0.007877
englishlanguag,0.017966
ensu,0.013364
ensur,0.008980
enter,0.015983
entertain,0.094087
entiti,0.009275
erad,0.015895
evelyn,0.321920
eventu,0.007769
everyth,0.010885
examin,0.008555
exceed,0.011479
execut,0.016561
experi,0.006722
explain,0.014717
extern,0.004274
failur,0.019022
fairli,0.012381
falk,0.022055
fall,0.007461
far,0.007412
faraday,0.016653
fashion,0.011505
fatal,0.014208
fbi,0.058594
fear,0.010257
fellow,0.024718
femal,0.021950
fiction,0.058295
fight,0.010257
film,0.554328
financ,0.034647
finish,0.012578
flatter,0.020076
flight,0.011975
floyd,0.019336
follow,0.014027
forb,0.015814
form,0.008935
format,0.023917
franc,0.008368
freeman,0.058510
friday,0.016912
friend,0.023219
fund,0.008059
futurist,0.036186
garden,0.052295
genet,0.009928
germani,0.008801
given,0.005862
global,0.007762
gloss,0.020260
goe,0.010519
got,0.013187
govern,0.016675
grasp,0.014583
great,0.006587
gross,0.068572
groundbreak,0.016653
group,0.010545
grow,0.014990
guardian,0.028885
ha,0.023385
hairi,0.022166
hall,0.044361
hardrict,0.029571
harsh,0.013908
hauser,0.044332
heal,0.028657
heartbreak,0.025046
help,0.019197
hentschel,0.025046
hi,0.073375
hipster,0.025985
hole,0.013304
hollywood,0.037626
home,0.016693
hong,0.013666
howev,0.009487
human,0.016894
idea,0.013120
ideasdriven,0.029571
ident,0.008369
identifi,0.007066
imag,0.009000
imax,0.051283
imit,0.013828
includ,0.008115
independ,0.018011
indic,0.007190
infect,0.013267
influenc,0.006438
initi,0.006351
instantli,0.034952
instead,0.013692
intellig,0.036481
intend,0.009048
intermedi,0.011488
intern,0.005122
internet,0.028766
ireland,0.012217
iron,0.010468
itali,0.010519
jack,0.027639
jame,0.008244
joel,0.017074
johnni,0.082927
join,0.034777
joseph,0.019264
josh,0.021370
jr,0.012415
juli,0.008442
june,0.017031
justic,0.009878
k,0.008862
kate,0.075253
kenneth,0.013115
kermod,0.026369
kick,0.018191
kidnap,0.016591
kill,0.019298
kingdom,0.007774
knowledg,0.007351
known,0.004883
kong,0.013785
korea,0.011797
laboratori,0.009917
larg,0.015450
later,0.005811
laud,0.018773
lead,0.017492
leader,0.017090
leap,0.015551
leav,0.008275
like,0.010439
link,0.004249
lionsgat,0.028632
list,0.016301
live,0.006318
lo,0.013402
locat,0.007185
logic,0.018915
looper,0.026369
maguir,0.022783
main,0.006186
mainli,0.008112
major,0.005184
male,0.010960
mammoth,0.020260
man,0.017291
mani,0.004416
mara,0.067740
march,0.016206
mark,0.007845
market,0.014628
marter,0.029571
martin,0.010343
master,0.010428
max,0.119410
mcavoy,0.029571
media,0.008962
medicin,0.009316
member,0.006234
mentor,0.016135
met,0.019985
metacrit,0.045567
mexico,0.024381
michael,0.008992
militari,0.007904
million,0.061400
mind,0.018429
mindexpand,0.029571
mix,0.018797
mm,0.014298
mojo,0.024318
moment,0.011116
monologu,0.022922
month,0.017266
morgan,0.055490
mortal,0.013060
motiv,0.009546
movi,0.066821
murphi,0.052349
music,0.032217
mychael,0.029571
nanomachin,0.024107
nanoparticl,0.062641
nanotechnolog,0.015846
narr,0.012858
natur,0.005216
neg,0.025419
negoti,0.010227
network,0.008168
new,0.017458
newfound,0.018936
newli,0.010280
nigel,0.018505
nolan,0.062415
nonstart,0.027904
noomi,0.029571
north,0.015517
note,0.005633
notic,0.022180
nuclear,0.009984
number,0.004888
octob,0.008449
offer,0.007266
offic,0.031460
offici,0.007418
oil,0.009753
old,0.024255
onli,0.017991
open,0.006689
organ,0.010659
origin,0.005415
paglen,0.088714
paranoia,0.021370
partial,0.008988
particular,0.006116
partner,0.010682
partnership,0.012298
path,0.009907
paul,0.042468
payday,0.024318
peopl,0.005387
percent,0.009813
period,0.005805
person,0.006393
persuad,0.014121
petal,0.021552
pfister,0.191276
photochem,0.019738
physic,0.006393
physicist,0.011565
piec,0.010790
pitch,0.033389
place,0.005526
plan,0.029600
plant,0.008483
play,0.007055
plot,0.047902
pollut,0.013144
poloniumlac,0.029571
poor,0.019322
popular,0.006990
posit,0.005791
possibl,0.011430
postproduct,0.024107
potenti,0.007254
power,0.011468
prais,0.013101
predict,0.008198
premis,0.013144
prescienc,0.026805
present,0.005602
pretens,0.038042
primarili,0.007659
produc,0.040810
product,0.005839
project,0.020799
promis,0.010528
protect,0.007467
provid,0.004971
public,0.005643
puddl,0.022399
purpos,0.007288
pursu,0.010190
qualiti,0.008348
quantum,0.011301
question,0.014072
quit,0.009469
rank,0.009460
rapac,0.025985
rate,0.020831
read,0.006171
real,0.007375
realiz,0.009936
reason,0.012755
rebecca,0.078126
receiv,0.020367
recept,0.042192
refer,0.007107
refus,0.010370
releas,0.079930
remain,0.005877
remot,0.024061
repair,0.012651
report,0.013660
request,0.010800
research,0.040355
resolut,0.010870
respect,0.006758
review,0.063698
revolutionari,0.022618
rhythmless,0.029571
richard,0.008778
rift,0.114222
right,0.006376
risibl,0.027904
roeper,0.028632
rogerebertcom,0.028632
role,0.018675
rotten,0.040776
russel,0.012455
s,0.005040
said,0.014602
salari,0.013144
sampl,0.010589
satellit,0.012398
saturday,0.017755
save,0.020182
schedul,0.023622
scienc,0.017930
scientist,0.040629
scifi,0.020880
score,0.039077
scout,0.018397
screenplay,0.064940
script,0.028016
sentienc,0.020590
sentient,0.050805
seri,0.007079
serv,0.007073
shapeless,0.024785
shoot,0.030139
sight,0.013187
singular,0.013032
site,0.008330
slightli,0.010718
slog,0.026805
sold,0.010443
solut,0.008461
sometim,0.006438
sound,0.010413
sourc,0.018662
south,0.007744
spearhead,0.017269
speci,0.009095
specul,0.010289
spread,0.026225
star,0.043871
start,0.006260
state,0.008654
steven,0.012461
stewart,0.015703
stock,0.009541
stop,0.019541
storytel,0.017997
straight,0.041149
structur,0.005778
stuart,0.013716
stun,0.037707
stylish,0.024107
stylist,0.017669
subject,0.006291
succeed,0.010110
summit,0.014464
sunflow,0.040153
suntim,0.027904
superhuman,0.039920
suppli,0.008062
support,0.005880
surviv,0.008529
suspici,0.016890
synchron,0.016243
tafoya,0.029571
tagger,0.050092
team,0.010328
technic,0.008795
technolog,0.071306
tell,0.022279
territori,0.025969
terrorist,0.074845
theater,0.031958
theme,0.010953
theyv,0.020660
thi,0.014796
think,0.008576
thoma,0.017693
thought,0.007009
thoughtprovok,0.023722
threaten,0.011245
thriller,0.020590
time,0.013127
timid,0.021948
tobey,0.027904
togeth,0.013639
tomato,0.033915
took,0.007725
topic,0.007846
total,0.006900
town,0.020312
tradit,0.006227
traffic,0.013067
transcend,0.237828
transhuman,0.020136
tread,0.021948
troublesom,0.018327
turan,0.024785
turkey,0.012898
uk,0.009065
underappreci,0.024318
unit,0.009976
univers,0.005122
unless,0.011082
unproduc,0.025985
upload,0.171975
use,0.012123
usual,0.005951
utmost,0.018733
utopia,0.017423
utopian,0.017194
varieti,0.007425
vast,0.019862
veri,0.005647
version,0.008651
versu,0.011544
virtual,0.009780
viru,0.120559
visual,0.010434
wa,0.081101
walli,0.053093
waltz,0.022399
warner,0.018224
wast,0.011863
water,0.030515
watertow,0.029571
weekend,0.036654
welcom,0.013649
welljudg,0.027904
went,0.009071
wife,0.025500
william,0.007924
wind,0.011750
wisdom,0.013094
word,0.006865
work,0.029090
world,0.005014
worldwid,0.020066
wound,0.013567
write,0.007732
written,0.007830
wrote,0.008426
year,0.004991
yorker,0.020018
zealand,0.011759
