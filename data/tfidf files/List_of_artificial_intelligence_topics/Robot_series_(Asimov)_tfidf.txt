abandon,0.008578
absenc,0.008442
acknowledg,0.008855
adapt,0.021938
age,0.023005
alexand,0.008798
alien,0.010761
allen,0.010602
allencompass,0.015684
ancestor,0.009870
anderson,0.011968
andrew,0.009326
ani,0.004047
ann,0.010756
announc,0.007712
anoth,0.022085
antholog,0.078246
appear,0.015957
approv,0.007889
asimov,0.412377
asimovian,0.024159
audley,0.022797
aurora,0.014889
author,0.031024
award,0.016192
balanc,0.007268
baley,0.159580
barrett,0.015059
base,0.024009
basi,0.011440
bbc,0.020895
beatrix,0.019695
becam,0.005061
becaus,0.008336
becom,0.008865
befor,0.023646
began,0.005529
begin,0.005399
benevol,0.013762
best,0.006103
bicentenni,0.017121
blot,0.016126
book,0.031577
brain,0.008861
brent,0.016260
bridget,0.020051
british,0.006182
bro,0.017251
broadcast,0.010744
c,0.005299
caliban,0.020694
calvin,0.096721
canon,0.010070
carhunt,0.024159
carson,0.015470
cave,0.066909
centuri,0.009524
chao,0.010732
charact,0.022992
chimera,0.017388
chose,0.010432
citi,0.006405
civil,0.006362
classic,0.006127
clement,0.013315
clown,0.018203
cold,0.008841
coll,0.017685
collect,0.010820
complet,0.015369
compli,0.012229
conceiv,0.009739
concret,0.010162
conflict,0.013817
connect,0.012214
consid,0.012978
consist,0.004964
contain,0.005321
creat,0.004537
creator,0.011117
cush,0.017931
dale,0.014213
daneel,0.107719
databas,0.008678
david,0.006589
dawn,0.035219
daybyday,0.021899
death,0.006496
decenc,0.018109
descend,0.008978
differ,0.003932
direct,0.004925
directli,0.005960
distort,0.010473
doe,0.010038
doubleday,0.015721
dr,0.027610
dramatis,0.037228
dream,0.010709
dure,0.008540
dust,0.012110
earli,0.008933
earlier,0.006599
earth,0.042068
earthmen,0.024159
edit,0.006989
element,0.005679
elijah,0.084688
ellison,0.034117
empir,0.068548
encompass,0.008360
ensur,0.007337
entir,0.005792
entitl,0.009219
episod,0.043874
era,0.007056
especi,0.005185
estat,0.019014
event,0.005811
evit,0.021899
expand,0.006317
expans,0.007535
explain,0.006012
explicitli,0.009091
explor,0.006804
extern,0.003492
fall,0.006096
fantasi,0.012645
featur,0.048318
feel,0.008348
fiction,0.057151
film,0.036230
final,0.005776
finch,0.016307
firbank,0.024159
focu,0.006551
follow,0.011460
form,0.010950
format,0.006513
formul,0.007647
foundat,0.049818
fourth,0.008332
fox,0.012301
friend,0.009485
futur,0.005816
galact,0.028341
galaxi,0.011887
gener,0.006863
genr,0.011276
gifford,0.017121
golden,0.009676
got,0.010773
govern,0.004541
guarante,0.017519
ha,0.003184
habit,0.010125
hal,0.015089
hardcov,0.015540
hardwir,0.036218
harlan,0.016937
harri,0.010043
harrison,0.013639
hi,0.025691
histori,0.003977
howev,0.007751
hugo,0.012159
human,0.009201
humaniform,0.023392
humanoid,0.015210
illustr,0.008144
imag,0.007353
includ,0.009945
inconsist,0.022195
inferno,0.035065
ing,0.016451
initi,0.005188
inspir,0.023703
instrument,0.007353
integr,0.005762
interact,0.005958
interconnect,0.011544
internet,0.007834
interview,0.009874
inunivers,0.021899
irvin,0.015797
isaac,0.020671
jameson,0.049811
jeff,0.014191
jensen,0.014835
john,0.005431
jone,0.010602
jupit,0.013674
known,0.003990
late,0.005527
later,0.033238
law,0.028858
learn,0.006706
lehman,0.016040
lehmann,0.017388
leo,0.011950
let,0.008945
liar,0.032521
lije,0.024159
like,0.004264
link,0.003471
list,0.004439
littl,0.012220
lock,0.010340
locu,0.025946
loos,0.009925
loss,0.006982
lost,0.013875
macbrid,0.020694
magnifico,0.024159
make,0.008119
man,0.021190
mani,0.007216
mark,0.006409
martin,0.008450
maxin,0.018400
maxwel,0.011905
mechan,0.005895
men,0.007402
mention,0.023186
merg,0.009615
mickey,0.018505
mike,0.013255
mirag,0.016402
mirror,0.010779
moral,0.007986
mostli,0.006811
mother,0.009200
motion,0.008055
movi,0.032755
moynahan,0.024159
multipl,0.006140
music,0.008773
mysteri,0.042150
nake,0.076453
natur,0.004261
neil,0.012681
new,0.003565
nomin,0.008332
nonhuman,0.012249
note,0.004602
novel,0.123698
novemb,0.014320
nuclear,0.008157
obey,0.022784
offer,0.005936
olivaw,0.107719
onli,0.003674
origin,0.017696
outlin,0.007770
overal,0.007306
overcrowd,0.015916
pamela,0.015575
pappi,0.024159
parad,0.015119
partner,0.008727
paul,0.006939
pebbl,0.015836
peopl,0.004401
percept,0.008497
period,0.004743
peter,0.007279
pictur,0.008936
place,0.004514
planet,0.009057
planetari,0.011444
plato,0.010516
play,0.005763
portray,0.020782
positron,0.122911
poul,0.018505
prairi,0.015836
prelud,0.015403
prequel,0.017459
present,0.004577
preserv,0.007715
previous,0.006978
probabl,0.006568
professor,0.008181
progress,0.006239
project,0.005664
prophet,0.024030
protect,0.006101
provid,0.004061
public,0.009220
publish,0.020324
r,0.036754
race,0.008993
radioact,0.022468
read,0.010084
reason,0.010421
receiv,0.005546
recent,0.005027
refer,0.002903
regard,0.005570
reichert,0.020948
renam,0.010079
resnick,0.017764
revolut,0.007192
robbi,0.018614
robert,0.006383
robin,0.012495
robopsychologist,0.024159
robot,0.721045
robotsinde,0.024159
roger,0.009735
room,0.009417
row,0.011227
s,0.008235
sargent,0.017251
satellit,0.020259
satisfact,0.024049
scienc,0.019531
screen,0.021442
screenplay,0.053055
script,0.022889
seen,0.005696
sens,0.005973
separ,0.005304
sequel,0.028512
seri,0.104106
serv,0.011558
set,0.049470
settl,0.008251
settler,0.010924
sever,0.008424
share,0.005657
sheckley,0.022797
sheila,0.017388
short,0.068092
shortli,0.009036
shortlist,0.019695
sky,0.011490
smith,0.008560
sometim,0.010519
sourc,0.010164
space,0.006229
spacer,0.076948
spec,0.018400
specul,0.008406
spiritu,0.009309
spooner,0.018400
star,0.053763
start,0.005114
state,0.003535
stcenturi,0.015241
steel,0.050602
stephen,0.008975
stori,0.196570
storylin,0.016451
striprunn,0.024159
suitabl,0.008969
sun,0.053781
surviv,0.006968
susan,0.086478
suspect,0.010797
televis,0.027322
tell,0.009101
terraform,0.018203
terran,0.019100
theatric,0.014889
theme,0.008948
thi,0.027199
thousand,0.021376
tiedemann,0.042459
time,0.007149
timelin,0.019021
titl,0.014505
togeth,0.011143
told,0.010247
trantor,0.022311
travel,0.007134
trilog,0.056683
turn,0.005585
twenti,0.009938
twentieth,0.009532
ultim,0.006988
unifi,0.008457
unintent,0.014256
uniqu,0.006889
univers,0.016739
unknown,0.008365
upcom,0.027821
utopia,0.014234
variou,0.004599
victori,0.009357
vintar,0.023392
visisonor,0.024159
volum,0.006921
w,0.006779
wa,0.056320
war,0.011479
warner,0.014889
way,0.004320
wendi,0.015957
william,0.006474
word,0.005608
work,0.007922
world,0.020482
woven,0.029670
write,0.006317
written,0.006397
wrote,0.006884
year,0.024466
zorom,0.072478
zucker,0.020694
