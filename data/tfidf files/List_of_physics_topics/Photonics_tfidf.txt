abil,0.006827
achiev,0.006242
activ,0.009902
addit,0.010040
adopt,0.006847
advanc,0.030643
agricultur,0.007375
albert,0.010085
algaa,0.026104
allow,0.009869
aluminium,0.013904
amplif,0.046211
amplifi,0.149882
analog,0.008866
ani,0.004373
anoth,0.004772
appear,0.005747
appli,0.010609
applic,0.071113
approxim,0.006762
archiv,0.009133
area,0.019713
arsenid,0.039337
art,0.015182
associ,0.004847
atom,0.008676
avail,0.006065
aviat,0.013054
backlight,0.024107
bandwidth,0.016401
barcod,0.018176
base,0.017295
basi,0.006180
beam,0.013526
began,0.005974
bell,0.011584
biolog,0.007096
biophoton,0.022939
bit,0.012168
block,0.008913
board,0.008631
bulb,0.015725
bulk,0.010606
came,0.013103
camera,0.012952
carrier,0.012034
case,0.005010
cathod,0.033359
ccd,0.018432
cddvdbluray,0.026104
cell,0.017236
centuri,0.015437
characterist,0.006907
charg,0.007733
chemic,0.022891
circa,0.013465
circuit,0.056529
civilian,0.010418
classic,0.026484
clock,0.012542
close,0.005362
code,0.007754
coin,0.008298
cold,0.009553
command,0.008408
commerci,0.007678
common,0.004846
commonli,0.013311
commun,0.052406
compon,0.006902
compound,0.009031
compris,0.008199
comput,0.033206
confirm,0.008640
conjunct,0.011200
connot,0.066235
consist,0.005364
consortium,0.014032
construct,0.006206
consum,0.007980
continu,0.004856
control,0.010650
convert,0.008170
correct,0.016142
cost,0.014093
counteract,0.015403
coupl,0.009274
cover,0.019797
crash,0.011920
creat,0.004902
crt,0.039764
crystal,0.056221
current,0.005131
cut,0.008509
data,0.019190
defens,0.008797
degrad,0.011498
depend,0.010577
deploy,0.010847
deriv,0.006094
describ,0.004933
desir,0.007543
detect,0.052812
detectionsens,0.026104
develop,0.032068
devic,0.116452
diagnost,0.025669
differ,0.012747
digit,0.008950
diod,0.067014
directli,0.006440
discoveri,0.007506
dispers,0.011000
display,0.036437
distanc,0.008535
distribut,0.006613
domain,0.007828
dot,0.012952
dotcom,0.016829
dramat,0.009051
drill,0.013280
dure,0.004613
earli,0.004826
earlier,0.014261
econom,0.005280
effect,0.018751
einstein,0.011634
electr,0.007979
electricalopt,0.026104
electron,0.032300
electroopt,0.059646
emerg,0.024738
emiss,0.022901
emit,0.012078
emitt,0.017520
employ,0.006400
encod,0.011750
encompass,0.009033
end,0.010552
endoscopi,0.039337
energi,0.013510
engin,0.006954
enterpris,0.009083
entertain,0.011865
equat,0.016051
equip,0.008627
erbiumdop,0.049265
establish,0.005149
europ,0.006543
european,0.006010
everyday,0.010308
exampl,0.012965
expand,0.006826
explain,0.006496
extern,0.003773
eyesight,0.017520
fabric,0.012071
famous,0.012534
far,0.006543
fast,0.009878
fell,0.009319
fiber,0.125341
fiberopt,0.019025
field,0.036888
flashlight,0.021879
fluoresc,0.028122
focus,0.013434
follow,0.004127
form,0.003944
format,0.028151
frequenc,0.019722
frequencydivis,0.023663
frequent,0.007253
function,0.015849
fundament,0.013215
fusion,0.012027
futur,0.006284
gaa,0.019882
gain,0.006277
gallium,0.033658
gener,0.007415
germanium,0.017424
gigahertz,0.021879
glass,0.011374
goal,0.007097
govern,0.009813
grate,0.017154
greek,0.007306
growth,0.006531
guid,0.007295
gyroscop,0.018238
ha,0.006881
harvest,0.011438
health,0.007193
high,0.005224
highpow,0.019567
histori,0.004297
holograph,0.018056
holographi,0.022360
howev,0.008375
huge,0.009596
huygen,0.015752
hybrid,0.010780
ieee,0.014061
iiiv,0.043758
imag,0.007945
implement,0.014368
import,0.004400
improv,0.012699
includ,0.046565
increas,0.009923
indium,0.019283
industri,0.017669
inform,0.035909
infrastructur,0.008736
instead,0.006043
instrument,0.007945
integr,0.031134
interact,0.012876
internet,0.008464
invent,0.033584
investig,0.021691
involv,0.005002
ir,0.014421
journal,0.006636
just,0.006016
key,0.019698
km,0.009711
laboratori,0.008755
lack,0.006578
lamp,0.029080
larg,0.004546
laser,0.205511
late,0.011945
lay,0.009707
lcd,0.019469
lead,0.005147
led,0.011135
len,0.013085
letter,0.008515
level,0.005158
life,0.005545
light,0.206249
lightemit,0.040225
like,0.032254
liquid,0.009247
long,0.005601
macroscop,0.012357
mani,0.003898
manipul,0.009235
manufactur,0.016452
marketplac,0.013638
mast,0.016907
materi,0.024178
maxwel,0.012863
mean,0.004750
measur,0.005649
mechan,0.006370
media,0.007912
medic,0.015887
medicin,0.016448
medium,0.019667
mesoscop,0.021467
metamateri,0.019882
method,0.016026
metrolog,0.055922
microphoton,0.026104
microwav,0.014166
militari,0.013956
mirror,0.023294
mixtur,0.010785
mobil,0.009162
modern,0.005173
modif,0.010252
modul,0.151415
monitor,0.009328
mors,0.015725
multiplex,0.017829
nanoopt,0.024632
nanophoton,0.023278
navig,0.021542
nearinfrar,0.019375
network,0.014422
nonlinear,0.011817
note,0.004973
object,0.005896
onchip,0.022939
onoff,0.019773
oper,0.010883
optic,0.415077
optoatom,0.026104
optoelectron,0.059646
optomechan,0.052209
optronicsoptoelectron,0.026104
organ,0.014115
orthogon,0.014610
outgrowth,0.015832
overview,0.008643
parametr,0.015072
particl,0.009272
path,0.008745
perform,0.012127
period,0.005125
phaseshift,0.024107
phonon,0.018640
phosphid,0.021106
photo,0.013348
photodetector,0.070989
photodiod,0.020638
photoelectr,0.032001
photon,0.668022
physic,0.005644
pic,0.038750
plasma,0.025279
plastic,0.011596
pockel,0.025275
polariton,0.049265
poor,0.008528
potenti,0.012807
practic,0.010584
preced,0.009371
precis,0.008147
principl,0.005937
print,0.018577
printer,0.015357
process,0.028056
produc,0.005146
properti,0.023955
provid,0.008777
pump,0.012440
qualiti,0.014739
quantiz,0.014664
quantum,0.089787
raman,0.017520
rang,0.023750
rangefind,0.044220
rate,0.006129
ray,0.010715
record,0.006612
reduct,0.009026
refer,0.003136
reflect,0.006741
refract,0.014338
regener,0.013960
region,0.005617
relat,0.012688
relationship,0.005987
remot,0.010620
remov,0.007242
requir,0.004938
rescu,0.012448
research,0.066158
respons,0.005440
revolut,0.007771
robot,0.011628
s,0.026694
scanner,0.015943
scienc,0.026380
scientif,0.006312
screen,0.034753
search,0.008029
semiconductor,0.164274
send,0.009810
sens,0.006454
sensor,0.026005
sever,0.004551
signal,0.045130
signific,0.005573
silicon,0.066629
simplest,0.011135
sinc,0.004365
singl,0.005842
slightli,0.009462
slow,0.009013
smart,0.013715
societi,0.005469
solar,0.010414
solid,0.009382
sophist,0.010044
sourc,0.054916
special,0.010920
specif,0.010447
spectroscopi,0.013054
spectrum,0.010324
speed,0.017855
stakehold,0.014032
state,0.003820
structur,0.010201
studi,0.004613
submarin,0.013192
success,0.011579
sunlight,0.013613
superluminesc,0.026104
surfac,0.007905
surgeri,0.024618
surgic,0.013225
surveil,0.013781
switch,0.010719
synthesi,0.009747
tattoo,0.017424
technic,0.007764
technolog,0.031473
telecommun,0.079661
tenet,0.012854
terahertz,0.020785
term,0.028877
tft,0.026104
th,0.025030
therapi,0.011119
thermal,0.011194
thi,0.013061
thinfilm,0.021665
time,0.007725
timekeep,0.018788
today,0.006271
tool,0.006990
topic,0.020780
tradit,0.005497
transistor,0.015357
transmiss,0.064711
transmit,0.032090
transpar,0.011173
tube,0.012064
typic,0.011714
ubiquit,0.013768
uniqu,0.007444
unlimit,0.013064
use,0.082048
user,0.009685
usual,0.010507
variou,0.009940
veri,0.019943
vibrat,0.012740
virtual,0.008634
visibl,0.009650
vision,0.009456
visual,0.009211
voluntari,0.012078
wa,0.017898
wave,0.008804
weld,0.016272
wherea,0.007575
wide,0.011246
word,0.012120
xerographi,0.025275
year,0.004406
