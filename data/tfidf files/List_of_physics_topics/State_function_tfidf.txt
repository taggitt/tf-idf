abov,0.020894
academi,0.015720
acquir,0.013291
analog,0.014581
ani,0.014383
arriv,0.026272
asin,0.028140
axe,0.022325
b,0.021617
becaus,0.014814
best,0.010844
bodi,0.019747
break,0.013563
buxbk,0.042929
calcul,0.075338
callen,0.034710
case,0.016479
chang,0.022289
choic,0.012949
clausiu,0.029504
clear,0.024533
connecticut,0.024329
conserv,0.013083
consid,0.007687
continu,0.007986
contrast,0.011134
coordin,0.013972
current,0.008438
d,0.202743
defin,0.018091
delta,0.018880
denot,0.096502
depend,0.034788
describ,0.040565
descript,0.012049
determin,0.009153
differ,0.013975
differenti,0.040039
dimens,0.015166
dimension,0.020390
displaystyl,0.317080
dpdtdtint,0.042929
dphi,0.037723
dpvdtdtptvtptvt,0.042929
dure,0.015175
dvdtdtint,0.042929
dvtdtdt,0.042929
ed,0.011109
effect,0.007709
ellipsi,0.034997
end,0.008676
endpoint,0.027196
energi,0.066656
enthalpi,0.027083
entir,0.010293
entropi,0.043073
equal,0.010807
equat,0.013198
equilibrium,0.066341
equival,0.012782
exact,0.030278
exampl,0.078179
exhibit,0.014980
express,0.020564
extern,0.006205
f,0.023378
field,0.008666
fix,0.012595
fluid,0.033454
follow,0.006788
form,0.025945
fpvtldot,0.042929
function,0.199824
ga,0.028553
gener,0.012195
gibb,0.047823
given,0.025532
graph,0.018880
graphic,0.036504
ha,0.005658
heat,0.014963
herbert,0.018374
hi,0.007608
higherdimension,0.028354
histori,0.007067
hold,0.010221
hysteresi,0.032179
ideal,0.013807
identifi,0.010257
ii,0.011105
increment,0.021434
independ,0.017431
inexact,0.031154
infinitesim,0.022449
integr,0.071682
integrand,0.031863
intern,0.022308
introduct,0.011249
irrespect,0.024651
isbn,0.020062
josiah,0.024961
know,0.012856
knowledg,0.010672
label,0.015266
like,0.015155
likewis,0.016803
list,0.007888
locat,0.010430
loos,0.017636
mandl,0.036360
markov,0.024175
mechan,0.010475
method,0.017571
mode,0.015970
monatom,0.030424
n,0.012739
necessarili,0.013885
need,0.008527
nonholonom,0.039645
nonstat,0.029060
notat,0.019061
note,0.016357
number,0.028386
onli,0.013059
order,0.007817
overview,0.014213
p,0.184087
paper,0.011704
paramet,0.098123
particl,0.030496
particular,0.008879
path,0.172591
pdvint,0.042929
perform,0.009972
perhap,0.026373
permit,0.014448
peter,0.012935
phi,0.046444
physic,0.009282
plu,0.015959
point,0.008228
possibl,0.008296
postul,0.017501
present,0.008133
pressur,0.085712
process,0.015379
product,0.008477
properti,0.019697
proport,0.013449
pt,0.048472
ptvt,0.085859
pv,0.054055
quantit,0.015112
quantiti,0.079496
quit,0.013746
rankin,0.030538
refer,0.005158
relat,0.006955
repres,0.008462
reserv,0.013720
rudolf,0.021202
s,0.036583
second,0.008466
seen,0.010122
sens,0.010615
sever,0.007484
simpl,0.011407
simpli,0.011715
sinc,0.007179
son,0.027493
sort,0.015059
space,0.088555
specif,0.008590
specifi,0.058390
state,0.238719
statement,0.013180
statist,0.011779
suffic,0.025033
suppos,0.015776
surfac,0.013000
symbol,0.013034
t,0.624593
tait,0.030424
temperatur,0.072571
term,0.013568
therefor,0.019517
thermodynam,0.184283
thermostatist,0.035630
thi,0.016110
think,0.012451
thomson,0.022304
threedimension,0.039440
time,0.044467
tphi,0.034439
trace,0.027534
transact,0.015459
transfer,0.012286
transit,0.012499
ttfrac,0.033708
ttint,0.041566
ttpfrac,0.042929
ttptfrac,0.042929
ttvfrac,0.042929
twodimension,0.069373
u,0.016752
uniqu,0.024484
use,0.023466
v,0.230432
valu,0.036808
variabl,0.065623
vdp,0.039645
vector,0.017363
visual,0.015148
volum,0.073790
vt,0.057311
w,0.060233
wa,0.005886
way,0.007676
wiley,0.035137
willard,0.047653
william,0.023009
wish,0.031749
word,0.009966
work,0.056308
wpdv,0.042929
wttint,0.040509
