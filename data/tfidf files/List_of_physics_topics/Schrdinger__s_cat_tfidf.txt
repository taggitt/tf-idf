aapotapov,0.020045
abil,0.005242
abl,0.004813
absenc,0.007005
absolut,0.006463
absurd,0.021899
acceler,0.007328
accept,0.004783
accord,0.007620
account,0.008975
achiev,0.004793
acid,0.007677
act,0.004355
actual,0.009753
advanc,0.009412
aliv,0.114252
allow,0.007578
alon,0.006516
altern,0.004896
alway,0.005123
american,0.004317
amplifi,0.020926
analysi,0.004593
ani,0.010074
anim,0.005333
anoth,0.014659
answer,0.006784
anyth,0.007530
appar,0.013009
apparatu,0.047416
appear,0.013240
appli,0.008146
applic,0.004550
approach,0.008770
argu,0.005069
articl,0.013585
articlenam,0.020045
associ,0.003722
assumpt,0.006491
astronom,0.008579
atom,0.066629
audio,0.010000
august,0.011580
austrian,0.009127
author,0.004290
b,0.005047
bacterium,0.011672
bank,0.005510
becom,0.014712
befor,0.023544
begin,0.004480
behavior,0.005389
beryllium,0.013380
besid,0.007861
bibcodenw,0.020045
bigg,0.029615
billion,0.013052
bit,0.018688
blown,0.013274
blur,0.010698
bohr,0.011360
box,0.129200
bradi,0.013344
bring,0.005711
carri,0.004960
case,0.011542
cat,0.673576
catsiz,0.020045
chamber,0.016331
charg,0.011876
classic,0.020337
clear,0.005727
close,0.004117
cloud,0.008802
coin,0.006372
collaps,0.101687
combin,0.008776
commonli,0.005110
commun,0.008048
compar,0.004552
complet,0.004250
compris,0.006296
comput,0.005099
consciou,0.018201
conscious,0.007840
consid,0.014357
consist,0.016477
construct,0.009532
contain,0.008830
contemporari,0.006020
content,0.012146
contradictori,0.010866
contrari,0.008048
contrast,0.005199
controversi,0.006065
cool,0.008621
copenhagen,0.098437
correspond,0.027936
cosmologist,0.013492
counter,0.033288
cours,0.016092
creat,0.007529
creation,0.005602
criticis,0.009059
critiqu,0.007819
current,0.003940
dark,0.007959
date,0.005144
david,0.005467
dead,0.100310
deadandal,0.020045
deal,0.004958
death,0.005390
decay,0.059240
decoher,0.084925
defin,0.004223
definit,0.014599
demand,0.005494
deni,0.007135
depart,0.005396
depend,0.012183
der,0.008054
describ,0.015153
descript,0.022504
design,0.004520
destroy,0.012872
detector,0.021899
develop,0.006156
devic,0.013757
devis,0.008443
diagram,0.008466
did,0.009137
die,0.005897
differ,0.026102
direct,0.012261
discard,0.010294
discharg,0.010331
discuss,0.015066
displaystyl,0.007050
disput,0.006190
distanc,0.006554
distinct,0.004740
distinguish,0.005464
doe,0.033316
doibf,0.012544
domain,0.006011
doubt,0.007943
effect,0.003599
einstein,0.071473
electromechan,0.013650
electron,0.024803
elegantli,0.014547
embodi,0.008567
emit,0.009275
en,0.009106
endors,0.008770
energi,0.010374
ensembl,0.032030
ensur,0.006087
entangl,0.045617
entir,0.009612
environ,0.004911
epr,0.083464
equal,0.015138
erwin,0.033576
establish,0.003954
event,0.009644
everett,0.013530
everyday,0.007915
evolut,0.012032
examin,0.005799
exampl,0.006637
exchang,0.005238
exist,0.018424
expect,0.010182
experi,0.141258
experiment,0.030947
explan,0.013045
explod,0.010901
exploit,0.006898
express,0.009602
extens,0.010007
extern,0.011589
fact,0.004704
famou,0.006121
far,0.010049
fastest,0.010463
featur,0.010022
flash,0.010405
flask,0.014739
flow,0.017506
flu,0.013274
fog,0.012865
follow,0.003169
forbid,0.010655
fork,0.012471
form,0.003028
formal,0.004958
formul,0.006345
forward,0.007033
frac,0.008867
friend,0.007870
function,0.016227
fundament,0.005074
game,0.013598
gegenwrtig,0.018915
geiger,0.042309
gone,0.008792
govern,0.003767
grw,0.020045
gunpowd,0.058446
ha,0.036989
hammer,0.011574
happen,0.012524
haran,0.014673
haroch,0.017614
held,0.004684
hi,0.007105
highlight,0.016317
histori,0.009900
hit,0.008203
honest,0.011672
hour,0.013925
howev,0.016078
hugh,0.009298
human,0.007635
hydrocyan,0.034763
idea,0.008894
ijrap,0.020045
illustr,0.033786
impli,0.006160
import,0.003379
impress,0.008087
inanim,0.011846
independ,0.008139
indeterminaci,0.028740
individu,0.004087
inform,0.027574
instead,0.009282
institut,0.004192
intend,0.018401
interact,0.014831
interfer,0.016175
interpret,0.169159
intuit,0.008478
investig,0.005552
involv,0.019206
ion,0.008592
irrespect,0.011510
irrevers,0.010937
issu,0.008503
ital,0.012242
j,0.004976
jfoukzon,0.020045
journal,0.005096
keg,0.017170
known,0.019863
laboratori,0.006722
larg,0.006982
larger,0.005228
largescal,0.007718
laue,0.014807
ldot,0.027640
left,0.005280
leggett,0.017614
letter,0.013078
life,0.017033
light,0.005461
limit,0.004045
linear,0.007048
link,0.011521
live,0.017132
lock,0.008579
long,0.012903
look,0.016408
loop,0.009121
loos,0.008235
lost,0.005756
machin,0.012791
macroscop,0.018979
magnif,0.013910
main,0.004193
mainstream,0.008038
make,0.020210
mani,0.008981
manyworld,0.074753
martyn,0.015538
mass,0.010864
mathemat,0.010414
matter,0.004982
max,0.008094
mean,0.003647
meanwhil,0.016019
measur,0.013015
mechan,0.078264
mention,0.012825
microscop,0.008551
mind,0.006246
mix,0.006371
mixtur,0.016563
model,0.004126
modelintern,0.020045
modern,0.003973
modif,0.007873
motion,0.006683
motiv,0.006471
movi,0.009059
multipl,0.010189
n,0.005948
naiv,0.010913
nation,0.003670
natur,0.010608
naturwissenschaften,0.017381
near,0.005446
necessarili,0.006483
new,0.002958
niel,0.011302
nobel,0.007710
nobodi,0.011590
non,0.009546
nonissu,0.018170
note,0.003818
notebook,0.012377
noth,0.014018
nottingham,0.013239
novemb,0.005940
nucleu,0.008774
nucleusdead,0.020045
nucleusliv,0.020045
object,0.040751
observ,0.174866
observerinduc,0.020045
occur,0.012625
octob,0.005727
onc,0.004949
onli,0.021343
open,0.036277
origin,0.011012
orthodoxi,0.011099
oscil,0.009585
outcom,0.013543
outoffocu,0.018915
p,0.010112
paper,0.010930
paradox,0.042354
pardon,0.011810
particl,0.021359
particular,0.004146
pen,0.010613
penros,0.012400
percept,0.014100
perform,0.009312
perhap,0.012314
persist,0.007510
phillip,0.010250
philosoph,0.005813
photograph,0.009116
photon,0.019357
phrase,0.015962
physic,0.039008
physicist,0.039200
piezoelectr,0.014950
pig,0.009878
place,0.007491
platform,0.008111
play,0.004782
plu,0.007452
podolski,0.032968
point,0.007684
poison,0.018516
poliakoff,0.018170
pose,0.024204
possibl,0.011622
possiblydead,0.020045
pp,0.006192
prepar,0.006160
presenc,0.005743
present,0.015191
prevail,0.016070
prevent,0.005430
principl,0.009118
prize,0.007083
probabl,0.005450
problem,0.004062
process,0.014362
produc,0.003952
promin,0.005956
promot,0.005418
propon,0.007959
propos,0.028117
ps,0.011655
pseudoschrding,0.020045
psi,0.011558
psifunct,0.040091
puls,0.010121
pure,0.006006
push,0.007223
quantenmechanik,0.016341
quantum,0.229824
quantummechan,0.013075
quark,0.011542
qubit,0.030367
question,0.009539
quit,0.012838
radiat,0.007606
radioact,0.027963
raider,0.012377
random,0.007245
rangl,0.042462
reach,0.004895
real,0.004999
realiti,0.019657
realityr,0.020045
realli,0.007750
recent,0.004171
reduc,0.004563
refer,0.004817
refut,0.009514
regard,0.004621
rel,0.004319
relat,0.009743
relay,0.011192
releas,0.012040
remain,0.011951
rememb,0.008843
repres,0.003951
requir,0.018960
research,0.003907
resembl,0.007598
resolut,0.014738
resolv,0.006997
reson,0.009219
restrict,0.005583
result,0.006710
review,0.005397
riddl,0.013454
ridicul,0.011302
ring,0.016317
riski,0.010215
roger,0.008077
rosen,0.012519
rosenin,0.020045
rule,0.004397
said,0.004949
sapodosenov,0.020045
saw,0.005894
say,0.010437
scenario,0.026176
schrdinger,0.343743
schroding,0.038818
scientif,0.004847
scientist,0.005508
second,0.007907
secur,0.005117
seen,0.004726
separ,0.004400
serg,0.013107
seri,0.004798
seriou,0.006748
serv,0.004795
set,0.003731
settl,0.006846
setup,0.011274
shaki,0.013776
shatter,0.012137
shortliv,0.009464
sift,0.014259
similar,0.004003
similarli,0.006351
simpli,0.005470
simultan,0.042153
sinc,0.010057
singl,0.013459
situat,0.020646
sixti,0.010198
slac,0.016341
small,0.008204
smear,0.013380
snapshot,0.013013
someth,0.012891
sometim,0.004364
sort,0.007031
special,0.008385
split,0.006890
spontan,0.008664
sqrt,0.010820
squid,0.024894
stand,0.006300
standard,0.013546
stanford,0.007787
state,0.114401
statesyet,0.020045
statist,0.011000
steam,0.009984
steel,0.016794
stop,0.019869
strang,0.009946
strength,0.006799
subatom,0.010655
subensembl,0.020045
substanc,0.007048
success,0.004445
suffici,0.006197
suggest,0.009386
suicid,0.009611
superconduct,0.046756
superpos,0.032968
superposit,0.255340
surviv,0.005782
symbol,0.006086
taken,0.004908
technic,0.005962
tegmark,0.015739
temperatur,0.006777
term,0.006335
test,0.005480
theme,0.007424
theoret,0.010994
theori,0.019648
thi,0.045136
thought,0.061767
threshold,0.009338
thu,0.003918
time,0.014831
tini,0.009043
toni,0.010386
touchston,0.013309
transform,0.005487
translat,0.005771
trap,0.008783
trillion,0.009344
trivial,0.010008
tube,0.009263
tune,0.010889
typic,0.004497
unclear,0.008806
unconsci,0.010024
undecay,0.018170
unexplod,0.016341
uniqu,0.011432
univers,0.006944
unstabl,0.008943
upper,0.006770
upward,0.009332
use,0.010957
valid,0.012803
variant,0.016242
vector,0.008107
verschrnkung,0.017614
vibrat,0.019566
view,0.017187
viewpoint,0.008825
viru,0.010215
vol,0.014410
wa,0.008246
wave,0.027043
wavefunct,0.037939
way,0.025092
weak,0.006659
welldefin,0.019985
wigner,0.012780
wineland,0.017170
wish,0.014825
won,0.006786
word,0.004653
world,0.010196
wrote,0.011423
xbox,0.014609
xray,0.009143
yam,0.012865
year,0.003383
zero,0.007050
