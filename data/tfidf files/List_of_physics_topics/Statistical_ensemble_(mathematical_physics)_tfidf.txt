abl,0.004775
abov,0.014520
accomplish,0.007598
accord,0.011341
actual,0.004838
add,0.007209
addit,0.011474
adopt,0.005217
allow,0.007519
alway,0.005083
analog,0.006755
ani,0.016659
anoth,0.003636
answer,0.013463
apparatu,0.028227
appear,0.004378
appli,0.016166
applic,0.009030
approach,0.004351
appropri,0.018180
arbitrari,0.016327
arbitrarili,0.020879
articl,0.004493
ascend,0.009906
asid,0.008622
assign,0.006790
associ,0.003693
assum,0.010474
assumpt,0.012882
atom,0.006610
attempt,0.004312
averag,0.022587
b,0.015022
base,0.003294
basi,0.009418
bath,0.010614
becaus,0.003431
befor,0.003893
blanket,0.012626
block,0.013581
boltzmann,0.011515
boundari,0.006364
braket,0.015616
broad,0.012245
built,0.005682
c,0.030539
calcul,0.011634
canon,0.107772
carlo,0.008688
case,0.011452
caus,0.004075
certain,0.004178
certainli,0.008715
chain,0.006821
chang,0.003442
characterist,0.005262
chemic,0.034881
choic,0.011998
chosen,0.013953
class,0.005064
classic,0.045401
clear,0.011365
close,0.012256
cnnldot,0.019888
collect,0.004454
come,0.008734
commonli,0.005071
commut,0.010144
compar,0.004516
compens,0.007717
complet,0.012652
concept,0.004261
concern,0.008574
conclus,0.006804
condit,0.017091
connect,0.005027
consequ,0.010385
conserv,0.012123
consid,0.003561
consider,0.010263
consist,0.012261
constant,0.006071
constraint,0.015163
construct,0.004728
contact,0.051262
contain,0.021904
context,0.005371
continu,0.007400
control,0.004057
convex,0.010769
coordin,0.077681
copi,0.014723
correct,0.049195
correspond,0.011087
count,0.006964
covari,0.011214
crucial,0.007433
crude,0.009589
d,0.029661
defin,0.033526
definit,0.004828
denot,0.007451
densiti,0.095800
depend,0.016117
deriv,0.013929
describ,0.022552
despit,0.005178
dhat,0.018767
diagon,0.034262
differ,0.025898
difficult,0.005326
dimens,0.007026
directli,0.004906
discret,0.007202
discuss,0.004982
displaystyl,0.083942
distinct,0.014109
distribut,0.040309
divid,0.004842
doe,0.016527
dpldot,0.019888
dqn,0.037534
dt,0.012042
duplic,0.010018
e,0.039587
electr,0.006079
element,0.004675
elim,0.019888
encod,0.026858
energi,0.041175
energytim,0.018367
ensembl,0.720348
ensemblea,0.059666
entir,0.004768
entropi,0.039911
environ,0.009747
eoperatornam,0.018767
equals,0.035471
equat,0.018344
equilibrium,0.069154
equival,0.011844
erron,0.010530
es,0.011648
especi,0.004269
essenti,0.005110
evalu,0.006370
everi,0.004600
evolv,0.042243
exactli,0.014404
exampl,0.029634
exchang,0.025985
exclud,0.006916
exist,0.007311
exk,0.018767
expect,0.020205
experi,0.009042
experiment,0.006141
explicit,0.008027
explor,0.005601
express,0.004763
extend,0.009698
extent,0.006049
fact,0.004668
factor,0.023308
fair,0.007446
far,0.004985
ferromagnet,0.013136
field,0.008029
fix,0.017506
fluid,0.015499
follow,0.009434
form,0.006010
formalis,0.011548
formul,0.012591
formula,0.013730
foundat,0.005126
frac,0.017595
fulli,0.005927
function,0.040250
futur,0.004788
ga,0.019842
gener,0.025424
gibb,0.044311
given,0.019715
grand,0.038182
grant,0.006026
h,0.027305
ha,0.023592
hamilton,0.009756
hamiltonian,0.022810
hat,0.044000
heat,0.006932
hilbert,0.010551
hncp,0.019888
hopfield,0.014985
howev,0.003190
human,0.003787
ideal,0.012793
ident,0.016887
identif,0.007743
import,0.016765
includ,0.010916
inconsist,0.009136
incorpor,0.005398
index,0.006245
indistinguish,0.011117
individu,0.012165
infer,0.007555
infinit,0.030791
influenc,0.004330
infti,0.012001
input,0.007122
insid,0.006593
instanc,0.005324
instead,0.004604
int,0.043848
integr,0.028465
interact,0.014715
intern,0.003445
interpret,0.005244
introduc,0.013835
involv,0.007622
ipipsi,0.019888
irangl,0.017036
ise,0.013312
isol,0.012765
issu,0.004218
iter,0.009838
j,0.004937
joint,0.013844
just,0.004584
k,0.011920
kind,0.031419
knoperatornam,0.019888
knowledg,0.004944
known,0.013138
lab,0.008588
laboratori,0.013340
lack,0.005011
langl,0.039015
larg,0.010391
lattic,0.019601
law,0.003959
ldot,0.054849
lead,0.007843
like,0.003510
lim,0.012083
linguist,0.007726
liouvil,0.029523
literatur,0.011744
local,0.008902
locat,0.004832
logic,0.006361
macroscop,0.037661
maintain,0.004582
mani,0.017823
manipul,0.007036
manner,0.006342
manual,0.008301
map,0.006012
markov,0.044801
materi,0.004605
mathemat,0.030998
matrix,0.057747
maxim,0.007474
maximum,0.007146
mea,0.081777
mean,0.003619
measur,0.012913
mechan,0.106772
member,0.004193
mention,0.006362
microcanon,0.033014
microscop,0.016968
microst,0.093442
mix,0.012642
model,0.012281
molecul,0.007096
moment,0.007476
momenta,0.042130
mont,0.009853
motion,0.006631
multipl,0.015164
n,0.118039
naiv,0.010828
nearbi,0.008088
nearest,0.010593
nearestneighbor,0.033014
necessari,0.010474
necessarili,0.012866
neighbor,0.014827
network,0.005494
neumann,0.020705
new,0.002935
ninfti,0.027985
normal,0.005238
notat,0.008830
note,0.015156
notion,0.037306
nrightarrow,0.014833
ns,0.037959
nsinfti,0.039777
nsum,0.014257
number,0.062467
nve,0.018767
nvt,0.019888
object,0.004492
observ,0.030362
obtain,0.020432
offset,0.009556
onc,0.009821
onli,0.009075
open,0.004499
oper,0.045605
operatornam,0.024992
order,0.018108
orthogon,0.022263
outcom,0.006718
overcount,0.234373
p,0.060201
paradox,0.008404
paramet,0.007576
partial,0.006045
particl,0.190735
particular,0.016454
partit,0.027311
past,0.005213
percept,0.006995
perform,0.009240
period,0.003904
phase,0.176954
physic,0.098907
physicsinfluenc,0.019888
place,0.007433
pn,0.036377
point,0.003812
posit,0.007789
possibl,0.019218
potenti,0.024395
precis,0.012415
predetermin,0.010793
prep,0.015616
prepar,0.024447
present,0.003768
preval,0.008297
princip,0.005886
principl,0.018095
priori,0.010101
probabilist,0.009800
probabl,0.097336
problem,0.012091
procedur,0.052503
produc,0.011763
properti,0.018251
protocol,0.017042
provid,0.006687
psi,0.011467
q,0.042200
qn,0.044955
quantiti,0.042967
quantum,0.098811
question,0.014197
radi,0.012653
random,0.014377
rang,0.009047
real,0.009921
recogn,0.015374
reduc,0.004527
refer,0.002389
reflect,0.005136
region,0.008559
relat,0.006445
remain,0.011858
remov,0.011036
repeat,0.013797
replic,0.008436
repres,0.031364
represent,0.012059
requir,0.015049
reservoir,0.010758
respect,0.004545
restrict,0.005539
result,0.033292
rho,0.090758
rigor,0.008047
robot,0.008859
s,0.020338
said,0.004910
sampl,0.014244
second,0.003922
section,0.005922
seen,0.004689
select,0.005197
sens,0.004917
sequenc,0.026540
seriou,0.006695
serv,0.004757
set,0.037023
shape,0.005696
shown,0.005763
sigma,0.020500
similar,0.007943
similarli,0.006301
simpler,0.008869
simplest,0.008484
simpli,0.016282
simultan,0.006970
sinc,0.006652
size,0.015539
small,0.008140
smaller,0.005655
sole,0.006751
solv,0.006121
sometim,0.004330
somewhat,0.007051
sort,0.006976
space,0.138464
specif,0.003979
specifi,0.013525
spin,0.009225
state,0.069850
static,0.008508
stationari,0.019287
statist,0.196455
stay,0.007280
strictli,0.007844
strongli,0.006411
structur,0.003886
studi,0.003515
subject,0.004231
subregion,0.025199
subsequ,0.005190
subspac,0.012547
sum,0.039810
suppos,0.007309
t,0.005787
taken,0.004870
technic,0.005915
tediou,0.013348
temperatur,0.026897
term,0.015715
terminolog,0.007832
test,0.027188
theorem,0.007456
theori,0.007797
therefor,0.004521
thermal,0.025588
thermodynam,0.085376
thi,0.054734
thu,0.007776
time,0.020601
tool,0.005326
total,0.032487
tr,0.070963
trace,0.012756
turn,0.004598
type,0.007998
typic,0.004462
unabl,0.020737
uncertainti,0.015481
unifi,0.006962
uniqu,0.005671
unit,0.010064
unusu,0.008064
use,0.029896
usual,0.004002
valid,0.006351
valu,0.038369
vari,0.019614
variabl,0.030402
varianc,0.009985
varieti,0.004994
variou,0.003786
veri,0.007597
virtual,0.006578
volum,0.005697
von,0.013492
vt,0.013275
walker,0.010479
way,0.032009
weak,0.026429
weight,0.013408
wide,0.008568
willard,0.011038
word,0.009234
written,0.031597
x,0.094231
xdpldot,0.019888
xk,0.030295
xrangl,0.034954
xrho,0.016355
ye,0.021229
yesno,0.014257
