academ,0.021020
access,0.019417
agenc,0.022077
aim,0.020039
analysi,0.016917
anim,0.019643
appli,0.015002
archiv,0.025830
area,0.013938
avail,0.034310
b,0.018588
backgroundpropos,0.073828
basic,0.017023
befor,0.014452
begin,0.033000
block,0.025208
branch,0.017995
breadth,0.040194
broader,0.028214
budget,0.026487
build,0.017906
burn,0.029007
cascad,0.040738
champion,0.035183
chemic,0.302128
chemist,0.065143
chemistri,0.024840
combin,0.016162
come,0.016211
complex,0.033173
compound,0.229879
concern,0.031828
consist,0.015171
convert,0.023107
corey,0.051942
critic,0.016674
deal,0.018261
describ,0.027904
design,0.016649
desir,0.021334
develop,0.011336
differ,0.012017
difficult,0.019771
diminish,0.030691
effort,0.056701
ej,0.048043
equip,0.024399
establish,0.029129
eventu,0.019397
exampl,0.012222
execut,0.020673
exist,0.013571
express,0.017682
extern,0.010671
facil,0.026160
final,0.017652
flask,0.054287
form,0.033464
foundat,0.019030
fund,0.020121
goe,0.026262
gram,0.040785
grant,0.022372
ha,0.009730
health,0.020344
highlight,0.060097
impli,0.022687
includ,0.010130
individu,0.030105
industri,0.016657
inordin,0.056549
inorgan,0.068238
instanc,0.019763
institut,0.015442
intermedi,0.057362
introduct,0.019346
isol,0.094773
known,0.012193
laboratori,0.123804
link,0.010608
mani,0.022053
manmad,0.039205
materi,0.017095
mean,0.013435
method,0.015108
methodolog,0.051645
mix,0.023465
modern,0.029265
multicompon,0.054044
multipl,0.075054
multistep,0.056549
nation,0.027035
natur,0.039071
necessari,0.019441
new,0.032690
nobel,0.028398
obtain,0.018961
onc,0.018228
organ,0.079839
organometal,0.045334
paracetamol,0.061878
particular,0.015269
percentag,0.027184
phase,0.023459
place,0.027592
plan,0.018475
plant,0.021180
play,0.017613
possess,0.021922
possibl,0.014268
practic,0.014967
present,0.013987
prior,0.021129
prize,0.052179
proce,0.068114
procedur,0.024362
process,0.039673
produc,0.014555
product,0.262437
propos,0.017259
provid,0.012411
pure,0.022120
purif,0.040880
purpos,0.036392
quantiti,0.022785
reactant,0.262599
reaction,0.341242
reactor,0.040417
reagent,0.044475
refer,0.008871
reliabl,0.026528
reproduc,0.030099
requir,0.027933
research,0.071965
resourc,0.017961
retrosynthet,0.071484
robert,0.019507
roundbottom,0.069665
scienc,0.014921
secondari,0.050528
select,0.038584
semisynthet,0.057591
seri,0.017674
serv,0.017660
set,0.013743
sever,0.012872
simpl,0.019618
singl,0.049572
skill,0.023786
someth,0.023739
special,0.015442
start,0.031259
step,0.083748
strategi,0.046001
submit,0.030926
synthes,0.096262
synthesi,0.578896
synthesis,0.046294
synthesissynthesi,0.073828
synthet,0.091870
target,0.068970
telescop,0.037949
tend,0.020276
theoret,0.020245
therapeut,0.036436
thi,0.018470
time,0.010924
total,0.034455
transform,0.040423
type,0.014845
typic,0.033130
unwant,0.041122
usag,0.025898
use,0.020178
valuabl,0.028985
varieti,0.018539
variou,0.014056
vessel,0.030061
weight,0.024887
won,0.024994
woodward,0.048762
work,0.012104
workup,0.120373
yield,0.096483
