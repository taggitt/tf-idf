aaron,0.006252
abderhalden,0.004908
abegg,0.005242
abel,0.003240
abeprocess,0.005414
academ,0.001541
academi,0.003965
access,0.001424
accum,0.005242
acetylsalicyl,0.004378
achil,0.003745
acid,0.006221
actor,0.002372
actual,0.001317
ada,0.003362
adenin,0.003459
adkin,0.004343
administr,0.001439
adolf,0.011358
adolph,0.003368
adriaan,0.004058
adrien,0.004124
adsorpt,0.004000
advanc,0.002542
advoc,0.003533
africa,0.001671
african,0.001804
agr,0.004311
agricola,0.003837
agricultur,0.003059
ahm,0.002957
aikin,0.005414
alan,0.006546
alberi,0.005414
albert,0.006275
alchemist,0.016594
alder,0.004223
aleksandr,0.003452
alessandro,0.003144
alexand,0.015776
alfr,0.006327
alizarin,0.014725
alkalin,0.003251
alkaloid,0.003809
alkharafi,0.005109
allen,0.002376
alsatian,0.004908
altman,0.004000
aluminium,0.002884
amedeo,0.004038
american,0.087466
amin,0.003122
amir,0.003483
analysi,0.001240
analyt,0.009382
andr,0.002686
andrea,0.002738
andrew,0.004180
anfinsen,0.004586
ang,0.003881
angela,0.003585
angelo,0.003585
anthoni,0.002430
anthracen,0.005109
anticip,0.002289
anticoagul,0.004038
antistok,0.005242
antoin,0.008471
anton,0.002984
antonio,0.002680
antun,0.005109
appli,0.001100
applic,0.001229
archer,0.003284
archibald,0.003409
arduengo,0.005242
area,0.002044
arfwedson,0.005242
argentin,0.003220
arkel,0.005109
armenian,0.003130
arn,0.003604
arrheniu,0.003770
arthur,0.008398
artturi,0.005414
ascanio,0.004343
asian,0.001960
asprey,0.005109
associ,0.001005
aston,0.004147
asymmetr,0.002815
atmospher,0.002016
atom,0.003599
atomist,0.003256
august,0.010948
augusto,0.003444
auschwitz,0.004311
australian,0.004281
austrian,0.002465
avogadro,0.007352
avram,0.004758
awardwin,0.003757
b,0.009543
babcock,0.004171
bachmann,0.004171
baekeland,0.004452
baeyer,0.004343
baglioni,0.005414
bahram,0.004695
bakhui,0.005109
ball,0.002541
baptist,0.006010
bard,0.003614
baromet,0.003498
baron,0.005477
barri,0.005450
bartlett,0.003585
barton,0.003490
basic,0.001248
basolo,0.005414
batcheld,0.004452
baum,0.003654
bayer,0.003745
becher,0.005109
beilstein,0.009172
bel,0.003745
beletskaya,0.005414
belgian,0.011559
belgianamerican,0.005414
belgium,0.002436
bell,0.002402
bellini,0.004695
benjamin,0.004706
bennett,0.003190
benzen,0.003654
berg,0.003180
bergiu,0.005242
berkeley,0.009695
berthelot,0.004147
berthollet,0.004251
bertozzi,0.004758
beryllium,0.003614
berzeliu,0.003687
best,0.002735
bijvoet,0.005414
biochemist,0.069450
bioinorgan,0.003796
biolog,0.002943
biomed,0.002725
black,0.001674
blue,0.002212
boger,0.004758
boisbaudran,0.004828
boldingh,0.005414
bond,0.003545
born,0.177076
borodin,0.008447
bosch,0.003823
botanist,0.002948
boudouard,0.010829
boussingault,0.005242
bowen,0.007209
boyer,0.006790
boyl,0.003084
braconnot,0.004828
brain,0.001986
brand,0.002508
branko,0.004124
brazilian,0.002861
breslow,0.004586
breweri,0.003783
british,0.027715
brnsted,0.004758
bromin,0.003665
brook,0.002731
brown,0.002105
bruce,0.002550
buchner,0.004197
buchwald,0.004311
buchwaldhartwig,0.005414
bunsen,0.007892
burner,0.004019
burton,0.006524
buss,0.004000
butenandt,0.005000
butlerov,0.004586
bx,0.004058
c,0.007126
caesium,0.003757
caffein,0.004058
calcium,0.002731
california,0.003807
calvin,0.003096
canada,0.001780
canadian,0.006329
cannizzaro,0.008988
cantharidin,0.005242
car,0.002196
carbid,0.003881
cariu,0.005414
carl,0.016602
carlsberg,0.004378
caro,0.004124
carolina,0.002800
carolyn,0.003687
caroth,0.004538
catalan,0.003381
catalysi,0.003375
cavendish,0.003349
ceauescu,0.004452
cech,0.004378
cell,0.003575
centuri,0.001067
ceram,0.002922
ceva,0.004908
chaim,0.003963
chalfi,0.005000
chancellor,0.002948
chang,0.000937
charl,0.019104
charlesadolph,0.004828
chateli,0.004280
chauvin,0.003981
chemic,0.009496
chemist,0.731011
chemistri,0.346159
chevreul,0.004586
china,0.003194
chines,0.005360
chlorofluorocarbon,0.004171
chocol,0.003416
christian,0.006443
christoph,0.004357
chromatographi,0.003409
chromium,0.003395
ciamician,0.005414
ciechanov,0.005109
claud,0.002692
clayton,0.007190
cn,0.003139
cndea,0.005414
coach,0.003423
coauthor,0.002850
cochran,0.003307
cocoa,0.003171
codiscov,0.032468
codiscover,0.004124
coenraad,0.004452
cohen,0.002736
coher,0.002291
coin,0.001721
coinventor,0.008248
colleg,0.001804
combin,0.001185
combust,0.002872
communist,0.002050
compos,0.003154
compound,0.003746
comput,0.002755
comt,0.003084
concept,0.001160
confus,0.003742
congoles,0.003687
conrad,0.003162
conserv,0.001650
constantin,0.003008
contracept,0.003567
contribut,0.002440
corecipi,0.009076
corey,0.007619
cori,0.007794
corneliu,0.003278
cornforth,0.005109
coryel,0.004828
costa,0.002944
costin,0.005109
cotton,0.005083
coulson,0.003837
count,0.001896
couper,0.004414
crack,0.002766
craft,0.002500
cram,0.004079
creat,0.002033
creator,0.002491
crook,0.003307
crude,0.002610
crutzen,0.004414
crystallograph,0.010996
curi,0.006626
curl,0.003540
curtiu,0.004695
cyanid,0.003698
cycl,0.001704
cyclotron,0.003698
cyril,0.003318
czech,0.007992
d,0.006729
dale,0.003185
dalton,0.002994
dam,0.002805
dame,0.003251
dan,0.002692
dandolo,0.004908
daniel,0.002067
danish,0.010517
danishefski,0.005414
darlean,0.005242
databas,0.001945
davi,0.004821
david,0.004430
davorin,0.005414
ddt,0.003929
dean,0.002710
deby,0.003913
definit,0.001314
deisenhof,0.005414
del,0.002510
demaria,0.005414
der,0.002175
derek,0.003318
descript,0.001519
design,0.001221
develop,0.006651
dewar,0.004101
di,0.002587
dialysi,0.003963
diderik,0.004538
die,0.001593
diederich,0.004452
diel,0.004586
diet,0.002458
diethylamid,0.004586
diffus,0.002271
director,0.001999
dirk,0.003676
discov,0.051245
discover,0.015209
discoveri,0.004671
dmitri,0.003235
doctor,0.007936
doisi,0.004586
dolar,0.005109
dolph,0.004758
donald,0.004688
dongala,0.005414
dorothi,0.003349
dorp,0.005109
dow,0.003235
drebbel,0.004452
drug,0.001960
du,0.002349
duchek,0.005414
dudley,0.003483
duisburg,0.004586
duma,0.003624
dupont,0.003733
dutch,0.032671
dutchamerican,0.004586
dutchgerman,0.005414
dye,0.005695
e,0.006736
earl,0.003026
earli,0.003003
earth,0.001571
east,0.001538
econom,0.001095
eduard,0.006252
educ,0.002550
edward,0.007283
edwin,0.005281
effici,0.001567
egyptian,0.002256
egyptianamerican,0.005242
ehrlich,0.003567
eichengrn,0.005242
eigen,0.004171
eilhardt,0.005414
electrochemist,0.009390
element,0.017822
elementari,0.002258
elena,0.003929
elhuyar,0.004586
elia,0.003375
elio,0.004378
ellen,0.003049
elmer,0.003498
elsay,0.005000
elvehjem,0.005414
emelu,0.005414
emil,0.019711
emmanuel,0.006568
employe,0.002097
energi,0.001401
engin,0.007212
english,0.032063
englishcanadianamerican,0.005414
enrico,0.003235
entheogen,0.003981
environment,0.001609
erlenmey,0.005109
ernest,0.005150
ernst,0.012133
ertl,0.004638
esther,0.003634
estonianamerican,0.010829
ether,0.003109
eugn,0.003757
eugnemelchior,0.005242
eulerchelpin,0.005414
eva,0.003467
evangelista,0.003733
experi,0.003692
extract,0.001926
eyr,0.003796
f,0.002948
faiza,0.005414
fajan,0.004908
famou,0.008268
faraday,0.003049
father,0.012318
fausto,0.004280
federico,0.003634
fehl,0.004414
fellow,0.011315
femtochemistri,0.003913
fenn,0.004908
ferdinand,0.002705
ferment,0.002938
fermi,0.003195
ferricyanid,0.005000
fibr,0.003210
field,0.004372
fighter,0.002718
finnish,0.003084
fischer,0.018118
fischertropsch,0.004538
fission,0.003038
fittig,0.005414
flamel,0.004828
florentin,0.003523
flori,0.003981
focu,0.001468
food,0.001540
footbal,0.002678
ford,0.002834
form,0.000818
formal,0.001339
format,0.001459
formos,0.005000
founder,0.009380
fourcroy,0.004586
fourier,0.002793
francesco,0.009432
franchis,0.002889
franci,0.004099
frank,0.004191
frankland,0.004124
franklin,0.002448
franoi,0.007939
franoismari,0.004452
franz,0.002826
frasch,0.010485
fraser,0.003295
frdric,0.010785
fred,0.005552
frederick,0.009658
free,0.001222
french,0.047535
freseniu,0.005414
friedel,0.004638
friedelcraft,0.009516
friedrich,0.017514
fritz,0.005774
frumkin,0.004828
fticr,0.005414
fuel,0.001983
fukui,0.004758
g,0.004343
ga,0.001800
gabor,0.003946
gabriel,0.002701
gadolin,0.004908
gadolinium,0.004058
galileo,0.002635
galissard,0.005109
garnett,0.004494
gaspard,0.003981
gaylussac,0.007927
gener,0.000769
geochemist,0.004197
geochemistri,0.003262
geoffrey,0.002798
geometri,0.002182
georg,0.015426
georgiu,0.004378
gerardu,0.003783
gerhard,0.006218
gerhardt,0.004343
germain,0.003567
german,0.077091
germancanadian,0.005414
germani,0.006446
germanrussian,0.004908
germanswiss,0.005109
gerti,0.004147
giacomo,0.003796
giauqu,0.005414
gibb,0.003016
gilbert,0.007680
gillavri,0.005109
gilman,0.008000
giulio,0.003665
glass,0.002359
glassmak,0.004638
glauber,0.004695
glendenin,0.005414
glenn,0.003162
gmelin,0.004124
gnter,0.003881
gobley,0.005109
gold,0.001861
goldschmidt,0.004079
gomberg,0.004908
goorl,0.005414
gordon,0.002496
gorlaeu,0.005414
gottfri,0.002823
gottlob,0.003256
gowland,0.004414
graham,0.002727
gray,0.002670
grbe,0.005414
green,0.001840
grignard,0.004171
grubb,0.004378
guillaumefranoi,0.005414
gun,0.002513
gustav,0.002738
gwyn,0.004452
h,0.010407
ha,0.000713
haber,0.007171
hahn,0.003416
haldan,0.003337
hall,0.002030
hallhroult,0.004586
hammond,0.007331
han,0.008556
hansjoachim,0.004494
harden,0.003284
harold,0.007705
harri,0.006753
hartmut,0.004638
harvard,0.002091
harvey,0.002714
hassel,0.004758
hatchett,0.005000
hauptman,0.004908
havemann,0.005414
haworth,0.004494
hayyan,0.004452
head,0.004145
health,0.001492
heathcock,0.005414
heeger,0.005414
heinrich,0.010625
helmont,0.003929
hen,0.003687
hendrik,0.003251
henri,0.029327
henricu,0.003981
henriett,0.004586
henrik,0.003467
herbert,0.006953
herman,0.002858
hermann,0.013081
herschbach,0.005414
hershko,0.005109
herti,0.005109
herzberg,0.004223
hess,0.003180
hesss,0.004343
hevesi,0.004538
heyrovsk,0.005242
hi,0.002879
hideki,0.003913
hillar,0.005109
hinshelwood,0.004828
historian,0.001803
hodgkin,0.003595
hoff,0.003757
hoffman,0.003490
hoffmann,0.007064
hofmann,0.007619
homer,0.003001
hopkin,0.002636
houben,0.005414
houten,0.005109
hoveyda,0.005414
howard,0.004965
hsiao,0.004251
hsiehwilson,0.005414
huang,0.003355
huber,0.004343
hubert,0.003349
humphri,0.006818
hungarian,0.005518
hyde,0.003567
hydrogen,0.006650
ibn,0.002330
ignaci,0.004197
iii,0.002046
ilmari,0.005414
ilya,0.003558
import,0.004564
includ,0.000743
indigo,0.003437
industri,0.004886
industrialist,0.006390
influenti,0.001721
inform,0.001064
ingold,0.004058
inorgan,0.017517
institut,0.001132
invent,0.010449
inventor,0.029134
ion,0.002321
ipatieff,0.005414
iranian,0.002699
iridium,0.004019
irina,0.004494
irish,0.002538
irn,0.003913
irradi,0.003343
irv,0.002884
irwin,0.003318
isol,0.005213
isomorph,0.003101
isotop,0.002592
israel,0.002191
italian,0.018075
italianamerican,0.004343
ivanovich,0.003823
izaak,0.005414
j,0.017474
jabir,0.003981
jacob,0.002413
jacobu,0.007592
jacqu,0.004965
jaffrey,0.005109
jakob,0.003210
jame,0.007547
jan,0.004704
janssen,0.007892
jaroslav,0.003981
jean,0.008879
jeanbaptist,0.002901
jeanmari,0.003929
jeffrey,0.005561
jen,0.003313
jeremia,0.004638
jerom,0.002998
jerri,0.002948
jinp,0.004280
jn,0.003490
joachim,0.003362
joan,0.002910
johan,0.009340
johann,0.018322
john,0.018261
joliotcuri,0.008394
josef,0.003295
joseph,0.015873
josiah,0.003148
jr,0.004546
julian,0.002759
juliu,0.013615
justu,0.003549
k,0.003245
kagan,0.003881
kari,0.003654
karl,0.012078
karrer,0.004538
kastner,0.004908
kazimierz,0.003929
kekul,0.008394
kelk,0.005000
ken,0.002970
kendrew,0.004414
kenichi,0.004280
key,0.001362
kineticmolecular,0.004695
kingdom,0.001423
kipp,0.005242
kippgener,0.005414
kirchhoff,0.003595
kjeldahl,0.005414
klaproth,0.004758
kletz,0.005109
klug,0.004494
knoevenagel,0.005242
knowl,0.004101
known,0.018779
knute,0.005414
kohn,0.003733
koichi,0.004414
kolb,0.007703
kolthoff,0.005414
konrad,0.003256
kool,0.004828
kornberg,0.004147
kreb,0.003665
kroto,0.003913
kuhn,0.003162
kurdish,0.003634
kurt,0.005637
kuwait,0.003026
kuwaiti,0.004171
kyriaco,0.005414
l,0.007876
la,0.001786
laker,0.005109
langmuir,0.003783
lar,0.003430
larn,0.005109
latanoprost,0.005414
laureat,0.002771
laurenc,0.003278
laurent,0.003423
lauri,0.003614
lauterbur,0.004538
lavoisi,0.003101
lavon,0.004908
lavoslav,0.005414
law,0.007546
lawrenc,0.002393
le,0.004263
lead,0.001067
leblanc,0.004378
lecithin,0.004908
lecoq,0.004586
lee,0.004866
lehn,0.004452
leloir,0.005109
lemieux,0.004908
leo,0.002678
leopold,0.009016
levi,0.002514
lewi,0.002246
libaviu,0.005414
libbi,0.004414
liebermann,0.005414
liebig,0.003604
light,0.001475
linda,0.002951
link,0.000778
linu,0.003262
lipscomb,0.004538
lise,0.003634
list,0.003980
lister,0.007248
live,0.001156
livermor,0.003866
ljubljana,0.003809
lomonosov,0.007733
longuethiggin,0.004378
lothar,0.003946
loui,0.014133
lowri,0.003929
lozani,0.005000
lsd,0.003929
ludwig,0.002456
lui,0.005571
luigi,0.003190
lundgren,0.004758
lutetium,0.004171
luxembourg,0.003041
lyserg,0.004586
m,0.003839
mac,0.003113
macdiarmid,0.004758
mackinnon,0.004058
macquer,0.004638
major,0.000949
maker,0.002468
manfr,0.003467
manuel,0.002861
manufactur,0.001706
marcellin,0.004586
marcu,0.002828
margaret,0.002614
mari,0.002067
maria,0.002644
marignac,0.005109
marinski,0.005414
mario,0.003101
marion,0.003540
mark,0.001436
markovnikov,0.004908
marshal,0.002371
martin,0.011363
martinu,0.003676
marum,0.005109
mass,0.001467
master,0.003819
materi,0.003761
mauvein,0.004586
max,0.006559
mccollum,0.004414
mcmillan,0.003981
mechan,0.001321
medal,0.005255
medicin,0.018765
meitner,0.003913
melvin,0.003698
member,0.001141
mendeleev,0.003644
mercer,0.003783
mercuri,0.002635
meredith,0.004019
merkel,0.004000
merriam,0.004058
merrifield,0.004538
merril,0.003416
metabol,0.002371
metal,0.003724
method,0.001108
mexicanamerican,0.004197
meyer,0.006039
micha,0.003783
michael,0.003293
michel,0.007358
michelsonmorley,0.003823
microscopi,0.005631
middl,0.001475
mikhail,0.008455
miller,0.002497
millerurey,0.004038
millington,0.004378
mineralog,0.003045
mineralogist,0.003981
minglon,0.005414
minist,0.003228
miramont,0.005414
mitchel,0.005996
mitscherlich,0.009390
mller,0.003080
modern,0.004292
mohr,0.003837
moissan,0.004695
molecul,0.001932
molina,0.003733
molli,0.004000
mong,0.004124
monod,0.003913
moor,0.007244
morley,0.003676
mose,0.002919
moseley,0.007466
mostafa,0.004758
moulton,0.004280
muetterti,0.005414
muguet,0.005414
mulder,0.004343
mulli,0.003929
mulliken,0.004251
murder,0.002272
muse,0.003307
n,0.001606
nalbandyan,0.005109
namesak,0.003644
nation,0.000991
natta,0.004828
natur,0.002865
naumovich,0.004908
nba,0.003981
neil,0.002842
nenitescu,0.005414
neopren,0.004828
neri,0.004311
nernst,0.004147
nevil,0.003507
new,0.000799
newland,0.004019
newton,0.002259
niacin,0.004378
nicholson,0.003654
nicola,0.013402
nicolau,0.003215
nieuwland,0.005109
nikolau,0.004251
nikolay,0.003614
nikolayevich,0.004695
nil,0.003459
niobium,0.004124
nitril,0.004311
nitroglycerin,0.004101
nobel,0.349917
nobleman,0.003733
norman,0.002521
norrish,0.004695
northrop,0.004019
norwegian,0.002780
notat,0.002404
notr,0.003498
novelist,0.006169
noyori,0.005109
nuclear,0.010969
nucleobas,0.004019
numer,0.001325
nutrit,0.002570
nuzzo,0.005414
nylon,0.003676
o,0.003773
octav,0.003614
octavio,0.004586
odd,0.002524
oil,0.001785
oktay,0.005414
olah,0.005414
olsen,0.003929
onli,0.000823
onsag,0.004058
open,0.001224
oral,0.002312
orchard,0.003654
organ,0.017567
organometal,0.003324
origin,0.000991
osamu,0.004280
ostwald,0.003837
otto,0.015126
owner,0.002023
oxygen,0.002178
ozon,0.003139
p,0.001365
pakistani,0.003251
palladium,0.003523
palladiumpatalyz,0.005414
panel,0.002636
papermak,0.003929
paracelsu,0.003687
paris,0.005000
parr,0.004343
parti,0.001382
particl,0.001923
pasteur,0.006202
patent,0.002387
patsi,0.004758
paul,0.020218
peac,0.001725
pedersen,0.003897
perci,0.003278
perform,0.001257
period,0.002126
perkin,0.013091
peroxid,0.003532
persian,0.002248
person,0.001170
perutz,0.004079
peter,0.011420
petru,0.003946
pharmaceut,0.002437
pharmaceutica,0.005242
pharmacist,0.007134
phd,0.002312
philbin,0.005414
philologist,0.003823
phlogiston,0.003614
phoenix,0.003235
phosphoru,0.002948
photoelectron,0.004079
physic,0.023416
physician,0.002257
physicist,0.040239
physiolog,0.014752
piero,0.003532
pierr,0.006909
pill,0.003416
pioneer,0.024298
plastic,0.002405
pligot,0.005242
plunkett,0.004908
pneumat,0.003576
poet,0.002474
poison,0.002500
polanyi,0.003676
polish,0.004834
polishamerican,0.005414
polishborn,0.004414
politician,0.002146
polyest,0.003897
polym,0.005377
pop,0.003068
popl,0.004251
porter,0.006039
postul,0.004415
potassium,0.002845
powder,0.003049
practic,0.001097
prebiot,0.003929
pregl,0.005414
prelog,0.004538
prepar,0.001664
present,0.001025
presid,0.002967
priestley,0.006777
prigogin,0.003929
prime,0.003296
primo,0.004101
princip,0.001602
prize,0.342524
process,0.006789
product,0.002138
professor,0.005501
promethium,0.012842
promot,0.001463
propel,0.002875
proport,0.001696
protein,0.002084
proton,0.002652
proust,0.004124
psychopharmacolog,0.003837
pupil,0.002676
pure,0.001622
quantum,0.002069
quasicryst,0.004452
r,0.008237
radiat,0.002054
radic,0.003761
radiochemist,0.004586
ralph,0.002759
ramakrishnan,0.004586
raman,0.003634
ramsay,0.003624
raoult,0.009172
rapoport,0.004038
rapson,0.005414
raymond,0.008023
razi,0.004223
rdx,0.004908
reaction,0.011679
reagent,0.003262
rebek,0.005000
recipi,0.002511
rees,0.004058
refer,0.000650
regnault,0.004452
reichstein,0.004828
reina,0.004251
reinhold,0.003409
rememb,0.002388
remigiu,0.005109
research,0.004222
resist,0.001686
reson,0.002490
resul,0.005414
rex,0.003467
rhaze,0.003757
rhodium,0.004197
rice,0.002454
richard,0.017682
richter,0.007466
riehl,0.004908
ro,0.003064
roald,0.004079
robert,0.021461
robinson,0.002668
robiquet,0.005109
rock,0.002037
rockn,0.005414
roderick,0.003452
roger,0.004364
romanian,0.006297
ronald,0.004648
ronni,0.003881
rootar,0.005414
roozeboom,0.005414
rosalind,0.003437
rose,0.001974
rouell,0.010829
rowland,0.003676
roy,0.002668
royal,0.001716
rsted,0.003913
rubber,0.002823
rubidium,0.003796
rudolf,0.002674
rudolph,0.010546
ruika,0.005109
rupo,0.004758
russian,0.020955
russianamerican,0.008248
rutherford,0.006390
ruzicka,0.005242
ryji,0.005414
s,0.005537
sabati,0.004343
sabir,0.004494
safeti,0.002097
sage,0.002565
salimuzzaman,0.005414
samuel,0.002313
sanger,0.003515
scheel,0.003866
schnbein,0.004197
scholar,0.001652
schreiber,0.004311
schrock,0.004638
schultz,0.003654
scienc,0.003283
scientist,0.010415
scott,0.004816
scottish,0.012489
seaborg,0.004000
secretari,0.002125
sefstrm,0.005242
selmi,0.005414
semyonov,0.004452
serbian,0.003256
servai,0.004908
sever,0.000944
shahak,0.005109
share,0.003803
sharpless,0.004251
shechtman,0.005414
sherman,0.003409
sherwood,0.004019
shimomura,0.004908
shirakawa,0.004586
shoichet,0.005414
shulgin,0.005109
siddiqui,0.004343
sidgwick,0.003783
sidney,0.003135
signific,0.001156
sima,0.003783
sinanoglu,0.005414
singl,0.001211
singlegrain,0.004586
sir,0.014308
skou,0.004414
slovenegerman,0.005414
smalley,0.004251
smith,0.001918
soap,0.003195
sobrero,0.004908
societi,0.003403
soddi,0.004019
solar,0.002160
solidphas,0.005000
solomon,0.002780
solvay,0.004101
somorjai,0.005414
son,0.001733
sorbic,0.005242
south,0.002836
soviet,0.001961
soybean,0.003402
spanish,0.001958
spanishmexican,0.005414
specialist,0.002247
spectrometri,0.003337
spectroscopi,0.002707
spectroscopist,0.005109
spl,0.004758
sport,0.002150
srensen,0.003981
st,0.001459
sta,0.004223
stahl,0.003913
standish,0.004695
stanford,0.004207
stanislao,0.004638
stanley,0.004770
stanovnik,0.005414
state,0.000792
stauding,0.004251
steenbock,0.005414
stein,0.003337
steitz,0.004758
stephen,0.004023
sterl,0.003008
sterochemistri,0.005414
steroid,0.003409
stock,0.001747
stoddart,0.004378
stoichiometri,0.003687
stone,0.002022
stoni,0.003595
stookey,0.005414
stork,0.004223
stradonitz,0.004638
structur,0.001058
stuart,0.005023
studi,0.000957
summer,0.002095
sumner,0.003558
sunney,0.005414
surgeon,0.005647
susan,0.002768
sutermeist,0.005414
svant,0.003897
svedberg,0.004378
swallow,0.003084
swan,0.003256
swart,0.004828
swedish,0.017288
swiss,0.012920
swissborn,0.004638
syng,0.004538
synthes,0.004706
synthesi,0.016174
synthet,0.002246
t,0.006302
tabl,0.003485
tadeu,0.005109
tanaka,0.004147
tang,0.002925
taub,0.004197
technolog,0.001305
teflon,0.004378
term,0.001711
tetraval,0.004124
th,0.001038
thallium,0.004019
thatcher,0.003307
thenard,0.005414
theodor,0.012392
theoret,0.008909
theori,0.003184
thermal,0.002322
thermochemistri,0.003614
thi,0.000677
thnard,0.004638
thoma,0.008099
thompson,0.002688
thomson,0.002813
tiseliu,0.004758
tishler,0.005414
tissu,0.004376
tnt,0.003745
tobin,0.003796
todd,0.006324
toni,0.002805
torricelli,0.003770
total,0.001263
train,0.001563
transform,0.001482
trevor,0.003498
tsien,0.004638
tsvet,0.004638
tungsten,0.003343
turkish,0.002621
u,0.002113
uc,0.006450
ukasiewicz,0.004171
ultra,0.003452
unit,0.001826
univ,0.003068
univers,0.005627
uranium,0.005657
urbain,0.004147
urea,0.003267
urey,0.004251
usag,0.001899
usbas,0.003423
use,0.002219
v,0.001614
valenc,0.006306
van,0.015847
vanadium,0.003709
vasilevich,0.004494
vaska,0.005414
vauquelin,0.004908
venkatraman,0.004828
victor,0.007312
vigneaud,0.005414
viktor,0.003409
vincent,0.005739
vincenzo,0.003929
violet,0.003783
virtanen,0.005109
vitamin,0.002974
vladimir,0.007731
volmer,0.005109
volta,0.003395
voltaic,0.003709
von,0.009183
vratislav,0.005109
w,0.004558
wa,0.001485
waal,0.003205
walker,0.005706
wallac,0.002768
wallach,0.004038
walter,0.006480
walther,0.003368
warfarin,0.004343
warner,0.003337
warri,0.005000
weight,0.001825
weizmann,0.004147
wendel,0.003507
werner,0.005734
west,0.001554
wheatley,0.004124
whinfield,0.005109
whip,0.003507
whitesid,0.004586
whler,0.003665
wieland,0.004638
wilbrand,0.005414
wiley,0.002216
wilhelm,0.015958
wilkinson,0.003262
willard,0.006010
willem,0.003343
william,0.024669
williamson,0.006626
willson,0.004638
willsttter,0.004758
wim,0.004058
windau,0.004638
winner,0.057347
wirth,0.004223
wittig,0.004586
wolf,0.033284
wollaston,0.003851
woman,0.002261
won,0.001833
woodward,0.003576
work,0.017756
worm,0.002853
wreyford,0.005414
wthrich,0.005414
wurtz,0.008988
wurtzfittig,0.005414
x,0.001603
xi,0.002790
xiaoliang,0.005414
xie,0.008394
y,0.003857
yale,0.002475
year,0.000913
yi,0.003381
yonath,0.004638
ytterbium,0.004280
yuan,0.003096
yunusov,0.005414
yve,0.003604
z,0.002209
zare,0.004758
zealand,0.002153
zewail,0.004251
ziegler,0.003837
zsigmondi,0.005000
