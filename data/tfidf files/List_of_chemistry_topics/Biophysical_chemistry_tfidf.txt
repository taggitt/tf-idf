academ,0.046931
acta,0.049987
action,0.019249
archiv,0.028835
area,0.015560
award,0.027619
base,0.013651
behavior,0.022159
bibliographi,0.027473
bind,0.032621
biochem,0.077677
biochemistri,0.112645
biochimica,0.077771
biolog,0.179235
biologiqu,0.070598
biomembran,0.072423
biophys,0.670313
biophysica,0.077771
biophysicalchemistri,0.082418
cantor,0.150481
cell,0.027210
charl,0.072695
chemist,0.109083
chemistri,0.277304
chimi,0.121766
common,0.015300
commun,0.016546
composit,0.026232
concept,0.017659
conform,0.034925
devot,0.029912
differ,0.013415
diffract,0.091812
ed,0.063985
edp,0.067776
elsevi,0.128348
employ,0.020208
engag,0.024126
enzym,0.037038
et,0.026188
exampl,0.027289
explain,0.020509
explan,0.026818
featur,0.020604
franais,0.053134
franci,0.031198
freeman,0.122305
function,0.050038
gttingen,0.053256
ii,0.021321
iii,0.031155
includ,0.022618
institut,0.051717
intern,0.014276
ion,0.035327
isbn,0.057776
journal,0.146676
liposom,0.062428
macromolecul,0.093178
magnet,0.031970
make,0.013849
match,0.028962
max,0.033280
membran,0.038565
metal,0.028345
method,0.033733
model,0.016964
modif,0.032370
molecul,0.088220
nmr,0.049325
nobel,0.031702
nuclear,0.027827
oldest,0.029460
paul,0.071018
phenomena,0.052005
phospholipid,0.052322
physic,0.089101
physicochimi,0.082418
physiqu,0.057006
planck,0.041347
pocket,0.043511
press,0.036759
prize,0.029125
probe,0.038838
protein,0.063450
publish,0.017333
r,0.125384
refer,0.009903
reput,0.034462
research,0.032135
reson,0.037905
ribosom,0.048560
schimmel,0.207234
scienc,0.049972
seek,0.022224
shape,0.047212
similarli,0.026113
size,0.021465
socit,0.049987
spectroscop,0.047991
st,0.066660
structur,0.112733
studi,0.058267
subject,0.017534
substrat,0.041859
supramolecular,0.108321
taylor,0.034683
techniqu,0.105912
term,0.026049
themselv,0.020445
thi,0.010309
understood,0.025868
use,0.022525
variou,0.031383
vesicl,0.051375
wa,0.022603
wh,0.150744
work,0.013512
xray,0.075184
