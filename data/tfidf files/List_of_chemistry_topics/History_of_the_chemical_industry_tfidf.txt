abat,0.011871
abl,0.004369
abov,0.004429
abraham,0.008124
abroad,0.007512
academi,0.006664
account,0.012223
acid,0.069702
acquir,0.005634
acryl,0.013264
ad,0.004414
addit,0.006999
adhes,0.051906
administr,0.004837
adsorpt,0.013444
advanc,0.004272
aesthet,0.008390
affili,0.007913
aftalion,0.018199
agenc,0.005442
agent,0.005898
aggreg,0.007316
agricultur,0.010284
agrochem,0.014836
air,0.005314
al,0.005934
alcohol,0.007942
alexand,0.006628
alfr,0.014177
alkali,0.021544
alkalin,0.010927
alli,0.006389
allow,0.003440
alon,0.023664
alway,0.004652
america,0.009696
american,0.007839
ammonia,0.019993
ancient,0.005012
anilin,0.013642
anim,0.004842
annual,0.010628
anoth,0.006654
anticip,0.007694
anywher,0.008468
apparel,0.023207
appear,0.004007
appendic,0.013573
applianc,0.032782
applic,0.004131
area,0.010308
artifici,0.005842
ash,0.028040
asia,0.011880
aspect,0.004546
assur,0.007882
automobil,0.008667
avail,0.004228
balanc,0.005475
base,0.009043
basf,0.015253
basic,0.050359
batch,0.011076
bayer,0.025176
becam,0.007625
becaus,0.003140
becom,0.003339
began,0.012496
begin,0.004067
belgian,0.019426
bell,0.008076
benzen,0.012284
bibliographi,0.006066
biggest,0.007831
billion,0.047401
biolog,0.004947
birth,0.006146
black,0.005629
bleach,0.023801
bottl,0.020397
bottleneck,0.011412
box,0.007820
brandt,0.012672
brazil,0.007454
brine,0.012672
brink,0.011529
britain,0.017629
british,0.009315
broad,0.011205
brother,0.013990
brunner,0.027016
built,0.015600
bulk,0.029579
busi,0.018809
butadien,0.013940
byproduct,0.018518
canada,0.005983
capac,0.005791
carbid,0.013046
carbon,0.013618
care,0.005248
carpet,0.011930
case,0.006986
catalyst,0.027369
catch,0.008410
categori,0.047978
caustic,0.013046
ceficshow,0.018199
celluloid,0.013940
cement,0.009212
cent,0.008667
centr,0.005744
central,0.003916
centuri,0.025113
ceram,0.009823
challeng,0.004990
chamber,0.007413
chandler,0.011366
chang,0.006299
chapter,0.006511
character,0.005299
characterist,0.004815
charl,0.016052
charleroi,0.014600
cheap,0.008655
chemic,0.750103
chemist,0.008029
chemistri,0.018370
china,0.021471
chlorid,0.039764
chlorin,0.030267
clean,0.016357
cleaner,0.011900
close,0.014954
cloth,0.014608
cluster,0.030663
coast,0.006376
coat,0.033108
coincid,0.007515
colour,0.007974
combin,0.007968
come,0.003996
commerci,0.010706
commod,0.054254
commun,0.007307
compani,0.066182
competit,0.005494
complex,0.008177
complianc,0.008968
compound,0.006296
compris,0.011432
concentr,0.005468
condens,0.008085
conglomer,0.010436
consid,0.003258
consolid,0.007337
constant,0.005555
construct,0.025963
consum,0.055640
contact,0.005863
contain,0.020043
contin,0.007200
continu,0.003385
contribut,0.004101
control,0.003712
convert,0.011392
corros,0.011115
corrosionresist,0.015589
cosmet,0.030824
cost,0.014738
council,0.010190
countri,0.025741
creat,0.003418
creation,0.005086
crop,0.006908
crude,0.017550
crystal,0.007839
current,0.003577
custom,0.005650
cyanamid,0.015992
cylind,0.010395
d,0.004523
data,0.013378
deal,0.004501
dedic,0.006712
deforest,0.010274
deliv,0.006586
demonstr,0.005022
deni,0.006477
deriv,0.008497
despit,0.004738
deterg,0.012249
develop,0.016768
devis,0.007665
diagnost,0.008948
diammonium,0.017621
differ,0.014811
differenti,0.011316
dioxid,0.008211
direct,0.003710
directli,0.008979
discov,0.004921
discoveri,0.005233
discret,0.006590
distil,0.020016
divers,0.010749
divid,0.004431
dollar,0.045706
domin,0.009196
dow,0.032623
downstream,0.010857
dri,0.014638
drug,0.006590
drum,0.010612
dupont,0.025095
dure,0.003216
dye,0.038285
dyestuff,0.015414
e,0.009056
earli,0.006729
east,0.010344
econom,0.007363
economi,0.009189
ed,0.004709
effect,0.003268
effici,0.010535
elastom,0.029673
electrochemistri,0.011412
electron,0.016889
element,0.004278
elev,0.007506
emerg,0.008623
employ,0.004462
encount,0.006626
end,0.003678
energi,0.014129
engin,0.014545
england,0.029189
english,0.004685
ensur,0.005527
enterpris,0.012665
entir,0.004363
environ,0.004459
environment,0.010817
equip,0.006014
ernest,0.008655
especi,0.007813
establish,0.007180
et,0.005783
ethylen,0.036544
eu,0.029300
euro,0.025679
europ,0.027371
european,0.025141
everi,0.004209
everyday,0.007186
everywher,0.008738
evolut,0.005462
exampl,0.009039
exceed,0.014130
exclud,0.006328
exhibit,0.012701
expand,0.009518
expans,0.005676
explos,0.022373
export,0.018514
expos,0.006738
extent,0.005535
extern,0.002630
extract,0.006475
facil,0.032243
fact,0.004271
factori,0.029444
fallen,0.008174
farben,0.030507
fastestgrow,0.012051
fat,0.009212
feedstock,0.012356
fertil,0.021155
fiber,0.017477
fibr,0.010788
field,0.007347
fifti,0.017053
figur,0.004982
film,0.006823
filtrat,0.011843
fine,0.043153
firm,0.024034
flavor,0.010274
flavour,0.011680
fluoropolym,0.018199
follow,0.005755
food,0.015532
foreign,0.004683
form,0.005499
formul,0.005761
fraction,0.007041
fragranc,0.027571
franc,0.015450
fratern,0.011038
fred,0.009330
french,0.009682
fungicid,0.014388
furnish,0.010236
ga,0.024210
gadget,0.013099
game,0.006173
garbett,0.018199
gase,0.008020
gaseou,0.010113
gdp,0.027063
gener,0.010340
german,0.010161
germani,0.016250
gesner,0.014290
glasgow,0.021134
glass,0.015860
global,0.023887
glycerin,0.016229
good,0.003999
goodyear,0.014290
govern,0.003420
great,0.004054
greater,0.004531
group,0.003245
grow,0.004613
growth,0.036428
gulf,0.008138
ha,0.019190
halt,0.008197
hancock,0.013712
hand,0.004457
harvard,0.007028
health,0.005015
heat,0.006343
heavi,0.012435
help,0.003938
henri,0.011596
herbert,0.007789
herbicid,0.013508
hi,0.019353
high,0.010928
highli,0.004735
hightech,0.010822
hill,0.007156
histor,0.004041
histori,0.008989
hoechst,0.015781
home,0.015411
hough,0.014600
howev,0.005839
httpwwwcccgroupcomchemicalssolutionsindustri,0.019014
httpwwwworldofchemicalscom,0.018199
huge,0.013380
hundr,0.005465
hydrogen,0.007451
ici,0.012051
ig,0.012629
impact,0.004930
imperi,0.007008
implement,0.005008
import,0.006136
impos,0.006186
includ,0.049946
increas,0.006918
index,0.005714
india,0.011015
induc,0.007123
industri,0.246372
infrastructur,0.006090
ink,0.032072
innov,0.006231
inorgan,0.033643
insecticid,0.012996
inspect,0.008988
institut,0.003806
instrument,0.016618
integr,0.004341
intens,0.005974
intermedi,0.014140
intern,0.012609
invent,0.011707
invest,0.005122
involv,0.010462
isbn,0.008505
itali,0.006474
item,0.006599
jame,0.015221
japan,0.017847
job,0.006212
john,0.012275
joseph,0.005928
joshua,0.010125
jr,0.007641
juli,0.005196
just,0.012584
kingdom,0.014355
korea,0.007260
laboratori,0.012207
labour,0.012178
lancashir,0.013712
larg,0.034867
larger,0.004747
largescal,0.007008
largest,0.049675
largestvolum,0.018199
largevolum,0.047343
late,0.004164
later,0.007153
latest,0.008033
law,0.003623
lead,0.003588
leaden,0.015781
leas,0.009079
leblanc,0.073575
legisl,0.005506
lever,0.010805
life,0.015464
like,0.003212
lime,0.043023
limit,0.003673
liquefi,0.012356
liquid,0.012893
list,0.003344
live,0.003888
liverpool,0.011344
livr,0.012850
locat,0.022110
london,0.015192
long,0.003904
look,0.004965
losh,0.036399
lot,0.014814
louisiana,0.010395
low,0.004609
lpg,0.015105
ludwig,0.008257
lye,0.014491
main,0.003807
mainli,0.004992
maintain,0.004193
major,0.015953
make,0.012232
manag,0.004307
mani,0.019027
manmad,0.019329
manufactur,0.131908
map,0.005501
market,0.040514
marketplac,0.009508
mass,0.004931
materi,0.058999
matur,0.013995
mccoy,0.014600
mcgraw,0.011322
meet,0.004865
mention,0.005822
metal,0.012518
metallurgist,0.015414
methanol,0.012672
method,0.018622
micheal,0.018199
michigan,0.008679
middl,0.009916
milk,0.017764
million,0.023618
miner,0.006658
mixtur,0.015038
modern,0.014428
mond,0.036155
money,0.005374
monom,0.011458
monsanto,0.014290
mostli,0.005131
multi,0.011458
muspratt,0.018199
n,0.005400
nanci,0.010186
nation,0.006664
natur,0.006421
near,0.009889
nearli,0.005237
nepic,0.018199
netherland,0.014016
network,0.005027
new,0.008058
news,0.006563
nicola,0.009009
nigeria,0.009626
nitric,0.011578
nitrocellulos,0.013642
nitrogen,0.008301
norri,0.012898
north,0.009549
northeast,0.025695
note,0.003467
number,0.003008
numer,0.004455
nylon,0.012356
offer,0.004471
oil,0.042017
oldest,0.006505
oleochem,0.018199
onli,0.005536
onlin,0.010526
onsit,0.011787
open,0.004117
oper,0.018969
organ,0.013121
organicindustri,0.018199
origin,0.003332
outlook,0.008621
output,0.050215
outsid,0.004587
overal,0.005504
overlap,0.006683
oxid,0.015958
oxygen,0.007322
packag,0.045743
paint,0.022913
paper,0.014886
park,0.014057
parkesin,0.015992
particularli,0.004217
partli,0.006466
pass,0.004671
patent,0.024075
pe,0.010755
pennsylvania,0.009009
peopl,0.003315
percent,0.090598
perform,0.004227
perfum,0.011843
period,0.003573
perkin,0.011000
peroxid,0.011871
perspect,0.005381
pesticid,0.042329
petrochem,0.110382
petroleum,0.023191
pharmaceut,0.032770
pharmacist,0.011990
phosphat,0.018596
phosphor,0.025430
pictur,0.006732
piec,0.006641
pigment,0.020154
pilot,0.008908
pineo,0.016807
pioneer,0.006282
pipe,0.030197
pipelin,0.009834
place,0.003400
plant,0.046990
plastic,0.105106
pollut,0.024268
polycarbon,0.015105
polyest,0.013099
polyethylen,0.051218
polym,0.117483
polypropylen,0.040721
polystyren,0.027016
polyvinyl,0.028581
popul,0.004278
port,0.006795
portal,0.008673
portion,0.006043
portland,0.011481
potash,0.025518
pound,0.017288
poundgrowth,0.018199
powder,0.020498
power,0.003529
pp,0.028110
practic,0.003689
precipit,0.007930
present,0.003448
press,0.012175
pressur,0.010381
prestonpan,0.015992
price,0.025525
pricesov,0.018199
primari,0.004552
primarili,0.004713
princip,0.016161
print,0.006475
prize,0.012863
process,0.068460
produc,0.064585
product,0.212053
promot,0.004919
propylen,0.029200
protect,0.004596
prove,0.010644
provid,0.006119
provok,0.008824
ps,0.010582
pulp,0.010909
purpl,0.010597
purposebuilt,0.014600
pvc,0.026764
qualiti,0.010276
quantiti,0.016850
quickli,0.005613
r,0.004614
rail,0.008679
rang,0.004139
rank,0.005822
rapid,0.005976
rapidli,0.011627
rare,0.011350
rate,0.021367
raw,0.014268
react,0.007641
reaction,0.028040
realli,0.007036
record,0.004609
recreat,0.008447
refer,0.002187
refin,0.020582
refineri,0.010137
regard,0.004195
region,0.011749
rel,0.003921
relat,0.005897
remain,0.010851
remark,0.006707
remov,0.005049
repeal,0.009897
repres,0.007175
requir,0.003442
research,0.014192
researchanddevelop,0.017173
resin,0.011038
resourc,0.004427
respons,0.003793
rest,0.005124
result,0.003046
revenu,0.019141
review,0.004900
revolut,0.021671
rhnepoulenc,0.017173
right,0.003924
river,0.005979
road,0.006352
roebuck,0.014290
rollox,0.016229
rothamst,0.014290
rotterdam,0.012083
rubber,0.056944
s,0.018611
safe,0.007150
saintdeni,0.014290
sale,0.076454
salt,0.022479
saltpet,0.014491
samuel,0.007774
saw,0.010704
scale,0.039601
scienc,0.018391
scope,0.006429
scotland,0.016440
scrutin,0.011654
sea,0.005580
sealant,0.067228
second,0.003589
sector,0.040136
segment,0.007493
select,0.004755
separ,0.011986
serv,0.004353
servic,0.004261
sever,0.009519
shape,0.010425
share,0.008523
shown,0.010548
shreve,0.014197
similar,0.007269
sinc,0.006087
singl,0.008146
singlemolecul,0.015253
sir,0.006870
site,0.005127
slake,0.016807
slow,0.006283
small,0.003724
smaller,0.020702
smallest,0.015844
soap,0.032218
soda,0.096165
sodium,0.018533
softwar,0.006448
sold,0.019283
solid,0.013083
solvay,0.082713
solvent,0.019606
sometim,0.003962
soon,0.005581
sour,0.011481
sourc,0.007657
south,0.004766
special,0.034260
specialti,0.051865
specif,0.007283
spend,0.012636
spread,0.005380
spur,0.008548
st,0.004906
stale,0.013642
standard,0.004099
start,0.007705
state,0.018642
station,0.006649
stori,0.006169
structur,0.003556
styren,0.014491
sublim,0.010628
subsidiari,0.009205
substanc,0.019197
success,0.012109
suggest,0.004261
sulfur,0.062637
sunlight,0.009490
superphosph,0.017173
suppli,0.009924
surfac,0.005511
surfact,0.027880
surplu,0.024024
surplus,0.010788
symbiosi,0.011175
synthet,0.037745
tabl,0.011715
tabul,0.011389
tankcar,0.018199
tanktruck,0.018199
tariff,0.016720
techniqu,0.004677
technolog,0.004388
teessid,0.034346
temperatur,0.006153
tend,0.014994
tennant,0.027880
terephthal,0.015414
term,0.002876
termin,0.007131
test,0.009951
texa,0.008593
textil,0.024689
th,0.017450
thi,0.015936
thoma,0.005444
time,0.016158
titanium,0.010892
today,0.008745
togeth,0.004197
toluen,0.013861
ton,0.049519
took,0.009509
total,0.012740
town,0.006250
toy,0.019926
trace,0.005836
trade,0.026418
tradit,0.007665
transform,0.004982
transport,0.015653
treat,0.010659
treatment,0.005599
trend,0.005813
triad,0.024786
trillion,0.016967
tube,0.008410
turpentin,0.014715
tyne,0.014107
type,0.003659
typic,0.016333
uneconom,0.012468
union,0.004523
unit,0.033770
univers,0.009457
unlik,0.005068
urin,0.010161
use,0.069638
usual,0.007325
util,0.016519
valu,0.011703
varieti,0.022850
variou,0.003465
vcm,0.016497
veget,0.014532
vent,0.010723
veri,0.006952
version,0.005324
vessel,0.007410
vinyl,0.013785
visit,0.006115
vitamin,0.009996
volum,0.036496
vulcan,0.012430
w,0.005107
wa,0.052409
ward,0.009314
wash,0.009526
wast,0.007301
water,0.014085
watersolubl,0.012804
watson,0.009072
wax,0.010340
went,0.005582
western,0.009157
wide,0.003920
william,0.019509
wilson,0.007603
winnington,0.018199
wood,0.007023
work,0.014919
world,0.052460
worldwid,0.018525
worth,0.006891
woytinski,0.029932
x,0.005389
xxii,0.013508
xylen,0.014836
year,0.018430
york,0.004687
young,0.005651
