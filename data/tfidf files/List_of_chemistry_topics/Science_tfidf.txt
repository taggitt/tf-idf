abbasidera,0.013219
abi,0.011459
abil,0.009027
abl,0.008288
abraham,0.007704
absenc,0.006031
abstract,0.005773
academi,0.012640
acadmi,0.011649
accademia,0.012058
accept,0.016475
accord,0.029525
accur,0.011179
achiev,0.020636
act,0.007499
activ,0.019641
actual,0.020994
ad,0.012560
add,0.006256
addit,0.006638
address,0.004940
adher,0.013272
adopt,0.009054
advanc,0.028364
advertis,0.007709
advoc,0.005631
affect,0.012861
affirm,0.007918
age,0.020543
agenc,0.010322
agenda,0.007642
ago,0.006357
aim,0.004684
air,0.005039
aircraft,0.007286
alex,0.009683
alfr,0.006722
algebra,0.021135
alhacen,0.012937
alhaytham,0.031191
alhazen,0.072187
alkindi,0.010888
allegedli,0.008448
alloc,0.006547
allow,0.013050
almagest,0.011341
alon,0.011221
alongsid,0.006823
alreadi,0.009559
alway,0.008823
ambit,0.008556
america,0.004597
american,0.007434
analysi,0.003954
analyz,0.011029
anarch,0.009567
ancient,0.019013
ani,0.017348
announc,0.005509
anoth,0.009466
answer,0.005841
antibiot,0.009258
antiqu,0.020367
antir,0.022055
apart,0.006012
apertur,0.022153
appear,0.003799
appercept,0.012324
appli,0.038579
applic,0.003918
appreci,0.007279
approach,0.030205
appropri,0.010517
approv,0.005635
arab,0.012119
archiv,0.018115
area,0.022809
argentina,0.008138
argu,0.030550
argument,0.038584
aristotelian,0.051068
aristotl,0.060868
arithmet,0.016021
arm,0.005231
art,0.005019
articl,0.007798
artific,0.011313
artifici,0.005540
artisan,0.027152
artist,0.006885
ask,0.016270
aspect,0.008623
assert,0.005635
assess,0.005588
associ,0.009614
assum,0.013634
assumpt,0.011179
astronomi,0.022098
atom,0.022946
attain,0.006641
attempt,0.011226
attribut,0.005100
australia,0.005702
austrianbritish,0.013297
author,0.003693
authoritarian,0.008417
automobil,0.008219
avail,0.004010
avoid,0.014701
awar,0.005935
award,0.005783
axiomat,0.018067
bachelor,0.008543
bacon,0.087954
bad,0.006730
badli,0.008918
baghdad,0.027334
balanc,0.005192
barri,0.008686
base,0.005717
basi,0.004086
basic,0.031837
batteri,0.008887
bayesian,0.009120
bce,0.007259
bear,0.006139
beat,0.008321
becam,0.014463
becaus,0.014889
becom,0.009500
befor,0.013514
began,0.011850
begin,0.015429
begun,0.006289
behavior,0.018561
belief,0.015886
believ,0.004091
berezow,0.013378
bertrand,0.008617
best,0.013080
better,0.004460
bia,0.030173
bias,0.008197
biolog,0.018766
biologist,0.007401
biotechnolog,0.008617
black,0.005338
bless,0.009376
blueski,0.013073
book,0.030077
bool,0.010933
boolean,0.009965
botch,0.012525
bound,0.005919
box,0.007416
boy,0.007795
branch,0.016827
breadth,0.009396
breakthrough,0.008441
britain,0.005572
british,0.004416
broad,0.015940
broader,0.006595
build,0.008372
built,0.009863
bulk,0.007012
bullet,0.009781
bush,0.008159
byzantin,0.008138
c,0.011357
cabl,0.008624
cairo,0.009937
calculu,0.014788
calendar,0.008112
california,0.012137
caliph,0.008959
came,0.004331
campbel,0.008871
canada,0.005674
capabl,0.005217
capit,0.004309
care,0.014931
career,0.012998
cargo,0.008840
carri,0.012813
case,0.016563
cast,0.006730
catalog,0.009068
categori,0.010110
cater,0.009719
cathol,0.006397
catholic,0.008879
caus,0.031827
causal,0.014753
caveat,0.010933
cell,0.005698
center,0.008451
centr,0.005447
centuri,0.074848
certain,0.021758
certainti,0.058540
chain,0.017757
challeng,0.004732
chang,0.008961
character,0.005025
characterist,0.004566
charl,0.010148
chemistri,0.005807
chief,0.005517
child,0.006363
children,0.005449
china,0.005090
choic,0.005206
chri,0.008918
christin,0.020691
circuit,0.007474
cite,0.016624
citizen,0.010844
civil,0.009089
claim,0.032175
class,0.008789
classic,0.021888
classif,0.011692
classifi,0.026798
clearcut,0.010676
clearli,0.005957
climb,0.008967
close,0.010636
cockpit,0.012186
coher,0.007303
coin,0.010973
colleagu,0.007162
collect,0.007730
colloqui,0.009220
color,0.006600
combin,0.011335
come,0.018948
commerci,0.015229
commercialis,0.011683
common,0.006408
commonli,0.013201
commun,0.069298
compat,0.007602
compet,0.005463
competit,0.005210
complet,0.007319
complex,0.019387
comput,0.030736
concept,0.014792
concern,0.022321
conclus,0.017713
condit,0.014831
conduct,0.004541
confirm,0.022851
conflict,0.009871
connect,0.008725
conscienti,0.010822
conscious,0.006750
consens,0.026926
consensu,0.013220
consequ,0.009012
consid,0.030904
consider,0.008906
consili,0.012058
consiliencefit,0.013463
consist,0.007093
constantli,0.007387
construct,0.008207
contain,0.007603
contamin,0.008454
contempl,0.009147
contemporari,0.010366
contend,0.007638
content,0.005228
contest,0.006972
contrast,0.022382
contribut,0.003889
control,0.007041
controversi,0.020887
convent,0.005131
cooper,0.005064
copernican,0.010541
copernicu,0.009897
corpor,0.010398
correct,0.005336
correl,0.006176
cosmic,0.008106
cosmolog,0.007747
council,0.009663
countri,0.006974
cours,0.013855
cover,0.013088
creat,0.009724
creation,0.019294
creativ,0.013694
credibl,0.008247
credit,0.010256
criterion,0.007956
critic,0.019490
crossinstitut,0.013378
csic,0.013145
csiro,0.012372
cult,0.008658
cultur,0.007969
curiositydriven,0.013297
current,0.010177
curri,0.010800
cuttingedg,0.011977
dalton,0.009545
damag,0.005718
darwin,0.007416
data,0.021145
date,0.004429
david,0.004707
day,0.003998
deal,0.017076
debat,0.019734
decad,0.004817
declin,0.005031
deconstruct,0.009831
dedic,0.006365
deduc,0.007914
deduct,0.007667
deep,0.011799
deepli,0.007482
defens,0.005816
defianc,0.011003
defin,0.007273
definit,0.004190
degre,0.021698
dei,0.010597
deliber,0.007093
demand,0.009462
demarc,0.009258
demonstr,0.004763
deni,0.006143
depend,0.010489
depict,0.013220
deriv,0.004029
descart,0.033170
describ,0.039140
design,0.007784
determin,0.007360
detriment,0.008707
deutsch,0.008959
develop,0.053005
dialogu,0.007398
dictionari,0.012849
did,0.015735
differ,0.030902
directli,0.008515
director,0.006371
disagre,0.007618
discard,0.008863
disciplin,0.053713
discours,0.007256
discov,0.018667
discoveri,0.039706
discrimin,0.014717
dispar,0.008143
disproof,0.012142
disprov,0.009156
disput,0.005330
distinct,0.016325
distinguish,0.023523
distort,0.007482
divid,0.008404
dk,0.011489
docta,0.013145
document,0.005116
doe,0.010756
domain,0.005175
domest,0.005490
domin,0.008721
doublecheck,0.013219
doubt,0.054713
doxa,0.012472
dramat,0.011969
dure,0.027454
e,0.004294
earli,0.028719
earlier,0.004714
earn,0.017990
earth,0.010017
easili,0.005465
eastern,0.005295
econom,0.003491
edibl,0.010296
edwina,0.013145
effect,0.006198
effort,0.008836
egg,0.007717
einstein,0.007692
electromagnet,0.007152
electron,0.016016
element,0.004057
elimin,0.011034
emphas,0.016797
emphasi,0.005786
empir,0.071228
empiric,0.051826
employ,0.004231
enabl,0.010119
encompass,0.011945
encyclopedia,0.005494
encyclopedist,0.012690
end,0.010464
energeia,0.012937
energi,0.008932
engag,0.015156
engin,0.022989
england,0.005536
english,0.004443
enlighten,0.006966
enorm,0.006871
ensur,0.005241
enter,0.004664
enterpris,0.012010
entertain,0.007844
entir,0.004138
entireti,0.009120
entiti,0.016239
entranc,0.008983
entri,0.006171
environment,0.005129
eon,0.010910
epistemolog,0.015164
equip,0.005704
equival,0.005139
era,0.005040
ernst,0.007734
error,0.006066
esof,0.013463
especi,0.018523
essay,0.006182
essenc,0.007207
essenti,0.013305
establish,0.017024
estim,0.004489
ethic,0.005871
euclid,0.019438
europ,0.012978
european,0.003973
eurosci,0.026926
evalu,0.016584
event,0.012455
eventu,0.004534
everi,0.011976
everyth,0.006353
evid,0.026103
evolut,0.010360
exampl,0.042860
exceptionfre,0.013145
exclud,0.006001
execut,0.004832
exist,0.015862
expect,0.008766
experi,0.062773
experiment,0.063949
expertis,0.015564
explain,0.034359
explan,0.056161
explanatori,0.008992
exploit,0.005939
expos,0.006390
exposit,0.008743
express,0.008267
extend,0.004208
extens,0.008616
extent,0.005249
extern,0.002494
extrem,0.009153
eye,0.019134
face,0.009577
facilit,0.005751
fact,0.012152
factor,0.008090
factual,0.009192
faculti,0.007369
fall,0.008709
fallaci,0.045340
fallibil,0.012472
fallibilist,0.012872
fallibl,0.011076
fals,0.006470
falsif,0.021776
falsifi,0.018632
famou,0.005270
fantast,0.010362
far,0.008652
faraday,0.009719
farm,0.006365
fatal,0.008292
favor,0.005269
feel,0.005964
feelgood,0.013219
femal,0.019216
feminist,0.008362
fertil,0.006687
festiv,0.007923
feyerabend,0.035050
feynman,0.009366
fiction,0.006804
field,0.059230
fifth,0.006794
final,0.028886
finess,0.012422
finit,0.019794
fix,0.010127
fl,0.019390
flourish,0.014043
focu,0.004680
follow,0.005458
forc,0.003419
forerunn,0.009059
form,0.013038
formal,0.076842
format,0.009306
formul,0.016389
formula,0.005957
forschungsgemeinschaft,0.013073
fortif,0.009601
forum,0.007006
forward,0.006056
foundat,0.026692
fraenkel,0.012231
frame,0.006519
framework,0.014663
franc,0.004883
franci,0.013066
fraud,0.007858
frege,0.010379
french,0.004591
frequent,0.014387
fring,0.018258
frontier,0.007520
fruitlessbut,0.013463
function,0.006985
fund,0.056446
fundament,0.017475
fusion,0.007952
fuss,0.012372
futur,0.004155
gain,0.008301
galileo,0.033595
gather,0.011894
gcse,0.012100
gdel,0.009781
gdp,0.006416
gender,0.007313
gener,0.036771
generalis,0.009844
genet,0.005794
genr,0.008055
genuin,0.007914
geocentr,0.010656
geograph,0.005548
geolog,0.006519
geometri,0.006957
georg,0.004916
geoscienc,0.010093
germ,0.009201
germani,0.005136
girl,0.008423
giusepp,0.010263
given,0.006843
glass,0.015040
global,0.009061
goal,0.014077
god,0.005971
goe,0.006139
golden,0.013825
good,0.007585
gottlob,0.010379
govern,0.035686
government,0.007140
gradual,0.010968
graduat,0.012785
grant,0.005229
great,0.015380
greatli,0.011173
greek,0.024155
grew,0.005701
ground,0.004964
group,0.015386
grow,0.004374
growth,0.008636
ha,0.029572
half,0.018784
hank,0.011649
har,0.009512
health,0.004756
heard,0.008112
heat,0.006015
heavili,0.005500
hebrew,0.008563
held,0.004033
helicopt,0.009277
heliocentr,0.020217
hellenist,0.008672
help,0.003734
henc,0.005323
heret,0.009870
herschel,0.011027
hi,0.018353
high,0.003454
higher,0.004055
highest,0.005224
highli,0.004491
hilbert,0.009156
histor,0.007664
histori,0.017049
hold,0.012327
honesti,0.010397
horgan,0.012690
hous,0.009153
howev,0.024918
human,0.059163
humancentr,0.012810
hundr,0.005183
hybrid,0.007127
hype,0.012058
hyperbol,0.009201
hypothes,0.061054
hypothesi,0.029712
hypotheticodeduct,0.011861
ibn,0.037134
icbm,0.012017
idea,0.034460
ideal,0.005551
idealist,0.009448
ident,0.004884
identifi,0.004124
ideolog,0.012888
ignor,0.011835
imag,0.015759
imagin,0.020461
imit,0.008070
impact,0.004675
import,0.023277
improv,0.008396
imr,0.010933
inaccess,0.009491
incent,0.007102
includ,0.040260
incomplet,0.014868
incorpor,0.004684
incorrect,0.016245
increas,0.016403
increasingli,0.010058
independ,0.007007
index,0.010839
indian,0.005647
individu,0.007037
induct,0.007555
inductiv,0.012749
industri,0.015576
inessenti,0.012422
infer,0.006556
infinit,0.006680
influenc,0.007515
influenti,0.005486
inform,0.013566
infrastructur,0.005776
initi,0.007413
innat,0.008392
innov,0.023637
inocul,0.011649
input,0.006180
inquir,0.010049
inquiri,0.020414
insist,0.006960
inspir,0.011289
instead,0.003995
institut,0.032490
instrument,0.015759
integr,0.004116
intellect,0.009147
intellectu,0.011705
intellig,0.005323
intensifi,0.008362
intent,0.005741
interact,0.008513
interdisciplinari,0.014025
intern,0.005979
internet,0.011193
interpret,0.004551
intersubject,0.011551
intim,0.008537
invent,0.011102
invert,0.018441
involv,0.009921
iraq,0.007634
iron,0.006110
isidor,0.010866
islam,0.011358
issu,0.018304
italian,0.006401
jet,0.008345
job,0.005891
john,0.023282
journal,0.035103
journalist,0.008030
juli,0.004927
june,0.004970
junk,0.020899
justifi,0.013721
kaiser,0.010866
karl,0.012832
keith,0.008583
kepler,0.009183
kept,0.006146
kind,0.004544
kingdom,0.004537
kitti,0.012231
knew,0.008035
know,0.010337
knowledg,0.171627
known,0.022803
kurt,0.008983
la,0.005693
labor,0.005600
lack,0.008698
ladd,0.036300
laid,0.006437
lakato,0.011584
landmark,0.008030
languag,0.008574
larg,0.006011
late,0.015795
later,0.016960
latin,0.026804
law,0.030923
lead,0.017015
leap,0.009076
learn,0.009582
led,0.022086
left,0.009093
legal,0.004706
legitimaci,0.008729
leibniz,0.017022
level,0.003410
librari,0.005358
life,0.014665
lifestyl,0.008122
light,0.023510
like,0.021325
lincei,0.012634
link,0.009920
list,0.003171
literari,0.007152
literatur,0.015287
live,0.007375
lofti,0.011584
logic,0.077280
long,0.007406
longer,0.004703
lost,0.009912
lower,0.008428
luck,0.010168
ludwig,0.007831
macroscop,0.008170
magazin,0.006602
magic,0.015904
main,0.007221
mainli,0.004734
maintain,0.007953
major,0.018154
make,0.026101
male,0.006397
maledomin,0.024373
man,0.005046
mani,0.043821
mark,0.004578
masquerad,0.012422
mass,0.009354
master,0.006086
materi,0.015986
math,0.026522
mathemat,0.103117
mathematica,0.009033
mathematician,0.020942
matter,0.017159
max,0.006969
mean,0.025126
measur,0.007470
mechan,0.016846
mechanist,0.009512
media,0.026155
medicin,0.027187
mediev,0.024331
men,0.010576
mere,0.022442
merit,0.008138
metaphys,0.007296
method,0.074173
methodolog,0.042256
michael,0.005248
microbiologist,0.011489
middl,0.014106
mind,0.026890
minim,0.006019
minist,0.005145
misconduct,0.010636
misrepres,0.010578
miss,0.007194
mmr,0.012525
mobil,0.006057
model,0.028419
modern,0.037628
modifi,0.005564
mohist,0.011584
mongol,0.008959
monitor,0.006167
monophysit,0.012142
mooney,0.012810
motion,0.011509
movement,0.003932
multipl,0.004386
multipli,0.007351
museum,0.013754
muslim,0.006378
mytholog,0.023928
nation,0.031601
natur,0.170498
nearterm,0.011937
necessari,0.009089
need,0.017141
neg,0.004945
nestorian,0.011683
new,0.043306
newborn,0.010021
news,0.006224
newton,0.014401
night,0.006995
ninth,0.008926
nomenclatur,0.008096
nonaristotelian,0.012810
nonscienc,0.012422
nonscientif,0.011429
nontechn,0.011027
nonteleolog,0.013463
normal,0.009091
northern,0.005331
notabl,0.017504
note,0.006576
notion,0.016186
nuclear,0.017482
nucleu,0.007555
number,0.025677
nutshel,0.012058
o,0.006013
object,0.015594
obscur,0.007900
observ,0.090335
obsolet,0.008895
obtain,0.004432
occam,0.010956
occur,0.003623
oecd,0.008441
offend,0.009545
offer,0.008481
offic,0.004590
old,0.009437
older,0.005984
oldest,0.006169
onc,0.012784
onli,0.028877
ontolog,0.023439
open,0.007808
oper,0.003597
opportun,0.005386
optic,0.034303
option,0.005950
order,0.009428
organ,0.018664
origin,0.018963
otherwis,0.005424
outcom,0.005830
output,0.005952
outreach,0.010153
outsid,0.004350
outward,0.008780
overlook,0.008810
oversimplifi,0.011313
overturn,0.009355
overus,0.010956
paleontolog,0.018793
paper,0.004705
parent,0.006422
parmenid,0.021601
parsimoni,0.010696
particl,0.006130
particular,0.021418
particularli,0.011998
partli,0.012264
pass,0.004430
pattern,0.009731
paul,0.004957
peano,0.022304
pecham,0.013297
peckham,0.013003
peer,0.007952
peirc,0.029573
peopl,0.018865
percent,0.005727
percept,0.012140
perceptu,0.009120
perform,0.012027
peri,0.011489
period,0.027108
pernici,0.011551
persecut,0.008219
person,0.011195
perspect,0.005103
perspectiv,0.012186
perspectivist,0.013378
pervas,0.008967
phd,0.022108
phenomena,0.059897
phenomenon,0.011747
philosoph,0.065074
philosopherphysicist,0.013463
philosophi,0.067976
phusi,0.013073
physeo,0.012690
physi,0.011370
physic,0.052244
physicist,0.013500
pivot,0.008498
place,0.003225
plan,0.004318
planck,0.008658
plant,0.009902
plato,0.007512
play,0.012352
plini,0.009386
poem,0.008417
poetri,0.007956
point,0.029773
poison,0.007971
polici,0.054864
polit,0.025055
politician,0.027367
pope,0.016040
popper,0.046729
popul,0.004057
populac,0.009025
popular,0.012240
posit,0.020279
possess,0.010249
possibl,0.016677
post,0.005534
potenti,0.012701
potentia,0.013297
power,0.006693
practic,0.052483
preced,0.006195
precept,0.010231
precis,0.010773
predict,0.047849
predomin,0.007460
prefer,0.005007
present,0.003269
preserv,0.022048
presocrat,0.019958
press,0.003848
pressur,0.004922
presum,0.007300
prevail,0.006918
prevent,0.004675
primarili,0.004470
principia,0.009076
principl,0.007851
print,0.006141
priori,0.008765
probabl,0.009385
problem,0.020985
process,0.018549
product,0.006816
profession,0.005299
program,0.008227
programm,0.012571
progress,0.008914
project,0.008093
promin,0.005128
promis,0.006145
promot,0.023325
proof,0.020003
proper,0.017601
properli,0.013754
proport,0.005406
propos,0.004034
prove,0.020188
provid,0.017409
pseudosci,0.029937
psycholog,0.016723
psychologist,0.007493
ptolemi,0.053370
public,0.062576
publish,0.029038
pupil,0.008530
pure,0.010342
purport,0.009248
purpos,0.012761
pursuanc,0.012422
pursuit,0.022119
pyramid,0.008787
quantum,0.013191
quarrelsom,0.012937
quarter,0.006802
question,0.016426
race,0.012849
radioact,0.008025
rais,0.004823
rang,0.011776
rapid,0.005667
rare,0.005381
ration,0.022543
raw,0.013531
razor,0.010231
reach,0.008429
read,0.003602
readership,0.011076
real,0.004304
realism,0.008091
realiti,0.022566
realm,0.013103
realworld,0.008617
reason,0.018611
receiv,0.003962
recent,0.007182
recherch,0.010888
recogn,0.004447
recognit,0.005684
reconceptu,0.012100
record,0.008743
recruit,0.007646
red,0.017246
refer,0.018665
reflect,0.004457
regard,0.007958
reject,0.010225
rel,0.011156
relat,0.011185
relationship,0.007917
releas,0.005183
relev,0.005424
reliabl,0.012403
religion,0.005282
remain,0.010290
remark,0.006361
ren,0.008127
renaiss,0.020574
replac,0.012679
report,0.003986
repres,0.006804
represent,0.005232
reproduc,0.007036
requir,0.019590
rescu,0.008230
research,0.154776
researchori,0.012372
resign,0.007087
resolv,0.006024
resourc,0.008397
respect,0.011832
respond,0.005593
respons,0.003597
rest,0.004859
restrict,0.009613
result,0.031779
retain,0.005454
retina,0.010737
reveal,0.005455
review,0.004647
revolut,0.015413
richard,0.005123
rigor,0.006983
rise,0.008347
rod,0.008967
roger,0.020865
role,0.018166
roman,0.005563
royal,0.016417
rule,0.015143
run,0.009115
russel,0.007269
s,0.008824
sack,0.008630
sahl,0.011459
said,0.004261
salmonella,0.012277
sander,0.009417
satellit,0.007236
savan,0.012634
say,0.004493
scam,0.011285
scarc,0.007942
scene,0.014942
scholar,0.015801
scholast,0.008983
scholastic,0.019875
school,0.021098
scienc,0.610446
sciencescientist,0.013463
scientif,0.308844
scientifiqu,0.012058
scientist,0.099596
scope,0.006097
search,0.031851
seek,0.018615
seen,0.008138
select,0.009019
selfconsist,0.011052
selfcritic,0.012472
sens,0.029873
sensat,0.008454
separ,0.003789
serv,0.008257
set,0.012851
settl,0.005894
sevil,0.010980
sex,0.014362
shall,0.015141
shape,0.004943
share,0.008083
sharp,0.007009
sharpli,0.008159
shouldnt,0.011861
shown,0.015004
shut,0.008665
signific,0.011054
similar,0.010340
similarli,0.005468
simonton,0.013003
simpl,0.004586
simpleton,0.013219
simpli,0.004710
simul,0.012543
sinc,0.008659
singl,0.019314
singlecaus,0.013297
sir,0.006515
situat,0.013332
sixth,0.007663
skeptic,0.022807
smartphon,0.010578
smith,0.012231
social,0.029375
societi,0.043397
socrat,0.044554
solar,0.006885
solut,0.004938
solv,0.005312
someth,0.016649
sometim,0.015030
sought,0.016869
sourc,0.003630
space,0.008900
spain,0.006133
span,0.006667
speci,0.010617
special,0.018050
specif,0.017267
specul,0.024021
sphere,0.019378
st,0.004653
standard,0.003887
stanovich,0.026146
state,0.017679
statist,0.014206
steadili,0.007614
stem,0.006488
step,0.004894
stimul,0.006459
stood,0.007764
strict,0.013414
strictest,0.011459
strictli,0.006807
stronger,0.006949
stroud,0.012525
structur,0.006744
struggl,0.012101
student,0.009825
studi,0.070160
style,0.005947
subatom,0.009174
subcommun,0.013219
subdivid,0.007909
subject,0.003671
succeed,0.005901
success,0.022967
suddenli,0.008563
suitabl,0.006407
summar,0.014703
summari,0.006946
sun,0.006403
supernatur,0.025160
support,0.013727
surviv,0.009956
synthesi,0.012888
syriac,0.010933
systemat,0.027573
tabl,0.005555
taken,0.012679
tamper,0.011178
tangenti,0.011399
tank,0.007995
tax,0.005424
team,0.006027
techn,0.011937
technic,0.015400
techniqu,0.008871
technolog,0.033293
teleolog,0.009636
tend,0.004740
tenuretrack,0.013145
term,0.035457
test,0.033031
testabl,0.019047
text,0.014938
textbook,0.006506
th,0.059576
thcenturi,0.006655
themselv,0.004281
theorem,0.012941
theoret,0.014198
theori,0.131951
theorist,0.006727
therefor,0.007846
thermodynam,0.007408
thi,0.053974
thing,0.064300
think,0.015017
thinker,0.006977
thirteenth,0.009731
thorough,0.008658
thought,0.028636
thousand,0.010180
thu,0.013496
time,0.017877
today,0.016586
togeth,0.003980
toledo,0.011231
tool,0.004621
topic,0.004579
topolog,0.007942
total,0.008054
touchston,0.011459
tradit,0.007269
transact,0.006215
translat,0.029815
transmit,0.007072
transpar,0.007387
transport,0.004948
treat,0.020217
tri,0.018173
trial,0.006586
tribe,0.007105
trigonometri,0.010559
troubl,0.007578
true,0.028041
trust,0.006309
truth,0.024565
turn,0.007980
twothird,0.007909
type,0.062466
typic,0.011617
ultim,0.004992
uncertain,0.007630
uncertainti,0.006717
unchang,0.007696
uncrit,0.011258
underli,0.005481
undermin,0.007650
understand,0.023508
undertak,0.007835
unexpect,0.008429
uniform,0.006692
unit,0.011645
univers,0.038864
unjustifi,0.010933
unlik,0.009613
unobserv,0.009897
unrealist,0.009448
unreli,0.018497
unsatisfactori,0.010362
upbring,0.011489
updat,0.006992
urban,0.006054
usag,0.006054
usaibia,0.013463
use,0.073115
user,0.006403
usual,0.003473
util,0.005221
utter,0.009512
valid,0.011024
vannevar,0.012017
vari,0.004255
variabl,0.005276
variat,0.005410
variou,0.023002
vast,0.005796
vein,0.008596
verbal,0.008304
veri,0.009889
verif,0.008787
verifi,0.021890
version,0.015147
vie,0.010578
view,0.025896
viewpoint,0.007598
viii,0.009192
virtual,0.005708
vision,0.037512
vital,0.006446
vitello,0.013463
voic,0.006879
wa,0.087568
war,0.008200
water,0.004452
waterfil,0.012872
way,0.030863
weakest,0.011285
weapon,0.006409
weight,0.005817
wellb,0.007721
wellfund,0.012372
wellintend,0.013297
west,0.004956
western,0.013025
wherea,0.005008
whewel,0.011231
whilst,0.007456
whitehead,0.009512
wide,0.011153
wider,0.013144
widespread,0.011091
wikibooksorg,0.013463
wilhelm,0.007266
william,0.004625
wilson,0.007210
wisdom,0.015285
witelo,0.012186
wittgenstein,0.009965
women,0.049441
word,0.028047
work,0.039616
worklif,0.012472
world,0.017558
worldchang,0.013219
worri,0.008504
worship,0.007822
write,0.009026
written,0.004570
wrong,0.013578
wrote,0.009835
xray,0.007872
year,0.011652
young,0.005359
zermelo,0.011752
zermelofraenkel,0.011683
ziman,0.025499
