abil,0.011091
abl,0.006788
absenc,0.004940
ac,0.007318
academ,0.004024
access,0.003718
achiev,0.003380
acid,0.005414
acoust,0.007507
activ,0.008044
actual,0.003439
actuat,0.009289
addit,0.010874
adequ,0.005637
advanc,0.016594
advisor,0.006884
advoc,0.009225
advocaci,0.007273
affili,0.006147
afm,0.010596
age,0.006730
agenc,0.008454
agenda,0.006260
agent,0.004581
aim,0.007674
airborn,0.008021
aircraft,0.005968
albert,0.005461
ald,0.012814
alex,0.007931
alfr,0.005506
allotrop,0.010259
allow,0.005344
alongsid,0.005588
alreadi,0.007829
alter,0.009640
aluminium,0.007529
analog,0.014405
analysi,0.003239
analyz,0.004517
andor,0.004702
andrew,0.005457
ani,0.007104
anisotrop,0.009655
anoth,0.015507
anthoni,0.006345
antibacteri,0.010442
anticip,0.011953
appar,0.004587
appli,0.011490
applianc,0.008488
applic,0.096278
approach,0.049482
approv,0.009232
approxim,0.003662
arbitrari,0.011605
architect,0.007144
area,0.021351
argu,0.003574
aris,0.004383
arrang,0.026285
art,0.004111
arthur,0.005481
articl,0.006387
asbesto,0.009810
asbestosi,0.012422
assembl,0.071315
assess,0.004577
associ,0.010500
atom,0.169159
atombyatom,0.013339
atomiclevel,0.012814
attempt,0.009195
attent,0.013145
attract,0.004323
august,0.004083
automat,0.016457
autumn,0.007150
avail,0.009854
averag,0.004013
awar,0.004861
award,0.004737
bacteria,0.012338
bacteriostat,0.012109
ball,0.019908
bandag,0.010442
base,0.016390
basepair,0.031328
basi,0.003347
basic,0.003259
beam,0.036625
bear,0.005028
beard,0.008664
becaus,0.009756
becom,0.018156
began,0.006471
bell,0.006273
benefici,0.005946
berkeley,0.018985
big,0.004906
bigpictur,0.012422
billion,0.013807
billionth,0.010596
binnig,0.023947
biolog,0.038429
biomark,0.009746
biomateri,0.019963
biomed,0.014230
biomimet,0.029945
biomimicri,0.011256
biominer,0.011027
biomolecul,0.008042
bionanotechnolog,0.012422
bionic,0.010018
biosensor,0.022512
biotechnolog,0.007058
bispeptid,0.013339
bolt,0.009289
bond,0.009256
bone,0.006077
book,0.006159
bottomup,0.051989
bound,0.004848
bovin,0.009243
bowl,0.008231
brain,0.005185
breakthrough,0.006914
breath,0.006966
broad,0.008704
bucki,0.011732
build,0.013715
built,0.008078
bulk,0.017232
bulkscal,0.013339
burden,0.006185
c,0.006202
california,0.004970
calvin,0.008085
cambridg,0.004298
cancer,0.005968
capabl,0.021367
car,0.005734
carbon,0.058180
carboncarbon,0.010216
care,0.004076
carlo,0.006175
carri,0.006996
carv,0.007192
catalyst,0.070863
catalystin,0.012814
catalyt,0.016658
categori,0.004140
caus,0.005793
cell,0.018669
cellular,0.006004
center,0.020767
cerium,0.010394
certain,0.002970
challeng,0.007753
chang,0.004893
character,0.004116
characteris,0.006505
characterist,0.007481
cheaper,0.020962
chemic,0.045454
chemistri,0.023782
chief,0.004519
child,0.005212
cho,0.008864
chromosom,0.007052
citi,0.003747
classic,0.003585
classifi,0.004389
clean,0.006352
cleaner,0.018487
clear,0.004039
clearli,0.009759
cloth,0.005673
coat,0.012858
cockroach,0.010094
cofound,0.007456
coin,0.008988
colleagu,0.005866
colloid,0.009685
combat,0.005696
combust,0.007499
come,0.009312
commerci,0.024948
commodor,0.009436
common,0.005248
compani,0.003427
compar,0.016051
comparison,0.004551
complementari,0.006331
complet,0.005995
complex,0.028584
compon,0.033641
comput,0.007193
concept,0.027260
conceptu,0.010448
concern,0.015236
conclud,0.004439
conduct,0.003720
configur,0.005996
confoc,0.010958
conform,0.011981
consequ,0.003690
consid,0.002531
consist,0.002905
consol,0.008634
construct,0.020167
consum,0.008643
contain,0.003113
contest,0.005710
context,0.007636
contrast,0.003666
contribut,0.006371
control,0.028838
controversi,0.008554
convent,0.016812
converg,0.005717
cool,0.006080
copi,0.005232
copolym,0.010891
copper,0.006027
cornel,0.006864
cosmet,0.015962
cost,0.011448
counter,0.005868
countless,0.008063
countri,0.002856
cover,0.007147
cow,0.007485
cowork,0.008778
creat,0.029204
creation,0.003950
critic,0.003192
crystal,0.006089
culmin,0.005751
curl,0.009243
current,0.022230
cut,0.004608
d,0.021083
damag,0.009368
danish,0.006864
data,0.003464
databas,0.005078
davi,0.006293
david,0.003855
deal,0.006993
debat,0.020204
decad,0.003945
decreas,0.017097
deep,0.004832
defin,0.008936
definit,0.020592
degre,0.007109
delay,0.005543
deliber,0.011620
deliveri,0.018820
demonstr,0.011704
depend,0.005727
deposit,0.030832
descend,0.005253
describ,0.010686
descript,0.015871
design,0.025505
desir,0.016340
desktop,0.008559
destroy,0.004539
destruct,0.005137
determin,0.003014
determinist,0.006929
develop,0.049928
devic,0.092170
devis,0.005954
diamet,0.014184
diblock,0.013055
diesel,0.026385
differ,0.011505
differenti,0.004395
difficult,0.003785
difficulti,0.004687
diffus,0.005929
dilemma,0.007365
dimens,0.019976
dimension,0.033572
dioxid,0.019133
dip,0.017268
direct,0.014411
directli,0.003487
director,0.005219
discov,0.007644
discoveri,0.008130
discret,0.005119
discuss,0.010625
diseas,0.022734
disinfect,0.010056
display,0.004933
dispos,0.006421
distanc,0.004622
distinct,0.006686
distinguish,0.003853
divers,0.008349
dkk,0.011430
dmoz,0.005748
dna,0.028009
doe,0.002936
dollar,0.015215
doomsday,0.010394
dot,0.007014
doublehelix,0.011176
dr,0.010770
drawback,0.008446
drawn,0.005122
drew,0.005982
drexler,0.080672
drive,0.004663
drop,0.009528
drug,0.015358
dual,0.006119
durabl,0.007311
dure,0.004997
earli,0.005227
earlier,0.003861
earliest,0.004367
earth,0.004102
easi,0.005307
easier,0.005595
easili,0.004477
econom,0.002859
ecosystem,0.005927
edinburgh,0.007109
effect,0.027925
effici,0.004091
electr,0.004320
electron,0.034983
embed,0.006077
emerg,0.026793
emphasi,0.004739
employ,0.006932
enabl,0.008288
enact,0.005792
encephalopathi,0.010442
endoflif,0.011340
energi,0.018291
engag,0.004138
engin,0.060256
enhanc,0.004840
ensur,0.004293
entiti,0.004433
environ,0.003464
environment,0.012603
envis,0.007138
enzym,0.006352
enzymesubstr,0.011732
epitaxi,0.011430
equal,0.003558
equival,0.004209
era,0.012386
eric,0.012414
especi,0.006068
establish,0.005577
estim,0.003677
etch,0.009843
ethic,0.004809
europ,0.003543
european,0.006509
everi,0.003270
everyth,0.005203
evolv,0.004289
exampl,0.028084
exchang,0.003694
exclus,0.004464
exemplar,0.009032
exemplifi,0.006657
exhaust,0.024441
exhibit,0.009866
exist,0.007795
expens,0.009334
experi,0.003213
experiment,0.008729
expert,0.005161
exponenti,0.006290
expos,0.005234
exposur,0.017423
extend,0.006893
extens,0.010586
extern,0.004086
fabric,0.032686
face,0.003922
fact,0.003317
factori,0.005717
fall,0.007134
farm,0.005213
fascin,0.007791
fast,0.016048
faster,0.010903
favor,0.004316
fe,0.016324
feasibl,0.006349
featur,0.007068
featureori,0.026679
femtotechnolog,0.012258
fert,0.012606
fewer,0.005541
feynman,0.015343
fiber,0.006787
fibrosi,0.009746
field,0.022830
film,0.005299
fine,0.005586
fit,0.009515
flat,0.005976
flavor,0.007981
fli,0.005832
flourish,0.005751
flush,0.009778
focus,0.007275
fold,0.006661
follow,0.004470
food,0.016087
foot,0.006071
forc,0.016805
foresight,0.009072
form,0.010679
formal,0.003496
forth,0.005680
forthcom,0.008460
foundat,0.003643
fraction,0.005469
framework,0.012010
free,0.003191
fuel,0.010356
fulleren,0.019556
fume,0.030168
fumesand,0.013339
function,0.017165
fund,0.011558
fundament,0.007156
furnitur,0.008279
fusion,0.006513
futur,0.017017
game,0.004795
gap,0.005439
garner,0.007791
gass,0.009778
gear,0.007092
gecko,0.010018
gener,0.014055
genet,0.004746
genu,0.007058
gerber,0.011176
gerd,0.009655
giant,0.012220
given,0.005605
global,0.003710
goal,0.015374
gold,0.009718
golf,0.008812
gossard,0.012814
govern,0.013286
grant,0.004283
graphen,0.010651
great,0.003149
grnberg,0.012422
group,0.007561
grow,0.010749
growth,0.010611
guid,0.003950
ha,0.024222
hairless,0.011430
hall,0.005301
hand,0.006924
handl,0.005081
hard,0.004586
harder,0.006945
harm,0.005387
harri,0.005876
head,0.007214
heal,0.006849
health,0.046747
heart,0.005129
heinrich,0.006935
help,0.009177
hi,0.010022
high,0.002829
higher,0.006644
highvis,0.013339
hit,0.005785
ho,0.007167
home,0.003990
hope,0.014273
household,0.005558
howev,0.009071
human,0.016153
hybrid,0.005838
hydrocarbon,0.007228
hydrogen,0.005787
ibm,0.007185
idea,0.003136
identifi,0.003377
imag,0.004302
impact,0.003829
implement,0.011671
implic,0.024343
implicitli,0.007406
import,0.011916
imposs,0.004724
includ,0.027157
inclus,0.005756
incorpor,0.003837
increas,0.018809
independ,0.002870
indic,0.006875
individu,0.014411
induc,0.005533
industri,0.009568
industrialscal,0.011732
ineffici,0.006470
inert,0.007902
infanc,0.008406
inflamm,0.008634
inform,0.005556
infus,0.016043
inhal,0.017298
initi,0.021253
innov,0.009680
innovationsfonden,0.013339
inorgan,0.006533
inquiri,0.005573
insolubl,0.008918
inspir,0.004623
instanc,0.003784
institut,0.008870
insuffici,0.006335
insur,0.010774
interact,0.006973
interfac,0.006083
interferometri,0.009156
intermolecular,0.009093
invent,0.018187
invest,0.015914
investig,0.003915
involv,0.005417
ion,0.024238
iron,0.005004
issu,0.002998
item,0.015377
japan,0.004620
john,0.003178
join,0.004156
k,0.004236
key,0.003555
killer,0.008419
kind,0.003722
know,0.004233
known,0.007004
kroto,0.010216
lab,0.006104
label,0.005027
laboratori,0.018964
lack,0.003562
larger,0.025813
late,0.003234
launch,0.009410
law,0.002814
lawrenc,0.006250
lay,0.005257
layer,0.016133
lead,0.005574
leader,0.004085
led,0.012060
lee,0.006352
length,0.009270
letter,0.004611
level,0.002793
lifeform,0.008955
light,0.007702
like,0.004990
limit,0.017118
lineag,0.006914
link,0.004062
lipid,0.007358
list,0.002597
lithographi,0.085664
logic,0.004521
longer,0.011558
longterm,0.004916
look,0.003857
low,0.003580
lower,0.003451
lung,0.021916
lyon,0.007981
machin,0.054125
macro,0.008139
macroscal,0.020432
macroscop,0.013384
mad,0.007614
magnet,0.010967
magnetoresist,0.012258
magnetoresistancebas,0.013339
main,0.002957
major,0.007435
make,0.014252
man,0.008266
manag,0.003345
mani,0.014779
manipul,0.045015
manner,0.004508
manufactur,0.049003
manufactureridentifi,0.013055
map,0.004273
marbl,0.008367
market,0.010490
marvin,0.008304
mass,0.003830
massachusett,0.006125
materi,0.091657
matter,0.035138
maxim,0.015938
maynard,0.007606
mbe,0.055138
mean,0.005145
meanwhil,0.005648
mechan,0.031046
mechanosynthesi,0.024516
media,0.004284
medic,0.008603
medicin,0.008907
mem,0.010708
member,0.002980
memori,0.004859
mere,0.004595
merg,0.005626
mesothelioma,0.012606
metal,0.004861
meter,0.012574
method,0.020251
methodolog,0.009889
mice,0.015198
micro,0.007647
microelectromechan,0.011256
microenviron,0.010543
microfabr,0.010492
microprocessor,0.008918
microscop,0.090457
microscopi,0.029406
microtechnolog,0.011176
mid,0.004651
mihail,0.011848
militari,0.003778
million,0.007338
mimic,0.015655
miniaturis,0.012258
minim,0.014790
minski,0.008544
mode,0.005258
modern,0.011207
modifi,0.004557
molecul,0.080704
molecular,0.180653
monoxid,0.017392
montemagno,0.013339
moral,0.004673
motion,0.004713
motor,0.012049
mutual,0.004927
mycoplasma,0.010543
nano,0.030168
nanobioelectron,0.013339
nanobot,0.022512
nanocar,0.013339
nanocellulos,0.012814
nanochain,0.013339
nanodevic,0.036774
nanoelectromechan,0.025629
nanoelectron,0.043068
nanofib,0.025212
nanoimprint,0.026679
nanoion,0.040019
nanolithographi,0.053358
nanomachin,0.011524
nanomanipul,0.013339
nanomanufactur,0.013339
nanomateri,0.204328
nanomechan,0.025629
nanomedicin,0.021916
nanomet,0.054559
nanomotor,0.026679
nanoobject,0.012814
nanoparticl,0.179674
nanoparticlebas,0.013339
nanophoton,0.012606
nanopillar,0.013339
nanorobot,0.033769
nanorod,0.012109
nanoscal,0.182903
nanoscaleemerg,0.013339
nanostructur,0.031328
nanosystem,0.128146
nanotech,0.034875
nanotechnolog,0.689374
nanotitanium,0.013339
nanotoxicolog,0.013339
nanotub,0.109020
nanowir,0.011732
nation,0.015530
natur,0.014962
necessari,0.003722
necessarili,0.004572
need,0.005616
nem,0.011100
neurolog,0.007069
new,0.022952
newer,0.006859
newli,0.004914
news,0.005098
newspap,0.005782
nextlarg,0.012814
nitrogen,0.006448
nm,0.060666
nobel,0.021750
nonbiolog,0.019821
noncoval,0.010216
norio,0.026110
normal,0.003723
notabl,0.003584
novel,0.005170
nox,0.011524
nuclear,0.004773
nucleic,0.007671
number,0.004673
object,0.003193
observ,0.006166
occup,0.009734
occur,0.008903
odor,0.008812
offic,0.003759
onli,0.006450
onlin,0.004088
opaqu,0.008502
open,0.003197
oper,0.008840
optic,0.011239
optim,0.004954
optimis,0.008618
order,0.002574
organ,0.005095
origamibas,0.013339
origin,0.005177
oscil,0.006759
osteoclast,0.012109
outdoor,0.008419
outlin,0.004546
overregul,0.011973
oversight,0.007399
overwhelm,0.006263
oxid,0.037186
oxygen,0.005687
p,0.003565
pace,0.006304
packag,0.005921
paint,0.011865
parallel,0.014520
particip,0.007370
particl,0.020084
particular,0.008771
passiv,0.012873
patent,0.006233
pattern,0.007971
paull,0.012422
pen,0.014970
peopl,0.005150
percept,0.004972
perform,0.009851
perhap,0.008684
person,0.003056
perspect,0.008359
peter,0.004259
pharmaceut,0.006363
phenomena,0.017840
physic,0.027509
physicist,0.005529
picotechnolog,0.012422
piec,0.010316
pit,0.007399
place,0.002641
platform,0.005720
platinum,0.033626
play,0.006745
plenti,0.007345
plural,0.006007
point,0.002709
polaris,0.008634
polit,0.002931
polym,0.007019
popular,0.010025
posit,0.019378
possibl,0.021856
poster,0.008696
potent,0.008406
potenti,0.034679
power,0.002741
practition,0.005621
precautionari,0.010018
preced,0.005074
precis,0.017649
precursor,0.011671
premis,0.006283
prepar,0.004344
present,0.008035
principl,0.019292
privat,0.003840
prize,0.019982
probabl,0.003843
probe,0.039970
procedur,0.004664
process,0.017725
produc,0.019509
product,0.053043
programm,0.015445
progress,0.025555
project,0.029829
promin,0.004200
promis,0.005033
promot,0.003821
pronounc,0.006470
properti,0.032432
propos,0.023133
prospect,0.005692
protein,0.005441
provid,0.004753
public,0.026976
publicli,0.011464
publish,0.005946
pulmonari,0.008918
purpos,0.003484
quantiti,0.004363
quantum,0.043220
quantumrealm,0.011848
quarter,0.005571
quat,0.024516
question,0.003363
r,0.003584
racket,0.009876
rais,0.019754
rang,0.022508
rare,0.004408
rat,0.007109
ratio,0.009661
razor,0.008380
reach,0.006904
reaction,0.004356
realm,0.005366
reason,0.003048
receiv,0.006491
recent,0.011766
recognit,0.018625
recommend,0.005007
recycl,0.007228
reduc,0.012873
reduct,0.014664
refer,0.011891
reflect,0.003650
regard,0.013037
regim,0.009900
regul,0.044239
regulatori,0.017638
reject,0.004187
rejeski,0.013339
relat,0.013743
relax,0.006493
releas,0.012737
relev,0.004443
remov,0.007844
renown,0.007247
report,0.009795
reproduct,0.005770
requir,0.008022
research,0.079923
resist,0.004402
resolut,0.005196
resorpt,0.012109
respond,0.004581
respons,0.011786
result,0.011831
revers,0.004676
revolut,0.012625
richard,0.012590
rise,0.003418
risk,0.029259
road,0.004934
roadmap,0.009542
robert,0.003735
roco,0.013055
rohrer,0.011256
role,0.005951
room,0.005510
rotaxan,0.011973
routin,0.006098
royal,0.008964
rule,0.003101
s,0.014456
safeti,0.027378
said,0.006980
sam,0.007544
sampl,0.010125
save,0.004824
scaffold,0.017837
scale,0.049985
scan,0.101746
scenario,0.006153
school,0.003456
scienc,0.017143
scientif,0.017092
scientist,0.011653
scotland,0.006385
search,0.004348
seaton,0.012422
second,0.002788
section,0.004209
sector,0.004453
seed,0.006027
seek,0.034307
seen,0.006666
self,0.005838
selfassembl,0.067589
selfsuffici,0.007138
semiconductor,0.029653
sens,0.003495
seriou,0.009518
serv,0.003381
set,0.002631
settl,0.004828
sever,0.009859
shape,0.004049
shift,0.004116
shortcom,0.007921
signific,0.009054
silica,0.009072
silicon,0.021649
silver,0.035466
similar,0.005646
simpl,0.003756
sinc,0.004728
singl,0.009492
singlemolecul,0.023697
sit,0.006250
size,0.055227
sizebas,0.013339
skin,0.005996
slow,0.004881
small,0.002893
smaller,0.020100
smallest,0.012306
smalley,0.033300
socal,0.009541
socialis,0.009876
societ,0.006648
societi,0.011848
sock,0.022352
solar,0.016918
sole,0.004799
solid,0.005081
solidst,0.016866
solubl,0.007560
sometim,0.009233
sophist,0.005439
sort,0.004959
sound,0.004977
space,0.003645
spark,0.006256
special,0.005913
specif,0.016972
specul,0.009837
spintron,0.020885
spongiform,0.010492
stabl,0.004652
stage,0.008257
stainresist,0.012606
stakehold,0.007599
start,0.002992
state,0.006206
statist,0.003878
stem,0.010628
step,0.004009
sticki,0.008588
stifl,0.008778
stm,0.010828
stochast,0.006561
straighter,0.012422
strategi,0.004404
stream,0.005666
strengthen,0.005347
stress,0.009696
stresssensit,0.013339
stricter,0.008544
structur,0.027623
studi,0.012492
sub,0.008341
subfield,0.024345
subsequ,0.007379
substanc,0.004970
substitut,0.005253
substrat,0.007179
success,0.012541
suffici,0.004370
suggest,0.013239
suitabl,0.020993
summer,0.005469
sunscreen,0.034573
superparamagnet,0.013055
support,0.002811
supramolecular,0.037159
surfac,0.068495
surfacetovolum,0.012258
synthesi,0.031670
synthet,0.017591
taken,0.003461
talk,0.005087
taniguchi,0.024516
tape,0.007713
target,0.008804
team,0.009874
technic,0.004204
techniqu,0.061766
technolog,0.054541
telephon,0.006463
tend,0.003882
tenni,0.017298
term,0.011170
testifi,0.008063
textil,0.006392
thalidomid,0.010828
thank,0.006501
themselv,0.010520
theoret,0.007753
therapeut,0.006976
therefor,0.003213
thermal,0.006062
thermodynam,0.006068
thi,0.030062
thing,0.003761
threshold,0.006585
thu,0.008291
tighter,0.008712
time,0.008367
timeconsum,0.008649
tip,0.026376
tissu,0.011426
titanium,0.008460
today,0.010189
togeth,0.003260
tool,0.015143
topdown,0.050762
toxic,0.006537
tradit,0.005954
trait,0.005601
transfersom,0.011732
transmiss,0.005840
transpar,0.012101
transport,0.008106
treatment,0.004349
trouser,0.011100
tube,0.013066
tunnel,0.028553
turn,0.009805
twoyear,0.007688
type,0.005685
typic,0.006343
uc,0.008419
ucla,0.008696
uk,0.004333
ultim,0.012267
ultradeform,0.013339
ultraviolet,0.007385
unawar,0.007688
understand,0.006418
unintend,0.007971
union,0.003513
uniqu,0.008062
unit,0.002384
univers,0.002448
unlik,0.003937
unpreced,0.006714
unsustain,0.007827
upper,0.004774
usa,0.010556
usag,0.004959
use,0.115911
util,0.008554
vapor,0.007385
vari,0.003485
variat,0.004431
varieti,0.007099
variou,0.008074
varnish,0.010708
vast,0.004747
vegabbc,0.013339
veloc,0.006217
veri,0.010800
version,0.008271
vesicl,0.008812
video,0.010407
view,0.009090
virus,0.007069
visual,0.004988
voltag,0.015426
volum,0.004049
wa,0.021324
warrant,0.007292
wash,0.007399
wast,0.011342
water,0.010941
watsoncrick,0.034022
wavelength,0.007161
way,0.007583
week,0.004969
welldefin,0.014094
wellunderstood,0.009910
wide,0.009135
widescal,0.010303
widespread,0.004542
wilson,0.011811
wing,0.006436
won,0.004786
woodrow,0.017392
word,0.003281
work,0.009271
worker,0.004401
write,0.007393
xiii,0.008728
xray,0.006448
y,0.005034
year,0.002386
yield,0.004618
zettl,0.013339
zinc,0.007448
zurich,0.008993
