abl,0.001733
abov,0.005271
absorb,0.005487
abund,0.016699
acceler,0.010558
access,0.001898
account,0.003232
acetylen,0.005562
achiev,0.001726
acid,0.049772
acidform,0.006184
acr,0.003673
actinid,0.004756
acut,0.003601
addit,0.002776
aerob,0.004573
affin,0.003371
age,0.001718
agre,0.002069
agricultur,0.002039
agt,0.006812
air,0.018974
alchem,0.004845
alcohol,0.003150
alexand,0.002629
alkali,0.004273
allotrop,0.130987
allotropesth,0.007219
allow,0.001364
alloy,0.007637
alon,0.002346
alpha,0.012707
alphaparticl,0.006667
altern,0.001763
altogeth,0.003402
aluminum,0.004313
alway,0.003691
amino,0.010657
amorph,0.009000
amphid,0.007219
amphigen,0.007219
analog,0.009809
analysi,0.001654
ancient,0.009942
anemia,0.004721
ani,0.004838
anim,0.005762
anion,0.004449
anorexia,0.005384
anoth,0.003959
antimoni,0.014537
antiqu,0.002839
antistat,0.005937
applic,0.019668
approxim,0.003740
arsen,0.011691
artifici,0.004635
assembl,0.002142
associ,0.001340
assum,0.001901
atmospher,0.008065
atom,0.071993
atp,0.004096
attach,0.010393
attempt,0.004696
au,0.003935
avail,0.003355
averag,0.008199
b,0.001817
bacteria,0.006301
bad,0.002815
base,0.001195
batteri,0.003717
bayden,0.007219
becaus,0.012457
becom,0.001324
bed,0.003574
begin,0.001613
behavior,0.003882
belgium,0.003248
bent,0.004204
beryllium,0.004819
berzeliu,0.019665
beta,0.003384
better,0.003732
bi,0.003880
bibl,0.003318
billion,0.016453
biltz,0.007219
binari,0.003304
biochemistri,0.003289
biolog,0.013738
birth,0.002438
bismuth,0.019329
black,0.002233
bleed,0.004425
blood,0.008084
blue,0.002949
bodi,0.009963
boil,0.003701
bombard,0.014692
bond,0.061453
bone,0.003103
borch,0.006990
borid,0.019032
born,0.004675
boron,0.013004
boronchalcogen,0.007219
boronhydrogen,0.007219
brazil,0.002957
break,0.002281
breath,0.010673
brittl,0.004554
bromid,0.020543
bromin,0.004887
bronz,0.003330
bronzebrass,0.007219
burn,0.005673
byproduct,0.007346
c,0.003167
cadmium,0.009561
calaverit,0.021659
calcium,0.010927
came,0.003624
canada,0.002373
captur,0.002275
carbanion,0.005707
carben,0.006115
carbon,0.021610
carl,0.002767
case,0.002771
catalyst,0.003619
cation,0.008444
caus,0.011834
cdrw,0.006544
cell,0.004767
celsiu,0.009072
center,0.005303
centuri,0.007116
certain,0.004550
chain,0.007428
chalcogen,0.569972
chalcogenhalogen,0.007219
chalcogenid,0.096572
chalcogenlov,0.006990
chalcogenoketon,0.014439
chalcogenol,0.036099
chalcogenophosphin,0.007219
chalcogenphosphoru,0.014439
chalcohalid,0.007219
chalcophil,0.026669
chalko,0.007219
chang,0.002499
character,0.002102
characterist,0.001910
chemic,0.042208
chemist,0.003185
chemistri,0.014575
chlorid,0.015774
chlorin,0.004002
chromium,0.004527
claim,0.001682
clean,0.003244
close,0.001483
cluster,0.006082
coin,0.002295
collect,0.001616
color,0.005522
combin,0.003161
come,0.012682
commerci,0.004247
common,0.016083
commonli,0.014726
complic,0.002512
compon,0.009545
compound,0.164856
concentr,0.004338
concret,0.003036
conduct,0.001899
confer,0.002182
configur,0.003062
confirm,0.002389
conform,0.003059
consid,0.001292
consist,0.007418
constitu,0.005191
consum,0.006621
contain,0.041347
content,0.002187
context,0.001950
convert,0.009038
convuls,0.005061
cool,0.006210
copper,0.027705
copperform,0.007219
core,0.007062
correspond,0.002012
cosmic,0.003391
cough,0.004977
count,0.002528
countri,0.001458
coupl,0.002565
cow,0.007645
creat,0.009491
creation,0.002017
critic,0.001630
crust,0.033227
crystal,0.031098
crystallis,0.005116
cubic,0.015305
cui,0.005498
cuii,0.006115
curi,0.017669
curium,0.017375
cut,0.002353
cyanid,0.004931
cyclic,0.003443
cystein,0.005044
daili,0.002592
damag,0.002392
danger,0.010415
data,0.001769
day,0.006691
dberein,0.006344
dcto,0.007219
decay,0.030480
decid,0.002160
declin,0.002104
decreas,0.006549
defin,0.001521
denser,0.004474
densiti,0.002675
deplet,0.006918
deposit,0.002624
depth,0.002910
deriv,0.001685
dermat,0.005748
describ,0.001364
despit,0.003759
detect,0.002434
devic,0.007432
diarrhea,0.005079
diatom,0.008494
dichalcogen,0.014439
dichalcogenoimidodiphosph,0.007219
dichlorid,0.019314
did,0.004936
diethyl,0.005707
differ,0.003525
difficult,0.001933
difluorid,0.006667
dihalid,0.012688
diiodi,0.007219
diiodid,0.006990
dimer,0.009309
dimethyl,0.030815
diminish,0.003001
dioxid,0.016286
dipropyl,0.007219
directli,0.001781
discov,0.025378
discoveri,0.010381
diselenium,0.014439
disk,0.011423
display,0.002519
distil,0.007940
distinguish,0.001968
distribut,0.001829
disulfur,0.006667
ditellurid,0.006812
ditellurium,0.007219
dival,0.005631
diver,0.004676
dmitri,0.004313
dna,0.002860
doe,0.004499
domain,0.002165
dorbit,0.005748
dose,0.004040
dot,0.003582
doubl,0.007362
dozen,0.003100
dri,0.005807
drown,0.004417
dure,0.001276
dvdrw,0.007219
e,0.008981
earli,0.004004
earth,0.031429
effect,0.003889
effici,0.002089
eightatom,0.007219
electr,0.004413
electrolyt,0.008520
electron,0.020099
electroneg,0.014028
electroposit,0.005596
element,0.079777
emiss,0.012667
emit,0.006681
emitt,0.004845
enzym,0.003244
equal,0.001817
equilibrium,0.005578
escap,0.002679
especi,0.006199
etymolog,0.002841
eventu,0.001896
exampl,0.003585
excess,0.005158
exclud,0.002510
excret,0.004307
exist,0.007962
experi,0.001641
explain,0.001796
explor,0.004066
exposur,0.008898
extend,0.005281
extract,0.017983
extrem,0.003828
eye,0.002668
f,0.001965
fact,0.001694
fail,0.002006
failur,0.002322
fairli,0.009069
famili,0.001848
famous,0.003466
far,0.003619
farm,0.002662
fe,0.004168
featur,0.003609
feet,0.003259
fewer,0.002830
firework,0.005027
fischer,0.004026
fission,0.004050
fluorid,0.024229
fluorin,0.004633
flux,0.003510
focus,0.001857
follicl,0.005285
follow,0.001141
food,0.004107
form,0.058906
formal,0.001785
format,0.001946
formula,0.004984
fourth,0.002490
fraction,0.005586
franz,0.003768
frasch,0.006990
frozen,0.003786
fuel,0.002644
function,0.001461
fungi,0.003454
fusion,0.009979
ga,0.007203
gadolinium,0.005411
gain,0.001736
gallium,0.004654
garlic,0.010271
gase,0.009545
gaseou,0.012035
gather,0.002487
gaylussac,0.005285
gen,0.004210
gender,0.003059
gene,0.005538
gener,0.011280
german,0.002015
germani,0.004297
germanium,0.009638
glassmak,0.012368
goddess,0.008033
goe,0.012841
gold,0.007444
gram,0.007976
gray,0.010681
greek,0.022230
group,0.034757
guess,0.004002
gunpowd,0.004210
gut,0.004091
ha,0.029499
hair,0.003397
halflif,0.008727
halid,0.054885
halochalcogen,0.007219
halogen,0.028326
hamper,0.003782
handl,0.002595
hanov,0.004901
hard,0.002342
harder,0.003547
harm,0.002751
haul,0.004721
heat,0.007549
heavi,0.002466
heavier,0.028838
heavili,0.002300
help,0.001562
herman,0.003811
hexafluorid,0.005885
hexagon,0.013527
hi,0.003838
high,0.005780
higheffici,0.006184
higher,0.005089
highest,0.004370
highli,0.011272
histor,0.003206
histori,0.001188
holmium,0.006260
hook,0.003837
howev,0.010424
hs,0.004536
hse,0.005885
human,0.019249
hundr,0.002168
hydrid,0.018493
hydrogen,0.032515
hydroxid,0.009352
impli,0.002218
import,0.004868
includ,0.020804
incorpor,0.001959
increas,0.004117
indigest,0.005562
indium,0.005333
individu,0.001472
industri,0.006515
inflamm,0.004409
ingest,0.016840
ingredi,0.007495
inhal,0.008834
initi,0.001550
insight,0.002647
instanc,0.007730
instead,0.006686
institut,0.001510
interact,0.010683
interchalcogen,0.014439
intern,0.001250
interv,0.003006
invent,0.002322
involv,0.002766
iodid,0.014933
iodin,0.008698
ion,0.034042
ionic,0.003948
iron,0.005111
irrit,0.004273
isol,0.002317
isom,0.018704
isotop,0.069141
jacob,0.006435
japan,0.004720
jn,0.004654
johann,0.003053
john,0.001623
joint,0.002512
jon,0.003983
joseph,0.007055
key,0.005448
khalk,0.006667
kill,0.004711
kilogram,0.012093
kindl,0.005079
klaproth,0.012688
klkdnz,0.007219
known,0.026232
labil,0.005562
laboratori,0.002421
lanthanid,0.009512
lanthanum,0.005044
larg,0.010059
larger,0.001883
largest,0.005911
late,0.001651
later,0.004256
latin,0.006727
latinis,0.005596
lattic,0.003557
lawrenc,0.003191
layer,0.002746
lbnl,0.006184
lead,0.001423
leav,0.004041
led,0.001539
lesion,0.004483
lethal,0.004112
leukemia,0.004665
level,0.002853
life,0.006134
ligand,0.012704
light,0.003933
lighter,0.018721
lightest,0.004806
like,0.005097
liken,0.004222
liquefi,0.004901
liquid,0.017902
list,0.001326
liter,0.005487
lithophil,0.013334
littl,0.003652
litvinenko,0.006344
liver,0.003744
livermor,0.005155
livermorium,0.091725
lomonosov,0.005155
long,0.003098
longer,0.001967
loss,0.002086
loui,0.002692
louisiana,0.004123
louisjacqu,0.006990
low,0.007313
lower,0.005288
lowlevel,0.008600
lung,0.003730
lute,0.011583
lv,0.010997
lymphopenia,0.006667
m,0.001706
machin,0.002303
macropolyhedr,0.007219
magic,0.003326
mainstream,0.002895
major,0.003797
make,0.019411
manganes,0.004307
mani,0.010783
manipul,0.002554
manufactur,0.002275
mari,0.005512
martin,0.002525
mass,0.005869
match,0.005074
materi,0.003343
mean,0.010510
medic,0.002196
melt,0.006808
membran,0.003378
mendeleev,0.004859
mention,0.002309
mercur,0.005285
metal,0.081940
metalbear,0.006812
metalloid,0.025308
metalloprotein,0.006544
metallurgi,0.003884
metalrich,0.005791
metaphys,0.003052
metast,0.004977
meter,0.012843
methionin,0.010088
method,0.007387
metric,0.032039
microb,0.004055
microelectron,0.004806
microgram,0.034014
microorgan,0.003563
middl,0.003933
mikhail,0.003758
milk,0.003523
milligram,0.005261
million,0.011243
miner,0.044901
minut,0.002847
mislead,0.003607
mix,0.004589
mixtur,0.008948
mller,0.012322
moder,0.005466
modern,0.002861
moh,0.005196
molecul,0.020608
molecular,0.007480
molybdenum,0.004545
monoclin,0.026178
monoxid,0.004441
month,0.004215
moon,0.006213
moscl,0.007219
mostli,0.012212
mucou,0.005498
muller,0.009309
murder,0.003029
mushroom,0.004466
music,0.002621
nabs,0.007219
nanogram,0.006438
nanoparticl,0.005097
nanotub,0.005061
nateo,0.006812
nation,0.001321
natur,0.006368
nausea,0.005061
nearli,0.004155
need,0.005736
needl,0.004204
neutron,0.018807
neutronpoor,0.012688
neutronrich,0.006260
neutronstarv,0.007219
new,0.001065
newland,0.005358
nitrogen,0.006586
nobl,0.003102
noncoval,0.005217
nonlinear,0.003268
nonmet,0.019025
nontox,0.016076
notabl,0.001830
note,0.002750
noth,0.002524
notic,0.002707
notori,0.003786
nuclear,0.026814
nuclei,0.003431
number,0.017902
numer,0.005302
nut,0.004118
nutrient,0.013539
o,0.020126
observ,0.001574
obsolet,0.003720
occasion,0.002691
occur,0.022735
occurr,0.003085
octav,0.004819
odor,0.009000
offici,0.003622
oh,0.004192
oil,0.004762
ole,0.004946
onethird,0.003624
onion,0.004593
onli,0.007687
optic,0.002869
order,0.002629
ore,0.027342
oreform,0.006667
organ,0.026025
organochalcogen,0.006990
orthorhomb,0.006344
osmium,0.005196
outer,0.003048
outermost,0.004168
oxi,0.006184
oxid,0.069636
oxyacid,0.006812
oxygen,0.217851
oxygenoxygen,0.013981
ozersk,0.007219
ozon,0.008372
pack,0.003481
panel,0.003515
paper,0.005905
paramagnet,0.004859
particl,0.015386
particular,0.002986
past,0.001892
pattern,0.004070
penetr,0.003186
period,0.011340
peroxid,0.018838
peru,0.003764
petersburg,0.004293
ph,0.003779
phenol,0.004901
phosphin,0.027650
phosphor,0.005044
phosphoru,0.019653
phosphoruschalcogen,0.014439
phosphorusselenium,0.007219
phosphorussulfur,0.007219
phosphorustellurium,0.007219
photocopi,0.005384
photophys,0.006544
photoreceptor,0.005239
photovolta,0.005155
physic,0.003122
pierr,0.006142
pigment,0.003997
pitchblend,0.011874
place,0.005396
plant,0.010356
plastic,0.003207
pn,0.004401
pnictid,0.025752
pnictogen,0.032722
pnxex,0.007219
po,0.008641
poetic,0.003837
point,0.002767
poison,0.016672
poland,0.003259
polar,0.002922
polariz,0.005530
polon,0.006260
polonium,0.223996
polym,0.007170
ponder,0.004603
popular,0.003413
pose,0.002905
posit,0.001413
potenti,0.001771
practic,0.001463
praseodymium,0.006344
predict,0.002001
prefer,0.002094
prefix,0.003676
prepar,0.002218
presenc,0.004137
present,0.001367
previouslydiscov,0.007219
priestley,0.004518
primari,0.001805
primordi,0.011536
princip,0.002137
prior,0.002066
problem,0.001463
process,0.005173
produc,0.058359
product,0.009980
properti,0.014907
propos,0.003375
protect,0.001823
proton,0.007073
prove,0.002111
provid,0.002427
psb,0.006344
publish,0.001518
pump,0.003440
pure,0.017305
purif,0.003997
purifi,0.003893
purpos,0.003558
pxtey,0.007219
pyrit,0.010159
quantum,0.002759
radic,0.005015
radii,0.009000
radioact,0.036929
ran,0.003062
rapidli,0.002306
rare,0.004502
rarest,0.005239
rariti,0.004756
ratio,0.002467
ray,0.002963
rbte,0.006990
reach,0.001763
react,0.024250
reaction,0.011123
reactiv,0.003268
reactor,0.011857
readili,0.002968
reagent,0.008698
realiz,0.007277
rearrang,0.003717
reason,0.001557
receiv,0.001657
recent,0.003004
recogn,0.001860
recognit,0.002378
recur,0.003704
recycl,0.003691
red,0.016833
reddishbrown,0.005837
redox,0.008742
reduc,0.001643
reduct,0.004992
refer,0.002602
refin,0.010886
reichenstein,0.006667
rel,0.006222
relat,0.001169
releas,0.002168
remain,0.002869
renown,0.003701
replac,0.001768
report,0.001667
requir,0.001365
research,0.004222
reserv,0.002307
respiratori,0.007861
result,0.008459
rewrit,0.004060
rhombic,0.013089
rhombohedr,0.005992
ribbonlik,0.006812
rich,0.004975
ring,0.008815
robert,0.001907
rock,0.002716
rocket,0.003685
role,0.006079
roman,0.002327
root,0.002191
rotten,0.004977
roughli,0.004867
rpe,0.006812
rse,0.006260
rser,0.007219
rubber,0.007529
rumen,0.005837
russia,0.010199
s,0.011074
safeti,0.002796
salt,0.014862
samara,0.005992
sampl,0.028440
sanskrit,0.003593
sbse,0.007219
scheel,0.010311
scienc,0.001459
scientist,0.001983
scope,0.002550
se,0.014430
seawat,0.012612
secl,0.006990
second,0.001423
secondari,0.012353
sediment,0.007675
selen,0.041200
selenid,0.022676
selenit,0.006667
selenium,0.354076
seleniumgath,0.007219
selenocystein,0.011874
selenoeth,0.021659
selenoketon,0.027962
selenol,0.013334
selenomethionin,0.006544
selenophenol,0.007219
selenoprotein,0.006990
semiconductor,0.003786
send,0.002713
sens,0.001785
sensat,0.007073
sent,0.002404
seo,0.005631
separ,0.004755
seri,0.005185
serv,0.001727
sesx,0.007219
sever,0.007552
shampoo,0.005669
shell,0.005840
short,0.003699
siderophil,0.006115
siev,0.004859
signific,0.003082
silic,0.004349
silicon,0.014742
similar,0.011534
similarli,0.004575
simpl,0.001918
sinc,0.003622
singl,0.006463
sink,0.003596
skin,0.006124
skunk,0.005439
slightli,0.002617
slowli,0.002783
small,0.004432
smell,0.015774
sodium,0.007352
soft,0.006165
soil,0.016415
solar,0.008640
solid,0.010380
solubl,0.003861
solut,0.006197
solventhold,0.007219
sometim,0.004715
sourc,0.003037
spallat,0.005707
special,0.001510
specif,0.001444
spin,0.003348
spiral,0.003695
split,0.002481
sport,0.002867
spray,0.004545
st,0.001946
stabil,0.006755
stabl,0.045149
stack,0.003764
star,0.005355
state,0.016904
steadi,0.003094
steam,0.003596
steelmak,0.011674
stench,0.006544
stereoelectron,0.006990
stream,0.005788
strength,0.002448
strikeanywher,0.007219
strong,0.003508
stronger,0.002907
structur,0.014107
studi,0.001276
suboxid,0.006438
success,0.006405
suggest,0.003380
sulfat,0.017283
sulfenyl,0.006812
sulfid,0.069646
sulfit,0.005669
sulfonyl,0.006260
sulfosalt,0.006438
sulfur,0.330128
sulfurcontain,0.005885
sulfurium,0.007219
sulfursulfur,0.013981
sulfuryl,0.006667
sulver,0.007219
sun,0.005357
sunlight,0.003764
superh,0.005748
superhydrid,0.007219
supramolar,0.007219
surfac,0.004372
surround,0.002211
sweden,0.003016
sylvanit,0.007219
synthes,0.009413
synthet,0.005989
tabl,0.011619
taken,0.001768
tar,0.004732
te,0.004378
tebr,0.007219
tecl,0.006990
tei,0.012688
tellur,0.021759
tellurid,0.055626
tellurit,0.006812
tellurium,0.354374
telluroeth,0.014439
telluroketon,0.027962
tellurol,0.020971
telu,0.006812
temperatur,0.002440
tend,0.015862
tendenc,0.008134
tenth,0.003701
teo,0.005439
terbium,0.006184
term,0.006845
ternari,0.005097
tertiari,0.003651
test,0.003947
tetrafluorid,0.018781
tetrahalid,0.006544
tetraoxygen,0.006990
texa,0.003408
th,0.011076
themselv,0.001791
thi,0.019869
thing,0.001921
thio,0.006184
thioether,0.012230
thioketon,0.026669
thiol,0.022120
thiosulf,0.006115
thirdmost,0.006344
thnard,0.006184
thorium,0.004665
thought,0.005133
throughbond,0.006812
thu,0.002823
ti,0.004425
time,0.013888
tin,0.007536
tired,0.005748
tissu,0.002917
titanium,0.004320
titls,0.007219
togeth,0.001665
ton,0.036014
took,0.001886
total,0.003369
toxic,0.063434
toxin,0.004140
trace,0.009261
tract,0.003443
transfer,0.002066
transform,0.005929
transit,0.006306
translat,0.002078
treat,0.002114
treatment,0.002221
trend,0.004612
triad,0.004916
trillion,0.003365
trioxid,0.011415
tunabl,0.005175
tungsten,0.004457
turn,0.001669
type,0.002903
typic,0.012959
ubiquit,0.007615
uncharacter,0.006260
undergo,0.016797
underwat,0.003893
undiscov,0.004583
unit,0.003653
univers,0.008754
unknown,0.002499
unlik,0.002010
unpleas,0.021569
unsophist,0.005239
unstabl,0.016106
ununhexium,0.007219
uranium,0.030175
uraniumiv,0.006990
urin,0.004031
usa,0.002695
use,0.033545
usual,0.005812
util,0.002184
vacant,0.004394
valenc,0.025224
vari,0.001780
vast,0.002424
veri,0.006894
version,0.004224
vetch,0.005937
vi,0.003433
vib,0.006344
vital,0.005393
volatil,0.003212
vomit,0.009374
von,0.002448
vulcan,0.009862
wa,0.028711
warm,0.003085
wast,0.002896
water,0.013038
way,0.003873
weak,0.002398
weaker,0.003438
weapon,0.002681
weight,0.031638
wellknown,0.011338
wellstudi,0.009537
werner,0.003822
whatsoev,0.004151
white,0.002292
wide,0.001555
wilhelm,0.006079
wolfgang,0.003666
word,0.021789
world,0.002448
x,0.004275
year,0.012185
yearli,0.007159
ytterbium,0.005707
zeolit,0.005631
zinc,0.003804
zrte,0.007219
