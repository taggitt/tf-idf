abandon,0.018646
abu,0.054323
acocella,0.042128
addit,0.020197
address,0.015032
aden,0.036689
adopt,0.041326
africa,0.081056
african,0.139971
agreement,0.015237
american,0.011310
amero,0.048496
andorra,0.070893
ani,0.008797
antipodean,0.047603
arabia,0.073257
area,0.019829
argentina,0.049525
arrang,0.016274
articl,0.011863
australasian,0.033933
australia,0.034702
australian,0.020761
author,0.022478
autonom,0.020185
bahrain,0.094593
bahraini,0.041814
barbado,0.062655
bartolomeo,0.038110
belgium,0.023626
bergin,0.045535
bilater,0.022893
birr,0.044478
bloc,0.024152
borneo,0.032671
botswana,0.064260
briefli,0.022327
british,0.134395
brunei,0.031479
cambodia,0.029356
caribbean,0.068998
cayman,0.037216
centr,0.016576
circul,0.021093
citi,0.013922
closer,0.019948
coast,0.018399
collaps,0.017759
combin,0.011496
common,0.048745
concis,0.024557
conservat,0.030448
coordin,0.017092
countri,0.042443
currenc,0.406388
custom,0.065216
czech,0.025839
czechoslovak,0.037951
david,0.014323
denmark,0.023652
depend,0.010639
determin,0.011197
develop,0.008064
dhabi,0.077229
di,0.025094
dinar,0.034959
disband,0.028027
dollar,0.169566
dubai,0.034685
dure,0.009281
earli,0.009709
east,0.074625
eastern,0.048337
econom,0.074365
economist,0.018225
ed,0.027179
effici,0.015199
egypt,0.020500
egyptian,0.021879
emir,0.027670
encyclopedia,0.016718
eritrea,0.031795
establish,0.010359
ethiopia,0.027461
ethiopian,0.030578
eu,0.021136
everi,0.012147
exist,0.019306
extern,0.007590
facto,0.023534
februari,0.015771
fiscal,0.022008
florin,0.040707
foreign,0.027026
form,0.015868
formal,0.038968
franc,0.044581
freeli,0.022575
french,0.027939
g,0.014042
gain,0.012629
gambia,0.033628
geograph,0.016882
german,0.058639
germani,0.031260
gold,0.018050
greec,0.019621
grenada,0.034596
guiana,0.064033
guilder,0.037951
guinea,0.024510
gulf,0.070447
ha,0.006921
henderson,0.029112
inch,0.027970
independ,0.010661
indi,0.026345
india,0.047677
indian,0.034366
indianapoli,0.030155
indochines,0.040460
inform,0.010319
initi,0.011278
integr,0.012526
involv,0.020126
ireland,0.021696
irish,0.024619
isbn,0.012271
island,0.030770
issu,0.033416
itali,0.037361
italian,0.038954
jamaica,0.059598
jamaican,0.074999
januari,0.014814
juli,0.014993
kenya,0.115513
kingdom,0.027614
known,0.008672
koruna,0.044014
kuwait,0.058712
lao,0.029610
late,0.012015
later,0.051605
latin,0.016311
leon,0.025819
letter,0.017131
liberia,0.062457
liberti,0.022028
librari,0.016304
limit,0.010598
link,0.007545
lira,0.037216
list,0.019299
local,0.011753
magazin,0.020090
mainland,0.067628
malaya,0.032671
malaysia,0.025355
marino,0.034596
market,0.025978
materi,0.012160
mauritian,0.040707
mauritiu,0.031531
maxim,0.019735
member,0.011071
mercosur,0.035978
merg,0.020899
monaco,0.033855
monetari,0.297635
multilater,0.027539
multipl,0.013346
n,0.015583
nd,0.016145
necessarili,0.016986
new,0.031004
nigeria,0.027777
north,0.041333
norway,0.048777
note,0.010004
oclc,0.024992
octob,0.015005
oecd,0.025686
offici,0.013174
oman,0.062857
opt,0.026993
optim,0.018405
optimum,0.028813
order,0.009562
p,0.013246
pacif,0.020112
pact,0.026056
panamerican,0.042128
papua,0.030363
paul,0.015083
peg,0.059446
perspect,0.015527
peseta,0.042128
piastr,0.046146
plan,0.026282
polici,0.025682
pound,0.174599
prior,0.015029
propos,0.049106
provision,0.025214
prussia,0.029463
pula,0.043585
push,0.018922
qatar,0.123952
qatari,0.040707
quadrant,0.034509
question,0.012495
r,0.013315
rand,0.117710
read,0.010960
reason,0.011326
refer,0.006310
regard,0.012107
regim,0.018389
region,0.011300
republ,0.044433
reunif,0.031428
riyal,0.083035
romania,0.026473
roubl,0.046829
rubl,0.070691
rupe,0.170062
russia,0.018546
s,0.026851
sadc,0.036947
samoa,0.031035
san,0.020040
saudi,0.051448
scandinavian,0.029647
search,0.016152
separ,0.011529
settlement,0.018416
seychel,0.033779
share,0.024594
shill,0.034959
sierra,0.029500
sign,0.014460
singapor,0.023143
singl,0.047014
slovakia,0.030237
socal,0.017721
social,0.011172
solomon,0.026970
somaliland,0.102767
sometim,0.022866
south,0.096276
southern,0.016300
soviet,0.038046
spain,0.037327
spanish,0.018993
special,0.010984
state,0.084531
step,0.014892
sterl,0.029180
strait,0.027565
sudan,0.027041
supplement,0.021744
sweden,0.021938
switzerland,0.022510
tanganyika,0.075292
tanzania,0.029500
temporari,0.020712
tender,0.029285
territori,0.046117
thaler,0.038110
theoret,0.014400
theori,0.010294
thereof,0.026993
time,0.007770
tirelli,0.052514
tobago,0.128521
trade,0.012704
treat,0.015379
trinidad,0.122315
trucial,0.088957
type,0.010559
uae,0.033628
uganda,0.121117
unilater,0.027539
union,0.326275
unit,0.044292
use,0.222467
vari,0.012947
vatican,0.029647
vietnam,0.023800
virtu,0.022253
wa,0.007201
west,0.060322
zanzibar,0.145293
zealand,0.062650
zimbabw,0.028716
zone,0.054458
