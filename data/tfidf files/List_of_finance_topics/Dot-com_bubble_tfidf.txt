abl,0.006108
abramson,0.019845
absenc,0.008890
academ,0.007243
acceler,0.009301
access,0.026764
accessconnect,0.025440
accord,0.004835
account,0.017087
accus,0.009698
achiev,0.006083
acknowledg,0.009325
acquir,0.015753
act,0.011054
activ,0.004825
actual,0.006189
ad,0.018514
addit,0.004892
adequ,0.010145
advanc,0.011945
advantag,0.007506
advent,0.010575
advertis,0.022727
aerospac,0.013896
aftermath,0.012051
afterward,0.021304
ag,0.014589
alan,0.010252
alert,0.013536
allow,0.009618
alltim,0.016255
amazon,0.013835
amazoncom,0.088273
america,0.013554
american,0.005479
analysi,0.005829
andor,0.008462
andrew,0.009821
anecdot,0.014256
ani,0.021310
announc,0.024362
annual,0.007428
anticip,0.010755
aol,0.039202
appar,0.008255
appl,0.012593
april,0.036991
argu,0.012866
articl,0.005747
asset,0.008940
assum,0.006699
astonish,0.015708
att,0.017028
attack,0.007796
attempt,0.005516
attent,0.007885
attract,0.007781
auction,0.013927
aura,0.017172
author,0.005444
automobil,0.012115
avail,0.017734
awar,0.017499
away,0.007366
babi,0.012642
backbon,0.013761
backdrop,0.015708
bank,0.006993
bankrupt,0.029297
bankruptci,0.113607
bar,0.010931
base,0.021068
basi,0.006023
basic,0.005866
bbc,0.011001
becam,0.026650
becaus,0.013168
becom,0.028007
befor,0.014940
bell,0.011289
benefit,0.014152
bernard,0.011357
big,0.017658
billion,0.049695
boardroom,0.020569
bob,0.013564
boocom,0.025440
book,0.005541
booksamillion,0.025440
boom,0.088501
bought,0.055710
bowl,0.029627
brand,0.011786
brent,0.017123
broadband,0.030500
broadcast,0.011314
broadcastcom,0.025440
browser,0.015621
bruce,0.011980
bubbl,0.268922
buffett,0.016555
build,0.037023
built,0.014538
builttoord,0.025440
buoyant,0.017596
burden,0.011130
burn,0.029987
burst,0.065675
busi,0.052587
businessweek,0.019976
bust,0.015679
buy,0.009224
cabl,0.012712
came,0.006385
campaign,0.008766
cancel,0.011062
canon,0.010604
capabl,0.015381
capac,0.008095
capit,0.050813
capitalist,0.034610
career,0.009580
case,0.014649
cash,0.039044
cassidi,0.020409
catch,0.011757
categor,0.009927
caus,0.026063
caution,0.013747
cautiou,0.014944
cb,0.015456
cbsback,0.025440
celebr,0.010065
center,0.006228
ceo,0.041828
certainti,0.012327
chairman,0.011045
chang,0.008806
charg,0.015073
charl,0.007479
cheapli,0.015920
chf,0.020257
chick,0.017272
choos,0.008401
cisco,0.075898
cite,0.008168
citi,0.013489
citigroup,0.017596
close,0.031355
cnn,0.017028
coincid,0.010506
collaps,0.034414
com,0.048982
combin,0.011139
come,0.005586
commerci,0.014965
commiss,0.015552
commod,0.009479
common,0.009445
commonli,0.006486
commun,0.040859
compani,0.302213
competit,0.015361
competitor,0.024199
complet,0.005394
comput,0.006472
computerrel,0.019376
conclud,0.007988
confer,0.007688
confid,0.019983
confus,0.008791
connect,0.019293
consist,0.005228
consolid,0.020512
conspiraci,0.013881
contend,0.011259
contest,0.010277
continu,0.014198
contract,0.007835
contrast,0.013196
convict,0.022640
convinc,0.010398
corn,0.024430
corpor,0.030655
corridor,0.014709
cost,0.006867
cours,0.006807
covad,0.025440
cover,0.006431
crash,0.069705
creat,0.019111
credit,0.007559
crise,0.012051
crisi,0.016121
critic,0.005745
cross,0.008525
cuban,0.014362
current,0.010001
custom,0.031593
cybersquatt,0.025440
cycl,0.008007
d,0.006323
daisey,0.024006
damag,0.008429
dark,0.010101
databas,0.009138
date,0.006528
daum,0.021792
david,0.020816
day,0.041261
deal,0.006292
debt,0.027174
decad,0.007100
decemb,0.014336
decept,0.013732
decid,0.015225
declar,0.015089
declin,0.014833
deep,0.008696
deepli,0.011029
defunct,0.013805
degre,0.006396
deliveri,0.022579
demand,0.013947
demis,0.013805
depend,0.010308
describ,0.004807
despit,0.013247
destruct,0.009244
develop,0.007813
dialup,0.020739
did,0.034791
digit,0.026168
directli,0.006276
disadvantag,0.011583
disagr,0.011107
discuss,0.006373
disregard,0.013483
distributor,0.016477
divid,0.006194
dixi,0.023061
documentari,0.036012
dog,0.011693
dollar,0.009127
domain,0.007629
doom,0.015858
dot,0.025246
dotbomb,0.025440
dotcom,0.492042
dotcombubbl,0.025440
dotcon,0.025440
downturn,0.013379
dramat,0.008821
drastic,0.011693
draw,0.008007
driven,0.009093
drop,0.034294
drove,0.012652
dub,0.012565
dull,0.015679
dupe,0.040514
dure,0.040468
dwarf,0.014187
e,0.006329
earli,0.018814
earlier,0.006949
earn,0.008839
easili,0.008056
ebay,0.018462
ebaycom,0.050881
ebber,0.025440
ecommerc,0.016891
econom,0.005146
economi,0.019267
edig,0.025440
edigit,0.050881
edit,0.007360
editori,0.013405
edream,0.025440
educ,0.011982
effect,0.009137
elabor,0.009785
elearn,0.019976
electron,0.007869
employe,0.019708
encourag,0.015459
end,0.020567
endur,0.011477
engag,0.014894
engin,0.006777
enter,0.006875
enterpris,0.008852
entitl,0.009708
entrepreneur,0.023689
entri,0.009096
environ,0.006233
equip,0.016815
equiti,0.010760
era,0.014860
establish,0.010037
etoyscom,0.024632
euro,0.011965
europ,0.012753
event,0.012240
eventu,0.006684
evid,0.006412
exagger,0.013111
exampl,0.025270
exceed,0.009875
excess,0.009088
exchang,0.013295
exchangelist,0.023061
execut,0.014247
exemplifi,0.011980
exempt,0.012223
exist,0.004676
expand,0.033262
expect,0.006461
experienc,0.008442
expert,0.009289
extern,0.003677
extrem,0.006745
exuber,0.018310
facil,0.018029
factor,0.005962
fail,0.028282
failur,0.016364
fake,0.015225
fall,0.006419
famou,0.007769
far,0.006376
fashion,0.009898
fast,0.019254
faster,0.009810
favor,0.023302
favorit,0.013865
featur,0.012720
februari,0.007640
feder,0.007487
fell,0.036331
felt,0.009927
fiber,0.036645
field,0.010271
fifthlargest,0.019270
file,0.062982
film,0.009537
financ,0.007451
financi,0.060423
fine,0.010053
finnish,0.014492
firm,0.050395
firstday,0.025440
flop,0.020921
flow,0.007405
follow,0.008045
forb,0.013605
forc,0.005040
formerli,0.009420
fortun,0.011407
founder,0.008814
fraud,0.023167
fraudul,0.027980
fred,0.013042
free,0.017232
freeinternetcom,0.050881
friend,0.009988
fulltim,0.013392
function,0.005148
fund,0.006933
fundament,0.006439
futur,0.012249
g,0.013606
gain,0.018354
gave,0.014635
geeknet,0.025440
geociti,0.048012
german,0.007101
germani,0.007571
global,0.013356
glut,0.017075
gold,0.008744
goldfarb,0.021322
goliath,0.019376
gone,0.011159
good,0.011181
googl,0.048366
govern,0.004782
govworkscom,0.025440
great,0.011335
greatest,0.008849
greenspan,0.033693
grew,0.016807
grocer,0.022686
grossli,0.016477
grow,0.006448
growth,0.050922
h,0.006985
ha,0.020119
half,0.006922
halfhour,0.019976
handl,0.009144
har,0.014022
headlin,0.015650
height,0.010026
help,0.005505
hewlettpackard,0.018974
hi,0.004508
high,0.030552
highli,0.006620
highspe,0.029844
histor,0.011297
histori,0.020942
hit,0.020823
home,0.007180
hope,0.008562
host,0.018391
hous,0.006745
howev,0.012243
httpssrncomabstract,0.022059
hub,0.013054
huge,0.018704
hype,0.017774
ibm,0.012931
ict,0.016083
illeg,0.010133
immedi,0.007392
impact,0.006891
implement,0.007001
import,0.008577
importantli,0.011523
improv,0.006188
includ,0.010472
increas,0.033850
incumb,0.013483
indepth,0.014589
individu,0.015561
industri,0.051659
industrydomin,0.025440
influenc,0.005538
inform,0.024997
infospac,0.025440
infrastructur,0.034057
ingram,0.019601
initi,0.038247
inktomi,0.025440
instanc,0.006810
instant,0.012998
instead,0.005890
intel,0.032440
interexchang,0.024006
intern,0.004406
internet,0.140241
internetbas,0.018385
internetservic,0.025440
intraday,0.059536
introduc,0.005898
invest,0.078758
investor,0.095804
invinc,0.018029
ipo,0.098863
irrat,0.012953
irrevoc,0.016846
isbn,0.023778
isp,0.018974
itali,0.009049
italian,0.009435
item,0.009224
iwoncom,0.025440
januari,0.043059
japan,0.008315
jd,0.016185
job,0.017368
john,0.005719
jorgen,0.020921
journal,0.006467
just,0.011727
kb,0.015593
kid,0.030024
kindleberg,0.019270
kingdom,0.006688
kirsch,0.019601
known,0.008403
koreabas,0.024006
kozmocom,0.025440
kuo,0.015984
lack,0.019232
laid,0.009489
laidoff,0.024006
larg,0.022154
largest,0.027776
lastminutecom,0.025440
late,0.011641
later,0.030000
launch,0.016934
lavish,0.016718
law,0.005064
lawyer,0.012198
leader,0.007351
learn,0.007062
leav,0.007119
led,0.021704
legaci,0.010571
legal,0.006937
let,0.009420
level,0.005027
licenc,0.014399
licens,0.010011
lifespan,0.013850
like,0.013471
limit,0.005134
line,0.006017
link,0.010966
linux,0.034246
liquid,0.009011
list,0.018699
littl,0.006434
live,0.010871
lobbyist,0.017484
local,0.028470
locat,0.006181
lockin,0.019270
long,0.005458
longdist,0.014750
longterm,0.017693
loss,0.022059
lost,0.051140
lot,0.010354
low,0.012886
lowenstein,0.019845
lucki,0.016439
luxuri,0.025226
lyco,0.023061
lynch,0.015250
ma,0.010618
machin,0.008117
major,0.004460
make,0.008549
mammon,0.021792
mani,0.045596
mania,0.051084
manufactur,0.008016
march,0.055770
mark,0.013499
market,0.100683
mascot,0.021322
mass,0.006894
mattel,0.022686
mean,0.009259
meant,0.008622
measur,0.005506
media,0.023132
megafirm,0.025440
memor,0.013958
mental,0.009096
merg,0.010124
merril,0.016049
messag,0.010447
meteor,0.014668
metric,0.011289
michael,0.007736
micro,0.013761
microsoft,0.013316
microstrategi,0.025440
mike,0.013958
miller,0.011735
million,0.072632
millionair,0.035309
mind,0.015855
misfortun,0.017172
mislead,0.012712
misrepres,0.015593
misus,0.013865
mit,0.010214
mitig,0.012083
mobil,0.017859
model,0.010472
modern,0.005042
money,0.030051
month,0.029709
mortgag,0.012416
mosaic,0.014006
motto,0.014944
movi,0.011497
multibillionair,0.022686
multibilliondollar,0.018385
nasdaq,0.049785
nation,0.009316
natur,0.004487
necess,0.010533
need,0.005053
net,0.008836
network,0.063250
networken,0.023061
new,0.022529
news,0.009175
night,0.010311
nonprofit,0.012124
nonsens,0.014492
nontechnolog,0.025440
norri,0.018029
nortel,0.063344
northpoint,0.025440
notabl,0.006450
note,0.009693
noth,0.008895
notic,0.009541
notori,0.013341
novemb,0.037699
number,0.008410
numer,0.006227
nyse,0.016935
obstacl,0.012132
obstruct,0.014256
occur,0.016022
octob,0.036346
offer,0.037506
offic,0.013532
oldeconomi,0.025440
oligopoli,0.016017
onehour,0.019976
oneyear,0.015593
onli,0.007739
onlin,0.036788
onomatopo,0.021792
open,0.011510
opencom,0.025440
oper,0.031819
optic,0.010112
option,0.008771
oracl,0.014835
origin,0.009317
os,0.015351
otcbbtrad,0.025440
outsid,0.006412
overextens,0.021547
overlook,0.012986
overvalu,0.031905
p,0.006417
page,0.008241
paid,0.025713
panic,0.014103
paper,0.006936
parti,0.006496
particular,0.005261
pattern,0.007172
pay,0.007848
pe,0.045105
peak,0.036898
penguin,0.012642
peopl,0.009269
period,0.009989
person,0.005500
pet,0.013328
petscom,0.046122
phenomenon,0.008657
phoenix,0.015201
phrase,0.010128
pick,0.010942
pioneer,0.008781
pixelon,0.025440
place,0.004754
plan,0.006366
plateau,0.013661
platform,0.010294
player,0.033005
plummet,0.015325
pocketbook,0.021547
portfolio,0.011563
portion,0.008447
posit,0.009964
possibl,0.009833
post,0.008158
potenti,0.006240
power,0.004933
powerhous,0.017323
practic,0.010314
prefix,0.025907
press,0.017020
preston,0.017221
previou,0.007269
previous,0.036743
price,0.149862
pricetoearn,0.022355
prime,0.007745
primetim,0.024006
principl,0.005786
print,0.009052
problem,0.005155
produc,0.015046
product,0.020096
profession,0.007811
profit,0.066629
profitor,0.025440
program,0.006063
programm,0.018531
progress,0.006570
project,0.011929
promis,0.009058
promot,0.006876
proport,0.007970
prospect,0.010243
provid,0.021384
public,0.043692
publicli,0.010315
pull,0.011062
purchas,0.016397
push,0.009166
quantiti,0.007851
quickli,0.031385
quit,0.008146
railroad,0.012407
rais,0.007110
ran,0.021581
rang,0.005786
rapidli,0.032507
rate,0.029869
ratio,0.034774
rboc,0.025440
reach,0.006213
read,0.005309
reason,0.005486
reced,0.015201
record,0.006444
recordset,0.024006
recov,0.009410
redirect,0.015058
reduc,0.005791
refer,0.009171
reflect,0.006570
region,0.005474
relat,0.004122
releas,0.007640
reli,0.014733
remain,0.005056
report,0.005875
requir,0.004812
research,0.009919
resel,0.034344
resembl,0.009643
reserv,0.008130
respect,0.005814
restat,0.014668
resum,0.011693
resurrect,0.013564
retail,0.044228
retrac,0.017899
retrospect,0.013457
return,0.006187
revenu,0.026757
revolut,0.015147
rh,0.015708
rise,0.061519
risk,0.007522
riski,0.012964
robert,0.006722
roger,0.010252
role,0.005355
rose,0.018554
roughli,0.008575
rule,0.005580
rush,0.013353
s,0.021679
safe,0.009995
said,0.012562
sandberg,0.019376
satirewir,0.025440
satur,0.013865
saw,0.029925
say,0.006623
scale,0.006919
school,0.012439
scope,0.008987
search,0.007825
second,0.005017
secondlargest,0.014589
sector,0.016030
secur,0.012990
selfperpetu,0.019601
sell,0.008648
seoul,0.016760
septemb,0.007372
serv,0.006085
server,0.014454
servic,0.023826
session,0.011631
set,0.009471
sever,0.017742
share,0.107233
sharehold,0.012362
ship,0.008674
shortliv,0.012012
shortterm,0.011113
signific,0.016294
significantli,0.007846
silicon,0.012986
similar,0.005080
similarli,0.008060
simpli,0.020828
site,0.007166
situat,0.013101
small,0.010412
smith,0.027043
soar,0.027582
social,0.005412
softwar,0.018029
sold,0.035940
sometim,0.005538
sonera,0.048012
soon,0.007802
sought,0.016577
sourc,0.005351
south,0.006663
space,0.006559
spanish,0.009201
special,0.005321
spectacular,0.014813
specul,0.044260
spend,0.035327
spent,0.018809
spike,0.014730
spin,0.011800
spite,0.012257
sport,0.010105
spot,0.021304
ssrn,0.017028
stabl,0.008373
staff,0.010030
stage,0.007430
stand,0.007996
startup,0.059170
startupcom,0.023061
startupscom,0.025440
state,0.018614
steadi,0.010904
stock,0.254469
stop,0.008405
store,0.008620
strategi,0.007925
stream,0.010198
street,0.029126
structur,0.004971
struggl,0.017838
student,0.007241
stuff,0.015275
subprim,0.016477
substanti,0.015796
succeed,0.008698
success,0.005642
suffix,0.014530
suggest,0.005956
sum,0.008487
super,0.027611
suppli,0.006936
support,0.005058
surpass,0.012012
surprisingli,0.014204
surviv,0.029352
suspend,0.010575
sustain,0.008322
swedish,0.011603
symbol,0.007724
symptom,0.012107
tail,0.012318
tandem,0.015377
target,0.007922
tax,0.023988
tech,0.043364
technolog,0.110421
technoutopian,0.021547
teeter,0.021322
telecommun,0.033272
telefnica,0.023494
telephon,0.011631
televis,0.009590
telia,0.024006
teliasonera,0.022686
temporari,0.010034
tend,0.006986
term,0.004020
terminolog,0.010018
terra,0.031716
th,0.004878
theglobecom,0.024632
themselv,0.006311
theori,0.004987
therefor,0.005783
thi,0.047736
think,0.007378
thirdgener,0.020113
thirdlargest,0.016718
throughput,0.015952
thu,0.004973
time,0.022587
tiscali,0.025440
today,0.006112
told,0.010790
took,0.013293
tool,0.006812
toy,0.027854
trade,0.030774
trader,0.010474
tradit,0.010715
transact,0.027483
transit,0.014814
trillion,0.011859
truli,0.010661
turn,0.011764
typic,0.017124
ubiquit,0.013417
uk,0.007798
ultim,0.014718
uncorrel,0.017596
understand,0.011550
undervalu,0.016049
undo,0.017221
unemploy,0.009965
uniphas,0.025440
unit,0.017165
univers,0.004406
unpreced,0.012083
unprofit,0.016439
updat,0.010307
use,0.020859
user,0.009438
usual,0.005120
va,0.014792
vacant,0.015483
vacat,0.014608
valley,0.009839
valu,0.021813
valuat,0.047086
variou,0.004843
vast,0.017087
vein,0.012672
ventur,0.067236
veri,0.004859
verizon,0.021547
viabl,0.011707
video,0.009364
view,0.005453
virginia,0.012257
visibl,0.009404
wa,0.097681
wakeup,0.019976
wall,0.027027
warner,0.031358
warren,0.012425
way,0.004549
wealth,0.017523
weather,0.010433
web,0.036898
websit,0.016090
webvan,0.046122
week,0.017885
went,0.054627
whatev,0.010474
whatsoev,0.014628
whi,0.007828
wide,0.010959
wiley,0.010411
wilson,0.010628
wisdom,0.011265
wolff,0.015377
word,0.005906
work,0.004171
world,0.008627
worldcom,0.079904
worldwid,0.008631
worst,0.011735
write,0.006652
xceleracom,0.025440
xla,0.025440
xo,0.021792
xxxiv,0.020739
xxxv,0.021114
yahoo,0.066542
year,0.034351
yearli,0.012613
young,0.007900
