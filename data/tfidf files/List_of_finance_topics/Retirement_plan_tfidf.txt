abil,0.002505
abolish,0.003805
absenc,0.003347
acceler,0.003502
accept,0.002286
accord,0.005462
account,0.034316
accru,0.010634
accrual,0.020602
accumul,0.003493
accur,0.003102
achiev,0.002290
act,0.031219
actual,0.002330
actuari,0.033765
acut,0.004779
ad,0.004647
addit,0.007369
administ,0.003569
administr,0.010184
administracin,0.009275
adult,0.003564
advanc,0.004498
advantag,0.014133
advic,0.003963
advisor,0.009330
advoc,0.003125
affair,0.003011
affect,0.004759
africa,0.002957
age,0.059294
agenc,0.014323
agetyp,0.008846
albeit,0.004602
algemen,0.008418
align,0.003934
alloc,0.010902
allow,0.007243
alway,0.002448
america,0.005104
american,0.004126
ani,0.011234
annual,0.011189
annuiti,0.043432
anoth,0.005254
anyth,0.003598
appar,0.006217
appear,0.006327
approach,0.002095
appropri,0.002919
april,0.002785
argentina,0.004517
arrang,0.023750
asid,0.004153
aspect,0.002393
asset,0.030298
assist,0.002715
associ,0.001778
assum,0.005045
assumpt,0.009307
attain,0.007372
attract,0.002930
auditor,0.006162
australia,0.025322
austria,0.004213
automat,0.003717
aux,0.006670
avail,0.004452
averag,0.032640
avoid,0.002720
balanc,0.023057
bank,0.002633
bankruptci,0.019013
base,0.009520
basic,0.011045
bear,0.006815
becam,0.002007
becaus,0.006611
becom,0.010546
befor,0.007501
began,0.002192
beggar,0.013846
begin,0.002141
belt,0.004786
beneficiari,0.005227
benefit,0.231819
best,0.002420
better,0.002476
bia,0.008374
birth,0.003235
birthrat,0.007337
bismarck,0.006190
blind,0.004494
bn,0.016415
board,0.003167
bodi,0.002203
bond,0.003136
born,0.003101
bradley,0.011305
break,0.003026
brehon,0.008846
brokerag,0.005687
bulk,0.003892
busi,0.007425
calcul,0.019614
california,0.003368
canada,0.018899
canadian,0.003732
cap,0.004479
care,0.005525
career,0.007215
carri,0.002370
case,0.005516
cash,0.025729
catchup,0.007573
center,0.004691
central,0.002061
centuri,0.005665
certain,0.018115
challeng,0.010508
chapter,0.003427
character,0.002789
childbirth,0.006068
choic,0.005779
choos,0.006327
chronic,0.004520
circumst,0.003378
citizen,0.006019
civil,0.005045
civilian,0.007646
claim,0.002232
classifi,0.002974
clergi,0.005009
closer,0.003639
code,0.002845
coincid,0.003956
combin,0.004194
come,0.004207
commiss,0.011712
commit,0.003082
committe,0.003155
common,0.007113
commonli,0.007327
compani,0.009289
compens,0.014870
competitor,0.004556
complet,0.006094
comprehens,0.003239
compulsori,0.004651
concern,0.004129
confus,0.003310
congression,0.005221
consid,0.001715
consider,0.004943
conspicu,0.005761
construct,0.002277
contain,0.004220
continu,0.005346
contractu,0.005227
contrast,0.002484
contribut,0.151122
control,0.001954
controversi,0.002898
convert,0.002998
corpor,0.014429
correct,0.002962
cost,0.020688
counsel,0.004735
countri,0.034842
coupl,0.003403
cover,0.002421
coverag,0.004424
cpp,0.015901
creat,0.003598
creation,0.002677
credit,0.008539
crise,0.004538
crisi,0.003035
critic,0.006490
cross,0.003210
crunch,0.006176
csr,0.015370
current,0.013181
custodian,0.006485
czech,0.004713
damon,0.007426
dark,0.003803
data,0.002347
date,0.002458
db,0.012324
deaf,0.006279
death,0.007728
debat,0.002738
debt,0.006821
decis,0.004959
declar,0.002841
declin,0.002792
decreas,0.002896
deduct,0.004255
defer,0.015246
deferr,0.014153
deficit,0.004082
defin,0.159472
definedbenefit,0.009039
definit,0.004651
degener,0.005149
degre,0.004817
deliv,0.003467
denmark,0.004314
depart,0.002579
depend,0.013585
deriv,0.002236
describ,0.001810
design,0.019444
despit,0.002494
determin,0.010213
develop,0.001471
differ,0.004678
dilemma,0.004991
directli,0.004726
director,0.003536
disabl,0.029986
disband,0.005112
disciplin,0.002710
discount,0.004246
discretionari,0.005995
discuss,0.007200
distinct,0.002265
distress,0.004882
distribut,0.002426
divid,0.002332
dollar,0.034370
domin,0.002420
drag,0.005092
drawn,0.003471
drop,0.003228
dublin,0.010900
duke,0.004638
dure,0.015238
earli,0.019483
earn,0.013314
eas,0.004076
easili,0.003033
econom,0.001937
economi,0.004836
effect,0.005161
effici,0.002772
egg,0.004283
eighteenth,0.004526
elderli,0.009918
element,0.002252
elimin,0.003062
elizabethan,0.006377
employ,0.082214
employe,0.133583
employeeonli,0.009275
employmentbas,0.017693
enabl,0.005616
encourag,0.005821
end,0.005808
english,0.002466
enrol,0.004869
ensur,0.005818
enter,0.002588
entri,0.006850
equal,0.009646
equiti,0.004052
equival,0.002852
era,0.005595
erisa,0.007878
ernest,0.004556
establish,0.005669
esther,0.006429
estim,0.004984
eu,0.003855
europ,0.004802
european,0.002205
event,0.004609
everlarg,0.007809
exampl,0.031720
exceed,0.007437
excess,0.003422
exchang,0.002503
execut,0.002682
exhibit,0.003342
exist,0.005282
expand,0.002505
expect,0.012165
expens,0.006325
experienc,0.003179
express,0.002294
extend,0.002335
extens,0.002391
extent,0.005827
extern,0.001384
f,0.002608
face,0.005315
fact,0.006745
factor,0.008981
famili,0.002452
fap,0.015256
fast,0.003625
favor,0.002924
featur,0.007184
feder,0.016917
feeonli,0.009039
fell,0.003420
fer,0.007522
fewer,0.003755
fiduciari,0.006190
figur,0.005245
file,0.003952
final,0.009162
financ,0.011224
financi,0.017696
finland,0.009470
firm,0.003162
fix,0.014053
flat,0.008100
follow,0.006059
forc,0.001898
forecast,0.004172
forese,0.005539
foresight,0.006148
forgo,0.006081
form,0.014474
formal,0.002369
formula,0.009920
framework,0.002713
franc,0.010843
freez,0.004907
function,0.001938
fund,0.133158
futur,0.016144
gain,0.004607
gave,0.002755
gener,0.005442
germani,0.008553
given,0.007597
good,0.002105
got,0.004272
gotha,0.007522
govern,0.028811
government,0.003963
grant,0.011611
greatli,0.003101
greec,0.003579
group,0.001708
grow,0.014569
guarante,0.017367
guaranti,0.022721
guid,0.002677
ha,0.010101
ham,0.006504
harsh,0.004505
health,0.005279
hear,0.003993
held,0.002238
henc,0.002954
hi,0.001697
high,0.005752
higher,0.004502
highest,0.002899
highli,0.004985
hinder,0.004771
hire,0.004134
histori,0.004731
hoc,0.005298
hong,0.004427
howev,0.010757
hybrid,0.019781
hypothet,0.004258
ignor,0.003284
ii,0.002478
immigr,0.011305
impact,0.002595
import,0.001615
impos,0.003256
imprison,0.004502
inadequ,0.004496
incent,0.003942
includ,0.013145
incom,0.043506
incorpor,0.002600
increas,0.018209
increasingli,0.005583
incur,0.004541
inde,0.003219
index,0.012032
india,0.005798
individu,0.035158
individualemployeeretire,0.009275
infant,0.004550
inflat,0.032095
inform,0.001882
insan,0.005840
insolv,0.006007
instal,0.003736
instanc,0.010258
instead,0.002217
institut,0.008015
insur,0.062062
insurancetyp,0.009275
interestfre,0.006764
invest,0.075491
investig,0.002653
investor,0.003607
involuntari,0.005850
involv,0.001835
ipp,0.008206
ir,0.005292
ira,0.005705
ireland,0.023747
irish,0.008982
island,0.002806
j,0.002378
japan,0.006262
journal,0.002435
jshape,0.008206
k,0.005741
kansanelkelaito,0.009275
kelli,0.005422
kept,0.003411
kin,0.005687
kingdom,0.015112
kiwisav,0.017367
known,0.015821
kong,0.004465
korea,0.003821
la,0.003160
labor,0.006217
labour,0.003205
lack,0.002414
larg,0.008342
largest,0.005229
late,0.002191
later,0.003765
law,0.013350
leader,0.002768
leav,0.002681
led,0.002043
legal,0.018286
legisl,0.008695
level,0.009465
levi,0.004449
liabil,0.022245
liber,0.002854
lie,0.006203
lieu,0.005971
life,0.014245
lifespan,0.005215
lifestyl,0.004508
like,0.001690
limit,0.009666
link,0.001376
list,0.001760
live,0.002046
loan,0.003672
local,0.002144
longer,0.005221
longev,0.005670
look,0.002613
loss,0.002768
lotteri,0.005983
low,0.004852
lowcost,0.005733
lower,0.007017
lowest,0.003765
lump,0.016832
mainli,0.002628
mainten,0.003797
major,0.003358
make,0.004829
malaysia,0.004625
manag,0.006801
mandatori,0.009731
mani,0.024323
mariana,0.005850
mark,0.002541
market,0.011847
match,0.010099
matern,0.005057
mauritiu,0.005752
maximum,0.003442
mean,0.010460
meanstest,0.007950
meet,0.007683
member,0.016157
membership,0.003486
mere,0.003114
method,0.005881
mexico,0.007898
midcar,0.007809
militari,0.005121
million,0.004972
minimum,0.006905
minist,0.002856
minu,0.004678
mitig,0.009100
mobil,0.006724
model,0.001971
modern,0.001898
monetari,0.003393
money,0.014145
month,0.011187
monthli,0.008629
moral,0.003166
mortal,0.004230
movement,0.002182
multipli,0.008161
municip,0.003857
mutual,0.003339
nacion,0.005262
nation,0.026311
natur,0.001689
necessarili,0.003098
need,0.003805
neg,0.002744
nest,0.009805
netherland,0.007377
new,0.008483
nineteenth,0.003959
nonchurchbas,0.009275
noncontributori,0.008029
normal,0.010092
northern,0.002959
notabl,0.002429
notion,0.005989
number,0.014252
numer,0.002345
oblig,0.006991
obtain,0.004920
occup,0.016492
octob,0.002737
offer,0.018831
old,0.013096
older,0.013287
onli,0.002914
ontario,0.005204
open,0.006501
openend,0.012062
oper,0.001997
opra,0.008114
option,0.006606
order,0.005233
organ,0.001726
organis,0.002997
origin,0.001754
otto,0.004460
ouderdomswet,0.009275
outliv,0.013528
outpac,0.006394
outright,0.004902
overseen,0.005652
pace,0.004272
paid,0.054869
parallel,0.003280
parti,0.002446
partial,0.011647
particip,0.009988
particularli,0.002219
past,0.002511
paternalist,0.007145
path,0.003209
pattern,0.002700
pay,0.032508
payabl,0.005661
payasyougo,0.016228
payer,0.006360
payg,0.008683
paygo,0.008846
payment,0.040943
payout,0.025181
payrol,0.005733
pbgc,0.027827
penalti,0.004735
pension,0.703687
peopl,0.006981
percentag,0.007054
perform,0.004450
perhap,0.002942
period,0.003761
person,0.016570
personn,0.008114
pertain,0.004377
piou,0.006279
place,0.001790
plan,0.366788
play,0.002285
plu,0.007122
point,0.001836
polit,0.001986
poor,0.012519
popul,0.009009
popular,0.009058
portabl,0.031680
portfolio,0.004354
portion,0.006361
posit,0.001876
possess,0.002844
possibl,0.001851
post,0.003072
potenti,0.004700
poverti,0.003734
power,0.007430
practic,0.001942
predetermin,0.010397
prefund,0.009039
premium,0.018567
present,0.001815
pressur,0.002732
price,0.002687
prior,0.005483
privat,0.020822
privatesector,0.012789
profession,0.002941
program,0.006850
progress,0.002474
prohibit,0.003703
promis,0.006821
proport,0.003001
propos,0.004479
protect,0.004838
provid,0.046705
provis,0.021213
prsi,0.009275
prussian,0.005493
public,0.020109
purchas,0.021610
purpos,0.007083
push,0.003451
qualifi,0.007634
quasicrimin,0.009275
quebec,0.005329
question,0.002279
quit,0.003067
rais,0.008032
rang,0.004357
rare,0.002987
rate,0.020245
reach,0.007018
readili,0.003939
reason,0.004132
receiv,0.008797
recent,0.007973
recipi,0.004443
recogn,0.002468
record,0.004853
recordkeep,0.006895
reduc,0.006542
reduct,0.006624
refer,0.004604
reflect,0.002474
reform,0.011264
regardless,0.003480
regist,0.010709
regul,0.005450
regular,0.009461
regularli,0.007837
regulatori,0.003984
rel,0.004128
relat,0.003104
relax,0.004400
relief,0.004104
remain,0.003807
replac,0.002345
report,0.006637
repres,0.001888
republ,0.005403
republican,0.004112
requir,0.012685
resembl,0.003631
reserv,0.009185
resid,0.003049
resist,0.002983
resort,0.004272
resourc,0.002330
respect,0.004378
respons,0.009983
restructur,0.004547
result,0.001603
retail,0.004163
retain,0.003027
retir,0.317295
retire,0.034761
return,0.013979
review,0.005158
revolutionari,0.007327
reward,0.007964
riester,0.009039
right,0.002065
rigor,0.003876
risk,0.036824
riskfre,0.005904
role,0.004033
roth,0.006094
royal,0.003037
rpi,0.007809
rrsp,0.008542
s,0.004898
salari,0.051097
saskatchewan,0.006789
save,0.026153
savvi,0.007685
say,0.002494
scheme,0.062367
scope,0.003384
second,0.005668
secondli,0.005062
section,0.002852
sector,0.009054
secur,0.056255
seen,0.004517
seguridad,0.007878
select,0.007509
selfdirect,0.013904
selfemploy,0.005850
selffund,0.007180
selfinvest,0.008846
seriou,0.003224
servic,0.013457
set,0.012483
sever,0.003340
shift,0.005579
shill,0.006377
sick,0.004520
signific,0.002045
significantli,0.002954
similar,0.003826
simpl,0.002545
sinc,0.006408
singapor,0.004222
singl,0.002144
situat,0.002466
slovenia,0.005493
slowli,0.003692
small,0.003921
social,0.052992
softwar,0.003394
sole,0.003252
solidarit,0.008206
solvent,0.005160
sometim,0.010428
south,0.005018
sp,0.014734
spain,0.006809
special,0.002003
specif,0.005750
specifi,0.006515
sponsor,0.020083
sponsoremploy,0.018551
ss,0.004924
ssa,0.007878
stabil,0.002987
stand,0.003011
standard,0.002157
start,0.006084
state,0.058878
statesponsor,0.006485
statut,0.004706
steadi,0.004106
steadili,0.004226
step,0.005433
stock,0.006182
stream,0.003840
street,0.003655
stringenc,0.007878
structur,0.007487
studi,0.001693
subject,0.002038
substanti,0.005948
succeed,0.003275
suffer,0.002924
suffic,0.005586
suffici,0.005924
suit,0.003724
sum,0.019175
sumner,0.006295
super,0.005198
superannu,0.059784
supplement,0.015866
support,0.003809
surplu,0.004215
survey,0.006159
surviv,0.002763
survivor,0.009954
sweden,0.004002
switch,0.003934
switzerland,0.008212
tailor,0.005298
target,0.002983
task,0.002972
tax,0.036132
taxfre,0.006604
taxfund,0.008114
taxpay,0.010697
teacher,0.003671
temporari,0.003778
temptat,0.005971
tend,0.013155
tendenc,0.003597
term,0.007569
termin,0.011261
testifi,0.005464
th,0.001837
therefor,0.002177
thi,0.034753
thu,0.005618
tier,0.005733
time,0.012758
today,0.002301
tool,0.002565
total,0.004470
trace,0.003072
trade,0.002317
tradit,0.014122
transfer,0.002741
transform,0.002622
treasuri,0.004258
treat,0.002805
trend,0.003060
trust,0.007004
truste,0.011323
twotier,0.006895
type,0.032746
typic,0.023644
uk,0.023493
ukand,0.008846
uncertain,0.004235
underfund,0.015370
unemploy,0.003752
unfortun,0.004619
unfund,0.039048
uninterrupt,0.006019
union,0.009523
unit,0.038784
univers,0.006637
unknown,0.003317
unless,0.003590
untax,0.014946
updat,0.007762
usa,0.003577
usc,0.012215
use,0.009164
usual,0.021208
util,0.002898
vagabond,0.007256
valid,0.003059
valu,0.006160
valuabl,0.003761
valuat,0.004432
variou,0.003647
vehicl,0.014551
veri,0.001829
veteran,0.024389
victorian,0.005144
voluntari,0.008865
voluntarili,0.005193
von,0.003249
wa,0.018391
wage,0.007041
wagebas,0.007878
wall,0.003392
war,0.011379
way,0.005139
week,0.003367
welfar,0.007380
wellfund,0.006867
western,0.004820
wherea,0.002780
wherebi,0.003620
whichev,0.006360
widespread,0.003078
widow,0.027008
withdraw,0.003851
withdrawn,0.004963
word,0.002224
work,0.012565
worker,0.026842
workforc,0.013517
workhous,0.006923
world,0.006497
worldwid,0.006500
worth,0.003627
year,0.048508
yearli,0.004749
younger,0.007961
zealand,0.019048
