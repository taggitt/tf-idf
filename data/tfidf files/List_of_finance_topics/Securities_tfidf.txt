acceler,0.006582
accept,0.008593
accomplish,0.006878
accord,0.003422
account,0.028215
achiev,0.004305
acquir,0.005574
act,0.019557
addit,0.006924
adjust,0.006550
administ,0.006708
admit,0.014433
adopt,0.004722
aftermarket,0.015089
agenc,0.016151
agent,0.017505
agreement,0.005223
akin,0.009159
allmani,0.017432
allow,0.006806
altern,0.013192
alway,0.013806
amend,0.007216
andor,0.017966
ani,0.012064
anoth,0.016458
anyth,0.006763
appear,0.007927
appli,0.003658
applianc,0.010810
appoint,0.005625
appropri,0.005485
approv,0.005879
approxim,0.004663
arn,0.011985
arrang,0.022317
articl,0.020337
assent,0.010774
asset,0.082247
assist,0.005102
associ,0.023402
assum,0.004741
attract,0.011013
auction,0.009856
author,0.019266
automat,0.006986
avail,0.004183
b,0.013599
bad,0.007020
bank,0.074237
banknot,0.010409
bankruptci,0.026799
basi,0.004262
bear,0.006404
bearer,0.111372
becam,0.003771
becaus,0.006212
becom,0.009910
behalf,0.045669
belgium,0.008099
beneath,0.008919
benefici,0.015145
benefit,0.005007
besid,0.007060
best,0.009096
bloomberg,0.011095
board,0.005953
bodi,0.004140
bond,0.106092
bondhold,0.012759
book,0.019609
bookentri,0.050966
borrow,0.027395
borsa,0.015820
bought,0.023655
broad,0.016628
broader,0.006880
broadli,0.006907
broker,0.036245
brokerag,0.042758
busi,0.023259
buy,0.019584
buyer,0.008236
callabl,0.013179
capit,0.058434
carri,0.008910
case,0.010367
cash,0.034538
categor,0.007025
categori,0.031641
cedelbank,0.017432
centr,0.005682
central,0.003874
certain,0.015131
certif,0.112461
chain,0.006174
challeng,0.004937
chang,0.003115
characterist,0.023819
chequ,0.012017
claim,0.004195
class,0.013752
classif,0.024394
classifi,0.005590
clear,0.020577
clearingdepositori,0.017432
clearstream,0.030497
client,0.007959
close,0.003698
coca,0.039715
code,0.005348
cola,0.041137
collater,0.130836
columbia,0.007687
combin,0.007882
come,0.003953
commerci,0.037069
commiss,0.005503
commit,0.005793
common,0.016711
commonli,0.013771
compani,0.087294
compet,0.005699
complet,0.011453
compulsori,0.017484
computer,0.010138
concern,0.003880
conduct,0.004737
consid,0.006447
consider,0.004645
constitut,0.013094
consum,0.033025
contain,0.003965
contract,0.011090
contractu,0.019646
control,0.029381
convers,0.005703
convert,0.050715
cooper,0.005283
corp,0.008155
corpor,0.027117
counter,0.007474
countri,0.025464
court,0.010486
coven,0.020354
coverag,0.008315
crackdown,0.011158
credit,0.016048
creditor,0.019295
currenc,0.012115
current,0.007077
custodi,0.021838
custodian,0.024375
custom,0.011179
customari,0.009214
data,0.008823
day,0.008342
daytoday,0.009432
deal,0.022266
dealer,0.051081
dealerbas,0.016988
debentur,0.068186
debt,0.192306
decad,0.010049
decentr,0.009032
deciph,0.010229
default,0.016870
definit,0.021854
degre,0.009054
deliv,0.013031
deliveri,0.007989
demand,0.004935
demateri,0.016320
denomin,0.016472
depart,0.004846
depend,0.014589
deposit,0.052356
depositari,0.014335
depositori,0.080526
deriv,0.008406
detach,0.008996
determin,0.007677
develop,0.011058
did,0.004103
differ,0.002930
dilut,0.009638
direct,0.003670
directli,0.008883
disadvantag,0.008197
discount,0.007981
discretionari,0.011266
discuss,0.004510
display,0.006282
distinct,0.012772
district,0.006686
divid,0.030684
divis,0.004791
doe,0.003740
domest,0.011455
domicil,0.012712
dr,0.013717
draft,0.007134
draw,0.005666
dtc,0.185062
dtcc,0.032640
duckl,0.017432
dwindl,0.011095
earli,0.003328
earn,0.006255
econom,0.003642
ecp,0.015248
effect,0.006466
effici,0.005211
effort,0.009218
egg,0.008050
elect,0.004908
electron,0.044553
elimin,0.005755
emerg,0.004265
enabl,0.005277
enact,0.014755
end,0.010916
endors,0.007876
enjoy,0.006336
enorm,0.007168
ensur,0.005467
enter,0.004865
enterpris,0.012528
entir,0.008633
entiti,0.016940
entitl,0.061836
entri,0.012875
equiti,0.197993
equival,0.016082
essenti,0.004626
etf,0.013065
euro,0.008467
eurobond,0.056935
eurocertif,0.017432
euroclear,0.032109
eurocommerci,0.017432
euromt,0.017432
euronext,0.013637
euronot,0.034864
europ,0.009025
european,0.004145
eurosecur,0.069729
evas,0.010757
event,0.004331
everyth,0.006627
exact,0.006349
exampl,0.014903
exchang,0.047046
exchangetrad,0.011715
exclud,0.006260
exempt,0.008650
exercis,0.011436
exist,0.006618
expect,0.004572
extens,0.004494
extinguish,0.011095
facil,0.006379
facilit,0.012000
fact,0.004225
fail,0.005003
fall,0.004542
fallen,0.008086
fashion,0.007004
featur,0.009001
feder,0.010597
file,0.014857
financ,0.010547
financi,0.076018
finra,0.013790
firm,0.017832
firstli,0.009802
fiscal,0.007545
fisdsiia,0.017432
fix,0.015847
fledgl,0.011201
forc,0.007134
form,0.043523
formal,0.004453
formerli,0.013332
forward,0.006317
fraction,0.020897
franc,0.005094
fulli,0.005365
function,0.003643
fund,0.053975
fungibl,0.113206
furthermor,0.006098
futur,0.004334
gain,0.008659
gener,0.017900
giant,0.007781
global,0.018904
gold,0.006188
goldman,0.021027
good,0.007913
govern,0.037226
government,0.014896
great,0.004010
greatest,0.006262
gross,0.006958
group,0.003210
grow,0.004563
grown,0.006853
growth,0.013513
ha,0.018984
hand,0.004409
handbook,0.007202
heavili,0.005737
held,0.029453
hi,0.003190
higher,0.008461
highli,0.009369
highrisk,0.011744
hoder,0.016626
hold,0.030006
holder,0.186089
howev,0.017329
howey,0.017432
huge,0.006618
hundr,0.005407
hybrid,0.014870
ibm,0.018303
ident,0.010191
identif,0.007009
immobilis,0.030497
implic,0.006200
import,0.006070
incept,0.009262
includ,0.014822
incom,0.021803
inconsist,0.008270
incorpor,0.004886
increas,0.006844
india,0.010896
individu,0.003670
industri,0.044682
industrywid,0.013790
ineffici,0.008240
inform,0.014152
initi,0.011600
insolv,0.011289
instead,0.004168
institut,0.030126
instrument,0.060277
insur,0.006860
intermedi,0.006994
intermediari,0.045160
intern,0.012474
invest,0.101338
investingcom,0.015421
investor,0.101699
involv,0.003449
ipo,0.046642
island,0.005274
issu,0.099288
issuanc,0.010052
issuer,0.243612
italiana,0.011801
jone,0.039507
juli,0.005140
jurisdict,0.043336
kind,0.004740
kingdom,0.009467
known,0.002973
languag,0.004472
larg,0.009406
largest,0.004914
later,0.007076
launch,0.005992
law,0.010752
legal,0.034367
lend,0.015287
lender,0.009618
lent,0.009834
level,0.010673
lientyp,0.017432
like,0.006355
limit,0.007267
line,0.008516
liquid,0.057397
list,0.026466
loan,0.096638
loantovalu,0.015089
local,0.004029
locat,0.004374
logic,0.005758
london,0.015028
long,0.007725
look,0.004912
low,0.009119
lower,0.008792
luxembourg,0.030341
main,0.007532
maintain,0.016593
mainten,0.007137
major,0.003156
make,0.006050
mallard,0.015089
manag,0.017043
mani,0.013444
margin,0.013039
market,0.146957
markup,0.011715
matur,0.034613
mean,0.022934
measur,0.007793
medium,0.006782
meet,0.004813
member,0.003795
memorandum,0.010513
mere,0.017557
merger,0.008620
method,0.003684
million,0.004672
miss,0.007504
modern,0.003568
monetari,0.006377
money,0.042533
month,0.005256
mr,0.015996
msrb,0.016054
multilater,0.009441
municip,0.021747
mutual,0.025101
nasd,0.015820
nation,0.009889
natur,0.009528
near,0.004891
nearli,0.005181
necess,0.007454
need,0.017881
neg,0.010317
negoti,0.012453
new,0.021258
newli,0.006259
nomine,0.011222
noncertif,0.087161
nonfung,0.032109
nonprofit,0.008580
nontransferoftitl,0.034864
nonu,0.012297
note,0.017149
nscc,0.016626
number,0.014880
oblig,0.032848
obtain,0.004624
offer,0.066356
offici,0.018066
old,0.004922
older,0.006242
onli,0.024646
open,0.004072
oper,0.007506
oppos,0.004971
option,0.024830
ordinari,0.006487
organ,0.019469
organis,0.005632
origin,0.003296
otc,0.022908
otherwis,0.011317
outright,0.009214
outsid,0.013615
outstand,0.016273
overthecount,0.022445
owe,0.014480
owner,0.047104
ownership,0.034932
paid,0.012131
paper,0.024543
parent,0.006699
parlanc,0.011503
parti,0.013792
particip,0.032851
particular,0.003723
partnership,0.014975
pay,0.005554
payment,0.038473
pension,0.024796
peopl,0.003279
perceiv,0.005982
perform,0.004182
permit,0.012118
person,0.019463
perspect,0.005323
physic,0.011678
place,0.010093
placement,0.018674
player,0.007785
polic,0.006616
pool,0.007789
portfolio,0.008183
portion,0.005978
possess,0.005346
possibl,0.003479
postdat,0.013871
practic,0.007299
preelectron,0.016626
prefer,0.015671
previous,0.005200
price,0.020200
primari,0.031521
primarili,0.004663
princip,0.031974
prioriti,0.014387
privat,0.039131
privileg,0.007785
pro,0.009911
problem,0.003648
product,0.003555
profit,0.011788
properti,0.008260
proprietari,0.010203
prospect,0.007249
protect,0.009093
prove,0.010529
provid,0.021187
provinci,0.008250
public,0.037791
publicli,0.007300
purchas,0.029010
purpos,0.004437
pursuant,0.022578
qualifi,0.021521
rais,0.010063
ramp,0.012187
rare,0.005614
rata,0.014233
rate,0.012682
readili,0.007402
reason,0.003882
receiv,0.028934
recent,0.003746
recogn,0.004639
record,0.009120
recov,0.006659
redeem,0.020354
redeliveri,0.017432
refer,0.015144
regard,0.004150
regardless,0.006540
regim,0.006304
regimen,0.011772
region,0.007748
regist,0.107341
registr,0.025273
regul,0.035853
regular,0.005927
regulatori,0.059902
relat,0.002917
repay,0.009144
report,0.004158
repres,0.039039
requir,0.006811
resel,0.012152
residu,0.008351
resourc,0.004380
respect,0.004114
restrict,0.025071
restructur,0.008545
result,0.006027
retail,0.015649
retain,0.005689
return,0.017514
reuter,0.011633
right,0.058236
rise,0.004353
risk,0.010646
roundtabl,0.014044
s,0.003068
sach,0.021480
sale,0.012605
salesmarket,0.017432
satisfi,0.006417
save,0.006143
scarc,0.008285
scenario,0.007836
scheme,0.019534
sebi,0.016320
sec,0.073704
secondari,0.049287
sector,0.011344
secur,0.588369
securitiesbas,0.016626
securitywher,0.017432
seek,0.009709
seen,0.008490
segreg,0.009468
self,0.014870
sell,0.048962
seller,0.008644
senior,0.014341
separ,0.015810
seri,0.004310
serv,0.008613
servic,0.008430
set,0.003351
settl,0.006148
settlement,0.006313
share,0.134910
sharehold,0.026246
shelf,0.010560
short,0.018452
shorter,0.007702
sign,0.004957
signific,0.003843
similar,0.007191
simpl,0.004784
simpli,0.014739
singl,0.008059
slowli,0.006940
smaller,0.005119
smith,0.012758
softwar,0.006379
sold,0.050868
someon,0.007033
sometim,0.015678
sophist,0.006927
sourc,0.011362
sovereign,0.021590
speci,0.005537
special,0.003765
specif,0.010807
specifi,0.012243
sro,0.014233
stakehold,0.009677
standard,0.012166
start,0.003811
startup,0.010468
state,0.021076
statement,0.005527
stock,0.092946
street,0.013741
strict,0.006996
structur,0.007036
subordin,0.008310
subsidiari,0.009106
subsovereign,0.017432
superderiv,0.017432
suppli,0.004908
supposedli,0.009190
supran,0.011953
swan,0.010827
swap,0.009182
taker,0.012083
task,0.005585
tax,0.022635
taxfre,0.012412
telephon,0.008231
term,0.036987
territori,0.005270
themselv,0.004466
therefor,0.004092
therefrom,0.013179
thereof,0.009254
thi,0.018017
thirti,0.016236
thrive,0.008335
thu,0.003519
time,0.005328
titl,0.005404
togeth,0.008304
tot,0.013238
tradabl,0.036457
trade,0.030490
tradeabl,0.013712
tradit,0.018958
transact,0.006483
transfer,0.051525
transferoftitl,0.052296
treasuri,0.016005
treatment,0.005538
trend,0.005751
trust,0.019746
turn,0.008325
type,0.003620
typic,0.036356
ucc,0.015089
ugli,0.023844
ultim,0.010415
uncertif,0.017432
undergo,0.006981
underli,0.005718
underwrit,0.022316
underwritten,0.013427
undivid,0.104973
uniform,0.006981
unit,0.030370
univers,0.003118
unlik,0.005014
unsecur,0.039033
updat,0.007294
upsid,0.012493
use,0.014761
usual,0.010870
util,0.005447
v,0.005368
valu,0.003859
vari,0.004438
variou,0.010283
vendor,0.020621
veri,0.010315
verit,0.012535
version,0.015801
view,0.003859
virgin,0.009222
volum,0.010315
vote,0.017129
wa,0.004937
wall,0.006375
warrant,0.065010
way,0.012878
wherea,0.015674
wherebi,0.006803
wholesal,0.009371
wj,0.013427
word,0.008359
world,0.009158
year,0.003038
