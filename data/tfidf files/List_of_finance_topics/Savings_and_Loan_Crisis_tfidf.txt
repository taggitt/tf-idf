abil,0.013793
abl,0.003165
abolish,0.005237
absenc,0.004607
abus,0.016174
acceler,0.004820
accept,0.006293
accompani,0.004845
accord,0.007518
account,0.044278
accur,0.004270
accus,0.010052
act,0.040103
action,0.018477
activ,0.005001
actual,0.003207
addit,0.017748
adequ,0.010515
adjust,0.004797
administ,0.014739
administr,0.007008
admit,0.005285
adopt,0.003458
advantag,0.011671
advers,0.011740
afford,0.005382
agenc,0.015771
aggrav,0.008125
agricultur,0.003725
aid,0.003759
alan,0.010626
alleg,0.005182
allow,0.024923
alreadi,0.003651
altern,0.003220
america,0.003512
american,0.011358
amort,0.008406
analyst,0.005687
ani,0.004417
anoth,0.002410
anxiou,0.008187
appear,0.002902
appli,0.002679
applic,0.002993
apprais,0.007520
approach,0.002884
appropri,0.008035
approv,0.008611
approxim,0.003415
april,0.003834
arguabl,0.006108
array,0.010938
aspir,0.006300
assess,0.004269
asset,0.050966
assetli,0.023515
assist,0.003736
associ,0.041621
assum,0.003472
attempt,0.002858
attract,0.016131
august,0.007617
austin,0.007210
author,0.011287
authorfinanci,0.013185
avail,0.003063
avoid,0.003743
b,0.003319
babi,0.006552
background,0.004304
bad,0.005141
bailout,0.050012
ban,0.005049
bank,0.130481
banker,0.006844
bankruptci,0.013084
base,0.004367
basic,0.006080
bassist,0.011951
becam,0.005524
becaus,0.009099
becom,0.004838
befor,0.005162
began,0.012070
begin,0.008840
behalf,0.011148
believ,0.009377
bert,0.009211
best,0.006661
better,0.006815
big,0.004575
billion,0.077265
bl,0.068973
black,0.016314
board,0.030518
bodi,0.003032
bond,0.025898
book,0.002872
booknot,0.010423
boom,0.017200
borrow,0.020062
boston,0.005571
branch,0.006427
breach,0.006838
british,0.003374
broader,0.005038
broker,0.039816
brought,0.007450
build,0.022385
builder,0.007352
bundl,0.006719
bureau,0.005716
burst,0.006807
bush,0.062335
busi,0.040881
buy,0.014342
buyer,0.006031
c,0.002892
california,0.004636
came,0.013237
campaign,0.004543
capabl,0.003985
capit,0.013167
car,0.005348
card,0.005750
career,0.004965
carter,0.007147
case,0.007592
cash,0.005058
cassel,0.009005
caus,0.040523
cd,0.013935
celest,0.021154
central,0.002837
centuri,0.010396
certain,0.005540
certif,0.005882
chafe,0.010842
chairman,0.017174
challeng,0.007231
chang,0.022819
character,0.003838
charg,0.015623
charl,0.011629
charlott,0.007983
charter,0.038115
check,0.005247
chief,0.008429
choos,0.004354
chronobibliographi,0.013185
cincinnati,0.008978
cite,0.004233
citi,0.006991
citizen,0.004142
civil,0.003472
claim,0.003072
clash,0.006024
class,0.003357
classic,0.003344
clear,0.007534
close,0.016250
closur,0.006537
collaps,0.035671
collect,0.002952
combat,0.005313
combin,0.005772
comment,0.004848
commerci,0.042659
commiss,0.004030
commission,0.006225
committe,0.021714
commonli,0.003361
commun,0.007940
compar,0.002994
competit,0.003980
complet,0.002795
complex,0.002962
compound,0.004561
concentr,0.007923
concis,0.012331
concomit,0.008580
condit,0.008497
condominium,0.009180
conduct,0.013878
conflict,0.011311
congress,0.022679
consequ,0.010327
consider,0.003402
conspiraci,0.007194
constraint,0.005026
construct,0.015674
consum,0.012092
continu,0.014717
contribut,0.011885
control,0.045724
convent,0.003920
convert,0.004126
convict,0.017600
convinc,0.005389
cooper,0.011607
core,0.004299
corp,0.005972
corpor,0.075465
cost,0.035592
cottag,0.009033
counsel,0.006517
count,0.009234
counti,0.006024
countri,0.002664
coupl,0.009369
court,0.019198
cranston,0.023903
crash,0.006020
creat,0.019809
creation,0.011054
credit,0.011752
crimin,0.005127
criminogen,0.011951
criminologist,0.009786
crisi,0.112796
critic,0.002977
custom,0.020467
cut,0.004297
d,0.003277
damag,0.013106
daughter,0.005556
dave,0.008218
david,0.010788
dawn,0.006407
daz,0.018979
dca,0.020084
deal,0.016306
debacl,0.019390
decad,0.007359
decis,0.006826
declar,0.003910
declin,0.015374
deconcini,0.026370
default,0.006177
defeat,0.004789
delay,0.010341
demand,0.003614
deni,0.004693
denni,0.013881
depart,0.007099
deposit,0.143785
depositinsur,0.013185
depositor,0.042404
depositori,0.058972
depress,0.010059
deputi,0.005421
deregul,0.089781
derelict,0.010220
design,0.008920
despit,0.006865
determin,0.005622
develop,0.004049
dick,0.007561
did,0.003005
didmca,0.012441
difficult,0.003530
difficulti,0.004372
direct,0.002688
directli,0.006505
director,0.029205
disast,0.005592
discount,0.005844
discov,0.003565
disintermedi,0.010577
dispos,0.005989
dissert,0.006894
distant,0.011254
district,0.004896
divest,0.017799
dmi,0.023903
doe,0.005478
doh,0.026370
dollar,0.004730
domest,0.004194
don,0.006719
donald,0.005707
doubl,0.004482
dozen,0.005662
drain,0.006981
dramat,0.013715
dream,0.005844
drive,0.004349
drop,0.004443
dtx,0.012441
dub,0.006512
dure,0.009321
duti,0.009634
dutton,0.009149
dwindl,0.008125
e,0.003280
eager,0.014850
earli,0.012188
earlier,0.007203
earn,0.013743
easier,0.005218
eastern,0.004045
econom,0.018671
economi,0.016642
economist,0.004575
ed,0.006824
effect,0.014206
effort,0.023628
eisenhow,0.008480
eli,0.007745
elimin,0.008429
ellefson,0.013185
emerg,0.003123
employ,0.003232
employe,0.005107
enabl,0.003865
enact,0.016208
encourag,0.004005
encyclopedia,0.004197
end,0.018653
energi,0.003412
enforc,0.009138
engag,0.003859
ensu,0.005958
ensur,0.004004
enter,0.003563
entir,0.003161
environ,0.006461
era,0.003850
erta,0.012176
especi,0.008490
essenti,0.003388
establish,0.002601
estat,0.051884
estim,0.006859
ethic,0.013457
evalu,0.008446
event,0.003171
eventu,0.003464
exacerb,0.006748
examin,0.007629
examinationsupervis,0.012766
exampl,0.002182
exceed,0.005118
excess,0.004710
exclus,0.008327
execut,0.007384
exercis,0.004187
expand,0.006895
expans,0.012337
expens,0.013059
experi,0.008991
experienc,0.013127
expertis,0.005945
explicit,0.005321
explicitli,0.004961
explod,0.007170
explos,0.005402
extend,0.003214
extens,0.006582
extern,0.001905
extrem,0.003496
facilit,0.004394
factor,0.006180
fail,0.040309
failur,0.042406
fall,0.006653
falloff,0.011050
famili,0.013499
familiar,0.005156
fanni,0.017956
far,0.003304
farm,0.004862
fatal,0.006335
fdic,0.076228
feder,0.135821
fee,0.011409
fellow,0.005510
felt,0.005145
fhfb,0.012441
fhlbb,0.025532
fiduciari,0.008519
figur,0.003609
final,0.003152
financ,0.030896
financi,0.073068
fine,0.005210
firm,0.008706
firrea,0.023515
fix,0.011605
fixedr,0.021320
focu,0.003575
follow,0.006254
footnot,0.006497
forbear,0.042641
forbearanceallow,0.013185
forc,0.007836
foreclosur,0.009695
form,0.005976
format,0.003554
formerli,0.004882
forprofit,0.008559
fortun,0.005912
fractionalreserv,0.009489
franklin,0.005962
fraud,0.108061
fraudul,0.007250
freddi,0.018423
fricker,0.012176
fslic,0.127663
fund,0.035935
futur,0.003174
gain,0.003170
garnst,0.033152
gather,0.004543
gave,0.003792
gener,0.013109
georg,0.003756
georgetown,0.008388
germain,0.026058
germani,0.003924
ginni,0.010842
glenn,0.015399
goal,0.003584
govern,0.024783
government,0.005454
governmentback,0.010099
governor,0.004783
gradual,0.004189
grant,0.007990
gray,0.006502
great,0.005874
greater,0.006565
greatest,0.004586
greatestev,0.012441
greenwood,0.021902
grew,0.004355
grossli,0.008539
group,0.007052
grow,0.006683
growth,0.029690
guarante,0.004780
guilti,0.006994
h,0.007240
ha,0.001737
habit,0.005525
hal,0.008234
half,0.003587
hand,0.003229
harder,0.006477
havoc,0.009149
hazard,0.011903
head,0.003364
health,0.003633
hear,0.005495
hefti,0.010498
help,0.008559
henc,0.004066
henderson,0.007309
hi,0.016357
high,0.018473
higher,0.015491
highest,0.003991
highli,0.006861
highlight,0.005366
highway,0.013155
highyield,0.009344
histori,0.008683
historian,0.004391
hold,0.009417
holiday,0.012269
holland,0.006789
home,0.048380
homeown,0.009180
homeownership,0.011294
hope,0.004437
hous,0.020977
howev,0.004230
hurt,0.006831
identifi,0.003150
ii,0.006821
immedi,0.003831
immens,0.006262
impact,0.003571
implic,0.009081
import,0.008891
improperli,0.008825
improv,0.006414
imprud,0.010285
inabl,0.006291
inadequ,0.006189
incent,0.005426
includ,0.010855
incom,0.003991
increas,0.035087
indecis,0.009061
independ,0.002676
indianapoli,0.007571
indict,0.008352
individu,0.008064
industri,0.056521
industrialist,0.007780
inflat,0.053990
influenc,0.005741
influencepeddl,0.012441
inform,0.002591
initi,0.005663
innov,0.004514
inquiri,0.005198
insid,0.013113
insolv,0.057873
instanc,0.010588
institut,0.093767
instrument,0.004013
insuffici,0.005908
insur,0.130638
intend,0.004034
interestbear,0.009452
interestr,0.009276
interst,0.007780
interven,0.011200
interview,0.005389
introduc,0.003057
invest,0.037107
investig,0.010955
involv,0.010106
isbn,0.027728
issu,0.008389
j,0.006546
januari,0.007438
jeopardi,0.009149
jim,0.013776
jimmi,0.007722
job,0.004500
john,0.011857
johnson,0.005955
jr,0.016607
judgment,0.005523
juli,0.003764
june,0.003797
junk,0.023949
k,0.019756
keat,0.077979
kenneth,0.005847
kingdom,0.003466
knowledg,0.003277
known,0.017420
l,0.007671
lack,0.013289
larg,0.016074
larger,0.003439
largest,0.010796
late,0.024134
later,0.007774
law,0.010499
lawrenc,0.005829
lax,0.008406
leader,0.007620
leadership,0.004727
leagu,0.005346
learn,0.007320
leav,0.003689
led,0.028121
legal,0.007191
legisl,0.015957
lend,0.072772
lender,0.014087
lenient,0.009415
lent,0.007202
lesson,0.018347
liabil,0.006123
liberti,0.005530
librari,0.004093
life,0.002800
lift,0.005816
like,0.006981
limit,0.013304
lincoln,0.044385
line,0.003118
link,0.005683
list,0.007268
littl,0.006669
live,0.002817
loan,0.414523
local,0.005902
longterm,0.009170
loot,0.014758
loss,0.034297
lost,0.003786
loui,0.004916
louisiana,0.007530
low,0.006678
lower,0.003219
lowest,0.005182
lowi,0.011167
lure,0.008352
lyndon,0.008621
lyric,0.007853
mac,0.015163
mae,0.027016
major,0.009245
make,0.026586
mampel,0.013185
manag,0.021842
mani,0.031508
manner,0.008409
march,0.010838
mari,0.005033
mark,0.006996
market,0.039135
martin,0.009223
maryland,0.007234
masica,0.013185
mason,0.007540
matter,0.013109
matur,0.005069
mayer,0.016343
mccain,0.019479
mcgrawhil,0.006296
mean,0.002399
meanwhil,0.005268
mechan,0.003217
meet,0.003525
megadeth,0.023903
member,0.019458
membership,0.009597
men,0.004040
michael,0.012028
mid,0.004338
midwest,0.016812
midwestern,0.009149
militari,0.003524
milken,0.010842
million,0.023954
mind,0.004108
minim,0.004598
minimum,0.014257
minneapoli,0.008110
minnesota,0.021068
mismatch,0.015756
misunderstand,0.007155
moderateincom,0.012176
modern,0.002613
monetari,0.023352
money,0.027255
moral,0.013075
moreov,0.004998
mortgag,0.148003
mortgagerel,0.011757
mostli,0.003717
mount,0.005079
movement,0.006008
multifamili,0.011167
multipl,0.006701
muolo,0.013185
mutual,0.004595
nation,0.021727
nationwid,0.006619
nd,0.008107
nearli,0.003794
necessari,0.003472
need,0.002619
neg,0.003777
negoti,0.004560
neil,0.034604
net,0.022899
new,0.038921
nonprofit,0.006283
nonresidenti,0.010577
note,0.002511
notforprofit,0.008156
novdec,0.011432
novemb,0.007815
number,0.028334
numer,0.003227
obviou,0.005503
occur,0.008304
oclc,0.006275
octob,0.003767
offer,0.012958
offic,0.017534
offici,0.006615
offset,0.006335
ohio,0.021110
ohiobas,0.013185
oil,0.008697
oklahoma,0.007903
old,0.003604
olson,0.008267
onebranch,0.012766
ongo,0.004767
onli,0.020055
open,0.002982
openand,0.012766
oper,0.016491
opportun,0.004115
opportunist,0.008500
oppos,0.003640
option,0.009092
order,0.007202
organ,0.007129
organiz,0.005707
origin,0.004828
ot,0.009452
otherwis,0.016576
outstand,0.005958
overal,0.003987
overbuild,0.011951
overdrawn,0.011586
overextend,0.009452
overse,0.006497
oversight,0.034506
overturn,0.007147
owner,0.004928
oxford,0.004093
paid,0.044421
panel,0.006420
panic,0.007309
particip,0.006873
particular,0.005454
particularli,0.003055
partner,0.009525
pass,0.020307
passag,0.005190
paul,0.011361
pay,0.024404
payment,0.004695
peddl,0.010423
peopl,0.009608
percent,0.026254
percentag,0.004854
period,0.010354
perman,0.004222
permit,0.013312
person,0.005701
phd,0.005629
pioneer,0.004551
pizzo,0.013185
place,0.009855
plead,0.008370
plu,0.004901
polic,0.004845
polici,0.009672
polit,0.016406
politician,0.005226
poor,0.004307
poorli,0.005768
popular,0.003116
portfolio,0.011985
pose,0.005306
possibl,0.005096
post,0.004228
postwar,0.005925
power,0.007669
practic,0.010691
praeger,0.008825
preced,0.004733
predat,0.005545
preliminari,0.006663
premium,0.006388
present,0.002498
preserv,0.004210
presid,0.014451
presidenti,0.005108
press,0.011761
pressur,0.011282
presum,0.005576
preval,0.005500
prevent,0.007143
previous,0.003808
prewar,0.007481
price,0.007396
primarili,0.010244
princip,0.003902
prison,0.005302
privat,0.003582
problem,0.024046
proce,0.006082
procedur,0.008701
proceed,0.004571
process,0.004723
product,0.005207
profession,0.004048
professor,0.004465
profit,0.012949
program,0.009428
prohibit,0.005097
project,0.003091
promis,0.009388
promot,0.010691
proport,0.008261
prosecutor,0.007903
prospect,0.005308
protect,0.013318
provid,0.002216
prudent,0.008267
public,0.007548
publish,0.005545
purchas,0.004249
q,0.005595
qualifi,0.015761
quick,0.006173
r,0.003343
race,0.004908
racket,0.018423
radiat,0.005003
rais,0.014739
rampant,0.007916
rang,0.002998
rapidli,0.004211
rate,0.114552
ratio,0.004505
raz,0.021154
real,0.032885
realiz,0.004430
reason,0.008530
rebuk,0.009211
receiv,0.006054
recess,0.011341
record,0.003339
recoveri,0.015824
reduc,0.012006
refer,0.003168
reform,0.019378
regard,0.003039
regul,0.078770
regulatori,0.049352
reinvent,0.007929
rel,0.002840
relat,0.004272
relax,0.006056
relianc,0.006352
remain,0.005240
reopen,0.007202
rep,0.009528
repaid,0.008424
repay,0.006696
replac,0.006457
report,0.009135
repres,0.010396
republican,0.005659
requir,0.012471
reserv,0.021069
resid,0.004197
residenti,0.013639
resign,0.005414
resolut,0.033929
resolv,0.023011
resort,0.005879
resourc,0.003207
respons,0.013740
restor,0.004480
restraint,0.014234
restrict,0.003672
result,0.017656
retain,0.008333
return,0.022446
revers,0.004362
review,0.003550
revolut,0.003925
riegl,0.023515
rise,0.015941
risk,0.015594
riskbas,0.010352
riski,0.006719
riskier,0.017601
road,0.004602
rob,0.007571
robberi,0.017462
robert,0.003483
robinson,0.019491
role,0.005551
roller,0.009180
roosevelt,0.007080
rose,0.009616
rtc,0.065658
rule,0.008676
run,0.006963
rural,0.005124
rush,0.006920
s,0.060674
saif,0.020847
salari,0.005860
sale,0.004615
save,0.305959
say,0.003432
scam,0.008621
scandal,0.026763
scenario,0.005739
scheme,0.004768
scholar,0.004023
school,0.003223
scribner,0.008406
sec,0.007710
second,0.002600
sector,0.004153
secur,0.006732
seek,0.003555
segreg,0.006934
seidman,0.011050
select,0.003445
sell,0.013446
senat,0.021742
sens,0.003260
sentenc,0.005416
separ,0.002894
seri,0.003156
serv,0.009461
servicemen,0.009276
set,0.007363
settl,0.009006
settlement,0.004623
sever,0.009195
share,0.009262
sharp,0.005355
sharpli,0.006233
short,0.006756
shortterm,0.011519
sign,0.010891
signific,0.002814
significantli,0.004066
silverado,0.118665
similar,0.010532
similarli,0.004177
simpli,0.003598
simultan,0.004621
sinc,0.002205
singl,0.005902
situat,0.006790
sixcount,0.013185
sixth,0.005854
sl,0.385038
slow,0.009105
slowdown,0.007929
small,0.005396
socal,0.004449
social,0.005610
societi,0.008288
somewhat,0.004674
son,0.008444
song,0.011695
soon,0.004043
sort,0.004625
sought,0.004295
sourc,0.002773
spark,0.005835
spearhead,0.007699
special,0.002757
specif,0.002638
specul,0.013763
speech,0.004750
spend,0.004577
spoke,0.006085
spread,0.003897
spur,0.006193
st,0.007109
staff,0.010396
stagflat,0.008800
stake,0.006707
standard,0.014850
state,0.069459
statechart,0.021320
statement,0.004048
steadi,0.011303
steer,0.007283
stem,0.004956
steven,0.005556
stock,0.008508
stockbrok,0.009415
stockhold,0.017039
strategi,0.004107
stream,0.005285
street,0.005031
strengthen,0.004987
strong,0.003204
structur,0.005152
subprim,0.017079
subsequ,0.003441
subsidiari,0.006668
substant,0.007462
substanti,0.008187
suburban,0.017039
success,0.005848
sudden,0.012626
suffer,0.004024
suit,0.005125
summari,0.005306
supervis,0.015817
supervisori,0.033004
support,0.005243
suprem,0.009031
surg,0.006807
surround,0.004038
susan,0.006742
symptom,0.006275
systemat,0.004212
taken,0.009686
takeov,0.006825
taught,0.004916
tax,0.016576
taxpay,0.051531
taxpayerfund,0.011050
technic,0.003921
technolog,0.003179
texa,0.018676
th,0.010113
themselv,0.013083
thi,0.031337
thrift,0.394681
tilt,0.007865
time,0.019510
togeth,0.003040
tolchin,0.013185
told,0.005592
took,0.006889
total,0.009230
trade,0.006379
tradit,0.002776
train,0.003807
transact,0.018991
transform,0.003609
transit,0.003838
translat,0.003796
traumat,0.007623
treasuri,0.005860
trend,0.004211
trillion,0.006146
true,0.003570
trust,0.024102
truth,0.004691
turmoil,0.006875
turn,0.003048
type,0.007953
typic,0.002958
ultim,0.015255
unanim,0.006954
unbecom,0.011757
uncontrol,0.007666
underestim,0.007361
understand,0.002993
uniform,0.010225
uniqu,0.003759
unit,0.033361
univers,0.011419
unpreced,0.012524
unsound,0.018619
unusu,0.005346
unwilling,0.008708
urban,0.004625
use,0.009008
v,0.007863
valu,0.008478
vari,0.003250
varieti,0.003310
variou,0.002510
vast,0.004427
ventur,0.011615
veri,0.002518
vernon,0.008284
vice,0.005105
vietnam,0.005975
violat,0.009757
virtu,0.005587
virtual,0.004360
volcker,0.009489
volum,0.003777
vote,0.008363
vulner,0.011216
w,0.007399
wa,0.083169
wall,0.004669
war,0.015662
washington,0.009709
wave,0.008893
way,0.011788
weak,0.004380
weaker,0.006279
welldefin,0.006572
went,0.004044
western,0.003316
wherea,0.003826
whi,0.004057
white,0.004186
wholli,0.006416
wide,0.005680
wider,0.005021
william,0.017667
winstar,0.013185
wire,0.006370
withdraw,0.010600
withdrew,0.006197
women,0.004196
work,0.002161
workingclass,0.008335
world,0.006706
worsen,0.013166
worth,0.024964
worthless,0.008318
wreak,0.009695
wright,0.019626
wring,0.011757
write,0.003447
written,0.003491
wrongdo,0.008925
x,0.007808
year,0.011127
yearn,0.009211
york,0.020377
zvi,0.009609
