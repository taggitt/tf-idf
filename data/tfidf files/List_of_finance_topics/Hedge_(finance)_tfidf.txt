abl,0.008264
abov,0.004188
abroad,0.007104
accident,0.008461
account,0.007705
activ,0.003264
actor,0.007541
actual,0.016747
adopt,0.004514
advers,0.007661
affect,0.008549
agre,0.024661
agreement,0.014980
agricultur,0.014587
airlin,0.024981
allow,0.013012
alreadi,0.004765
alway,0.004399
ani,0.011532
anoth,0.025171
appli,0.006994
approach,0.011294
arbitrag,0.020100
arbitragefre,0.012242
aris,0.005336
articl,0.007775
artifici,0.005524
asset,0.006047
assetli,0.015346
associ,0.006391
assum,0.004531
autumn,0.008704
away,0.004983
b,0.030331
backtoback,0.027211
balanc,0.005177
bank,0.014192
base,0.002850
basic,0.007936
bb,0.010299
bbstrategi,0.017209
becam,0.003605
becaus,0.017816
becom,0.003157
befor,0.003368
believ,0.008159
benefit,0.009573
bet,0.059870
beta,0.024204
better,0.008896
blackisgreen,0.103259
bond,0.011268
bonus,0.011456
borrow,0.006546
bs,0.010748
bushel,0.025933
busi,0.013340
buy,0.087361
buyer,0.047236
calcul,0.025168
candid,0.006085
capit,0.008593
case,0.009909
cash,0.019809
categori,0.005040
caus,0.003526
centuri,0.006784
certain,0.025311
cfd,0.012437
chanc,0.006359
chang,0.011914
chicago,0.006438
choos,0.011366
classic,0.004365
clearinghous,0.036196
close,0.007070
closer,0.026150
coal,0.046223
collaps,0.005820
come,0.022673
commerci,0.010124
commit,0.011075
commod,0.083367
common,0.006389
compani,0.137682
companion,0.007620
compar,0.003908
competitor,0.008185
complex,0.003866
compris,0.005405
concentr,0.005170
concept,0.007374
conceptu,0.006359
consid,0.009244
constantli,0.007366
construct,0.004091
consum,0.005261
consumpt,0.006163
contract,0.275632
contractu,0.009390
contrari,0.006909
control,0.010532
converg,0.006960
convers,0.010903
convert,0.016159
corpor,0.005184
correl,0.006159
corridor,0.009950
cost,0.009291
countri,0.006954
cours,0.004605
cover,0.013051
crash,0.007858
credit,0.010226
crop,0.006532
crude,0.008297
currenc,0.104227
current,0.013531
custom,0.026715
damag,0.005702
date,0.039747
day,0.039874
deal,0.008513
debt,0.006127
decid,0.025748
decreas,0.031221
default,0.008063
defin,0.003626
deflect,0.009790
degre,0.004327
deliv,0.018684
deliveri,0.038185
delta,0.015137
deltahedg,0.031786
demand,0.028305
depreci,0.009442
deriv,0.032142
desir,0.014919
develop,0.007928
differ,0.033615
direct,0.007017
directli,0.004245
discount,0.007628
discourag,0.007977
diversif,0.008777
dodg,0.012196
doe,0.003575
dollar,0.024697
dramat,0.011935
drawback,0.010283
driven,0.006151
drop,0.005799
dure,0.003041
dynam,0.004954
earli,0.006363
earlier,0.004700
earn,0.005979
easier,0.006811
economi,0.004344
effect,0.012362
effici,0.014943
elect,0.004692
electr,0.015780
elsewher,0.006558
emot,0.019605
employe,0.026663
encount,0.006265
end,0.003478
energi,0.017814
engag,0.010075
england,0.005520
english,0.004430
ensur,0.010453
entail,0.007423
entir,0.012379
equal,0.004332
equiti,0.058233
equival,0.015373
eso,0.040539
essenti,0.008844
establish,0.006790
estim,0.004476
etymolog,0.006772
eur,0.021851
evad,0.009855
event,0.008280
everi,0.007961
everyon,0.007344
evil,0.007738
exact,0.006069
exampl,0.034190
exchang,0.062959
exchangetrad,0.011199
execut,0.004819
exot,0.008474
expand,0.004500
expect,0.030596
expens,0.011363
expir,0.017198
expiri,0.011199
export,0.005835
exposur,0.021210
extern,0.002487
face,0.019099
facil,0.006098
fan,0.008949
farmer,0.121204
fasb,0.013107
favor,0.005254
feder,0.005065
felt,0.006715
fenc,0.010586
fiction,0.006785
fifti,0.008063
financ,0.030245
financi,0.068123
financialeducom,0.029152
firm,0.005681
fix,0.015148
fixedincom,0.013513
flow,0.015029
fluctuat,0.028416
forecast,0.022484
foreign,0.039855
forward,0.144930
fra,0.012542
fraction,0.006658
free,0.007771
ftse,0.025195
fuel,0.050432
fulfil,0.006879
fund,0.014071
fungibl,0.012023
futur,0.186447
gain,0.016554
gambl,0.018842
game,0.005837
gbp,0.037628
gener,0.009777
global,0.009035
goe,0.012243
good,0.003782
govern,0.006469
greatli,0.011141
group,0.003068
grow,0.004362
guarante,0.012480
ha,0.024951
half,0.004682
hand,0.004215
happen,0.005376
hard,0.005583
harvest,0.060329
health,0.004742
heat,0.005998
hecg,0.017209
hedg,0.620022
hedgeabl,0.017209
hedger,0.013806
hi,0.021351
highli,0.008956
histor,0.003821
home,0.004857
honor,0.007037
hour,0.005977
hous,0.004563
household,0.027068
howev,0.008282
hurrican,0.009085
ia,0.010626
idea,0.003818
ident,0.009741
immedi,0.005000
immun,0.007446
impli,0.010577
import,0.002901
impos,0.005850
imposs,0.005752
includ,0.018891
incom,0.005210
increas,0.032712
incur,0.008157
index,0.021616
indic,0.004184
individu,0.003508
industri,0.031062
industryrel,0.017209
influenc,0.003746
infrastructur,0.005759
injuri,0.007541
instead,0.003984
instrument,0.036666
insur,0.032791
intend,0.005266
interestbear,0.012337
introduct,0.004509
invers,0.007183
invest,0.072651
investor,0.045366
involv,0.009893
iraq,0.007612
irp,0.015893
issu,0.003650
issuanc,0.009608
item,0.018720
jet,0.016642
just,0.011899
katrina,0.012489
know,0.015462
known,0.008526
languag,0.004274
larg,0.014986
leav,0.004816
lesser,0.006972
let,0.006372
level,0.003400
liabil,0.007992
life,0.007311
like,0.006075
limit,0.006946
link,0.002472
liquid,0.018288
list,0.003162
live,0.003677
loan,0.006598
lock,0.029464
long,0.036925
longer,0.004690
longshort,0.028304
longterm,0.005984
lose,0.023925
loss,0.049741
lost,0.004942
lot,0.007004
low,0.004358
lower,0.012606
mainli,0.004721
make,0.020243
manag,0.008145
mani,0.015422
market,0.136218
marri,0.007129
match,0.018142
math,0.008815
mathemat,0.008941
matur,0.013234
mean,0.021922
metal,0.011837
method,0.003521
minim,0.018005
mismatch,0.010283
mitig,0.016348
model,0.007084
moment,0.006469
monetari,0.006096
money,0.060986
month,0.010048
mostli,0.004851
movement,0.015685
multicurr,0.016663
mutual,0.005998
mwh,0.045368
natur,0.012143
ndf,0.042457
nearbi,0.006998
necessari,0.004531
need,0.006837
neg,0.009862
net,0.005977
neutral,0.018673
new,0.005080
news,0.012413
nondeliver,0.015893
nonfinanci,0.012065
notori,0.009024
nullifi,0.011172
number,0.008534
oblig,0.018839
obligatori,0.010666
obligor,0.015122
occasion,0.006415
offer,0.004228
offset,0.033075
oil,0.011351
old,0.004705
onc,0.012747
oneself,0.008901
onli,0.005235
open,0.019465
oper,0.010762
oppon,0.007064
opportun,0.005371
oppos,0.009504
opposit,0.014037
option,0.089007
order,0.012535
organ,0.003101
origin,0.009454
ought,0.008163
outcom,0.011627
overthecount,0.010727
owe,0.006921
owner,0.012864
paid,0.005798
pair,0.012120
parent,0.006404
pariti,0.008185
parti,0.013183
patriot,0.008297
pay,0.026545
payment,0.006129
payout,0.022618
peopl,0.006270
perform,0.007995
period,0.010136
person,0.011163
physic,0.007442
pick,0.007402
plant,0.014811
play,0.008211
point,0.003298
polici,0.008416
polit,0.003569
pool,0.052122
portfolio,0.023467
pose,0.006926
posit,0.064034
possibl,0.003326
potenti,0.012665
practic,0.006977
preciou,0.008558
predefin,0.010706
predetermin,0.009339
predict,0.009542
premium,0.008338
prepurchas,0.015893
pressur,0.004908
price,0.255856
primarili,0.004457
problem,0.003487
produc,0.020357
product,0.023790
profit,0.033804
prohibit,0.006653
project,0.004034
properti,0.003948
protect,0.017384
provid,0.005786
public,0.003284
publish,0.007238
purchas,0.027730
quantiti,0.005311
quarter,0.006782
question,0.008189
rate,0.076780
rebat,0.011865
receiv,0.011853
record,0.004359
reduc,0.027425
refer,0.010340
regret,0.010727
regul,0.004896
rel,0.003708
relat,0.008365
remain,0.003420
renegoti,0.023037
repres,0.003392
requir,0.006511
reserv,0.005500
respect,0.007865
restrict,0.009586
retail,0.037398
return,0.008370
revenu,0.036200
revers,0.011387
right,0.011133
rise,0.008323
risk,0.259520
riskless,0.012654
risktransf,0.016663
rival,0.006833
rose,0.006275
s,0.005866
said,0.004249
sale,0.012049
save,0.005872
season,0.013366
second,0.006788
secur,0.013181
select,0.008994
sell,0.093605
seller,0.024789
sens,0.004255
set,0.003203
settl,0.011755
share,0.048359
shop,0.007716
short,0.083781
sign,0.004738
signal,0.011901
signup,0.017209
similar,0.020621
similarli,0.005452
simpl,0.009146
simul,0.006253
simultan,0.006031
sinc,0.014390
singl,0.007703
sold,0.006078
sophist,0.006622
sort,0.006036
southwest,0.008078
specif,0.006887
specifi,0.023407
sport,0.006835
spot,0.028823
spread,0.005087
stack,0.008974
stand,0.005409
standard,0.007753
state,0.005036
stay,0.006300
stock,0.133270
stockfutur,0.017209
stori,0.011668
straddl,0.011069
strategi,0.064338
stricter,0.010402
strictli,0.006787
strike,0.019206
stronger,0.006929
structur,0.003362
subsidiari,0.008704
substanti,0.005343
suffer,0.010506
suggest,0.004029
summer,0.006658
superhedg,0.016239
suppli,0.018768
sureti,0.013107
swap,0.026333
synthet,0.035692
systemat,0.005498
taken,0.004214
team,0.012021
techniqu,0.013269
term,0.002719
th,0.006600
therefor,0.007824
thi,0.030139
thought,0.004079
threat,0.006036
tie,0.005614
time,0.012732
took,0.004496
tool,0.004608
topic,0.004566
tracker,0.037011
trackercurv,0.017209
trade,0.058290
trader,0.077941
tradit,0.003624
transact,0.024789
transpar,0.007366
trend,0.005497
tri,0.009060
truli,0.007212
twoway,0.010835
type,0.027683
typic,0.011584
uncertainti,0.006698
underli,0.010931
underpr,0.013424
understand,0.003906
undesir,0.008999
unexpect,0.008405
unfavor,0.010202
uniqu,0.004907
unit,0.002903
unwant,0.009585
usd,0.017253
use,0.042332
usual,0.017318
valu,0.055334
vari,0.008486
varieti,0.004321
variou,0.003276
verb,0.009518
veri,0.003286
versa,0.008168
vice,0.006663
visibl,0.006361
vodafon,0.051866
volatil,0.045946
volum,0.019720
wa,0.004719
want,0.021255
war,0.004088
way,0.015387
weaker,0.008196
weather,0.007058
wheat,0.147330
wherev,0.009231
wholesal,0.026874
wide,0.007414
widget,0.091752
win,0.006471
winter,0.035660
wipe,0.009319
word,0.007990
worsen,0.008592
worth,0.013033
year,0.005809
yield,0.022490
