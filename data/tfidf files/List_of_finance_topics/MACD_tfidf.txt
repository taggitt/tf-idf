abov,0.017671
absolut,0.005853
acceler,0.026548
accompani,0.006671
action,0.004240
actual,0.004416
ad,0.004403
addit,0.013964
algorithm,0.006918
alway,0.004640
analysi,0.016639
analyst,0.023493
andor,0.006038
ani,0.006082
anoth,0.003319
anticip,0.007674
apart,0.006323
apo,0.029355
appel,0.051912
appli,0.011067
approach,0.003971
approxim,0.009405
aspray,0.018153
asprey,0.017130
attribut,0.010728
averag,0.103087
avoid,0.005154
axi,0.023742
b,0.004570
bar,0.046802
base,0.009020
basic,0.004186
bearish,0.043364
becaus,0.003132
becom,0.003330
befor,0.003553
better,0.004692
break,0.005735
bullish,0.070360
buy,0.019747
c,0.003982
calcul,0.021239
chang,0.050270
characterist,0.009606
chart,0.015876
claim,0.008460
classif,0.006149
classifi,0.005637
close,0.003729
collect,0.004065
come,0.003986
common,0.003370
commonli,0.004628
compar,0.016489
comput,0.004618
confirm,0.024035
conform,0.007692
constant,0.033248
context,0.004903
continu,0.003377
converg,0.007342
convergencediverg,0.016188
correl,0.006497
correspond,0.005059
creat,0.003409
cross,0.042585
crossov,0.156924
customarili,0.011452
d,0.004512
daili,0.013038
data,0.004448
day,0.037855
deal,0.004490
decis,0.004699
declin,0.005292
definit,0.013222
denot,0.006801
depend,0.003677
deriv,0.033904
detrend,0.014928
differ,0.047278
direct,0.029610
disagr,0.007926
display,0.006334
diverg,0.120533
divid,0.008839
doe,0.011314
dpo,0.017577
drift,0.008593
dub,0.008966
durat,0.015876
element,0.004267
ema,0.180765
emphas,0.005889
ensur,0.005513
equal,0.013710
equival,0.005405
errat,0.011294
especi,0.007793
estim,0.023611
event,0.013101
everi,0.004199
evid,0.004576
exampl,0.009016
exponenti,0.024233
extern,0.002624
factor,0.004254
fals,0.040835
famili,0.004646
fast,0.020608
filter,0.133827
follow,0.002870
forecast,0.007906
frequenc,0.006857
gain,0.013097
gaug,0.008872
gener,0.002578
gerald,0.037228
graph,0.031935
greater,0.004519
greatli,0.005876
ha,0.004785
half,0.004939
hand,0.004446
happen,0.005671
height,0.007154
held,0.004242
high,0.010900
higher,0.008532
highlight,0.007388
histogram,0.079046
histor,0.004030
histori,0.002988
horizont,0.016550
howev,0.002912
ignor,0.006224
import,0.003060
includ,0.002491
increas,0.003450
index,0.005700
indic,0.052972
inher,0.006876
input,0.006501
intermedi,0.007052
interpret,0.019149
invent,0.005838
known,0.002998
lack,0.004574
lag,0.037358
late,0.004153
length,0.005952
level,0.007174
like,0.006408
line,0.090165
link,0.002608
long,0.003895
longer,0.009895
look,0.004953
low,0.027585
lower,0.008865
lowpass,0.043056
macd,0.861046
macdabc,0.018153
mainli,0.004980
major,0.003182
make,0.009151
match,0.006379
mathemat,0.004715
mean,0.003303
measur,0.011787
member,0.003827
metric,0.008056
miss,0.007566
modern,0.003598
moment,0.006824
momentum,0.015772
month,0.005299
movement,0.004136
multipl,0.004613
multipli,0.015466
necessari,0.004780
neg,0.020806
new,0.010717
notat,0.008060
number,0.006001
occur,0.019055
old,0.004963
onli,0.005522
order,0.003305
oscil,0.069445
overload,0.024105
overrul,0.012325
paramet,0.020746
particular,0.003754
particularli,0.004206
pass,0.004660
pattern,0.005118
percentag,0.013368
period,0.024949
platform,0.007345
plot,0.022055
point,0.003479
popular,0.004291
posit,0.014220
possibl,0.003508
power,0.003520
ppo,0.046127
practic,0.007360
prefer,0.015802
price,0.162952
probabl,0.004935
process,0.003251
profit,0.005943
proper,0.012342
provid,0.003051
prudent,0.011383
push,0.006541
quickli,0.005598
r,0.004602
rang,0.004129
read,0.003788
reason,0.003915
recent,0.007555
recommend,0.006430
reduc,0.004132
refer,0.010907
rel,0.007822
remain,0.003607
repres,0.007157
respect,0.012446
respond,0.005883
reveal,0.017214
rsi,0.013677
s,0.006188
scale,0.009875
second,0.003580
secur,0.027809
seen,0.004280
sell,0.012342
seri,0.130377
set,0.016896
setup,0.010210
shift,0.010573
short,0.018605
shortterm,0.007930
shown,0.005260
sign,0.009997
signal,0.100432
signallin,0.036307
signific,0.007751
significantli,0.005598
sinc,0.003035
singl,0.004063
situat,0.009349
slow,0.018804
smaller,0.005162
smooth,0.008051
special,0.003797
specif,0.003632
standard,0.012267
stick,0.009129
stock,0.087862
strategi,0.011311
strength,0.018473
subsequ,0.004737
substanti,0.005636
subtl,0.017959
sudden,0.008692
suddenli,0.009007
suggest,0.004250
sum,0.006056
suppos,0.006671
taken,0.004445
tandem,0.021945
technic,0.026998
term,0.017213
terminolog,0.007149
thi,0.006812
thoma,0.010861
thu,0.003549
time,0.045666
track,0.006717
trade,0.030743
trader,0.014948
trend,0.069589
true,0.009831
ultim,0.005251
underli,0.017297
upward,0.008451
use,0.019846
usual,0.007307
valu,0.027239
vari,0.008951
variat,0.005691
varieti,0.004558
veloc,0.015967
wa,0.007468
week,0.025524
weekli,0.009007
wherea,0.005268
william,0.004865
work,0.002976
year,0.003064
zero,0.044694
