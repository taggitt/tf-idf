aag,0.018630
academ,0.006612
acc,0.017497
account,0.020797
achiev,0.005553
act,0.005045
activ,0.004404
acut,0.023172
ad,0.005633
addit,0.031262
adjust,0.016899
administ,0.008653
admiss,0.011775
admit,0.018618
adult,0.017284
advanc,0.010904
advantag,0.006852
affect,0.005768
aggreg,0.009335
aid,0.006622
air,0.027125
airway,0.028625
al,0.007572
alexand,0.008458
alli,0.008153
alphabet,0.010630
american,0.015005
anaesthesiolog,0.042893
analges,0.016853
analysi,0.010643
andor,0.015450
andrew,0.008965
anesthesiolog,0.034473
annual,0.013562
anoth,0.004245
ansthesiologisk,0.023223
antibiot,0.012457
approach,0.010161
arbejdet,0.023223
area,0.013153
array,0.009632
arrhythmia,0.018630
arriv,0.007106
aspen,0.018630
aspir,0.011097
assist,0.006582
associ,0.008624
australia,0.023019
author,0.004970
avail,0.021585
averag,0.006593
ba,0.011745
baltimor,0.012997
bander,0.023223
base,0.003846
basi,0.005498
becam,0.014596
becaus,0.004006
becom,0.008522
bed,0.034489
befor,0.004546
began,0.005315
better,0.018007
billion,0.007560
bjrn,0.017497
board,0.023037
boardcertifi,0.018932
book,0.005058
born,0.007519
boston,0.019627
brain,0.025553
branch,0.005660
brasel,0.023223
brazilian,0.012272
breath,0.011444
britain,0.007498
british,0.005943
broad,0.007149
brought,0.006561
calibr,0.012814
cardiac,0.027534
cardiolog,0.015720
cardiovascular,0.013212
care,0.522393
carlgunnar,0.023223
cathet,0.018235
caus,0.004758
ccemtp,0.023223
ccpc,0.023223
ccrn,0.020407
ccu,0.039785
central,0.004997
cerebrospin,0.017076
certain,0.004879
certif,0.031085
certifi,0.023264
challeng,0.006368
chanc,0.008582
chang,0.004019
characterist,0.006144
charg,0.013759
chart,0.010155
chines,0.007663
choos,0.007669
chronic,0.010958
cicu,0.023223
civetta,0.023223
classroom,0.012966
clear,0.006635
close,0.019082
colleg,0.015478
comanag,0.018776
combin,0.005084
common,0.008622
commonli,0.005921
complet,0.004924
compromis,0.010251
concern,0.005005
condit,0.019956
conflict,0.006641
consid,0.004158
consult,0.008233
contract,0.007152
contribut,0.005233
control,0.009474
convert,0.007268
coordin,0.007558
copenhagen,0.025342
coronari,0.015418
cost,0.012538
costeffect,0.013662
countri,0.014077
cours,0.006214
creat,0.004361
credenti,0.013282
crimean,0.014591
critic,0.183576
criticalist,0.023223
crucial,0.008680
cumul,0.011317
cush,0.017236
dahl,0.016116
daili,0.008339
dandi,0.091801
death,0.018735
debat,0.006638
decemb,0.006543
decis,0.006011
declar,0.006887
declin,0.006770
dedic,0.017130
deem,0.009649
defin,0.004893
delirium,0.035576
demograph,0.009573
demonstr,0.006409
denmark,0.010459
depart,0.006251
depend,0.009409
describ,0.004388
design,0.005237
develop,0.010698
devic,0.007969
diagnosi,0.010819
dictat,0.010197
did,0.005293
die,0.006832
dietician,0.020137
differ,0.003780
directli,0.005729
discharg,0.023938
diseas,0.014938
doctor,0.008509
doiccmc,0.023223
doijamasurgeri,0.023223
doitmj,0.023223
dr,0.017693
drain,0.012296
drug,0.016820
dure,0.008209
dysfunct,0.012756
earli,0.004293
educ,0.010938
edward,0.007809
effect,0.004170
ehealth,0.019464
elect,0.006331
emerg,0.016506
empir,0.005990
employ,0.005694
en,0.010549
endocrin,0.013724
endotrach,0.022486
england,0.007449
engstrm,0.021914
enlist,0.013446
enter,0.018828
equip,0.030700
era,0.027131
esicm,0.023223
espnic,0.023223
establish,0.013744
estim,0.006041
et,0.007379
europ,0.005821
european,0.005347
eventu,0.006101
evid,0.011708
exampl,0.003844
exclus,0.014667
expenditur,0.018984
expens,0.007667
extern,0.003356
extrem,0.012316
failur,0.022408
fashion,0.009035
februari,0.006974
fell,0.008291
fellowship,0.024640
fix,0.006813
florenc,0.024714
fluid,0.018097
form,0.010526
formerli,0.008599
fpc,0.019097
franc,0.006571
g,0.006210
gastrointestin,0.014651
gdp,0.008633
gener,0.016493
given,0.004604
glossopharyng,0.020709
goal,0.006314
gomer,0.023223
good,0.005103
govern,0.004365
graduat,0.008602
greenstein,0.021446
ground,0.006679
group,0.004140
grow,0.005886
h,0.006376
ha,0.006121
hallucin,0.015223
halpern,0.016714
harvey,0.011641
head,0.005926
health,0.012799
healthcar,0.010812
heart,0.008425
help,0.010051
hematolog,0.015814
hemofiltr,0.023223
hi,0.012347
high,0.023241
higher,0.010914
histori,0.011470
hn,0.015544
hoc,0.012844
hong,0.010732
hopkin,0.045238
hospit,0.153890
hospitalis,0.018115
hour,0.008066
howev,0.007451
human,0.004422
hunterian,0.021446
hypertensionhypotens,0.023223
ibsen,0.093152
ic,0.014562
icu,0.439775
identifi,0.005549
ill,0.035654
imag,0.007068
import,0.007830
inaccur,0.012444
includ,0.022306
increasingli,0.013534
infect,0.020838
infecti,0.012296
infus,0.013178
injuri,0.010176
inotrop,0.021446
instabl,0.009925
institut,0.019430
intens,0.289715
intensivecar,0.020407
intensiveinvas,0.023223
intensiverecoveri,0.023223
intensivist,0.197226
intermedi,0.009021
intern,0.008045
interpret,0.006124
intraven,0.015418
intub,0.019669
invas,0.008370
involv,0.008900
iron,0.008221
irwin,0.028469
issu,0.004925
j,0.005764
jama,0.017407
januari,0.006551
john,0.020885
jointli,0.011074
june,0.006687
just,0.005352
karen,0.013144
key,0.017524
kirbi,0.016011
known,0.007670
kommunehospitalet,0.023223
kong,0.010825
kvittingen,0.023223
laboratori,0.007788
lack,0.011704
larger,0.006057
lead,0.009158
leapfrog,0.018932
led,0.004953
lesson,0.021544
lethal,0.013229
level,0.004589
lexicon,0.013745
lifethreaten,0.015223
limit,0.004686
line,0.005492
link,0.003337
locat,0.005642
lower,0.005670
lung,0.024002
m,0.005488
machin,0.007409
major,0.004071
make,0.007804
manag,0.054960
mani,0.006937
manual,0.009693
margaret,0.011213
marino,0.015299
massachusett,0.010063
maxmen,0.023223
mccambridg,0.023223
md,0.011359
mean,0.008452
mechan,0.022668
medic,0.063602
medicalsurg,0.019274
medicin,0.263396
member,0.009792
metabol,0.010171
method,0.004752
microbiolog,0.012033
micu,0.021914
missouri,0.027203
mnire,0.021446
mobilis,0.014013
monitor,0.058091
monoton,0.013724
month,0.006779
mortal,0.051282
multipl,0.011804
multiprofession,0.021446
municip,0.009350
muscl,0.010642
na,0.011604
nasogastr,0.023223
nation,0.004252
nccu,0.023223
nearli,0.006683
need,0.018452
neg,0.006654
neonat,0.062348
nephrolog,0.018115
nerv,0.011804
nervou,0.010507
neurosci,0.010580
neurosurgeri,0.016520
new,0.013710
nicu,0.021914
nightingal,0.080850
nois,0.010943
nordisk,0.020137
norway,0.010785
note,0.004424
np,0.013876
number,0.007677
nurs,0.089329
nutrit,0.066138
object,0.005245
observ,0.005064
observationinterventionimpress,0.023223
observationsafdel,0.023223
offer,0.005706
oir,0.023223
olson,0.014562
onehalf,0.014448
onequart,0.014533
oneyear,0.014234
onli,0.003532
open,0.005253
oper,0.004841
oppos,0.006412
organ,0.016742
ottoman,0.010853
outbreak,0.010442
outcom,0.023536
overcom,0.009424
overnight,0.013372
overview,0.007689
oxygen,0.009343
p,0.005857
paediatr,0.017076
pain,0.009719
paralysi,0.014743
paramed,0.016648
parenter,0.054347
pastor,0.012121
patient,0.168247
pave,0.010853
pediatr,0.054815
period,0.004559
peripheri,0.012874
personnel,0.009483
pharmacist,0.030599
physician,0.058080
physiotherapi,0.018630
picu,0.023223
pioneer,0.008016
pittsburgh,0.014183
plan,0.011623
pmid,0.011824
pncctp,0.023223
point,0.004451
poliomyel,0.021914
posit,0.009095
postgradu,0.012859
postop,0.017407
potenti,0.011394
practic,0.009416
practition,0.009235
predict,0.006438
present,0.004399
pressur,0.026495
primari,0.029043
prime,0.007070
principl,0.005282
prize,0.008206
procedur,0.007663
produc,0.009157
profession,0.014260
protect,0.005864
protract,0.013522
provid,0.015616
provis,0.017141
psycholog,0.007500
psychosi,0.015767
publish,0.004884
pulmonari,0.014651
pump,0.022134
pure,0.006958
pursu,0.048016
qualiti,0.006556
quiver,0.019892
rais,0.006490
rang,0.005282
rapidli,0.014837
rate,0.021812
reach,0.005671
receiv,0.010663
recogn,0.005984
recognis,0.008616
recommend,0.008226
record,0.005882
redmann,0.023223
refer,0.013953
referr,0.014651
rel,0.005003
relat,0.003762
relax,0.010668
remain,0.004615
renal,0.046016
replac,0.005687
requir,0.017573
requisit,0.013561
research,0.004527
resid,0.029574
resourc,0.022599
resourceintens,0.019274
respir,0.012602
respiratori,0.063218
respons,0.004840
respriatori,0.023223
return,0.005648
revers,0.007683
reynold,0.013299
rigidli,0.015112
ripp,0.046447
risk,0.006866
rj,0.015041
rogov,0.023223
role,0.004888
room,0.009052
root,0.007049
royal,0.007363
russia,0.008201
s,0.003958
safar,0.019464
saint,0.009331
school,0.039745
schwarz,0.015630
sciatic,0.020709
scottish,0.010713
secondari,0.007947
secret,0.008610
sed,0.015501
sedalia,0.023223
senior,0.009249
sepsi,0.018002
septemb,0.006730
servic,0.016312
set,0.004323
sever,0.004049
shocktrauma,0.023223
shortag,0.010134
shoulder,0.013029
sicu,0.023223
significantli,0.014325
skin,0.009850
sm,0.014109
soap,0.013703
sobrati,0.023223
societi,0.077859
soldier,0.008837
someon,0.009072
sometim,0.010112
sophist,0.008936
space,0.005988
special,0.029145
specialist,0.019274
specialti,0.066182
spectrum,0.009184
spread,0.006865
staf,0.026222
standard,0.005231
state,0.016992
statu,0.006486
stay,0.008501
sticu,0.023223
strong,0.005643
student,0.019832
studi,0.008209
subject,0.004940
subspecialti,0.028841
substitut,0.008630
success,0.005150
suction,0.016116
suffer,0.007088
suggest,0.005437
support,0.018471
surgeonreport,0.023223
surgeri,0.032852
surgic,0.023530
surround,0.007112
surviv,0.006698
symptom,0.011052
syndrom,0.035504
taiwan,0.010929
tap,0.013229
taylor,0.009772
team,0.024333
technic,0.006907
techniqu,0.011937
technolog,0.005599
teleintens,0.046447
telemedicin,0.018932
terrah,0.023223
tetanu,0.017591
theoret,0.006368
therapist,0.026892
thi,0.020335
thoma,0.006947
time,0.010309
tone,0.011785
total,0.010838
trace,0.007447
trachea,0.017497
tracheotomi,0.019097
tract,0.011074
train,0.073775
transfer,0.006646
treatment,0.021434
tube,0.021465
turkey,0.010129
typhoid,0.016520
uk,0.007119
underli,0.007375
understand,0.005272
unfamiliar,0.013484
uniqu,0.006622
unit,0.109689
univers,0.020113
unstabl,0.010361
use,0.009520
usual,0.009347
valu,0.004978
variabl,0.007099
ventil,0.122397
ventricular,0.017407
ventriculographi,0.021914
veterinari,0.040282
voic,0.009257
volum,0.019959
wa,0.054138
wall,0.008223
walter,0.009264
war,0.022069
ward,0.011885
way,0.004152
wellestablish,0.026091
wide,0.005002
work,0.026653
world,0.007875
xray,0.010592
year,0.007839
zealand,0.027705
