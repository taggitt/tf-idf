aberdeen,0.008880
abil,0.016950
abl,0.006224
academ,0.003690
academi,0.009493
accept,0.006186
access,0.010227
accord,0.007391
account,0.002901
accredit,0.074892
achiev,0.006199
acquir,0.004013
act,0.008448
activ,0.014751
actual,0.003153
acupunctur,0.008800
acut,0.038799
addit,0.012463
address,0.014841
adequ,0.005168
adher,0.004984
adjunct,0.008111
administr,0.006890
admiss,0.006572
admit,0.005195
adolesc,0.007168
adult,0.019294
advanc,0.012172
advic,0.005362
advoc,0.004229
affect,0.006439
age,0.006171
agent,0.016804
aid,0.025874
aidestech,0.025924
ailment,0.007834
aim,0.010555
al,0.004226
alberta,0.007770
allianc,0.018432
allow,0.009800
alongsid,0.005124
alreadi,0.003589
alzheim,0.015777
ambul,0.008583
america,0.017265
american,0.019541
amput,0.009329
anatomi,0.005786
annual,0.018924
appli,0.018438
applic,0.002942
appropri,0.011848
approxim,0.006715
apta,0.067436
area,0.022024
armi,0.004238
arriv,0.003966
arthriti,0.016829
asid,0.005619
assess,0.016787
assign,0.004425
assist,0.044085
associ,0.031290
athlet,0.028479
attack,0.003972
attempt,0.002810
august,0.003744
avail,0.006023
averag,0.014721
bachelor,0.051332
balanc,0.011698
base,0.004293
bc,0.004395
becaus,0.002236
becom,0.004756
befor,0.005074
began,0.008899
behavior,0.003485
believ,0.003072
benefit,0.010815
biolog,0.003523
biomechan,0.007526
birth,0.004377
board,0.021430
bodi,0.011925
bone,0.005572
brain,0.009508
branch,0.003159
bridg,0.004908
britain,0.004185
british,0.006634
brunswickcollg,0.012962
bsc,0.008496
bscpt,0.038886
bureau,0.016858
burn,0.005092
burnout,0.019636
bursiti,0.012231
bypass,0.013780
caledonian,0.009531
campt,0.025924
canada,0.017047
canadian,0.030303
cancer,0.005472
capr,0.012962
capt,0.056930
cardiac,0.023052
cardiopulmonari,0.021516
cardiovascular,0.036871
care,0.074761
career,0.004881
catalyz,0.007177
categori,0.003796
caus,0.002655
center,0.012694
central,0.005578
centuri,0.005110
cerebr,0.007757
certif,0.028916
certifi,0.006492
cgep,0.012962
chang,0.002243
charcotmarietooth,0.011390
charter,0.010705
child,0.004779
children,0.008184
chiropract,0.009986
chronic,0.012232
circumst,0.004571
clear,0.011111
client,0.011461
clinic,0.083034
clinician,0.015617
close,0.002662
club,0.005770
cmt,0.010659
cognit,0.004717
cold,0.004743
collabor,0.009142
collect,0.002902
colleg,0.077755
collegesunivers,0.012962
collg,0.009329
columbia,0.005534
commiss,0.007923
common,0.004812
commonli,0.006609
commonwealth,0.005140
commun,0.015613
compet,0.004103
complet,0.043977
complianc,0.006387
compon,0.003427
compress,0.006144
compromis,0.005721
concept,0.002777
concern,0.002794
condit,0.033415
conduct,0.003410
confeder,0.011880
congenit,0.007973
conjunct,0.005561
consid,0.002320
consider,0.003344
consist,0.005327
consult,0.004595
consum,0.003962
contamin,0.006349
content,0.007853
continu,0.009645
contribut,0.002921
control,0.002644
controversi,0.003921
conveni,0.005313
coordin,0.004218
cord,0.007103
coronari,0.017211
correl,0.004639
cost,0.006998
council,0.003628
countri,0.026190
cours,0.017343
coursebas,0.012231
coursework,0.008337
cover,0.003276
credenti,0.029653
credit,0.003851
criteria,0.004984
critic,0.002927
cryotherapi,0.012550
csp,0.009986
ctscan,0.012962
culmin,0.005273
current,0.022930
curricula,0.037216
curriculum,0.024659
cystic,0.009119
daili,0.013964
damag,0.004294
data,0.006352
databas,0.004656
date,0.006652
dawson,0.009055
dc,0.005510
decad,0.003617
deciph,0.007364
decis,0.003355
decisionmak,0.005736
decreas,0.003919
defici,0.006312
defin,0.002731
definit,0.006293
deform,0.006497
degre,0.065185
deliv,0.004691
demand,0.003553
demonstr,0.003577
depart,0.003489
depend,0.005252
depress,0.004944
design,0.005846
detect,0.004370
develop,0.015923
development,0.005602
devic,0.008895
diagnos,0.025785
diagnosi,0.036231
differ,0.004219
difficult,0.003471
diploma,0.045483
direct,0.005285
directli,0.003197
disabl,0.017388
discuss,0.003247
diseas,0.041689
disord,0.035912
disordersdiseas,0.012962
divis,0.003449
doctor,0.042746
document,0.003842
dpt,0.020355
draft,0.005136
dress,0.006611
dri,0.005212
du,0.022500
dure,0.004581
duti,0.004735
dysfunct,0.014239
earli,0.007189
earliest,0.004005
earn,0.018014
edema,0.009531
edinburgh,0.006518
educ,0.082418
edward,0.004358
effect,0.018621
effici,0.003751
eighteenth,0.006124
electr,0.007923
electroacupunctur,0.012231
electrodiagnost,0.011749
electromyogram,0.012962
electrophysiolog,0.025956
electrotherapi,0.011558
electrotherapyphys,0.012962
elig,0.011987
emerg,0.009212
emg,0.010566
emgncv,0.012962
emot,0.009844
employ,0.038139
enabl,0.007599
enact,0.005311
encompass,0.008971
encount,0.004719
encourag,0.007876
end,0.002619
endocrin,0.007660
endur,0.011696
engag,0.003794
enrol,0.026355
enter,0.003502
entri,0.004634
entrylevel,0.019241
environ,0.006352
environment,0.011556
equip,0.004283
equival,0.007719
especi,0.002782
essenti,0.003330
establish,0.010228
ethic,0.004409
europ,0.006498
european,0.002984
evalu,0.012455
event,0.003118
everi,0.002998
evid,0.006534
evolv,0.003933
examin,0.045002
exampl,0.004291
excel,0.005183
execut,0.003629
exemplifi,0.006104
exercis,0.049401
exhaust,0.011205
exist,0.002382
expect,0.003292
expedit,0.005419
experienc,0.004301
expert,0.004732
expertis,0.005844
explan,0.008435
explor,0.003650
extend,0.003160
extens,0.003235
extern,0.001873
extrem,0.003437
facial,0.007119
facil,0.036743
facilit,0.004319
factor,0.018228
faculti,0.005534
father,0.004212
fcampt,0.025924
feder,0.007629
fellow,0.005417
fellowship,0.034382
femal,0.004810
fibrosi,0.017873
field,0.028783
fine,0.005122
fit,0.008724
floor,0.005828
focu,0.003515
focus,0.010006
follow,0.012297
forc,0.002568
form,0.007833
formal,0.003206
foundat,0.003341
fouryear,0.006518
fractur,0.007228
frenkel,0.009872
frequent,0.003601
fulfil,0.005181
fulltim,0.006823
function,0.028855
furthermor,0.004390
galen,0.007345
garment,0.007757
gastrointestin,0.008177
gener,0.001841
genitourinari,0.011103
georgia,0.006124
geriatr,0.052345
given,0.002569
glasgow,0.015052
global,0.010207
goal,0.007048
gordon,0.005975
gout,0.009766
govern,0.002436
graduat,0.028806
great,0.002887
greater,0.003227
greatli,0.004195
gross,0.005009
group,0.004622
grow,0.003285
growth,0.009729
guid,0.003622
gymnast,0.026896
gymnasticon,0.012962
ha,0.017084
half,0.003526
hand,0.003174
heal,0.012561
health,0.067866
healthcar,0.006034
healthcentr,0.012962
healthi,0.005594
healthier,0.008675
heart,0.004702
heat,0.009035
henrik,0.008300
high,0.007783
higher,0.006091
hip,0.008111
hippocr,0.007433
hire,0.005594
histori,0.004268
hold,0.015430
home,0.003658
homebound,0.010758
hospic,0.008936
hospit,0.025262
hospitalbas,0.010111
hotcold,0.011749
hour,0.013507
hourli,0.017035
howev,0.006238
human,0.002468
hydrotherapi,0.010659
identif,0.005046
ifompt,0.012962
ill,0.009950
imag,0.003945
impair,0.026627
import,0.010926
improv,0.018917
incept,0.006668
includ,0.039129
incontin,0.029785
incorpor,0.003518
increas,0.014783
independ,0.013157
individu,0.029070
industri,0.002924
infant,0.006156
infertil,0.008318
influenc,0.002822
initi,0.002783
injur,0.006497
injuri,0.068158
inperson,0.010177
insidi,0.009818
institut,0.002711
institution,0.006771
instrument,0.003945
insur,0.009879
integumentari,0.043914
interact,0.006393
intern,0.006735
interperson,0.007237
intervent,0.027893
involv,0.014903
iontophoresi,0.011970
irrig,0.006476
island,0.003797
issu,0.010997
job,0.030971
joint,0.022557
jurisdict,0.031200
kinesiolog,0.026810
knowledg,0.009667
known,0.008562
la,0.012828
labor,0.016825
laboratori,0.004347
labrador,0.008826
larg,0.002257
late,0.002965
later,0.005095
laval,0.009928
lead,0.002555
leader,0.003745
legal,0.003534
level,0.023052
levelwcpt,0.012962
lgb,0.011970
licens,0.020402
licensur,0.008699
life,0.008260
lifespan,0.007057
lifestyl,0.006100
ligament,0.008853
like,0.009151
limit,0.013079
ling,0.009220
link,0.001862
list,0.009527
listen,0.006049
literaci,0.012570
littl,0.003278
live,0.013847
locat,0.003149
longer,0.007065
loss,0.007492
low,0.003282
lower,0.003165
lung,0.006698
lymphedema,0.011390
machin,0.004135
main,0.002711
mainli,0.003555
maintain,0.002986
major,0.002272
make,0.002178
malign,0.007988
manag,0.030675
mani,0.019359
manifest,0.004927
manipul,0.055032
manitoba,0.018304
manual,0.037871
march,0.003551
margaret,0.006258
mari,0.004948
marievictorin,0.012962
massag,0.034703
master,0.041139
maximum,0.004657
mcmillan,0.009531
mean,0.002358
mechan,0.006326
median,0.013337
medic,0.023666
medicalleg,0.012962
medicin,0.024502
meet,0.003465
member,0.016396
membership,0.004717
mentorship,0.010978
metabol,0.005676
minimum,0.014016
mobil,0.009099
mobilizationmanipul,0.012962
modal,0.013594
model,0.005335
modern,0.002569
montmor,0.011390
montral,0.010247
montreal,0.007684
mostli,0.003654
motor,0.005524
movement,0.023627
mpt,0.042637
mri,0.007423
msc,0.015805
multipl,0.013177
muscl,0.029700
musculoskelet,0.034071
nation,0.007120
necessari,0.003413
neck,0.021079
need,0.005149
needl,0.007547
nerv,0.006588
neurolog,0.064820
neuromuscular,0.028465
neurophysiotherapi,0.025924
neurosci,0.005905
new,0.007652
newfoundland,0.007973
ninth,0.006704
nonmalign,0.012550
nonmusculoskelet,0.012550
nonpati,0.012550
normal,0.010241
north,0.006801
nouveaubrunswick,0.012962
nova,0.006456
nurs,0.037394
obstruct,0.007263
obtain,0.006658
oc,0.009575
occup,0.013388
occur,0.005442
occurr,0.005540
offer,0.022294
offic,0.003447
offici,0.003251
older,0.013483
oncolog,0.015750
onet,0.023940
ongo,0.004686
onli,0.001971
onlin,0.003748
onset,0.006588
ontario,0.014083
opportun,0.008091
oppq,0.025924
optim,0.009085
option,0.004469
order,0.009441
ordr,0.031440
oregon,0.007290
organ,0.009344
organis,0.004055
organiz,0.005611
origin,0.002373
orthopaed,0.103210
orthoped,0.097386
orthos,0.011970
osteoporosi,0.019636
otago,0.010177
outbreak,0.011656
outcom,0.013136
outlin,0.004168
outpati,0.043144
overview,0.004291
pack,0.006249
paid,0.004367
pain,0.048823
palli,0.017549
palsi,0.020355
park,0.005005
parkinson,0.007808
particip,0.003378
particular,0.002680
pass,0.009982
past,0.003397
patholog,0.012177
pathway,0.005752
patient,0.119991
patientcent,0.010978
patienttherapist,0.023940
payment,0.004616
pce,0.020960
pediatr,0.038243
peer,0.005972
pelvic,0.026027
pend,0.007228
peopl,0.007084
perceiv,0.004307
perform,0.015054
perhap,0.003981
period,0.002544
person,0.005605
phd,0.005534
philosophi,0.003646
physiatri,0.010864
physic,0.397974
physician,0.005402
physiolog,0.020179
physiotherapi,0.166374
physiotherapist,0.195325
physiothrapeut,0.012962
physiothrapi,0.038886
plan,0.009730
play,0.003092
polici,0.006339
polio,0.027262
polish,0.005786
popul,0.009142
portland,0.008177
posit,0.005076
possess,0.007697
post,0.012470
postdoctor,0.008318
postgradu,0.007177
postop,0.009716
postpartum,0.020355
postur,0.007494
practic,0.052554
practis,0.019384
practition,0.030927
pre,0.007136
precursor,0.005350
predictor,0.007861
prenat,0.009025
prerecord,0.009818
prerequisit,0.014207
prescript,0.019415
present,0.002455
preval,0.005407
prevent,0.010533
primari,0.006484
primarili,0.003357
princ,0.005300
privaci,0.007263
privat,0.010564
privateown,0.011970
problem,0.005253
procedur,0.008554
processingintegr,0.012962
procoveri,0.012962
profess,0.030794
profession,0.039797
professionnel,0.034170
prognosi,0.008414
program,0.086506
progress,0.003347
project,0.006078
promot,0.014014
prospect,0.005219
prosthes,0.009818
prove,0.003790
provid,0.019612
provinc,0.022701
provinci,0.005940
pt,0.124403
pta,0.050556
public,0.002473
publish,0.002726
pulminari,0.012962
pulmonari,0.065419
pursu,0.008933
qualif,0.012482
qualifi,0.010329
qualiti,0.007318
qubcoi,0.011103
qubec,0.027167
quebec,0.014422
queen,0.005154
radiat,0.004918
rang,0.005896
rapid,0.008512
rapidli,0.004140
rate,0.012174
ray,0.005320
rcig,0.012962
recent,0.002697
recogn,0.023379
recommend,0.009183
reconstruct,0.009762
recoveri,0.005185
recreat,0.006016
recruit,0.005742
reed,0.014806
reeduc,0.010047
refer,0.004672
regardless,0.004708
region,0.002789
regist,0.009660
registr,0.006065
regul,0.007375
regulatori,0.016172
rehabilit,0.127610
relat,0.010500
relationship,0.005946
relax,0.005954
remedi,0.012321
remot,0.005273
remov,0.003596
replac,0.003174
report,0.020956
reproduct,0.005291
requir,0.044137
research,0.020215
resid,0.028886
respect,0.023697
respiratori,0.007057
respond,0.004201
respons,0.005403
restor,0.013215
result,0.006509
retrain,0.009716
return,0.003152
review,0.017450
robert,0.003424
role,0.005457
room,0.005052
routin,0.005591
royal,0.004109
s,0.006627
safe,0.005092
salari,0.005761
saskatchewan,0.009186
satisfact,0.012902
satisfi,0.004620
scale,0.003525
school,0.025352
scienc,0.031437
sclerosi,0.017653
scoliosi,0.011970
scotia,0.008079
scotland,0.005854
secret,0.004805
section,0.003859
seen,0.003056
semiprofession,0.011239
senior,0.005162
sens,0.003205
sensori,0.006038
separ,0.002845
serv,0.003100
servic,0.015174
sessionsth,0.012962
set,0.019303
share,0.006070
sherbrook,0.012962
shown,0.007512
significantli,0.007995
similar,0.012943
similarli,0.004106
simpl,0.003444
simpli,0.003537
sinc,0.006503
sit,0.017192
situat,0.003337
sjukgymnast,0.012962
skelet,0.007591
skill,0.050113
skin,0.005498
societi,0.005432
softwar,0.004592
soldier,0.004932
someon,0.005063
sonographi,0.011103
soon,0.007950
sound,0.004564
span,0.005007
special,0.027112
specialist,0.043031
specialti,0.080035
specif,0.012968
spend,0.004499
spinal,0.022675
spine,0.029345
splint,0.010659
sport,0.066930
sprain,0.022780
spring,0.004889
standard,0.002919
start,0.005488
state,0.049316
statist,0.017782
steadili,0.005718
stimul,0.004851
strain,0.005388
straincounterstrain,0.012962
strainsinjuri,0.012962
stream,0.005195
strength,0.008793
stroke,0.013849
strong,0.003149
student,0.029518
studi,0.022909
subspecialti,0.008048
success,0.005749
suffer,0.003956
suggest,0.006069
suit,0.005039
supervis,0.010366
support,0.005154
surgeon,0.006759
surgeri,0.030560
surgic,0.013133
sweden,0.005415
swedish,0.011824
syndrom,0.006605
systemat,0.016566
tailor,0.007168
task,0.008043
team,0.009054
tech,0.014729
technic,0.007710
technician,0.027727
techniqu,0.013325
technolog,0.003125
telehealth,0.070498
telerehabilit,0.012962
temporari,0.005112
tendenc,0.004867
tendinopathi,0.012231
tendon,0.008675
term,0.002048
territori,0.007588
test,0.007087
th,0.002485
theme,0.004801
therapeut,0.019191
therapi,0.469325
therapist,0.450298
therapistpati,0.012962
therapyrel,0.012962
therefor,0.002946
thi,0.016214
threaten,0.004929
threeyear,0.007111
thu,0.002534
time,0.003836
tissu,0.010476
titl,0.007782
tool,0.003471
topic,0.003439
tout,0.008318
traction,0.016492
train,0.048664
trainer,0.008282
transit,0.007548
trauma,0.007160
travel,0.003827
treat,0.026571
treatment,0.087731
trend,0.004140
triag,0.010398
turnov,0.006967
twoyear,0.014098
type,0.002606
typic,0.005816
uk,0.003973
ultrasound,0.007888
undergon,0.006765
undergradu,0.005773
understand,0.002942
uniqu,0.003696
unit,0.041543
univers,0.035923
universit,0.024792
universitylevel,0.010177
updat,0.010502
upgrad,0.018671
urinari,0.008264
use,0.021255
usual,0.005217
util,0.003921
vacanc,0.008111
vaccin,0.007194
valid,0.004139
valu,0.005556
valuabl,0.005088
vari,0.022370
variat,0.004063
varieti,0.013019
variou,0.004935
veloc,0.005700
vision,0.004695
wa,0.014219
wage,0.019054
walter,0.005170
war,0.006158
warm,0.005540
washington,0.004772
wave,0.004371
wcpt,0.012962
week,0.004556
welfar,0.004993
wellb,0.005799
wellnessori,0.012962
western,0.003260
wherea,0.003761
wide,0.008376
widerang,0.007569
women,0.033005
word,0.003009
work,0.025502
workplac,0.006803
world,0.010989
worldwid,0.004397
wound,0.029735
xray,0.005912
year,0.043755
yield,0.004234
yukon,0.010247
zealand,0.005154
