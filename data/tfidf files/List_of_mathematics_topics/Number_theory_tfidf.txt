aa,0.007682
ab,0.019392
abababldot,0.012303
abc,0.022193
abelian,0.017345
abmod,0.024607
abroad,0.005603
absent,0.006288
absqrt,0.011496
abstract,0.009080
academ,0.003864
access,0.003569
accord,0.002580
account,0.003038
achiev,0.003245
acquir,0.004202
activ,0.005149
actual,0.006604
addit,0.010441
address,0.003885
adject,0.007052
adrienmari,0.010457
africa,0.004190
age,0.012924
aim,0.003684
alexandria,0.007033
alfakhri,0.011769
algebra,0.188386
algorithm,0.082761
alhaytham,0.008176
alkaraj,0.024607
alkhwrizm,0.010026
allow,0.002565
almamun,0.010123
alreadi,0.007517
altogeth,0.006396
amateur,0.007045
ambigu,0.006062
american,0.002923
amic,0.008811
amountsin,0.012303
analog,0.004610
analysi,0.040433
analyt,0.098783
ancient,0.003738
ani,0.022739
annot,0.007065
anoth,0.004963
answer,0.013782
antiqu,0.005339
anybodi,0.008176
anyth,0.005099
apequiv,0.012103
appear,0.020919
appendix,0.007973
appli,0.002758
applic,0.030813
approach,0.008908
appropri,0.004135
approxim,0.035162
aq,0.010226
arab,0.009531
archimed,0.030537
area,0.015375
argu,0.003432
arguabl,0.031441
argument,0.008669
aris,0.004209
aristotl,0.005318
arithmet,0.201596
arithmetica,0.063129
articl,0.009199
asian,0.004915
asid,0.005884
ask,0.029856
assum,0.003574
assur,0.005878
astrolog,0.007464
astronom,0.005809
astronomi,0.011585
asymptot,0.007560
attempt,0.005886
attent,0.004207
attract,0.004151
attributionsharealik,0.010457
author,0.008715
automorph,0.009619
averag,0.007707
award,0.004548
axbyc,0.011927
axbycz,0.012303
axiom,0.012868
b,0.058098
babylon,0.007773
babylonian,0.049099
bachet,0.024207
bare,0.012710
base,0.008992
basic,0.028168
bce,0.005709
becam,0.005687
becaus,0.004683
becom,0.002490
befor,0.007971
begin,0.006067
behavior,0.003649
belong,0.017583
bernhard,0.008110
best,0.003428
better,0.007016
bhaskara,0.021043
bhskara,0.040697
bjagaita,0.012303
bmod,0.021615
book,0.017740
borevich,0.012103
boston,0.005735
brahmagupta,0.047920
brahman,0.008218
branch,0.003308
brhmasphuasiddhnta,0.011496
briefli,0.005770
broad,0.004178
broken,0.005187
brouncker,0.011065
brute,0.008545
build,0.003292
busili,0.011626
c,0.035729
ca,0.022890
calcul,0.015880
caliph,0.007045
came,0.003406
canon,0.005657
care,0.007828
carl,0.005202
carri,0.003359
case,0.023447
catalyst,0.006804
cattl,0.006103
cdot,0.007244
ce,0.028546
central,0.005841
centuri,0.040134
certain,0.022815
chakravala,0.024207
challeng,0.003722
chang,0.002349
chapter,0.004856
characterist,0.003591
chines,0.013436
chinesewherea,0.012303
choic,0.004094
chosen,0.004761
cicero,0.007762
circl,0.010041
circlewhich,0.012303
cite,0.004358
citizendium,0.010457
claim,0.015815
clariti,0.007481
class,0.006912
classic,0.017213
classif,0.004597
classifi,0.004215
clay,0.006653
clearli,0.009370
close,0.011152
coalesc,0.007643
coeffici,0.006013
cole,0.007251
colebrook,0.011162
collect,0.006079
column,0.005920
combinator,0.031989
come,0.008941
common,0.007559
commun,0.002724
compar,0.003082
complementari,0.006079
complet,0.005756
complex,0.039643
complexvalu,0.009936
composit,0.004320
comprehens,0.004590
comput,0.055251
concept,0.005816
concern,0.008777
concret,0.005709
confus,0.004690
congruenc,0.017976
congruent,0.018220
conjectur,0.032908
connect,0.010293
consid,0.012152
consist,0.008368
constantinopl,0.007208
construct,0.022590
contain,0.011959
contemporari,0.020382
continu,0.005050
contrari,0.005449
convent,0.004035
coordin,0.022089
copi,0.005024
coprim,0.010588
correct,0.008393
correspond,0.007566
count,0.004753
cours,0.010896
cover,0.006862
cramr,0.010888
creativ,0.005385
credit,0.012099
crisi,0.004300
crucial,0.015219
cryptographi,0.007752
cubic,0.007193
current,0.002668
curv,0.076911
curvethat,0.012303
cyclic,0.006472
cyclotomi,0.011927
d,0.016869
date,0.010449
dawn,0.006595
day,0.003144
deal,0.003357
dearth,0.009584
decompos,0.006755
dedekind,0.009215
defin,0.031461
definit,0.003295
denot,0.005085
densiti,0.005029
depend,0.005499
deriv,0.003168
descent,0.035873
describ,0.007695
descript,0.003809
determin,0.002894
develop,0.025011
devot,0.014778
diagon,0.007794
dialoguesnam,0.012303
dickson,0.009242
did,0.034030
didiciss,0.011927
differ,0.011046
difficult,0.010904
difficulti,0.009002
dimens,0.004795
dimension,0.012893
diophantin,0.202738
diophantu,0.137647
diophantuss,0.061519
direct,0.002767
dirichlet,0.027035
discipl,0.006964
disciplin,0.003840
discov,0.003670
discoveri,0.007806
discret,0.004915
discuss,0.003400
displaystyl,0.372372
disquisition,0.033486
distinct,0.003209
distinguish,0.007400
distribut,0.017193
divid,0.006609
divis,0.025284
divisor,0.061683
divulg,0.010337
doe,0.019739
domain,0.004070
donald,0.005875
dot,0.006734
doughnut,0.011065
dover,0.006964
draw,0.004272
drawn,0.004918
dure,0.007197
e,0.006754
earli,0.030114
earlier,0.007415
earliest,0.004193
ed,0.017562
edit,0.007854
educ,0.003196
egypt,0.010597
egyptian,0.011310
eighteenth,0.006413
eisenstein,0.010337
element,0.038293
elementari,0.056603
eleventh,0.007794
ellipt,0.014945
elsewher,0.005172
emend,0.010588
emphasis,0.006761
encod,0.006110
encrypt,0.017308
encyclopedia,0.004321
endeavour,0.007244
english,0.006989
enlighten,0.005478
entir,0.003254
enumer,0.007251
epigram,0.020793
equal,0.003416
equat,0.150230
equationsin,0.012303
equival,0.004041
eratosthen,0.008854
erd,0.008791
ergod,0.008654
erron,0.007186
especi,0.002913
essenc,0.005667
establish,0.002677
estim,0.003530
euclid,0.061150
euclidean,0.021042
euler,0.098199
europ,0.013609
eusebiu,0.009358
event,0.006530
everi,0.015698
evid,0.003421
examin,0.003927
exampl,0.038201
exercis,0.008621
exist,0.007485
expel,0.006049
explain,0.003377
explicitli,0.005107
exposit,0.006876
express,0.003250
extens,0.023717
extensionsthat,0.012303
extent,0.004128
extern,0.001961
f,0.040655
fact,0.019114
factor,0.009544
factoris,0.022992
fairli,0.005683
falt,0.011265
far,0.003402
fashion,0.005280
fast,0.010272
felicit,0.010396
fermat,0.216703
ferunt,0.011927
fewer,0.005320
fibonacci,0.008965
field,0.073982
figur,0.007431
final,0.003245
finit,0.020756
fix,0.003982
float,0.005941
focu,0.003680
follow,0.021462
foray,0.009483
forc,0.005378
form,0.036914
formal,0.006714
foundat,0.006997
fourier,0.007001
foursquar,0.021462
fraction,0.005251
fragment,0.015831
french,0.003610
frenicl,0.012303
friedrich,0.005487
friend,0.005329
fruit,0.005316
fuller,0.007858
fulli,0.008090
function,0.027469
fx,0.007762
fxi,0.043555
fxxx,0.024607
fxyz,0.011769
fxyzw,0.012303
g,0.018148
gallk,0.012103
galoi,0.017623
galoiss,0.011626
gather,0.004677
gauss,0.048740
gave,0.011712
gcd,0.011265
gcdaq,0.012303
gener,0.023135
generalis,0.030969
genu,0.033885
geometr,0.016552
geometri,0.087552
germain,0.008942
gfdl,0.010337
ggg,0.011769
given,0.016145
gloss,0.009299
god,0.004695
goe,0.009656
goldbach,0.020053
golden,0.005436
good,0.005965
gradual,0.004313
graduat,0.005027
graphic,0.005770
great,0.003023
greater,0.006758
greatest,0.009443
greec,0.015214
greek,0.034194
ground,0.007808
group,0.009680
groupar,0.012303
growth,0.006792
gustav,0.006865
h,0.011181
ha,0.023257
half,0.007386
hand,0.013297
happen,0.016961
hardi,0.015717
hardylittlewood,0.011162
hazewinkel,0.008110
head,0.003463
heathbrown,0.012103
height,0.005349
hellenist,0.020461
help,0.002937
henri,0.004324
herbert,0.005809
heurist,0.006865
hi,0.050519
higher,0.009568
highspe,0.007961
hilbert,0.007200
hing,0.008021
hippasu,0.011376
histor,0.003013
histori,0.006704
hole,0.006106
howev,0.010887
hugh,0.006295
hundr,0.004076
huygen,0.008190
hypothesi,0.004673
ibn,0.011681
idea,0.003011
ideal,0.034925
ident,0.007683
identifi,0.003243
igor,0.008396
ii,0.007022
impetu,0.006923
impli,0.008342
implicit,0.012568
import,0.009153
improv,0.003301
includ,0.009312
incommensur,0.009161
incorpor,0.003684
increasingli,0.003955
inde,0.004562
indefinit,0.006303
independ,0.008267
indetermin,0.008046
india,0.008215
indian,0.022206
indigen,0.010699
induct,0.005941
infanc,0.008071
infinit,0.026267
infinitud,0.010974
influenc,0.008865
inform,0.002667
initi,0.014575
insist,0.005474
instanc,0.018168
instead,0.003142
instruct,0.010102
integ,0.205892
integr,0.006475
intersect,0.012092
introduc,0.012589
introduct,0.014227
invari,0.005932
invers,0.011330
involv,0.002600
irrat,0.027645
irreduc,0.007826
isbn,0.019030
islam,0.004466
issu,0.002879
ivan,0.007215
iwasawa,0.011376
ix,0.031348
j,0.003369
jacobi,0.008444
jayadeva,0.033796
jeanpierr,0.008304
john,0.003051
josephloui,0.019961
just,0.006256
justic,0.004534
k,0.016270
keen,0.007389
kenneth,0.006020
key,0.003414
kind,0.010721
knew,0.012638
know,0.024390
known,0.029142
knuth,0.009769
kroneck,0.009060
kuaka,0.036911
kummer,0.010123
l,0.019744
lack,0.010261
lagrang,0.023849
langland,0.009619
languag,0.006743
larg,0.023640
larger,0.010622
largescal,0.005226
larsa,0.011626
late,0.015528
later,0.021341
latin,0.004216
law,0.005404
lawsi,0.012303
layout,0.007515
layperson,0.009270
lead,0.010705
learn,0.018840
led,0.005790
leftfrac,0.007357
leftxfrac,0.022752
legendr,0.017839
lejeun,0.010226
length,0.004450
leonard,0.006911
leonhard,0.008123
let,0.005025
letter,0.008855
lfunction,0.020915
librarian,0.008750
licens,0.010682
like,0.009583
likelihood,0.006220
limit,0.005478
line,0.003210
link,0.009751
list,0.002494
littl,0.013731
live,0.008700
logic,0.004341
long,0.002912
longer,0.007398
look,0.003703
lost,0.003897
luqa,0.011376
m,0.032078
ma,0.005665
machin,0.004330
magi,0.009892
magic,0.006253
main,0.017037
make,0.004561
manag,0.003212
mani,0.036490
manipul,0.004802
manuscript,0.012172
margin,0.009830
materi,0.015715
mathemat,0.095200
mathematician,0.010980
mathematicsanci,0.012303
matter,0.016869
matur,0.005219
mean,0.024701
meant,0.004600
measur,0.005875
men,0.004159
mention,0.008684
mesopotamia,0.006820
messag,0.005573
method,0.052778
michiel,0.007949
middl,0.007395
midtwentieth,0.008875
million,0.010568
mind,0.004229
mineola,0.009936
minim,0.004733
misnam,0.041831
mistak,0.006173
mod,0.034767
model,0.005587
modern,0.034973
modular,0.015014
moduli,0.010026
modulo,0.018716
montgomeri,0.008854
month,0.003962
moreov,0.010290
motiv,0.004381
mr,0.006029
multipl,0.003449
music,0.004929
mutual,0.004731
mxni,0.012303
mystic,0.013229
n,0.064446
nation,0.004970
natur,0.007183
nave,0.008246
ndimension,0.008511
nearli,0.003906
necessari,0.003574
necessarili,0.004390
need,0.008088
neoplaton,0.007804
nequiv,0.023539
new,0.004006
ngeq,0.010657
nineteenth,0.011220
ninth,0.007020
niven,0.010074
nonabelian,0.009850
noncommut,0.009110
nonelementari,0.011769
nonmathematician,0.011927
nonprim,0.010807
nonprobabilist,0.011626
nonrat,0.009584
nonrigor,0.022752
nontrivi,0.015525
nonzero,0.007365
norm,0.005202
north,0.003561
notabl,0.006883
notat,0.006026
note,0.007757
noth,0.004746
notic,0.005090
notion,0.004243
nowaday,0.013575
nowher,0.007722
number,0.316371
numbertheoret,0.029677
numbertheorist,0.012303
numer,0.013291
ny,0.006490
ob,0.009584
object,0.027594
observ,0.005920
obtain,0.003486
occupi,0.004496
occurr,0.005801
odd,0.018982
old,0.014844
older,0.023533
omnia,0.010888
onli,0.024775
onward,0.005685
open,0.009211
oppos,0.007496
order,0.002471
origin,0.007456
otherwis,0.008532
outsid,0.003421
overlap,0.004984
oxford,0.004214
p,0.020542
padic,0.009483
pair,0.004779
pappu,0.010337
parametr,0.007837
parametris,0.011626
partial,0.004125
particular,0.033688
particularli,0.003145
partit,0.006212
partli,0.004822
pass,0.003484
pe,0.008021
peano,0.008770
pearson,0.007065
pell,0.057716
pentagon,0.026075
pequiv,0.012103
perfect,0.010022
period,0.013324
persia,0.006678
peter,0.004089
philosoph,0.003936
philosophi,0.003818
pierr,0.005773
pioneer,0.009370
place,0.007609
plan,0.003396
plane,0.005402
plato,0.035449
platonem,0.011927
plenti,0.007052
plimpton,0.010731
point,0.062439
polygon,0.008511
polynomi,0.027370
popular,0.009626
pose,0.005463
possibl,0.007869
poverti,0.005290
power,0.005263
practic,0.002751
predat,0.005709
prefer,0.003938
prefigur,0.009419
present,0.002571
press,0.006053
presum,0.005741
pretheodoru,0.012303
priest,0.006089
primal,0.017091
primarili,0.003515
prime,0.107437
principl,0.003087
privat,0.003687
prize,0.014389
probabilist,0.040132
probabl,0.022142
problem,0.049510
procedur,0.017915
process,0.002431
procur,0.007104
program,0.003235
programm,0.004943
progress,0.017526
project,0.003182
promin,0.004033
proof,0.125854
prop,0.017345
properti,0.009341
proport,0.004252
propos,0.003173
proposit,0.005911
protocol,0.011630
prove,0.027784
proven,0.032593
provid,0.004563
provok,0.006581
public,0.005180
publish,0.005709
pulveris,0.024607
pure,0.012200
pxi,0.010974
pythagora,0.031093
pythagorea,0.011927
pythagorean,0.085055
q,0.028800
qc,0.009730
quadrat,0.119427
quantiti,0.004189
queen,0.005397
question,0.041986
quickli,0.004186
quit,0.013039
quotat,0.006831
qusta,0.011626
r,0.024091
radiu,0.006804
random,0.009812
rang,0.003087
rapidli,0.013007
rare,0.004232
rash,0.009299
ration,0.141836
rationalsth,0.012303
rd,0.004605
reach,0.003314
read,0.005665
reader,0.011124
real,0.027083
rebirth,0.007578
recent,0.002824
reciproc,0.036763
recognis,0.005035
recreat,0.006299
reduc,0.006180
refer,0.001631
regain,0.006265
rel,0.008773
relat,0.010996
remain,0.005395
remaind,0.012105
renaiss,0.005393
renew,0.004769
reorder,0.009358
repeat,0.009416
rephras,0.009730
report,0.003135
reprint,0.012545
reput,0.005675
requir,0.002567
research,0.010584
resembl,0.005145
resort,0.006052
respect,0.003101
restat,0.007826
restrict,0.003780
result,0.006816
retract,0.008349
retriev,0.014537
rev,0.007288
reveal,0.004290
reward,0.005642
riemann,0.040170
rigor,0.010984
ring,0.005524
ripe,0.009619
rise,0.003282
role,0.002857
root,0.012361
rosen,0.008477
roshdi,0.011162
rough,0.006468
routin,0.005855
rsa,0.009692
ryabhaa,0.058847
s,0.018507
said,0.020108
sanskrit,0.013511
say,0.024737
school,0.006637
scienc,0.002743
scientist,0.003729
sec,0.007938
secant,0.018966
second,0.008031
secondaryschool,0.011927
sect,0.006831
section,0.012124
seen,0.012801
selberg,0.011265
selfconsci,0.008163
sens,0.010068
sensibl,0.007215
sent,0.004521
sequenc,0.004528
seri,0.009748
serr,0.009242
set,0.027794
sever,0.002366
shafarevich,0.011496
shift,0.003952
shortli,0.005076
shown,0.011800
siev,0.027407
signific,0.002897
silverman,0.009730
simpl,0.003606
simplest,0.005790
simultan,0.004757
sinc,0.009079
sindhind,0.012303
singl,0.003037
size,0.007070
smaller,0.003859
socal,0.004580
societi,0.002844
solut,0.135934
solv,0.033422
sometim,0.017730
somewhat,0.004812
son,0.004346
soon,0.004162
sophi,0.009358
sourc,0.014277
space,0.010499
speak,0.017411
special,0.014195
specif,0.002716
spite,0.013079
split,0.009331
spoke,0.006265
springer,0.010989
spur,0.006375
sqrt,0.073267
squar,0.043286
start,0.022988
state,0.017876
statement,0.016669
step,0.011547
strand,0.006472
strike,0.005049
structur,0.002652
studi,0.079168
suanj,0.011265
subdisciplin,0.006187
subdivis,0.013337
subfield,0.023375
subfieldsin,0.012303
subject,0.023102
subset,0.011387
subsum,0.007587
subtract,0.006983
success,0.006020
suffer,0.004143
suggest,0.006355
sum,0.031697
sunzi,0.022130
supersed,0.006558
superstit,0.007858
suppos,0.004988
surfac,0.020552
surviv,0.015660
symbol,0.008242
systemat,0.004337
systematis,0.009655
tabl,0.017475
tablet,0.007236
tail,0.006572
taken,0.006647
takiltum,0.012303
tangent,0.031524
task,0.004211
tauberian,0.011626
taught,0.005061
teach,0.004446
technic,0.004037
techniqu,0.003488
term,0.034320
terminolog,0.005345
termsto,0.012303
test,0.011133
text,0.007831
textbook,0.005116
textual,0.007131
th,0.028632
thale,0.007569
thank,0.012485
thcenturi,0.005234
theaetetu,0.010521
theaetetuss,0.012303
theaetetusthat,0.012303
themselv,0.003367
theodoru,0.010588
theodoruss,0.012303
theorem,0.091598
theoret,0.003722
theori,0.234154
theoris,0.008460
theorist,0.005290
theoryaccompani,0.012303
theoryor,0.012303
thi,0.052636
thing,0.010836
think,0.003936
thirteen,0.006809
thth,0.007578
thu,0.015921
thusconsist,0.012303
time,0.016068
togeth,0.006260
took,0.003546
tool,0.018174
toolbox,0.009299
topic,0.003601
touch,0.005991
tradit,0.011434
transcendent,0.021754
translat,0.023448
travel,0.004008
treat,0.011925
treatis,0.010953
treatment,0.008351
tri,0.003573
triangular,0.015740
trigonometri,0.008304
tripl,0.018761
truli,0.011376
ture,0.006865
turn,0.009414
twelfth,0.007938
twentieth,0.005355
twin,0.006678
unambigu,0.007672
understood,0.008520
uniqu,0.003870
unit,0.002289
uniti,0.004989
univers,0.002351
unknown,0.004699
unlik,0.007560
unport,0.011065
unsulli,0.011927
unusu,0.011007
usag,0.004761
use,0.042662
usual,0.002731
valid,0.004334
valu,0.002909
valuat,0.012561
variabl,0.029048
variablesturn,0.012303
varieti,0.006816
varist,0.010731
vedic,0.015525
veri,0.020739
view,0.005819
vii,0.020716
vinogradov,0.033796
virtual,0.004489
visit,0.004560
visualis,0.008528
viz,0.008412
w,0.003808
wa,0.061422
walli,0.008123
ware,0.007926
way,0.009708
web,0.004921
wed,0.007672
wellknown,0.005329
west,0.003897
western,0.006829
whatev,0.011176
width,0.007634
wienerikehara,0.012303
wikiquot,0.008598
wiley,0.005554
wilson,0.005670
wisdom,0.012021
wise,0.007091
wish,0.005019
word,0.006302
work,0.066763
workedout,0.011769
wright,0.013469
write,0.010648
written,0.003594
wrote,0.007735
x,0.196948
xac,0.011769
xaqfrac,0.012303
xigir,0.012303
xlv,0.011769
xni,0.022992
xnynzn,0.012303
xrightright,0.011496
xrightrightleftfrac,0.012303
xxx,0.009035
xy,0.024065
xyland,0.011496
xyz,0.026826
y,0.101518
ybc,0.011065
year,0.004581
z,0.038768
zeta,0.044487
zuckerman,0.010074
