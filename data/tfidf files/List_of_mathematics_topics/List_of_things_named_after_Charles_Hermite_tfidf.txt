adjoint,0.039009
algebra,0.021454
aris,0.016299
asteroid,0.028869
astronom,0.022496
binari,0.024057
bundl,0.053568
certain,0.022086
chang,0.009096
charl,0.015452
classic,0.013330
complex,0.059040
complexvalu,0.038474
concern,0.011329
condit,0.011291
conjug,0.083081
connect,0.026572
constant,0.032086
continu,0.009777
convex,0.028460
cotang,0.039811
covari,0.029635
crater,0.030346
cubic,0.027855
data,0.012878
defin,0.011074
dens,0.022971
discret,0.019033
discrimin,0.044819
distribut,0.039945
einsteinhermitian,0.048537
entri,0.037586
equal,0.026462
everi,0.012157
express,0.012588
extens,0.013119
famili,0.026906
fiber,0.025236
field,0.021220
finit,0.040186
form,0.047646
fraction,0.020335
free,0.023733
french,0.013981
function,0.042546
gausshermit,0.047643
gaussian,0.028805
generalis,0.029980
geometri,0.021188
given,0.010419
hat,0.029068
hermit,0.630003
hermitehadamard,0.050889
hermitelindemann,0.052558
hermiteminkowski,0.052558
hermitian,0.612153
ident,0.059503
inequ,0.021858
integ,0.049828
integr,0.012537
interpol,0.048549
khler,0.036473
lattic,0.025899
law,0.010463
lowoscil,0.052558
magnitud,0.021427
mainbelt,0.045573
mani,0.015699
manifold,0.081627
manifoldstructur,0.052558
mathematician,0.021258
matric,0.028677
matrix,0.065402
method,0.021512
metric,0.023323
modul,0.023450
multipl,0.013357
normal,0.027686
number,0.052129
numer,0.012866
object,0.011872
onli,0.015988
oper,0.054781
origin,0.009624
orthogon,0.058832
parametr,0.030346
point,0.010074
polynomi,0.158975
positivedefinit,0.040999
probabl,0.028580
problem,0.021301
quadratur,0.076284
quadric,0.042495
rank,0.016814
real,0.026218
reciproc,0.047452
relat,0.017031
respect,0.012011
riemannian,0.068242
ring,0.042783
rowreduc,0.052558
satisfi,0.018735
selfadjoint,0.037983
sequenc,0.035068
sesquilinear,0.048537
sign,0.014472
small,0.010756
smoothli,0.032157
sometim,0.034328
space,0.027104
specif,0.021033
spline,0.080515
squar,0.018623
stabli,0.037678
state,0.007691
symmetr,0.100122
theorem,0.078818
thing,0.013986
thirddegre,0.045573
transcendent,0.028078
transpos,0.097908
trigonometr,0.032273
type,0.010568
uniqu,0.029976
unsolv,0.027801
vari,0.012958
variabl,0.016068
varieti,0.013198
vector,0.042516
wavelet,0.155303
way,0.009398
