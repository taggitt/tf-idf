abelian,0.013942
absolut,0.007035
abstract,0.007299
act,0.004740
actual,0.005308
adamson,0.017944
addit,0.004196
algebra,0.311763
alpha,0.009601
american,0.004699
analogu,0.011337
andrzej,0.015522
ani,0.003655
applic,0.004953
approach,0.004773
arithmet,0.010127
artinschrei,0.042256
aspect,0.005451
automorph,0.015464
b,0.010988
bartlett,0.014449
basi,0.010333
basic,0.005031
biject,0.014904
biquaternion,0.019174
branch,0.005318
braunschweig,0.018289
brown,0.008486
c,0.004786
cambridg,0.019902
case,0.004188
categori,0.006391
cextens,0.152748
characterist,0.063512
class,0.011112
classifi,0.006776
close,0.044824
closur,0.054093
cmfield,0.021821
coeffici,0.019335
common,0.004050
commut,0.011129
compat,0.009611
complet,0.027763
complex,0.009804
compositum,0.043642
conjug,0.011497
consid,0.003907
consist,0.004484
contain,0.009612
cover,0.005516
cyclotom,0.018110
d,0.005423
defin,0.004598
definit,0.015893
degre,0.054868
degreetwo,0.021821
denot,0.024525
der,0.008768
differ,0.003551
differenti,0.013568
dimens,0.015417
diophantin,0.014814
disjoint,0.014004
displaystyl,0.023024
distinct,0.005160
distinguish,0.005948
divis,0.011613
domain,0.006543
dont,0.009362
e,0.190023
ed,0.016940
edit,0.006313
ef,0.183580
efe,0.018921
ekf,0.019780
element,0.123124
embed,0.009380
encyclopedia,0.006947
equal,0.005493
equat,0.013417
equival,0.012994
ergebniss,0.020151
everi,0.060570
exampl,0.010837
exist,0.008022
extens,0.397637
f,0.409978
factor,0.005114
factoris,0.036964
famili,0.005585
fffcdot,0.021821
ffp,0.021821
field,0.449314
fifi,0.019780
finit,0.108450
fix,0.006402
fke,0.021821
folg,0.021128
form,0.016484
formal,0.010794
foundat,0.005624
fp,0.013823
fri,0.012915
friedr,0.021821
frobeniu,0.016356
fs,0.028468
function,0.013248
fundament,0.005523
fx,0.112320
fxi,0.017505
fy,0.026469
g,0.011670
galoi,0.226660
gener,0.037193
geometri,0.017594
given,0.017304
global,0.011456
glossari,0.010513
graduat,0.024247
grenzgebiet,0.019780
grothendieck,0.016196
group,0.054471
ha,0.023009
hensel,0.017944
henselian,0.021821
hilbert,0.011576
hilbertian,0.020151
homomorph,0.074295
howev,0.003500
iain,0.014375
idea,0.004841
ident,0.006176
ihrer,0.019174
imaginari,0.022593
imperfect,0.042475
impli,0.006705
includ,0.005988
independ,0.013290
infinit,0.008445
inject,0.010649
insepar,0.012856
integ,0.020687
integr,0.005205
introduc,0.005059
introduct,0.011436
invers,0.018215
irreduc,0.012581
isbn,0.040791
isomorph,0.024993
jarden,0.021821
jeanpierr,0.026702
jfield,0.021821
join,0.006415
jone,0.009576
k,0.052314
kroneckerian,0.021821
kummer,0.032549
l,0.012696
lam,0.015464
lang,0.023838
lectur,0.007840
lemma,0.012513
let,0.016159
lie,0.014130
line,0.010322
linear,0.007672
linearli,0.012837
link,0.003135
local,0.004883
m,0.005157
mani,0.003259
martin,0.007632
mathemat,0.045347
mathematik,0.015407
michael,0.006635
michel,0.009884
minim,0.015220
mordellweil,0.020151
mosh,0.015298
motiv,0.007044
mr,0.019387
multipl,0.016637
n,0.045327
nb,0.013942
nd,0.006708
necessarili,0.007058
new,0.003220
nonzero,0.059208
normal,0.028736
note,0.008314
nth,0.014487
number,0.079357
obtain,0.005604
old,0.005966
onli,0.003319
oper,0.027292
order,0.011920
overfield,0.021821
p,0.027521
padic,0.015245
perfect,0.024167
perform,0.005068
physic,0.009436
piec,0.007962
pn,0.013303
point,0.004182
polynomi,0.165007
posit,0.004273
practic,0.004423
press,0.009732
primari,0.005457
prime,0.026572
primit,0.036596
probabl,0.005932
problem,0.004421
product,0.008618
profinit,0.019174
project,0.005116
properti,0.015018
propertiesif,0.020590
pseudo,0.031539
pure,0.013075
purpos,0.005378
quadrat,0.059998
radic,0.030318
ration,0.057005
rd,0.007403
reach,0.005329
real,0.048983
reduc,0.004967
refer,0.002622
regard,0.005030
regular,0.007183
repeatedli,0.009330
research,0.004254
revis,0.015672
ring,0.026643
roman,0.007033
root,0.072864
s,0.014876
satisfi,0.031114
say,0.017043
schinzel,0.020151
selfregular,0.021821
sens,0.010791
separ,0.043116
serg,0.028537
serr,0.044577
set,0.008124
simpl,0.034791
singl,0.009767
smallest,0.047491
societi,0.004572
sohn,0.018110
sophu,0.016714
space,0.016879
special,0.004564
split,0.015001
springerverlag,0.045350
stand,0.006858
steven,0.009195
studi,0.019283
subfield,0.056367
subject,0.009285
subset,0.018306
subtract,0.011226
summand,0.017944
survey,0.007014
symmetri,0.010159
t,0.006349
tensor,0.011705
term,0.003448
text,0.012590
theorem,0.024542
theori,0.081275
thi,0.013648
thu,0.004266
togeth,0.005032
topic,0.005790
total,0.040735
tower,0.010673
tradit,0.004595
transcend,0.010323
transcendent,0.069946
transit,0.006353
translat,0.006282
tsityuen,0.021821
type,0.004387
typic,0.004896
und,0.010736
uniqu,0.018668
uniti,0.016043
univers,0.007559
unrel,0.010434
use,0.005963
valuat,0.020193
variabl,0.020013
varieti,0.010959
vector,0.026477
veri,0.004167
vieweg,0.018921
waldschmidt,0.020590
way,0.003902
write,0.005706
wrt,0.057524
x,0.025846
y,0.015543
york,0.005620
zbl,0.109074
zero,0.015349
zp,0.019458
