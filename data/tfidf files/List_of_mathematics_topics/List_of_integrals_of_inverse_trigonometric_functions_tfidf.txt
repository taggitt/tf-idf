amint,0.064447
antideriv,0.016716
aoperatornam,0.028857
arbitrari,0.004553
arc,0.005667
arcco,0.217632
arccosaxafrac,0.011093
arccosaxdxxarccosaxfrac,0.011093
arccosaxdxxxarccosaxfrac,0.011093
arccosaxndx,0.011093
arccosaxndxfrac,0.011093
arccosaxndxquad,0.011093
arccosaxndxxarccosaxnfrac,0.011093
arccosec,0.011093
arccosin,0.010244
arccosxdxxarccosxsqrt,0.011093
arccot,0.115147
arccotang,0.011093
arccotaxafrac,0.011093
arccotaxdxfrac,0.033280
arccotaxdxxoperatornam,0.011093
arccotaxfrac,0.033280
arccotaxmfrac,0.011093
arccotxdxxoperatornam,0.011093
arccotxfrac,0.011093
arccsc,0.115147
arccscaxdxfrac,0.033280
arccscaxdxxoperatornam,0.011093
arccscaxfrac,0.033280
arccscaxmfrac,0.011093
arccscxdxxoperatornam,0.011093
arccscxln,0.011093
arccscxoperatornam,0.011093
arcosh,0.081957
arcsec,0.105810
arcsecaxdxfrac,0.033280
arcsecaxdxxoperatornam,0.011093
arcsecaxfrac,0.033280
arcsecaxmfrac,0.011093
arcsecxdxxoperatornam,0.011093
arcsecxoperatornam,0.011093
arcsin,0.232447
arcsinaxafrac,0.011093
arcsinaxdxxarcsinaxfrac,0.011093
arcsinaxdxxxarcsinaxfrac,0.011093
arcsinaxndx,0.011093
arcsinaxndxfrac,0.011093
arcsinaxndxquad,0.011093
arcsinaxndxxarcsinaxnfrac,0.011093
arcsinxdxxarcsinxsqrt,0.011093
arctan,0.090571
arctanaxafrac,0.011093
arctanaxdxxarctanaxfrac,0.011093
arctang,0.009207
arctanxdxxarctanxfrac,0.011093
artanh,0.040978
asin,0.007271
axac,0.061468
axafrac,0.010467
axarccosaxac,0.011093
axarccosaxnanfrac,0.011093
axarccosaxnannint,0.011093
axarcsinaxac,0.011093
axarcsinaxnanfrac,0.011093
axarcsinaxnannint,0.011093
axc,0.059354
axdxquad,0.042964
axfrac,0.009502
c,0.068136
common,0.002059
complet,0.002352
constant,0.003386
correspond,0.003092
d,0.126840
determin,0.002365
displaystyl,0.140462
express,0.002656
follow,0.001754
formula,0.034463
frac,0.068700
function,0.029185
ha,0.001462
hyperbol,0.005914
indefinit,0.005152
infinit,0.004293
instanc,0.002969
int,0.220115
integr,0.034400
invers,0.023151
involv,0.002125
known,0.003664
leftaxrightac,0.021482
leftaxrightafrac,0.021482
leftaxrightsqrt,0.021482
leftxrightc,0.021482
leftxsqrt,0.011093
list,0.008153
ln,0.080813
m,0.094382
mneq,0.058489
n,0.111926
nneq,0.019238
nnint,0.020111
notat,0.004925
nsqrt,0.018791
number,0.001833
onli,0.001687
operatornam,0.055760
page,0.003593
point,0.002126
sin,0.005276
someth,0.003567
sqrt,0.047904
thi,0.001387
thu,0.002168
trigonometr,0.027247
use,0.003031
valu,0.002377
written,0.002937
x,0.808105
xac,0.038476
xarccosaxdxfrac,0.022186
xarccosaxfrac,0.022186
xarccosaxnnnfrac,0.011093
xarcsinaxdxfrac,0.022186
xarcsinaxfrac,0.022186
xarcsinaxnnnfrac,0.011093
xarctanaxdxfrac,0.022186
xarctanaxfrac,0.022186
xasqrt,0.042964
xc,0.032934
xmarccosaxdxfrac,0.011093
xmarccosaxmfrac,0.011093
xmarcsinaxdxfrac,0.011093
xmarcsinaxmfrac,0.011093
xmarctanaxdxfrac,0.011093
xmarctanaxmfrac,0.011093
xmaxdxquad,0.021482
xmoperatornam,0.064447
xmsqrt,0.042964
xoperatornam,0.105244
xrightcxoperatornam,0.011093
xsqrt,0.016898
