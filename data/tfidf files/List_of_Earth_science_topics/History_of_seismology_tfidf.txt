aaa,0.014350
aboveocean,0.021582
acceleromet,0.018895
accid,0.010709
activ,0.008865
addit,0.008988
adequ,0.009319
advanc,0.010973
advent,0.009715
affect,0.005805
ailsa,0.019218
alaska,0.026876
alegret,0.021582
alessandro,0.013569
alexand,0.008511
allabi,0.039174
alway,0.005973
america,0.024903
american,0.010066
analysi,0.005355
analyz,0.007467
anaximen,0.016753
ancient,0.006436
anthropogen,0.013919
anticlin,0.018748
antiqu,0.009192
appear,0.010290
appli,0.004749
applic,0.005305
april,0.006796
arenilla,0.021582
argu,0.005909
ari,0.014596
aris,0.007247
aristotl,0.009157
arriv,0.007151
arthur,0.009062
artifici,0.007501
arz,0.020536
assess,0.022700
associ,0.013018
asteroid,0.012836
athanasiu,0.016384
atmospher,0.008702
attempt,0.015201
author,0.005001
b,0.005884
barton,0.015066
base,0.003870
basilicata,0.020839
bce,0.029490
becaus,0.012096
befor,0.004574
behaviour,0.016517
believ,0.011080
bell,0.010370
benmenahem,0.020536
bevi,0.020264
bibcodenaturh,0.019587
bibcodesci,0.019793
bibcodesciw,0.020839
bodi,0.032250
bomb,0.010141
book,0.005090
borehol,0.016959
boundari,0.037390
bown,0.020264
bralow,0.021582
branch,0.005696
broadband,0.014009
bulletin,0.012044
c,0.015379
calcul,0.006835
caltechusg,0.021582
cambridg,0.014210
carri,0.005783
catalog,0.012278
caus,0.033519
causat,0.025880
ce,0.019660
cell,0.007715
center,0.005722
central,0.010058
centuri,0.004606
challeng,0.006408
channel,0.008855
characterist,0.012367
charl,0.006871
chemic,0.013662
chicxulub,0.040036
china,0.006892
christeson,0.021582
christian,0.006952
civil,0.006154
claey,0.021582
claim,0.010891
clear,0.006677
clive,0.014456
close,0.004800
coast,0.008188
cockel,0.019793
coin,0.007429
coincid,0.019302
collin,0.012668
come,0.005131
commonli,0.005958
compani,0.005665
complet,0.004955
compon,0.006179
comprehens,0.007903
compress,0.011078
compression,0.019396
comput,0.005945
concis,0.010928
conclud,0.007338
condemn,0.010106
condit,0.005020
connect,0.005907
consider,0.012060
consist,0.004802
consortium,0.012562
constraint,0.008909
continu,0.004347
control,0.009534
controlledsourc,0.021582
controversi,0.007070
convect,0.013401
core,0.060959
coremantl,0.036231
crater,0.026987
creat,0.004389
cretaceouspaleogen,0.032022
cryospher,0.016217
cusp,0.017345
d,0.005808
data,0.011453
david,0.012748
deal,0.005780
deep,0.007988
definit,0.005673
depend,0.004734
deploy,0.009710
design,0.005270
detect,0.023640
determin,0.009966
deutsch,0.012131
develop,0.010765
dictionari,0.008699
differ,0.003803
dinosaur,0.014149
direct,0.014294
discoveri,0.006720
discret,0.008463
divers,0.006901
divid,0.005690
dixon,0.030415
doe,0.004855
doia,0.015032
doiscienc,0.030871
dome,0.014324
donald,0.010116
dougla,0.010604
drawn,0.008468
dynam,0.006727
dynasti,0.009330
earli,0.004320
earlier,0.006383
earliest,0.014441
earth,0.169556
earthquak,0.579946
east,0.006641
ed,0.024191
effect,0.008393
effici,0.006764
ejecta,0.019396
elast,0.064921
electromagnet,0.009685
elisabetta,0.021582
emil,0.012153
enabl,0.006851
energi,0.012095
engin,0.049806
environment,0.006945
equival,0.006958
eric,0.010261
erupt,0.010787
estim,0.006079
europ,0.005857
europeanmediterranean,0.020536
event,0.033731
evid,0.005891
ewe,0.015599
exampl,0.003869
excit,0.010531
exist,0.008591
expans,0.007289
expect,0.005935
experi,0.005312
explor,0.013164
explos,0.067034
extern,0.003378
extinct,0.019642
f,0.012726
facilit,0.007788
fail,0.006495
faster,0.009012
fastest,0.012198
fault,0.023209
featur,0.011684
fidel,0.013095
field,0.018870
firstli,0.012723
floor,0.010508
flower,0.010490
fluid,0.018212
follow,0.003695
forecast,0.030533
forens,0.012373
form,0.007062
forthcom,0.013986
forward,0.008200
foundat,0.018071
francisco,0.019938
frequenc,0.017656
function,0.004729
futur,0.005626
g,0.006249
gail,0.017032
gareth,0.016889
gener,0.019916
geolog,0.017656
geophys,0.024175
giant,0.010101
given,0.004633
glacier,0.012076
global,0.018404
goldin,0.017430
goto,0.016501
govern,0.004392
grajalesnishimura,0.021582
grand,0.008973
great,0.005206
greek,0.006541
greg,0.013963
griev,0.016384
ground,0.020165
gubbin,0.021582
guid,0.006531
gulick,0.018476
ha,0.024642
hall,0.008764
han,0.009231
handbook,0.009349
harold,0.022170
harri,0.009715
hazard,0.031649
helmberg,0.021582
heng,0.030489
hi,0.004141
high,0.004677
higher,0.005491
highresolut,0.015516
hiroo,0.019793
histor,0.005189
histori,0.011542
horizont,0.010653
hour,0.008117
howev,0.011247
hundr,0.007018
iceberg,0.016011
identifi,0.011168
ignacio,0.015599
impact,0.012661
impend,0.014456
implic,0.008048
import,0.007879
incess,0.016820
includ,0.019240
incorpor,0.006343
independ,0.004744
indict,0.059220
individu,0.004764
induc,0.018294
industri,0.005272
infer,0.008877
inform,0.013777
ing,0.015913
inner,0.009286
insid,0.007747
institut,0.004888
instrument,0.035565
intensifi,0.011323
interact,0.011527
interfac,0.010056
interior,0.045327
intern,0.012144
introduct,0.012247
involv,0.004478
iri,0.014836
iron,0.008273
isbn,0.016382
issn,0.011919
itali,0.008313
italian,0.008667
j,0.029006
jaim,0.015866
japan,0.007639
jardetzki,0.021582
jay,0.012373
jeffrey,0.024004
jo,0.021983
joanna,0.016011
john,0.010508
johnson,0.010555
kanamori,0.018230
kazuhisa,0.021582
kenneth,0.010365
kiessl,0.020839
kilomet,0.011677
kiloton,0.018230
kircher,0.017799
kirk,0.014805
known,0.003859
koeberl,0.020839
kring,0.019396
l,0.006798
laia,0.017608
laid,0.008717
laquila,0.041679
larg,0.020351
largescal,0.008999
largest,0.012757
later,0.004593
lay,0.008690
layer,0.017780
led,0.004984
legaci,0.009710
lehmann,0.016820
lemeri,0.020839
lianx,0.021582
like,0.004125
link,0.006716
liquid,0.041391
lisbon,0.027062
lister,0.015642
local,0.010461
locat,0.017034
loga,0.019587
longburi,0.021582
longitudin,0.013128
longterm,0.008126
love,0.018336
lowshearveloc,0.021582
m,0.016569
macleod,0.016061
magnetotellur,0.019587
magnitud,0.019055
main,0.004888
mainstream,0.009371
major,0.008194
make,0.007854
mallet,0.018115
mani,0.010471
manslaught,0.017345
mantl,0.052256
map,0.028258
march,0.012807
mario,0.013383
martin,0.008174
mass,0.012666
materi,0.021646
mathemat,0.006070
matsui,0.021582
mcgrawhil,0.011159
mean,0.008505
media,0.007083
meet,0.006248
melosh,0.020839
meteor,0.026949
method,0.019130
michael,0.021320
michel,0.021171
micros,0.021582
mile,0.008976
miletu,0.028648
mixtur,0.009655
mode,0.043469
modern,0.004632
monitor,0.016702
montanari,0.020018
month,0.006822
morgan,0.010963
motion,0.070131
motiv,0.007544
movement,0.010650
nation,0.004279
natur,0.008245
neal,0.014376
near,0.025397
network,0.006455
nichol,0.015557
nicola,0.011568
nois,0.011012
nonearthquak,0.021582
noninvas,0.014743
normal,0.030776
norri,0.016562
notabl,0.011851
note,0.004452
nuclear,0.015781
number,0.003863
observ,0.020386
occur,0.009812
occurr,0.009988
ocean,0.023839
offici,0.011725
oil,0.007707
oldham,0.039174
onli,0.007109
opposit,0.006354
origin,0.008559
outer,0.029599
overview,0.007737
oxford,0.014511
p,0.017684
pacifi,0.014868
packag,0.009789
paleoseismolog,0.021582
particular,0.014500
pass,0.005999
past,0.006126
paul,0.006712
pdf,0.035750
penni,0.014032
perceiv,0.007765
period,0.004588
perpendicular,0.025732
perspect,0.006910
peter,0.014083
petroleumbear,0.021582
philipp,0.012349
physic,0.010106
pi,0.011100
pierazzo,0.021582
pkp,0.021582
planet,0.017523
planetlik,0.016889
plate,0.020054
pmid,0.023799
point,0.004479
polar,0.028380
popul,0.005494
possibl,0.004516
pp,0.007219
precis,0.007294
precursor,0.009646
predict,0.045354
prepared,0.016164
presenc,0.006696
press,0.020846
pressur,0.006665
primari,0.011690
princip,0.006917
probabl,0.012708
process,0.016744
produc,0.004607
program,0.005570
progress,0.006035
pronunci,0.011743
propag,0.039465
propos,0.005463
proven,0.009352
provid,0.011786
provinc,0.008185
public,0.004459
purpos,0.011519
pwave,0.143752
r,0.023702
radexpro,0.021582
rang,0.005315
rapid,0.007674
ravizza,0.021582
rayleigh,0.029800
read,0.004877
realtim,0.011771
rebolledovieyra,0.021582
rebound,0.013295
record,0.035517
refer,0.005616
regard,0.005388
region,0.010058
reid,0.013897
reimold,0.021582
relat,0.003786
releas,0.007018
remain,0.004644
report,0.005397
research,0.009112
resolut,0.008591
respons,0.009742
result,0.003911
retriev,0.008343
richard,0.027751
ring,0.019023
risk,0.006910
robert,0.012349
robin,0.012087
rock,0.026384
routin,0.020162
s,0.031864
salg,0.021582
salt,0.009621
san,0.017837
satisfi,0.008330
sazmldi,0.021582
scholarli,0.009940
schult,0.018476
scienc,0.042510
scientif,0.016953
scientist,0.025687
sean,0.014625
second,0.009218
secondari,0.007997
secondli,0.012349
seiscomp,0.021582
seism,0.021582
seismic,0.291924
seismogram,0.098969
seismograph,0.072462
seismolog,0.401436
seismologist,0.106212
seismomet,0.086318
seismoscop,0.020018
sensor,0.011640
separ,0.005130
set,0.004350
seth,0.015396
sever,0.004074
shadow,0.010942
shake,0.014174
shallow,0.023758
shear,0.013193
shearer,0.018608
shift,0.006805
shorter,0.009998
shown,0.006772
signal,0.032322
silic,0.014078
similar,0.004667
simul,0.008492
sinc,0.007816
site,0.006583
size,0.006086
slower,0.022457
slowli,0.009008
societi,0.019587
softwar,0.008280
solid,0.033600
sourc,0.029498
special,0.004888
specul,0.008131
speijer,0.021582
spur,0.010977
stand,0.007345
stein,0.014402
stephen,0.008682
stream,0.009368
strike,0.008693
strong,0.011358
stronger,0.009409
strongest,0.011340
strongli,0.007533
structur,0.013699
studi,0.053697
subsurfac,0.014625
suggest,0.005471
sumatraandaman,0.021582
support,0.004647
surfac,0.106156
surround,0.007157
swave,0.190518
sweet,0.012175
t,0.006800
takafumi,0.021582
tamara,0.017900
techniqu,0.006006
tecton,0.061869
terrorist,0.011829
test,0.006389
th,0.004481
thale,0.013032
theoret,0.006408
theori,0.018325
therefor,0.010624
thi,0.008770
thorn,0.014032
thu,0.004568
time,0.010374
timespan,0.018608
timothi,0.012485
tnt,0.016164
tobia,0.016272
togeth,0.005389
tomographi,0.013687
tool,0.006258
trace,0.007494
transvers,0.013550
trap,0.020481
travel,0.048309
trial,0.008918
tsunami,0.042819
type,0.018796
ultralow,0.018476
underground,0.010833
understand,0.010610
underwat,0.012601
union,0.005807
univers,0.012144
unlik,0.006508
urrutiafucugauchi,0.021582
use,0.025549
usg,0.015244
uwe,0.018608
v,0.013938
vajda,0.020536
valdivia,0.018005
van,0.008549
variou,0.008898
vault,0.014174
veloc,0.010277
vertic,0.010111
vibrat,0.011405
vivi,0.021582
volcan,0.011213
w,0.013116
wa,0.012818
warn,0.028195
wave,0.307403
websit,0.007390
week,0.008214
wellestablish,0.013128
wen,0.015820
whalen,0.019218
wide,0.005033
wiechert,0.019218
wileyblackwel,0.014402
willumsen,0.021582
wind,0.009286
wolf,0.011971
wolfgang,0.011869
word,0.005425
work,0.007663
world,0.003962
write,0.006111
wysess,0.021582
zhang,0.026522
zone,0.008078
