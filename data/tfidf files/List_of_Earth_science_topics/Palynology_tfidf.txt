aasp,0.039668
abl,0.005559
absenc,0.008091
abund,0.035704
accomplish,0.008846
acet,0.014117
acetolysi,0.062968
acid,0.044341
acritarch,0.076872
activ,0.004391
advanc,0.010872
age,0.016536
ago,0.008528
agricultur,0.006542
air,0.006761
al,0.007549
alga,0.012177
allergi,0.030509
allow,0.004377
alter,0.023685
america,0.012337
american,0.009974
analog,0.007864
analys,0.008605
analysi,0.047753
ancient,0.006377
andr,0.011489
anhydrid,0.017026
ani,0.003879
anim,0.006160
annelid,0.016733
annot,0.012052
anoth,0.004233
antev,0.020347
anthropogen,0.013791
apertur,0.014860
appear,0.005098
appli,0.004705
applic,0.021026
archaeolog,0.028272
area,0.013114
arthropod,0.013835
assarson,0.020989
assemblag,0.013483
associ,0.017198
audienc,0.019869
austria,0.010183
aweb,0.020648
base,0.003835
basi,0.005482
basin,0.010458
becam,0.004851
becaus,0.011985
bed,0.011462
began,0.005299
belgium,0.010417
better,0.005984
biolog,0.018883
biostratigraphi,0.056630
blackwel,0.022190
bonfir,0.017949
botani,0.036326
botanist,0.012606
branch,0.011288
broadli,0.008884
brother,0.008900
burn,0.009097
c,0.005079
calcar,0.016733
calcul,0.006772
canadian,0.009022
capabl,0.006999
carbon,0.008663
carbonifer,0.016177
care,0.006677
caus,0.009488
cell,0.007644
cellulist,0.020989
cellulos,0.013748
centr,0.007308
centuri,0.004564
challeng,0.012699
chang,0.020037
chemic,0.060915
chitin,0.031630
chitinozoan,0.080312
chose,0.009998
christian,0.006888
cimp,0.020648
circular,0.010359
classic,0.005873
climat,0.053130
clue,0.026450
coal,0.010365
collect,0.005185
colour,0.010146
combaz,0.020989
commiss,0.014155
common,0.008597
commun,0.004648
compar,0.005258
comparison,0.007454
complet,0.004910
compon,0.006122
compos,0.006744
composit,0.022110
compound,0.008010
comput,0.005890
condit,0.024872
confin,0.009545
conjunct,0.009934
consid,0.016584
consider,0.005974
contemporari,0.006954
context,0.006254
copenhagen,0.012634
core,0.007549
correctli,0.010215
correl,0.016574
correspond,0.006454
count,0.008108
countri,0.004678
crime,0.008820
cromerian,0.020347
cyst,0.083669
data,0.005673
databas,0.008317
defin,0.009758
deflocul,0.020989
densiti,0.008579
deposit,0.067336
deriv,0.010811
describ,0.004375
descript,0.006498
desmid,0.040156
destroy,0.007434
detect,0.007807
determin,0.014811
develop,0.003555
devonian,0.015913
devot,0.008403
diagram,0.009780
diatom,0.027244
diffus,0.009712
digest,0.032587
dinocyst,0.020347
dinoflagel,0.084380
directori,0.011933
disciplin,0.013102
dissolut,0.010714
dissolv,0.017660
distribut,0.011732
divers,0.013675
doe,0.004810
du,0.010048
dust,0.023214
earli,0.012843
earlier,0.006324
earliest,0.007154
earth,0.006719
edit,0.006699
effect,0.004158
ehrenberg,0.017446
electron,0.007162
end,0.004679
energet,0.011597
english,0.005961
enumer,0.012370
environ,0.017021
environment,0.013762
erdtman,0.041979
estim,0.006023
et,0.007357
europ,0.005804
evid,0.011673
evolutionari,0.008433
examin,0.046895
exampl,0.003833
exin,0.020989
exist,0.004256
exoskeleton,0.016599
explor,0.013043
extern,0.003346
extract,0.024718
f,0.006304
fast,0.008762
fatal,0.011125
featur,0.005788
feder,0.006815
fever,0.012200
fgri,0.020989
field,0.009348
fine,0.018301
flotat,0.018062
flower,0.010394
fluoresc,0.012472
follow,0.007322
foraminiferan,0.018721
forens,0.012260
form,0.003498
format,0.012485
fossil,0.079364
fossilifer,0.020078
fraction,0.008958
fragment,0.009002
french,0.012319
frequenc,0.008746
freshwat,0.024194
frh,0.020989
fundament,0.005861
fungi,0.011079
ga,0.007700
gain,0.011137
garden,0.010237
gener,0.006577
geochemistri,0.013949
geochronolog,0.016410
geograph,0.007443
geographi,0.008425
geolog,0.052481
geologist,0.037073
georg,0.006596
geotherm,0.015333
german,0.006463
ghavidel,0.020989
gideon,0.016534
given,0.009181
glycerol,0.015720
glyceroljelli,0.020989
gottfri,0.012074
grain,0.019889
gram,0.012791
granlund,0.019612
greatli,0.007495
greek,0.012962
green,0.007871
grew,0.007648
group,0.004128
gunnar,0.030912
h,0.006358
harbour,0.011770
hay,0.012777
help,0.010021
henri,0.007377
herb,0.013190
hf,0.029994
hi,0.016415
histori,0.011436
holocen,0.028701
honey,0.013813
hopley,0.020989
horizon,0.011156
host,0.008369
howev,0.003714
human,0.004409
humic,0.017736
hyde,0.030509
hydrochlor,0.015456
hydrofluor,0.036874
hydrogen,0.009480
hydroxid,0.014997
hyman,0.015456
identif,0.018031
import,0.011711
improv,0.005632
includ,0.031772
index,0.014542
india,0.007007
infer,0.017592
inform,0.004550
initi,0.004973
inorgan,0.010701
input,0.008292
insect,0.010400
insolubl,0.014608
institut,0.004843
interdisciplinari,0.009408
intern,0.008021
international,0.015254
interpret,0.006106
introduc,0.016107
investig,0.019240
isbn,0.016232
isol,0.007431
iversen,0.018876
jaw,0.013602
johann,0.009793
journal,0.005886
kerogen,0.037443
kerosen,0.015913
kew,0.017840
kidston,0.020989
known,0.003824
knut,0.015585
koh,0.016804
kring,0.019218
kristiania,0.020078
l,0.006736
laboratori,0.007765
lagerheim,0.020989
laid,0.008637
lake,0.027077
land,0.011583
languag,0.017255
late,0.005298
latin,0.007192
leaflet,0.016665
learn,0.006428
lectur,0.016640
lennart,0.017840
level,0.004575
life,0.009837
light,0.012617
like,0.004087
limnolog,0.014142
link,0.009981
linnean,0.016733
literatur,0.006836
litter,0.014763
live,0.004947
log,0.010720
logi,0.016068
long,0.009936
lspsg,0.020989
lucr,0.012620
macromolecular,0.015414
major,0.004059
make,0.003890
mani,0.010375
mantel,0.017446
marin,0.035036
materi,0.032170
matter,0.028777
matur,0.008903
maximum,0.008320
mean,0.008427
melissopalynolog,0.020989
metamorph,0.015864
method,0.004738
methodolog,0.016197
microflor,0.020648
microforam,0.020989
microfossil,0.033752
micrometr,0.015864
micropalaeontolog,0.040156
microscop,0.088898
microscopi,0.024083
middl,0.006308
million,0.012019
miner,0.008471
modern,0.013768
mohammad,0.013857
moor,0.010325
moss,0.013642
mount,0.026758
mouthpart,0.018437
n,0.006871
narrowli,0.011891
nehemiah,0.017840
netherland,0.008916
nitrogen,0.010561
nonorgan,0.017105
nordic,0.013208
north,0.012150
number,0.019138
object,0.005230
observ,0.010099
obtain,0.005947
occup,0.007972
oil,0.015273
onc,0.005717
onli,0.003522
optic,0.018409
orbicul,0.020347
organ,0.066774
organicwal,0.020989
oxygen,0.009315
p,0.005840
page,0.007500
palaeoclimat,0.020989
palaeoecolog,0.018876
palaeoenviron,0.041979
palaeoenvironment,0.040694
palaeotemperatur,0.020989
palaeozo,0.018721
paldat,0.020989
pale,0.014860
paleobotani,0.018062
paleontolog,0.025213
paleopalynolog,0.020648
paleosampl,0.020989
palozoiqu,0.020648
palun,0.020989
paluno,0.020989
palynofaci,0.230886
palynolog,0.529110
palynologisch,0.020989
palynologist,0.095208
palynomorph,0.447640
palynotaxa,0.020989
parent,0.008616
particl,0.016448
particul,0.054737
particular,0.014367
particularli,0.005365
past,0.024279
pattern,0.006528
paul,0.006650
pd,0.015068
peat,0.031082
pedolog,0.016233
percentag,0.008526
period,0.004546
physic,0.010013
phytoplankton,0.032032
picea,0.020347
pine,0.013039
pinu,0.017446
plant,0.033213
plantrel,0.020989
pleistocen,0.013972
pollen,0.383032
polychaet,0.017635
pom,0.037151
pondicherri,0.019834
post,0.007425
potassium,0.012165
practic,0.004694
prasinophyt,0.019041
precambrian,0.016534
predict,0.006419
prehistor,0.010605
prepar,0.049810
presenc,0.019904
present,0.008773
preserv,0.014790
prevail,0.009281
principl,0.005266
problem,0.004692
produc,0.013695
product,0.009145
profus,0.015767
provid,0.015571
pseudochitin,0.020989
public,0.008837
publish,0.019479
quantit,0.008151
quaternari,0.013857
radiolarian,0.018721
rang,0.005266
rapidli,0.007396
ratio,0.007912
reason,0.004993
recent,0.004818
reconstruct,0.017439
record,0.017595
recrystal,0.018575
reduc,0.005271
refer,0.008347
reflect,0.005979
region,0.004982
reinsch,0.020347
rel,0.004989
relat,0.003751
remov,0.019273
report,0.005348
reproduct,0.009452
requir,0.013141
research,0.027084
resist,0.007210
result,0.003875
revis,0.008315
revolution,0.011682
robert,0.012236
rock,0.078424
rootlet,0.019834
routin,0.009988
ruptur,0.013972
s,0.023678
safeti,0.008968
sampl,0.058045
sarauw,0.020989
scan,0.022220
scandinavian,0.013072
scene,0.010023
scienc,0.032759
scientif,0.011198
scientist,0.006362
scleroprotein,0.020989
scolecodont,0.040156
seam,0.015541
sear,0.015414
season,0.017984
second,0.004566
sediment,0.135394
sedimentari,0.138578
separ,0.005083
sequenc,0.015449
servic,0.005421
sever,0.004037
sexual,0.009143
sheffield,0.014827
shelter,0.010848
siev,0.046755
silic,0.027898
silicon,0.011820
similar,0.004624
singl,0.005182
site,0.019569
size,0.006030
skin,0.009821
slide,0.048344
societi,0.014555
soil,0.008774
sometim,0.010082
sonif,0.020989
sourc,0.004871
special,0.004843
specialist,0.009608
specimen,0.012008
spore,0.141421
sporopollenin,0.019612
sprinkl,0.033752
spruce,0.016804
st,0.006242
stamen,0.018181
step,0.013133
strata,0.013444
stratigraph,0.074303
stratigraphi,0.015178
strew,0.041979
strewn,0.018721
strong,0.005626
structur,0.004524
stub,0.017446
studi,0.102314
subject,0.004926
subset,0.009712
substanc,0.008141
suffer,0.007068
sulfur,0.011384
surfac,0.007012
swedish,0.031684
syooki,0.020989
taken,0.005670
taxon,0.015333
taxonomi,0.022282
techniqu,0.005951
teeth,0.012189
term,0.018296
terrestri,0.021086
textbook,0.008728
thermal,0.019860
thesi,0.009583
thi,0.017379
thousand,0.006829
thu,0.009053
time,0.013705
togeth,0.016020
tradit,0.004876
transmit,0.009488
travers,0.012592
treat,0.006781
treatment,0.049866
tree,0.007977
trybom,0.020648
type,0.013967
typic,0.005195
uk,0.014196
ultrason,0.031928
ultraviolet,0.012097
undertak,0.010512
uniqu,0.006603
unit,0.003905
univers,0.008021
unsiev,0.020989
unwin,0.014323
use,0.069614
util,0.007005
uv,0.013173
variou,0.004408
veget,0.036979
veri,0.004422
vienna,0.011079
visibl,0.008559
vitrinit,0.020989
von,0.007854
wa,0.038102
water,0.005973
way,0.004140
weber,0.011840
wellpreserv,0.016534
wet,0.011561
white,0.007352
wide,0.004987
wider,0.008817
widespread,0.007440
william,0.012410
witt,0.015720
word,0.010751
work,0.007592
worm,0.012200
year,0.003908
