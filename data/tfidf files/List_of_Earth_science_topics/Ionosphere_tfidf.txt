abl,0.006307
abov,0.031966
absent,0.006085
absorb,0.004991
absorpt,0.066856
academ,0.007479
account,0.005881
accur,0.004254
achiev,0.003141
act,0.002853
action,0.003067
activ,0.027405
actual,0.003195
ad,0.003186
addit,0.007578
aero,0.022779
aerosa,0.013135
affect,0.003262
aindex,0.013135
air,0.007671
alaska,0.007552
alcayd,0.013135
allow,0.004966
alouett,0.036391
alpha,0.011559
altitud,0.047304
alway,0.003357
amateur,0.027274
american,0.002828
amsterdam,0.006351
angl,0.016907
ani,0.002200
anomali,0.040911
ansfr,0.013135
antarctica,0.007444
antenna,0.007512
anywher,0.012223
ap,0.007542
appear,0.002892
appleton,0.025234
appletonbarnett,0.013135
applic,0.005963
appreci,0.005540
approxim,0.006805
arecibo,0.031612
aronomi,0.013135
arthur,0.010186
associ,0.007317
astronomi,0.005606
astrophys,0.007112
atmospher,0.083153
atom,0.013098
attack,0.004025
attenu,0.007926
attract,0.004017
auror,0.034627
aurora,0.008095
australia,0.004340
australian,0.005192
avail,0.006104
averag,0.003729
award,0.004401
away,0.011410
b,0.006614
background,0.004288
backscatt,0.019793
balanc,0.003951
band,0.005429
barnett,0.008999
base,0.008702
basic,0.003028
bay,0.005638
beacon,0.008156
beam,0.006806
bear,0.004672
becaus,0.006799
becom,0.007230
befor,0.005142
behavior,0.007063
belros,0.013135
belt,0.006563
bent,0.007648
berkner,0.013135
berlin,0.010723
bibcoderascb,0.013135
bilitza,0.013135
bit,0.006123
black,0.004063
blackout,0.017889
blelli,0.026270
board,0.004343
bodi,0.006042
boulder,0.007637
bounc,0.032321
bragg,0.008449
british,0.006723
broadcast,0.005841
c,0.011525
calcul,0.003841
cambridg,0.007987
canada,0.008637
canadian,0.005118
cap,0.012284
captur,0.008280
carbon,0.004914
carl,0.005034
carri,0.003250
caus,0.021531
celesti,0.006392
chanc,0.004854
chang,0.009093
character,0.003824
charg,0.011673
chemistri,0.004419
chian,0.012394
chines,0.004334
circa,0.006775
circuit,0.005688
civilian,0.005242
close,0.002698
closer,0.004989
cloud,0.011535
cm,0.006342
code,0.003902
coher,0.022233
collect,0.002941
collid,0.006830
collis,0.012387
columbia,0.005608
combin,0.002875
committe,0.004326
common,0.002438
commun,0.018459
compens,0.005097
compet,0.004157
complet,0.005570
compon,0.006946
composit,0.004180
compound,0.004544
comput,0.006683
concentr,0.003946
condit,0.002821
conduct,0.003456
confirm,0.004347
congress,0.004518
consist,0.002699
construct,0.003123
contactor,0.012394
contain,0.011573
content,0.007958
contest,0.005306
contribut,0.002960
cornwal,0.008507
coron,0.007584
cosmic,0.006169
cospar,0.013135
countri,0.002654
coupl,0.004666
creat,0.004933
crest,0.008374
critic,0.017799
crystal,0.005657
current,0.015491
curvatur,0.007342
cutoff,0.008338
cycl,0.012403
d,0.035914
daili,0.004716
dampen,0.008187
data,0.012874
davi,0.011695
dawndusk,0.013135
day,0.024347
daytim,0.017889
db,0.008449
decemb,0.003701
decreas,0.011914
deduc,0.006023
defin,0.002767
deform,0.006584
degre,0.013211
demonstr,0.003625
densest,0.009274
densiti,0.063270
depend,0.005322
describ,0.007447
descript,0.007373
detect,0.013287
determin,0.008402
develop,0.008068
did,0.002993
dieter,0.008721
differ,0.006414
dip,0.008022
direct,0.005356
directli,0.006481
disappear,0.010255
discharg,0.006769
discov,0.003551
discoveri,0.003777
dislodg,0.009274
dispers,0.005535
displaystyl,0.013859
distanc,0.017179
distant,0.011212
disturb,0.039644
dit,0.010801
diurnal,0.008999
doe,0.002728
doi,0.005339
doir,0.012394
domin,0.003318
dominion,0.006888
dordrecht,0.007835
doublehop,0.013135
dr,0.005003
dregion,0.091947
drift,0.006218
du,0.005700
dual,0.005686
dure,0.025537
dx,0.008527
dynamo,0.009177
e,0.045754
earli,0.004857
earlyfast,0.013135
earth,0.099112
eastward,0.007434
echo,0.006763
ed,0.006798
edg,0.005297
edit,0.003800
edward,0.008834
edwin,0.012812
effect,0.016512
eiscat,0.013135
eject,0.007387
elay,0.013135
electr,0.040149
electrodynam,0.021871
electrojet,0.026270
electromagnet,0.032661
electron,0.113769
electronionplasma,0.013135
electrostat,0.014869
elizabeth,0.005776
elsevi,0.006818
emiss,0.028808
emphasi,0.004404
enabl,0.007701
end,0.005309
energet,0.013158
energi,0.020395
engin,0.006998
enhanc,0.026984
enthusiast,0.006921
entir,0.003149
environ,0.003218
ephemer,0.008816
eponym,0.007861
equat,0.024230
equatori,0.050972
es,0.038466
escap,0.004875
especi,0.002819
essexcohen,0.013135
estim,0.003416
et,0.004173
event,0.028438
eventu,0.003451
evid,0.003311
ew,0.009416
examin,0.003800
exampl,0.002174
exce,0.006342
excit,0.005919
exist,0.014487
exospher,0.009896
expect,0.003336
experi,0.008957
experiment,0.004055
explor,0.003699
express,0.003146
extend,0.006405
extern,0.001898
extract,0.004673
extrem,0.006966
f,0.071533
facilit,0.004377
fact,0.003082
fail,0.003650
far,0.003292
farther,0.006781
fast,0.004970
fcritic,0.012394
field,0.039774
fix,0.003853
flare,0.032751
flow,0.003823
fluctuat,0.010844
flux,0.025550
focu,0.003562
focus,0.003379
follow,0.004153
forc,0.010409
form,0.003969
fountain,0.008631
fourier,0.006775
fragment,0.005106
free,0.011863
french,0.003494
frequenc,0.188553
frequencydepend,0.010004
frequent,0.003650
friedrich,0.005310
ft,0.005674
ftextcriticalsin,0.013135
ftextcriticaltim,0.013135
ftextmuffrac,0.013135
function,0.007974
ga,0.017473
gakona,0.013135
gase,0.005788
gauss,0.015722
gave,0.003778
gener,0.009328
geograph,0.004222
geomagnet,0.087442
geometr,0.005339
geophys,0.020381
geophysicist,0.009208
geostationari,0.010459
geosynchron,0.010120
german,0.003666
ginzburg,0.009896
given,0.007812
glace,0.013135
gleiberg,0.013135
goe,0.004672
govern,0.002469
gp,0.007075
gpsgnss,0.013135
great,0.005852
greater,0.006540
greatli,0.004251
grenobl,0.021074
ground,0.003778
growth,0.003286
guglielmo,0.009658
guid,0.003671
h,0.003606
ha,0.008656
haarp,0.039406
hamper,0.006881
handbook,0.010509
hard,0.008522
hargreav,0.009749
heat,0.004578
heatingion,0.013135
heavili,0.004185
heavisid,0.045280
heidelberg,0.007861
height,0.025884
helium,0.006948
hemispher,0.023651
henc,0.008102
hf,0.051044
hfconnect,0.013135
hi,0.004656
high,0.036807
highenergi,0.007502
higher,0.024693
highfrequ,0.008767
hill,0.010329
histori,0.002162
hit,0.010751
hop,0.016539
horizon,0.006328
horizont,0.029938
hour,0.022812
howev,0.012643
huge,0.004828
hydrogen,0.010755
hz,0.008917
ideal,0.004224
iee,0.011542
ii,0.003398
impli,0.004036
import,0.006643
impos,0.004465
inadvert,0.007980
incid,0.020549
includ,0.009011
incoher,0.032880
incom,0.003976
increas,0.029961
inde,0.004414
independ,0.002666
index,0.024748
indic,0.009582
induct,0.005749
influenc,0.002859
inform,0.002581
inner,0.005219
innermost,0.008917
instead,0.003041
institut,0.002747
instrument,0.003997
intend,0.004019
intens,0.021561
interact,0.009718
interfer,0.005299
intern,0.011376
interpret,0.003463
introduc,0.003045
investig,0.003638
involv,0.007551
ion,0.084455
ionic,0.007182
ionis,0.009145
ioniz,0.219806
ionogram,0.065677
ionosond,0.052541
ionospher,0.642006
iri,0.033355
irregular,0.006074
isbn,0.018416
isi,0.015409
j,0.009782
jack,0.012277
japanes,0.004948
jicamarca,0.026270
john,0.005906
johnson,0.005932
joseph,0.004278
juli,0.003750
jupit,0.007434
just,0.009082
k,0.023618
kamid,0.013135
karl,0.009766
kelley,0.009308
kennelli,0.024789
kennellyheavisid,0.026270
kenneth,0.005825
khz,0.018908
kitesupport,0.013135
klobuchar,0.026270
kluwer,0.007873
km,0.102615
known,0.021693
la,0.004333
label,0.004671
laboratori,0.008810
lack,0.003309
larg,0.002287
larger,0.003426
later,0.005163
latitud,0.019753
launch,0.017487
law,0.002615
layer,0.269827
led,0.002801
leo,0.006497
lep,0.010459
lespac,0.011389
letter,0.008570
level,0.015574
light,0.003578
lighter,0.006812
lightn,0.044664
lightninginduc,0.012394
lilensten,0.013135
limit,0.007952
line,0.009320
link,0.001887
literatur,0.003878
lloyd,0.006693
local,0.005879
locat,0.012766
london,0.003654
long,0.005636
longer,0.003579
loss,0.007593
lost,0.003772
low,0.013306
lower,0.019244
lowest,0.005162
ltdthe,0.013135
luxembourg,0.014757
lyman,0.009615
m,0.009312
magnet,0.076429
magnetospher,0.026675
main,0.002747
mainli,0.003603
major,0.004605
make,0.002207
malvern,0.010246
mani,0.007847
mar,0.005939
marconi,0.018759
mass,0.007119
mathemat,0.003412
mathematician,0.005312
mauric,0.006472
maximum,0.014159
mcnamara,0.009573
mean,0.002390
measur,0.039800
mechan,0.012821
medium,0.004948
mention,0.004202
mesospher,0.018832
messag,0.005394
meter,0.005841
method,0.008064
mf,0.009379
mhz,0.091153
mi,0.114927
middl,0.003578
midlatitud,0.028975
mile,0.005045
militari,0.003511
millston,0.023426
minut,0.010361
mode,0.009772
model,0.024332
modif,0.005159
modifi,0.004235
molecul,0.028120
molecular,0.004536
moleculartoatom,0.013135
monitor,0.004693
month,0.007669
mors,0.007912
motion,0.004379
mtorologi,0.013135
muf,0.026270
multipl,0.006676
n,0.019489
nagi,0.009658
nanometr,0.009796
nasa,0.006553
natur,0.006951
nd,0.008076
near,0.007137
nearbi,0.005341
need,0.002609
neg,0.003763
network,0.007257
neutral,0.033255
new,0.001938
newfoundland,0.016160
night,0.037267
nitric,0.008356
nm,0.028184
nobel,0.005052
nonhomogen,0.010537
nonsi,0.011009
noon,0.008269
normal,0.013838
northern,0.008115
note,0.002502
nova,0.006542
number,0.006514
o,0.009154
object,0.002967
obliqu,0.008568
observ,0.014323
observatori,0.020133
obtain,0.006747
occur,0.022060
occurr,0.005614
older,0.004554
oliv,0.012308
onli,0.009989
onward,0.005502
open,0.005942
oper,0.010952
optic,0.015664
orbit,0.005504
order,0.002391
ordinari,0.004733
origin,0.004810
oscil,0.025124
overview,0.004349
overwhelm,0.005819
owe,0.005282
oxfordshir,0.009454
oxid,0.005758
oxygen,0.015853
ozon,0.007616
pai,0.008791
paramet,0.015011
particl,0.013996
particular,0.002716
particularli,0.003043
pass,0.010115
passiv,0.005980
path,0.013202
pca,0.023778
pdf,0.005023
peak,0.014288
penetr,0.034788
penticton,0.013135
peregrinu,0.010246
period,0.005157
permit,0.004420
persist,0.009842
perturb,0.013492
peter,0.003957
phase,0.004173
phenomenon,0.004470
photon,0.012684
physic,0.014200
physicist,0.015412
piggott,0.013135
pl,0.015879
place,0.002454
planck,0.006589
plane,0.005227
planet,0.024622
planetari,0.006222
plasma,0.082682
play,0.006267
point,0.007553
polar,0.031903
poldhu,0.011251
pole,0.012024
popular,0.003105
portion,0.004361
posit,0.007716
possibl,0.002538
postul,0.005355
power,0.010188
pp,0.004057
practic,0.002662
precipit,0.011446
predict,0.010925
predominantli,0.005525
present,0.012443
press,0.008787
prevail,0.005265
previous,0.003794
primari,0.003285
primarili,0.013608
prize,0.004641
probe,0.006189
process,0.014117
produc,0.012948
product,0.005188
profil,0.005373
program,0.006261
progress,0.003392
project,0.009238
propag,0.083183
properti,0.012053
proport,0.004115
propos,0.015353
proton,0.032171
provid,0.002208
publ,0.009454
publish,0.002762
puerto,0.007159
puls,0.006632
purpos,0.003237
pursu,0.004526
qualit,0.005770
quantiti,0.004053
quiet,0.007290
r,0.006661
radar,0.069480
radiat,0.064798
radio,0.295528
radioelectr,0.012130
radiofrequ,0.009749
ran,0.005571
rang,0.011950
rapidli,0.008392
rare,0.008191
ratcliff,0.012394
rate,0.006168
ratio,0.004488
rawer,0.039406
ray,0.010783
reach,0.019247
reason,0.002832
recal,0.006104
receiv,0.021110
recept,0.012494
recombin,0.052507
reduc,0.005980
reduct,0.009083
refer,0.006313
reflect,0.033921
refract,0.064936
region,0.039572
rel,0.002830
relat,0.004256
releas,0.015779
reli,0.003803
remain,0.005221
requir,0.002484
reradi,0.023085
research,0.023046
reson,0.006041
respond,0.004257
respons,0.008213
result,0.015391
retriev,0.004689
return,0.003194
revers,0.004345
rico,0.007453
robert,0.006941
rocket,0.006705
role,0.002765
rotat,0.005225
rough,0.006259
roy,0.006472
rule,0.002881
russian,0.004621
rutherford,0.007751
s,0.004477
santin,0.013135
satellit,0.049566
scale,0.003572
scatter,0.046632
schunk,0.010537
scienc,0.007964
scientist,0.007218
scotia,0.008187
scottish,0.006059
season,0.010201
seen,0.003097
seri,0.006289
seriesalpha,0.012718
set,0.002445
sever,0.011450
shape,0.003762
sheet,0.005708
shell,0.005312
shine,0.007453
short,0.003365
shorter,0.005619
shortwav,0.026997
shown,0.015226
si,0.006907
sid,0.019406
signal,0.068127
signific,0.002804
significantli,0.008102
similar,0.002623
simpli,0.003584
simultan,0.004603
sin,0.012494
sinc,0.013180
sine,0.007727
situ,0.007798
sixti,0.006682
skip,0.008095
sky,0.006247
skywav,0.012394
slough,0.010313
small,0.005376
smaller,0.003735
socal,0.008865
soft,0.005608
solar,0.115283
solardriven,0.013135
solarterrestri,0.021803
soleil,0.010901
sondr,0.013135
soon,0.004028
sophist,0.005054
sounder,0.009896
sourc,0.005526
southern,0.008154
space,0.010160
spacecraft,0.006927
sparkgap,0.011125
speci,0.004040
special,0.002747
specifi,0.013399
spectrum,0.005194
spiral,0.006722
sponsor,0.005507
spontan,0.005677
sporad,0.028041
sporadic,0.026270
springer,0.005317
springerverlag,0.006824
sq,0.006558
sqrt,0.007090
st,0.007082
standard,0.005917
star,0.004871
start,0.002780
state,0.001922
station,0.014397
statist,0.003604
stay,0.004808
storm,0.024616
stratospher,0.017095
strength,0.008911
stretch,0.005537
strike,0.004886
stromfjord,0.013135
strong,0.003192
structur,0.007700
studi,0.009286
subsequ,0.003428
success,0.002913
successor,0.004934
sudden,0.012579
suffici,0.008122
suggest,0.003075
summer,0.020328
summertim,0.023085
sun,0.043860
sunlight,0.006849
sunlit,0.010620
sunset,0.008321
sunspot,0.036835
super,0.007128
superdarn,0.026270
support,0.002611
surfac,0.015910
surround,0.004022
surveil,0.006934
sweep,0.006728
syncom,0.013135
t,0.003822
tec,0.034169
techniqu,0.010127
telescop,0.013503
temperatur,0.013323
temporari,0.005180
term,0.004151
terr,0.009703
terrestri,0.005980
tesla,0.008411
test,0.003591
tether,0.018170
thank,0.006041
theoret,0.003602
theori,0.005149
therefor,0.002985
thermal,0.005633
thermospher,0.019064
thi,0.042721
thought,0.003113
thousand,0.003874
thu,0.007704
tidal,0.007912
time,0.013605
tip,0.006127
titan,0.007659
togeth,0.006058
took,0.003431
topic,0.003485
topsid,0.024789
total,0.012260
trace,0.008424
transatlant,0.015549
transceiv,0.010313
transcontinent,0.008917
translat,0.003781
transmiss,0.021707
transmit,0.043060
transmitt,0.015085
transpolar,0.013135
travel,0.003878
tropospher,0.008791
trough,0.008721
true,0.003556
ts,0.008172
turn,0.003037
twentyyear,0.010061
twice,0.005235
typic,0.005894
uk,0.012079
ultraviolet,0.034312
understand,0.005963
union,0.003264
uniqu,0.003745
unit,0.002215
uniti,0.009657
univers,0.004550
universit,0.008374
unlik,0.003658
unreach,0.010120
unstabl,0.005860
unusu,0.005326
updat,0.005321
upper,0.017744
upward,0.006115
uranu,0.008891
ursi,0.026270
usabl,0.006740
use,0.025130
useless,0.007198
usual,0.010574
util,0.003974
uv,0.022418
v,0.011751
vari,0.006476
variat,0.012353
varieti,0.006596
veloc,0.011553
venu,0.006649
veri,0.012543
verlag,0.007053
vertic,0.017049
vhfoper,0.013135
virtual,0.004344
vitali,0.009416
vlf,0.013135
volland,0.013135
w,0.007371
wa,0.023415
war,0.003120
watsonwatt,0.024789
wave,0.146197
wavelength,0.033273
way,0.002348
weaken,0.005502
weaker,0.006255
whistler,0.010537
wide,0.005658
wilk,0.009274
william,0.003520
wilson,0.005487
wind,0.010438
winter,0.016330
wireless,0.007120
wolfgang,0.006671
work,0.008614
world,0.002227
worldwid,0.008913
x,0.003889
xray,0.053921
y,0.004678
year,0.008868
yearli,0.006512
zigzag,0.009615
zone,0.004540
