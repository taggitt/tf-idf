ab,0.013638
abil,0.002496
abl,0.011460
abnorm,0.004516
abov,0.004646
absorb,0.003627
absorpt,0.013250
acceler,0.003490
access,0.005021
accumul,0.013924
achiev,0.002282
acid,0.051183
acquir,0.002955
acquisit,0.003923
act,0.004147
actual,0.004644
addit,0.003671
advanc,0.002241
afm,0.007155
afterward,0.003996
age,0.002272
agenc,0.002854
aggreg,0.053723
aid,0.016333
aim,0.002591
alan,0.003846
align,0.007840
allergi,0.006288
allow,0.014435
alpha,0.029402
alreadi,0.002643
alway,0.004880
alzheim,0.005809
amid,0.009624
amino,0.065757
aminoacid,0.007600
amorph,0.005950
amphipath,0.008388
amphiphil,0.007230
amplitud,0.005336
amyloid,0.061745
amyloidrel,0.009007
analysi,0.002187
andor,0.003175
anfinsen,0.008085
angl,0.020478
ani,0.003198
anoth,0.006981
antibodi,0.005291
antiparallel,0.014170
antitrypsinassoci,0.009007
anton,0.010522
apolar,0.009007
appear,0.002101
appli,0.001939
approach,0.004176
approv,0.003117
aqueou,0.021370
aris,0.002960
aromat,0.005519
arrang,0.002958
asic,0.007270
aspect,0.002384
assembl,0.005665
assign,0.003259
assist,0.010822
associ,0.003545
assum,0.027651
assumpt,0.003091
astronom,0.008171
atom,0.003172
attain,0.003673
attempt,0.002069
avail,0.002218
away,0.002764
backbon,0.020654
bacteria,0.004165
balanc,0.002871
base,0.006324
basi,0.002260
basic,0.002201
beam,0.014838
becaus,0.014823
becom,0.007006
befor,0.003737
begin,0.019201
believ,0.002262
bend,0.010230
bengt,0.007311
beta,0.053703
betasheet,0.008815
bhageerathh,0.009007
big,0.003312
bilay,0.006197
bind,0.003778
biochem,0.004498
biolog,0.007784
biosynthesi,0.005799
blood,0.010689
boil,0.004894
bond,0.043751
bovin,0.006242
break,0.003015
brian,0.004475
bridg,0.003614
bryngelson,0.009007
built,0.002727
buri,0.004326
c,0.004187
cage,0.011425
calcium,0.004816
calciumbound,0.009007
carbon,0.003571
carbonyl,0.006073
cardiomyopathi,0.007354
case,0.001832
catalyst,0.009570
catalyz,0.005285
categor,0.003725
caus,0.011735
cd,0.005044
cell,0.025212
cellular,0.008109
certain,0.006017
chain,0.058929
chang,0.004956
chaperon,0.145409
charact,0.003028
character,0.008338
characterist,0.007577
checkpoint,0.006371
chemic,0.005580
chevron,0.013741
chiral,0.006047
chosen,0.003348
chri,0.004932
ci,0.005125
circular,0.025625
circulardichro,0.009007
circularli,0.007657
class,0.002430
clear,0.002727
close,0.001960
clot,0.006462
cloud,0.008383
coagul,0.006957
coarsegrain,0.007546
coarsest,0.008000
coassembl,0.008815
codon,0.006211
cofactor,0.006112
coil,0.016976
coin,0.003034
collaps,0.012913
collid,0.004964
collis,0.004501
combin,0.008359
compar,0.002167
compart,0.005722
complement,0.004321
complet,0.006072
complic,0.006643
composit,0.003038
comput,0.019428
concentr,0.014340
condens,0.004240
condit,0.006152
conduct,0.005024
configur,0.008098
confirm,0.003159
conform,0.076859
confus,0.003298
consequ,0.002492
consist,0.001961
constant,0.002913
contain,0.012615
continu,0.001775
continuoustrajectori,0.009007
contribut,0.012907
convert,0.002987
cook,0.004132
core,0.015562
correct,0.020660
correl,0.003416
cost,0.002576
cotransl,0.008653
cours,0.002554
coval,0.005202
cow,0.005054
cpu,0.005938
creat,0.003585
creutzfeldtjakob,0.007230
critic,0.002155
crossbeta,0.018015
crowd,0.004567
crucial,0.003567
crystal,0.032894
crystallographi,0.015913
ctermin,0.007922
current,0.003752
custom,0.002963
cyru,0.006602
cystein,0.006669
cystic,0.006716
d,0.002372
data,0.002339
death,0.002567
deciph,0.005423
decreas,0.002886
degen,0.013384
degener,0.005131
degrad,0.012614
degre,0.009601
delta,0.004198
demonstr,0.002634
denatur,0.116658
densiti,0.007073
depend,0.015471
depict,0.003655
der,0.007671
deriv,0.002228
describ,0.003608
descript,0.005358
design,0.006458
detect,0.003218
determin,0.010177
develop,0.004397
devic,0.006551
diagram,0.004032
dichroism,0.043154
differ,0.012430
difficult,0.002556
diffract,0.026585
diffus,0.004004
dimension,0.018136
direct,0.003892
directli,0.002355
discern,0.004816
discov,0.002581
discret,0.003456
diseas,0.039913
disord,0.011334
dispers,0.004022
display,0.003331
disrupt,0.007553
distinct,0.002257
distribut,0.002418
disulfid,0.020449
dobson,0.007051
doe,0.005949
dogma,0.005010
domain,0.008587
dramat,0.006620
drive,0.012596
drug,0.003456
dual,0.008264
durat,0.004174
dure,0.005061
dyer,0.006790
dynam,0.013739
e,0.002375
earli,0.001764
eaton,0.006987
effect,0.005142
effici,0.011051
egg,0.004268
electr,0.002917
electron,0.005905
emerg,0.006784
emiss,0.012561
emphysema,0.007850
en,0.004336
enabl,0.002798
encephalopathi,0.007051
enclos,0.004792
endeavor,0.004652
energi,0.041995
enhanc,0.003268
ensembl,0.005084
enthalpi,0.012044
entropi,0.028733
envelop,0.005141
environ,0.018713
environment,0.002836
enzym,0.004289
equilibrium,0.022127
error,0.003355
essenti,0.007359
establish,0.003766
estim,0.002483
european,0.002197
everi,0.002208
evolutionarili,0.006371
exact,0.003366
exampl,0.006321
excess,0.003410
exchang,0.002494
excit,0.008603
exist,0.017547
exit,0.004628
expediti,0.007230
experi,0.006509
experiment,0.023579
explicit,0.003853
expos,0.014138
extern,0.005519
extracellular,0.005632
extrem,0.002531
face,0.005297
fact,0.002240
factor,0.013424
failur,0.003070
famili,0.002443
far,0.002392
fast,0.014449
faster,0.003681
fastest,0.004982
fastmix,0.018015
fastpp,0.008512
favor,0.017487
fersht,0.009007
fft,0.007718
fibril,0.029081
fibrillari,0.008388
fibrosi,0.006581
field,0.001927
figur,0.010453
final,0.006847
fix,0.002800
flow,0.005557
fluoresc,0.061703
fold,0.526313
folded,0.009007
foldinghom,0.015315
foldit,0.008512
follow,0.001509
forc,0.018912
form,0.030288
format,0.030883
formul,0.003021
fourier,0.009848
fraction,0.003693
free,0.008621
freedom,0.002978
freeenergi,0.008085
frequenc,0.003606
frequent,0.002652
frustrat,0.009680
ftir,0.007718
fulli,0.008534
function,0.025114
funnel,0.045502
g,0.002552
gain,0.002295
game,0.003237
gener,0.006779
geograph,0.003068
gibb,0.010634
given,0.001892
global,0.007517
globular,0.006500
good,0.002097
gpu,0.006646
gradual,0.003033
gray,0.004707
greatli,0.006180
group,0.005106
grow,0.002419
gruebel,0.009007
guanidinium,0.008815
guid,0.002667
ha,0.008807
hand,0.007014
harri,0.003968
hbond,0.016554
heat,0.006654
heavi,0.006522
heinrich,0.004682
helic,0.046477
help,0.004131
hi,0.001691
hierarch,0.004328
high,0.013374
highdimension,0.006624
higher,0.002243
highest,0.002889
highli,0.004968
hightemperatur,0.006112
histor,0.002119
homeostasi,0.005799
hour,0.003315
howev,0.012251
human,0.003635
hundr,0.002866
huntington,0.005712
hydrochlorid,0.008277
hydrogen,0.046899
hydrogendeuterium,0.009007
hydrophil,0.025924
hydrophob,0.114900
hyperthermophil,0.008085
hz,0.006481
ideal,0.003070
ident,0.005403
identifi,0.002280
idl,0.005511
ill,0.007327
immun,0.004130
implicit,0.004419
import,0.009655
improv,0.002321
inact,0.004919
includ,0.006549
inclus,0.003886
incorrect,0.013477
increas,0.009072
indic,0.006963
individu,0.007785
ineffici,0.004369
infer,0.003626
influenc,0.006234
inform,0.005627
initi,0.002050
initio,0.021255
insid,0.006329
insolubl,0.018066
instanc,0.002555
instead,0.004420
instrument,0.002905
intens,0.009401
interact,0.030605
interconnect,0.004561
interconvers,0.006816
interfac,0.008215
interferometri,0.012365
intermedi,0.029667
intermediari,0.004788
intermolecular,0.018420
interrupt,0.004693
intracellular,0.005676
intracytoplasm,0.008815
intramolecular,0.020782
intrins,0.003995
introduc,0.004426
investig,0.002644
involv,0.020121
inward,0.005382
ion,0.004091
irrevers,0.005208
isol,0.006127
isomer,0.006371
isomeras,0.016001
isomorph,0.005466
jeremi,0.004978
jo,0.004489
joseph,0.003109
jump,0.004356
just,0.002200
k,0.002860
kinet,0.025625
know,0.002858
known,0.012612
konermann,0.009007
lack,0.004810
landscap,0.043269
lar,0.006047
larg,0.011637
larger,0.007470
largest,0.002605
laser,0.004696
late,0.002184
lattic,0.018816
law,0.001900
layer,0.003631
lead,0.011293
left,0.005029
length,0.003129
level,0.011318
levinth,0.034612
light,0.007802
like,0.010109
limit,0.007706
linear,0.010069
link,0.001371
linu,0.005750
lipid,0.004968
littl,0.004828
live,0.002039
local,0.002136
locat,0.002319
london,0.002656
long,0.004096
longest,0.004522
longlast,0.005809
longtim,0.005375
loss,0.005518
low,0.002417
lower,0.002330
lowest,0.003752
lysosom,0.006716
m,0.002256
macromolecul,0.005396
mad,0.005141
magnet,0.007405
main,0.001996
mainli,0.002618
make,0.003208
mani,0.007128
manifold,0.004941
manner,0.006088
margin,0.003456
marker,0.004852
martin,0.003338
mass,0.002586
massiv,0.003356
mathemat,0.002479
maxim,0.007174
maxima,0.006520
md,0.014007
mean,0.005211
measur,0.026858
mechan,0.006988
medicin,0.003007
medium,0.003596
melt,0.013503
mere,0.003103
metal,0.003283
metast,0.006581
method,0.015628
microsecond,0.014038
millisecond,0.017886
minim,0.006658
minima,0.012743
minut,0.003765
misfold,0.102051
mislead,0.004769
mix,0.003034
mixtur,0.003943
model,0.015718
modifi,0.003077
molecul,0.054496
molecular,0.016484
monitor,0.003411
monolay,0.007155
mrna,0.005819
multipl,0.007278
multitud,0.004978
mutat,0.013139
nanosecond,0.014170
nascent,0.005291
nativ,0.101068
natur,0.005051
nd,0.002934
nearli,0.002747
necessari,0.005027
need,0.001896
neg,0.008205
neurodegen,0.013293
neutron,0.004973
nlting,0.009007
nm,0.010241
nmr,0.005712
noncrystallograph,0.009007
norm,0.003658
note,0.001818
novo,0.005973
ntermini,0.008815
nterminu,0.007230
ntl,0.009007
nuclear,0.003223
nucleat,0.006500
nucleationcondens,0.009007
nucleu,0.012535
number,0.014202
numer,0.002336
observ,0.010409
obtain,0.004903
occur,0.006012
oligom,0.016170
oligomer,0.008000
onc,0.007070
onli,0.008711
onset,0.004852
onuch,0.009007
opaqu,0.005741
oper,0.001989
opportun,0.002979
oppos,0.002636
optic,0.022767
order,0.010429
organ,0.001720
origin,0.001748
otherwis,0.009001
outsid,0.002406
outward,0.004856
overal,0.005773
pack,0.004602
pand,0.008277
paper,0.002602
paradox,0.008067
parallel,0.013073
parkinson,0.005750
partial,0.005803
particular,0.003948
pass,0.004900
pathway,0.063542
pattern,0.008073
paul,0.002741
peptid,0.010559
peptidylprolyl,0.009007
perform,0.004434
perhap,0.002932
person,0.002064
petaflop,0.008176
peter,0.002876
ph,0.014989
pharmaceut,0.004297
phase,0.015166
phe,0.008512
phenomenon,0.006497
phenylalanin,0.006957
phi,0.010327
photochem,0.006371
physic,0.006192
picosecond,0.007546
place,0.001783
pleat,0.032340
plot,0.015463
point,0.003659
polar,0.003864
polaris,0.011660
polym,0.004740
polymer,0.005650
polyneuropathi,0.008176
polypeptid,0.107743
portion,0.012678
posit,0.001869
possibl,0.014759
postmitot,0.008388
potenti,0.002341
power,0.001851
precipit,0.008318
predict,0.015879
prematur,0.005147
presenc,0.016411
present,0.001808
pressur,0.002722
prevent,0.005171
primari,0.007162
primarili,0.002472
principl,0.004342
prionrel,0.009007
prior,0.002731
probe,0.004498
problem,0.003868
proce,0.004403
process,0.034198
produc,0.007527
profil,0.007809
project,0.006714
prolin,0.007119
proper,0.009735
properti,0.006570
propos,0.002231
proteasom,0.007657
protect,0.002410
protein,0.679787
proteolysi,0.028340
proteom,0.006168
proteopathi,0.009007
proton,0.004676
prove,0.002791
provid,0.009628
psi,0.005504
publish,0.002007
pure,0.002860
pursuit,0.004078
quantum,0.014592
quaternari,0.028564
radford,0.007495
ramachandran,0.006987
random,0.013801
rang,0.004342
rapid,0.003134
rapidli,0.006098
rate,0.008966
ratedetermin,0.007600
reach,0.002331
reaction,0.008824
read,0.001992
real,0.002380
realli,0.003690
realtim,0.004808
reason,0.002058
recent,0.013904
recogn,0.002459
reduc,0.008692
refer,0.004588
reflect,0.002465
refold,0.041941
regard,0.002200
region,0.006162
regular,0.003142
relat,0.009280
relev,0.006000
reli,0.002764
remain,0.009486
render,0.007524
replac,0.004675
reproduc,0.003891
requir,0.003611
research,0.003721
resembl,0.003618
residu,0.022139
residuespecif,0.009007
resist,0.002972
resolut,0.007018
resolv,0.003332
reson,0.004390
respect,0.002181
restrict,0.007976
result,0.006391
reveal,0.003017
review,0.002570
ribosom,0.016873
rich,0.003289
right,0.004117
rise,0.002308
risk,0.002822
roder,0.007446
role,0.006028
rosettahom,0.009007
rotat,0.003797
roughli,0.006435
rout,0.003453
routin,0.008235
s,0.004880
saddl,0.011085
salt,0.003930
sampl,0.013674
say,0.004970
scale,0.005192
scatter,0.004236
scientist,0.005246
search,0.005872
secondari,0.039199
seek,0.005148
select,0.002494
sensit,0.003694
sensor,0.004754
sequenc,0.031846
sequenti,0.004968
seri,0.002285
server,0.005423
sever,0.001664
sh,0.005872
shape,0.002734
shaw,0.005416
shear,0.010778
sheena,0.008388
sheet,0.053935
shell,0.003861
shock,0.003921
shoot,0.004864
shown,0.016598
sidechain,0.007311
signal,0.009902
signific,0.002037
silico,0.006389
similar,0.003812
similarli,0.003024
simpl,0.002536
simpli,0.002605
simul,0.027751
sinc,0.001596
singl,0.014955
singledomain,0.009007
singlemolecul,0.008000
size,0.002486
slant,0.006520
slow,0.009888
slower,0.004586
slowest,0.006540
small,0.003907
smallsiz,0.007850
socal,0.003221
solut,0.019119
solvent,0.020567
sometim,0.002078
space,0.004922
specif,0.007640
specifi,0.003245
spectroscopi,0.047736
spiral,0.004885
spongiform,0.007085
spontan,0.016504
src,0.007922
stabil,0.041683
stabl,0.009425
standard,0.002150
stanford,0.003708
state,0.043303
step,0.013535
stereoisom,0.007850
stimulu,0.004676
stop,0.006308
storag,0.003799
strength,0.003237
stress,0.006547
stretch,0.004024
strictli,0.003765
strong,0.002319
stronger,0.003843
structur,0.124975
studi,0.032056
subangstrom,0.009007
subsequ,0.002491
subunit,0.010724
suggest,0.002235
suitabl,0.003544
summar,0.004066
supercomput,0.005703
supersatur,0.007922
supersecondari,0.008653
support,0.001898
surfac,0.002890
surfacebas,0.008512
surround,0.005847
synthes,0.012446
synthesi,0.003564
tafamidi,0.009007
techniqu,0.029441
temperatur,0.035501
tend,0.005243
tendenc,0.003584
term,0.003017
tertiari,0.048281
tetramer,0.008388
themselv,0.007104
theoret,0.002617
theori,0.003742
therapi,0.004066
therefor,0.017359
thermal,0.008187
thermodynam,0.028684
thi,0.026270
think,0.002768
thought,0.004525
threedimension,0.030695
thu,0.007465
tightli,0.005001
time,0.009887
timeresolv,0.007600
tissu,0.003857
tm,0.006060
togeth,0.008805
tool,0.005112
topolog,0.004392
total,0.002227
toxic,0.008828
tran,0.004898
transform,0.005226
transit,0.022235
translat,0.005496
transport,0.002736
transthyretin,0.017024
treatment,0.002936
trigger,0.003883
trp,0.046692
tryptophan,0.006646
turn,0.006621
turnov,0.005131
tweezer,0.035258
twodimension,0.005141
type,0.001919
typic,0.006425
tyr,0.033553
tyrosin,0.006624
ultrafast,0.007155
umbrella,0.004754
unambigu,0.005396
understand,0.004334
undesir,0.004991
unfold,0.115487
univers,0.001653
unobtain,0.007718
unstabl,0.008518
unwant,0.010634
urea,0.005760
use,0.027394
usual,0.003842
util,0.002888
valu,0.014323
van,0.006984
vari,0.002353
variabl,0.002918
variant,0.003867
variat,0.005985
varieti,0.002397
variou,0.005452
vastli,0.004991
vcd,0.015845
veri,0.014585
vibrat,0.009317
vijay,0.007051
visualiz,0.008512
vital,0.003565
vitro,0.005519
vivo,0.017138
volunt,0.004400
von,0.003237
vwf,0.026446
vyndaqel,0.009007
wa,0.005236
waal,0.011300
water,0.029552
wavelength,0.019344
way,0.006828
welldefin,0.004758
wherea,0.002770
whi,0.002937
white,0.003031
wide,0.002056
willebrand,0.009007
william,0.002558
wish,0.003529
wolyn,0.009007
word,0.002216
work,0.003130
xray,0.034832
year,0.001611
yield,0.009356
