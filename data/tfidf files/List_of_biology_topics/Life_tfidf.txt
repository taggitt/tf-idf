abandon,0.005539
abil,0.020399
abiogenesi,0.010789
abl,0.014982
abolish,0.006196
absenc,0.005451
abund,0.018040
acceler,0.005703
accept,0.007445
accid,0.007148
accord,0.014825
account,0.010477
accumul,0.005688
acetylcoa,0.011273
achiev,0.003730
acid,0.071692
acknowledg,0.005717
acquir,0.004829
acronym,0.008982
activ,0.017752
ad,0.003784
adapt,0.023609
addit,0.005999
adenin,0.019934
adhes,0.008898
adjac,0.013573
adopt,0.008184
adult,0.005805
advanc,0.003662
affect,0.007749
afterlif,0.009070
age,0.007427
agent,0.015167
ago,0.057458
aim,0.004234
air,0.004555
algebra,0.006367
alien,0.006948
aliv,0.037046
allow,0.008846
alongsid,0.006167
alreadi,0.004319
altern,0.022861
ambient,0.008617
ambigu,0.006967
america,0.004155
american,0.003359
amino,0.038377
amphibian,0.008752
anabol,0.022365
anatom,0.007909
anatomist,0.009381
ancestor,0.019119
ancient,0.017185
ani,0.018293
anim,0.078859
anima,0.010384
animalia,0.011182
announc,0.004979
anoth,0.014260
antarctica,0.008840
anticip,0.006595
antiparallel,0.011578
apart,0.005434
appar,0.010124
apparatu,0.007379
apparit,0.011419
appeal,0.005522
appear,0.010303
applic,0.003541
approach,0.006825
appropri,0.004753
aqueou,0.008730
arbitrari,0.006403
archaea,0.037164
archaean,0.011634
area,0.002945
argu,0.011834
arguabl,0.007226
aris,0.024187
aristotl,0.048902
arm,0.004728
aromat,0.009019
aros,0.011777
arrang,0.019336
artifici,0.040059
artisan,0.008180
asexu,0.009544
aspect,0.019486
assembl,0.004629
assemblag,0.009083
associ,0.002896
assum,0.004107
asteroid,0.008568
atmospher,0.023235
atom,0.015555
atomist,0.009381
atp,0.008851
attach,0.011227
attempt,0.030441
attribut,0.009219
august,0.004505
australia,0.010308
author,0.003338
autocatalyt,0.011880
autonom,0.005995
autopoiet,0.012248
averi,0.010622
avoid,0.004429
backbon,0.025314
bacteria,0.040844
bang,0.007990
base,0.046507
basi,0.003693
basic,0.017985
bc,0.021159
bear,0.005549
becam,0.003268
becaus,0.034989
becom,0.002862
befor,0.006107
began,0.007140
begin,0.013945
begun,0.005684
behavior,0.004194
belief,0.033503
believ,0.014792
belt,0.007794
beneath,0.007728
bergson,0.010559
bernal,0.011578
bichat,0.012946
big,0.005413
bilay,0.010127
billion,0.081257
billionyearold,0.034257
bind,0.006174
binomi,0.016701
bio,0.009444
biochem,0.022053
biochemistri,0.014213
biodivers,0.007409
biogen,0.010687
biogenesi,0.012018
biogeochem,0.009967
biolib,0.013910
biolog,0.182342
biologist,0.013379
biomolecul,0.017749
biophys,0.007929
biophysicist,0.022029
biophysiolog,0.013707
biopoesi,0.013526
biopolym,0.010412
biospher,0.074542
biosynthesi,0.009477
biota,0.028286
biotechnolog,0.015577
biotic,0.008549
bird,0.006103
block,0.005326
blood,0.023290
bloodless,0.009351
bodi,0.010763
bodiesmay,0.013074
bombard,0.015872
bond,0.015320
bone,0.006706
book,0.003398
borderlin,0.010079
bound,0.005349
boundari,0.004991
branch,0.003802
break,0.004928
broad,0.004802
build,0.011350
built,0.004457
bulk,0.006338
byproduct,0.007936
c,0.010265
calcul,0.004562
came,0.007830
camouflag,0.010687
canada,0.005129
capabl,0.028293
carbohydr,0.008438
carbon,0.023345
carbonbas,0.010034
carbonrich,0.012168
care,0.004498
carl,0.011957
carri,0.007720
case,0.002994
catabol,0.022278
catalyt,0.018382
categor,0.006087
categori,0.022846
caus,0.019177
causal,0.006667
ceas,0.006048
cech,0.012612
cell,0.298708
cellular,0.013252
centiped,0.011096
central,0.003356
centuri,0.027675
cephalopod,0.010412
certain,0.019665
chain,0.010699
challeng,0.021388
chang,0.021598
character,0.004541
characterist,0.016510
charl,0.004586
chemic,0.095755
chemist,0.006882
chemistri,0.015745
chemotaxi,0.011880
chicken,0.008511
chiral,0.009882
chloroplast,0.018987
chnop,0.013910
choic,0.004705
chromatin,0.010034
chromosom,0.038912
circular,0.006979
civil,0.012323
clade,0.009477
cladist,0.009989
class,0.011915
classif,0.042272
classifi,0.038753
clearli,0.005384
cleavag,0.009882
clonal,0.010754
close,0.019226
closur,0.015468
cloud,0.006849
coast,0.005465
coat,0.007094
code,0.009267
cofactor,0.009989
coil,0.009247
coloni,0.009904
combin,0.017075
commenc,0.007244
comment,0.005736
common,0.023167
commonli,0.003977
commun,0.003131
compact,0.014131
compar,0.003542
comparison,0.010044
competit,0.004709
compil,0.006179
complet,0.009923
complex,0.063083
compli,0.007896
compon,0.041246
compos,0.027263
composit,0.009930
compound,0.032381
compris,0.004899
comput,0.007937
concept,0.006684
conceptu,0.005764
concern,0.010087
conclud,0.004898
condit,0.046917
conduct,0.004104
confin,0.006430
confirm,0.015490
connect,0.003943
conscious,0.006101
consensu,0.011948
consequ,0.008145
consid,0.027932
consider,0.004025
consist,0.012822
constant,0.004761
constraint,0.011893
construct,0.003708
consum,0.004769
contact,0.005025
contain,0.010308
continu,0.002902
contract,0.004804
contradict,0.006397
contrast,0.008091
control,0.006364
controversi,0.023598
convent,0.004637
convert,0.004882
coordin,0.010154
cope,0.007687
copeland,0.010975
copi,0.023096
core,0.010172
cosmic,0.007327
cosmolog,0.007002
coval,0.017004
cover,0.003943
creat,0.020507
crick,0.018826
criteria,0.005998
critic,0.007046
crustacean,0.009510
csb,0.027415
current,0.027596
cyanobacteria,0.009882
cycl,0.009819
cystein,0.010898
cytoplasm,0.017398
cytosin,0.020606
d,0.003877
darwin,0.006702
darwinian,0.045681
data,0.007644
date,0.012009
daughter,0.013147
day,0.003614
death,0.046144
debat,0.008918
decad,0.004353
decay,0.006585
decis,0.004038
decompos,0.015528
decreas,0.004716
dedic,0.005753
deep,0.005332
deepest,0.027211
defin,0.036157
definit,0.079531
degrad,0.006871
delay,0.006117
democritu,0.009083
demonstr,0.012915
deoxyribonucl,0.021509
deoxyribos,0.011419
depend,0.009480
deriv,0.003641
descart,0.007495
descend,0.005797
descent,0.013742
describ,0.020636
descript,0.013134
desicc,0.010654
design,0.007035
desir,0.004507
destin,0.006702
detect,0.005259
determin,0.009978
detriment,0.007870
detritivor,0.011578
detritu,0.010898
develop,0.011977
dictat,0.006849
die,0.009178
diet,0.007082
differ,0.007617
difficult,0.012532
diffract,0.008688
diffus,0.006543
digit,0.005348
dilthey,0.011227
direct,0.009541
directori,0.008039
disciplin,0.004413
discov,0.016871
discoveri,0.004485
discredit,0.008333
discuss,0.007816
diseas,0.005017
dispatch,0.008203
dispel,0.010720
dispers,0.006573
disproof,0.010975
disprov,0.008275
distinct,0.003688
distinguish,0.017009
distribut,0.003951
disturb,0.006725
divers,0.013819
diversifi,0.007253
divid,0.022788
divin,0.012740
divis,0.029058
dna,0.166900
dnalik,0.013910
document,0.004624
doe,0.009722
dog,0.007170
domain,0.004677
domin,0.003941
doubl,0.005302
doublestrand,0.020553
drake,0.020159
dramat,0.005409
draw,0.004909
duplic,0.007857
dure,0.019299
dust,0.015639
dutrochet,0.013910
dwarf,0.008699
dynam,0.031433
ear,0.007652
earli,0.011536
earlier,0.004261
earliest,0.014459
earth,0.212774
earthlik,0.036998
easili,0.004940
echinoderm,0.010861
ecolog,0.021736
ecosystem,0.065405
edg,0.006291
effect,0.002801
effici,0.004515
effort,0.003993
egg,0.006975
elabor,0.012000
element,0.047676
elimin,0.004986
emerg,0.022174
empedocl,0.010079
enabl,0.013719
enclos,0.007832
encod,0.014044
encompass,0.005398
encourag,0.004739
encyclopedia,0.004966
end,0.009458
endocrin,0.009219
endoplasm,0.010412
endosymbiosi,0.010975
energi,0.048442
engin,0.008311
enhanc,0.010682
entail,0.006729
entir,0.007480
entiti,0.004892
entri,0.005577
entropi,0.015651
environ,0.072627
environment,0.018543
environmentsseek,0.014140
enzym,0.014020
enzymecatalyz,0.013074
eon,0.009861
epicuru,0.009667
epigenet,0.009276
epoch,0.016470
equat,0.019183
equilibrium,0.006026
ernst,0.006990
error,0.005483
erwin,0.008709
especi,0.003348
essenti,0.016034
estim,0.036520
etern,0.014580
eugen,0.007049
eukaryot,0.087371
event,0.003752
eventu,0.016393
everi,0.007216
everyth,0.017226
evid,0.015728
evolut,0.037455
evolutionari,0.045450
evolv,0.033132
exactli,0.005649
examin,0.013539
exampl,0.012912
excel,0.006238
exchang,0.004076
exhibit,0.005443
exist,0.040144
expand,0.008158
expans,0.009730
expens,0.005150
experi,0.014184
explain,0.038818
explan,0.020304
exploit,0.010735
explor,0.008787
exposur,0.012817
express,0.011208
extant,0.007630
extend,0.011410
extens,0.003893
extern,0.006764
extinct,0.052443
extraterrestri,0.044972
extrem,0.012409
extremophil,0.033548
fabric,0.007213
factor,0.007312
faith,0.005764
famili,0.003992
far,0.003910
father,0.005069
favor,0.004762
featur,0.011699
feedback,0.014004
feel,0.005390
feet,0.014083
field,0.006298
fieri,0.010687
fish,0.005380
fission,0.008752
fivekingdom,0.012332
flea,0.010529
floor,0.014028
flow,0.009082
flux,0.015171
focu,0.004230
follow,0.012333
food,0.008875
forc,0.015453
forgotten,0.008142
form,0.108418
format,0.037850
formul,0.014813
forward,0.005473
fossil,0.060149
fossilcontain,0.013910
foundat,0.004020
fourth,0.005380
fox,0.007942
francesco,0.009057
franci,0.011810
franklin,0.007053
franoi,0.007624
free,0.007044
freez,0.007990
french,0.004149
friedrich,0.018920
ft,0.026957
function,0.059982
fundament,0.023691
fungi,0.044786
futur,0.011266
g,0.012514
ga,0.010375
gaia,0.030102
galaxi,0.023026
garbag,0.009882
gave,0.004486
gene,0.047864
gener,0.042098
genesfirst,0.014140
genet,0.047136
genu,0.007788
geochem,0.009305
geolog,0.023570
geometri,0.006288
georg,0.004444
geospher,0.010825
gerald,0.007997
germ,0.008317
giant,0.013485
gkpid,0.013910
global,0.012284
glycerol,0.021181
goal,0.008482
goaldirected,0.013910
golgi,0.010303
got,0.006956
govern,0.002932
gradient,0.007336
gradual,0.004956
grandest,0.012612
graphit,0.009578
graviti,0.006722
greater,0.007767
greec,0.005828
greek,0.013099
greenland,0.008957
grier,0.012946
group,0.027814
grow,0.015815
growth,0.007805
guanin,0.020769
guid,0.008719
ha,0.041121
habit,0.026150
habitat,0.007191
habitatdamag,0.014140
haeckel,0.009667
harold,0.007399
health,0.004298
heat,0.010874
heavi,0.010658
heavier,0.007788
held,0.007291
helix,0.009305
helmholtz,0.009335
help,0.003375
henc,0.019244
henri,0.009939
hered,0.008806
hereditari,0.007379
hermann,0.007537
hi,0.022117
high,0.009366
higher,0.014662
highlight,0.006349
histon,0.010303
histor,0.003463
histori,0.012841
hold,0.003714
holocen,0.009667
homeopathi,0.011096
homeostasi,0.028432
host,0.022553
hot,0.006790
howev,0.030029
human,0.008912
hutton,0.019054
hybrid,0.006442
hydrocarbon,0.007976
hydrogen,0.012773
hydrospher,0.009261
hydrotherm,0.009967
hylomorph,0.045093
hypothes,0.018394
hypothesi,0.064451
hypothet,0.013867
ice,0.006725
idea,0.020764
ident,0.008830
identifi,0.014909
imit,0.007294
immens,0.007409
imperfect,0.007591
imperfectli,0.010225
implic,0.005372
import,0.010519
imposs,0.005213
improv,0.003794
inanim,0.018438
includ,0.029967
increas,0.002965
independ,0.003167
indirectli,0.006644
individu,0.009541
induc,0.006105
inert,0.008720
inevit,0.006793
inferior,0.007505
influenc,0.003396
inform,0.030655
inher,0.005909
initi,0.003350
inject,0.007613
inner,0.006198
inorgan,0.036045
input,0.005586
inquiri,0.006150
insect,0.021019
insid,0.020686
instantan,0.008090
instead,0.003611
instruct,0.011610
integr,0.003721
intellig,0.004811
intens,0.005121
interact,0.015388
interdepend,0.007740
interior,0.006051
intermedi,0.006060
intermediari,0.007825
intern,0.010808
interphas,0.011320
interplay,0.017060
interpret,0.004113
interstellar,0.008945
interv,0.012992
intoler,0.008982
introduc,0.010851
invertebr,0.008188
investig,0.008641
involv,0.005978
isbn,0.007290
isol,0.005006
issu,0.003308
j,0.003872
jame,0.017395
januari,0.004400
japan,0.005099
john,0.007014
join,0.009172
joyc,0.009705
juli,0.008907
juliu,0.007844
just,0.003595
justu,0.010225
juxtacrin,0.013910
kauffman,0.043446
key,0.003923
kind,0.012321
kingdom,0.020506
km,0.011606
knowledg,0.003878
known,0.028339
laboratori,0.010463
lack,0.007861
larg,0.013584
larger,0.004069
late,0.010707
later,0.012263
latin,0.004845
layer,0.005934
lead,0.003075
leav,0.004365
led,0.009981
leftov,0.010898
legaci,0.006481
length,0.005114
level,0.009247
lichen,0.010127
liebig,0.010384
life,0.453990
lifedistribut,0.013074
lifeprincipl,0.013362
like,0.024781
likelihood,0.014297
linger,0.009366
link,0.002241
linnaeu,0.036281
lipid,0.016239
list,0.002866
lithospher,0.009397
littl,0.003945
live,0.143323
locat,0.007580
lock,0.006676
logic,0.004989
long,0.010041
longer,0.008503
look,0.008512
lost,0.004479
loui,0.005816
lovelock,0.011096
low,0.007901
lower,0.003809
luca,0.016486
luminos,0.010559
lysosom,0.010975
m,0.014746
machin,0.009954
macromolecul,0.008817
macronutri,0.012332
macroscop,0.014769
magnet,0.006051
main,0.006526
mainli,0.008558
mainsequ,0.012248
maintain,0.028754
mainten,0.006183
major,0.008204
make,0.020970
malnutrit,0.009461
mammal,0.006525
man,0.004560
mani,0.044267
manipul,0.005519
manmad,0.008283
manyword,0.014140
map,0.004715
mar,0.007053
march,0.004274
mari,0.005955
mariana,0.019054
martian,0.010225
martin,0.005456
mass,0.016909
massiv,0.005484
mat,0.009320
materi,0.036121
materialist,0.008333
mathemat,0.004052
matter,0.065916
mayer,0.009667
mean,0.008516
meant,0.005286
mechan,0.026645
mechanist,0.025793
medic,0.004746
member,0.009866
membran,0.043796
membranebound,0.021119
mere,0.010141
metabol,0.061487
metabolismfirst,0.013074
metasedimentari,0.011578
meteorit,0.028841
meteoroid,0.011753
methionin,0.010898
method,0.003192
mi,0.038995
mice,0.008385
microb,0.017525
microbi,0.051469
microbiolog,0.016165
microenviron,0.011634
microfossil,0.022738
microorgan,0.061589
microscop,0.006654
microst,0.010469
middl,0.004249
miescher,0.012946
millennia,0.015021
miller,0.007196
millerurey,0.023269
million,0.024292
minimum,0.005622
minor,0.004770
mirror,0.006960
mitochondria,0.018671
mitosi,0.009613
mixtur,0.006444
model,0.019265
modelbuild,0.012092
modern,0.009275
molecul,0.077922
molecular,0.053876
mollusc,0.009428
moment,0.017593
monera,0.011880
month,0.009108
moon,0.020138
morowitz,0.013362
mostli,0.004397
motion,0.010402
movement,0.003554
mud,0.009006
multiag,0.010079
multicellular,0.050957
multipl,0.007929
muscl,0.014297
mutat,0.007157
mutual,0.005437
nadh,0.011369
narrow,0.005820
nasa,0.015564
natur,0.033021
natura,0.009291
nearli,0.004489
necessari,0.012323
necessarili,0.005045
need,0.012394
neg,0.008939
nervou,0.007057
network,0.008618
new,0.025326
nietzsch,0.008709
nineteenth,0.006447
nitrogen,0.014230
nitrogencontain,0.012168
nomenclatur,0.014635
nonbiolog,0.010936
noncellular,0.012716
noncod,0.010303
nonfraction,0.028280
nonlif,0.011320
nonliv,0.017377
nonmateri,0.010329
normal,0.004108
northwestern,0.007825
note,0.002971
nourish,0.009461
novel,0.011409
nuclear,0.005266
nuclei,0.007414
nucleic,0.033861
nucleobas,0.023156
nucleobaseeith,0.014140
nucleotid,0.041257
nucleu,0.013656
number,0.007736
nutrient,0.007313
nuvvuagittuq,0.012946
obey,0.007355
observ,0.010205
occur,0.019649
ocean,0.042434
old,0.012794
older,0.005409
oldest,0.016728
onc,0.003851
onethousandth,0.024184
ongo,0.005640
onli,0.021354
open,0.010586
oper,0.009755
opportun,0.009737
opposit,0.008482
optim,0.010934
orbit,0.006537
order,0.011362
ordergener,0.028280
ordinari,0.005620
organ,0.247419
organel,0.035977
organis,0.004880
organismsoften,0.014140
origin,0.034279
outer,0.026343
outsid,0.011796
ovipar,0.011816
oxygen,0.018827
pah,0.011320
pair,0.016479
panspermia,0.012248
parent,0.023220
partial,0.009483
past,0.012267
pasteur,0.008933
path,0.005226
pathway,0.006922
pattern,0.008795
perceiv,0.005183
percent,0.015530
perform,0.003623
period,0.009188
perman,0.009990
persist,0.005844
person,0.003372
perspect,0.004612
phenomena,0.034450
philosoph,0.031670
philosophi,0.008777
phosphat,0.031879
phosphit,0.026424
phospholipid,0.009903
phosphoru,0.025478
phosphoryl,0.010079
photosynthesi,0.008219
phototrop,0.012421
phyla,0.010469
phylogenet,0.008149
physic,0.033728
physicalchem,0.012018
physicist,0.006101
physiolog,0.018214
place,0.002915
placement,0.008090
planet,0.035089
planetari,0.014779
plant,0.058177
planta,0.011369
plato,0.006790
plausibl,0.015409
point,0.002989
poison,0.007204
polar,0.006314
polycycl,0.011014
polym,0.015492
polynucleotid,0.023269
popular,0.003687
portion,0.005179
pose,0.006278
posit,0.003054
possess,0.013896
possibl,0.018088
postul,0.006359
potenti,0.026787
prebiot,0.022641
precellular,0.013362
precipit,0.006797
precis,0.009737
precursor,0.032196
predat,0.006561
predecessor,0.006444
predict,0.008649
preexist,0.008046
prefer,0.004526
prepar,0.009587
presenc,0.004469
present,0.005910
preserv,0.014945
previous,0.004505
primari,0.011705
primit,0.006540
primordi,0.008308
principl,0.007096
prior,0.013393
probabl,0.008482
problem,0.012644
problemat,0.007123
process,0.072650
produc,0.015377
product,0.003080
progenitor,0.009723
project,0.003657
prokaryot,0.061493
promin,0.004634
proper,0.005302
properti,0.035787
propos,0.036467
protect,0.003939
protein,0.102079
protist,0.037652
protista,0.011523
protocel,0.012421
protoctista,0.014140
protophytathallophyta,0.014140
protozoa,0.009882
provid,0.010489
pseudoscientif,0.009967
psych,0.018466
publish,0.003280
purpos,0.007689
pyrimidin,0.022278
quadrup,0.023156
quebec,0.008678
question,0.003711
quickli,0.004811
radiat,0.017758
radii,0.009723
radio,0.006051
rang,0.042577
rate,0.007325
ration,0.005093
react,0.019648
reaction,0.019227
reactiv,0.007061
read,0.003255
rearrang,0.008032
reason,0.006728
recent,0.009738
reciproc,0.014083
reconstruct,0.005874
record,0.007902
recreat,0.007240
recycl,0.007976
red,0.015587
redefinit,0.009903
redi,0.011320
reduc,0.014205
reduction,0.009461
reenter,0.009596
refer,0.005623
reflect,0.008057
region,0.006713
regul,0.004437
regular,0.005135
regulatori,0.006487
reincarn,0.009276
reject,0.004621
rel,0.003361
relat,0.040440
relationship,0.014312
releas,0.004684
religi,0.004788
religion,0.009548
remain,0.024802
remaind,0.006956
remot,0.006346
ren,0.007346
repeatedli,0.006670
replic,0.039700
report,0.025220
repres,0.006150
represent,0.004729
reproduc,0.038158
reproduct,0.031838
reptil,0.008243
requir,0.023608
research,0.006082
resembl,0.005913
resist,0.004857
resourc,0.007590
respect,0.003564
respond,0.015167
respons,0.013005
restrict,0.004344
result,0.023500
resurrect,0.008317
reticulum,0.010276
retrospect,0.008251
return,0.003793
reus,0.008597
reveal,0.009861
revis,0.011203
reviv,0.005957
rhetor,0.007258
ribonucl,0.011369
ribosom,0.018382
ribozym,0.011182
rna,0.154443
rnabas,0.012828
robert,0.012365
robot,0.006948
rock,0.035222
root,0.004735
rosalind,0.009903
rosen,0.009742
rough,0.007434
rudolf,0.007704
rule,0.003421
run,0.004119
s,0.010634
sandston,0.010329
scale,0.008485
scaveng,0.009596
schrdinger,0.008359
schreibersit,0.014140
schreibersitecontain,0.014140
schwann,0.010590
scienc,0.022069
scientif,0.026405
scientist,0.064298
scope,0.005510
scorpion,0.010898
scottish,0.007196
sea,0.009566
seab,0.010559
search,0.004798
season,0.012115
section,0.004644
sedimentari,0.009335
seed,0.006651
select,0.020381
selfassembl,0.031963
selforgan,0.018219
selfproduc,0.013362
selfregul,0.018466
selfsustain,0.020159
senesc,0.011096
sens,0.007714
sensat,0.007641
separ,0.013699
sequenc,0.026020
serv,0.011194
set,0.011615
seti,0.012018
sexual,0.006159
share,0.003652
sharpli,0.007375
shell,0.006309
short,0.007993
shorter,0.006673
shortli,0.005834
shot,0.007290
sick,0.007360
sidney,0.009031
signal,0.032362
signific,0.006660
similar,0.006230
simpl,0.029017
simpler,0.006956
simpli,0.004257
simul,0.017006
sinc,0.007826
singl,0.024439
sixkingdom,0.013212
size,0.004062
slowest,0.010687
slowli,0.006013
small,0.006384
smaller,0.004436
smallest,0.006790
smooth,0.006918
soil,0.005911
solar,0.043561
solv,0.004801
someth,0.010032
sometim,0.003396
soon,0.004784
soul,0.074095
sourc,0.006563
space,0.012066
spark,0.006904
speci,0.105558
special,0.016314
specif,0.003121
specifi,0.010608
specimen,0.008090
spider,0.009191
split,0.005361
spontan,0.047198
spot,0.013063
spring,0.005884
stabl,0.010268
stahl,0.011273
stanford,0.006060
star,0.034714
start,0.019814
starvat,0.008607
state,0.011413
statu,0.004357
stimuli,0.015967
stoic,0.009219
storag,0.006208
store,0.026427
strand,0.066951
strata,0.009057
strength,0.005291
stress,0.005349
strictli,0.006152
structur,0.036578
stuart,0.028942
studi,0.033085
subfield,0.006716
subject,0.006637
subordin,0.007200
subsequ,0.012213
substanc,0.016454
subsurfac,0.019524
success,0.003459
suffici,0.004823
sugar,0.019071
sugarphosph,0.012716
suggest,0.025566
suicid,0.007479
sulfur,0.007669
sulfurth,0.014140
sum,0.005204
sun,0.017362
sunlight,0.008134
superdomain,0.013910
superior,0.011613
supernova,0.009177
superorgan,0.011273
support,0.012407
suppos,0.005732
surround,0.004777
surviv,0.067493
sustain,0.010205
swap,0.007956
sway,0.008730
sweat,0.009444
symbiosi,0.009578
synthes,0.020339
synthesi,0.029122
synthet,0.012940
systema,0.010898
t,0.004539
ta,0.009083
taken,0.003820
talk,0.011227
taxa,0.018013
taxadomain,0.014140
taxon,0.020659
taxonomi,0.007505
taxonomicon,0.013910
teleolog,0.017419
temperatur,0.010548
templat,0.008597
tenac,0.011948
term,0.022186
termin,0.012225
test,0.004264
th,0.017948
themselv,0.011609
theodor,0.007140
theori,0.070333
therebi,0.010799
therefor,0.010638
thermodynam,0.026785
thi,0.060492
thing,0.053965
think,0.009048
thoma,0.004666
thought,0.003697
threedomain,0.011369
thrive,0.050556
thu,0.006099
thymin,0.020606
tidal,0.009397
time,0.027699
today,0.003747
togeth,0.014390
toler,0.026404
ton,0.014148
tonn,0.017255
topolog,0.007178
total,0.021840
toxic,0.007213
trace,0.015007
tradit,0.009855
trait,0.006181
transcrib,0.008773
transcript,0.007704
transform,0.004270
translat,0.004491
transmiss,0.006444
transport,0.004472
treat,0.009136
tree,0.005374
trench,0.017613
trend,0.009966
tri,0.004106
trillion,0.029087
true,0.004224
ttc,0.025893
turn,0.003606
twocompon,0.012946
type,0.021956
typic,0.010500
ulanowicz,0.013074
ultim,0.013537
ultraviolet,0.008149
unchang,0.006956
uncommon,0.007997
undergo,0.012097
underground,0.007231
underli,0.009908
understand,0.024789
undirect,0.010975
undiscov,0.009903
unequivoc,0.009649
unicellular,0.009686
unit,0.018419
univers,0.027020
unknown,0.016204
unlik,0.008689
unnecessari,0.007704
upper,0.005268
uracil,0.011227
urea,0.009413
use,0.021317
user,0.005787
usual,0.006279
util,0.004719
vacuol,0.010412
vacuum,0.006807
vari,0.007691
variabl,0.009538
variat,0.004890
varieti,0.003917
variou,0.014850
vascular,0.008597
veget,0.006228
vent,0.009191
venu,0.007896
veri,0.008938
verm,0.013074
versatil,0.009335
vertebr,0.007752
view,0.013375
viewpoint,0.013735
virchow,0.010975
viroid,0.023385
virolog,0.009742
viru,0.007949
virus,0.070207
vita,0.010529
vital,0.046612
vitalist,0.011948
vitro,0.009019
vivipar,0.011470
void,0.008350
von,0.015873
wa,0.068451
walker,0.008219
washington,0.005743
water,0.008048
watson,0.007776
way,0.002789
week,0.005483
weigh,0.014702
weltanschauungen,0.013212
western,0.011773
whale,0.008127
wherea,0.004527
whi,0.004800
white,0.004953
whittak,0.010789
whler,0.021119
wide,0.016800
wigner,0.009945
wikispeci,0.012168
wilhelm,0.006567
withstand,0.008921
word,0.003621
work,0.015345
world,0.021159
write,0.004079
writer,0.005354
x,0.018477
xavier,0.009742
xray,0.007115
year,0.044759
youngest,0.008982
zone,0.048530
zoolog,0.007876
zoophyt,0.014140
