abolish,0.003646
absolut,0.002959
absorpt,0.004247
ac,0.009504
acar,0.009179
accord,0.006979
account,0.002055
achiev,0.002195
acid,0.014062
acquir,0.002842
activ,0.001741
acycl,0.006094
ad,0.044536
ada,0.005700
add,0.003327
addict,0.021667
addictionlik,0.009179
addit,0.005295
address,0.002627
adequ,0.003660
adjust,0.003340
administr,0.002439
adolesc,0.005077
adopt,0.002407
adult,0.010248
advantag,0.002708
affect,0.002280
affin,0.004287
africa,0.008501
age,0.008741
agricultur,0.005187
aid,0.002617
aim,0.002491
air,0.002680
alcohol,0.016023
aldehyd,0.012428
alongsid,0.003629
alway,0.002346
alzheim,0.005586
amaranthacea,0.008662
america,0.004890
american,0.003954
amylas,0.007115
analysi,0.004206
ancient,0.010112
ani,0.004613
anim,0.007327
announc,0.002930
annual,0.002680
anoth,0.005034
anthoni,0.004120
antigen,0.005520
april,0.002669
arab,0.006445
arabia,0.004268
area,0.001733
argument,0.002931
arkar,0.018359
articl,0.004147
artifici,0.011786
asia,0.020972
aspartam,0.016954
aspect,0.002293
assert,0.002997
assign,0.003134
associ,0.013636
attest,0.004594
attribut,0.002712
august,0.002651
aurora,0.005657
australia,0.006066
author,0.003929
autumn,0.004642
avail,0.008532
averag,0.010425
avoid,0.002606
azcar,0.009179
bacteria,0.004005
bad,0.003579
bag,0.005416
bagass,0.008066
bake,0.036665
barberi,0.007693
barley,0.005329
barrett,0.005722
base,0.003040
bc,0.003112
beatriz,0.007774
becam,0.015385
becaus,0.004751
becom,0.001684
beet,0.060025
befor,0.005390
began,0.002100
behavior,0.002468
benefit,0.002553
bengal,0.005520
beta,0.004303
better,0.002372
beverag,0.040113
biennial,0.006144
bihar,0.006391
bing,0.006391
biopolym,0.006127
blackwel,0.004398
bleach,0.006002
blend,0.008821
block,0.003134
blockad,0.004818
blood,0.027411
bloodstream,0.005959
bobadilla,0.008662
bodi,0.012667
boil,0.009412
bond,0.015025
book,0.001999
boy,0.004146
brazil,0.018799
bread,0.004924
break,0.002900
bring,0.005231
british,0.004698
broken,0.007016
brought,0.007780
brown,0.021420
buddhist,0.004232
built,0.002622
bulk,0.014919
burn,0.007213
butter,0.005779
byproduct,0.009340
c,0.012081
cake,0.018142
calcul,0.005369
calori,0.040455
calvi,0.008185
cambridg,0.005581
came,0.002303
campaign,0.003163
canari,0.005647
candi,0.025315
cane,0.041002
capita,0.015298
caravan,0.005815
carbohydr,0.069517
carbon,0.006868
carbonat,0.027538
carbonoxygen,0.007160
carbonyl,0.005840
cardiac,0.005441
cardiovascular,0.031334
cari,0.032066
caribbean,0.004020
carri,0.004543
carrot,0.006505
case,0.003523
catarina,0.006916
caus,0.009404
caviti,0.005094
cell,0.018183
cellulos,0.016350
center,0.002247
centr,0.002897
centrifug,0.011059
centuri,0.030762
cereal,0.005195
certain,0.001928
chain,0.009444
chang,0.003177
chaptal,0.007959
characterist,0.002428
chart,0.004014
cheap,0.013097
chemic,0.005366
chemistri,0.003088
chew,0.011680
chief,0.002934
children,0.020287
china,0.013537
chines,0.006058
chlorin,0.005088
cho,0.011512
chop,0.006481
christoph,0.003693
chronic,0.008662
chronicl,0.004438
claim,0.002139
clamp,0.005815
clarifi,0.012750
classic,0.002328
classifi,0.005701
climat,0.006018
closedchain,0.018359
coars,0.005616
coast,0.003216
coat,0.012524
cocacola,0.006435
coffe,0.008797
cognat,0.005700
cognit,0.003341
collect,0.002055
colleg,0.006118
collin,0.004976
colon,0.003739
coloni,0.005828
color,0.003510
colour,0.004022
columbu,0.004818
combin,0.008038
come,0.004031
commerci,0.002700
commiss,0.005611
committe,0.003023
commod,0.003420
common,0.005112
commonli,0.004681
compani,0.002225
complex,0.002062
compon,0.007281
compos,0.002673
compound,0.006351
concentr,0.013790
condit,0.001972
conduct,0.002415
coneform,0.009179
confection,0.018359
confectionari,0.009179
confectioneri,0.028831
confirm,0.003038
conflict,0.002625
connect,0.002320
consid,0.003287
consist,0.007545
constitu,0.009901
construct,0.004365
consult,0.006509
consum,0.030870
consumpt,0.055884
contain,0.012131
contemporari,0.005513
content,0.008343
context,0.002479
continent,0.003753
continu,0.003415
contrast,0.004761
contribut,0.004137
control,0.009363
controversi,0.002777
convers,0.002907
convert,0.017238
convey,0.004255
convinc,0.003751
cook,0.015894
cooki,0.013761
cool,0.007896
corn,0.008815
cornderiv,0.009179
coronari,0.012189
correl,0.006570
correspond,0.002558
count,0.003214
countri,0.005564
cours,0.002456
crave,0.006289
cream,0.011756
creat,0.001723
crop,0.013938
crosssensit,0.009179
crown,0.003650
crunch,0.005918
crusad,0.009565
crystal,0.059310
crystallin,0.005229
cs,0.004306
cuba,0.004486
cube,0.036750
culinari,0.006661
cultiv,0.025199
cup,0.014193
customari,0.004698
customarili,0.005791
cut,0.005984
cyclic,0.004377
daic,0.009179
daili,0.003296
damag,0.003041
darkcolor,0.008888
data,0.004498
databas,0.003297
date,0.004711
day,0.010634
debat,0.002623
decay,0.015501
declin,0.008028
decor,0.004576
decreas,0.002775
defin,0.001934
definit,0.002228
degener,0.004934
degrad,0.004043
deliber,0.003772
demand,0.012581
demarara,0.009179
dementia,0.005973
densiti,0.010203
dental,0.026921
deoxyribos,0.013439
deriv,0.006429
describ,0.006939
dessert,0.013906
destroy,0.005894
deterior,0.004150
determin,0.003914
devanagarikhaa,0.009179
develop,0.005638
dextro,0.009179
dextros,0.025986
diabet,0.039895
dialect,0.004146
did,0.002092
diet,0.037508
dietari,0.025945
differ,0.014941
difficulti,0.006088
diffus,0.007701
digest,0.038756
dioscorid,0.006606
dioxid,0.004141
director,0.003388
disaccharid,0.076080
discov,0.004964
diseas,0.032476
display,0.003203
dissolv,0.017503
distantli,0.006690
distinguish,0.002502
distribut,0.002325
diverg,0.003809
divid,0.002235
dmoz,0.003733
dna,0.003637
document,0.002721
doe,0.007628
domino,0.006580
doubl,0.003120
dri,0.014766
drink,0.012539
drove,0.004565
drug,0.003324
duncan,0.005243
dure,0.014602
dust,0.013804
dv,0.025397
dysfunct,0.010084
earli,0.005091
earliest,0.005672
easier,0.003633
easili,0.005814
east,0.005217
eastern,0.002816
eat,0.007919
eaten,0.015357
econom,0.001857
edul,0.009179
effect,0.011539
effici,0.002656
elder,0.004169
emperor,0.003742
empti,0.004045
encount,0.003342
end,0.001855
energi,0.023755
engin,0.002445
england,0.002944
english,0.004726
entir,0.004402
envoy,0.005042
enzym,0.037127
enzymat,0.005657
epidemiolog,0.004804
equal,0.002310
equilibrium,0.003546
equival,0.005466
especi,0.003940
essenti,0.004717
establish,0.007243
estat,0.003612
ethanol,0.005493
ethnic,0.006697
etymolog,0.010836
eugen,0.004148
europ,0.016106
european,0.006340
evapor,0.014256
everi,0.002123
evid,0.006941
evolut,0.002755
examin,0.002655
exampl,0.001519
exceed,0.007127
excess,0.009837
exclus,0.005797
exist,0.003374
expans,0.002863
expect,0.006994
expens,0.003030
experienc,0.003046
experiment,0.008503
expert,0.003351
explos,0.007523
export,0.006225
expos,0.003399
express,0.002198
extent,0.002792
extern,0.001326
extra,0.003886
extract,0.026131
fact,0.004309
factori,0.003712
famili,0.004699
fao,0.009232
fat,0.018586
fatti,0.015626
fed,0.004540
ferment,0.009963
fertil,0.003556
feuerzangenbowl,0.009179
fiber,0.013222
fibr,0.005441
field,0.001853
figur,0.002513
filter,0.004229
final,0.002194
financi,0.002422
fine,0.007255
firmli,0.004435
fiveyear,0.004132
flame,0.005036
flammabl,0.006370
flavor,0.010364
fluid,0.003576
follow,0.005806
food,0.086179
foodstuff,0.005236
form,0.031900
formula,0.019011
free,0.029016
french,0.004883
frequenc,0.003467
fresh,0.004075
frostfre,0.008066
fructos,0.096876
fructoseglucos,0.009179
fruit,0.043149
fuel,0.003362
fulli,0.002735
fund,0.005003
g,0.007364
gain,0.002207
gal,0.006413
galactos,0.033749
gari,0.004526
gave,0.002640
gday,0.008888
gener,0.005215
gentri,0.005878
genu,0.004583
georgia,0.004337
german,0.002562
germani,0.002732
germin,0.005756
giant,0.003967
girl,0.004480
glucos,0.147233
glycem,0.055041
glyceraldehydephosph,0.007959
glycerol,0.006232
glycosid,0.020439
glyn,0.007115
gml,0.030775
goal,0.002495
gomera,0.006991
good,0.008069
govern,0.003450
governor,0.003330
gp,0.004944
grain,0.015769
gram,0.020284
grant,0.002781
granul,0.056602
granular,0.012617
grape,0.005484
grass,0.004572
great,0.004090
greek,0.007708
ground,0.002640
group,0.019641
grow,0.004653
grown,0.003494
growth,0.002296
guidanc,0.008182
guinea,0.004284
gum,0.011809
gupta,0.005791
gut,0.005202
h,0.015123
ha,0.020568
half,0.004995
halt,0.004134
hand,0.002248
handl,0.003299
hannah,0.011975
hard,0.005956
harsha,0.007072
harvest,0.012067
hazelnut,0.008185
hdl,0.013692
health,0.025296
heart,0.009991
heat,0.006399
help,0.001986
henri,0.002924
hereditari,0.004342
heroin,0.005711
hexos,0.008185
hi,0.006507
high,0.018373
highcarbohydr,0.017776
highdens,0.006991
higher,0.008628
highest,0.002778
highfructos,0.008662
hispaniola,0.006370
histor,0.002038
histori,0.006045
holi,0.003841
home,0.005182
honey,0.038333
hot,0.007991
howev,0.001472
hugil,0.009179
human,0.012237
hundr,0.002756
hybrid,0.003790
hydrolys,0.006846
hydrolysi,0.011444
hydrolyz,0.013323
hydroxyl,0.005803
hyperact,0.026022
ice,0.011873
icumsa,0.045898
ignit,0.005285
immers,0.005229
impact,0.012433
impair,0.004714
imperi,0.003534
implement,0.002526
implic,0.003161
import,0.009285
impur,0.005278
incident,0.005467
includ,0.012596
inclus,0.003737
incom,0.002779
inconclus,0.015856
increas,0.026173
indentur,0.011059
independ,0.003727
index,0.008647
indi,0.004605
india,0.025002
indian,0.021025
indic,0.008928
individu,0.001871
indonesia,0.004298
industri,0.008284
industrialis,0.005131
infarct,0.006633
inflamm,0.005606
influenc,0.001998
ingest,0.010705
ingredi,0.004765
initi,0.001971
injur,0.004601
insulin,0.005376
intak,0.066470
intend,0.002808
intent,0.003053
intern,0.007950
intervent,0.003292
intoler,0.005285
introduc,0.002128
inulin,0.008066
invent,0.002952
inventor,0.004489
invert,0.009808
involv,0.008795
ionexchang,0.008185
iran,0.004116
irrig,0.004586
isbn,0.012870
island,0.013446
isom,0.005945
issu,0.001947
italian,0.003404
item,0.003328
j,0.002278
jagara,0.009179
jaggeri,0.009179
jakub,0.007863
jam,0.012095
jamaica,0.005208
jame,0.002559
januari,0.005179
join,0.002698
journal,0.002333
juic,0.051399
juli,0.002620
junk,0.005557
kept,0.003269
keton,0.012501
kg,0.033088
kgl,0.025431
kgm,0.006719
khanda,0.008321
kill,0.002995
kilocalori,0.007618
kilogram,0.025626
kind,0.002416
kingdom,0.002413
knowledg,0.002282
known,0.015160
kroke,0.009179
krytof,0.009179
l,0.008011
la,0.003028
labor,0.008936
laborintens,0.005931
lactas,0.007364
lactos,0.039483
laevorotatori,0.009179
land,0.002295
langen,0.009179
languag,0.002280
larg,0.003197
largest,0.007516
late,0.006301
later,0.001804
lb,0.052292
ldl,0.007548
lead,0.005430
leav,0.005137
led,0.003915
left,0.004836
length,0.003009
level,0.016325
licoric,0.007863
lieuten,0.004841
light,0.005001
lightli,0.005891
like,0.001620
lime,0.010850
limit,0.005557
lincolnshir,0.007257
linear,0.003227
link,0.009233
lipid,0.004778
lipoprotein,0.013499
liquid,0.006503
liquor,0.018432
list,0.001686
litr,0.006505
liverpool,0.005722
load,0.016651
loav,0.007072
local,0.002054
locat,0.002230
london,0.002554
longer,0.005003
loos,0.003771
loss,0.002653
low,0.006974
lowcalori,0.017776
lowdens,0.006749
lower,0.006724
lowfat,0.008888
lump,0.010752
lund,0.006349
luxuri,0.004551
lyle,0.007364
macular,0.007774
main,0.003840
mainli,0.002518
maint,0.004850
major,0.003218
make,0.009255
malm,0.007693
malt,0.012963
maltas,0.009179
maltodextrin,0.017776
maltos,0.030195
mani,0.009597
mankind,0.004377
manufactur,0.028927
manuscript,0.004116
march,0.005030
marmalad,0.008477
mass,0.002487
materia,0.006232
maximum,0.006596
meal,0.005169
meant,0.003111
measur,0.009933
mechan,0.004480
medic,0.005586
medica,0.006078
medicin,0.005784
medium,0.003458
men,0.002812
metaanalys,0.006413
metaanalysi,0.005815
metabol,0.016081
metastudi,0.024199
method,0.015028
michael,0.002791
microorgan,0.004530
middl,0.005001
milk,0.017920
million,0.004765
miner,0.003358
minimum,0.003308
mission,0.003024
mix,0.005835
ml,0.005577
modern,0.003638
moistur,0.010528
molass,0.046437
molecul,0.042579
molecular,0.003170
monk,0.004486
mono,0.006606
monosaccharid,0.099509
month,0.002679
moravian,0.006580
morocco,0.004800
mostli,0.002588
mother,0.013982
mouth,0.004300
multipl,0.004666
myocardi,0.006953
n,0.005448
napoleon,0.004136
natal,0.006127
nation,0.005042
nativ,0.003135
natur,0.019432
near,0.002494
necess,0.003800
necessari,0.002417
need,0.003646
neg,0.002630
new,0.006774
nh,0.005030
nineteenth,0.007588
nip,0.006991
nonfre,0.007257
normal,0.002417
north,0.004816
notabl,0.002327
note,0.001748
notion,0.005739
nuala,0.009179
number,0.010622
numer,0.002247
nutrient,0.004303
nutrit,0.004357
o,0.019191
obes,0.033520
obtain,0.002357
occur,0.019271
occurr,0.003923
ocean,0.003121
oclc,0.008737
officinarum,0.009179
oh,0.005329
oil,0.003027
old,0.002509
older,0.003183
oligosaccharid,0.013832
onli,0.008377
onset,0.004666
openchain,0.007693
oppos,0.002534
oral,0.003919
order,0.001671
organ,0.004963
origin,0.010085
ose,0.007072
ossorio,0.008662
otherwis,0.002885
ounc,0.006161
overal,0.002776
overview,0.003039
pacif,0.003515
pack,0.004426
packag,0.003845
palat,0.006002
palm,0.004726
panel,0.008940
paramet,0.003496
parent,0.003416
partial,0.002790
particip,0.002392
particl,0.003260
particular,0.007594
particularli,0.002127
pastor,0.004791
patent,0.008095
paterson,0.006127
pdf,0.003510
pebbl,0.006017
pectin,0.006991
pentos,0.006781
peopl,0.005017
pepsico,0.007774
percent,0.006092
perform,0.004264
perish,0.005169
person,0.013893
persuad,0.004383
phosphat,0.009380
photosynthesi,0.009673
physician,0.003826
place,0.003430
placebo,0.011730
plan,0.002297
plant,0.023701
plantat,0.013054
plenti,0.004769
plini,0.004992
plo,0.005744
plu,0.010237
poacea,0.008185
polar,0.003715
polym,0.013674
polyol,0.008066
polysaccharid,0.028235
polyunsatur,0.007257
popul,0.006474
popular,0.002170
portugues,0.011583
posit,0.003595
possibl,0.001774
potenti,0.002251
pound,0.008720
powder,0.025847
precipit,0.003999
preconceiv,0.006749
prepar,0.005641
presenc,0.002630
present,0.010435
preserv,0.002931
press,0.006141
prevent,0.002486
previous,0.005303
primari,0.002295
princip,0.002717
prior,0.002627
process,0.027953
produc,0.018097
product,0.019941
prolong,0.004422
proport,0.005751
protein,0.003533
provid,0.004629
publish,0.007722
pud,0.006178
purchas,0.005916
purer,0.006880
purifi,0.004949
puriti,0.019696
purpos,0.002262
qualiti,0.002591
quantiti,0.002833
question,0.004368
quickli,0.002831
r,0.004655
rad,0.006505
rainfal,0.009573
rais,0.005131
randomli,0.004702
rang,0.006263
rapidli,0.005864
rate,0.006466
ratio,0.003136
raw,0.025189
reach,0.002241
reaction,0.002828
reactiv,0.004155
read,0.001915
readi,0.004054
readili,0.003774
real,0.002289
receiv,0.004215
recent,0.003820
recogn,0.002365
recommend,0.013006
red,0.003057
reduc,0.008359
reduct,0.006348
refer,0.006618
refin,0.086511
refineri,0.015338
reflect,0.002370
regard,0.006349
region,0.001975
reign,0.003814
rel,0.001977
relat,0.001487
relationship,0.004211
reli,0.002658
remain,0.003648
remov,0.017828
replac,0.002247
report,0.010600
requir,0.008682
research,0.001789
reserv,0.002933
residu,0.008515
resin,0.005567
resist,0.002858
resourc,0.004466
respect,0.006293
result,0.016902
retent,0.005131
review,0.007415
ribos,0.014231
right,0.001979
ring,0.011208
rise,0.002219
risk,0.027142
rna,0.004544
role,0.001932
roman,0.002958
romant,0.004694
root,0.016719
rout,0.003321
rum,0.005878
rumin,0.006047
run,0.002424
s,0.004693
saccharid,0.007959
saccharin,0.008477
saccharum,0.018359
sail,0.004172
sailor,0.004718
salt,0.003779
sanskrit,0.009137
santa,0.004371
sap,0.005636
satur,0.010006
saulo,0.008662
scientif,0.002219
scientist,0.002522
screen,0.004073
season,0.003564
seed,0.011742
seen,0.002164
segreg,0.004827
separ,0.008061
seri,0.002197
set,0.005126
settl,0.003135
seventh,0.004451
sever,0.004801
shape,0.005258
ship,0.003129
short,0.002352
shown,0.010640
signific,0.001959
significantli,0.002831
similar,0.001833
simpl,0.009757
sinc,0.006140
size,0.007172
skkari,0.009179
slave,0.010882
slaveri,0.008091
slice,0.005425
small,0.001878
smaller,0.002610
social,0.001953
societi,0.001923
soda,0.006063
soften,0.005959
soil,0.006957
sold,0.006484
solid,0.003299
solubl,0.009818
solut,0.005253
sometim,0.003997
sourc,0.011586
south,0.009616
southeast,0.011275
spanish,0.003320
speci,0.005646
specif,0.003673
split,0.003155
spoil,0.005827
sponsor,0.003848
spread,0.002713
sprinkl,0.006690
spun,0.005767
ssb,0.007959
st,0.004949
stage,0.005362
standard,0.004135
stapl,0.004860
starch,0.055392
starchi,0.007115
state,0.008059
statisticallysignific,0.009179
stay,0.006720
stcenturi,0.005791
steam,0.004572
stem,0.010352
stevia,0.007863
sticki,0.011154
stop,0.003033
storag,0.003653
store,0.006220
strong,0.002230
strongli,0.002958
structur,0.007174
studi,0.037316
subcontin,0.004731
subject,0.001953
substanc,0.003227
substanti,0.002849
substitut,0.003411
sucr,0.006781
sucralos,0.018359
sucras,0.009179
sucros,0.110189
suffici,0.005676
sugar,0.856657
sugaralzheim,0.009179
sugarcan,0.094127
sugari,0.007774
sugarloaf,0.008066
sugarsensit,0.009179
sugarsweeten,0.016954
suggest,0.017194
sukar,0.009179
sulfur,0.004513
sum,0.009187
supersatur,0.007618
supplement,0.003800
suppli,0.002502
support,0.001825
surfac,0.002779
surinam,0.005827
surplu,0.004039
survey,0.005902
suspect,0.004102
swedish,0.004186
sweet,0.062173
sweeten,0.093664
sweetest,0.008066
switch,0.007539
symbiot,0.005322
syndrom,0.004678
synthet,0.003807
syrup,0.102697
tabl,0.014773
taizong,0.008066
tang,0.009920
tast,0.017236
tate,0.019174
taub,0.007115
taught,0.003422
tea,0.009818
technolog,0.002213
teeth,0.009664
temper,0.004631
templ,0.004058
term,0.002901
textur,0.005400
th,0.017603
thailand,0.013917
thesauru,0.006880
thi,0.017224
thirtyf,0.006633
thousand,0.002707
thrive,0.004250
thu,0.001794
time,0.014941
tissu,0.007419
toffe,0.018359
togeth,0.002117
told,0.007787
tonn,0.005077
took,0.004796
tooth,0.022386
total,0.012852
toxic,0.004244
toyearold,0.008185
trace,0.002943
trade,0.008883
transport,0.010527
travel,0.005421
treacl,0.016642
treat,0.005376
treatis,0.003703
tri,0.002416
trial,0.003503
tropic,0.015671
tuber,0.006580
turn,0.002122
twentieth,0.003622
type,0.011074
typic,0.006178
tyre,0.012501
ultim,0.002655
unabl,0.003190
undergon,0.004791
undertaken,0.007911
unequivoc,0.005678
unexplain,0.005700
uniform,0.007119
unimport,0.005791
union,0.006844
unit,0.013936
univers,0.004770
unleash,0.005756
unrefin,0.014514
unsift,0.009179
unwant,0.005112
usda,0.006269
use,0.045159
usual,0.003694
vacuum,0.008011
valu,0.005903
vari,0.004526
variabl,0.002806
varieti,0.002305
variou,0.006990
vascular,0.005059
veget,0.010994
venetian,0.005502
venic,0.010061
veri,0.003506
villag,0.003508
vitamin,0.005042
volum,0.002629
vulgari,0.007309
wa,0.033987
wall,0.006501
war,0.002180
wash,0.004804
water,0.026049
week,0.003226
weight,0.021660
west,0.005272
wherea,0.005328
white,0.014573
whiter,0.008321
wide,0.007909
william,0.002460
wine,0.017280
winemak,0.006289
withdraw,0.003690
women,0.002921
woodhead,0.015097
word,0.019179
work,0.001505
world,0.012451
worri,0.004523
write,0.002400
x,0.002718
y,0.003269
year,0.009296
yield,0.002999
york,0.004729
zero,0.003228
zucchero,0.009179
zuchr,0.009179
