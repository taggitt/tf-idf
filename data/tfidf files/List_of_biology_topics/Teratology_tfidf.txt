abil,0.023012
abnorm,0.208135
absorpt,0.020356
academi,0.016110
access,0.011571
account,0.009849
acid,0.016849
acquir,0.013621
act,0.019116
action,0.010275
addit,0.008460
advers,0.117521
affect,0.021856
agent,0.128329
al,0.014344
alcohol,0.019199
ancestor,0.017974
ancient,0.012116
ani,0.007370
anim,0.035116
anomali,0.045675
anoth,0.016087
anyth,0.016527
appli,0.008940
associ,0.008169
avail,0.010222
awar,0.015130
basic,0.010144
bear,0.015650
becam,0.009217
becaus,0.015181
becom,0.008072
bifida,0.036514
biolog,0.023919
birdlik,0.038148
birth,0.089150
block,0.015021
botani,0.046013
broader,0.016813
broadli,0.016879
came,0.011042
carcinogen,0.030530
case,0.016888
caus,0.054086
cell,0.014525
center,0.010771
centuri,0.017345
choristoder,0.043995
chronic,0.020759
classifi,0.013662
close,0.009037
collect,0.009852
common,0.008167
commonli,0.011217
complic,0.015308
composit,0.014003
conceptu,0.032516
congenit,0.243579
contact,0.014174
cosmet,0.024837
critic,0.009936
current,0.008647
databas,0.015803
david,0.011999
death,0.023661
defect,0.119637
deform,0.110267
degre,0.022124
delay,0.017252
demonstr,0.012141
depend,0.017826
describelabel,0.043995
design,0.009921
develop,0.087824
development,0.114097
deviant,0.058893
differ,0.007161
dime,0.034319
dinosaur,0.053275
discours,0.018496
discov,0.011895
discoveri,0.025303
diseas,0.028300
disord,0.052239
dog,0.020221
dosag,0.029210
dose,0.024623
drug,0.031864
dure,0.015551
earli,0.016268
effect,0.039503
embryo,0.045071
embryolog,0.026822
embryonicfet,0.043995
environ,0.010780
environment,0.078448
et,0.013979
etymolog,0.017312
european,0.010129
evalu,0.014091
event,0.010583
evid,0.033270
evolutionari,0.016023
exampl,0.036417
expect,0.011173
expos,0.016290
exposur,0.072297
extern,0.006359
extraordinari,0.021227
factor,0.020623
fetal,0.027698
fetu,0.026547
fetus,0.034103
field,0.035525
finland,0.021745
flower,0.059247
flowersfor,0.043995
foliar,0.043995
folic,0.034103
form,0.013294
forth,0.017678
fossil,0.094245
foundat,0.011340
frequenc,0.016619
function,0.017807
genet,0.044312
genit,0.027424
genotyp,0.049994
global,0.011548
glove,0.030141
greek,0.012314
gross,0.017003
growth,0.033022
guid,0.012295
ha,0.028993
handl,0.015813
hatchl,0.038659
head,0.011226
help,0.009520
hereditari,0.020813
hi,0.007797
highli,0.011448
histor,0.019537
histori,0.007243
howev,0.014115
human,0.067027
hyphalosauru,0.043995
implic,0.030304
import,0.029668
includ,0.042258
increas,0.016725
individu,0.008970
infant,0.041792
infect,0.019738
infer,0.016712
influenc,0.028735
influenza,0.082761
inform,0.017291
initi,0.009448
injuri,0.019278
instanc,0.011777
instead,0.010185
instrument,0.013390
insult,0.025960
interact,0.010850
investig,0.012185
iodid,0.060664
irrit,0.026040
jaw,0.025843
jim,0.022985
known,0.014531
lack,0.022172
larg,0.007662
ld,0.027698
leav,0.024624
lethal,0.025062
level,0.017387
life,0.018691
like,0.007765
link,0.006321
logo,0.022634
loos,0.018074
low,0.011142
major,0.007712
malform,0.180851
mammal,0.018404
mammalia,0.038659
mammalian,0.023673
mani,0.006570
manifest,0.050172
manner,0.014030
march,0.012055
marvel,0.053095
matern,0.046452
mean,0.048037
mechan,0.010735
medic,0.040163
medicin,0.027721
mental,0.015731
mice,0.023648
mild,0.022439
model,0.009055
modifi,0.014184
molecular,0.015194
monitor,0.015721
monkey,0.023849
monograph,0.022694
monster,0.025960
mortal,0.019430
mostli,0.012403
mother,0.067015
mous,0.023874
munoz,0.039231
mutagen,0.031665
natur,0.015521
new,0.025974
newborn,0.025544
noael,0.043995
nonbirth,0.043995
nonhuman,0.022307
notabl,0.022310
nutrient,0.020625
nutrit,0.020882
observ,0.028784
oldest,0.015726
oligohydramnio,0.043995
onli,0.006691
oral,0.018785
organ,0.023788
origin,0.008056
outcom,0.044587
overexposur,0.036179
overlap,0.016157
paleopathologist,0.041514
pandem,0.028110
pejor,0.025367
peopl,0.008015
period,0.008637
petal,0.032064
physic,0.038050
physiolog,0.017123
pistilsfurnish,0.043995
placent,0.031179
plant,0.012621
planta,0.032064
polio,0.092532
polycephali,0.043995
popular,0.010400
possibl,0.025507
potassium,0.046230
potenti,0.021585
potter,0.026459
pregnanc,0.145325
pregnant,0.178061
prevent,0.035753
primarili,0.011394
principl,0.030020
procedur,0.014517
process,0.007880
prodigi,0.029782
produc,0.008673
prospect,0.017714
provid,0.007396
puberti,0.030332
purpos,0.010843
rabbit,0.025805
rat,0.022124
rate,0.010330
raw,0.017246
readili,0.018090
receiv,0.030302
recent,0.009154
recommend,0.015584
record,0.033431
refer,0.010573
refin,0.016584
regard,0.010143
registri,0.026372
relat,0.014256
remain,0.008743
report,0.010161
reproduct,0.017959
research,0.008576
restraint,0.023748
result,0.014728
retard,0.051843
rex,0.028172
risk,0.013008
rout,0.015917
rubella,0.033896
s,0.014996
safe,0.017285
saw,0.012937
school,0.010756
scienc,0.008891
scientif,0.010638
scientist,0.012089
seek,0.011863
sequenc,0.014677
sever,0.015341
shown,0.012749
signific,0.037570
sinc,0.007357
skelet,0.025767
smallpox,0.026080
smith,0.015589
societi,0.018437
specialis,0.021317
specialist,0.036513
specif,0.008803
specimen,0.091267
spina,0.034782
stage,0.025698
staminoid,0.043995
stem,0.016539
structur,0.008596
studi,0.069983
substanc,0.030938
suscept,0.063637
syndrom,0.044840
tell,0.016573
tera,0.035293
terato,0.043995
teratogen,0.240235
teratogenesi,0.203145
teratolog,0.534076
teratologist,0.087990
teratoma,0.037685
term,0.027810
test,0.012028
th,0.016873
theoret,0.012064
theori,0.017248
therapeut,0.021712
thi,0.027517
thought,0.010427
thyroid,0.028698
time,0.006510
tip,0.020521
tissu,0.035560
today,0.021140
toxic,0.061033
toxicolog,0.027477
toxin,0.025229
transfer,0.012591
transmit,0.018027
troodon,0.043995
twist,0.024138
tyrannosauru,0.037685
understand,0.029962
univers,0.007620
use,0.024048
utero,0.035293
vaccin,0.219761
vari,0.010846
vertebra,0.060860
vertic,0.019035
viscer,0.031179
vulner,0.018714
w,0.012345
wa,0.024131
washington,0.016199
way,0.015734
welldocu,0.030049
western,0.011067
wilson,0.018379
women,0.056012
wonder,0.021317
word,0.010213
york,0.011332
