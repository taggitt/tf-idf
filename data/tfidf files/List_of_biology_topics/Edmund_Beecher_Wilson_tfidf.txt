academi,0.075016
accept,0.016296
addit,0.013131
adjunct,0.042733
advanc,0.016031
alawqati,0.068285
america,0.036382
american,0.058827
amphioxu,0.061899
ancestor,0.027898
annelid,0.049349
annual,0.019939
archiv,0.023890
art,0.039715
articl,0.030853
associ,0.025360
atla,0.030434
attent,0.021166
award,0.045767
b,0.103158
balanc,0.020543
basi,0.032335
baxter,0.092183
bchromosom,0.068285
becam,0.014306
beecher,0.253413
bibliographi,0.022762
biolog,0.129939
biologist,0.087853
born,0.022109
boveri,0.173509
bryn,0.051724
c,0.014979
came,0.017138
career,0.051430
cell,0.202901
chromosom,0.374738
cleavag,0.043259
colbi,0.052304
colleg,0.045513
columbia,0.058314
come,0.014993
common,0.012676
conclud,0.042885
conduct,0.017969
constitut,0.016554
continu,0.012703
contribut,0.015388
credit,0.020289
cultur,0.031529
d,0.016973
daniel,0.026077
describ,0.038714
destruct,0.024814
determin,0.014560
develop,0.041943
development,0.029515
devot,0.049566
discov,0.018463
discoveri,0.019637
divis,0.018171
document,0.020242
dodd,0.045101
doi,0.055515
doibf,0.042733
doisciencea,0.063061
domain,0.020477
drscher,0.068285
dure,0.012069
e,0.067959
earn,0.023725
earthworm,0.045218
ed,0.035342
edit,0.059270
edmund,0.256931
elect,0.018617
elliot,0.044124
embryo,0.034978
embryolog,0.083264
encyclopedia,0.021739
entiti,0.021417
evolut,0.020494
extern,0.009870
f,0.037187
famou,0.020853
fashion,0.026567
fellow,0.028540
femal,0.025343
fertil,0.026459
final,0.032653
flatworm,0.054372
foreign,0.017571
frank,0.026429
friend,0.026809
gener,0.009699
geneticist,0.041205
geneva,0.035877
german,0.019062
germlay,0.068285
gilbert,0.032283
gilman,0.050443
giraud,0.054372
goe,0.024290
graduat,0.025293
group,0.012175
gutenberg,0.036155
h,0.037500
heinrich,0.033499
hemiptera,0.061899
hered,0.038551
hereditari,0.032305
hi,0.072615
histori,0.056212
honor,0.027922
hopkin,0.033254
illinoi,0.033040
import,0.011512
incorpor,0.018535
independ,0.055454
influenc,0.029733
inherit,0.050059
institut,0.014283
intern,0.035484
internet,0.022142
introduct,0.017893
invertebr,0.035843
isi,0.080106
john,0.015352
journal,0.017360
karyokinesi,0.066117
kingsland,0.061899
l,0.039732
law,0.013594
lectur,0.024536
legaci,0.028374
life,0.014506
lilli,0.048761
link,0.019624
live,0.014590
m,0.016138
maintain,0.015733
male,0.025309
mani,0.010198
march,0.018711
maria,0.033351
massachusett,0.029589
matern,0.036050
mawr,0.052611
mead,0.039994
medal,0.066269
member,0.014396
mendel,0.079639
mendelian,0.083861
modern,0.013534
mollusc,0.041274
morgan,0.032034
mosaic,0.037593
nation,0.012503
need,0.013564
netherland,0.026294
netti,0.115673
new,0.020157
nucleu,0.059782
observ,0.014892
octob,0.019511
organ,0.024615
pair,0.024045
paper,0.037235
patern,0.040418
phd,0.029157
philosophi,0.019210
phylogenet,0.035675
physic,0.014764
pioneer,0.023571
pmid,0.104308
preformationist,0.066117
presid,0.018711
probabl,0.018566
professor,0.115623
project,0.016009
public,0.039091
publish,0.014361
q,0.028977
r,0.017314
reason,0.014727
reduc,0.015545
refer,0.008205
relationship,0.031327
reread,0.050682
research,0.026624
rest,0.019225
result,0.011430
retain,0.021579
review,0.036772
revolut,0.020328
royal,0.021651
s,0.023276
said,0.016860
scienc,0.110410
scientif,0.016512
second,0.013467
sedgwick,0.048575
seen,0.016100
segreg,0.035911
separ,0.014991
seri,0.016347
serv,0.032669
seventh,0.033111
sever,0.011905
sex,0.085237
sexdetermin,0.053986
similar,0.013637
sinauer,0.053986
societi,0.014308
spent,0.025243
spiral,0.034948
st,0.018409
state,0.009992
steven,0.057551
subsequ,0.017821
success,0.015144
suggest,0.015987
supernumerari,0.114466
sutton,0.134620
suttonboveri,0.068285
t,0.059610
teacher,0.026171
technolog,0.016465
text,0.019700
textbook,0.025741
theodor,0.031256
theori,0.080317
thi,0.025626
thurston,0.047231
time,0.010104
unit,0.011518
univers,0.035484
use,0.009331
volum,0.019562
w,0.019162
wa,0.065547
walter,0.027241
william,0.018299
wilson,0.456436
work,0.033587
wrote,0.019457
xx,0.041344
xy,0.080712
yale,0.031219
year,0.023051
york,0.017589
zoolog,0.068961
zoologist,0.039935
