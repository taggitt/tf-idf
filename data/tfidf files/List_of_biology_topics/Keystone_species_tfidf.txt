absenc,0.014171
abund,0.031265
acorn,0.030554
acquir,0.012555
act,0.008810
addit,0.007798
aerat,0.030554
affect,0.020146
african,0.013510
agelaia,0.040552
allow,0.015331
alter,0.041481
america,0.021606
amphibian,0.022752
analog,0.013774
anchor,0.020974
anemon,0.029306
ani,0.013587
anim,0.021579
anoth,0.022242
apex,0.024002
aquat,0.020772
arch,0.070738
area,0.030624
australia,0.026797
avon,0.033656
balanc,0.012200
banksia,0.078528
barrier,0.047052
base,0.006716
bay,0.017407
beaver,0.079550
becam,0.016992
believ,0.009613
benefici,0.034114
best,0.010244
biolog,0.011023
biomass,0.023479
bird,0.015866
bison,0.028957
brood,0.028530
burrow,0.049622
californianu,0.038265
cassowari,0.078528
cattl,0.018234
caus,0.008308
central,0.017452
chain,0.013907
chang,0.021055
channel,0.015366
charact,0.012864
classic,0.020571
classifi,0.012592
clean,0.018223
collaps,0.027428
coloni,0.025746
commun,0.032564
compact,0.018368
complex,0.018221
composit,0.012907
concept,0.069510
consensu,0.015531
conserv,0.049436
consist,0.008333
consum,0.024795
consumpt,0.014522
context,0.010952
coral,0.020578
cover,0.010251
creatur,0.018987
crevic,0.034346
critic,0.018317
cut,0.013218
dam,0.084049
damag,0.013436
deer,0.023647
definit,0.019690
depend,0.008215
deploy,0.016850
describ,0.022991
descriptor,0.027217
destroy,0.013020
determin,0.008646
develop,0.006227
diet,0.018410
differ,0.013201
direct,0.008268
disproportion,0.021730
divers,0.035926
dog,0.111835
domin,0.020492
dr,0.015448
dramat,0.028123
drive,0.013377
easier,0.016050
ecolog,0.028252
ecologist,0.021820
ecosystem,0.238038
edg,0.032709
effect,0.021847
eleph,0.041472
elimin,0.012963
engend,0.023446
engin,0.054015
entir,0.009723
environ,0.029810
eros,0.020215
especi,0.008704
establish,0.008000
euhrychiopsi,0.040552
eurasian,0.022981
exact,0.028601
exampl,0.046995
experi,0.018436
explain,0.010091
explod,0.044108
factor,0.009504
fish,0.013986
flagship,0.025531
followup,0.023786
food,0.011536
forag,0.045560
forest,0.046246
form,0.006127
foundat,0.010452
frugivor,0.036759
function,0.008206
given,0.008039
grass,0.020199
gray,0.019998
graze,0.067521
great,0.027102
grow,0.010278
growth,0.010146
ha,0.026724
habitat,0.018695
help,0.026326
herbivor,0.095003
hi,0.028748
high,0.016233
histor,0.009004
histori,0.006676
holdfast,0.038265
honeyeat,0.074899
idea,0.008996
illustr,0.013669
impact,0.032955
implic,0.013966
import,0.013673
includ,0.005564
increas,0.023124
indic,0.009860
indigen,0.015982
interact,0.030003
interchang,0.018347
interspeci,0.030554
intertid,0.029684
introduc,0.018805
invertebr,0.021286
jaguar,0.029070
jungl,0.022836
kelp,0.141659
keyston,0.503255
known,0.013394
label,0.014420
land,0.010142
larg,0.028251
larger,0.010578
lecontei,0.040552
limit,0.008184
lion,0.020863
list,0.007451
local,0.009076
loss,0.023441
low,0.010270
lower,0.009901
maintain,0.009343
makah,0.040552
make,0.006814
mammalian,0.021820
mani,0.024226
meadow,0.026924
measur,0.008776
mere,0.013182
mobil,0.014233
mountain,0.014416
mule,0.026143
mussel,0.092150
mutual,0.014134
mutualist,0.052905
myriad,0.022952
mytilu,0.038265
natur,0.007153
near,0.011017
nearshor,0.094303
necessari,0.010678
nectar,0.029187
need,0.008055
neighbor,0.015116
nest,0.041508
network,0.011202
nonabund,0.040552
north,0.010639
nuisanc,0.027870
number,0.040221
numer,0.009927
nutrient,0.019011
observ,0.008843
ochraceu,0.076531
older,0.014061
orca,0.031061
organ,0.014617
otter,0.107982
outsiz,0.034346
oversimplifi,0.026582
owl,0.025038
pain,0.084859
paper,0.033169
parrotfish,0.039264
particip,0.010570
particular,0.008387
particularli,0.009397
perform,0.009419
perhap,0.012456
period,0.007961
pisast,0.076531
place,0.007578
plain,0.017510
plant,0.069801
play,0.029024
plover,0.036759
policymak,0.021935
pollin,0.024723
pond,0.023681
popul,0.019067
popular,0.019173
prairi,0.159493
predat,0.153515
prefer,0.011766
present,0.007683
pressur,0.023132
prevent,0.021970
prey,0.146559
primari,0.010142
prionot,0.040552
probabl,0.011025
procliv,0.029306
product,0.032033
professor,0.013732
profound,0.035056
prolifer,0.018915
pronghorn,0.035634
propos,0.009480
protect,0.010240
provid,0.006817
quantiti,0.025031
rainwat,0.028235
rang,0.009223
rapidli,0.012954
rate,0.019044
ray,0.016645
reef,0.106940
refer,0.004873
region,0.017452
rel,0.008737
relationship,0.009301
releas,0.012178
remain,0.008059
remov,0.033754
result,0.013576
revers,0.013416
riparian,0.089455
river,0.013323
riverin,0.029818
robert,0.021429
role,0.042683
room,0.015806
root,0.024620
runoff,0.022224
salmon,0.024637
savanna,0.049360
scenario,0.017651
scrape,0.027614
sea,0.161640
secondari,0.013876
seed,0.017290
serv,0.019400
sever,0.014140
shape,0.011614
shark,0.023446
shellfish,0.027697
shift,0.011809
shown,0.023503
similar,0.008098
similarli,0.038546
sinc,0.006781
size,0.021123
small,0.024896
soil,0.061467
sole,0.027533
song,0.017986
sourc,0.008530
south,0.010620
speci,0.486452
spread,0.011988
star,0.045121
starfish,0.028633
stone,0.015146
strain,0.016857
stream,0.032511
strong,0.009854
structur,0.015848
studi,0.007167
suggest,0.009494
support,0.008063
sustain,0.013265
swamp,0.024767
synonym,0.016925
t,0.011800
tabl,0.013052
tend,0.011137
term,0.006408
terrestri,0.018464
territori,0.011870
therefor,0.009218
thi,0.010145
thousand,0.011960
threaten,0.015420
topographi,0.022126
transform,0.011101
tree,0.069851
trim,0.027451
tunnel,0.020476
turn,0.009376
type,0.008153
umbrella,0.020199
uncontrol,0.023579
univers,0.007024
unless,0.015198
unparallel,0.028430
urchin,0.132262
use,0.022166
valu,0.008692
vari,0.009998
variou,0.007720
veget,0.016190
vicina,0.040552
wa,0.022243
washington,0.029863
wasp,0.025799
water,0.010461
watermilfoil,0.037449
web,0.014703
weevil,0.031633
western,0.010201
wetland,0.024002
wheatbelt,0.040552
wide,0.008735
wipe,0.021959
wolf,0.020772
woodland,0.024680
year,0.006844
younger,0.016850
zoolog,0.020476
