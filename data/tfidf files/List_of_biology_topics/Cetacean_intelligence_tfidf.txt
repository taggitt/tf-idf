abil,0.052954
abl,0.012964
abov,0.003284
abras,0.009276
absenc,0.004717
absolut,0.008704
abstract,0.004515
academ,0.003843
accompani,0.004960
accord,0.002565
achiev,0.003227
acoust,0.043008
acquaint,0.007509
acquisit,0.005547
act,0.002932
activ,0.005120
adam,0.005207
addit,0.005191
adept,0.008498
adjac,0.005872
adult,0.015069
advanc,0.003169
affect,0.003352
agre,0.003868
aid,0.003849
air,0.011824
aircor,0.013498
akeakamai,0.012737
akin,0.006867
alleg,0.005305
allianc,0.014396
allometr,0.023722
allow,0.007654
alongsid,0.010672
altern,0.003297
alway,0.003450
anaesthesia,0.009971
analog,0.004584
analys,0.005016
analysi,0.006186
ancestor,0.022059
ancestri,0.007045
andor,0.004490
anger,0.006697
ani,0.013568
anim,0.064646
anomal,0.007804
anoth,0.024678
answer,0.004568
anthropomorph,0.007941
ape,0.023968
appar,0.008760
appear,0.008915
approach,0.005905
approxim,0.017483
arbitrari,0.005540
area,0.012742
argu,0.006826
artifici,0.004332
asid,0.005851
ask,0.004241
asphyxi,0.010463
assist,0.007651
associ,0.010026
asymmetri,0.007639
attent,0.008368
au,0.007357
audienc,0.005791
australia,0.008919
avail,0.006273
averag,0.003832
avoid,0.011497
awar,0.009284
away,0.003908
b,0.003398
ball,0.006336
band,0.022317
base,0.002235
basi,0.003195
basic,0.006225
bay,0.011588
beach,0.013405
becam,0.005656
becaus,0.006986
befor,0.002642
befriend,0.009010
began,0.012357
behavior,0.123394
believ,0.006399
beluga,0.024471
beneath,0.006687
benefit,0.007508
better,0.003488
bird,0.005281
bite,0.008091
blubber,0.010828
boat,0.006167
bodi,0.018627
bodili,0.006760
bodysurf,0.013498
bond,0.017676
bore,0.006944
born,0.008740
bottlenos,0.133649
bow,0.015203
brain,0.158430
branch,0.006580
breach,0.007000
breather,0.012465
bredanensi,0.013498
bring,0.007692
british,0.003454
broadband,0.016183
brother,0.010376
bubbl,0.032428
bubblenet,0.013498
burst,0.020907
c,0.005921
canada,0.004438
canon,0.005626
capabl,0.004080
captiv,0.013273
case,0.002590
cat,0.006573
categor,0.005267
cell,0.008912
central,0.002904
certain,0.005672
certainli,0.005914
cetacea,0.010748
cetacean,0.090347
channel,0.005115
characterist,0.003571
chiasm,0.012235
chicago,0.010100
chimpanze,0.032419
choos,0.008915
christin,0.008091
circl,0.004993
claim,0.006291
clan,0.013587
class,0.003436
clean,0.006066
clever,0.007977
click,0.040198
close,0.008318
coeffici,0.005980
cognit,0.073694
cohes,0.006723
columbia,0.005763
common,0.015035
commun,0.029808
compar,0.015326
comparison,0.017382
compet,0.004272
complet,0.008586
complex,0.024260
complic,0.004696
composit,0.008592
comprehens,0.018259
compris,0.004239
concept,0.005784
concern,0.002909
conclud,0.008477
conduct,0.007104
congreg,0.007491
connor,0.009755
conscious,0.005279
consid,0.004834
consist,0.005547
constantli,0.011555
contact,0.004348
contain,0.005946
continu,0.002511
contrast,0.007001
control,0.002753
converg,0.005459
convolut,0.008273
cooper,0.019806
cortex,0.007536
count,0.004726
counterargu,0.009219
creat,0.010140
creation,0.003772
creativ,0.021420
critic,0.003048
cross,0.004523
crossmod,0.011562
crossspeci,0.011100
cultur,0.003116
current,0.002653
data,0.003307
date,0.003463
day,0.015637
debat,0.003858
defin,0.002844
definit,0.006554
degre,0.003394
delay,0.005293
delphinoidea,0.013069
demonstr,0.003725
denis,0.020037
depend,0.002734
deriv,0.006302
descend,0.005016
describ,0.002550
despit,0.003514
despond,0.010598
determin,0.002878
develop,0.012436
devot,0.009797
dialect,0.024387
diana,0.008397
die,0.003971
differ,0.017577
difficult,0.003614
dig,0.007730
directli,0.003330
disagre,0.005958
disband,0.007204
discov,0.003649
discoveri,0.015527
discrimin,0.011510
display,0.018841
distant,0.005760
distinct,0.009576
distinctli,0.007573
distress,0.006878
disturb,0.005820
diver,0.008742
diverg,0.005601
doe,0.002804
dog,0.006204
dolphin,0.836128
domin,0.003410
doubt,0.005348
dr,0.015426
dramat,0.004680
dure,0.011928
dye,0.007098
earli,0.004991
earth,0.003917
eat,0.005822
eavesdrop,0.022200
echidna,0.010529
echoloc,0.079771
economo,0.012737
edit,0.003905
eduardo,0.008606
eeg,0.008938
effect,0.004848
eleph,0.048316
emit,0.024983
enceph,0.031017
energi,0.003493
engag,0.003951
enjoy,0.004750
entir,0.003236
entri,0.004826
environ,0.016538
eq,0.030202
equalorgreat,0.013498
equip,0.008922
equival,0.004019
everi,0.009367
evid,0.030623
evolut,0.012153
evolutionari,0.014748
evolv,0.008191
exactli,0.004888
examin,0.003905
exampl,0.015643
exceed,0.005240
excit,0.006083
exhaust,0.005834
exhibit,0.023551
exist,0.007443
expect,0.003428
expens,0.004456
experi,0.027615
experiment,0.004167
explan,0.004392
expon,0.007078
extant,0.006602
extens,0.003369
extern,0.001951
extraterrestri,0.007782
extrem,0.003579
eye,0.004988
facilit,0.004498
fact,0.006336
factor,0.003163
fairli,0.005651
famili,0.010365
farremov,0.013498
faster,0.015616
favor,0.012363
femal,0.015029
fiction,0.005321
field,0.005449
figur,0.003695
fin,0.007953
final,0.003227
finger,0.007248
firth,0.009496
fish,0.037243
fishermen,0.007953
fissionfus,0.021657
flank,0.008397
flash,0.007006
flashlight,0.011313
flip,0.007989
floor,0.006069
florida,0.006549
flurri,0.009034
fm,0.008065
food,0.003840
footag,0.017740
forag,0.022748
ford,0.007065
form,0.008157
format,0.010917
frequenc,0.015297
frisbe,0.012235
frustrat,0.006844
function,0.008195
futur,0.009749
gallup,0.008985
gang,0.007381
gaze,0.018069
gener,0.011503
genet,0.004531
geneticallyenhanc,0.013498
gestur,0.014762
given,0.005352
gordon,0.006223
gori,0.010529
gram,0.022370
grammar,0.006636
great,0.012028
greater,0.020164
group,0.026474
growth,0.003377
ha,0.021349
hadnt,0.009795
hal,0.008430
half,0.007345
han,0.005332
hardli,0.007133
hardwar,0.006241
hawaii,0.007098
hear,0.011252
helic,0.008215
help,0.005842
hemispher,0.024305
herd,0.007119
herman,0.042757
herz,0.023408
hi,0.002392
high,0.005403
highli,0.007025
hoard,0.008551
hope,0.004542
hou,0.021496
howev,0.015158
human,0.084830
humandolphin,0.026996
humpback,0.010748
hundr,0.004053
hunt,0.016154
hypopig,0.013498
hypothesi,0.009295
idea,0.002994
ident,0.007640
identifi,0.012901
imag,0.012325
imit,0.018936
imposs,0.004511
inbreed,0.008938
includ,0.025930
increas,0.002565
indic,0.019693
individu,0.019264
induc,0.005283
inferior,0.006494
initi,0.002899
inject,0.006587
inspect,0.006666
instanc,0.010840
instead,0.006250
institut,0.005646
insul,0.007263
integr,0.003219
intellig,0.070773
inter,0.007500
interact,0.003329
interclick,0.012737
interestingli,0.008643
interpret,0.007119
interv,0.011242
intraspecif,0.009837
investig,0.007477
involv,0.007759
issu,0.002863
janet,0.007709
jerison,0.013069
john,0.012139
johnson,0.006096
join,0.003968
jump,0.012320
just,0.003111
karen,0.007639
keeper,0.008702
kelli,0.007639
kenneth,0.005986
kg,0.006950
khz,0.029145
killer,0.008039
kind,0.010661
know,0.004042
known,0.037897
kuczaj,0.013498
l,0.003926
la,0.004453
laboratori,0.004527
languag,0.013411
larg,0.011754
largest,0.003684
late,0.003088
learn,0.022483
leav,0.011332
length,0.004425
level,0.002667
life,0.011469
like,0.011913
lilli,0.019277
limit,0.005448
line,0.006385
lineag,0.013204
link,0.003879
littl,0.003413
live,0.020189
lobe,0.016236
long,0.002896
longest,0.012788
longfin,0.011432
lori,0.009881
loui,0.015099
lowerfrequ,0.012737
m,0.003190
macphail,0.012737
macrocephalu,0.013498
mad,0.007270
main,0.005647
maintain,0.003110
major,0.004732
make,0.009072
male,0.040024
malia,0.046249
mammal,0.067761
mammalian,0.007263
man,0.003946
manag,0.003194
mani,0.016128
mann,0.008319
manner,0.004304
marin,0.025530
marino,0.008892
mark,0.014324
marten,0.009881
mass,0.025605
match,0.028460
mate,0.013374
matrilin,0.039020
matur,0.005190
mean,0.017195
meaning,0.005797
media,0.004091
member,0.014229
memori,0.013919
mental,0.004826
mercado,0.010067
mere,0.004387
metacognit,0.010280
method,0.016574
mhzcapabl,0.013498
mice,0.007255
mid,0.004441
militari,0.003608
millisecond,0.008430
mimicri,0.027030
mind,0.004206
mirror,0.030113
misinterpret,0.008145
mississippi,0.008131
mit,0.005419
modal,0.007078
modern,0.005350
modul,0.006022
monitor,0.004823
moray,0.010598
morgan,0.006332
mostli,0.007611
mother,0.041122
motiv,0.008715
music,0.004902
mutual,0.004704
nachtigal,0.013498
narrowband,0.010828
narwhal,0.011704
natur,0.002381
navi,0.005338
near,0.007334
nearbi,0.005489
nearunison,0.013498
necessari,0.003554
necessarili,0.008732
need,0.008043
neighbor,0.005031
neocort,0.011562
neocortex,0.019430
neural,0.005993
neuron,0.032587
new,0.009961
nonecholoc,0.013498
nonfinit,0.012235
nonhuman,0.006844
normal,0.014220
norri,0.009566
note,0.002571
novel,0.039492
number,0.004462
numer,0.003304
object,0.009147
observ,0.017662
occas,0.005547
occasion,0.005031
occur,0.005667
ocean,0.009179
odontocet,0.013498
offer,0.009950
offspr,0.020077
omit,0.006969
onetenth,0.009306
ongo,0.004880
onli,0.018478
open,0.003053
optic,0.005365
orca,0.113731
order,0.009831
organ,0.002432
origin,0.002471
outsid,0.003402
overal,0.004082
pack,0.013016
pair,0.004753
pantrop,0.013498
paralimb,0.024931
parallel,0.004621
park,0.010426
partial,0.004102
particular,0.008375
particularli,0.006256
partli,0.004795
pass,0.003465
passiv,0.006146
past,0.010615
paul,0.007754
pay,0.004164
peopl,0.004918
perform,0.015677
perhap,0.008292
period,0.002650
peter,0.004067
physet,0.013498
piec,0.009851
pigment,0.007474
pilot,0.006607
pioneer,0.004659
place,0.002522
plata,0.009306
play,0.016101
pod,0.054825
podspecif,0.013498
point,0.010349
pool,0.005840
popul,0.003173
popular,0.003191
porpois,0.040074
posit,0.002643
possess,0.004008
possibl,0.007826
postur,0.007804
precursor,0.005571
predat,0.005677
presenc,0.003867
present,0.010229
press,0.003010
presum,0.005709
prevent,0.003656
previous,0.003899
primari,0.006752
primat,0.036279
prior,0.003863
problem,0.010941
problemsolv,0.008158
process,0.012089
produc,0.013306
product,0.005331
program,0.003217
propos,0.009466
protect,0.003408
provid,0.009077
pryor,0.035584
psarako,0.013498
publish,0.002838
puf,0.010671
puls,0.020447
purpos,0.009980
question,0.003211
quick,0.006320
quicker,0.008258
quickli,0.004163
quit,0.008644
quotient,0.024865
randal,0.007977
rang,0.006140
rank,0.004318
rapid,0.004432
rare,0.004209
rat,0.006788
rate,0.003169
ratio,0.009225
reach,0.003296
read,0.002817
realis,0.013081
realtim,0.006799
reason,0.005822
recent,0.008426
recogn,0.003478
record,0.006838
redirect,0.007989
refer,0.003244
regard,0.003112
regular,0.004443
reinforc,0.016572
reiss,0.010067
rel,0.002908
relat,0.008748
relationship,0.003096
relev,0.004242
relief,0.005783
rem,0.009531
remain,0.008048
rememb,0.011910
repeat,0.018727
repeatedli,0.005772
report,0.015588
represent,0.004092
requir,0.007660
research,0.047367
resembl,0.005116
resid,0.017189
respir,0.014650
respons,0.014067
rest,0.003800
result,0.015816
retriev,0.004819
reward,0.033668
richard,0.008014
ride,0.014104
ridgway,0.023722
ring,0.027469
rise,0.006528
risso,0.012036
river,0.008869
rock,0.005079
role,0.002841
rostra,0.013498
roughtooth,0.012737
roundtrip,0.009795
rubbish,0.020449
s,0.004601
said,0.003332
sam,0.007204
sampl,0.019335
sarasota,0.011861
scale,0.003671
scarc,0.006211
scientif,0.009792
scientist,0.014836
scotland,0.006096
sea,0.012416
search,0.004151
second,0.007986
section,0.004019
seek,0.003639
seen,0.015913
selfawar,0.085970
selfbehavior,0.013498
selfrecognit,0.011704
sens,0.006675
sensori,0.025153
sentenc,0.011090
separ,0.008890
sequenc,0.009006
seri,0.003231
serv,0.003228
session,0.018513
set,0.007538
sever,0.002353
shape,0.003866
share,0.012643
shark,0.023413
shorelin,0.008938
shown,0.011735
sight,0.006019
sign,0.007433
signal,0.009334
signatur,0.052857
signific,0.008645
significantli,0.004163
similar,0.005391
sinc,0.013544
singl,0.006042
singli,0.009034
sixteenth,0.007126
size,0.028124
skana,0.040495
skill,0.013046
sleep,0.032120
sleeplik,0.012465
slightli,0.009785
slow,0.004660
small,0.008287
smolker,0.013498
social,0.028718
solv,0.008309
sometim,0.002938
son,0.008644
sonar,0.008783
song,0.005986
sophist,0.005194
sort,0.004735
sound,0.033272
sourc,0.002839
speci,0.058125
specif,0.005402
specimen,0.007000
specul,0.009393
speed,0.004616
sperm,0.022345
spindl,0.036139
spinner,0.011004
spong,0.024776
spot,0.005651
stabl,0.017771
stage,0.007884
stan,0.008848
standard,0.003040
start,0.005715
state,0.003950
stem,0.010148
steno,0.009566
stop,0.008919
strategi,0.008410
stream,0.005410
strong,0.009840
strongest,0.006549
structur,0.013188
studi,0.033400
subject,0.008615
success,0.002993
suddenli,0.006697
suggest,0.012641
superfamili,0.009306
surfac,0.008175
surround,0.004134
suscept,0.006508
swim,0.007333
swimmer,0.009306
swipe,0.011100
symbol,0.004098
synchron,0.022243
tail,0.013071
talent,0.006671
tank,0.006253
target,0.004203
task,0.012563
tear,0.007240
techniqu,0.003469
televis,0.005088
temporari,0.005324
tend,0.003707
terrestri,0.006146
test,0.047977
thank,0.006208
themselv,0.016743
theori,0.002646
therefor,0.006136
theyd,0.022627
theyv,0.009430
thi,0.040525
thicker,0.008606
thing,0.003592
think,0.011744
thirtythre,0.009715
threat,0.004735
throw,0.006932
thu,0.002638
time,0.025966
titl,0.004052
togeth,0.003112
took,0.007053
tool,0.010844
topic,0.003581
toroid,0.010598
touch,0.017874
track,0.004994
train,0.011694
transmiss,0.011153
travel,0.015944
treeshrew,0.012465
trial,0.005151
trick,0.007545
truncatu,0.012465
tucuxi,0.013069
turn,0.003120
tursiop,0.012235
twist,0.007406
tyack,0.012235
typic,0.003028
uncategor,0.012465
understand,0.012257
underwat,0.007278
unexpect,0.006592
uniqu,0.015397
unit,0.004553
univers,0.007014
unknown,0.004673
unlik,0.007518
unsurpass,0.010067
unusu,0.010946
uplift,0.016156
use,0.033203
usual,0.005433
valid,0.004310
vari,0.006655
variabl,0.004126
variat,0.004231
variou,0.005139
ven,0.010067
ventral,0.009111
veri,0.012890
versu,0.005269
vertebr,0.006707
veterinari,0.007804
vigil,0.008783
vision,0.009779
visual,0.028578
vocal,0.035260
volunt,0.006223
voluntari,0.012491
von,0.004578
vortex,0.017696
vortexr,0.013498
wa,0.031467
wander,0.007564
washington,0.004970
water,0.006964
wave,0.027315
way,0.002413
wed,0.007630
week,0.004744
weight,0.009100
welldefin,0.006728
western,0.003395
whale,0.098456
wherea,0.007834
whistl,0.137876
whitehead,0.007439
whitlow,0.012737
wide,0.008722
wider,0.005140
wild,0.005791
wolz,0.012465
word,0.003133
work,0.002213
wrap,0.007564
wrong,0.005309
wursig,0.013069
xitco,0.040495
year,0.011391
young,0.004191
