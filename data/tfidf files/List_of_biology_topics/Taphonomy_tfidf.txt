abl,0.006689
abund,0.032219
accord,0.005295
accumul,0.030479
accur,0.009022
acid,0.010670
activ,0.010568
actual,0.013555
addit,0.005357
admix,0.024157
affect,0.013841
agent,0.009029
alga,0.029303
algaenan,0.027860
aliphaticaromat,0.027860
allochthon,0.027860
allocthon,0.027860
allow,0.005266
alter,0.018999
anaerob,0.034404
ancient,0.030692
andor,0.009267
ani,0.004667
anim,0.044476
animalpollin,0.026975
anoth,0.010187
anoxia,0.026289
anyth,0.010466
apart,0.009705
appar,0.009040
appli,0.005661
april,0.008101
archaeolog,0.011339
archaeologist,0.014596
area,0.021039
arguabl,0.012907
articul,0.025580
ash,0.014308
ask,0.008754
assemblag,0.097337
assess,0.009020
associ,0.010346
astrobiolog,0.017920
atom,0.009260
attribut,0.008232
autochthon,0.043752
autotroph,0.018449
averag,0.007910
bacteria,0.012158
base,0.004614
bauplan,0.027860
becaus,0.019228
becom,0.020447
bed,0.013791
befor,0.010907
began,0.006376
behavior,0.014981
belhaven,0.025254
believ,0.006604
benthic,0.021465
better,0.021603
bia,0.121767
bias,0.119095
bibliographi,0.009287
biggest,0.011988
bioeros,0.069369
biogeochem,0.017801
biolog,0.037867
biominer,0.065199
biomineralis,0.026975
biopolym,0.018596
biosignatur,0.023350
biospher,0.026626
biostratigraphi,0.022712
biostratinomi,0.027860
biota,0.067359
biotic,0.015269
bioturb,0.022026
bodi,0.032039
bone,0.023954
break,0.008802
broadscal,0.022183
burgess,0.018354
buri,0.012626
burial,0.043545
c,0.018334
calcit,0.019894
cambridg,0.008470
capabl,0.008422
captur,0.008781
carbon,0.020847
carcass,0.037943
catastroph,0.012692
caught,0.013171
caus,0.028542
cell,0.009198
centuri,0.010983
certain,0.005853
chanc,0.030887
chang,0.019287
characterist,0.007371
chemic,0.032575
chemolithoautotroph,0.025254
chemotroph,0.021733
chordat,0.021596
clam,0.057442
climat,0.009132
coast,0.009761
collecio,0.027860
collect,0.018717
colleg,0.009284
column,0.012151
come,0.006117
common,0.015516
commun,0.005593
compil,0.011035
complet,0.005907
compon,0.007366
composit,0.008867
compound,0.009638
comprehens,0.009422
concentr,0.008370
concern,0.006005
conclus,0.009531
condens,0.012376
condit,0.035911
consequ,0.007273
consist,0.017175
contain,0.024546
contemporan,0.015933
context,0.007524
continent,0.011392
contrast,0.007225
control,0.005683
convers,0.008825
cook,0.012060
correct,0.008614
correl,0.009970
cover,0.014085
creation,0.007786
crinoid,0.024844
critic,0.006292
crosslink,0.037610
crucial,0.010413
curios,0.015286
current,0.016429
cutan,0.041558
cuticl,0.102903
cutin,0.023864
cyclic,0.013286
d,0.013850
data,0.006826
dead,0.010724
death,0.007492
decay,0.047048
decomposit,0.041897
degli,0.021733
degrad,0.012271
degre,0.021016
deploy,0.011576
deposit,0.182294
depth,0.011231
describ,0.005265
descript,0.007819
destroy,0.008945
destruct,0.020248
determin,0.011881
develop,0.004278
di,0.013313
diagenesi,0.021596
diagenet,0.026975
differ,0.013604
differenti,0.008661
digest,0.013069
discard,0.014308
discern,0.014056
discontinu,0.012987
discov,0.007533
disintegr,0.013988
dissolut,0.012891
distinguish,0.007594
disturb,0.012012
divers,0.008227
dna,0.011040
doe,0.011576
doi,0.011325
domin,0.021117
donovan,0.020779
draw,0.008769
driven,0.009958
dune,0.016839
dure,0.004924
dwell,0.015135
earth,0.016170
earthscienc,0.024157
eas,0.011856
easi,0.020918
easili,0.026469
ecolog,0.019410
ed,0.007209
effect,0.010006
efremov,0.027860
element,0.006549
els,0.010605
elsewher,0.010617
emig,0.027860
en,0.012656
encontr,0.027860
encrust,0.021733
endem,0.028692
energi,0.014419
entir,0.013360
environ,0.034134
episod,0.025297
equival,0.008295
eros,0.027776
especi,0.023921
etholog,0.016390
event,0.026808
evid,0.014045
evolut,0.016723
evolv,0.008453
examin,0.008060
exampl,0.013837
excav,0.013845
exclud,0.009688
exhum,0.021465
exot,0.013718
experi,0.012666
experienc,0.009245
experiment,0.017204
explor,0.015693
explos,0.011416
expos,0.020632
exposur,0.011445
extent,0.016948
extern,0.004027
extract,0.019827
extraterrestri,0.016063
extrins,0.017469
fact,0.013077
factor,0.019590
far,0.006983
faster,0.010743
fauna,0.013137
favour,0.010127
ferrara,0.020052
fidel,0.031224
field,0.011248
floor,0.012527
flora,0.013921
fluviolacustrin,0.025254
focus,0.007169
forc,0.005519
foremost,0.014384
forens,0.014751
form,0.008418
format,0.007511
fossil,0.608754
fossilis,0.041767
frequenc,0.010524
fresh,0.012370
fulli,0.008302
fungi,0.013331
furthermor,0.009437
gap,0.021440
geochemistri,0.016783
geolog,0.031573
global,0.007313
goal,0.007574
gradual,0.008852
graviti,0.012006
greater,0.013872
greatest,0.009691
greek,0.007798
greenwood,0.015426
group,0.009935
ha,0.022032
habit,0.035028
habitat,0.038532
hand,0.006823
hard,0.036154
harvard,0.010759
hazen,0.023597
heat,0.009710
held,0.006511
herefordshir,0.026289
hiatu,0.018044
higher,0.006546
highli,0.007249
highstand,0.027860
histori,0.004586
howev,0.017877
human,0.026528
hundr,0.008367
ichnolog,0.024844
ideal,0.008960
identifi,0.013314
imag,0.016959
implic,0.009595
import,0.014090
includ,0.026760
incomplet,0.012000
increas,0.005295
inde,0.009363
indic,0.006774
individu,0.005680
inert,0.015574
infer,0.010583
inflat,0.010371
inform,0.005474
initi,0.005983
inorgan,0.012875
instanc,0.007458
interconnect,0.013313
interconnected,0.019087
intern,0.004825
interpret,0.014694
intrins,0.011660
intris,0.027860
introduc,0.006460
introduct,0.007300
invertebr,0.014624
involv,0.010677
itali,0.009910
ivan,0.014809
januari,0.007859
jaw,0.016365
journal,0.007083
k,0.008349
kerogen,0.022525
key,0.007007
know,0.008343
known,0.004601
konzervatlagersttten,0.027860
l,0.008105
laboratori,0.009343
lack,0.014041
lagersttt,0.027860
lake,0.021719
larg,0.009704
largerscal,0.019270
largescal,0.010728
largest,0.007604
late,0.006374
law,0.005546
layer,0.021196
lead,0.005493
leav,0.015593
lesser,0.011287
lichen,0.018086
life,0.029592
lignin,0.019818
like,0.024588
likeli,0.022712
likelihood,0.025535
link,0.004003
list,0.010238
lithospher,0.016783
littl,0.014092
live,0.029764
longterm,0.019376
low,0.007055
lyman,0.020394
m,0.013168
macrofossil,0.024157
major,0.014652
make,0.009363
mani,0.024966
mar,0.062990
marin,0.021078
mark,0.007391
mass,0.007549
materi,0.019353
mean,0.025350
meaning,0.011965
mediat,0.011277
meet,0.007448
megabia,0.027860
megabias,0.027860
method,0.005701
microbi,0.015320
microorgan,0.013749
millennia,0.013414
miner,0.010192
minor,0.008519
misconcept,0.016176
mission,0.009179
mix,0.017709
model,0.005734
molecul,0.019881
moreov,0.010561
morpholog,0.025224
motiv,0.008993
moult,0.022712
mudslid,0.044700
multipl,0.007080
nasa,0.041697
natur,0.004914
necessari,0.007336
net,0.019354
new,0.008224
nomo,0.019601
noncontemporan,0.055720
nondeposit,0.024844
nonsclerotis,0.027860
number,0.009210
object,0.012586
obliter,0.020134
obscur,0.012752
observ,0.006075
obtain,0.007155
obviou,0.011628
obvious,0.029416
occur,0.017546
oceanograph,0.018401
odontogriphu,0.027860
onc,0.006878
onli,0.025426
opportun,0.008695
oppos,0.007693
order,0.005073
organ,0.150644
origin,0.010203
otherwis,0.008756
outlin,0.008960
outofhabitat,0.027860
overabund,0.022910
overrepres,0.097661
p,0.021082
palaeoecolog,0.045424
paleobiolog,0.021596
paleoceanographi,0.021103
paleoecolog,0.019464
paleontolog,0.030336
paleontologist,0.073796
palynolog,0.019894
particular,0.011524
particularli,0.012912
pass,0.007151
past,0.007303
pathway,0.012363
pattern,0.007854
pdf,0.010654
peataccumul,0.027860
peculiar,0.014333
perhap,0.008557
period,0.027350
phenomena,0.008789
phosphat,0.014234
photoautotroph,0.022712
phyla,0.018699
physic,0.018071
pikaia,0.025254
place,0.015618
plain,0.012030
planet,0.020890
plankton,0.017961
plant,0.063940
play,0.013293
pollen,0.047675
polychaet,0.021219
polysaccharid,0.034277
poor,0.009102
poorer,0.015038
popul,0.013099
potenti,0.034172
prebiot,0.020218
predat,0.011718
predict,0.007724
presenc,0.007982
present,0.015835
preserv,0.213544
press,0.018639
pressur,0.007946
primari,0.006968
probabl,0.015149
problem,0.011291
process,0.044914
produc,0.021970
product,0.005501
prone,0.029165
properti,0.006391
protein,0.010724
provid,0.004683
proxi,0.014794
quantifi,0.011862
question,0.006629
r,0.014128
rare,0.017375
rate,0.019625
reaction,0.008584
read,0.005814
readili,0.011455
recollect,0.016783
record,0.119969
recordjun,0.027860
redeposit,0.023350
reduc,0.012685
refer,0.006695
reflect,0.007194
rel,0.042020
relat,0.018056
reli,0.008067
remain,0.077519
remov,0.007729
render,0.021960
report,0.006434
repres,0.021967
requir,0.005270
research,0.010862
resist,0.017351
resolut,0.020483
respect,0.006366
result,0.004663
review,0.007501
rework,0.018002
rip,0.017880
rippl,0.018262
river,0.009153
robert,0.007361
rock,0.031453
role,0.017594
root,0.008457
rough,0.013277
rover,0.018129
russian,0.009801
s,0.009496
say,0.007253
scale,0.022733
scaveng,0.051416
scienc,0.005630
scientist,0.007655
scleritis,0.027860
sclerobiont,0.053951
scope,0.009842
scour,0.022525
sea,0.017084
search,0.042846
secular,0.011587
sediment,0.074048
sedimentari,0.066694
sedimentolog,0.041356
select,0.007280
separ,0.006116
septemb,0.008073
seri,0.006669
shale,0.018449
shallow,0.014161
shape,0.007979
shelf,0.016341
shell,0.033806
shellless,0.026975
shift,0.016227
shipman,0.024481
short,0.007138
shorter,0.011919
shown,0.008073
silurian,0.020580
sinc,0.004659
singl,0.012471
site,0.023545
slope,0.013451
slow,0.019239
small,0.017104
smaller,0.015845
soft,0.071376
softtissu,0.021339
sometim,0.006065
sourc,0.035165
spatial,0.010892
spatiotempor,0.018449
speci,0.059985
spore,0.034031
sporopollenin,0.023597
stabl,0.009169
stormier,0.027860
structur,0.005443
studi,0.039393
subfield,0.011994
subject,0.005927
substanc,0.009796
substrat,0.028300
success,0.006179
suggest,0.006523
sulfat,0.016673
sunk,0.016728
surfac,0.016873
surg,0.014384
surviv,0.040180
tapho,0.051457
taphonom,0.180101
taphonomi,0.362779
taphonomist,0.027860
taxa,0.016086
taxonom,0.030237
taylor,0.011724
tecton,0.014751
tegellaar,0.027860
tempor,0.012278
tend,0.007651
term,0.004402
test,0.007617
th,0.005342
therefor,0.012666
thi,0.031366
thought,0.019810
thousand,0.016434
thu,0.032680
time,0.037103
timeaverag,0.070052
tissu,0.101334
topic,0.007392
tough,0.015518
trace,0.008934
transit,0.008111
transport,0.007987
true,0.007544
turbid,0.021876
turnov,0.014975
twentieth,0.010992
type,0.033611
typic,0.006251
ubiquit,0.014694
unconform,0.023864
undergon,0.014542
understand,0.012649
undoubtedli,0.016440
unfortun,0.013433
unit,0.004699
univers,0.009651
universit,0.017762
unless,0.010441
unseen,0.017576
unstabl,0.012430
usual,0.016821
valencia,0.019601
vari,0.013737
variou,0.005304
veri,0.021284
versu,0.010876
vertebr,0.027690
video,0.010255
volcan,0.013367
wa,0.015281
wall,0.019732
water,0.028749
websit,0.008810
wetland,0.016490
wilson,0.023278
windpollin,0.026975
wood,0.010751
wooster,0.023597
work,0.004567
wrong,0.021918
year,0.009404
