abil,0.012208
abl,0.016811
abov,0.005679
absenc,0.008156
accept,0.011139
accident,0.011475
account,0.005225
accumul,0.017022
acquir,0.014452
acquisit,0.009592
act,0.015211
actual,0.011356
adapt,0.021194
add,0.008459
addit,0.008976
address,0.006680
adopt,0.006122
advanc,0.005479
advantag,0.020659
adzuki,0.022023
affect,0.017392
align,0.019168
allel,0.184939
allow,0.017647
alter,0.007958
ambigu,0.010424
american,0.005026
analog,0.007927
ancestor,0.019070
ancestr,0.023196
ancestri,0.012182
ani,0.011729
anim,0.043468
anoth,0.012801
antibiot,0.025039
antievolut,0.019992
anyon,0.009672
ape,0.027628
appear,0.010277
argu,0.005901
aris,0.007237
aros,0.008810
artifici,0.022475
ascertain,0.012776
associ,0.004333
attain,0.008980
authorit,0.012381
avoid,0.006626
bacteri,0.011589
bacteria,0.040740
baramin,0.023339
baraminolog,0.023339
barrier,0.009026
base,0.015462
basepair,0.017241
basesdu,0.019767
basi,0.005526
bdelloid,0.021156
bean,0.012431
becaus,0.004026
becom,0.017129
beetl,0.014254
befor,0.004568
begin,0.005216
behavior,0.006275
believ,0.005532
belong,0.007558
benefici,0.019634
berkeley,0.010447
biolog,0.031722
biologist,0.010009
book,0.010168
botan,0.013062
botanist,0.012706
branch,0.005688
break,0.007373
breed,0.010727
breeder,0.016362
brought,0.006594
callosobruchu,0.022023
carri,0.011551
case,0.013439
caus,0.028692
cell,0.030821
cellular,0.009913
central,0.005022
cerevisia,0.015893
chanc,0.008624
chang,0.092905
charact,0.007403
characterist,0.006175
charl,0.006862
chemic,0.020466
china,0.006883
chinensi,0.020812
chloroplast,0.014204
choic,0.007039
chromosom,0.128081
circumstanti,0.017010
cite,0.007493
claim,0.016316
classic,0.005919
classifi,0.007247
close,0.004794
code,0.006933
color,0.008925
combin,0.010218
common,0.025997
commonli,0.005950
compar,0.015899
complet,0.009898
comprehend,0.012692
concept,0.005000
concern,0.005030
consid,0.004179
consist,0.004796
constantli,0.009989
contrari,0.009370
contrast,0.006053
contribut,0.005259
convers,0.007393
copi,0.025917
cornerston,0.012585
cost,0.012600
creat,0.026299
creation,0.045660
creationist,0.034020
crossov,0.028821
current,0.004587
damag,0.030933
darwin,0.030085
darwinian,0.013669
data,0.011438
daughter,0.009835
debat,0.013343
decad,0.006514
decis,0.006041
defens,0.007865
degre,0.005868
delet,0.012834
deleteri,0.014464
depend,0.009456
depress,0.008903
describ,0.035286
desert,0.010190
desir,0.006744
determin,0.004976
detriment,0.011775
develop,0.010751
development,0.020176
differ,0.091175
diffus,0.009790
disappear,0.009111
discoveri,0.006711
display,0.008144
disprov,0.012381
dissip,0.012748
distanc,0.007631
distinct,0.011038
distinguish,0.012724
diverg,0.038740
divers,0.020676
divid,0.005682
dna,0.194220
dnanevertheless,0.023339
dobzhanski,0.015893
document,0.006918
domain,0.013997
domin,0.017691
donkey,0.014785
dougla,0.010590
draw,0.007346
drift,0.121536
driven,0.008342
drosophila,0.014083
duplic,0.058781
dure,0.028875
earth,0.013546
ecolog,0.016260
effect,0.025147
eggderiv,0.023339
emerg,0.005529
emigr,0.010845
encod,0.010506
end,0.009434
englishspeak,0.011824
entir,0.011192
entomologist,0.016250
environ,0.005719
environment,0.006936
enzym,0.010488
epistat,0.018091
equilibrium,0.009016
error,0.106651
especi,0.010019
essenti,0.005997
establish,0.009208
eukaryot,0.035651
evad,0.013366
event,0.005614
eventu,0.006132
everi,0.010797
evid,0.005883
evolut,0.077054
evolutionari,0.059501
evolv,0.028327
exampl,0.030911
exchang,0.024395
experi,0.005305
explain,0.005807
explan,0.007594
extend,0.005690
extern,0.003373
eye,0.008624
fact,0.005477
factor,0.016411
fail,0.006486
fall,0.005889
famili,0.005974
far,0.005850
favor,0.007125
fertilis,0.015151
filipchenko,0.022023
fisher,0.011382
fit,0.007854
fli,0.009630
flow,0.081530
focu,0.006329
forg,0.011475
form,0.024684
format,0.018876
formless,0.035551
fraction,0.009030
frequenc,0.079348
frog,0.013143
function,0.023616
fundament,0.017723
fungi,0.011167
fuse,0.011357
futuyma,0.023339
gazett,0.013923
gene,0.447578
gener,0.023205
genet,0.274258
genom,0.052384
genu,0.011652
german,0.006515
given,0.004627
gradual,0.007416
grand,0.008961
grass,0.011625
gray,0.011509
great,0.020798
greater,0.017432
greenleaf,0.022023
groundbreak,0.013143
group,0.008323
grow,0.005915
ha,0.015381
happen,0.007291
harm,0.008894
held,0.005454
hered,0.013176
herit,0.028119
hi,0.024819
high,0.004671
higher,0.021938
highway,0.023287
hinder,0.023251
horizont,0.031917
hors,0.009965
howev,0.011232
human,0.026668
hundr,0.007009
hybrid,0.077107
hypermut,0.020509
idea,0.010355
ident,0.019817
immigr,0.018361
immun,0.010098
import,0.023608
imposs,0.007800
inbreed,0.015455
includ,0.006405
increas,0.008872
inde,0.007844
independ,0.004738
individu,0.019034
induc,0.018270
infertil,0.014978
insist,0.009412
instanc,0.012495
integr,0.005567
interbre,0.052755
interf,0.013143
intermedi,0.027200
introduc,0.016235
invers,0.009741
involv,0.004472
isol,0.022470
issu,0.004950
journal,0.005933
kimura,0.017494
kind,0.043015
known,0.003854
lack,0.005881
languag,0.005797
larg,0.012194
larger,0.012176
largerscal,0.016143
largescal,0.008987
later,0.004587
lead,0.018407
leavitt,0.034169
led,0.004977
level,0.013836
levelmacroevolut,0.023339
lie,0.007556
life,0.004957
light,0.006358
like,0.016478
limit,0.004710
line,0.005520
lineag,0.011415
link,0.006707
littl,0.005902
low,0.005910
macro,0.026877
macroevolut,0.221422
macroevolutionari,0.041018
main,0.004882
maintain,0.010755
major,0.008183
make,0.015687
mani,0.006971
manifest,0.008872
manmad,0.012394
mark,0.006192
mate,0.011562
materi,0.032426
mathemat,0.006062
mean,0.012741
measur,0.005051
mechan,0.011390
medicin,0.007353
meiosi,0.057119
melanogast,0.015081
metabol,0.010221
method,0.004776
microevolut,0.390264
microevolutionari,0.021156
migrat,0.024721
migratori,0.015224
million,0.012115
minor,0.014273
mismatch,0.013945
mistaken,0.012734
mitochondria,0.013967
mix,0.007418
mobil,0.024575
modern,0.018503
modul,0.010413
molecular,0.016121
morpholog,0.010565
motoo,0.017775
mountain,0.008297
mule,0.015046
multipl,0.011863
mutagen,0.050394
mutat,0.224877
mysteri,0.020359
natur,0.053523
neutral,0.042206
new,0.055117
nich,0.011671
night,0.009459
noncod,0.015415
noth,0.008160
novel,0.025606
nucleotid,0.012345
number,0.015432
numer,0.005713
object,0.005271
observ,0.025449
occas,0.009592
occasion,0.026099
occupi,0.007731
occur,0.063697
ocean,0.007936
offspr,0.046285
old,0.006381
onli,0.003550
oppon,0.019160
optim,0.008179
orderli,0.013513
organ,0.063099
origin,0.029917
otherwis,0.007335
pair,0.024655
parent,0.017370
particular,0.019309
particularli,0.010816
partner,0.016861
pattern,0.006580
percent,0.007745
percentag,0.008593
perform,0.005421
phenotyp,0.047535
place,0.004361
plant,0.046869
play,0.005568
point,0.004473
pollen,0.066565
polygen,0.017877
polymer,0.027628
polymeras,0.026989
polyploid,0.017877
polyploidi,0.036183
pool,0.050490
popul,0.153638
posit,0.004570
possess,0.006930
possibl,0.004510
postzygot,0.021553
potenti,0.005725
predict,0.006470
pressur,0.006656
prevent,0.006322
previous,0.006741
prezygot,0.021553
primari,0.005837
primarili,0.006045
probabl,0.006345
problem,0.004729
process,0.075252
produc,0.027608
product,0.009218
prokaryot,0.013143
promot,0.006308
prone,0.012216
proofread,0.049621
properti,0.016063
propon,0.009266
protein,0.035936
provid,0.003923
punctuat,0.013793
qualit,0.010253
quantit,0.008216
radiat,0.017713
random,0.016872
rang,0.010617
rank,0.007466
rapid,0.007664
rapidli,0.014911
rare,0.007277
rate,0.049323
raw,0.009149
readili,0.009596
reason,0.005033
receiv,0.005358
recent,0.009713
recombin,0.046648
reduc,0.015939
redund,0.012406
refer,0.005609
region,0.010044
regularli,0.009547
rekindl,0.017010
rel,0.015086
relat,0.003781
reli,0.006758
remain,0.013915
remaind,0.010407
remov,0.012951
repair,0.059909
replic,0.039598
reproduc,0.019030
reproduct,0.095272
requir,0.004415
resist,0.021803
respons,0.009729
result,0.019534
return,0.005676
rise,0.005643
rna,0.023107
robert,0.006166
role,0.009826
ronald,0.010018
rotif,0.018870
rubric,0.015081
russian,0.008211
s,0.007955
saccharomyc,0.016196
sampl,0.025074
scale,0.025392
scienc,0.009434
scientif,0.005643
scientist,0.012826
second,0.009206
section,0.006949
seed,0.009951
seen,0.005503
select,0.121975
sens,0.005771
separ,0.005124
sequenc,0.046717
sequencedupl,0.023339
set,0.008689
sever,0.012207
sexual,0.009216
shall,0.010237
share,0.010930
short,0.005980
signific,0.009965
similar,0.009322
sinc,0.015612
singl,0.010447
site,0.006574
slow,0.008058
small,0.019105
smaller,0.006637
sometim,0.015244
sourc,0.004909
speci,0.215363
special,0.004881
speciat,0.110859
specifi,0.007936
spread,0.013799
stage,0.013633
strain,0.009702
strand,0.022260
strongli,0.007523
structur,0.031923
studi,0.016500
subsequ,0.006091
substitut,0.008673
success,0.031058
suggest,0.010929
surviv,0.020196
synthesi,0.017429
systemat,0.007457
taxa,0.013475
taxonomi,0.011229
tend,0.012819
tenet,0.022984
term,0.033195
textbook,0.008798
theodosiu,0.014576
theori,0.018301
therebi,0.008079
therefor,0.005305
thi,0.055471
thoroughli,0.012369
thousand,0.006883
thousandfold,0.021156
thu,0.013688
time,0.024175
togeth,0.005382
toler,0.009876
total,0.016338
trace,0.007484
tradeoff,0.012585
tradit,0.004915
trait,0.018497
transfer,0.066795
transloc,0.015151
transport,0.006691
transposon,0.016798
tree,0.008040
type,0.014078
typic,0.015710
uc,0.013901
unabl,0.008111
unclear,0.010253
und,0.011483
underli,0.007412
understand,0.005298
union,0.005800
usag,0.008187
use,0.028704
usual,0.014091
uv,0.013278
valid,0.007453
valuabl,0.009163
vari,0.005754
variabilitt,0.022598
variabl,0.007135
variant,0.037822
variat,0.043901
veri,0.004457
versa,0.011078
viabl,0.021481
vice,0.009036
view,0.015008
vigor,0.011292
virus,0.058357
vision,0.016909
vs,0.009424
wa,0.032004
wage,0.008577
wall,0.008265
way,0.004173
weakli,0.013348
wellstudi,0.015415
whilst,0.010083
wild,0.010013
wind,0.009274
work,0.003826
world,0.003957
write,0.012206
year,0.007878
yeast,0.013046
young,0.007247
yuri,0.013923
