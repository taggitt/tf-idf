abl,0.007325
acid,0.011685
activ,0.005787
addit,0.011734
adhes,0.034806
aid,0.017401
allow,0.011534
alpha,0.013425
amino,0.015012
amphibian,0.034236
anim,0.032471
apicobas,0.030510
appear,0.006717
aquaporin,0.026455
aris,0.009461
artifici,0.009793
atpas,0.024872
axi,0.026601
ball,0.042966
base,0.005053
basi,0.007223
basolater,0.026455
becaus,0.010528
becom,0.016794
begin,0.027275
behav,0.024877
behavior,0.008203
beta,0.014303
bibliographi,0.010170
biolog,0.008293
blasto,0.028790
blastocoel,0.403060
blastocyst,0.123343
blastoderm,0.028176
blastomer,0.206733
blastula,0.490764
blastulastag,0.030510
cadherin,0.081621
cambridg,0.009276
cap,0.014267
capabl,0.009223
case,0.005856
catenin,0.028790
caviti,0.050801
cell,0.392848
cellcel,0.022988
cellular,0.038878
characteris,0.014040
cleavag,0.096642
clinic,0.012215
combin,0.006679
commit,0.009817
common,0.005664
compact,0.013820
compon,0.008067
compos,0.026661
consist,0.006269
contain,0.013440
continu,0.005676
control,0.018671
controversi,0.009231
creat,0.017190
creation,0.008527
crucial,0.022807
cullen,0.021872
cycl,0.038412
defin,0.012857
definit,0.007407
degener,0.016400
degrad,0.013439
demonstr,0.008420
depend,0.012362
deriv,0.007122
destroy,0.009796
develop,0.107757
differ,0.004966
differenti,0.028456
diffus,0.012798
diseas,0.019626
divis,0.024357
doe,0.012677
drosophila,0.055232
dure,0.032355
earli,0.039487
ecadherin,0.108828
ectoderm,0.023369
ed,0.007895
egg,0.027285
elimin,0.009753
embryo,0.296942
embryoblast,0.030510
embryogenesi,0.021543
embryolog,0.037202
embryon,0.051674
encourag,0.009269
encyclopedia,0.009713
end,0.006166
endoderm,0.046475
enter,0.008245
epcadherin,0.061020
epiblast,0.028790
epitheli,0.061606
epithelium,0.021103
equatori,0.016913
essenti,0.007840
establish,0.024076
eventu,0.008016
exclus,0.009635
experi,0.006935
express,0.029229
extraembryon,0.025841
eye,0.011274
facilit,0.010167
factor,0.021453
featur,0.007627
femal,0.011323
fertil,0.047288
fertilis,0.019807
fetu,0.036821
field,0.018477
floor,0.013718
fluid,0.011888
fluidfil,0.023800
forgc,0.029541
form,0.041488
format,0.041128
foundat,0.007864
fulli,0.009092
function,0.012349
furrow,0.023800
g,0.024476
gastrul,0.077524
gastrula,0.025090
gene,0.011701
gener,0.004333
genom,0.013695
germ,0.016267
given,0.006048
goe,0.021706
gradient,0.028699
greek,0.008540
group,0.005440
growth,0.022901
guid,0.008527
ha,0.008042
high,0.006106
hollow,0.017313
hormon,0.015628
human,0.005810
implant,0.081665
implic,0.021015
import,0.010287
improv,0.007421
includ,0.004186
increas,0.017398
indistinct,0.023650
induc,0.035826
induct,0.013355
infertil,0.019581
infobas,0.025572
injuri,0.026738
inner,0.036370
insemin,0.023110
instrument,0.009286
interact,0.030098
involv,0.005846
ion,0.013078
isbn,0.021388
junction,0.069252
ke,0.020653
known,0.005038
larg,0.005313
later,0.005996
layer,0.023212
lead,0.006015
lengthen,0.018981
life,0.006481
line,0.007216
lineag,0.029846
locat,0.007413
mainten,0.012094
make,0.005126
mammal,0.038290
mammalian,0.065669
mani,0.031898
manipul,0.010794
margin,0.011048
mark,0.008094
mass,0.016535
matern,0.064429
mbt,0.026134
mcgeadi,0.029541
mean,0.005552
mechan,0.007445
mediat,0.012350
medicin,0.009612
membran,0.014276
mesoderm,0.022334
mice,0.049201
microrna,0.022755
mid,0.010039
midblastula,0.091531
migrat,0.010772
misexpress,0.026455
mitosi,0.018803
morula,0.026134
mother,0.023237
mous,0.016557
movement,0.006951
mrna,0.093007
na,0.015245
nak,0.026134
necessari,0.008034
neural,0.013546
new,0.004503
newman,0.018634
nineti,0.018289
nonmatern,0.028790
nonpolar,0.020257
nuclei,0.014501
number,0.005043
occur,0.019215
onli,0.009281
organ,0.032994
origin,0.005587
osmot,0.019902
overal,0.009227
paracellular,0.029541
particular,0.006310
particularli,0.007070
pass,0.007832
pathway,0.013539
percent,0.010125
perform,0.007087
permeabl,0.018473
phase,0.029084
phenotyp,0.015535
physic,0.006596
placenta,0.020594
plate,0.013090
pluripot,0.091479
point,0.023392
polar,0.098805
posit,0.005974
potenti,0.037423
preced,0.010952
pregnanc,0.016797
preimplant,0.023956
press,0.006804
prevent,0.008264
primarili,0.023707
primit,0.012792
prior,0.017463
process,0.021860
produc,0.024060
protein,0.023488
prove,0.008922
provid,0.005129
reduct,0.010549
refer,0.010999
regen,0.020903
region,0.006565
regul,0.017359
repair,0.013052
research,0.005948
result,0.010214
retin,0.020903
retina,0.018981
rise,0.022133
role,0.006422
roof,0.017843
sac,0.019288
scienc,0.006166
seal,0.041248
serv,0.007298
set,0.011359
sever,0.010639
shift,0.008885
shown,0.008841
signal,0.021099
signific,0.006513
similar,0.006093
size,0.023838
smaug,0.030510
solidifi,0.016797
speci,0.028153
specif,0.024420
sperm,0.016835
sphere,0.022838
sprout,0.021172
stage,0.071288
start,0.006459
stem,0.068819
step,0.008652
stimul,0.011419
strategi,0.009505
structur,0.011923
stuart,0.014152
studi,0.005392
success,0.006766
sugar,0.012433
superovul,0.030510
surround,0.028033
synchron,0.016759
synthesi,0.011392
technolog,0.007357
term,0.004821
therefor,0.006935
thi,0.034349
thoma,0.009127
tight,0.062299
tissu,0.073982
transcrib,0.017160
transcript,0.045207
transit,0.053301
transplant,0.017034
transport,0.008747
treat,0.008935
treatment,0.009386
tremend,0.014826
trophectoderm,0.030510
trophoblast,0.053621
turn,0.007054
undergo,0.047323
undifferenti,0.019950
univers,0.005284
use,0.004169
usual,0.006140
uteru,0.020421
variou,0.005808
veget,0.012181
veri,0.005827
vertebr,0.015161
veterinari,0.017640
vision,0.011052
vitro,0.017640
vivo,0.018259
volum,0.008740
wa,0.004183
wall,0.010804
water,0.015742
widen,0.015881
wileyblackwel,0.018803
xbu,0.030510
xenopu,0.189204
yolk,0.021703
zone,0.010546
zygot,0.018734
