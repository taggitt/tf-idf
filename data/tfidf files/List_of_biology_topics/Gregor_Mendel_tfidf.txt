abbey,0.063595
abbot,0.069194
abbrevi,0.008020
abomin,0.012519
accept,0.008268
access,0.004556
accus,0.013208
acknowledg,0.006349
acr,0.008813
action,0.008092
adjust,0.006303
administr,0.004604
adolar,0.017323
age,0.008248
aggress,0.007337
agre,0.004964
agreement,0.005026
agricultur,0.004894
aim,0.004702
allan,0.009076
alleg,0.006809
allow,0.003274
alway,0.004428
analys,0.006438
analysi,0.003969
analyz,0.005535
angular,0.008881
anim,0.018437
annoy,0.011327
anoth,0.003167
antarctica,0.009817
anton,0.009548
apicultur,0.015021
apomict,0.014839
appear,0.003814
applic,0.003932
appreci,0.007306
approach,0.011369
approxim,0.004487
archiv,0.006060
aris,0.005372
arnulf,0.014519
asexu,0.010599
ask,0.005443
assert,0.011314
assort,0.010176
astronomi,0.007393
attempt,0.003756
attend,0.006283
audiobook,0.010285
augustinian,0.047775
austriahungari,0.010951
austrian,0.023663
author,0.007415
averag,0.004918
awf,0.014839
base,0.002869
bateson,0.021949
becam,0.010888
becaus,0.014945
becom,0.006357
bee,0.055872
beekeep,0.013513
began,0.007929
bei,0.013602
benefit,0.009636
ber,0.010238
bertram,0.012145
better,0.004477
bia,0.022714
bias,0.008228
bibliographi,0.011549
biographi,0.015318
biolog,0.014127
biologist,0.022287
biometr,0.012021
biometrician,0.027588
bishop,0.008126
blend,0.016647
book,0.003773
border,0.005849
born,0.016827
botan,0.009695
bred,0.021160
brnn,0.028245
brno,0.103883
built,0.004949
burn,0.020419
c,0.007600
came,0.008696
career,0.006523
carl,0.019918
carniolan,0.015998
case,0.006650
cathol,0.012841
centuri,0.020489
certain,0.007279
certifi,0.017353
characterist,0.009167
charl,0.010186
childhood,0.008498
christian,0.005153
chronic,0.008174
cite,0.011124
civil,0.004561
claim,0.008073
clericscientist,0.015222
close,0.010675
closer,0.006580
coat,0.007878
coin,0.005507
colleagu,0.007188
collect,0.007759
color,0.039751
combin,0.007585
commun,0.003477
compos,0.005046
comprehens,0.005858
conclud,0.016319
conduct,0.018234
confirm,0.011468
conform,0.007341
consid,0.003101
consider,0.004469
consum,0.005296
continu,0.003222
contrast,0.004493
controversi,0.005241
convinc,0.007080
cook,0.007499
correct,0.005356
corren,0.027205
correspond,0.004828
countri,0.003500
credit,0.005147
critic,0.003912
criticis,0.007829
cross,0.005805
crossbreed,0.013052
crundal,0.017323
cultiv,0.006793
cyprian,0.014122
czech,0.059667
daniel,0.013231
darwin,0.022331
data,0.012734
dataset,0.009780
day,0.004013
dearest,0.016773
death,0.004658
debat,0.004951
decad,0.009670
definit,0.004205
deliber,0.014240
demonstr,0.009562
denot,0.006490
depart,0.004663
describ,0.003273
design,0.003906
desir,0.005006
detect,0.005841
determin,0.003693
deviat,0.007548
did,0.019742
die,0.010193
diebl,0.017323
differ,0.005639
digit,0.005939
diminish,0.007201
direct,0.003532
disciplin,0.004901
discontinu,0.008075
discov,0.004684
discoveri,0.004981
discuss,0.004340
display,0.006045
disprov,0.009190
disput,0.016050
divers,0.005115
doctor,0.006347
domain,0.005194
domin,0.030639
doppler,0.011300
doubt,0.006864
dowri,0.012519
dublin,0.009855
duplic,0.008726
dure,0.009185
earli,0.012811
educ,0.008159
edward,0.005825
effect,0.003111
eho,0.034647
elev,0.007144
empir,0.008936
enabl,0.005078
encyclopedia,0.005515
end,0.014005
english,0.008920
enter,0.014044
entri,0.006194
envisag,0.010285
erich,0.019895
error,0.006089
especi,0.011155
essay,0.006205
essenti,0.004451
establish,0.006835
evid,0.004366
evolutionari,0.006309
exact,0.006109
exam,0.018362
examin,0.005012
exampl,0.005736
expect,0.030799
experi,0.059069
experiment,0.021396
explain,0.017243
extens,0.004324
extern,0.002504
extrem,0.009187
f,0.018868
fabric,0.008010
fact,0.004065
factorsnow,0.016773
faculti,0.007397
fail,0.009629
fairbank,0.026694
falsif,0.010929
falsifi,0.009351
famili,0.013302
farm,0.006389
farmer,0.006778
father,0.005630
favor,0.010578
februari,0.005202
felt,0.006760
filial,0.012571
final,0.004142
financi,0.004571
fisher,0.076037
fit,0.005830
flock,0.010580
flower,0.023329
focus,0.013373
folia,0.015222
fond,0.010951
formal,0.004284
founder,0.006001
franklin,0.007833
franz,0.027126
fraud,0.007887
frequenc,0.006544
friar,0.044472
friedrich,0.014008
funer,0.009370
gain,0.004166
garden,0.015318
gave,0.004982
gcse,0.012145
gene,0.013288
gener,0.014763
genesin,0.017323
genet,0.063978
genotyp,0.009842
german,0.019344
germanspeak,0.021769
given,0.003434
good,0.003807
govern,0.003256
great,0.003859
green,0.029445
greenhous,0.009515
gregor,0.126300
ground,0.004982
group,0.003088
gutenberg,0.009172
gymnasium,0.023127
ha,0.006850
hartl,0.027391
hartmann,0.011501
hawkwe,0.069294
head,0.004420
heavili,0.005520
hectar,0.010087
height,0.013655
heinzendorf,0.017323
held,0.004048
help,0.003748
hered,0.058681
hereditari,0.016391
heterozyg,0.012277
heterozygot,0.037111
hi,0.110532
hieracium,0.016773
high,0.006934
histori,0.008556
hive,0.012188
homozyg,0.011761
homozygot,0.025476
honeybe,0.013428
hous,0.004593
howev,0.008337
hugo,0.017437
hybrid,0.028616
hynic,0.017323
idea,0.007686
ident,0.004903
ignor,0.005939
ill,0.013298
impact,0.004692
implaus,0.022388
import,0.002920
impos,0.005889
incorrect,0.008153
incorrectli,0.009264
increas,0.003292
independ,0.017585
infer,0.006580
inher,0.006562
inherit,0.063498
initi,0.003720
inspir,0.005665
instanc,0.004637
institut,0.010870
internet,0.005617
interpret,0.009136
investig,0.004798
invis,0.008358
j,0.004300
jan,0.007525
janek,0.016346
januari,0.009773
jasper,0.011595
johann,0.036637
join,0.005093
juli,0.004946
julyseptemb,0.016346
jw,0.011796
karl,0.019320
known,0.011444
l,0.005039
laboratori,0.005810
lack,0.004365
larg,0.003017
later,0.020428
law,0.024141
led,0.007389
legaci,0.007198
leo,0.008569
librivox,0.010543
life,0.007360
lifetim,0.007330
like,0.006115
link,0.002489
linkag,0.009025
list,0.003183
littl,0.008762
live,0.003701
local,0.003877
locat,0.004209
london,0.009640
longer,0.004721
lost,0.004974
macmillan,0.007820
major,0.006074
make,0.002911
man,0.010129
mani,0.010349
march,0.004747
margravi,0.014839
mark,0.004596
masaryk,0.014839
match,0.006087
matern,0.009145
mathemat,0.004500
max,0.006995
meet,0.004631
mendel,0.858667
mendelian,0.074462
mendeliana,0.017323
mention,0.005542
meteorolog,0.025182
mice,0.009312
mix,0.005506
mndl,0.017323
modern,0.017167
monasteri,0.034791
monica,0.011868
monk,0.008467
month,0.005057
moravia,0.036309
moravian,0.012418
moraviansilesian,0.017323
multipl,0.004402
museum,0.013805
napp,0.062813
natur,0.009167
naturforschenden,0.016346
nearli,0.004985
nephriti,0.015998
nestler,0.051971
newspap,0.007086
ngeli,0.015448
nineteen,0.010269
notabl,0.004392
notion,0.005415
novel,0.006335
oat,0.022186
object,0.003913
observ,0.003778
obtain,0.004449
occur,0.003636
occurr,0.007404
odrau,0.017323
offspr,0.034355
older,0.012014
olomouc,0.029039
onlin,0.005010
opava,0.015703
opposit,0.004710
oral,0.014794
order,0.003154
organ,0.003122
origin,0.009517
outlier,0.012103
palack,0.016346
pangenesi,0.013052
paper,0.028339
parent,0.025786
particular,0.003583
particularli,0.004014
pass,0.008894
pay,0.010688
pea,0.157082
pearson,0.009017
perhap,0.005321
pflanzenhybriden,0.014122
phenomena,0.005465
phenomenon,0.011790
phenotyp,0.061745
philosoph,0.005024
philosophi,0.009747
photograph,0.007878
physic,0.014982
pisum,0.014672
planck,0.008690
plant,0.109336
play,0.004132
pod,0.035181
polar,0.007012
porteou,0.016346
posit,0.003392
posthum,0.009351
practic,0.003511
preced,0.006219
predict,0.004802
present,0.003282
previou,0.009899
priest,0.007772
primari,0.004332
princip,0.005127
prioriti,0.006922
probabl,0.009420
process,0.006206
produc,0.006830
professor,0.011733
profound,0.007487
progeni,0.044085
project,0.004061
promin,0.005147
prove,0.005065
provid,0.002912
pub,0.009817
public,0.003305
publicis,0.011868
publish,0.018216
punnett,0.012857
purebr,0.027391
quantit,0.006098
quickli,0.010685
quit,0.005547
r,0.008784
ra,0.009441
ratio,0.053279
read,0.007231
real,0.004320
realiz,0.005820
reappear,0.010253
reason,0.003736
reassess,0.010471
rebuild,0.008761
receiv,0.003977
recent,0.003604
recess,0.037255
recogn,0.004463
recognit,0.005706
recommend,0.006136
reconstruct,0.006523
rediscov,0.009067
rediscoveri,0.040645
refer,0.008326
reginald,0.011246
reject,0.005132
relat,0.002806
religi,0.010635
remov,0.004806
repeat,0.006008
replac,0.004242
replic,0.007348
report,0.012003
reproduct,0.007071
republ,0.019543
research,0.010131
respons,0.003610
rest,0.004877
result,0.046397
return,0.004213
review,0.004664
rid,0.010044
rigor,0.007009
roman,0.005584
rosin,0.014246
round,0.019784
rule,0.003800
s,0.005905
said,0.004277
sampl,0.006203
sativum,0.014519
sceptic,0.009974
school,0.016941
schwirtlich,0.017323
scienc,0.014005
scientif,0.012567
scientist,0.009520
score,0.007630
second,0.003416
seed,0.044318
seen,0.004084
segreg,0.009110
select,0.004526
selffertil,0.014122
semin,0.007962
sent,0.005770
sermon,0.010436
settl,0.005916
seven,0.016388
sever,0.006040
sex,0.007208
shape,0.024809
sheep,0.016776
shock,0.007117
signific,0.003698
silesian,0.015021
sister,0.007996
size,0.004511
sketch,0.009292
slightli,0.006279
small,0.003545
societi,0.010889
son,0.011094
sought,0.005644
sourc,0.007288
span,0.006692
speci,0.010656
special,0.003623
spillman,0.016346
sponsorship,0.010525
spring,0.006535
st,0.028022
start,0.003667
state,0.002535
station,0.006329
statist,0.019013
strongest,0.008406
strongli,0.005584
struggl,0.006073
student,0.004931
studi,0.048989
substitut,0.006438
succeed,0.005923
success,0.003842
suggest,0.008112
support,0.003444
surviv,0.004996
switch,0.007113
synthesi,0.006468
taken,0.004242
tax,0.005444
taxat,0.007123
taylor,0.007290
teacher,0.033197
term,0.002737
terminolog,0.006822
test,0.009472
th,0.003322
theoret,0.004750
theori,0.023772
therefor,0.003937
theresia,0.032693
thi,0.023837
thirtyf,0.012519
thoma,0.015547
thomass,0.026244
thought,0.008212
thu,0.003386
tidi,0.014839
time,0.007690
tint,0.012062
today,0.004162
togeth,0.007990
took,0.009052
tour,0.008358
train,0.005003
trait,0.068647
tri,0.009120
troppau,0.017323
true,0.004691
truebreed,0.032693
tschermak,0.027391
turn,0.004005
twentieth,0.013670
twomonth,0.013269
unabl,0.006020
unawar,0.009421
understand,0.015730
understood,0.005437
uniformli,0.009441
univers,0.015003
unlik,0.009649
unrip,0.014839
unsuccess,0.007451
unsupport,0.011905
use,0.009469
usher,0.008873
variabl,0.005296
variat,0.010861
verein,0.013513
verhandlungen,0.015703
veri,0.003308
verifi,0.007323
veronika,0.015998
versu,0.013525
versuch,0.013513
vienna,0.016578
vigor,0.008382
virtual,0.005729
visibl,0.006404
visitor,0.007924
von,0.011752
vri,0.044572
w,0.004861
wa,0.092646
washbourn,0.017323
way,0.003097
weldon,0.013428
wherea,0.005027
whi,0.005330
wide,0.003731
william,0.009285
windl,0.015703
word,0.004021
work,0.059646
wrinkl,0.012370
write,0.004530
wrote,0.004936
year,0.011695
yellow,0.032549
young,0.005379
younger,0.007198
zumkel,0.017323
