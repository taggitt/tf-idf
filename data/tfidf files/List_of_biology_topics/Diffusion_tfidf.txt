aaldot,0.008040
abfrac,0.006835
absolut,0.010989
accord,0.003239
act,0.001851
action,0.001990
activ,0.004848
addit,0.001638
admixtur,0.017407
adolf,0.004468
adparticl,0.008521
adrm,0.008521
adsorpt,0.012589
advect,0.025681
aexp,0.007388
affect,0.004233
afrac,0.006210
aggreg,0.003425
ai,0.008138
air,0.029859
airway,0.005251
albert,0.006584
alveoli,0.099988
alway,0.002178
analog,0.008683
anatomi,0.003804
ani,0.005710
anim,0.002267
anisotrop,0.005820
annominu,0.008521
anomal,0.009853
anoth,0.006231
antigradi,0.017042
anu,0.005332
appear,0.001876
applet,0.005751
appli,0.012121
applic,0.001934
approach,0.014913
approxim,0.011037
arbitrari,0.003497
area,0.016087
arrang,0.002640
arriv,0.005214
assert,0.002782
assist,0.002415
assum,0.002243
atom,0.019826
atomist,0.030749
attempt,0.001847
averag,0.002419
avogadro,0.005785
avoid,0.002419
b,0.040764
background,0.005563
barodiffus,0.017042
base,0.002822
basic,0.003929
bcbnabla,0.008521
bcnabla,0.008521
becaus,0.011762
becom,0.001563
befor,0.003336
bfrac,0.006647
bia,0.003724
biolog,0.002316
biologist,0.003654
bipolar,0.005322
blood,0.022264
bnt,0.007007
bodi,0.011759
boltzmann,0.029602
book,0.001856
boundari,0.008180
boussinesq,0.007487
boyl,0.004854
bpi,0.015197
bracket,0.004659
breath,0.004199
british,0.002180
brought,0.002407
brown,0.006628
brownian,0.019572
btfrac,0.007487
btpi,0.008521
btsqrt,0.008521
bulk,0.045011
c,0.123367
calcul,0.004984
capillari,0.010503
carbon,0.012752
care,0.002457
carl,0.003265
case,0.004906
catalysi,0.005311
catalyst,0.004271
caus,0.012221
caviti,0.009458
cc,0.009538
ccfrac,0.007869
ccfxctdc,0.008521
ccldot,0.008040
ccnabla,0.008521
cdot,0.018191
cell,0.025319
cement,0.004313
center,0.002086
centuri,0.005039
ceram,0.004599
cfixctdc,0.008521
cfrac,0.011315
ch,0.004174
chandler,0.005322
chang,0.011798
chapman,0.004712
chapmanenskog,0.015448
charact,0.002703
character,0.002481
charg,0.007573
chemic,0.022417
chemistri,0.002867
chines,0.002811
chunk,0.005213
ci,0.004575
cicidelta,0.016081
cicinabla,0.017042
ciparti,0.025563
cite,0.002736
cixt,0.008521
cixtfrac,0.008521
cj,0.017356
classic,0.002161
clausiu,0.005856
cldot,0.008040
clearli,0.002941
clerk,0.009014
closest,0.003959
cm,0.004114
cn,0.004940
coarsegrain,0.006736
coauthor,0.004485
coeffici,0.086834
coin,0.002708
coincid,0.003519
collect,0.001908
collis,0.008036
cologn,0.005398
colour,0.003733
combin,0.005596
common,0.003163
compar,0.001935
compon,0.042809
concentr,0.138256
concept,0.005477
concern,0.001836
conclud,0.002675
condens,0.003785
condit,0.010983
conduct,0.008969
confus,0.005889
connect,0.004308
consequ,0.004449
conserv,0.005194
consid,0.007629
consist,0.001751
constant,0.015606
contact,0.002745
contain,0.001876
context,0.004603
continu,0.003170
contract,0.005249
contrast,0.002210
contribut,0.001920
control,0.001738
convect,0.034206
coordin,0.002773
copper,0.003633
correspond,0.011875
counterdiffus,0.008521
cours,0.002280
cowl,0.005856
cpartial,0.007007
creat,0.011202
crucial,0.003184
crystal,0.007340
current,0.011724
cx,0.012420
d,0.133438
da,0.003482
darci,0.011503
data,0.002088
db,0.005481
ddelta,0.007487
decoupl,0.005519
decreas,0.015459
deep,0.002912
defect,0.011586
defin,0.005386
definit,0.008275
delta,0.044970
demo,0.005642
demonstr,0.004703
densiti,0.056832
depend,0.003452
deriv,0.011936
describ,0.027376
descript,0.002391
develop,0.010468
dfrac,0.017922
diagon,0.004893
diamet,0.008549
differ,0.016644
differenti,0.002649
diffunder,0.008521
diffus,0.579059
diffusionlimit,0.007724
dij,0.017042
dijdjigeq,0.008521
dik,0.008040
dikfrac,0.008521
dimens,0.003010
dimensionless,0.005893
dioxid,0.011533
disciplin,0.002410
discov,0.002304
displaystyl,0.389619
distanc,0.011144
distinguish,0.002322
distribut,0.006476
dit,0.007007
div,0.006785
divmathbf,0.008521
divxjsum,0.008521
dmu,0.006736
dnabla,0.024122
dneq,0.008521
dni,0.007072
doe,0.001770
domin,0.002153
dope,0.011787
dpvtsqrt,0.008521
drift,0.004033
drive,0.011244
drm,0.006946
drop,0.005743
dsfrac,0.007388
dure,0.003012
dusum,0.008521
duti,0.003113
dynam,0.002452
earlier,0.002327
earth,0.002472
earthenwar,0.006835
econom,0.001723
eddi,0.005332
effect,0.009181
effort,0.002181
effus,0.006294
einstein,0.015191
einsteinteorel,0.017042
elabor,0.003277
elder,0.003870
electr,0.018232
electron,0.018451
electrostat,0.004823
element,0.004006
elementari,0.010660
ell,0.017063
end,0.005166
energi,0.017641
enhanc,0.002917
ensembl,0.009077
enter,0.006908
entir,0.002043
entropi,0.012824
eq,0.012710
equal,0.006435
equat,0.078594
equilibr,0.005802
equilibrium,0.013168
error,0.002995
essenc,0.003558
estim,0.002216
everyday,0.003364
evolut,0.002557
ex,0.004261
exactli,0.003085
exampl,0.016928
exchang,0.002226
exist,0.001566
exp,0.010582
expand,0.002228
expans,0.002657
experi,0.001937
experiment,0.007893
expir,0.004257
explain,0.004240
expon,0.004468
express,0.012245
extend,0.002077
extens,0.002127
extern,0.007390
eyr,0.005974
f,0.030163
fa,0.004990
facilit,0.002839
factor,0.003994
fals,0.003194
fe,0.004920
featur,0.002130
fi,0.010484
fick,0.115346
field,0.005160
figur,0.002332
financ,0.002495
finit,0.003257
fixct,0.008521
flight,0.003450
flow,0.022325
fluid,0.006640
flux,0.087019
fluxquantitytimearea,0.008521
follow,0.001347
forc,0.048960
form,0.006437
formal,0.004215
formalis,0.004947
formul,0.008092
formula,0.038238
fourier,0.004395
frac,0.079156
fraction,0.006593
frame,0.003218
framework,0.002413
free,0.025011
frenkel,0.025960
frequent,0.002367
function,0.003449
fundament,0.002156
furnac,0.005150
furri,0.006835
g,0.018229
ga,0.022670
gamma,0.004158
gase,0.060087
gaseou,0.004735
gener,0.008472
georg,0.002427
given,0.006757
glass,0.003712
goal,0.002316
goe,0.003031
gold,0.002929
govern,0.001601
gradfrac,0.025563
gradient,0.116225
gradnk,0.008521
graham,0.025753
grain,0.003659
gramion,0.017042
gramparticl,0.008521
gravit,0.003767
grow,0.004319
growth,0.002132
h,0.004679
ha,0.010108
heart,0.009275
heat,0.023760
heavier,0.008509
heaviest,0.005364
henri,0.002714
heterogen,0.004234
hevesi,0.007142
hi,0.007551
high,0.017055
higher,0.012014
histori,0.001402
hole,0.003833
hot,0.003709
howev,0.002734
hydrodynam,0.005213
icirho,0.008521
idea,0.009452
ideal,0.010962
idizdelta,0.008521
idiznabla,0.008521
ifrac,0.006158
igeq,0.007598
ik,0.017985
ileftnifrac,0.008521
imathfrak,0.008040
imini,0.008521
iminicixt,0.008521
immers,0.004854
implement,0.002345
import,0.001436
inci,0.008521
includ,0.007015
increas,0.004859
index,0.002675
indic,0.002072
individu,0.005212
infiltr,0.004763
inhomogen,0.011227
ini,0.006736
ink,0.005005
inner,0.003385
insid,0.002824
intens,0.005594
interact,0.002101
intern,0.002952
interstiti,0.006061
intim,0.004215
intrins,0.003566
introduc,0.011855
involv,0.001632
ion,0.025568
iparti,0.006785
irho,0.032163
iron,0.003016
irrevers,0.004649
isobar,0.006133
isol,0.002734
isotherm,0.028363
isotop,0.008160
isum,0.019261
ith,0.083637
iti,0.006454
itrm,0.008521
iv,0.003656
ivi,0.011676
iviwi,0.008521
ivnabla,0.008521
iwi,0.006647
ixisum,0.008040
iyi,0.007724
j,0.211530
jakovjacov,0.008521
jame,0.004751
java,0.004264
jdijcjdelta,0.008521
jdijcjnabla,0.008521
jdijdelta,0.008521
jditnabla,0.008521
jdpartial,0.017042
jeanbaptist,0.004566
jfrac,0.007072
jgeq,0.025563
jidfrac,0.008521
jlijxj,0.025563
jnabla,0.015738
jndijmathbf,0.008521
jntpartial,0.008521
jnv,0.008521
join,0.002505
jt,0.006016
jth,0.006690
jump,0.023333
junction,0.004835
just,0.003928
k,0.076609
kappa,0.005599
kb,0.010446
kgeq,0.014598
kinet,0.034312
kmu,0.007299
known,0.004221
knudsen,0.006324
knykfkfjright,0.008521
krm,0.037993
kt,0.005838
ktextbt,0.008521
ktfrac,0.008040
ktkappa,0.008521
ktmmpi,0.017042
l,0.019832
lam,0.006038
laplac,0.004835
lar,0.005398
larger,0.004445
later,0.003349
latin,0.002646
lattic,0.004199
law,0.042410
lead,0.008400
leav,0.004769
left,0.004489
leftdijfrac,0.008521
leftfrac,0.027715
leftsum,0.006565
length,0.013969
let,0.003155
level,0.001683
levi,0.003957
light,0.002321
lighter,0.008838
lij,0.016081
lijleftfrac,0.017042
lijrm,0.008521
like,0.001504
linear,0.020973
link,0.001224
liquid,0.012073
littleo,0.007487
ln,0.023875
lnnntexteq,0.008521
local,0.003814
locat,0.004140
long,0.005485
longer,0.002322
low,0.019422
lower,0.006242
ludwig,0.003866
lung,0.013210
m,0.106734
ma,0.003556
macroscop,0.004033
main,0.003565
manabla,0.016081
mani,0.001272
marian,0.006016
mark,0.004521
mass,0.041565
mathbf,0.089859
mathemat,0.006640
mathematis,0.007724
mathfrak,0.033454
matrix,0.028276
matter,0.004236
maxwel,0.008398
maxwellstefan,0.008521
mb,0.005311
mean,0.013956
measur,0.011065
mechan,0.008317
media,0.015496
mediat,0.003449
medium,0.009630
membran,0.007974
mesoscop,0.007007
metal,0.005861
metallurgist,0.007217
mexp,0.008521
mfrac,0.005785
mi,0.007100
miaisum,0.008521
micicixtfixctdc,0.008521
micicixtfixctdcright,0.008521
micixtint,0.008521
microscop,0.003635
millimet,0.005409
misconcept,0.004947
mix,0.005416
mixtur,0.031685
mmright,0.008521
mmrightleftfrac,0.008521
mnmnpmnmnff,0.008521
mnmnpmnmnffktfrac,0.008521
mobil,0.020936
mobilitybas,0.008521
model,0.014031
modern,0.005066
mole,0.035415
molecul,0.072969
molecular,0.014715
moment,0.006406
momentum,0.011105
monolay,0.006387
motion,0.034095
movement,0.021357
mrm,0.014598
mu,0.058037
multicompon,0.049902
multimolecular,0.008521
multipli,0.007259
mutual,0.002970
n,0.298382
nabla,0.032813
nanomet,0.005481
nanu,0.007598
natur,0.001503
nddleftfrac,0.008521
near,0.002315
nearest,0.004538
necessari,0.004487
need,0.001692
neg,0.007324
neglect,0.003683
neighbour,0.003439
net,0.017759
new,0.001257
ngamma,0.007724
ni,0.014864
niint,0.008521
ninjnabla,0.008521
nint,0.006835
niparti,0.023607
nixtint,0.008521
njnj,0.008040
njpartial,0.008040
njright,0.008521
nk,0.005687
nkrightnn,0.008521
nkrightnnrightdelta,0.008521
nkrightnnrm,0.008521
nm,0.009142
nmathbf,0.014014
nn,0.015966
nnabla,0.008040
nnmmnmnmnnabla,0.017042
nnndleftnabla,0.008521
nnnn,0.007869
nnominu,0.017042
nnright,0.008521
nnrightfrac,0.008040
noflux,0.008521
nominu,0.008521
nondiagon,0.030897
nonequilibrium,0.011536
nonid,0.006785
nonlinear,0.015430
nonperfect,0.008521
nontrivi,0.004873
normal,0.013466
notat,0.015134
notion,0.002663
npartial,0.037993
nrtnabla,0.008521
nsum,0.006108
nu,0.040226
number,0.005634
nxt,0.007869
nxtddelta,0.008521
nxtpartial,0.017042
nxtsum,0.008521
o,0.014846
object,0.003849
observ,0.003716
obviou,0.003556
occasion,0.003176
occur,0.001788
occurr,0.003642
ohm,0.005687
omit,0.004399
onc,0.002103
onli,0.003888
onnominu,0.008521
onsag,0.063873
oper,0.003552
opposit,0.002316
optic,0.003387
organ,0.001535
origin,0.001560
osmosi,0.005995
outsid,0.008592
oxid,0.003735
oxygen,0.047995
p,0.049436
pace,0.003799
panel,0.004149
partial,0.051803
particl,0.045399
particular,0.001762
partli,0.003027
path,0.017129
penetr,0.003761
peopl,0.001552
perfect,0.003145
perform,0.001979
permeabl,0.005159
peronamalik,0.008040
perrin,0.005893
persist,0.003192
pfrac,0.005532
phase,0.002707
phenomena,0.002688
phenomenolog,0.012773
phenomenon,0.005799
photon,0.004114
physic,0.018424
pi,0.008094
pkrm,0.008521
place,0.014331
plasma,0.008252
play,0.002032
pleftyjsum,0.008521
plini,0.004634
pmathbf,0.007072
pocl,0.008040
point,0.008166
pollen,0.004860
pore,0.005116
porou,0.032059
posit,0.011681
possibl,0.001646
potenti,0.020904
power,0.001652
present,0.003228
preserv,0.002721
pressur,0.063192
previous,0.002461
price,0.002390
prnt,0.008521
probabl,0.004633
probe,0.004015
process,0.013737
produc,0.001679
product,0.003365
proport,0.005339
propos,0.005976
psim,0.008521
pt,0.004810
pump,0.004060
q,0.007232
qnabla,0.008521
quadratur,0.006183
quantiti,0.026299
quasichem,0.017042
quotat,0.004288
r,0.023766
radiat,0.003233
radioact,0.003962
radioisotop,0.005444
random,0.043120
randomli,0.004364
rate,0.012005
ratio,0.011647
reaction,0.010502
reagent,0.020533
reciproc,0.003846
recogn,0.002195
refer,0.003071
region,0.014669
rel,0.003672
relat,0.004142
relev,0.002678
remain,0.001693
reorient,0.005354
repres,0.001679
repuls,0.009041
requir,0.001612
research,0.003322
respir,0.009248
result,0.005705
revers,0.002819
rho,0.038885
right,0.003675
rightfrac,0.006183
rigid,0.007620
rm,0.004867
rnu,0.008040
robert,0.004503
robertsausten,0.008521
role,0.001793
room,0.006643
rotat,0.003389
rtfrac,0.006946
rtright,0.008521
rtrightnabla,0.008521
rudolf,0.004208
rule,0.001869
s,0.026141
scalar,0.004585
scale,0.006953
schottki,0.007869
scienc,0.001722
scope,0.003010
sdelta,0.014598
second,0.005041
selfdiffus,0.025563
selfpropel,0.006133
semiconductor,0.008937
separ,0.009354
serv,0.002038
sever,0.002971
sh,0.005242
shine,0.004835
short,0.002183
sign,0.002346
silicon,0.004349
similar,0.001701
simpl,0.015850
sinc,0.001425
singl,0.003814
situat,0.002194
small,0.013951
smell,0.004654
smoluchowski,0.007142
snpartial,0.025563
socal,0.005751
sociolog,0.003376
solid,0.033691
sometim,0.001855
soon,0.002613
sorption,0.007869
sourc,0.003585
space,0.008788
spatial,0.003331
speci,0.013105
special,0.003564
speed,0.002914
sphere,0.006378
spontan,0.003683
spoon,0.006061
spray,0.005364
spread,0.007557
sqrt,0.013799
squar,0.003019
stain,0.004696
stale,0.006387
standard,0.005758
start,0.001803
state,0.006234
steel,0.003569
step,0.002416
stochast,0.003955
stop,0.002815
stress,0.002922
studi,0.012048
subject,0.001812
substanc,0.020973
suitabl,0.003163
sum,0.028427
superscript,0.006038
supplement,0.003528
surfac,0.020643
surround,0.002609
suspens,0.004427
symmetr,0.008116
systemat,0.008168
t,0.210762
task,0.002643
tconst,0.008521
tddelta,0.017042
temperatur,0.037453
tend,0.004680
teorel,0.076691
term,0.014813
termin,0.003339
textextern,0.025563
th,0.003268
themselv,0.002113
theoret,0.002336
theori,0.018374
therefor,0.013559
thermal,0.018271
thermodiffus,0.034085
thermodynam,0.051210
thi,0.033044
thoma,0.005098
thorac,0.011470
thu,0.001665
time,0.011348
tmathrm,0.006565
tnabla,0.047851
todelta,0.017042
total,0.009942
tpd,0.008521
tpdrm,0.008521
transfer,0.012193
transistor,0.005013
transport,0.039089
tright,0.013779
trm,0.015197
tsum,0.027787
turbul,0.013258
type,0.003426
typic,0.001911
u,0.006650
undergo,0.003304
underground,0.003950
undermost,0.008521
unit,0.007187
univers,0.001476
uppermost,0.006265
use,0.016302
usual,0.005144
util,0.002578
v,0.040657
vacanc,0.010665
valu,0.007306
variabl,0.002605
variat,0.002671
variou,0.008111
varphi,0.005953
vector,0.010339
veloc,0.044970
ventricl,0.011570
veri,0.003255
version,0.004985
vessel,0.003469
vfrac,0.012316
viciv,0.008521
view,0.003653
vileftsum,0.008521
violat,0.003153
viscos,0.005168
volum,0.009764
vs,0.003440
vt,0.005687
vtfrac,0.006785
w,0.016738
wa,0.012853
wafer,0.006265
wagner,0.005242
walk,0.032762
wall,0.003017
walter,0.003399
water,0.004396
way,0.001523
wellknown,0.003345
wendel,0.005519
wherea,0.004945
whi,0.002622
wi,0.005913
wide,0.001835
william,0.004567
word,0.005934
work,0.002794
wrong,0.003351
x,0.123642
xi,0.008783
xipip,0.008521
xirm,0.017042
xisum,0.007598
xj,0.012589
xjnabla,0.008521
xjrfrac,0.008521
xjxjyjnabla,0.008521
xnu,0.006785
xrm,0.008040
y,0.015174
yakov,0.006294
year,0.002876
yearold,0.004152
yirho,0.008521
z,0.027815
zbsum,0.008521
zc,0.007388
zeldovichra,0.008521
zero,0.005994
zinc,0.004489
zrich,0.005481
