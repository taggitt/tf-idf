abdomen,0.018179
abdomin,0.034574
abil,0.003752
absorb,0.027262
absorpt,0.019917
accessori,0.008875
account,0.006424
acetyl,0.010131
acid,0.027476
activ,0.008164
actual,0.003490
ad,0.003480
addit,0.005518
adject,0.007454
adult,0.010678
advent,0.005964
adventitia,0.053002
affect,0.028512
african,0.004780
aganglionosi,0.013539
agit,0.008110
aid,0.004091
alimentari,0.035725
alkalin,0.017230
allergi,0.009452
allow,0.002712
alongsid,0.005672
alway,0.003667
ambulatori,0.021621
amino,0.007059
amphibian,0.008050
anal,0.028972
anatom,0.014550
anatomi,0.044838
angiodysplasia,0.014348
ani,0.007211
anim,0.045811
anoth,0.005246
antibiot,0.007696
antibodi,0.015908
antigen,0.008629
antihelicobact,0.014348
antiport,0.011908
anu,0.062854
appar,0.009312
appear,0.003159
append,0.019207
appendix,0.033714
appetit,0.008961
approxim,0.003716
area,0.010835
arent,0.008573
ascend,0.042882
aspect,0.003584
assimil,0.007119
associ,0.005328
atla,0.006394
atrophi,0.009892
attach,0.010327
autocrin,0.012794
autoimmun,0.028504
autonom,0.005515
autopsi,0.009603
avoid,0.004073
b,0.003612
backward,0.006783
bacteri,0.014249
bacteria,0.056353
bacterium,0.025064
badminton,0.012152
ball,0.006735
barium,0.009657
barley,0.008331
barrier,0.011098
basal,0.008587
base,0.002376
basic,0.003308
bay,0.006159
becaus,0.004951
becom,0.007897
beef,0.007972
begin,0.012826
bendir,0.013539
benefici,0.012070
best,0.003624
bicarbon,0.021101
bilaterian,0.034531
bile,0.037529
biochem,0.006761
biopsi,0.010599
bird,0.016841
black,0.004438
bladder,0.009603
bleed,0.008794
blend,0.006894
bloat,0.010550
blockag,0.010285
blood,0.048200
bloodi,0.007720
bloodstream,0.018629
bodi,0.016500
boil,0.007356
bolu,0.011697
border,0.009689
boundari,0.004591
bowel,0.075058
bracket,0.007846
branch,0.003497
break,0.009066
breath,0.007070
brunner,0.010649
buccal,0.011192
bulb,0.008643
burn,0.005637
burp,0.013250
butyr,0.022849
buzz,0.010131
byproduct,0.007299
caecum,0.026012
cajal,0.010327
calf,0.020914
calfintestin,0.014348
calv,0.009861
camera,0.007119
canal,0.039198
cancer,0.048461
capnophil,0.014348
capsul,0.018179
carbohydr,0.007761
carcinoma,0.010990
cascad,0.007917
case,0.013770
cat,0.006987
catgut,0.013892
caught,0.013567
caus,0.014699
caviti,0.015927
cecum,0.048103
cell,0.037896
cellular,0.006094
certain,0.003014
chang,0.002483
character,0.004177
characterist,0.003796
chemic,0.004194
childhood,0.007038
chitterl,0.014348
cholecystokinin,0.012608
cholera,0.008859
chronic,0.013540
chyme,0.049765
cip,0.011799
circular,0.032097
circulatori,0.008629
clear,0.004099
clinic,0.005744
clinician,0.008643
cloaca,0.011343
clock,0.013788
closest,0.006667
clostridia,0.025216
cm,0.006927
coeliac,0.012152
colic,0.023598
coliti,0.070182
collect,0.003213
colon,0.116894
colonoscopi,0.011908
colorado,0.007769
colorect,0.021101
combin,0.003141
come,0.003150
common,0.015982
commonli,0.007316
compar,0.006516
compet,0.004541
complic,0.009985
compon,0.011381
compos,0.004179
compris,0.009013
concav,0.009250
concentr,0.004311
concern,0.003092
condit,0.021577
condom,0.010868
connect,0.025389
consensu,0.005495
conserv,0.008745
consid,0.002569
consider,0.003702
consist,0.023588
constant,0.008759
constip,0.042805
constitut,0.003478
contact,0.009245
contain,0.031604
content,0.008693
continu,0.002669
contract,0.004419
contrast,0.007442
contribut,0.009700
control,0.002926
coordin,0.004670
cord,0.023591
correctli,0.006330
correspond,0.003999
countri,0.002899
court,0.004178
cover,0.007254
cow,0.007597
crohn,0.032970
crop,0.005446
cure,0.013514
cypa,0.013892
cyst,0.010369
daili,0.005152
day,0.003324
dead,0.005523
deal,0.007098
death,0.003858
definit,0.003483
dehydr,0.009167
delin,0.007567
demarc,0.007696
demonstr,0.003959
dens,0.006271
depend,0.002906
deriv,0.016748
descend,0.026661
descent,0.006320
describ,0.008134
detect,0.004838
detoxif,0.021736
develop,0.017626
diagnos,0.007135
diaphragm,0.019980
diarrhea,0.020189
diarrhoea,0.011266
die,0.004221
diet,0.013028
dietari,0.008110
differ,0.016348
differenti,0.004460
digest,0.168276
dilat,0.008573
direct,0.005850
diseas,0.064608
disord,0.011358
dispos,0.013035
dissect,0.008141
distal,0.009603
distens,0.012608
distent,0.012152
diverticul,0.012794
diverticular,0.014348
diverticulosi,0.014348
diverticulum,0.013250
divid,0.020960
divis,0.011454
doe,0.002980
dose,0.008030
drug,0.010392
drum,0.025100
duct,0.009830
duoden,0.012152
duodenum,0.162415
duplic,0.007227
dure,0.010143
dye,0.007545
dynam,0.004130
dynasti,0.005728
dysphagia,0.013006
dysplasia,0.012152
easi,0.005386
eaten,0.008001
egglay,0.011122
egypt,0.005601
elabor,0.005519
electr,0.004385
element,0.003373
elimin,0.004586
embryo,0.029398
embryolog,0.008747
embryon,0.016201
empti,0.018970
emulsifi,0.012290
end,0.002899
endodermderiv,0.014348
endodermlin,0.014348
endometriosi,0.013250
endoscopi,0.032432
energi,0.003713
enhanc,0.009825
enter,0.023265
enterovirus,0.014348
entir,0.003440
enzym,0.032240
epitheli,0.019314
epithelium,0.019848
era,0.004190
escap,0.005325
esophagu,0.044219
especi,0.003079
estim,0.003732
european,0.003303
event,0.003451
evok,0.008080
evolut,0.004306
exact,0.005059
examin,0.020756
exampl,0.009501
exhibit,0.005006
exit,0.006957
expel,0.006394
expos,0.010625
exposur,0.005894
extend,0.003498
extens,0.003581
extern,0.002073
externa,0.025589
extract,0.005105
factor,0.010089
faec,0.011424
fat,0.007263
fatal,0.013788
fatti,0.016283
featur,0.003587
fece,0.019425
feet,0.006477
ferment,0.007786
fetal,0.009033
fever,0.007560
fewer,0.005624
fiber,0.006889
final,0.006861
fix,0.004209
flatul,0.013539
flex,0.021299
flexur,0.021856
flora,0.021508
fold,0.027045
follow,0.004537
food,0.057147
foodborn,0.011054
foodstuff,0.008184
foregut,0.039751
form,0.010839
formal,0.003549
foxp,0.011510
frame,0.005420
frequent,0.003987
fresh,0.006370
function,0.037749
fundament,0.003631
fuse,0.006982
ga,0.004771
gall,0.009250
gallbladd,0.010755
galt,0.012152
gardoubakia,0.014348
gase,0.006323
gastric,0.040827
gastrin,0.013006
gastriti,0.026501
gastroenter,0.023598
gastrointestin,0.362081
gener,0.008152
ghrelin,0.013539
gi,0.087292
giardiasi,0.013892
gibb,0.007991
git,0.011799
gizzard,0.013892
gland,0.015964
gluten,0.011343
glutenfre,0.013250
goat,0.007461
goldbeat,0.014348
gradual,0.004559
gramneg,0.011122
grind,0.008601
gross,0.005545
group,0.002558
gut,0.333390
gutassoci,0.013892
gutrel,0.028696
h,0.003939
ha,0.017020
haggi,0.013250
half,0.007808
halfwaytens,0.014348
hard,0.004654
harden,0.008702
harm,0.010935
head,0.003661
health,0.003953
healthenhanc,0.013539
heartburn,0.012290
helic,0.026197
helicobact,0.023202
help,0.003104
high,0.002871
highli,0.003733
hindgut,0.024882
hirschsprung,0.013006
histolog,0.026627
histon,0.009476
hog,0.009577
homeostasi,0.017434
horizont,0.006540
hormon,0.029398
host,0.005186
hostpathogen,0.023817
hour,0.019935
howev,0.009207
hull,0.007963
human,0.046452
hypothalamu,0.010285
ib,0.009250
identifi,0.003428
iga,0.011054
ileiti,0.014348
ileu,0.028696
ileum,0.065031
ill,0.005507
imag,0.008734
immun,0.062080
impair,0.007368
import,0.007256
includ,0.037407
increas,0.002727
indic,0.006978
individu,0.005850
induc,0.005616
induct,0.006280
industri,0.003237
infarct,0.010369
infect,0.025749
infecti,0.007597
inflam,0.010206
inflamm,0.052579
inflammatori,0.045544
influenc,0.003123
ingest,0.016733
inhibitor,0.008875
initi,0.003081
inner,0.017104
innermost,0.019482
insid,0.004756
institut,0.003001
instrument,0.004367
instrumentalist,0.010701
intak,0.008658
interact,0.003538
interfac,0.012348
intern,0.002485
interstiti,0.010206
intervent,0.005146
intestin,0.498596
intestinethi,0.014348
intracrin,0.013250
intraperiton,0.012794
intrins,0.006005
intussuscept,0.012608
invas,0.005171
irregular,0.006635
irrit,0.008492
jejunum,0.062207
juic,0.008926
kept,0.005110
kind,0.007555
known,0.011848
kokoretsi,0.013892
lamb,0.025637
lamina,0.010327
landmark,0.006676
larg,0.044981
larger,0.003742
later,0.002820
layer,0.120080
lead,0.008487
left,0.015119
life,0.006096
lifelong,0.008307
lifethreaten,0.009405
like,0.002532
limit,0.005791
line,0.006787
link,0.002061
lipid,0.007468
liquid,0.005082
live,0.003065
liver,0.014882
livestock,0.006692
local,0.003211
locat,0.006972
locu,0.007704
long,0.015393
longcas,0.014348
longitudin,0.032240
loop,0.006529
loss,0.008294
low,0.003633
lower,0.031531
lumen,0.019722
lymph,0.009830
lymphat,0.009800
lymphoid,0.021621
lymphoma,0.020825
m,0.016954
main,0.009004
mainli,0.015744
maintain,0.006612
major,0.010061
make,0.004822
malabsorpt,0.012794
malform,0.009830
malnutrit,0.008702
malt,0.010131
mammal,0.012004
manag,0.003395
mani,0.015001
marooncolour,0.014348
materi,0.009967
mauric,0.007070
meal,0.008080
mean,0.007833
mechan,0.003501
meckel,0.013892
mediat,0.005808
medicin,0.004520
megacolon,0.014348
mesenteri,0.012025
mesoderm,0.010503
metabol,0.012568
metal,0.009869
metaplasia,0.014348
meter,0.006381
method,0.002936
metr,0.006744
micel,0.011424
microaerophil,0.013892
microbiota,0.011601
microenviron,0.010701
microorgan,0.021243
microscop,0.006120
midgut,0.039751
midsect,0.012608
milk,0.007002
milkf,0.014348
moder,0.005431
modern,0.005687
modul,0.006401
molecul,0.005119
mouth,0.033612
movement,0.003269
mucos,0.046404
mucosa,0.107551
mucu,0.010094
mucusrich,0.014348
multipl,0.003646
muscl,0.059178
muscular,0.025210
musculari,0.038384
musician,0.016546
myenter,0.027078
nation,0.002627
natur,0.005062
nausea,0.020118
need,0.002850
nerv,0.007293
nervou,0.019475
neutral,0.010378
noncod,0.009476
normal,0.007558
north,0.003764
northern,0.004432
nutrient,0.026907
nutrit,0.006810
nylon,0.009741
object,0.003241
obtain,0.003685
occur,0.006024
oesophag,0.012290
oesophagu,0.011908
older,0.009950
oldest,0.005128
onc,0.003542
onli,0.002182
open,0.003245
oper,0.002991
oral,0.006126
order,0.007838
organ,0.020688
origin,0.007882
otherwis,0.004509
outer,0.012115
outermost,0.008284
outpati,0.019103
outpouch,0.013250
overgrowth,0.024305
overview,0.009501
pacemak,0.009861
page,0.009296
pain,0.018015
pancrea,0.019425
pancreat,0.019314
parenter,0.011192
partial,0.004361
pass,0.011049
passag,0.011296
passageway,0.011343
past,0.003761
pathogen,0.030508
patholog,0.006739
patient,0.005774
pattern,0.004045
pediatr,0.008466
peopl,0.005227
peptic,0.012441
percent,0.004761
perform,0.003333
peristalsi,0.045699
peristalt,0.014348
persist,0.005375
pertain,0.006556
ph,0.007510
pharynx,0.011424
phosphatas,0.010550
physic,0.003102
physiolog,0.016753
piec,0.005235
pig,0.021211
pinch,0.009577
pitch,0.024301
plane,0.005710
plant,0.004116
play,0.003423
plexu,0.034031
plu,0.005334
point,0.005500
polyp,0.021198
popul,0.003373
portion,0.014292
posit,0.002809
potenti,0.010559
pouch,0.030284
predomin,0.006201
predominantli,0.006035
prevent,0.011660
primari,0.003588
primit,0.036095
problem,0.002907
procedur,0.004734
process,0.005140
produc,0.008486
product,0.011334
promot,0.003878
propel,0.007619
proper,0.009755
propion,0.011601
propria,0.012794
protect,0.007246
protein,0.022092
proton,0.007028
provid,0.004824
pseudomembran,0.014348
pseudoobstruct,0.028696
pump,0.006837
purpos,0.003536
push,0.005170
pylor,0.012441
pylori,0.011697
pyloru,0.012290
qualiti,0.004050
racquet,0.014348
radioopaqu,0.012441
rang,0.006527
rare,0.004474
rate,0.003369
ratio,0.004903
realiz,0.004821
receiv,0.006588
recent,0.002985
rectum,0.052063
red,0.004779
reduct,0.004961
refer,0.010345
reflect,0.007410
regard,0.003308
region,0.006175
regress,0.006801
regul,0.012245
regularli,0.005869
regurgit,0.010094
relat,0.004649
relationship,0.003291
relax,0.006591
releas,0.012927
remain,0.014258
rennet,0.014348
replac,0.003513
reptil,0.007582
requir,0.008143
resect,0.011122
resourc,0.003490
respect,0.003279
respons,0.017943
rest,0.004039
result,0.004803
retroperiton,0.026501
rhythm,0.007828
right,0.003094
rise,0.003469
robust,0.006245
role,0.006041
roughli,0.004836
rye,0.009337
sac,0.018141
saliva,0.009861
salivari,0.010168
sausag,0.021980
second,0.002829
secret,0.015959
secretin,0.011266
secretori,0.010245
section,0.012817
seen,0.003383
segment,0.047259
selfcontain,0.009428
semiliquid,0.012794
semisolid,0.012794
sequenc,0.004786
seri,0.006869
serosa,0.041678
serv,0.006864
set,0.002670
sever,0.015009
shallow,0.007293
sheep,0.013894
short,0.003676
shortchain,0.013250
shorten,0.007287
shorter,0.006138
sigmoid,0.023202
sigmoidoscopi,0.013250
sign,0.003950
signific,0.003063
silk,0.007163
similar,0.005730
similarli,0.009092
singer,0.007696
situat,0.003694
sixteenth,0.007574
skin,0.012172
slinki,0.012441
small,0.049918
smooth,0.012727
snare,0.036077
sodium,0.007305
solid,0.010314
sometim,0.003123
somewhat,0.005087
soon,0.008800
sourc,0.009055
space,0.007399
speci,0.004413
special,0.006002
specialis,0.006952
specif,0.005742
specifi,0.004878
sphincter,0.012152
spiral,0.007343
spleen,0.009630
spot,0.006007
squar,0.005084
start,0.003037
stasi,0.009428
state,0.006298
steel,0.006010
steep,0.007688
stomach,0.184928
stool,0.050842
strangul,0.011799
string,0.070116
structur,0.039251
subdivid,0.013150
submucos,0.013892
submucosa,0.039019
subsequ,0.003744
substanc,0.005045
subtyp,0.008794
sugar,0.005847
superior,0.005341
suppli,0.007824
surfac,0.021725
surgeri,0.040595
surgic,0.007269
surround,0.013183
surviv,0.004138
suspensori,0.039019
swallow,0.024521
swell,0.007926
symbiosi,0.008810
symptom,0.040972
syndrom,0.014624
synthet,0.011903
t,0.004175
tail,0.006947
taken,0.007027
tarri,0.012441
tarrycolour,0.014348
tend,0.003940
tenni,0.008778
term,0.002267
test,0.011768
thi,0.030512
thoroughli,0.007604
thousand,0.004231
thu,0.002805
timbr,0.011697
time,0.004246
tissu,0.052188
titl,0.004307
today,0.003447
togeth,0.003309
tone,0.014562
tongu,0.016041
torcinello,0.014348
total,0.003348
toxic,0.006635
trace,0.004601
tract,0.348966
tradit,0.003021
transfer,0.004106
transit,0.008355
transmissionrout,0.014348
transmit,0.005879
transpylor,0.014348
transvers,0.033277
travel,0.004237
treat,0.008403
treatment,0.013242
treg,0.027078
trigger,0.017512
tropism,0.012152
truli,0.006013
tube,0.026523
tubular,0.010094
twist,0.007872
type,0.005770
ulcer,0.047263
uncorrect,0.011192
understood,0.004503
unifi,0.005022
unit,0.002420
univers,0.002485
untreat,0.009657
unusu,0.005817
upper,0.038766
ureas,0.012025
urin,0.008011
use,0.039215
usual,0.017326
vari,0.003537
variat,0.008996
variou,0.005463
ventral,0.029054
veri,0.002740
vessel,0.011684
villi,0.024051
villou,0.013539
visualis,0.009015
vitamin,0.015762
vitellin,0.013250
volvulu,0.013892
vomit,0.037259
wa,0.003935
wall,0.015243
wash,0.007510
wast,0.017268
water,0.007403
week,0.005043
weight,0.004836
wheat,0.006824
wide,0.006181
wire,0.013865
work,0.002352
world,0.002432
wrap,0.008040
xenobiot,0.012290
xray,0.013089
yellow,0.006739
yolk,0.020413
young,0.004455
