aaa,0.007156
abbrevi,0.005396
abov,0.002836
absenc,0.004073
ac,0.006033
access,0.009196
accumul,0.008500
acetyl,0.098757
acetyltransferas,0.020786
achiev,0.002787
acid,0.031246
act,0.015193
action,0.005444
activ,0.044214
add,0.012674
addit,0.006724
adpribosyl,0.022570
affect,0.002895
aid,0.003323
albrecht,0.007383
alfr,0.004539
alga,0.006129
alkalin,0.006998
allfrey,0.011285
alli,0.004092
allow,0.008812
alpha,0.005128
alphahelix,0.011285
alter,0.007948
amen,0.006239
amid,0.005875
amino,0.040143
analysi,0.002670
angstrom,0.018847
ani,0.001952
anim,0.003101
anoth,0.006392
antagon,0.006786
antibiot,0.006252
apoptosi,0.015357
apoptot,0.009983
appear,0.012830
appli,0.002368
approxim,0.006038
archaea,0.006941
archaeal,0.009768
arginin,0.086098
asid,0.005052
assembl,0.003458
associ,0.021642
asymmetr,0.006061
atom,0.003874
atpas,0.009501
attach,0.004194
attract,0.003564
aurora,0.007183
b,0.002934
backbon,0.012609
bacteria,0.010172
base,0.007721
basic,0.008062
bead,0.006888
becaus,0.004021
becom,0.002138
believ,0.005526
best,0.002944
bind,0.023065
biochem,0.005492
biolog,0.009505
bival,0.017301
bodi,0.008042
bond,0.007631
bound,0.011991
bp,0.006692
brct,0.011655
break,0.011047
bridg,0.004413
british,0.002982
broadli,0.004471
bystand,0.008260
c,0.005113
canon,0.009716
carri,0.008652
cascad,0.006431
case,0.002237
catalogu,0.006100
cathepsin,0.010763
caus,0.004776
cdomain,0.011655
cell,0.057719
cenpa,0.010998
centromer,0.017219
centuri,0.002297
certain,0.004897
chain,0.007994
chang,0.006051
characteris,0.005363
charg,0.027622
chemic,0.003406
chemistri,0.011764
chief,0.003725
chosen,0.004088
chromatin,0.104958
chromatinremodel,0.011655
chromo,0.011655
chromosom,0.069776
citrullin,0.031180
class,0.008903
classic,0.002956
classif,0.003948
clear,0.013321
clphsp,0.011655
clr,0.009871
cluster,0.009818
code,0.010387
collaps,0.003941
collect,0.002610
combin,0.007654
come,0.002559
common,0.002163
compact,0.031676
compass,0.011530
complet,0.002471
complex,0.028803
compon,0.003081
concern,0.002512
condens,0.041422
conform,0.004939
conserv,0.021313
consid,0.002087
constitut,0.002825
contain,0.005134
contrast,0.003022
contribut,0.002626
control,0.004755
convolut,0.007143
copi,0.004314
core,0.038002
correl,0.004171
coval,0.006352
ctd,0.021130
cycl,0.007337
damag,0.023171
danger,0.004203
data,0.008568
databas,0.008373
date,0.005981
david,0.003178
deacetyl,0.009501
deacetylas,0.019537
death,0.003134
deiminas,0.011655
deleteri,0.007223
delic,0.007033
demonstr,0.003216
denot,0.004366
deoxyribos,0.008531
depend,0.002361
depos,0.005885
deposit,0.008473
describ,0.002202
despit,0.006069
destroy,0.003742
detect,0.003929
determin,0.002485
develop,0.003579
di,0.022278
diamet,0.005847
differ,0.015177
differenti,0.010870
dimer,0.037570
dimeris,0.010393
dimethyl,0.008291
dinoflagel,0.008494
diploid,0.007251
discov,0.003151
discoveri,0.003351
dismiss,0.004767
display,0.004067
distanc,0.003810
distinct,0.008268
divers,0.006883
dna,0.184743
dnahiston,0.011655
dnalik,0.010393
domain,0.027960
domainscan,0.011655
domainseg,0.011655
domin,0.002944
doubl,0.011886
doublestrand,0.007678
duplic,0.005870
dure,0.016480
earli,0.006464
earlier,0.003183
easi,0.004375
effect,0.010465
electron,0.003605
electrostat,0.006596
elong,0.013349
embryon,0.006580
emphas,0.003781
enabl,0.003416
encod,0.015739
end,0.004711
ensur,0.007079
entir,0.002794
entri,0.004167
enzym,0.015713
epigenet,0.006931
especi,0.002501
eukaryot,0.047476
euryarchaea,0.011285
event,0.002803
evolut,0.003498
evolutionari,0.004244
evolutionarili,0.015559
exampl,0.007718
excel,0.004660
exert,0.004943
exist,0.006427
exit,0.005651
express,0.008374
exquisit,0.008458
extend,0.005683
extent,0.003545
extern,0.001684
facilit,0.003884
factor,0.002731
famili,0.002983
far,0.002921
fashion,0.004534
featur,0.011655
fiber,0.016788
fit,0.003922
fold,0.010984
follow,0.001842
fork,0.014502
form,0.017610
format,0.015711
function,0.037740
fungi,0.005577
furthermor,0.003948
g,0.003116
gamma,0.017065
gcn,0.011285
gene,0.116227
gener,0.004966
genom,0.026159
german,0.003253
globular,0.007936
greater,0.002901
greek,0.003262
groov,0.016230
group,0.014547
grunstein,0.022570
h,0.080009
ha,0.030724
hahb,0.022570
hakub,0.011285
halv,0.006622
handshak,0.008927
hax,0.080850
haz,0.020786
hb,0.071646
hbkub,0.011655
headtail,0.010393
helic,0.021280
helix,0.020859
helixdipol,0.011655
helixstrandhelix,0.011655
help,0.002522
heterochromatin,0.074244
hh,0.032040
higher,0.008216
higherord,0.007021
highli,0.027296
highlight,0.004743
histanai,0.011655
histo,0.010998
histon,0.785214
histonedb,0.022570
histori,0.001918
hk,0.031652
hkac,0.034965
hkacx,0.011655
hkme,0.153973
hlike,0.011655
homolog,0.011859
howev,0.009348
hp,0.007093
hs,0.014646
hsh,0.009584
huge,0.004284
human,0.006658
hydrogen,0.014315
hydrolyz,0.008458
idea,0.002585
ii,0.009045
imag,0.003547
imin,0.009423
implic,0.004014
import,0.007859
inact,0.006006
includ,0.009595
increas,0.004430
indic,0.002834
individu,0.002376
inert,0.006515
inform,0.002290
inhibit,0.005671
initi,0.010012
insert,0.005344
insid,0.003863
instead,0.005396
intact,0.005904
interact,0.025870
interphas,0.008458
interrupt,0.005730
intron,0.016071
involv,0.011167
irregular,0.005389
isoform,0.009151
k,0.010478
keto,0.009034
key,0.002931
kilobas,0.009423
kinas,0.007718
known,0.013474
kornberg,0.008927
kossel,0.009983
lack,0.008811
larg,0.006089
late,0.005333
later,0.002290
lead,0.006894
leav,0.003261
lefthand,0.007431
length,0.003821
level,0.002303
like,0.004114
likelihood,0.005341
lineag,0.011401
link,0.008373
linker,0.026345
list,0.002141
littl,0.002947
locat,0.002831
lock,0.004988
long,0.007502
loop,0.015911
loosen,0.007237
lorch,0.010763
lysin,0.194767
machineri,0.004749
macromolecul,0.006588
main,0.002438
major,0.014303
make,0.007834
maladapt,0.008010
mammal,0.019503
mani,0.005222
manifest,0.004430
manner,0.003716
mapanim,0.010565
mark,0.043291
marker,0.023698
materi,0.002698
matur,0.004481
mdc,0.009768
mean,0.008484
measur,0.002522
mediat,0.009436
meiosi,0.014262
metabol,0.005104
meter,0.005183
methyl,0.113777
methyltransferas,0.041573
michael,0.007088
micromet,0.016342
microorgan,0.005752
million,0.003025
minim,0.004064
minor,0.010692
mirror,0.005200
mirski,0.009768
mitosi,0.028732
mitot,0.016123
mm,0.005635
model,0.002399
modif,0.164796
modifi,0.015031
molecul,0.012475
molecular,0.004025
mono,0.033555
monomethyl,0.023310
mostli,0.003285
motif,0.019120
mrna,0.007105
multicellular,0.006345
multipl,0.002962
mutat,0.010695
nake,0.006147
natur,0.002056
ncbi,0.007718
nearli,0.003354
necessari,0.003069
need,0.002315
neg,0.010018
net,0.004048
neutralis,0.008736
new,0.001720
nm,0.018756
nomenclatur,0.005467
nonpolar,0.007738
nonspecif,0.007463
normal,0.003069
ntermin,0.035920
nterminu,0.008828
nuclear,0.007870
nuclei,0.016618
nucleosom,0.088284
nucleu,0.005101
number,0.005780
occur,0.009787
octamer,0.011655
onc,0.002877
onli,0.007091
opposit,0.003168
order,0.004244
organ,0.006302
origin,0.002134
outsid,0.002937
oxygen,0.004689
p,0.002939
pack,0.005619
packag,0.014647
pad,0.014392
pair,0.016416
paramagnet,0.007844
particl,0.012419
particularli,0.008102
peculiar,0.005996
peptidylarginin,0.011655
perform,0.002707
perhap,0.003580
pericentr,0.011285
peroxideinduc,0.011655
phd,0.004976
phosphat,0.023819
phosphoh,0.011655
phosphohb,0.011655
phosphoryl,0.143095
place,0.006534
play,0.005561
pmap,0.010763
point,0.002233
pois,0.007621
polya,0.018303
polycomb,0.021130
polymeras,0.020216
pomb,0.009983
posit,0.013694
possess,0.003460
post,0.003737
posttransl,0.025710
potenti,0.002859
prc,0.042863
precis,0.003637
precursor,0.004811
present,0.002208
presum,0.004929
prevent,0.012629
previous,0.003366
primarili,0.003018
process,0.008351
produc,0.004595
promin,0.003463
promot,0.031503
propos,0.008174
protamin,0.010106
protect,0.002943
protein,0.107675
proteindna,0.009983
proteinprotein,0.008531
proteolysi,0.008650
protrud,0.008570
provid,0.001959
prtt,0.011655
pseudodyad,0.011655
ptashn,0.010763
purpos,0.002872
r,0.002955
rad,0.008260
radiat,0.004422
rang,0.002651
rapidli,0.007446
ratio,0.007965
recent,0.004850
recognis,0.008648
recognit,0.003839
recruit,0.041311
reduc,0.005306
refer,0.002801
reflect,0.003009
regard,0.002687
region,0.010032
regul,0.023210
regulatori,0.004847
rel,0.002511
relat,0.001888
relief,0.004993
remain,0.002316
remov,0.006467
repair,0.014958
repeat,0.004042
replic,0.014831
replicationdepend,0.011655
replicationindepend,0.011655
repress,0.058106
repressor,0.008458
requir,0.011024
research,0.002272
resembl,0.004418
residu,0.032437
resist,0.003629
resolv,0.004068
reson,0.005360
respons,0.002429
result,0.003901
reveal,0.003684
review,0.003138
rit,0.009584
rna,0.023078
rnainduc,0.010241
roger,0.004696
role,0.019628
rpd,0.009584
s,0.007945
said,0.002877
salt,0.004798
schizosaccharomyc,0.010106
sensit,0.004511
separ,0.002558
sequenc,0.007776
serin,0.049746
serinethreoninetyrosin,0.011655
serv,0.002788
set,0.002169
sever,0.004064
share,0.008187
shorter,0.004986
signific,0.002488
silenc,0.006245
silent,0.005805
similar,0.006982
simpli,0.003180
simultan,0.004084
singlelett,0.010393
site,0.026267
socal,0.003933
solenoid,0.009151
solubl,0.006233
space,0.003005
speci,0.007169
specif,0.011661
sperm,0.006431
spermatogenesi,0.009214
sphase,0.011285
spinlabel,0.011655
spool,0.028504
spread,0.003445
spuriou,0.007514
stabil,0.010905
stabilis,0.006639
stain,0.006423
stall,0.006484
start,0.004934
state,0.003411
stem,0.017526
steric,0.008200
strand,0.022232
string,0.005177
strongli,0.007513
structur,0.036439
studi,0.008240
subdivid,0.005341
subject,0.002479
substrat,0.005919
sugar,0.004749
sumoyl,0.021527
sun,0.004324
superhel,0.011285
support,0.002317
surround,0.003569
suvh,0.011655
swi,0.010565
switch,0.004786
symmetr,0.011101
symmetri,0.010852
tail,0.039504
target,0.003629
techniqu,0.002995
templat,0.006423
tend,0.009602
termin,0.004567
tertiari,0.005895
tetram,0.019537
th,0.004470
therefor,0.007948
thermoproteal,0.010998
thi,0.046655
thought,0.008287
thu,0.006835
tie,0.003802
tightli,0.012212
time,0.003449
togeth,0.002687
topic,0.003092
topolog,0.005363
transcrib,0.039332
transcript,0.120887
translat,0.003355
trigger,0.004741
trimethyl,0.109820
tudor,0.015280
turn,0.016168
twothre,0.011285
type,0.004687
typic,0.005230
ub,0.007718
ubiquitin,0.035709
ultraviolet,0.006089
unaffect,0.006953
uncertain,0.005152
unclear,0.005120
undergo,0.013558
underli,0.003701
understand,0.002645
unicellular,0.007237
unit,0.001966
unpack,0.009280
unwound,0.009280
use,0.006371
usual,0.004691
variant,0.061386
varieti,0.002926
veri,0.011130
view,0.002498
vincent,0.006177
vital,0.004353
vitro,0.006738
vivo,0.006975
wa,0.003196
water,0.003006
wellcharacteris,0.021527
wellstudi,0.007698
wherebi,0.004404
width,0.006555
wind,0.013893
word,0.010823
wound,0.005347
wrap,0.013062
yahli,0.011655
yeast,0.032576
zigzag,0.008531
