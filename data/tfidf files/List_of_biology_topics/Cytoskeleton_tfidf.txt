access,0.005444
accessori,0.025610
acid,0.007928
act,0.004497
actin,0.210195
actinlik,0.140302
action,0.009669
activ,0.003926
actomyosin,0.038233
add,0.007503
addit,0.007961
adhes,0.011807
advanc,0.004860
allow,0.003913
alon,0.006729
alpha,0.009108
amino,0.010185
analog,0.007031
anchor,0.010706
ani,0.003467
anim,0.038554
anoth,0.007569
appear,0.004557
applic,0.004699
approxim,0.005362
archaea,0.012329
arrang,0.012830
array,0.008586
articl,0.004676
assembl,0.018428
associ,0.015375
atp,0.011746
atpas,0.016875
attach,0.014899
averag,0.005877
axonem,0.017949
bacteri,0.020558
bacteria,0.027100
bacterium,0.012053
barrier,0.008006
bear,0.014727
behavior,0.005565
behaviour,0.007315
beneath,0.010255
beta,0.009704
bind,0.032773
biochemistri,0.018861
biolog,0.011254
biosynthesi,0.012576
blog,0.010775
bodi,0.004761
bound,0.007099
bud,0.011919
build,0.010041
bundl,0.010549
cabl,0.020687
cage,0.012388
cap,0.019360
capabl,0.006257
caulobact,0.020043
cdc,0.013536
cell,0.416892
cellcel,0.015596
cellmatrix,0.019533
cellshap,0.020043
cellular,0.017585
centriol,0.016046
centrosom,0.030728
certain,0.008698
chang,0.003582
chromosom,0.010327
cilia,0.058466
class,0.005270
classifi,0.006428
clinic,0.016575
collect,0.004635
common,0.003842
commonli,0.015833
comparison,0.013328
compart,0.024817
compartment,0.014899
complex,0.013952
compon,0.010946
compos,0.024118
compound,0.014323
compress,0.009812
compris,0.006501
concept,0.004435
connect,0.010465
conserv,0.006308
consid,0.007413
consist,0.004253
constitu,0.007442
constrict,0.014096
construct,0.004921
contain,0.013678
content,0.006271
contract,0.025503
contractil,0.029563
contrast,0.005368
control,0.008445
coordin,0.006737
cortic,0.025659
crenarchaeota,0.019533
crescentin,0.062101
crescentu,0.020043
crowd,0.009905
current,0.004068
cylind,0.011823
cytokinesi,0.015291
cytokinet,0.040086
cytoplasm,0.046175
cytoskelet,0.177626
cytoskeleton,0.402226
cytosol,0.013637
cytosquelett,0.020700
databas,0.007435
daughter,0.017446
definit,0.005025
deform,0.020753
depend,0.012581
desmin,0.038233
determin,0.004413
diamet,0.062309
differ,0.026955
diffus,0.008683
disassembl,0.013855
discov,0.005597
discret,0.007496
distant,0.008834
distribut,0.005244
divid,0.010080
divis,0.049578
dna,0.024608
doe,0.004300
domain,0.012415
doublet,0.033212
drug,0.007496
dure,0.025610
dynam,0.041712
dynein,0.034700
element,0.004866
embryologist,0.016147
emerg,0.004904
encod,0.018636
end,0.004183
endocytosi,0.031033
entir,0.009926
envelop,0.011150
environ,0.005072
epitheli,0.013932
escrtiii,0.020043
especi,0.004443
essenti,0.010638
eukaryot,0.200264
evid,0.005218
evolutionari,0.007539
exampl,0.017135
exclud,0.007198
exclus,0.006537
exert,0.008780
exhibit,0.007223
exist,0.003805
express,0.004957
extend,0.005047
extens,0.005167
extern,0.002992
extracellular,0.024430
factin,0.018764
famili,0.005298
featur,0.010350
fiber,0.019878
fibr,0.012271
filament,0.405256
filopodia,0.018459
flagella,0.058466
fli,0.008541
forc,0.012303
form,0.043787
format,0.005580
french,0.011013
fruit,0.008108
ftsa,0.020700
ftsz,0.093822
ftszring,0.020700
fulfil,0.008274
function,0.020946
furthermor,0.007012
gactin,0.020043
gene,0.007939
gener,0.011761
glucan,0.019116
group,0.007382
grow,0.005246
growth,0.010358
gtp,0.065462
gtpbind,0.019533
guanosin,0.017350
guid,0.005785
ha,0.010913
happen,0.006467
harm,0.007888
helic,0.025197
heterogen,0.010287
hexagon,0.012928
highli,0.005386
histori,0.006816
hold,0.004928
hollow,0.011746
homolog,0.010531
homologu,0.014959
howev,0.003320
httpcelliximbaoeawacat,0.020700
human,0.003942
identifi,0.009892
imag,0.006300
immobil,0.013198
import,0.017449
includ,0.008521
instabl,0.008847
integr,0.004937
interact,0.015315
interlink,0.013226
intermedi,0.096502
intern,0.003585
intim,0.010239
intracellular,0.049239
introduc,0.004799
invad,0.008183
involv,0.027767
junction,0.011746
k,0.006203
keratin,0.043538
key,0.005206
kind,0.005450
kinesin,0.018190
koltsov,0.018764
lab,0.008938
lamellipodia,0.038233
lamin,0.028924
lamina,0.029798
largescal,0.007971
leukocyt,0.015516
level,0.004090
librari,0.006426
life,0.004397
like,0.014615
line,0.004895
linear,0.007278
link,0.005949
literatur,0.006111
local,0.004633
locat,0.005029
longitudin,0.011628
lumen,0.014226
macromolecul,0.011701
macromolecular,0.013780
main,0.008660
maintain,0.014309
mainten,0.024618
major,0.003629
mani,0.006183
materi,0.004793
matrix,0.017172
mbinfo,0.017949
mechan,0.020205
mediat,0.008379
membran,0.067804
mesenchym,0.016254
microb,0.011628
microfila,0.097525
microtubul,0.147254
migrat,0.007308
mind,0.006450
mitochondria,0.024777
mitosi,0.025515
mitot,0.014318
model,0.004260
molecul,0.022157
molecular,0.007149
monthli,0.009323
moreov,0.007847
mosaic,0.011396
mostli,0.005836
mother,0.007883
motil,0.025562
motor,0.035288
movement,0.009433
mreb,0.058600
multitud,0.010794
muscl,0.075891
myosin,0.044519
nanomet,0.013315
network,0.022873
neural,0.009191
neurofila,0.037529
new,0.003055
news,0.007465
nikolai,0.012141
nm,0.055521
nonmuscl,0.018764
nonspher,0.017532
normal,0.005452
nuclear,0.027957
nucleu,0.009061
number,0.006843
obviou,0.008640
onc,0.005111
onli,0.003148
open,0.004682
organ,0.026117
organel,0.047742
orient,0.007127
parallel,0.007087
parm,0.054570
particip,0.005396
particular,0.004281
partit,0.018950
patch,0.022347
pathogen,0.011003
pathway,0.009186
paul,0.005945
pentagon,0.013255
perform,0.009617
period,0.004064
peter,0.006237
physic,0.004475
plant,0.005938
plasma,0.030069
plasmid,0.026690
play,0.014815
plu,0.007695
podosom,0.019533
polar,0.016759
polym,0.030837
polymer,0.024505
posit,0.008107
presenc,0.005931
present,0.011765
prevent,0.011215
previou,0.005914
primarili,0.005361
process,0.003707
prokaryot,0.139889
propos,0.009678
protein,0.318730
proteinfold,0.019533
protofila,0.018764
proven,0.008284
provid,0.013920
push,0.007458
rac,0.018459
rang,0.004708
rapid,0.006797
recent,0.008615
recruit,0.009171
recycl,0.010585
refer,0.004975
registri,0.012408
regul,0.005889
relat,0.003354
relationship,0.004748
repx,0.020700
requir,0.003916
research,0.004035
resist,0.012892
review,0.005573
rho,0.023615
ring,0.025275
role,0.017430
rudolph,0.013438
scaffold,0.039178
segreg,0.032659
septin,0.120259
sequenc,0.013811
serv,0.009903
set,0.003853
sever,0.003609
shape,0.047432
share,0.004847
signal,0.007157
similar,0.024804
site,0.023326
small,0.008472
socal,0.006985
sometim,0.004506
space,0.005337
special,0.004329
specif,0.004142
spectrin,0.060129
spindl,0.013855
stabil,0.006456
stabl,0.006813
starshap,0.017180
step,0.005870
stress,0.007099
strike,0.007700
strong,0.005030
strongli,0.006672
structur,0.060674
studi,0.003658
subunit,0.011628
suggest,0.004846
support,0.012348
synchron,0.011370
synthas,0.014226
synthes,0.008996
synthesi,0.015458
task,0.006422
templat,0.011409
tension,0.015727
term,0.006542
therebi,0.007165
therefor,0.004705
thi,0.007768
thought,0.004906
threedimension,0.009509
time,0.003063
tissu,0.033463
track,0.007659
transport,0.023740
trial,0.007899
tridimension,0.020700
triphosph,0.013603
triplet,0.013032
true,0.005605
truli,0.008675
tubul,0.046092
tubulin,0.079278
tubulinlik,0.103502
tubz,0.020700
turn,0.004786
type,0.020811
unclear,0.009094
uptak,0.012688
use,0.011315
usual,0.008332
vari,0.005103
variou,0.003941
veri,0.011861
vesicl,0.038710
vibrioid,0.020700
video,0.007619
vimentin,0.055378
virtual,0.006846
wa,0.014193
waca,0.019533
wacaprotein,0.041401
walk,0.008843
walker,0.010907
wall,0.036652
wheelshap,0.019533
wintrebert,0.020043
yeast,0.034715
