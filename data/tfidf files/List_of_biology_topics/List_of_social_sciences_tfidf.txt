abil,0.023964
academ,0.073915
access,0.004016
accid,0.006998
accomplish,0.005834
accord,0.002902
account,0.006837
accumul,0.005568
acoust,0.008109
acquir,0.009456
acquisit,0.018829
act,0.006635
action,0.010700
activ,0.017379
actual,0.003715
adapt,0.004622
address,0.008742
administr,0.040588
adolesc,0.008446
aesthet,0.007040
affect,0.018967
affix,0.010060
afflict,0.009025
age,0.003635
agenc,0.004566
agenda,0.006762
agent,0.004949
aim,0.012435
air,0.004459
alli,0.005361
alloc,0.028965
alreadi,0.004229
alter,0.005207
altern,0.003730
analys,0.005675
analysi,0.045491
analyz,0.029277
anatomi,0.013635
ancient,0.004205
andor,0.010160
androcentr,0.011837
ani,0.012791
anim,0.004063
anoth,0.002792
anthropolog,0.105300
anthropologist,0.007447
anthrozoolog,0.012449
appli,0.037239
applic,0.055469
appreci,0.006441
approach,0.030067
aprior,0.013241
archaeolog,0.049724
architectur,0.005475
area,0.046130
aris,0.004735
art,0.013323
artifact,0.006878
aspect,0.022891
assess,0.004944
associ,0.008507
assum,0.004021
assumpt,0.009891
atmospher,0.017060
attempt,0.003311
attest,0.007642
attitud,0.011918
author,0.006536
avail,0.003548
bachelor,0.030239
bank,0.004198
base,0.012647
beauti,0.013301
bed,0.007559
begin,0.003413
behalf,0.006456
behavior,0.098543
behaviour,0.037776
belief,0.004685
berkeley,0.006836
best,0.003857
bias,0.007253
biocultur,0.011697
bioeconom,0.011229
biogeographi,0.008547
biolinguist,0.011130
biolog,0.037362
biopsycholog,0.011567
biospher,0.007297
biota,0.009230
bodi,0.017562
boundari,0.004886
brain,0.016803
branch,0.122838
broad,0.009402
broader,0.005836
budget,0.010957
build,0.014816
built,0.004363
bureaucrat,0.007756
busi,0.023675
buy,0.016611
capac,0.004859
capit,0.015250
captur,0.004813
care,0.008808
career,0.005750
cartographi,0.008867
categori,0.017892
caus,0.009387
central,0.006572
centuri,0.006020
challeng,0.004187
chang,0.015858
charact,0.004844
character,0.008892
characterist,0.004040
chart,0.006678
childhood,0.014983
children,0.014464
choic,0.004606
choos,0.005043
citi,0.008097
citizen,0.004797
citizenship,0.007307
civic,0.007636
civil,0.020107
civilian,0.006094
class,0.003888
classif,0.005173
classifi,0.004742
client,0.006751
climat,0.005005
climatolog,0.008842
clinic,0.036685
close,0.006274
coast,0.005350
coastal,0.012782
code,0.004536
coevolut,0.009715
cognit,0.044466
collect,0.017099
combin,0.003343
come,0.003353
common,0.002835
commonli,0.007787
commun,0.036790
compar,0.024275
comparison,0.004916
competit,0.004610
compil,0.006049
complex,0.003430
compon,0.008075
composit,0.004860
comprehend,0.016610
comprehens,0.005164
compris,0.009593
comput,0.019425
concentr,0.004588
concept,0.016360
conceptu,0.011286
concern,0.062544
condemn,0.006604
conduct,0.008037
conflat,0.008984
conform,0.006471
conscious,0.005972
consequ,0.003987
consid,0.013672
consist,0.009414
constitu,0.005490
constitut,0.003702
construct,0.010892
consum,0.009337
consumpt,0.005468
contact,0.004920
content,0.004626
context,0.024748
continu,0.002841
contrast,0.003960
contribut,0.006882
control,0.012461
corpor,0.004600
counsel,0.022644
counter,0.006340
countri,0.012342
cours,0.008173
courtroomcrimin,0.026839
creat,0.005736
creation,0.004268
creativ,0.006058
crime,0.005817
crimin,0.029696
criminolog,0.019646
critic,0.027592
critiqu,0.005957
cultur,0.084614
current,0.006003
custom,0.014223
cycl,0.004806
danger,0.005507
data,0.022452
deal,0.052883
decis,0.019766
decisionmak,0.006758
dedic,0.005632
defens,0.005146
defin,0.003217
definit,0.003707
degre,0.003839
deliber,0.006276
demand,0.012558
demographi,0.023653
depend,0.003093
deploy,0.006345
depth,0.006156
describ,0.008658
descript,0.008572
design,0.020664
desir,0.004413
develop,0.049245
development,0.026403
devot,0.005542
diagnosi,0.007114
dialect,0.013795
dialectolog,0.012799
dialectometri,0.013419
differ,0.012428
diplomat,0.005570
direct,0.003113
disaggreg,0.011082
disciplin,0.060489
disciplinari,0.008665
discours,0.006420
discoveri,0.004391
diseas,0.009823
disord,0.006044
dissemin,0.014604
distant,0.006517
distress,0.015564
distribut,0.050295
divers,0.004509
divis,0.004063
doe,0.003172
domain,0.009159
domin,0.003858
draw,0.009613
duti,0.011159
dynam,0.017584
dysfunct,0.016776
earth,0.031023
ecolog,0.021279
econom,0.296579
econometr,0.008305
economi,0.061685
economist,0.010599
ecosystem,0.019209
educ,0.071927
effect,0.030167
effici,0.017680
elect,0.008327
element,0.014361
emot,0.017397
emphasi,0.005120
empir,0.019695
employ,0.011233
employe,0.017745
enabl,0.004476
encompass,0.010569
enemi,0.006309
energi,0.007903
enforc,0.010584
engin,0.004068
enterpris,0.005313
entir,0.003661
entiti,0.004789
entrepreneur,0.007110
entrepreneuri,0.008781
entrepreneurship,0.025610
environ,0.041163
environment,0.054461
epistemolog,0.020127
equilibrium,0.005899
equiti,0.012918
equival,0.004547
ergonom,0.010035
erron,0.008085
especi,0.006556
essay,0.005470
establish,0.006025
estat,0.012018
ethic,0.020782
ethnic,0.005570
ethnobiolog,0.012934
ethnographi,0.009715
ethnolog,0.009575
ethnopoet,0.013081
etymolog,0.006009
evalu,0.004891
event,0.011021
evolut,0.013750
evolutionari,0.038933
examin,0.035347
exampl,0.005056
exchang,0.003990
exclud,0.005310
execut,0.004276
exist,0.008421
expect,0.007757
experi,0.006942
experiment,0.037722
expertis,0.006886
explain,0.007600
explan,0.004969
explor,0.004301
expositor,0.011631
express,0.010972
extent,0.009290
extern,0.002207
extrem,0.004049
factor,0.014317
famili,0.007817
faunal,0.010221
featur,0.011453
feder,0.008989
femin,0.008426
feminist,0.014799
feudal,0.007292
fiction,0.006021
field,0.095568
financ,0.013419
financi,0.008060
firm,0.015125
fix,0.004480
focus,0.015718
follow,0.004829
forc,0.003025
forens,0.032343
form,0.013844
formal,0.011332
format,0.004117
formul,0.004834
foundat,0.007872
framework,0.012974
frankfurt,0.008341
friendli,0.006953
function,0.015452
fundament,0.003865
game,0.005180
gender,0.025885
gener,0.021691
genet,0.005127
geodesi,0.009310
geograph,0.029456
geographi,0.261158
geolinguist,0.013419
geolog,0.011537
geomorpholog,0.017734
geopolit,0.024782
geospher,0.010597
gerontolog,0.010863
given,0.009082
glacier,0.007891
glaciolog,0.010010
global,0.004008
globe,0.006874
goal,0.016608
good,0.010068
govern,0.043058
government,0.006317
grammat,0.009154
gravit,0.013503
green,0.005191
ground,0.013177
group,0.005445
growth,0.007641
guid,0.008536
ha,0.004025
health,0.063124
healthrel,0.010462
help,0.003304
henc,0.004709
heterodox,0.017709
high,0.006113
highlight,0.006215
histor,0.023736
histori,0.140798
hominid,0.009674
hous,0.008098
household,0.006004
human,0.154139
humananim,0.011229
humanist,0.007332
hydrographi,0.011506
hydrolog,0.016001
hypothes,0.006002
hypothet,0.006787
ice,0.013169
idea,0.006775
ident,0.004322
identif,0.005946
identifi,0.007298
ideolog,0.005702
idiom,0.009674
igo,0.011334
ill,0.011722
immedi,0.004437
implement,0.016811
impli,0.004692
implic,0.010519
improv,0.014858
includ,0.027241
incom,0.018494
incorpor,0.008290
independ,0.003100
individu,0.046704
industri,0.017227
influenc,0.006649
inform,0.030010
infrastructur,0.015332
ingo,0.011281
inhabit,0.010495
injustic,0.008781
inland,0.007243
inquiri,0.012042
insepar,0.008997
insight,0.005599
inspir,0.004994
instanc,0.004088
instant,0.007802
instead,0.007071
institut,0.031942
instruct,0.005682
integr,0.010928
interact,0.026364
interdepend,0.007577
interdisciplinari,0.093076
interestfre,0.010783
interfac,0.013143
intergovernment,0.008817
intern,0.034388
interperson,0.008526
interpret,0.024163
intersect,0.020407
interv,0.006359
interven,0.006486
intervent,0.010954
intonationstress,0.013419
intut,0.012251
invent,0.004911
invest,0.004297
investig,0.016919
involv,0.020484
irreligi,0.021895
islam,0.020100
issu,0.019435
item,0.005537
journal,0.007765
judici,0.011590
jurisprud,0.016931
justic,0.020405
kinesiolog,0.010528
know,0.009147
knowledg,0.045558
known,0.002522
labor,0.004955
laboratori,0.005121
labour,0.005109
lake,0.005952
land,0.015278
landform,0.009327
landscap,0.012585
landus,0.010430
languag,0.132769
languagerel,0.012251
larger,0.003983
law,0.054724
learn,0.012718
legal,0.033315
len,0.015310
level,0.009053
lexi,0.011912
lexic,0.019229
lexicolog,0.012558
lexicon,0.018078
liberti,0.006405
librari,0.018965
life,0.019464
like,0.008086
limit,0.006164
limnolog,0.009327
line,0.003611
linguist,0.166115
link,0.004388
linkag,0.007956
list,0.039286
literari,0.012657
literatur,0.004508
live,0.009789
local,0.010254
locat,0.007421
logic,0.004884
lore,0.009867
lost,0.004385
lowest,0.006002
lowincom,0.009081
macro,0.008793
macroeconom,0.013635
mainstream,0.018372
maintain,0.003518
major,0.010708
make,0.012830
manag,0.036141
manageri,0.016417
mani,0.009123
manipul,0.005403
manner,0.004870
map,0.009232
market,0.026441
marriag,0.006379
marxism,0.007891
marxist,0.006851
mass,0.004138
materi,0.007072
mathemat,0.031736
matter,0.007591
maxim,0.005739
mean,0.027791
measur,0.033052
mechan,0.007453
media,0.018514
mediarich,0.013419
medic,0.004647
medium,0.005752
meet,0.004082
member,0.003219
memori,0.005249
mental,0.038223
merit,0.007201
metaphys,0.006456
method,0.065630
methodolog,0.016024
metropolitan,0.007667
micro,0.008260
microeconom,0.015713
midth,0.006225
migrat,0.005391
militari,0.020411
militarist,0.009985
mind,0.028552
mix,0.004853
mnc,0.011766
model,0.003143
modern,0.003026
modifi,0.004923
monetari,0.005409
moral,0.005048
morphem,0.010947
morpholog,0.013826
movement,0.003479
multidisciplinari,0.015096
multin,0.007642
multipl,0.003881
multiword,0.013241
music,0.016638
narr,0.006640
nation,0.011184
natur,0.064655
neoclass,0.022910
network,0.008437
neural,0.006780
neuroanatomi,0.010495
neurobiolog,0.017157
neuroeconom,0.010905
neurolinguist,0.010991
neuropsycholog,0.009053
new,0.002254
ngo,0.008296
nongovernment,0.016140
nonhominid,0.013419
nonhuman,0.007743
nonmetropolitan,0.012558
norm,0.017558
notforprofit,0.009446
number,0.005048
object,0.006899
occup,0.005258
occur,0.003206
ocean,0.010385
oceanographi,0.016665
offer,0.003752
onli,0.002322
open,0.003454
oper,0.003183
oppos,0.004217
optim,0.005352
option,0.005265
oral,0.013041
order,0.008342
organ,0.063306
organiz,0.026442
origin,0.013982
orofaci,0.012674
orthographi,0.010337
otherwis,0.009599
outcom,0.015477
outlin,0.044205
output,0.005266
outsid,0.003849
overcom,0.006197
overlap,0.005608
overview,0.005056
palaeoclimatolog,0.011766
palaeogeographi,0.011178
paragraph,0.008610
paraleg,0.012799
particular,0.022110
particularli,0.007077
pass,0.003920
past,0.024018
pastor,0.007971
patholog,0.014346
pattern,0.008610
pedolog,0.010706
peopl,0.030603
perceiv,0.015224
percept,0.026856
perform,0.014189
period,0.002998
person,0.019811
perspect,0.031608
pertain,0.006977
phenomena,0.024090
philolog,0.009614
philosoph,0.008858
philosophi,0.111702
phonet,0.009482
phonolog,0.009634
phrasal,0.012558
phrasem,0.013419
phraseolog,0.011697
physic,0.042925
physiolog,0.017831
pi,0.007253
place,0.014268
placement,0.007920
plan,0.015286
planet,0.005725
planner,0.008008
poetic,0.016235
poetri,0.007040
point,0.002927
polici,0.026140
polit,0.104514
politi,0.007802
politician,0.012107
popul,0.017951
pose,0.006146
potenti,0.003746
poverti,0.005952
practic,0.030959
pragmat,0.007053
praxeolog,0.011991
precursor,0.006303
predict,0.012701
prefer,0.004431
prefigur,0.010597
prepar,0.009385
present,0.008679
prevent,0.012410
price,0.004283
primarili,0.003955
primat,0.008208
principl,0.024314
problem,0.027852
procedur,0.005039
process,0.082065
produc,0.012043
product,0.027142
profess,0.012093
profession,0.009377
program,0.003639
project,0.003580
promin,0.004537
promot,0.016511
properti,0.007007
proposit,0.006650
prose,0.008183
prosper,0.005981
protect,0.003856
provid,0.012836
psepholog,0.012073
psycholinguist,0.010991
psycholog,0.369925
psychologist,0.026522
psychometr,0.019690
psychophys,0.009482
psychosoci,0.009429
public,0.032055
purpos,0.007527
qualiti,0.012934
quantit,0.005375
question,0.018168
questionphilosophi,0.013419
racial,0.007105
ration,0.004986
real,0.015235
realiti,0.009983
reallif,0.009327
reason,0.009880
record,0.007736
reduc,0.003476
refer,0.011010
referendum,0.006371
regard,0.007041
region,0.016431
rel,0.003290
relat,0.061859
related,0.009845
relationship,0.035030
reliev,0.015359
religi,0.028126
religion,0.014022
remain,0.009105
repres,0.003010
represent,0.013889
research,0.032749
resourc,0.052013
respond,0.004949
result,0.007668
retriev,0.005452
return,0.003714
riba,0.010991
right,0.019759
risk,0.004515
river,0.005017
road,0.005330
role,0.016074
rose,0.005568
rothengatt,0.013081
rule,0.003349
rulebas,0.009867
rural,0.005934
sacral,0.011334
sampl,0.005468
say,0.003975
scale,0.008307
school,0.022402
scienc,0.225315
scientif,0.055393
scientist,0.008392
scope,0.005395
scriptur,0.007383
sea,0.004682
secondari,0.005225
sector,0.014433
secur,0.003898
seek,0.016471
sell,0.010382
semant,0.006802
semiot,0.009110
sens,0.011328
sensat,0.014961
sentenc,0.006273
sequenc,0.005094
seri,0.003655
servant,0.014149
servic,0.010726
set,0.017056
settlement,0.005355
sever,0.002662
sexual,0.006030
shape,0.013122
share,0.003576
sharia,0.009039
sign,0.012615
signific,0.003260
significantli,0.009419
skill,0.004920
social,0.207939
socialist,0.017141
societi,0.038399
socioeconom,0.007378
sociolinguist,0.021266
sociolog,0.042360
sociologist,0.007248
soil,0.005786
solut,0.008739
sometim,0.013299
sort,0.005357
sound,0.010755
sourc,0.006425
space,0.027564
span,0.005899
spatial,0.071647
speci,0.009394
specialti,0.007253
specif,0.012223
speech,0.038518
speechlanguag,0.012073
sphere,0.005715
sport,0.006065
standard,0.003439
stanza,0.010633
state,0.015643
statist,0.025140
stimuli,0.007815
storag,0.006077
strand,0.007282
strateg,0.016722
strong,0.003711
strongli,0.009845
structur,0.041777
student,0.008694
studi,0.421066
stylist,0.009124
subarea,0.020675
subdisciplin,0.013922
subdomain,0.011036
subfield,0.039448
subject,0.012996
subpopul,0.021266
success,0.003387
sum,0.005094
supervis,0.006106
suppli,0.012491
sustain,0.029974
symbol,0.009273
syntax,0.008085
systemat,0.024397
taken,0.007479
tast,0.007168
taxonomi,0.007347
teach,0.015009
technic,0.004542
techniqu,0.019624
technolog,0.007364
term,0.012066
territori,0.004470
tertiari,0.007723
test,0.008350
testabl,0.008426
text,0.013217
textual,0.008023
th,0.002928
themselv,0.007576
theoret,0.012563
theori,0.071848
theorist,0.011905
therapi,0.013010
therapist,0.008842
thermodynam,0.006555
thi,0.013372
thing,0.008127
thought,0.007239
threaten,0.005807
threedimension,0.007015
tide,0.007941
tier,0.009139
time,0.022597
timevari,0.009674
togeth,0.003521
tongu,0.008536
topic,0.020261
topograph,0.008745
topographi,0.008332
total,0.003563
tourism,0.011883
town,0.005244
tradit,0.012864
traffic,0.006748
train,0.008820
trait,0.018154
transdisciplinari,0.009595
transmiss,0.006309
transmit,0.006257
transport,0.013135
travel,0.004509
treat,0.004472
treatment,0.004698
trial,0.005827
truth,0.005434
turn,0.003530
type,0.009212
typic,0.003426
typolog,0.008817
uc,0.009095
uk,0.004681
ultim,0.004417
uncertain,0.006751
underli,0.019401
understand,0.041602
undesir,0.007985
uneven,0.017200
uniform,0.005921
unit,0.005152
univers,0.005290
urban,0.042856
use,0.054259
user,0.005665
valu,0.009820
variat,0.014362
variou,0.008722
verb,0.008446
verbal,0.007347
vers,0.007519
version,0.004467
violat,0.005650
virtual,0.005050
vision,0.005532
vocabulari,0.007636
vote,0.004843
voter,0.006851
wa,0.008376
water,0.023638
watersh,0.008208
way,0.013654
wealth,0.010519
welfar,0.011765
wellb,0.034162
whi,0.004699
women,0.004860
word,0.014181
work,0.020030
workplac,0.024048
world,0.020715
write,0.011980
written,0.016174
wrong,0.006007
zooarchaeolog,0.012073
