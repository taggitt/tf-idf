abbott,0.042091
abil,0.025729
abov,0.007980
absorb,0.049844
absorpt,0.030346
accord,0.006233
achiev,0.007841
action,0.007659
activ,0.012439
adapt,0.009925
affect,0.008145
age,0.007806
aid,0.009351
al,0.010691
alkhalili,0.056868
andor,0.010908
ani,0.010987
anim,0.017449
anoth,0.017986
antenna,0.075018
anton,0.018073
aperiod,0.026306
appli,0.019991
applic,0.037221
argu,0.008292
arrang,0.010162
aspect,0.008192
assign,0.011196
bacteria,0.028620
ball,0.015393
bantam,0.021948
barrier,0.012682
becaus,0.005657
befor,0.012838
bezrukov,0.028815
biochem,0.015452
biochemistri,0.014939
biol,0.024112
biolog,0.169369
biologytrivi,0.032792
biophys,0.016668
biosystem,0.096017
blue,0.013397
bohr,0.018583
bond,0.010735
bordonaro,0.032792
brain,0.012027
brownian,0.018830
c,0.007193
cambridg,0.009969
cast,0.012786
catalyz,0.036315
cc,0.018353
cell,0.010826
cellular,0.069644
center,0.016057
chang,0.011350
charg,0.009714
chemic,0.067097
chemistri,0.044132
chlorophyl,0.021658
christoph,0.013192
chromophor,0.118900
claim,0.007641
classic,0.008317
cockroach,0.023416
coher,0.097135
combin,0.007178
come,0.007200
complementar,0.020998
complex,0.022101
comput,0.025027
concern,0.007068
condit,0.007044
configur,0.013909
contain,0.007223
control,0.006689
convers,0.020775
convert,0.020527
coval,0.017872
creat,0.012317
crick,0.019787
critic,0.007406
cryptochrom,0.090849
crystal,0.014124
current,0.006445
davi,0.058397
davia,0.032792
dawn,0.015934
debat,0.009373
delbruck,0.028815
demonstr,0.009050
depend,0.006643
derek,0.020099
determin,0.006992
devis,0.013812
differ,0.005337
diffus,0.013755
direct,0.013371
discuss,0.016430
distanc,0.032165
disturb,0.042416
divers,0.009683
dna,0.025988
doe,0.006812
doi,0.053318
doia,0.021092
doijbiosystem,0.065584
doinq,0.032792
doubli,0.023416
doubt,0.012994
dynam,0.009439
earli,0.006062
earth,0.019033
ed,0.008486
edg,0.013225
editor,0.012904
effect,0.023555
effici,0.047457
eisert,0.032792
electron,0.202872
element,0.007709
empir,0.008458
energi,0.101832
entangl,0.111936
environ,0.008035
environment,0.009745
enzym,0.044209
enzymat,0.040419
epigenet,0.019500
erwin,0.054926
et,0.010419
european,0.015100
evid,0.024798
evolut,0.009842
exampl,0.010857
excit,0.133001
experi,0.014908
explain,0.008160
explan,0.010670
expos,0.012142
exposur,0.013471
extern,0.004739
eye,0.012118
f,0.008929
favor,0.010011
feel,0.011332
femtosecond,0.023507
festiv,0.015054
field,0.033098
flavoprotein,0.032792
fluctuat,0.027072
fluoresc,0.017663
fmo,0.030943
followup,0.019234
form,0.009909
format,0.026522
franci,0.012413
frauenfeld,0.028815
frequenc,0.012387
fs,0.021390
ft,0.014166
function,0.006636
fundament,0.016601
furthermor,0.011107
garden,0.014497
geabanacloch,0.029725
genet,0.011009
geometr,0.013329
green,0.011147
ground,0.009432
group,0.005846
hameroff,0.029725
han,0.012953
harbor,0.016220
hi,0.005811
hidden,0.014273
high,0.013126
histori,0.005398
howard,0.015036
howev,0.031563
hundr,0.009848
hydrogen,0.013425
idea,0.014549
identifi,0.007835
illinoi,0.015866
import,0.005528
inclin,0.016039
includ,0.004499
independ,0.006657
indic,0.007973
induc,0.012835
influenc,0.007139
inform,0.012888
initi,0.007042
interact,0.016174
intermedi,0.012739
interpret,0.017295
interview,0.013402
introduc,0.015207
involv,0.018851
ion,0.014056
jame,0.009141
jen,0.020063
jim,0.034264
johnjo,0.065584
jordan,0.016334
julio,0.021390
khoshbinekhoshnazar,0.032792
known,0.005415
lab,0.014159
laboratori,0.010997
lakhan,0.029241
larg,0.005711
later,0.006444
lead,0.006465
leap,0.017245
letter,0.010697
level,0.006480
life,0.062694
light,0.053603
lightharvest,0.029725
like,0.005788
link,0.004711
live,0.007006
local,0.007339
long,0.028143
longer,0.017874
lost,0.009416
lwdin,0.030943
m,0.030999
magnet,0.076321
magnetorecept,0.295129
mani,0.009795
manner,0.020915
march,0.008985
mass,0.008886
massimo,0.022756
max,0.013241
mcfadden,0.052613
mechan,0.120028
minimum,0.011819
model,0.026998
molecul,0.011700
motion,0.010934
motor,0.013975
mr,0.014567
mutat,0.060182
natur,0.023138
navig,0.027060
necessari,0.008635
necessarili,0.010606
need,0.019541
network,0.009058
neuroquantolog,0.032792
new,0.004840
niel,0.018489
nois,0.030905
nonentangl,0.030943
nontrivi,0.037509
nuclear,0.011071
number,0.005420
object,0.007407
occur,0.013768
ogryzko,0.065584
olfact,0.024580
onli,0.004987
oppos,0.009055
order,0.017913
organ,0.023641
origin,0.006004
oscil,0.015680
pair,0.011547
pandey,0.028089
paper,0.008940
particip,0.008547
particl,0.011647
pascual,0.024338
pathway,0.014552
pati,0.026513
paul,0.009418
pcw,0.065584
pdf,0.012541
perfect,0.012106
perolov,0.030283
phenomen,0.018830
phenomena,0.031037
philip,0.013318
photo,0.016768
photoinduc,0.029725
photoinduct,0.032792
photoisomer,0.057631
photon,0.047499
photosynthesi,0.086392
phototransduct,0.061886
physic,0.021270
physiolog,0.012762
piec,0.011965
pigment,0.036315
pioneer,0.022638
plant,0.009407
play,0.023470
plenari,0.044795
pmc,0.040126
pmid,0.050090
podium,0.029725
possibl,0.006337
potenti,0.016088
pp,0.010129
pregnolato,0.032792
present,0.012425
press,0.007312
principl,0.007458
probabl,0.008915
problem,0.013290
proceed,0.011370
process,0.035243
product,0.012951
program,0.007816
propos,0.038330
protein,0.012622
proton,0.032126
publish,0.020689
quantiz,0.018420
quantph,0.030283
quantum,0.551410
question,0.007802
quickli,0.010113
radic,0.011390
radicalpair,0.393505
radio,0.012720
ram,0.018513
rang,0.007458
rapid,0.010768
rate,0.015400
reaction,0.101045
reactiv,0.014845
read,0.006844
recent,0.006823
receptor,0.033565
redox,0.059563
reduct,0.011338
refer,0.015762
reli,0.009495
remain,0.006517
report,0.007573
research,0.019178
resembl,0.012430
respir,0.053385
respons,0.006834
result,0.010978
retinaevid,0.032792
ring,0.013346
robin,0.033921
role,0.020709
saw,0.009643
schrdinger,0.052717
schroeding,0.029241
schulten,0.032792
scienc,0.019882
sensit,0.012692
separ,0.014398
septemb,0.009503
sergey,0.021948
set,0.006104
shape,0.009392
short,0.008402
signal,0.034015
signatur,0.016051
simultan,0.011492
singlet,0.024338
sink,0.016334
site,0.073902
small,0.006710
socal,0.011065
solut,0.009382
speak,0.010516
speci,0.010086
special,0.006859
specif,0.006561
spectroscopi,0.016398
specul,0.011410
spin,0.015210
stabil,0.010227
stage,0.009577
state,0.033590
structur,0.025630
stuart,0.015210
studi,0.040570
success,0.007272
suggest,0.030710
sulfur,0.016122
superposit,0.018986
surrey,0.023416
symmetri,0.015267
synonym,0.013687
talk,0.011801
tegmark,0.025748
temperatur,0.011086
theoret,0.017984
therefor,0.007454
thermal,0.014062
thi,0.036918
time,0.014557
transduct,0.019854
transfer,0.103233
transform,0.008977
travel,0.009683
triplet,0.020645
trivial,0.016372
trixler,0.032792
tunnel,0.149024
uk,0.010052
undergo,0.025431
undetermin,0.022071
univers,0.011360
urbanachampaign,0.022466
usabl,0.016826
use,0.031368
vari,0.008084
variou,0.006243
versa,0.015564
vice,0.012696
video,0.024141
vimal,0.032792
vision,0.023757
visual,0.011571
vv,0.045664
w,0.009201
wa,0.013490
warbler,0.025925
way,0.005863
weak,0.010893
wellestablish,0.018420
wherea,0.009516
whi,0.010090
wiseman,0.025925
work,0.005376
workshop,0.016233
world,0.005560
yield,0.010713
zeiling,0.028815
