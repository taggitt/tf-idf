abdomen,0.055974
aberdeen,0.010089
abl,0.010607
abov,0.021502
abund,0.005676
academ,0.008385
accur,0.014307
achiev,0.003521
achillini,0.012940
achromat,0.012222
acid,0.011279
act,0.006398
action,0.003439
activ,0.005586
adapt,0.017829
addit,0.002831
adhes,0.008399
adipos,0.010779
adopt,0.003862
adulthood,0.008757
advanc,0.024201
advent,0.006121
age,0.003505
aid,0.004199
air,0.030100
airway,0.009075
alessandro,0.008550
alexandria,0.045782
allegor,0.009388
allow,0.013918
alway,0.003764
amniot,0.020796
amphibian,0.057835
anatmn,0.013131
anatom,0.097067
anatomi,0.565369
anatomist,0.044282
ancestor,0.006016
ancestr,0.007318
ancient,0.032445
andrea,0.014896
andrew,0.005685
andri,0.010878
anemon,0.031926
angiographi,0.021465
ani,0.002467
anim,0.148888
animalia,0.010556
annelid,0.010642
antagonist,0.008144
antenna,0.025266
anterior,0.008993
antonio,0.007289
anu,0.018431
appear,0.006484
appendag,0.037720
appreci,0.006211
aquat,0.030172
arachnid,0.010779
arch,0.008562
argument,0.004703
aristotl,0.011541
arm,0.004463
armour,0.008422
arrang,0.004563
array,0.006108
art,0.008564
arteri,0.033089
arthropod,0.026396
articul,0.006760
articular,0.011642
artifici,0.004727
artist,0.017624
artwork,0.008574
aspect,0.003679
associ,0.002734
atria,0.023451
attach,0.026498
australia,0.004865
author,0.003151
autonom,0.005660
backbon,0.007965
baltimor,0.008241
band,0.006086
bar,0.012655
barber,0.009472
bark,0.008689
basal,0.017626
base,0.012195
basement,0.009911
basi,0.003486
basic,0.020373
bat,0.008282
bbc,0.006368
bce,0.018582
beak,0.009997
bear,0.005238
becam,0.012340
becaus,0.002540
becom,0.002701
befor,0.008648
began,0.003370
believ,0.003490
belli,0.009126
benivieni,0.012940
better,0.003806
bibliographi,0.004908
biggest,0.006336
bile,0.009629
biochemistri,0.013417
biolog,0.016012
bird,0.040332
birth,0.004973
bladder,0.059139
blastula,0.011279
block,0.005028
blood,0.054966
bloodstream,0.009560
boa,0.009388
bodi,0.149027
bodili,0.014751
bologna,0.009009
bone,0.075968
boni,0.064509
book,0.009623
born,0.004768
bound,0.005050
brabant,0.011095
brachiopod,0.011154
bragg,0.009472
brain,0.037808
branch,0.021536
breath,0.014513
bristl,0.011345
britain,0.014265
broad,0.004533
broadcast,0.006549
bronchi,0.011642
brought,0.004160
buccal,0.011487
buccopharyng,0.013131
build,0.003571
bundl,0.015009
buri,0.006674
burrow,0.009009
c,0.003230
cadav,0.052783
caecilian,0.036329
calcar,0.010642
calcium,0.007429
cameraequip,0.013131
captur,0.004641
carapac,0.012769
carbon,0.005509
carcass,0.010027
cardiac,0.017459
carri,0.018221
cartilag,0.029569
cartilagin,0.021197
case,0.005653
caudal,0.022431
caus,0.009051
caviti,0.008173
cell,0.155578
cemeteri,0.008292
centr,0.004648
central,0.006337
centuri,0.037737
cephalopod,0.009829
cephalothorax,0.025880
cerebellum,0.021197
cerebrum,0.022974
certain,0.009282
cetacean,0.009856
chamber,0.041990
chang,0.007646
character,0.008575
characterist,0.007793
charl,0.004329
chelicera,0.012940
chemic,0.004304
chemoreceptor,0.012614
chest,0.017005
chew,0.009368
chief,0.004707
childbirth,0.009329
chitin,0.020116
chlorin,0.008163
chloroplast,0.008962
chordat,0.011415
cilia,0.020796
ciliat,0.011038
circulatori,0.017713
citi,0.003904
class,0.018747
classic,0.003735
classifi,0.004573
claw,0.009653
clear,0.004207
climb,0.007651
clinic,0.005895
clinician,0.008871
cloaca,0.023284
close,0.015124
cloth,0.005910
cnidarian,0.021756
coat,0.020091
cochlea,0.011487
coelacanth,0.012222
coffin,0.009856
collagen,0.028290
collect,0.003297
colleg,0.014722
coloni,0.004674
column,0.032115
columnar,0.011563
come,0.003233
common,0.005467
commonli,0.003754
compani,0.003570
compar,0.013376
comparison,0.004741
compil,0.005833
complementari,0.006595
complet,0.009368
complex,0.006616
complic,0.005123
compos,0.047184
compound,0.005094
compress,0.006980
compris,0.013875
comput,0.003746
concern,0.009522
condemn,0.006368
condit,0.003163
conduct,0.003875
conic,0.008886
connect,0.044671
consequ,0.003844
consid,0.007910
consider,0.003799
consist,0.009078
constant,0.004495
constitut,0.014280
contact,0.004744
contain,0.016218
continu,0.008218
contour,0.026658
contract,0.031749
contractil,0.021031
contribut,0.006637
control,0.006008
convey,0.013652
cool,0.006333
copulatori,0.022830
cord,0.024212
corpori,0.010219
corps,0.017654
corpu,0.007651
cours,0.003940
cover,0.026058
crab,0.008886
creat,0.002765
creatur,0.006895
credit,0.004375
cretac,0.009605
crimin,0.017181
crocodil,0.009939
crocodilian,0.021559
crosslink,0.009939
crustacean,0.008978
crystal,0.006343
ctenophor,0.011642
cuboid,0.011215
cucumb,0.010288
cultur,0.003399
cunningham,0.009368
cut,0.014400
cutan,0.010983
cuticl,0.043513
cymbium,0.013131
cytolog,0.009883
cytoplasm,0.008212
da,0.006018
damag,0.004879
darwin,0.006327
dead,0.005668
deal,0.003642
deeper,0.006756
defin,0.003102
definit,0.007150
degre,0.007405
del,0.006826
demand,0.004036
deoxygen,0.033463
depth,0.005936
deriv,0.030941
dermal,0.010929
dermi,0.034926
describ,0.025047
descript,0.004133
despoli,0.012222
detect,0.009930
develop,0.033919
diagnost,0.014480
diagram,0.006219
diaphragm,0.010253
diatom,0.008663
differ,0.016778
differenti,0.013734
difficult,0.003943
diffract,0.008202
digest,0.020724
digit,0.005049
diploblast,0.012472
direct,0.006004
disc,0.008231
disciplin,0.012499
discov,0.011945
discoveri,0.008469
diseas,0.004736
dissect,0.125344
dissimilar,0.008993
distinct,0.013929
distinguish,0.008028
diverg,0.012221
divers,0.004348
divid,0.010756
divis,0.011756
dmoz,0.005988
doctor,0.010792
dog,0.006768
dorsal,0.009368
dorsoventr,0.024219
dragonfli,0.011279
dramat,0.010212
draw,0.013905
drawn,0.010672
dri,0.005922
duck,0.009309
dure,0.018219
dye,0.007744
dynasti,0.005879
ear,0.043345
eardrum,0.023812
earli,0.008168
earlier,0.004022
earthworm,0.019503
eber,0.009582
echidna,0.011487
echinoderm,0.010253
ectoderm,0.033838
edit,0.004260
edwin,0.007181
effect,0.002644
effici,0.004262
egg,0.026339
egglay,0.011415
egyptian,0.006135
electromagnet,0.006102
electron,0.004555
element,0.003462
elev,0.006073
elli,0.008241
embed,0.006330
embryo,0.007543
embryolog,0.026934
embryon,0.024941
employ,0.003610
enabl,0.017268
enact,0.006034
enamel,0.010828
encas,0.010219
end,0.011905
endoderm,0.033647
endoscopi,0.011095
endoskeleton,0.036666
endotherm,0.009883
energi,0.003810
england,0.004723
enter,0.003979
entir,0.003530
epidermi,0.051620
epiglotti,0.012472
epitheli,0.099116
epithelium,0.040742
equival,0.004384
era,0.004300
erasistratu,0.054141
especi,0.009482
essenti,0.003784
establish,0.002905
eukaryot,0.007498
eumetazoan,0.012614
eventu,0.003869
everi,0.003406
evolut,0.008839
evolutionari,0.010726
evolv,0.008936
examin,0.038345
exampl,0.004875
exchang,0.003848
excret,0.026354
excretori,0.011215
exist,0.005413
exoskeleton,0.052783
expand,0.007701
experi,0.010042
experiment,0.004546
explor,0.004147
extend,0.003590
extens,0.007351
exterior,0.016087
extern,0.014900
extracellular,0.043448
extrem,0.003904
eye,0.065303
eyelid,0.021859
eyesight,0.009883
fabrica,0.010185
face,0.004085
facial,0.008088
famou,0.008994
fan,0.007658
far,0.003691
fatal,0.007075
father,0.004785
feat,0.008901
feather,0.050945
featur,0.022088
feed,0.006249
feet,0.019942
fenestra,0.049370
fertil,0.005706
fever,0.023278
fibrou,0.021031
field,0.005945
filament,0.016958
fin,0.034705
final,0.003520
fine,0.005819
fish,0.066025
fit,0.004956
flagella,0.010398
flap,0.010152
flat,0.012451
flatten,0.026312
flexibl,0.012053
flight,0.017890
flipper,0.010878
float,0.006446
fluid,0.005737
fluoroscopi,0.011279
foetal,0.012472
foetu,0.012005
follow,0.002328
food,0.008378
forb,0.007875
forc,0.008752
forehead,0.009777
forelimb,0.021966
fork,0.009161
form,0.037824
format,0.011910
forward,0.010334
founder,0.005101
frequenc,0.005562
frequent,0.004092
frog,0.024878
fulli,0.004388
function,0.029802
fundament,0.003727
fuse,0.035831
ga,0.004897
galen,0.016691
gamet,0.008978
ganglia,0.010436
gastrointestin,0.009290
gather,0.005074
gaug,0.007197
gave,0.004235
gelatin,0.010185
gener,0.012549
genit,0.009179
germ,0.031405
gill,0.025101
girdl,0.023284
given,0.005838
gland,0.106503
glide,0.009883
glycoprotein,0.010152
goal,0.004003
golden,0.005898
grave,0.007095
graveraid,0.013131
graveyard,0.020721
gray,0.014524
great,0.019684
greek,0.037097
gross,0.039839
ground,0.016942
group,0.007877
growth,0.003684
guest,0.007686
guilti,0.007812
gut,0.016691
ha,0.027173
hair,0.006930
halt,0.006632
hand,0.007213
hardjoint,0.013131
harold,0.006985
head,0.041336
heart,0.074800
held,0.006883
hellenist,0.007399
help,0.015933
henri,0.004691
herophilo,0.011345
herophilu,0.076882
herophiluss,0.013131
heterotroph,0.009751
hi,0.026099
high,0.002947
higher,0.006921
highli,0.003832
hind,0.018580
hippocrat,0.009968
histolog,0.045547
histopatholog,0.011345
histori,0.004848
historian,0.004905
hold,0.003506
hollow,0.033425
home,0.004156
homolog,0.007491
hop,0.009271
horncov,0.013131
horni,0.048020
hospit,0.011480
hous,0.007809
howev,0.004724
human,0.056088
humani,0.010058
hyoid,0.012614
hypogloss,0.012472
hypothalamu,0.010556
identifi,0.010556
ignaz,0.010828
illustr,0.004964
imag,0.017928
imagin,0.005819
immatur,0.019211
immedi,0.004278
import,0.002482
impress,0.005941
improv,0.003582
impuls,0.007460
incid,0.005759
incis,0.009653
includ,0.044454
incomplet,0.006343
increas,0.002799
indic,0.003580
induc,0.005763
inflex,0.009629
influenc,0.003206
influenti,0.009363
inform,0.011575
inher,0.005578
inherit,0.005397
inner,0.011702
innerv,0.010686
inorgan,0.006805
input,0.005273
insect,0.026456
insert,0.006752
insid,0.009764
inspect,0.007273
instrument,0.008964
intellect,0.007804
intercellular,0.010642
interior,0.005712
interlock,0.009677
intermedi,0.005720
intern,0.025507
interpret,0.003883
intervertebr,0.011642
intestin,0.041944
intric,0.008098
invas,0.005307
invent,0.009472
invertebr,0.046378
investig,0.004078
involuntarili,0.010732
involv,0.011287
islam,0.004845
italian,0.005461
jakob,0.008729
jan,0.006397
jaw,0.077854
jefferson,0.008313
jellyfish,0.019554
john,0.003310
join,0.004329
just,0.003394
keel,0.009430
kept,0.010489
keratin,0.030972
keratinocyt,0.012614
kidney,0.007965
kingdom,0.011615
knowledg,0.014643
known,0.014592
lack,0.011132
laid,0.005492
lamina,0.021197
land,0.003683
landmark,0.006851
landscap,0.006068
larg,0.028212
largest,0.004019
larva,0.009197
latch,0.010878
late,0.006738
later,0.008682
latin,0.004574
layer,0.056019
learn,0.008176
leav,0.004121
lebanon,0.008061
lectur,0.015874
leg,0.093914
legisl,0.004455
length,0.009656
lens,0.008282
leonardo,0.007965
liber,0.004387
librari,0.004571
lie,0.004768
lifestyl,0.006930
lifetim,0.006231
light,0.012036
lightweight,0.018818
like,0.002599
limb,0.114250
limbless,0.012940
lime,0.008702
limit,0.002971
line,0.013931
lineag,0.021608
link,0.002116
littl,0.014897
live,0.022025
liver,0.045823
lizard,0.070167
lobster,0.019937
local,0.009887
locat,0.010734
locomot,0.016544
long,0.015798
longer,0.004013
longitudin,0.008272
loop,0.006701
loos,0.006049
lost,0.012686
low,0.003729
lower,0.010787
lowslung,0.013131
lumleian,0.013131
lung,0.053269
luzzi,0.012614
macroscop,0.020913
magnet,0.017137
main,0.021564
maintain,0.010179
major,0.010326
make,0.012372
male,0.005458
malpighi,0.011154
mammal,0.061603
mammalian,0.007924
mammari,0.010828
mani,0.026393
marcello,0.010089
mari,0.005621
marin,0.005570
marsupi,0.010288
materi,0.013639
matrix,0.036649
matthia,0.009026
mean,0.008039
medic,0.094103
medicin,0.018557
mediev,0.015570
melvyn,0.010828
member,0.003104
membran,0.027562
mening,0.010185
mere,0.004787
mesoderm,0.032339
messag,0.006047
metabol,0.006449
metazoa,0.011642
metazoan,0.032060
method,0.015068
microscop,0.087946
microscopi,0.007658
microtom,0.012222
microvilli,0.024219
middl,0.008024
midwiv,0.010686
milk,0.014374
million,0.003822
miner,0.005387
model,0.003031
modern,0.017512
modifi,0.033236
moist,0.009026
molecul,0.010508
molecular,0.005086
mollusc,0.008901
mondino,0.037417
monotrem,0.011154
mostli,0.037365
mother,0.016823
motil,0.009092
motion,0.004910
motor,0.025103
mouth,0.027597
mouthpart,0.035177
moveabl,0.020371
movement,0.013421
mucou,0.011215
multicellular,0.048104
murder,0.006179
muscl,0.155216
muscular,0.008624
museum,0.011735
myocyt,0.012005
mysteri,0.006423
nake,0.007766
narrow,0.005494
natur,0.005195
nd,0.004527
near,0.008001
nearbi,0.005988
nearli,0.004238
neck,0.015965
need,0.008775
nerv,0.082340
nervou,0.093278
net,0.005115
network,0.004068
neural,0.019615
neuron,0.021330
new,0.004347
newli,0.005119
night,0.005968
nippl,0.010779
nitrogen,0.020150
nocturn,0.010436
noninvas,0.009290
normal,0.003878
nostril,0.032060
notabl,0.003733
note,0.002805
notic,0.005522
notochord,0.046569
nourish,0.008931
nowaday,0.007364
nucleic,0.007991
nucleu,0.012892
number,0.012171
numer,0.007209
nurs,0.007080
nutrit,0.006989
obliqu,0.009605
observ,0.006423
obtain,0.015128
occup,0.005070
occupi,0.004878
occur,0.012366
ocelli,0.012472
octopu,0.010398
oculomotor,0.012005
oesophagu,0.012222
oili,0.012109
onc,0.003635
onli,0.017919
opaqu,0.008856
open,0.023318
operculum,0.012342
optic,0.011707
order,0.002681
organ,0.116784
organel,0.008490
origin,0.002696
orthotist,0.012940
ossifi,0.047625
outer,0.024868
outgrowth,0.008931
outlin,0.004736
ovari,0.009515
overal,0.004453
overlain,0.012222
overlap,0.005408
ovipar,0.011154
ovovivipar,0.011725
owl,0.009092
oxygen,0.017773
pack,0.007100
pad,0.009092
padua,0.009777
pair,0.057040
palpat,0.010878
paper,0.004014
papyru,0.016177
paralysi,0.009348
paramecium,0.012005
paramed,0.010556
pariet,0.020872
partial,0.004476
particular,0.003045
particularli,0.006825
pass,0.007560
passag,0.005797
patronag,0.007924
pedipalp,0.025228
pelvi,0.010475
pelvic,0.019713
perform,0.003420
pergamum,0.011154
period,0.005782
peripher,0.014983
person,0.003184
phagocytosi,0.010475
pharyng,0.011487
phenomen,0.008456
philadelphia,0.007399
philosoph,0.008542
phospholipid,0.009348
photograph,0.006697
photoreceptor,0.010686
photosynthesi,0.007759
phylogeni,0.008743
phylum,0.009451
physician,0.036829
physiolog,0.034389
physiologist,0.026148
physiotherapist,0.011095
pinacoderm,0.013131
pioneer,0.005083
pit,0.007707
place,0.005503
placenta,0.009939
placoid,0.012940
plan,0.007370
plant,0.021123
plastron,0.012614
plate,0.012636
platypu,0.011345
play,0.003513
point,0.005645
poison,0.006801
polychaet,0.011215
polyp,0.010878
portion,0.004889
pose,0.005927
posit,0.008651
possess,0.004372
posterior,0.017597
pouch,0.010360
power,0.008566
practic,0.011941
practis,0.007340
practition,0.011712
praxagora,0.012109
preclin,0.010288
predat,0.006194
preen,0.013131
prepar,0.004525
present,0.016740
pressur,0.004200
prevent,0.003989
prey,0.015206
primarili,0.007628
primit,0.012348
prismat,0.011345
problem,0.002984
procedur,0.004859
proceed,0.005106
process,0.007913
produc,0.020322
product,0.005816
professor,0.009973
progress,0.007605
project,0.006905
propound,0.008490
prostat,0.009472
prosthetist,0.012769
protect,0.018594
protein,0.017005
protozoan,0.021031
protrus,0.010779
provid,0.019805
pseudopodia,0.012940
ptolema,0.008813
ptolemi,0.007589
public,0.005620
publish,0.006194
puerper,0.023126
pulposu,0.012940
puls,0.007435
pump,0.028071
punctatu,0.013131
pupil,0.007278
purpos,0.003629
quadrup,0.010929
quit,0.004715
radial,0.008467
radiat,0.005588
radio,0.005712
radiographi,0.010732
radiolaria,0.012342
radiolog,0.008871
raid,0.006643
rais,0.004115
rang,0.010048
rapid,0.009671
rapidli,0.004704
rate,0.003457
ray,0.012089
reaction,0.004537
readili,0.006055
realiz,0.004948
rear,0.007915
reason,0.003176
receptor,0.022610
recogn,0.011383
record,0.003730
rediscoveri,0.008637
reduc,0.010057
refer,0.003539
reform,0.004328
region,0.012675
regiu,0.011154
regul,0.008378
rel,0.006345
relat,0.009544
relationship,0.003377
remain,0.002926
remov,0.008171
renaiss,0.011702
renown,0.007549
reorgan,0.007399
replac,0.003606
reproduct,0.030056
reptil,0.054473
requir,0.002785
research,0.014354
resembl,0.027911
resolut,0.005413
resolv,0.005140
reson,0.020318
respect,0.010096
respir,0.007991
respiratori,0.048104
respond,0.004772
respons,0.006138
rest,0.008292
result,0.002465
resurrectionist,0.012940
retain,0.009307
reticular,0.011215
rib,0.038331
richardson,0.008612
rigid,0.013169
rigidli,0.019165
ring,0.011987
ringlik,0.011906
rise,0.003561
rival,0.005846
robust,0.006410
rod,0.007651
role,0.003100
room,0.005740
round,0.011211
row,0.013686
royal,0.009338
ruler,0.005912
run,0.003888
ruth,0.008743
s,0.005019
sac,0.009309
salamand,0.029905
salivari,0.010436
saw,0.004330
scale,0.032042
scatter,0.006534
schleiden,0.010556
school,0.014401
schwann,0.009997
scienc,0.017857
scientif,0.007122
sea,0.018060
seal,0.006636
seat,0.005236
second,0.008713
secondarili,0.019823
secret,0.032759
seen,0.006944
segment,0.042440
segmentsa,0.013131
semmelwei,0.021756
send,0.005534
sens,0.010923
sensat,0.007213
sensori,0.034301
separ,0.025864
septum,0.010828
seri,0.003525
sessil,0.011279
set,0.013706
seven,0.004643
sever,0.002567
sex,0.006127
shape,0.008435
share,0.006896
shark,0.008514
sharp,0.005981
shed,0.007543
shell,0.017868
short,0.022639
shrimp,0.009161
sideway,0.022076
sidewaysfac,0.013131
sight,0.006566
silica,0.009451
silk,0.007352
similar,0.017645
simpl,0.011739
simpler,0.006566
simplest,0.006281
sinc,0.002462
singl,0.009887
singlecel,0.009368
sir,0.005558
situat,0.003791
sixteenth,0.015548
size,0.015341
skelet,0.051749
skeleton,0.067772
sketch,0.007899
skin,0.049969
skull,0.055070
slice,0.008702
slide,0.007686
slit,0.009911
slow,0.005084
slowli,0.011353
slowmov,0.011095
small,0.021095
smaller,0.004187
smell,0.016087
smith,0.005217
smooth,0.026125
snake,0.046920
snatch,0.010983
snout,0.022190
soft,0.006287
somat,0.008599
sometim,0.003206
space,0.003797
spawn,0.007851
speci,0.072471
special,0.012320
specialti,0.006994
specif,0.005893
spectacl,0.009215
spermatozoa,0.010398
sphenodon,0.013131
spider,0.034705
spinal,0.025761
spine,0.050009
spleen,0.009883
spong,0.027029
spoonshap,0.013131
spur,0.006917
squamou,0.023627
stage,0.012902
stagger,0.009368
stain,0.008116
standard,0.003317
start,0.003117
startlingli,0.011813
state,0.006464
steppingston,0.011154
sternum,0.011415
stiffen,0.021031
stimul,0.005511
stimulu,0.007213
sting,0.010152
stomach,0.016504
stratifi,0.008813
stream,0.005903
stretch,0.006208
striat,0.023812
striation,0.012005
strike,0.005478
strong,0.003578
stronger,0.011858
structur,0.086325
struther,0.012614
student,0.025151
studi,0.065068
sturgeon,0.010515
subdivid,0.006748
subject,0.003133
submerg,0.008743
substanti,0.004571
suction,0.010219
suffici,0.004553
suit,0.005725
superfici,0.046601
supplement,0.006097
suppli,0.008029
support,0.023426
surfac,0.044594
surgeon,0.030717
surgeri,0.006943
surround,0.004510
swallow,0.008388
sweat,0.008916
swell,0.008135
swim,0.040000
symmetr,0.007013
systemat,0.009410
taboo,0.008871
tail,0.064173
taken,0.003606
target,0.004585
tast,0.013825
taught,0.010982
tay,0.011415
teach,0.038596
technic,0.004380
techniqu,0.011354
teeth,0.069767
tend,0.004044
tendon,0.009856
tentacl,0.011095
term,0.009308
terrestri,0.006705
test,0.004026
tetrapod,0.031796
text,0.004248
textbook,0.027756
th,0.019768
theodor,0.006740
theori,0.002886
therapeut,0.007267
therapist,0.008526
thi,0.020263
thicken,0.010089
thicker,0.009388
thing,0.003918
thorax,0.064969
thorough,0.007387
thought,0.003490
thousand,0.004343
throat,0.008856
tick,0.009009
tie,0.004803
time,0.017432
timescal,0.007991
tip,0.013737
tissu,0.208296
titian,0.011215
tmn,0.012769
toe,0.018431
togeth,0.006792
tomographi,0.008624
tongu,0.008231
took,0.003847
tough,0.008202
tract,0.014045
train,0.008505
traine,0.009629
trait,0.005835
transform,0.004031
translat,0.004239
transmit,0.012068
transpar,0.006303
treatis,0.011883
triassic,0.010642
tricuspid,0.012472
trigemin,0.011813
triploblast,0.024685
trunk,0.070619
tuatara,0.034245
tube,0.020416
tubebuild,0.013131
tubercl,0.011725
tubul,0.010929
turtl,0.025579
tutori,0.008163
type,0.029610
typic,0.013216
typifi,0.009009
ultrasound,0.017924
ultrastructur,0.011415
unabl,0.005118
unaid,0.009777
underli,0.009354
undersid,0.010253
understand,0.016715
underw,0.007145
underwat,0.007940
undifferenti,0.009629
undul,0.020872
unicellular,0.009144
uniform,0.005710
unit,0.007452
univers,0.007652
unknown,0.005099
unlik,0.008202
unmistak,0.010475
unpreced,0.006994
upper,0.009946
urea,0.017772
uric,0.011563
urinari,0.018777
uropygi,0.012940
use,0.032198
usual,0.014818
uterin,0.010475
uteru,0.029569
vacuol,0.009829
valv,0.016844
van,0.010774
variat,0.004616
varieti,0.003697
variou,0.005607
vast,0.004945
vegetarian,0.009537
vein,0.029340
venom,0.020506
ventricl,0.019995
veri,0.005625
verrocchio,0.012222
vertebr,0.102452
vertebra,0.061114
vesaliu,0.019503
vessel,0.035976
vestibulocochlear,0.012472
vestig,0.009026
veterinari,0.008514
vibrat,0.014374
video,0.005420
view,0.003156
villi,0.012342
vinci,0.008399
visibl,0.005443
vision,0.005334
visual,0.005196
vital,0.005500
vivipar,0.010828
vivisect,0.032197
volum,0.004218
voluntari,0.006813
wa,0.060580
walk,0.006290
wall,0.036503
ward,0.007536
warmblood,0.010929
wash,0.007707
wast,0.017723
watchtow,0.011642
water,0.022794
waterproof,0.021373
wavelength,0.007460
way,0.002633
wear,0.006674
web,0.005339
welldevelop,0.008192
went,0.004517
wezel,0.013131
whale,0.007672
wide,0.006344
wider,0.005607
william,0.003946
wing,0.040230
women,0.004687
work,0.014486
world,0.004993
worm,0.015518
write,0.003850
writer,0.005054
written,0.003899
wrote,0.004196
xray,0.026867
year,0.007456
yolk,0.010475
yolki,0.012614
york,0.003793
young,0.022865
zone,0.005090
zootomi,0.013131
zygot,0.009042
