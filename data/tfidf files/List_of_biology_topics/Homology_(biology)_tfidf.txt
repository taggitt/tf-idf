adapt,0.009906
adult,0.012179
align,0.040320
analog,0.077816
analysi,0.007499
anatom,0.033189
anatomi,0.014610
ancestor,0.133713
ancestr,0.048792
ancestri,0.102498
anim,0.017416
anoth,0.005983
appendag,0.041916
april,0.009517
archetyp,0.018229
arm,0.009919
aros,0.012354
arthropod,0.039111
articular,0.025874
astragalu,0.029667
author,0.007004
b,0.016480
balanc,0.009846
base,0.010841
basi,0.007749
bat,0.036816
bd,0.021507
beauti,0.014253
becaus,0.022587
bee,0.017592
beetl,0.019989
behavior,0.035198
belong,0.010599
best,0.008267
biolog,0.080072
bird,0.051222
bodi,0.022582
bone,0.112559
brigandt,0.030883
calcaneum,0.032728
carrol,0.038060
centiped,0.023281
certain,0.006876
charact,0.031147
chromosom,0.032656
clade,0.019884
cladist,0.104791
classif,0.011086
cluster,0.027571
cn,0.018976
coin,0.020809
column,0.014275
common,0.060759
compar,0.007432
complet,0.006940
compound,0.011323
concept,0.007012
confirm,0.010833
conjectur,0.015869
connect,0.008273
consid,0.005860
controversi,0.009902
converg,0.026475
copi,0.024229
correspond,0.009122
counterintuit,0.020441
cover,0.008273
creat,0.006146
cretac,0.021349
cs,0.015353
deer,0.019085
defens,0.011029
defin,0.013792
definit,0.015891
depinna,0.032728
der,0.026302
deriv,0.030562
descend,0.012163
descent,0.028832
develop,0.050257
development,0.056585
dewey,0.018644
differ,0.047945
dipteran,0.032728
discov,0.017699
distinguish,0.008921
diverg,0.054326
dna,0.090784
dog,0.030086
doi,0.026607
doihmgddl,0.032728
doijtbx,0.021846
doijtig,0.030883
dp,0.020821
dragonfli,0.025068
duplic,0.115399
ear,0.032111
eardrum,0.026462
earli,0.006051
ecolog,0.011401
embryo,0.100588
embryogenesi,0.023109
embryon,0.036954
encyclopedia,0.010419
endless,0.018524
equip,0.010816
erlin,0.032728
essay,0.011724
eusthenopteron,0.032728
event,0.055112
evid,0.033000
evolut,0.068760
evolutionari,0.035759
evolutionist,0.022423
evolv,0.079446
exampl,0.043347
exist,0.006016
explain,0.016288
extent,0.009954
femal,0.036440
femur,0.024790
fewer,0.012829
fibula,0.024790
fish,0.011287
fitch,0.045129
fittest,0.021507
fli,0.027008
flight,0.013254
flipper,0.048354
flower,0.014691
foot,0.014056
forearm,0.051749
foreleg,0.082293
forelimb,0.097640
forens,0.017328
form,0.009890
fossil,0.042066
frog,0.018431
function,0.006623
fundament,0.008284
g,0.008751
gegenbaur,0.030224
gene,0.175738
generel,0.027431
genet,0.043953
genom,0.088149
grasp,0.016140
greek,0.009161
group,0.011671
ha,0.017255
haeckel,0.020284
halter,0.030224
ham,0.022220
hand,0.008015
hard,0.010617
hemiptera,0.029667
highli,0.008516
hind,0.061943
hindlimb,0.027431
hip,0.020481
homeobox,0.024927
homo,0.016958
homolog,0.649372
homologo,0.032728
homoplasi,0.026681
honey,0.019524
hors,0.027949
hox,0.053828
hoxad,0.032728
httpembryoasueduhandl,0.032728
human,0.024931
humeru,0.027431
hymenoptera,0.025874
hypothesi,0.011268
identifi,0.007820
ilium,0.029185
impli,0.020115
implic,0.011271
includ,0.022454
incu,0.059335
independ,0.006644
indic,0.007958
individu,0.006672
infer,0.024865
ingo,0.024177
initi,0.007029
inner,0.013004
insect,0.073499
involv,0.006271
isbn,0.015295
ischium,0.030883
issn,0.016692
ja,0.019225
jaw,0.019225
job,0.011171
kuzniar,0.032728
l,0.009521
labial,0.029667
larg,0.005700
leaf,0.017538
leav,0.027478
leg,0.112389
leipzig,0.019140
leunissen,0.032728
level,0.012935
limbbud,0.032728
line,0.007740
link,0.004702
list,0.006013
lizard,0.038986
lizardlik,0.032728
lobefin,0.029667
logo,0.016838
london,0.009106
lost,0.009398
maint,0.017295
major,0.005737
make,0.005499
male,0.024261
malleu,0.052120
mammal,0.068456
mani,0.024441
manner,0.010437
mapl,0.022286
match,0.011501
maxillari,0.029185
mc,0.019003
meyer,0.018251
middl,0.008916
mindel,0.028759
modif,0.025708
modifi,0.010552
molecular,0.022607
morphologi,0.026914
mosaic,0.018018
multipl,0.041590
new,0.009661
norton,0.033705
note,0.006235
novemb,0.009699
nucleotid,0.017312
occupi,0.010841
occur,0.006870
onc,0.008080
onli,0.014934
opposit,0.008898
organ,0.041292
organismen,0.027431
origin,0.017979
ortholog,0.206999
orthoptera,0.030883
ovari,0.042296
ovipositor,0.058370
owen,0.017937
pachter,0.030883
pachyrhachi,0.032728
pair,0.057623
palp,0.060449
paradigm,0.013103
parallel,0.011205
paralog,0.138602
parsimoni,0.020284
partial,0.009948
pdf,0.012516
person,0.007076
plan,0.016380
plant,0.028167
pmid,0.049993
pongor,0.032728
posit,0.006409
possess,0.009718
power,0.006346
present,0.006200
primari,0.016372
primat,0.035185
primordia,0.028379
problem,0.013264
problematicu,0.032728
process,0.005862
project,0.007673
protein,0.050393
provid,0.005502
pterosaur,0.060449
pubi,0.030224
purpos,0.008066
quadrat,0.017997
quest,0.015618
quit,0.010480
r,0.016596
radiu,0.016406
rc,0.020481
read,0.006830
record,0.008290
refer,0.003932
region,0.014085
relat,0.021211
relationship,0.007507
remain,0.006504
reproduct,0.026720
research,0.012760
result,0.016435
root,0.009935
run,0.008643
s,0.005578
said,0.008080
sean,0.040963
second,0.006454
secondari,0.011199
seed,0.013954
segment,0.067374
separ,0.028741
sequenc,0.174698
serv,0.007828
shape,0.009374
share,0.068976
shoot,0.033357
short,0.008385
signific,0.013974
similar,0.052289
similarli,0.031109
simpl,0.008697
singl,0.014650
skeleton,0.016735
small,0.013395
snake,0.052140
somit,0.030224
soon,0.010037
sound,0.011524
spec,0.024927
speci,0.050333
specialis,0.031717
speciat,0.058296
spinou,0.030224
stape,0.027720
state,0.009578
stem,0.012303
stinger,0.054862
storag,0.013025
strong,0.015906
structur,0.083138
studi,0.005784
success,0.014517
suggest,0.007662
support,0.006508
sycamor,0.028034
synapomorphi,0.027720
synonym,0.013660
taken,0.008014
taxa,0.094485
taxonom,0.017760
term,0.020688
test,0.008948
testicl,0.048131
tetrapod,0.070668
thi,0.008188
thigh,0.024177
thu,0.006398
tibia,0.026255
tissu,0.026453
today,0.015726
trait,0.012969
transmit,0.013411
tree,0.011275
trend,0.031364
typic,0.007343
ulna,0.027163
ultim,0.009467
underw,0.015881
use,0.022362
van,0.011973
varieti,0.008218
vergleichend,0.032728
vertebr,0.081321
vertebra,0.022637
vertic,0.014160
view,0.007015
vs,0.013215
wa,0.004488
walter,0.013056
way,0.005852
whale,0.034103
wholegenom,0.026914
wide,0.007049
wing,0.193725
wirbelthier,0.032728
wm,0.021401
word,0.007598
ww,0.039174
york,0.016860
