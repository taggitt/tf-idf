abl,0.002900
acceler,0.004416
acceptor,0.008200
accord,0.002296
achiev,0.002888
acid,0.050888
act,0.007872
activ,0.020620
ad,0.002930
adenin,0.054026
adjac,0.005255
adopt,0.006337
aform,0.023391
akin,0.006145
alex,0.006777
allow,0.002283
alreadi,0.003345
alter,0.004118
altman,0.008923
amino,0.035660
analysi,0.002767
andrew,0.004663
ani,0.004047
anim,0.006427
anna,0.006555
anoth,0.008833
anticodon,0.009766
antisens,0.019866
aris,0.003745
aromat,0.006984
arrang,0.003743
arthur,0.004683
assembl,0.007168
assist,0.003423
associ,0.002243
attach,0.021735
attack,0.003702
attempt,0.002619
award,0.012143
backbon,0.006534
backsplic,0.012079
bacteria,0.005271
bacteriophag,0.007957
baltimor,0.006760
base,0.020007
basepair,0.017846
begin,0.002699
bform,0.010771
bind,0.028685
biochem,0.005692
biolog,0.019701
biologist,0.005180
biomolecular,0.008041
biopolym,0.008062
block,0.004124
bodi,0.002778
bond,0.031635
bound,0.004142
breakdown,0.005699
build,0.002929
bulg,0.015402
c,0.010598
cajal,0.008693
cap,0.005648
carbohydr,0.006534
carbon,0.004519
carbonrich,0.009422
carl,0.004629
carri,0.014946
catalys,0.007978
catalysi,0.015059
catalyt,0.021351
catalyz,0.026754
caus,0.004949
cc,0.006760
cech,0.009766
cell,0.043867
cellular,0.015392
center,0.005915
certain,0.005075
chain,0.020713
chang,0.002090
charg,0.010735
chemic,0.014123
chiral,0.007652
chromosom,0.012052
circrna,0.036237
circuit,0.005231
circular,0.010808
cisregulatori,0.010614
cleav,0.015209
close,0.002481
cloud,0.005304
cn,0.007003
coat,0.005493
code,0.017941
codon,0.023581
collect,0.005410
combin,0.002644
common,0.002242
commonli,0.006159
commun,0.002424
comparison,0.003888
compart,0.007240
complementari,0.021641
complet,0.002561
complex,0.008141
compon,0.006387
compos,0.010555
compound,0.008358
condit,0.002595
conform,0.005118
consequ,0.003153
consist,0.004964
constitut,0.002928
contact,0.003891
contain,0.021285
context,0.003262
control,0.002464
convey,0.005599
copi,0.017884
correspond,0.006733
coval,0.006583
craig,0.006576
crispr,0.009422
crystal,0.005203
crystallographi,0.006712
cut,0.003937
cytoplasm,0.020208
cytosin,0.047869
databas,0.004339
davi,0.005377
david,0.006589
deamin,0.009549
decod,0.007500
decreas,0.003652
deep,0.004128
defens,0.004070
defin,0.002545
degrad,0.031924
deliv,0.004371
demonstr,0.003333
denot,0.004525
deriv,0.002819
determin,0.010302
develop,0.003709
dictat,0.005304
differ,0.001966
dinucleotid,0.009199
direct,0.012314
discov,0.022862
discoveri,0.010421
distinguish,0.003292
divers,0.003567
dna,0.119665
domain,0.003622
donor,0.005815
doubl,0.016424
doublestrand,0.047747
downregul,0.017459
downstream,0.007205
dribonucleotid,0.012079
dribos,0.012079
drna,0.011398
drug,0.004374
dsrna,0.020693
dulbecco,0.011155
dure,0.004269
dust,0.006055
earli,0.004466
earliest,0.003732
effector,0.008468
elegan,0.007860
element,0.011359
elong,0.006917
elsewher,0.004603
enantiom,0.009149
encod,0.027187
end,0.002441
endogen,0.006760
entir,0.002896
enzym,0.070568
enzymat,0.007444
enzymerna,0.012079
essenti,0.006207
eukaryot,0.061504
everi,0.002794
evolut,0.003625
exampl,0.005999
export,0.004096
express,0.011572
extens,0.003015
extern,0.001745
factori,0.004885
far,0.003027
fashion,0.009399
featur,0.003019
femal,0.004483
fier,0.009690
flexibl,0.004943
fold,0.022768
form,0.025551
format,0.006513
formationrev,0.012079
frequent,0.003356
friedrich,0.004883
fulli,0.003599
function,0.017112
g,0.009690
ga,0.004017
gametogenesi,0.010949
gene,0.083391
gener,0.003431
genet,0.032444
genom,0.037956
geometri,0.009739
germlin,0.008382
giant,0.005221
glean,0.007668
gnra,0.012079
gobind,0.008560
greater,0.003007
greatest,0.004201
groov,0.016821
group,0.015076
grow,0.003061
guanin,0.048247
guanineadenin,0.012079
guid,0.003375
ha,0.011144
hairpin,0.009766
har,0.006657
helic,0.022054
helicas,0.009847
helix,0.021617
help,0.002613
hi,0.002140
highli,0.006286
histori,0.001988
holley,0.018299
host,0.008732
howard,0.005538
howev,0.007751
hybrid,0.004988
hydrocarbon,0.006176
hydrogen,0.014836
hydroxyl,0.015272
hypothes,0.004747
hypothesi,0.004158
hypoxanthin,0.010949
imag,0.003676
impli,0.003712
import,0.006109
inactiv,0.008019
includ,0.008287
increas,0.002296
inform,0.016616
initi,0.002594
inosin,0.021543
instanc,0.006467
interact,0.002979
interf,0.013604
interfac,0.005197
interfer,0.034115
interferon,0.009484
intern,0.002092
interstellar,0.006926
intrachain,0.010949
introduc,0.002800
intron,0.041640
intronsnoncod,0.012079
involv,0.009258
ion,0.005177
join,0.003551
journal,0.003070
just,0.002784
key,0.006076
khorana,0.009009
kingdom,0.003175
known,0.007979
kornberg,0.018504
laboratori,0.008102
lack,0.003043
larg,0.004207
later,0.004748
lead,0.004763
led,0.005152
length,0.011881
letter,0.003940
life,0.005132
ligat,0.008729
like,0.017057
link,0.005207
linkag,0.006293
list,0.002219
lncrna,0.011695
locat,0.002934
long,0.020734
loop,0.016490
lribonucleotid,0.012079
lribos,0.012079
lrna,0.024158
macromolecul,0.013656
mainli,0.006627
major,0.004235
make,0.002029
mammal,0.005053
mammalian,0.006499
mani,0.019845
march,0.003309
mari,0.004611
marshal,0.005290
materi,0.008391
matur,0.009289
medicin,0.007611
mello,0.008965
messag,0.004960
messeng,0.025314
metal,0.004154
meteorit,0.007444
methyl,0.013872
mg,0.007301
microrna,0.036036
miescher,0.010025
minor,0.003693
mirna,0.050126
modif,0.028465
modifi,0.027262
molecul,0.064648
mostli,0.003405
mrna,0.154652
ms,0.005998
narrow,0.004507
natur,0.006392
ncrna,0.011398
nearli,0.003476
need,0.002399
neg,0.003461
new,0.005348
nirenberg,0.008965
nitrogen,0.005509
nobel,0.037170
noncod,0.047869
nonproteincod,0.011155
normal,0.003181
notabl,0.009188
nt,0.053019
nuclear,0.004078
nucleic,0.026220
nuclein,0.010771
nucleolar,0.021899
nucleolu,0.019099
nucleoprotein,0.009847
nucleosid,0.027599
nucleotid,0.076673
nucleu,0.021150
number,0.007987
numer,0.005914
observ,0.002634
occur,0.012679
ochoa,0.020248
omethylribos,0.012079
onc,0.002982
onli,0.003674
opposit,0.003284
organ,0.006531
outer,0.005099
output,0.004166
overview,0.003999
pack,0.005824
pah,0.008766
pair,0.008506
particl,0.004290
pathogen,0.006420
pathway,0.005360
peptid,0.013361
peptidyl,0.010771
perform,0.002805
petunia,0.010614
philip,0.004906
phosphat,0.012342
phosphodiest,0.010346
phosphorylas,0.009306
pirna,0.022796
piwiinteract,0.023391
place,0.002257
plant,0.010395
plastid,0.008625
play,0.008645
polioviru,0.010230
polya,0.009484
polyanion,0.010771
polycycl,0.008529
polymer,0.007149
polymeras,0.020952
polymeraseus,0.012079
polynucleotid,0.009009
polypeptid,0.007574
posit,0.009461
possibl,0.002334
posttranscript,0.010025
precursor,0.004986
premrna,0.040496
presenc,0.010383
present,0.002288
prevent,0.003272
primari,0.003021
prize,0.021343
process,0.015145
produc,0.004762
progress,0.003119
prokaryot,0.020407
promin,0.003589
promot,0.003264
protect,0.003050
protein,0.106942
provid,0.002030
pseudouridin,0.023391
purin,0.008439
pyle,0.010949
pyrimidin,0.025876
rare,0.003766
rdnaderiv,0.012079
reaction,0.011166
reactionsan,0.012079
recent,0.002513
recognit,0.007957
recogniz,0.006752
red,0.004023
refer,0.001451
region,0.015596
regul,0.017182
regulatori,0.020094
reli,0.003497
remov,0.006702
renato,0.009363
replic,0.020494
reportedli,0.005764
requir,0.002285
research,0.002354
respons,0.007553
result,0.006065
retrotransposon,0.010346
retrovirus,0.009422
reveal,0.003818
revers,0.015984
ribonucl,0.008803
ribonucleas,0.009847
ribos,0.037453
ribosom,0.142340
ribosomean,0.012079
riboswitch,0.011695
ribothymidin,0.012079
ribozym,0.025977
rich,0.004162
richard,0.003586
rna,0.873024
rnadepend,0.021228
rnai,0.009363
rnase,0.009933
robert,0.006383
roger,0.004867
role,0.020342
rout,0.004370
rrna,0.101622
s,0.016469
scaffold,0.007620
second,0.002382
secondari,0.012400
section,0.003596
selfcomplementari,0.012079
selfrepl,0.007823
seminar,0.006598
sens,0.002986
separ,0.002651
sequenc,0.040297
sever,0.008424
severo,0.010771
shallow,0.006140
share,0.005657
sharp,0.004906
short,0.003095
shorter,0.005167
shown,0.007001
sidney,0.006993
signal,0.004176
silenc,0.012945
similar,0.009649
sinc,0.004040
singl,0.008110
singlestrand,0.016878
sirna,0.067831
site,0.017014
small,0.027192
snorna,0.031843
snrna,0.034194
socal,0.004076
sourc,0.002541
space,0.003114
specif,0.012085
splice,0.031441
spliceosom,0.029800
spong,0.007390
spot,0.005057
spread,0.003570
srrna,0.012079
stabilis,0.006881
stabl,0.003975
stall,0.006720
starter,0.010124
stop,0.003991
strand,0.034562
structur,0.040125
studi,0.004269
subunit,0.006785
sugar,0.014767
suggest,0.002828
suspect,0.005398
synthes,0.026249
synthesi,0.054122
t,0.003514
tag,0.006562
tail,0.005848
target,0.007522
tc,0.007529
team,0.004218
telomeras,0.009484
temin,0.008842
templat,0.026630
term,0.001908
termin,0.004733
tertiari,0.012218
tetraloop,0.010614
themselv,0.002996
therebi,0.004181
thi,0.013599
thoma,0.003613
thought,0.005726
thymin,0.007978
time,0.007149
tmrna,0.012079
togeth,0.005571
tool,0.003234
topolog,0.011117
transcrib,0.040763
transcript,0.053693
transcriptas,0.009252
transfer,0.017284
transferas,0.009618
transfermesseng,0.012079
translat,0.020867
transmiss,0.004990
transposon,0.008693
trigger,0.004914
trna,0.083280
trnaderiv,0.012079
tsrna,0.012079
type,0.007286
typic,0.002710
u,0.014141
understood,0.003791
univers,0.004184
unknown,0.004182
unlik,0.006728
untransl,0.028268
unwound,0.009618
upstream,0.014754
uracil,0.069551
use,0.014856
usual,0.009724
variou,0.004599
veri,0.002307
vertebr,0.006002
viral,0.019882
viroid,0.009054
viru,0.012311
virus,0.042283
w,0.003389
wa,0.013251
walter,0.004818
way,0.004320
websit,0.003820
wide,0.002601
widespread,0.003881
win,0.004542
wobbl,0.008498
woes,0.009101
won,0.004089
work,0.001980
world,0.004096
x,0.003576
xist,0.011695
xray,0.005509
year,0.002038
yeast,0.006752
