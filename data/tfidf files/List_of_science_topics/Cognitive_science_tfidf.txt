aaai,0.014525
abil,0.037797
abl,0.014871
abov,0.005024
abstract,0.013811
academ,0.005878
accomplish,0.007887
accord,0.003924
account,0.013866
accur,0.013372
accuraci,0.008511
acquir,0.044746
acquisit,0.008485
action,0.024110
activ,0.023496
actor,0.018093
actual,0.005022
adopt,0.005415
adult,0.007683
advantag,0.012183
advoc,0.013473
affect,0.010256
ago,0.007604
ai,0.049295
aim,0.005604
alan,0.016639
algorithm,0.023603
allen,0.009060
allow,0.035124
alway,0.005277
analysi,0.014192
analyt,0.007155
analyz,0.006596
ancient,0.005686
ani,0.013835
anim,0.010986
anima,0.013744
anopia,0.017902
anoth,0.015098
anthropolog,0.017794
anthropologist,0.010068
anyth,0.007756
appar,0.013399
appear,0.004545
appli,0.004195
approach,0.049683
appropri,0.006290
architectur,0.007402
area,0.031182
argu,0.026104
aristotl,0.008090
array,0.017127
art,0.006003
articul,0.009478
artifici,0.066273
ascrib,0.010365
ask,0.012975
aspect,0.036105
assert,0.006741
assist,0.005851
associ,0.011501
assum,0.005436
atran,0.016105
attain,0.007944
attempt,0.008953
attent,0.038397
attenu,0.012458
attribut,0.012201
auditori,0.012479
avail,0.004797
avoid,0.005861
awar,0.014201
b,0.005198
bach,0.013567
barbara,0.010391
basic,0.004760
bayesian,0.010910
bear,0.014688
becam,0.012976
becaus,0.003562
becom,0.007576
befor,0.004041
began,0.009450
behavior,0.133224
behaviorist,0.012946
believ,0.004894
benedict,0.012109
best,0.010431
better,0.016009
bia,0.009023
bias,0.009806
bicycl,0.012844
biolog,0.022449
biologist,0.008854
bistabl,0.015907
bit,0.009624
blind,0.009685
blood,0.038531
bloodstream,0.013403
blur,0.011018
bodi,0.004748
bombard,0.021007
book,0.008994
born,0.006684
borrow,0.015707
boyer,0.012946
brain,0.174169
brainascomput,0.017902
breakfast,0.013671
broad,0.012712
cabani,0.016439
california,0.014518
capac,0.013139
captur,0.006507
carl,0.007912
caus,0.004230
central,0.008885
certain,0.017351
chalmer,0.012972
character,0.018034
cherri,0.013371
child,0.007612
chines,0.006812
choic,0.018682
chomski,0.046277
christoph,0.016612
circuitri,0.013403
circumst,0.007280
cite,0.006629
claim,0.004811
classic,0.005236
clearli,0.007126
close,0.012723
coercion,0.012437
cognit,0.668790
cognitivist,0.015047
coin,0.006563
collabor,0.007280
colleagu,0.008567
collect,0.013870
colleg,0.013760
color,0.015791
combin,0.004519
come,0.004533
comment,0.007592
commentari,0.009137
common,0.003832
compar,0.014065
compat,0.009094
complet,0.004377
complex,0.027830
complic,0.007183
compon,0.005459
compos,0.006013
comprehens,0.006982
comput,0.141817
concept,0.008847
conceptu,0.007629
concern,0.017801
condit,0.004435
conduct,0.005433
confus,0.007134
connect,0.010438
connection,0.041343
connectionist,0.043423
connectionistneur,0.017902
consciou,0.018746
conscious,0.032299
consid,0.003696
consider,0.010654
consist,0.008485
constitut,0.010010
contain,0.009095
content,0.006254
context,0.016729
continu,0.003840
contribut,0.009305
contributor,0.009272
controversi,0.012493
core,0.006731
correl,0.014778
correspond,0.005754
cortex,0.011527
cortic,0.012795
cover,0.005219
covert,0.012609
creat,0.007754
critic,0.009325
cube,0.023615
cultur,0.014299
current,0.004058
cut,0.006730
cyberneticist,0.015555
damag,0.006841
dan,0.010267
daniel,0.007884
david,0.016893
day,0.009567
debat,0.017704
decad,0.005762
decid,0.006177
decis,0.026723
decisionmak,0.009137
declar,0.012245
declin,0.006018
deemphas,0.013636
deficit,0.008798
defin,0.004350
degre,0.005191
delay,0.008096
dennett,0.013106
deoxygen,0.015638
depart,0.011116
depend,0.004182
descart,0.009920
descript,0.011589
detect,0.006961
determin,0.013206
develop,0.044384
dichot,0.033956
did,0.004705
diego,0.021473
differ,0.043687
differenti,0.006418
difficult,0.005528
digit,0.007078
direct,0.016837
directli,0.005093
disciplin,0.005841
discov,0.005582
discoveri,0.005937
discuss,0.005172
divid,0.005026
doe,0.004289
domain,0.006191
domin,0.010432
doubt,0.008181
dougla,0.009367
download,0.011191
dr,0.007865
draw,0.006498
drawn,0.007481
drive,0.013622
dualism,0.011029
dub,0.010197
dure,0.003649
dynam,0.005943
dysfunct,0.011340
dyslexia,0.015814
ear,0.010128
earli,0.019086
earliest,0.006379
eat,0.008906
econom,0.004176
educ,0.004862
edwin,0.010068
eeg,0.041015
electr,0.018931
electrod,0.011597
electroencephalographi,0.013935
element,0.009707
elman,0.015638
embodi,0.017648
emiss,0.018112
emot,0.023520
emphas,0.020094
emphasi,0.006922
empiricist,0.012316
encompass,0.007144
end,0.004172
endow,0.009666
engag,0.006043
engin,0.005500
enlarg,0.009748
enterpris,0.007183
entir,0.004950
entri,0.022146
environ,0.025295
environment,0.006135
episod,0.009373
epistemolog,0.009070
equal,0.005197
escher,0.014631
especi,0.004431
establish,0.004073
event,0.004966
eventu,0.005424
everi,0.009551
evid,0.010408
evolutionari,0.015038
evolv,0.006264
examin,0.005973
exampl,0.030762
exhaust,0.008923
exist,0.007590
experi,0.061011
experiment,0.006374
expert,0.007538
explain,0.020550
explan,0.013436
explanatori,0.021513
extend,0.005034
extent,0.012559
extern,0.005968
extrem,0.032847
eye,0.038148
f,0.005621
fact,0.009691
factor,0.004839
faculti,0.017631
fall,0.005209
familiar,0.008074
famou,0.012609
fashion,0.008032
featur,0.005161
feebl,0.014144
feel,0.014269
field,0.079188
fifti,0.009673
fillintheblank,0.017684
financ,0.006047
firstlanguag,0.017135
fixat,0.011776
flow,0.006010
fmri,0.040501
focu,0.022395
focus,0.015937
fodor,0.013601
follow,0.003264
forc,0.004090
form,0.018716
formal,0.015320
format,0.005566
forth,0.008296
frame,0.007799
free,0.004661
fulli,0.006152
function,0.062674
functionalist,0.014017
fundament,0.005226
gain,0.009930
gave,0.005938
gdel,0.011700
gene,0.015837
gener,0.017595
genet,0.020795
georg,0.011763
given,0.012279
goal,0.011226
gofai,0.015323
good,0.004537
govern,0.011642
grammar,0.020301
grammat,0.012376
grant,0.006256
greatli,0.006683
greek,0.005779
group,0.003681
guid,0.005770
gustatori,0.016562
ha,0.048982
hallmark,0.012479
hampshir,0.012565
hand,0.005056
haptic,0.014983
hard,0.006698
hardwar,0.009547
hear,0.008605
held,0.004825
help,0.022339
hemispati,0.017304
henrich,0.017135
herbert,0.008836
hi,0.007318
high,0.004132
higherlevel,0.013163
highli,0.005372
hint,0.011109
histori,0.010197
hofstadt,0.013567
hold,0.004915
hope,0.020845
horizon,0.009947
howev,0.013248
huge,0.007589
human,0.090432
humanlik,0.012998
hume,0.010621
hutchin,0.015047
idea,0.004580
identifi,0.004933
illus,0.010076
imag,0.050271
immanuel,0.010300
imper,0.011041
implement,0.022727
implicit,0.009558
import,0.003480
imposs,0.006900
improv,0.010043
inattent,0.015555
includ,0.039661
increas,0.003924
inde,0.013878
index,0.006483
indic,0.015061
indirect,0.008866
individu,0.021047
infant,0.029418
influenti,0.019690
inform,0.064915
infrar,0.010539
inher,0.007820
inject,0.010076
innat,0.040159
innov,0.007068
input,0.014787
insight,0.007570
inspir,0.006752
instanc,0.016580
instead,0.004780
institut,0.008636
instrument,0.006283
integr,0.009849
intellectu,0.014002
intellig,0.127352
interact,0.010183
interdisciplinari,0.033555
intern,0.014304
interpret,0.010889
interrel,0.009961
interrelationship,0.011871
investig,0.017155
involv,0.023737
isotop,0.019771
issu,0.004379
jame,0.011511
jcr,0.034270
jeffrey,0.010603
jerri,0.011240
john,0.027850
johnson,0.009325
johnsonlaird,0.017304
joseph,0.006724
journal,0.005248
judgment,0.017297
just,0.004758
kant,0.009872
kevin,0.010707
key,0.005193
kind,0.005435
know,0.018549
knowledg,0.066724
knowledgebas,0.012920
known,0.020458
lack,0.005202
lakoff,0.029051
languag,0.097440
larg,0.010787
late,0.004723
later,0.012173
layer,0.015707
lead,0.008141
learn,0.068778
led,0.008806
leibniz,0.010181
level,0.028559
lewandowski,0.017902
licklid,0.032008
life,0.004385
light,0.016874
lighthil,0.015724
like,0.007288
limit,0.008333
linguist,0.088227
link,0.008900
lisp,0.012920
list,0.022762
listen,0.019272
liter,0.007845
literatur,0.006095
litter,0.013163
littl,0.010443
live,0.004411
local,0.004620
locationbas,0.015907
lock,0.008836
logic,0.013206
longer,0.005626
longlost,0.016439
longterm,0.021538
longuethiggin,0.033385
look,0.005633
lot,0.008402
loud,0.012277
machin,0.019761
mack,0.014686
magnet,0.032034
magnetoencephalographi,0.014983
main,0.008638
major,0.010858
make,0.020815
malebranch,0.015181
manag,0.004886
mani,0.018501
manner,0.006584
map,0.006241
marr,0.014424
marvin,0.036383
mathemat,0.010726
mcclelland,0.029719
mcculloch,0.028654
mean,0.015028
measur,0.049152
mechan,0.005038
media,0.006257
meet,0.005519
meg,0.029841
memori,0.127738
memorygroup,0.017902
memoryref,0.017902
mening,0.014280
meno,0.015475
mental,0.044293
messag,0.025434
metaphor,0.028783
meter,0.009181
method,0.033801
methodolog,0.014442
michael,0.006278
miller,0.009524
million,0.005358
mind,0.109370
mindbrain,0.033662
mindth,0.017486
minski,0.037437
minut,0.008143
mit,0.008289
model,0.127487
moder,0.015630
modern,0.016368
modular,0.011418
monitor,0.007377
morpholog,0.009346
motor,0.026396
movement,0.028225
movi,0.018661
multipl,0.015741
nativist,0.015181
natur,0.025494
naturalist,0.009293
near,0.005609
necker,0.015907
need,0.012303
neglect,0.008923
neil,0.010837
nervou,0.009341
net,0.007171
network,0.028516
neumann,0.032239
neural,0.064168
neuralsymbol,0.017902
neurobiolog,0.011597
neuron,0.069780
neuropsycholog,0.012239
neurosci,0.047029
new,0.006094
newel,0.012820
newfound,0.013220
nicola,0.010220
noam,0.034880
node,0.020567
nonhuman,0.010468
norm,0.007912
normal,0.010875
notabl,0.015704
novel,0.007550
number,0.017064
nurtur,0.011252
o,0.007194
object,0.013990
observ,0.031518
occur,0.008668
okeef,0.015814
old,0.005644
oldfashion,0.013935
olfactori,0.014058
oneself,0.010678
onli,0.021982
onlin,0.005970
opaqu,0.012416
open,0.004670
oper,0.017215
optic,0.024621
option,0.007118
order,0.018797
organ,0.022327
orient,0.007108
orthographi,0.013976
outlin,0.019921
output,0.021361
oxygen,0.033225
page,0.013376
paradigm,0.024798
parallel,0.014137
park,0.007973
particip,0.005381
particular,0.038431
pascal,0.010612
pattern,0.005820
peopl,0.007522
perceiv,0.013721
percept,0.072615
perform,0.004795
period,0.004053
person,0.026784
personoid,0.017902
perspect,0.012209
persuas,0.010746
pertain,0.009433
pet,0.021633
phenomena,0.019541
phenomenon,0.021078
philip,0.008385
philosoph,0.023952
philosophi,0.034849
phone,0.038941
phonet,0.012820
phonolog,0.013025
photon,0.009968
physic,0.022320
piec,0.007533
pierr,0.008782
pinker,0.039930
pitt,0.025543
place,0.011574
plan,0.015499
plato,0.017973
platon,0.010889
play,0.004925
plural,0.008773
point,0.007914
polit,0.012845
poor,0.020235
popul,0.004853
popular,0.014642
posit,0.004043
positron,0.011670
posner,0.013857
possibl,0.015960
postmortem,0.014424
power,0.012010
practic,0.008370
pragmat,0.009535
precis,0.012887
precursor,0.008522
prefer,0.005990
prehistori,0.009825
presenc,0.005915
present,0.015646
presid,0.005657
presum,0.008732
primarili,0.005347
principl,0.009391
problem,0.020919
procedur,0.020438
process,0.129437
product,0.008154
professor,0.006991
profici,0.012336
program,0.019684
progress,0.005331
project,0.004840
prolong,0.009947
promin,0.006134
properti,0.014209
proport,0.006467
provid,0.006941
psycholog,0.100023
psychologist,0.026892
psychophys,0.038460
publish,0.008684
punish,0.008605
question,0.019650
quit,0.006611
quotat,0.010391
radioact,0.019200
rang,0.009391
rapidli,0.006595
ration,0.006741
reaction,0.012723
read,0.004309
realiti,0.006748
realiz,0.013874
realm,0.007837
realtim,0.010399
reason,0.008905
recal,0.028783
receiv,0.004740
recent,0.008592
recogn,0.021279
recognit,0.006800
record,0.010459
refer,0.007442
reflect,0.015995
regard,0.004759
region,0.008885
rel,0.008896
relat,0.016725
relev,0.006489
reli,0.005978
religion,0.006319
remain,0.004103
remark,0.007609
rememb,0.045542
report,0.009537
repres,0.008139
represent,0.050072
requir,0.003905
research,0.072449
resolut,0.068308
reson,0.009495
resort,0.009206
respons,0.025819
result,0.013823
rethink,0.011823
retriev,0.022113
review,0.011118
reviv,0.007884
revolut,0.012292
ride,0.010786
right,0.004452
rise,0.019970
risk,0.006104
robot,0.009196
rock,0.007769
role,0.008692
room,0.008047
root,0.006267
roughli,0.006959
rule,0.036230
rumelhart,0.014983
s,0.038706
safe,0.008111
san,0.015757
santa,0.009832
saw,0.006071
say,0.005375
scale,0.016846
scalp,0.028948
scath,0.014983
scheme,0.007467
school,0.005047
scienc,0.200292
scientif,0.009985
scientist,0.045386
scope,0.007293
scott,0.009181
search,0.012700
searl,0.012724
second,0.008143
secondlanguag,0.017135
section,0.012295
seen,0.004868
select,0.010789
self,0.008526
semant,0.027590
sens,0.015315
sensor,0.010283
sensori,0.009618
sentenc,0.025445
sequenc,0.006887
seri,0.004942
serial,0.011240
set,0.011529
shine,0.011715
short,0.010580
shortterm,0.018037
showcas,0.013079
similar,0.008246
similarli,0.006541
simon,0.008296
simpl,0.016459
simul,0.037512
sinc,0.010358
singl,0.009241
singleunit,0.015638
sit,0.009127
situat,0.005316
skinner,0.023775
sloan,0.012565
small,0.008450
soar,0.011191
social,0.017570
socialorganiz,0.017902
societi,0.012978
sociocognit,0.016978
sociolog,0.008181
solv,0.006354
someth,0.026554
sometim,0.004494
sought,0.013453
sound,0.014540
span,0.007976
spatial,0.056503
specif,0.028918
specifi,0.007020
spect,0.014859
speech,0.037196
sperber,0.015724
spinoza,0.011807
spotlight,0.013781
squid,0.012820
stahmer,0.017902
start,0.004370
state,0.015106
step,0.005855
steven,0.017400
stimul,0.007727
stimuli,0.042265
stimulu,0.020226
store,0.027981
strohmetz,0.017902
structur,0.024205
studi,0.124068
subject,0.021962
subset,0.008660
substanti,0.006409
subsymbol,0.046667
support,0.004105
surround,0.006323
symbol,0.068955
syntax,0.010931
syntheticabstract,0.017902
tactil,0.013403
taken,0.015167
task,0.038432
techniqu,0.026531
technolog,0.009956
tempor,0.045494
tend,0.011340
tenet,0.010166
term,0.019576
text,0.005956
textbook,0.015565
textur,0.024292
theme,0.007647
themselv,0.005121
thencurr,0.014983
theoret,0.005661
theori,0.044520
thi,0.056818
thing,0.005494
think,0.017964
thinker,0.008347
thought,0.029361
threshold,0.009618
thu,0.012109
tie,0.013470
time,0.033605
timebas,0.015323
tini,0.009314
told,0.008757
tomographi,0.024184
tone,0.020954
took,0.005394
tool,0.022115
topic,0.032870
trace,0.019862
traceabl,0.014101
track,0.015279
tradit,0.008696
trait,0.008181
transform,0.016956
translat,0.005944
transmitt,0.011855
tree,0.014225
tri,0.005434
truth,0.007346
ture,0.020884
turri,0.017304
type,0.016605
typic,0.009264
ultim,0.005972
unattend,0.016004
unawar,0.011228
uncov,0.009819
undergradu,0.018393
underli,0.006557
understand,0.079678
understood,0.006480
unifi,0.007227
univers,0.014304
unrealist,0.022605
usa,0.007708
usabl,0.010594
use,0.067712
usual,0.004155
vari,0.010180
variant,0.008364
varieti,0.005184
variou,0.011792
vassar,0.016831
verbal,0.009933
veri,0.007886
verif,0.010512
view,0.035404
visibl,0.007632
vision,0.007478
visual,0.036425
von,0.021009
vote,0.006547
wa,0.028311
walk,0.008819
walter,0.008236
warren,0.010083
warwick,0.013340
watch,0.008803
way,0.018459
web,0.007486
week,0.007257
went,0.006333
wherea,0.011983
whi,0.012706
wide,0.008894
wikimedia,0.009624
wikiquot,0.013079
wing,0.009400
wit,0.008410
word,0.019172
work,0.020310
world,0.010502
write,0.016196
writer,0.014173
year,0.010454
