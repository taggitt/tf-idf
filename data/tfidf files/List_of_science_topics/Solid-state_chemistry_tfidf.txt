academ,0.016276
act,0.012420
ad,0.027736
adduct,0.043541
advanc,0.013421
advent,0.023765
agent,0.018528
ahead,0.025369
air,0.016693
allow,0.021613
alreadi,0.015831
ampoul,0.254897
analog,0.019418
analysi,0.026199
andor,0.038034
anhydr,0.044889
anneal,0.037956
anoth,0.010452
applic,0.012978
array,0.023713
atom,0.019002
attach,0.020573
attempt,0.012395
band,0.023629
beam,0.029622
becaus,0.029591
bechgaard,0.050979
bibliographi,0.019057
boil,0.029309
box,0.024564
bragg,0.036772
bulk,0.023228
c,0.037621
ca,0.024102
carl,0.021910
carri,0.028295
case,0.043891
catalyst,0.028657
cell,0.056622
centuri,0.011269
ceram,0.030859
certain,0.024023
certainli,0.025050
character,0.149809
characterist,0.015126
charg,0.016935
chemic,0.033421
chemistri,0.134645
chlorin,0.031692
clue,0.032652
coat,0.025999
cold,0.020921
commerc,0.021871
compon,0.015116
composit,0.054588
compound,0.079113
conduct,0.030088
consider,0.029502
consist,0.011748
core,0.018640
counter,0.023734
crystal,0.049249
crystallin,0.032566
crystallographi,0.063534
curios,0.031366
current,0.011237
data,0.014008
defect,0.025910
demand,0.015671
depend,0.011581
deposit,0.020781
desir,0.016520
determin,0.012189
devic,0.039235
diagram,0.048293
difficult,0.015309
diffract,0.191055
diffus,0.047961
direct,0.011655
discov,0.015458
divers,0.033764
doe,0.011876
domain,0.017143
donald,0.024747
dri,0.022990
driven,0.020435
dsc,0.043073
dta,0.045197
dure,0.010104
earli,0.010569
easiest,0.035431
effort,0.014635
electr,0.017473
electrocrystallis,0.050979
electron,0.035368
element,0.013440
employ,0.042052
enabl,0.016759
end,0.011554
equal,0.014391
equip,0.018893
essenti,0.014690
establish,0.022556
ethylen,0.038264
evacu,0.028890
evapor,0.029595
exampl,0.009464
exclus,0.018053
extern,0.008263
facilit,0.019052
fall,0.014425
father,0.018579
field,0.011540
fine,0.022592
flux,0.027800
focu,0.015503
form,0.025913
fuel,0.020941
furnac,0.034555
ga,0.095060
gap,0.021998
gase,0.025195
gener,0.008120
given,0.022667
glove,0.039167
gradient,0.026887
ha,0.030140
halid,0.036216
hand,0.028003
handl,0.020548
hardli,0.030210
heat,0.039852
help,0.024742
high,0.057212
higher,0.026868
highpur,0.047915
histori,0.009412
homogen,0.025452
hydrotherm,0.036528
hygroscop,0.046605
idea,0.012683
identif,0.022258
identifi,0.027320
illustr,0.019271
import,0.019275
impos,0.019434
includ,0.007844
increas,0.010866
increasingli,0.016659
index,0.017951
induc,0.022376
industri,0.012898
inform,0.011234
ingot,0.039672
innov,0.019573
inorgan,0.026420
insid,0.018952
institut,0.011957
introduct,0.014980
invent,0.018387
involv,0.021909
iodin,0.068878
ion,0.024505
iter,0.028278
knowledg,0.014212
known,0.047208
later,0.011235
lawrenc,0.025275
lead,0.022544
let,0.021168
level,0.011297
librari,0.017749
like,0.030272
line,0.013521
link,0.016429
liquid,0.020250
look,0.015598
low,0.028956
make,0.009606
mani,0.051230
massachusett,0.024772
materi,0.132379
measur,0.012373
melt,0.134788
metallurgi,0.030762
method,0.128695
methodolog,0.039991
microelectron,0.038057
mineralog,0.032154
mit,0.022953
mixtur,0.141716
moistur,0.032783
molecular,0.019744
near,0.015532
necessarili,0.018491
new,0.050628
nitrogen,0.052151
nonmetal,0.037857
nonmolecular,0.042037
nonstoichiometr,0.048420
normal,0.015057
novel,0.020907
number,0.009450
obtain,0.073415
onc,0.014115
onli,0.017391
open,0.012932
opencoursewar,0.038813
oper,0.011917
optic,0.022725
organ,0.020607
origin,0.010468
oven,0.181853
overlap,0.020995
oxid,0.025063
oxygen,0.046000
oxygenfre,0.045520
paramet,0.021778
particularli,0.026495
pass,0.029351
pattern,0.048352
petroleum,0.024283
phase,0.199823
physic,0.024721
place,0.032049
platinumbas,0.048969
point,0.032873
polycristallin,0.050979
possibl,0.033145
powder,0.160972
precipit,0.024909
precursor,0.023598
prepar,0.122978
pressur,0.016305
prime,0.017404
procedur,0.037729
proceed,0.039647
process,0.010240
produc,0.011270
product,0.090318
progress,0.014763
properti,0.026231
pure,0.034257
question,0.013603
rang,0.013003
rare,0.017826
rate,0.013423
react,0.024002
reactant,0.169451
reaction,0.264239
reactiv,0.025880
redox,0.034613
refer,0.013739
refin,0.064652
rel,0.012317
relat,0.009262
relev,0.017968
requir,0.032444
rest,0.016095
revisit,0.031767
robust,0.024884
room,0.022284
s,0.038974
sadoway,0.050979
salt,0.070612
sampl,0.020472
sc,0.034673
scienc,0.011554
seal,0.077290
sem,0.041665
semiconductor,0.059959
sens,0.014136
sensit,0.044254
separ,0.025102
seri,0.013685
silicon,0.029183
similar,0.011417
singl,0.038386
size,0.014889
small,0.011699
solid,0.349324
solidifi,0.031473
solidst,0.136417
solut,0.032715
solv,0.017596
solvent,0.092382
sometim,0.049786
speci,0.017584
special,0.023915
spectra,0.031158
stabl,0.018816
state,0.058560
step,0.016212
stoichiometr,0.081965
stoichiometri,0.155717
straddl,0.036772
strong,0.013892
strongli,0.036855
structur,0.044683
studi,0.030312
subject,0.012162
suitabl,0.021224
superconduct,0.033336
symmetri,0.026616
synchrotron,0.039672
synthesi,0.064037
synthet,0.094852
systemat,0.018266
tantalum,0.039941
techniqu,0.058772
technolog,0.027570
temperatur,0.193285
temperaturedepend,0.043303
tetrathiafulvalen,0.050979
tga,0.048420
th,0.010963
thank,0.026292
theme,0.021175
theori,0.011206
therefor,0.012995
thermal,0.049033
thermodynam,0.024540
thi,0.057211
thirsti,0.043789
thu,0.011176
time,0.016918
togeth,0.013184
tool,0.015309
transfer,0.016361
transport,0.032781
treatment,0.035176
tri,0.015049
tube,0.105680
typic,0.025654
understand,0.012978
unit,0.019287
use,0.070311
usual,0.011505
uvvi,0.045197
vapour,0.037202
vari,0.014094
variat,0.035844
varieti,0.014355
veri,0.010918
vigor,0.027661
volatil,0.025438
wa,0.015678
wagner,0.035168
way,0.051115
wide,0.012314
william,0.015320
word,0.013271
work,0.009373
xray,0.104302
xrd,0.048420
zeolit,0.044595
zone,0.019761
