academ,0.011689
accept,0.009798
accid,0.018814
action,0.009589
activ,0.007787
actual,0.009988
ad,0.009959
address,0.035257
adequ,0.016372
administr,0.010912
advanc,0.009639
advic,0.033969
aesthet,0.037856
affair,0.012904
age,0.009773
airport,0.017190
al,0.026773
alan,0.033089
alleg,0.016137
allegedli,0.020096
allow,0.023283
alreadi,0.011369
amus,0.025542
analysi,0.009407
analyz,0.013118
ann,0.018278
annual,0.011988
answer,0.013896
anthropolog,0.017693
applic,0.018640
approach,0.053890
april,0.023878
architectur,0.014720
area,0.007751
art,0.023878
articl,0.009275
artifici,0.013179
ask,0.038704
assess,0.039879
attempt,0.008902
author,0.017574
authorit,0.021781
autumn,0.020765
avail,0.009540
awar,0.014120
background,0.013403
barbercheck,0.038741
barn,0.023357
barri,0.020663
bauchspi,0.037915
bazerman,0.036077
beauti,0.017880
becam,0.008601
becaus,0.007083
becom,0.007533
began,0.009396
behavior,0.011038
belief,0.012597
believ,0.009732
bendavid,0.035600
better,0.010611
biagioli,0.077483
bibliographi,0.013686
bijker,0.037216
bioethic,0.026064
biolog,0.011160
biomed,0.041327
black,0.012700
blackwel,0.019672
bloor,0.069547
bodi,0.009443
book,0.008943
borrow,0.015618
boundari,0.026275
bovin,0.026846
brain,0.015058
brian,0.019248
broad,0.012639
broader,0.015690
brought,0.011599
bruno,0.090515
bse,0.028216
busi,0.010608
calif,0.027712
cambridg,0.049929
carbondal,0.032237
carri,0.010160
case,0.007880
catch,0.018974
caus,0.016824
certain,0.008626
challeng,0.011258
chang,0.007105
charl,0.012071
cheltenham,0.028684
chernobyl,0.114347
chicago,0.015361
claim,0.009567
classic,0.010413
cognit,0.044829
colleagu,0.017037
collect,0.009194
collid,0.021351
collin,0.022256
combin,0.008988
commun,0.024727
compar,0.027969
complet,0.008706
complex,0.018447
compon,0.010855
concept,0.026390
conceptu,0.015172
concern,0.008849
condit,0.008820
consensu,0.015724
consid,0.007351
construct,0.029284
contain,0.018086
contamin,0.020111
context,0.044356
controversi,0.012422
cookmey,0.038741
cooper,0.012048
cours,0.010986
creativ,0.016288
criteria,0.015786
critiqu,0.016015
croissant,0.036611
crucial,0.030690
cultur,0.056870
cumbria,0.069547
current,0.008070
cyborg,0.027874
david,0.033594
deal,0.010155
death,0.011040
decad,0.011459
decept,0.022161
decis,0.021256
deconstruct,0.023388
deem,0.017060
defin,0.017302
democraci,0.028083
democrat,0.037055
depend,0.008317
depet,0.038741
describ,0.007759
design,0.018518
destroy,0.013182
detect,0.013843
determin,0.008754
develop,0.025218
dichotomi,0.068043
differ,0.020048
difficult,0.021989
directli,0.010128
disast,0.034829
disciplin,0.011615
disciplinari,0.023297
discours,0.034523
discoveri,0.023613
dissolv,0.015656
distinct,0.009708
distinguish,0.011191
divis,0.010925
dominiqu,0.027405
donald,0.017772
donna,0.055425
donovan,0.061242
driver,0.019322
dumit,0.038741
durat,0.017953
dure,0.014513
easili,0.013002
econom,0.008305
ed,0.053123
edinburgh,0.020646
edward,0.013806
elabor,0.015792
elgar,0.024651
elicit,0.022654
emili,0.026468
emot,0.015590
encephalopathi,0.030328
encompass,0.014208
endang,0.042171
engin,0.010937
epistem,0.048235
epistemolog,0.036074
erupt,0.075803
establish,0.008099
estim,0.010679
et,0.026091
ethic,0.013968
ethnomethodolog,0.033762
evalu,0.013150
evan,0.021216
event,0.009876
examin,0.011878
exampl,0.020391
exist,0.007547
experi,0.037331
experiencebas,0.034411
expert,0.134919
expertis,0.129589
extern,0.005934
extrem,0.010886
face,0.011391
fact,0.009636
factor,0.009622
fadiman,0.034773
fall,0.010359
fals,0.015392
farmer,0.032127
farrar,0.027712
felt,0.032043
femin,0.022654
feminist,0.039787
field,0.041440
fieldwork,0.025642
fine,0.016224
focu,0.011134
forc,0.016268
form,0.006203
formal,0.010155
format,0.011068
foucault,0.023481
foundat,0.010582
free,0.009269
fuller,0.023771
fundament,0.020785
g,0.010978
gain,0.009873
geimer,0.038741
gener,0.017494
genesi,0.020418
geograph,0.026397
giroux,0.028216
given,0.008139
global,0.021554
govern,0.015434
greg,0.024531
gross,0.031735
grundmann,0.037216
ha,0.016234
hancock,0.030934
handbook,0.016424
hansjrg,0.037216
haraway,0.032458
hard,0.013319
harper,0.020905
harri,0.017067
harvard,0.015855
hazard,0.037067
helga,0.035168
henri,0.013080
hess,0.024117
hi,0.007276
highli,0.010683
histor,0.036465
histori,0.020278
hope,0.013817
howard,0.018826
howev,0.013172
hugh,0.019043
human,0.023456
hybrid,0.033910
iceland,0.020869
idea,0.009108
ident,0.011620
identifi,0.029431
imman,0.025210
immens,0.019500
immunolog,0.024011
impact,0.044487
import,0.027686
impos,0.013956
includ,0.011267
incommensur,0.027712
increas,0.007804
influenc,0.008938
inform,0.016136
infractructur,0.038741
innov,0.014057
insight,0.015053
institut,0.008587
instrument,0.012496
integr,0.009793
interact,0.070879
interdisciplinari,0.050046
interview,0.016780
introduc,0.019039
introduct,0.010758
introductori,0.019347
investig,0.011371
involv,0.039337
irrat,0.020905
isbn,0.028781
island,0.012028
issu,0.008708
itali,0.014604
j,0.030575
jasanoff,0.038741
jeff,0.024117
jen,0.025119
jennif,0.024571
john,0.009230
joseph,0.026746
karin,0.029922
kill,0.013396
kind,0.021619
kingdom,0.010794
knorrcetina,0.036611
knowledg,0.142894
known,0.006780
kuhn,0.047952
laboratori,0.013769
languag,0.010198
larg,0.021451
latour,0.149614
lawrenc,0.018151
lay,0.091609
lead,0.016190
led,0.008756
left,0.010815
lehigh,0.064054
lessig,0.034075
liber,0.012232
lie,0.013293
life,0.017443
linear,0.014435
link,0.005899
local,0.036756
loss,0.011866
m,0.009702
mackenzi,0.027331
magazin,0.015707
main,0.008588
major,0.014395
make,0.020697
maker,0.018715
man,0.012003
manag,0.019432
map,0.012411
mari,0.031347
maria,0.020052
mario,0.047025
martin,0.014360
mass,0.033377
match,0.014427
mathemat,0.010665
meanwhil,0.016404
media,0.024887
medicin,0.012934
mere,0.013346
merton,0.024943
metanarr,0.032027
method,0.033608
methodolog,0.028720
michel,0.018597
milieu,0.025256
miscommun,0.033762
mistaken,0.022401
mit,0.016484
mitchel,0.022733
mob,0.024651
model,0.008450
modern,0.016274
momentum,0.017835
montserrat,0.143919
moral,0.013572
motiv,0.013253
multipl,0.010434
myer,0.025445
nation,0.007517
natur,0.072426
navelgaz,0.038741
nd,0.012622
need,0.008155
neil,0.021550
network,0.011341
new,0.054538
news,0.014807
novel,0.015015
nowotni,0.033195
nuclear,0.027724
number,0.006786
nyu,0.027874
oak,0.021846
object,0.055644
onli,0.018734
onlin,0.011873
open,0.009287
origin,0.007518
ossowska,0.038741
ossowski,0.037216
ourselv,0.021911
outbreak,0.018460
outcom,0.013869
oxford,0.012746
p,0.010356
pa,0.021141
pandora,0.030934
pantheon,0.023805
paradigm,0.032875
partial,0.024959
particip,0.010702
particl,0.014582
particular,0.008491
penguin,0.040805
peopl,0.037398
perceiv,0.013643
permiss,0.018859
perseu,0.026009
person,0.017754
personhood,0.028684
perspect,0.036419
peter,0.012370
phenomena,0.012953
philosoph,0.023815
philosophi,0.023100
physic,0.008877
pictur,0.015187
pinch,0.054810
place,0.007672
poetic,0.021824
polici,0.050197
policymak,0.022208
polit,0.025543
pollut,0.018249
popul,0.019304
porter,0.022895
pose,0.016524
positivist,0.023063
postman,0.030472
power,0.007961
powerknowledg,0.037216
pp,0.025365
practic,0.033292
practition,0.032653
predict,0.011382
premodern,0.023149
press,0.082403
pressur,0.011710
primari,0.010268
princeton,0.068700
privileg,0.017754
probabl,0.011162
problem,0.008319
process,0.022062
product,0.024323
program,0.009785
programm,0.029905
propos,0.009598
prove,0.012006
proven,0.016431
provid,0.013804
public,0.062677
publish,0.017269
pun,0.027633
purpos,0.010119
pursuit,0.017539
qualit,0.018037
quantit,0.014453
question,0.019538
radioact,0.038182
rang,0.009338
ration,0.013406
reader,0.050471
realiti,0.013420
realiz,0.013795
realm,0.015585
reason,0.008854
recent,0.017086
recept,0.019526
reconnect,0.028306
refer,0.004933
regul,0.011680
reiner,0.030934
reinvent,0.024692
relat,0.013304
relationship,0.009417
religion,0.012565
represent,0.012446
reprocess,0.029795
request,0.014995
requir,0.007766
research,0.056028
resist,0.012784
respect,0.009382
respons,0.017114
restivo,0.068822
restrict,0.034304
rethink,0.023512
retriev,0.014657
return,0.009985
review,0.022109
revolut,0.024444
rheinberg,0.036077
rheingold,0.033195
rhetor,0.038206
risk,0.072838
robert,0.010848
role,0.043214
routledg,0.087907
s,0.013994
sage,0.019448
sal,0.057771
scale,0.022333
scan,0.019699
scholar,0.012529
school,0.010037
scienc,0.522771
scientif,0.138994
scientist,0.067691
scientometr,0.032458
scope,0.014504
scrutin,0.026291
se,0.020515
season,0.015943
seek,0.011070
selfconsci,0.024692
selfinterest,0.022304
sellafield,0.036611
semiot,0.048984
separ,0.018027
shape,0.011759
sharon,0.027331
sheep,0.039759
sheepfarm,0.071200
sheila,0.029550
shift,0.011956
showcas,0.026009
simian,0.032691
similar,0.008199
singl,0.009189
situat,0.031715
siu,0.036611
smart,0.021571
social,0.069879
societi,0.111837
sociointellectu,0.038741
sociolog,0.130154
sociologist,0.019487
sokal,0.031269
someth,0.013201
soon,0.012591
sort,0.014402
sound,0.014457
sourc,0.008637
specif,0.008215
spirit,0.015514
spongiform,0.030472
st,0.044275
stanislaw,0.029096
star,0.015227
start,0.052150
state,0.012015
statist,0.011265
stay,0.015029
step,0.023286
steve,0.042208
strau,0.027556
strong,0.019954
structur,0.016045
student,0.011687
studi,0.188669
subject,0.034939
success,0.009105
suddenli,0.020371
suffer,0.012532
sullivan,0.026781
survey,0.026397
swan,0.024692
synthes,0.017844
technicalnatur,0.038741
technolog,0.178199
tendenc,0.030837
teresa,0.028216
term,0.006488
text,0.011844
themselv,0.020370
theodor,0.018792
theoret,0.011258
theori,0.024145
thesi,0.016992
thi,0.010271
thing,0.043702
thoma,0.024565
thorburn,0.035168
thousand,0.012109
threaten,0.015612
thu,0.008026
time,0.006075
topic,0.010894
tradit,0.008646
transcend,0.038846
traweek,0.037915
treat,0.012023
trench,0.023178
trevor,0.053059
tri,0.021615
true,0.011117
trust,0.015010
truth,0.043827
turn,0.018985
twothird,0.018814
type,0.008255
typic,0.009211
ua,0.026529
uk,0.025171
ulrik,0.032458
uncertainti,0.015979
underpin,0.020023
unifi,0.014372
unit,0.006925
univers,0.064004
unrest,0.019043
usa,0.030659
use,0.033663
valu,0.026401
valuabl,0.016119
variou,0.054717
veri,0.007841
view,0.008800
vinck,0.038741
visual,0.014487
voic,0.016365
vol,0.014757
volcan,0.019699
volcano,0.020308
volcanolog,0.167745
volcanologist,0.098074
w,0.011521
wa,0.045039
war,0.019508
warm,0.017547
washington,0.015117
wave,0.027694
way,0.007341
weingart,0.035168
western,0.010328
wieb,0.031825
wiki,0.022950
william,0.011002
women,0.026135
woolgar,0.034411
work,0.020194
world,0.006961
wyer,0.036077
wynn,0.030328
york,0.074028
