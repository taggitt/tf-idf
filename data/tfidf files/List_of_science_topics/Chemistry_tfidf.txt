ab,0.005941
abil,0.009788
abl,0.005990
absolut,0.004022
absorb,0.004740
abstract,0.004172
academ,0.003551
accept,0.005954
accompani,0.013753
accord,0.011856
accordingli,0.005337
acid,0.062112
acidbas,0.042348
act,0.002710
activ,0.011831
actual,0.003034
ad,0.003026
add,0.004521
addit,0.007197
advanc,0.005857
agent,0.008086
aggreg,0.015044
agrochemistri,0.009053
air,0.018214
al,0.004067
alan,0.005027
albrn,0.008546
alchem,0.008373
alchemi,0.050213
alchemist,0.022939
alcohol,0.005444
alessandro,0.007243
alkali,0.007384
alkindi,0.007870
alkm,0.033925
allegor,0.007953
allow,0.002358
alloy,0.006598
alpha,0.010978
alrayhn,0.010455
altern,0.009141
altusi,0.008657
amass,0.007490
amen,0.006678
american,0.002686
analog,0.004237
analogu,0.006481
analysi,0.011434
analyt,0.012970
analyz,0.007972
ancient,0.010307
andrew,0.009632
angular,0.006395
ani,0.006269
anion,0.023065
anoth,0.036493
antoin,0.006505
anyth,0.004686
apart,0.004345
appli,0.007605
applic,0.014160
appliedindustri,0.011308
approach,0.005458
approxim,0.003231
aqueou,0.013964
arab,0.008759
arabian,0.006592
archiv,0.004364
arduou,0.008874
area,0.004710
argon,0.007870
aris,0.007737
aristotl,0.004888
armstrong,0.007317
arrang,0.007732
arrheniu,0.034744
art,0.021767
ascrib,0.006263
associ,0.004633
astrochemistri,0.016699
astrolog,0.006860
astronomi,0.005324
atkin,0.070216
atmospher,0.004645
atom,0.277821
atomsmolecul,0.011308
attain,0.004800
attent,0.003866
attract,0.015262
attribut,0.003686
avail,0.002898
avicenna,0.006692
avogadro,0.008469
b,0.006282
babylonian,0.006446
bacon,0.006357
balanc,0.007506
barrier,0.004824
base,0.024795
basi,0.005907
basic,0.014383
bc,0.008460
beam,0.006464
bear,0.004437
becaus,0.012914
becom,0.011444
becquerel,0.008573
befor,0.002442
began,0.005710
begin,0.002788
behavior,0.006708
belong,0.004040
berzeliu,0.008495
better,0.003224
bibliographi,0.004158
billion,0.004061
biochemistri,0.028417
biolog,0.020347
black,0.011576
bodi,0.020085
bohr,0.007069
boltzmann,0.007223
bombard,0.006346
bond,0.130689
book,0.013587
borrow,0.004745
boseeinstein,0.007870
boston,0.005271
bound,0.017113
boundari,0.003991
boyl,0.028426
branch,0.003040
breadth,0.006791
break,0.007882
bridg,0.004724
britannica,0.005815
british,0.003192
brnstedlowri,0.031061
broaden,0.006097
broken,0.004767
bulk,0.015206
burrow,0.007632
byzantin,0.011765
ca,0.005259
calculu,0.005344
cambridg,0.007585
came,0.003131
capabl,0.003771
carbon,0.032672
carbonbas,0.008024
carl,0.004781
case,0.011972
cast,0.004864
catherin,0.006821
cation,0.029183
caveat,0.007903
cavendish,0.007717
cell,0.004118
central,0.005369
centuri,0.022132
certain,0.010484
chang,0.030227
chapman,0.006899
character,0.014529
characterist,0.009902
charg,0.055435
chemi,0.008908
chemic,0.419355
chemist,0.082556
chemistri,0.465911
chemoinformat,0.008841
chlorid,0.013628
chlorin,0.006915
christoph,0.005019
chromatographi,0.007854
chymist,0.009611
chymistri,0.020340
civil,0.003285
cl,0.022362
claim,0.002907
class,0.003176
classic,0.015820
classif,0.012677
classifi,0.015496
clayden,0.010686
close,0.007687
cloud,0.010955
collabor,0.004399
collect,0.005587
column,0.005441
combin,0.010924
combust,0.006618
come,0.010957
common,0.011579
commonli,0.012723
compar,0.002832
comparison,0.008032
complet,0.002645
complex,0.002802
complic,0.004340
compon,0.009895
compos,0.032704
composit,0.051619
compound,0.094951
comput,0.009521
concentr,0.011244
concept,0.029402
conceptu,0.004610
concern,0.010756
condens,0.022168
condit,0.010720
configur,0.015874
conform,0.005286
conquest,0.005160
consequ,0.003257
conserv,0.007604
consid,0.006701
consider,0.006437
consist,0.010254
constant,0.007616
constitu,0.004485
constitut,0.003024
constrain,0.005569
contact,0.004019
contain,0.002747
context,0.006738
continu,0.009283
contrast,0.003235
conveni,0.010228
coordin,0.004060
core,0.008135
cornel,0.006057
cosmochemistri,0.008304
coulson,0.008841
coupl,0.004432
cours,0.006676
coval,0.027197
creat,0.011714
crossdisciplinari,0.007903
crucial,0.013988
crust,0.006379
crystal,0.021494
crystallin,0.007106
cultur,0.002880
curi,0.007632
current,0.004904
dalton,0.006899
danish,0.006057
date,0.003201
davi,0.011108
day,0.002890
deal,0.006171
decay,0.005266
declar,0.003699
decreas,0.007544
defin,0.021029
definit,0.039373
delta,0.010972
democritu,0.007264
denot,0.004673
dens,0.010904
densiti,0.004622
depend,0.005054
depict,0.009555
der,0.005012
deriv,0.014562
describ,0.009430
design,0.002813
detect,0.004206
determin,0.010640
develop,0.022987
devis,0.010509
diagram,0.005269
diamond,0.005815
diatom,0.007339
dictionari,0.004643
did,0.002843
didnt,0.006294
differ,0.032489
differenti,0.003878
diffus,0.005232
dioxid,0.005628
dipoledipol,0.010455
direct,0.002543
disappear,0.009740
disciplin,0.017647
discov,0.030358
discoveri,0.014350
discret,0.018070
discuss,0.003125
disembodi,0.008746
displaystyl,0.008775
dissoci,0.021209
dissolv,0.019029
distant,0.005324
distil,0.006860
distinct,0.008850
distinguish,0.003400
distribut,0.003160
divid,0.006074
dmitri,0.007454
donat,0.017212
donor,0.006006
dover,0.006401
draw,0.007853
duet,0.010353
duma,0.008349
dure,0.006614
dutchman,0.008808
dye,0.006560
dynam,0.007182
e,0.018623
earli,0.018452
earlier,0.003407
earth,0.007240
easili,0.007901
econom,0.002523
ed,0.016141
edit,0.003609
educ,0.005875
edward,0.004195
eekt,0.010817
effect,0.002240
efficaci,0.006336
egypt,0.004870
egyptian,0.020790
electr,0.022878
electrochem,0.007350
electrochemistri,0.015645
electromagnet,0.005170
electron,0.169794
electroneg,0.016159
electroposit,0.009670
electrostat,0.014121
element,0.079188
elementari,0.010404
elixir,0.008349
elucid,0.012836
embodi,0.005331
emerg,0.008866
emit,0.005772
empir,0.016089
encompass,0.008634
encount,0.004542
encyclopdia,0.005981
end,0.007564
endergon,0.010353
endotherm,0.008373
energet,0.006248
energi,0.125905
engag,0.003651
engin,0.003323
english,0.003211
entiti,0.015651
entropi,0.012516
environ,0.003056
environment,0.003707
envis,0.006299
envisag,0.014813
epicuru,0.007731
equal,0.012561
equat,0.023012
equilibrium,0.028917
ernest,0.005933
ernst,0.011181
esr,0.009259
essenti,0.003205
essex,0.008469
establish,0.002461
et,0.003964
etern,0.005830
etymolog,0.004909
europ,0.006253
event,0.003001
everyth,0.004592
evid,0.003144
evolv,0.003785
exact,0.004399
exactli,0.004517
exalt,0.007791
exampl,0.020653
exchang,0.006519
excit,0.011243
exclud,0.004338
exergon,0.009933
exhibit,0.004353
exist,0.013759
exot,0.006142
exotherm,0.008495
expand,0.003262
experi,0.008507
experiment,0.023111
explain,0.015522
explor,0.003513
exponenti,0.005551
exposur,0.005125
express,0.002987
extens,0.003114
extern,0.001803
extract,0.004439
f,0.003396
factor,0.002923
fall,0.003147
familiar,0.024396
famou,0.003809
father,0.008108
feasibl,0.011206
femtochemistri,0.009015
fermion,0.007903
ferromagnet,0.008239
field,0.010073
final,0.005965
finger,0.006698
fix,0.003660
flavor,0.007043
flow,0.003631
focu,0.003383
follow,0.005917
foot,0.005357
forc,0.014829
form,0.033928
format,0.010089
formul,0.003948
formula,0.004306
franci,0.004722
frankland,0.009501
free,0.005633
french,0.006637
friedrich,0.005043
function,0.005049
fundament,0.015789
g,0.010007
ga,0.020743
gain,0.021000
galaxi,0.006138
galileo,0.006070
gamma,0.006088
gareth,0.009015
gase,0.021991
gaseou,0.006932
gasolin,0.007134
geber,0.009670
gener,0.007087
genet,0.004188
geochemistri,0.007515
geolog,0.004712
geometri,0.005029
georg,0.007107
german,0.003482
gibb,0.013897
gilbert,0.005897
given,0.004946
glaser,0.009304
glasswar,0.027276
gleq,0.011308
global,0.003274
glossari,0.006010
goal,0.003391
goe,0.004437
gold,0.012864
govern,0.002344
graduat,0.009241
gram,0.006891
granit,0.007674
great,0.002779
greater,0.003105
greec,0.004661
greek,0.017459
greekegyptian,0.021132
green,0.004240
greev,0.010686
ground,0.003588
group,0.008897
growth,0.003121
guid,0.003486
gwen,0.009862
ha,0.023019
hall,0.004678
handi,0.008546
harlow,0.008042
hayyn,0.008841
heat,0.026089
held,0.002915
helium,0.013197
hellenist,0.006268
helmont,0.009053
henri,0.007949
hi,0.011055
high,0.004993
higher,0.008794
histochemistri,0.010455
histori,0.010269
ho,0.006325
hold,0.008910
holman,0.010170
hook,0.006631
housecroft,0.011124
howev,0.014009
hs,0.007838
humphri,0.007854
hydrogen,0.051076
hydronium,0.031698
hydroxid,0.016159
hypothesi,0.004295
ibn,0.005368
idea,0.002767
identif,0.004857
identifi,0.014904
immunochemistri,0.009015
import,0.014722
importantli,0.005650
improv,0.003034
includ,0.020541
incompat,0.006283
incorpor,0.006772
increas,0.004742
inde,0.004192
independ,0.007598
index,0.007834
indian,0.004081
indic,0.006067
individu,0.005087
indivis,0.013998
industri,0.005629
inert,0.013947
infinitesim,0.006523
influenc,0.002716
influenti,0.003965
initi,0.008037
inorgan,0.051887
instead,0.008664
institut,0.002609
integr,0.002975
interact,0.027690
interconvers,0.017816
interfac,0.005368
intermedi,0.004846
intermolecular,0.016048
intern,0.012965
introduc,0.002892
introduct,0.006538
introductori,0.005878
invari,0.027262
invent,0.004012
investig,0.003455
invok,0.005921
involv,0.016733
ion,0.106947
ionic,0.040931
ioniz,0.013047
ir,0.006891
iron,0.004416
isbn,0.052471
isol,0.012010
isotop,0.005973
issu,0.002646
itali,0.004437
italian,0.004626
iupac,0.014068
j,0.024774
jame,0.003477
jbir,0.008841
jeanbaptist,0.006685
john,0.008414
jonathan,0.005765
joseph,0.008126
julio,0.008137
juliu,0.006273
just,0.002875
justu,0.008177
k,0.003738
ka,0.022545
kean,0.009259
kilogram,0.006965
kimi,0.011308
kind,0.026276
kinet,0.011162
knowledg,0.003101
known,0.024723
krypton,0.009215
l,0.007258
laboratori,0.020919
lack,0.003143
larg,0.008690
later,0.004903
latin,0.003874
lattic,0.006147
lavoisi,0.007144
law,0.012417
lead,0.004919
learn,0.003463
left,0.003286
level,0.009860
levi,0.005793
lewi,0.015523
liebig,0.008304
life,0.002650
light,0.010196
like,0.015414
limit,0.002517
line,0.002950
linear,0.004386
link,0.003585
linu,0.015030
lipid,0.006493
liquid,0.022095
list,0.009169
lithium,0.007183
littl,0.006310
live,0.005331
local,0.002792
logarithm,0.006671
london,0.003471
lone,0.014139
longman,0.007007
lord,0.004879
lose,0.017342
lost,0.003582
lothar,0.009092
low,0.003159
lower,0.006092
lucretiu,0.007854
m,0.005896
magic,0.005747
magnet,0.004839
main,0.005219
maintain,0.002874
major,0.002187
make,0.008385
manchest,0.006469
mani,0.022358
mantl,0.006973
march,0.003418
margin,0.004517
mari,0.004762
marin,0.004719
mass,0.037186
materi,0.023109
mathemat,0.009721
matter,0.058916
mayow,0.010455
mcweeni,0.010566
mean,0.009080
meant,0.008456
measur,0.016200
mechan,0.045662
mechanist,0.006875
mechanochemistri,0.009132
medicin,0.011790
mediev,0.004396
meet,0.003335
mendeleev,0.008396
metal,0.034323
metallurgi,0.013425
method,0.020424
meyer,0.006956
microscop,0.005321
microwav,0.006769
millennia,0.006006
miner,0.009127
mingl,0.008715
mix,0.011895
mixtur,0.030924
model,0.010271
modern,0.019780
modifi,0.004022
mol,0.008373
molar,0.007406
moldm,0.011308
mole,0.029627
molecul,0.142437
molecular,0.077555
moleculesatom,0.011308
moor,0.005563
moseley,0.008600
movement,0.002842
multipl,0.006341
multipol,0.009501
muslim,0.013830
mystic,0.006079
n,0.003701
na,0.018700
nacl,0.017554
nanotechnolog,0.006685
nation,0.004568
natur,0.022006
natura,0.007430
necessari,0.003285
necessarili,0.004035
need,0.002478
neg,0.025022
neon,0.007791
nervou,0.011288
network,0.006892
neurochem,0.008874
neurochemistri,0.025049
neutral,0.027072
neutron,0.019498
new,0.018413
newland,0.009259
newton,0.005204
nick,0.006915
niel,0.007034
nitrat,0.007153
nitric,0.007936
nitrogen,0.005690
nmr,0.007466
nobl,0.021441
nomenclatur,0.023409
nonmet,0.016437
nonnuclear,0.010353
normal,0.003285
note,0.004753
nuclear,0.021060
nuclei,0.029646
nucleic,0.006769
nucleon,0.016479
nucleu,0.032765
nuclid,0.008282
number,0.045368
numberth,0.011308
object,0.002817
observ,0.005441
occult,0.007490
occur,0.013095
octet,0.008943
oenolog,0.009015
oh,0.007243
old,0.003410
onli,0.013282
opposit,0.003391
orbit,0.020912
order,0.006814
organ,0.042721
organometal,0.015320
origin,0.006853
oscil,0.005965
outer,0.005266
outermost,0.014405
outlin,0.004012
overcom,0.005062
overlap,0.018326
overton,0.008177
overturn,0.006762
oxford,0.038731
oxid,0.103917
oxygen,0.005019
pair,0.026357
panopoli,0.010962
paramagnet,0.008396
paramet,0.004752
parson,0.007646
particl,0.031017
particular,0.015481
particularli,0.005781
pattern,0.003517
paul,0.014332
paula,0.008349
pearson,0.006493
penguin,0.006199
peptid,0.006899
perfect,0.004605
period,0.024493
persian,0.005179
peter,0.007517
petrochemistri,0.008978
ph,0.013059
pharmaceut,0.005615
pharmacolog,0.006435
phase,0.095137
phenomenon,0.004245
philosoph,0.010854
philosophi,0.007019
phlogiston,0.008327
phonon,0.008908
phosphat,0.006373
photochemistri,0.008546
photon,0.012046
physic,0.064736
physicist,0.004879
phytochemistri,0.008908
pierr,0.005306
pile,0.007274
pill,0.007870
pioneer,0.004306
place,0.006993
plasma,0.012080
play,0.002976
po,0.007466
point,0.002391
polyatom,0.035234
polym,0.012389
popul,0.002932
popular,0.005898
portion,0.004142
posit,0.014658
possibl,0.012054
potenti,0.015301
potteri,0.006481
pp,0.003853
practic,0.010116
practis,0.006218
practition,0.004961
predict,0.006917
prefer,0.007239
prepar,0.003833
present,0.007090
press,0.027820
pressur,0.010674
price,0.003499
priestley,0.007806
primari,0.003120
primo,0.009449
princip,0.003692
principl,0.017024
prior,0.003570
probabl,0.003391
probe,0.005878
problem,0.005056
process,0.011173
produc,0.007378
product,0.007390
profession,0.003830
professor,0.004224
profit,0.004084
program,0.005946
prolifer,0.005819
proper,0.004240
properli,0.004970
properti,0.042930
propos,0.017498
propound,0.021579
protein,0.004802
proton,0.054998
prout,0.009933
prove,0.003648
provid,0.002097
public,0.007141
pure,0.033639
pw,0.043885
pyramid,0.006352
quanta,0.007919
quantiz,0.007007
quantum,0.038140
quartz,0.007936
quest,0.005953
question,0.002968
r,0.006326
radiat,0.009467
radic,0.008666
radioact,0.017402
radiochemistri,0.008746
radiu,0.006253
radon,0.007971
ramsay,0.008349
rang,0.002837
rate,0.002929
rayleigh,0.007953
raymond,0.018484
rd,0.004232
reach,0.003046
react,0.010475
reactant,0.022186
reaction,0.226801
reactiv,0.011295
read,0.005207
rearrang,0.032119
reason,0.002690
receiv,0.005728
recent,0.007787
redox,0.015106
reduc,0.011360
reduct,0.030194
reductionoxid,0.010962
refer,0.008994
refract,0.006852
refut,0.005921
regard,0.002876
region,0.002684
registri,0.007478
reject,0.003695
rel,0.008063
relat,0.010106
releas,0.003746
relev,0.003921
reli,0.003612
remain,0.002479
remark,0.004598
remnant,0.005994
remot,0.005075
remov,0.003461
renaiss,0.004957
reorgan,0.006268
repeat,0.004327
report,0.002881
repres,0.004918
repuls,0.013236
requir,0.004719
rerum,0.008420
research,0.004864
reshap,0.006965
resid,0.003971
resolv,0.004354
respons,0.002600
rest,0.014049
result,0.018794
retain,0.003942
retrospect,0.006598
revers,0.004127
right,0.002690
rigor,0.005047
rise,0.003016
robert,0.009888
role,0.002626
roman,0.004021
room,0.009725
root,0.003786
rosenth,0.008746
rotat,0.009925
rourk,0.010962
row,0.005797
rule,0.019155
rutherford,0.007361
ry,0.009399
s,0.004252
said,0.033882
sale,0.004367
salt,0.025681
sam,0.006657
sampl,0.008934
scale,0.006786
scatter,0.005536
sceptic,0.007183
scheel,0.008908
scheme,0.004512
scienc,0.032777
scientif,0.021117
scientist,0.023996
scottish,0.005754
se,0.006233
second,0.002460
seen,0.002941
separ,0.002738
sequenc,0.004161
seri,0.005973
servic,0.002920
set,0.020900
sever,0.023925
share,0.005842
sharp,0.005066
shell,0.025229
shriver,0.010455
silic,0.007515
silica,0.008006
similar,0.004982
similarli,0.007905
simpl,0.006630
simplest,0.005321
sinc,0.008345
singl,0.005584
sir,0.004709
situat,0.003212
size,0.003249
skeleton,0.006379
slow,0.004307
small,0.002553
smaller,0.003547
smallest,0.021720
smart,0.006554
smith,0.004420
societi,0.005228
sodium,0.019056
softwar,0.004420
solid,0.058292
solidst,0.007442
solut,0.017847
solv,0.003839
sometim,0.010864
sonochemistri,0.008978
soon,0.003825
space,0.009650
speci,0.007674
special,0.010437
specif,0.004992
spectra,0.013598
spectral,0.006406
spectromet,0.008520
spectroscopi,0.024954
speech,0.004495
speed,0.008533
spirit,0.009428
split,0.004288
spoon,0.008874
st,0.003363
stabil,0.003891
stabl,0.008211
stahl,0.018031
standard,0.011240
star,0.004626
start,0.005282
state,0.034685
static,0.005337
statist,0.003422
step,0.007075
stephenson,0.008349
stepwis,0.008908
stereotyp,0.006852
stick,0.006273
strength,0.004231
strong,0.003031
structur,0.058504
stuart,0.005786
student,0.007102
studi,0.063942
stwertka,0.011308
subatom,0.006631
subdisciplin,0.034120
subject,0.007962
substanc,0.342141
succeed,0.004265
success,0.002766
suffici,0.003857
suffix,0.007125
sugar,0.010167
sulfat,0.007466
sulfid,0.007079
sum,0.004161
supercrit,0.009611
supramolecular,0.008197
surfac,0.003777
surmount,0.009015
surround,0.022924
svant,0.008978
swan,0.007502
symbol,0.011363
synthesi,0.004658
synthet,0.005174
systemat,0.011958
t,0.014520
tabl,0.044168
taken,0.003054
tale,0.005998
temperatur,0.033742
tend,0.006852
term,0.015771
tetra,0.009351
text,0.010797
th,0.023923
theoret,0.027368
theori,0.053801
therebi,0.004318
therefor,0.002835
thermal,0.005349
thermochemistri,0.008327
thermodynam,0.021420
thi,0.031211
thing,0.006639
thomson,0.006481
threedimension,0.005730
thu,0.026828
time,0.009229
togeth,0.023016
tool,0.006681
topic,0.003310
total,0.002911
trace,0.004000
tradit,0.007881
transfer,0.028562
transform,0.030737
transit,0.003632
translat,0.007183
transmitt,0.007163
transmut,0.028772
trend,0.003985
triatom,0.010258
tripl,0.005747
trivial,0.006228
true,0.010134
turn,0.011537
twentieth,0.004922
twodimension,0.006719
type,0.015050
typic,0.005598
ultim,0.003608
ultrasound,0.007592
ultraviolet,0.006517
unbound,0.007295
unchang,0.011126
uncharg,0.009351
undergo,0.019349
undergradu,0.005557
undergraduatelevel,0.010817
underpin,0.006084
understand,0.011328
understood,0.003915
unequ,0.006481
unintelligbl,0.010686
union,0.006200
uniqu,0.007115
unit,0.018939
univers,0.025930
unless,0.004675
unlik,0.006949
unpair,0.008304
unsolv,0.006598
urea,0.007527
use,0.034095
usual,0.017575
vacuum,0.005444
valenc,0.065377
valu,0.005348
van,0.009127
vari,0.006151
variabl,0.003813
variou,0.009500
veri,0.007148
version,0.003649
vesselsoften,0.011308
vibrat,0.012177
view,0.002674
visual,0.004402
viz,0.007731
voet,0.021132
volta,0.007822
voltaic,0.008546
volum,0.007147
von,0.004231
vsepr,0.010007
w,0.003500
wa,0.023949
waal,0.007384
war,0.002963
warren,0.006092
water,0.022528
way,0.015615
weaker,0.005941
weight,0.016821
weller,0.009933
wellsepar,0.010962
whenev,0.005871
wherea,0.003620
whler,0.008444
wide,0.002687
wiley,0.005105
wilhelm,0.005252
william,0.006686
wilson,0.005211
woodwardhoffmann,0.010455
word,0.017376
work,0.016363
world,0.008461
worldwid,0.008465
wother,0.010962
x,0.003694
xenon,0.008137
year,0.008422
york,0.003213
z,0.005090
zero,0.004387
zosimo,0.021924
