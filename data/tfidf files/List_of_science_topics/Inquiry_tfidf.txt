abduc,0.032662
abduct,0.193864
abov,0.005027
abstract,0.020729
ac,0.021387
accord,0.007853
account,0.013874
accumul,0.015065
achiev,0.004939
acquisit,0.016980
act,0.008975
action,0.004824
activ,0.007836
addit,0.011917
address,0.005913
admit,0.016561
adopt,0.005418
agent,0.020085
aim,0.016821
air,0.054287
algebra,0.008432
allow,0.003904
alpha,0.009089
alreadi,0.011441
alter,0.007043
alway,0.010560
amend,0.008280
analog,0.077180
analogycatalog,0.020657
analysi,0.023667
analyt,0.035794
analyz,0.006600
angluin,0.020657
ani,0.024224
ann,0.009196
anoth,0.030213
anyth,0.007760
appear,0.022740
appli,0.029384
applic,0.028136
appreci,0.008713
approach,0.009038
appropri,0.006294
approxim,0.032107
apt,0.012778
arbit,0.013751
arbitrari,0.008479
argument,0.019791
aristotl,0.072852
arriv,0.018963
art,0.024028
arthur,0.008010
aspect,0.010321
assembl,0.006129
associ,0.015343
assum,0.016319
attent,0.006403
augment,0.010111
august,0.005966
automat,0.016032
avail,0.004799
awbrey,0.041314
b,0.130026
base,0.044479
basi,0.004890
basic,0.004763
bc,0.007004
bear,0.007348
becaus,0.010692
becom,0.003790
befor,0.052568
begin,0.009233
begun,0.007528
belief,0.006338
beneath,0.010233
bibliographi,0.013772
bit,0.009629
blackwel,0.009898
bonus,0.013751
book,0.008999
bool,0.013086
boolean,0.011927
borrow,0.007858
bow,0.011632
boydston,0.020657
brace,0.012661
branch,0.005035
bring,0.005886
broadli,0.007925
brought,0.005836
buffalo,0.012011
burk,0.011910
c,0.117813
ca,0.008708
calculu,0.026550
cambridg,0.025121
capac,0.006573
carbondal,0.016220
carri,0.020448
case,0.099123
catalog,0.010853
catalogu,0.010812
categori,0.006050
caus,0.004232
certain,0.008680
certainti,0.010009
cf,0.010693
chapt,0.018725
charact,0.013105
character,0.006014
charl,0.024293
check,0.008221
cheryl,0.017694
chosen,0.021738
churchman,0.014585
circl,0.015282
circumst,0.029139
citat,0.009444
cite,0.006632
clarifi,0.009564
class,0.010519
classic,0.031436
classicu,0.016571
clear,0.011804
clearli,0.007130
close,0.012729
cloud,0.081635
cold,0.015119
collect,0.027756
combin,0.013566
come,0.018143
common,0.003834
commonli,0.005266
commun,0.012441
compani,0.005007
compar,0.004690
complet,0.008760
complex,0.009281
compon,0.005461
comprehend,0.011234
comput,0.005255
concept,0.008852
conceptu,0.007633
concern,0.013358
conclus,0.028267
concret,0.008688
condit,0.008875
conduct,0.005435
conjunct,0.008863
connect,0.005221
consequ,0.021572
consequenti,0.012528
consid,0.014795
consider,0.010660
consist,0.004245
constant,0.006305
constantli,0.008841
constitu,0.007427
constitut,0.005007
constraint,0.039374
consumm,0.014024
contain,0.027300
contempl,0.021896
content,0.018774
context,0.022317
continu,0.007685
contrast,0.010715
contribut,0.013965
control,0.008427
conveni,0.008468
convert,0.006465
cool,0.071078
cooler,0.012189
correct,0.006386
count,0.007233
coupl,0.007339
cours,0.011055
cover,0.005221
cp,0.024233
crash,0.009433
creat,0.003879
critic,0.004665
cs,0.019381
curios,0.011333
current,0.056846
curv,0.007803
cycl,0.019505
cyclic,0.009851
d,0.046211
dame,0.024806
dana,0.012953
dark,0.065614
data,0.010123
david,0.005634
day,0.019144
dc,0.008782
deduc,0.028416
deduct,0.165183
deed,0.011002
deepli,0.008955
defeat,0.007504
defer,0.010958
defin,0.004352
degre,0.005194
delaney,0.018152
deliber,0.008490
demand,0.005662
demonstr,0.011402
depend,0.004184
deriv,0.014467
describ,0.015615
descript,0.005797
detach,0.010321
determin,0.008809
develop,0.006344
dewey,0.152978
diagnosi,0.009623
diagram,0.008725
differ,0.010087
difficulti,0.006850
direct,0.008423
disciplin,0.005844
discours,0.008684
discov,0.005585
discoveri,0.005940
discrep,0.010948
discuss,0.005175
displaystyl,0.065388
distinct,0.019539
distinguish,0.016893
divid,0.005029
doe,0.008583
doubli,0.014750
doubt,0.024557
draw,0.006501
driven,0.007383
dualiti,0.032653
e,0.025698
earli,0.007638
eas,0.008791
ed,0.026728
edwardsvil,0.019492
effect,0.022257
element,0.019426
empir,0.010656
employ,0.005065
end,0.008350
entri,0.007386
entropi,0.010363
environ,0.005061
epast,0.082628
epistemolog,0.009075
eposs,0.123942
epr,0.057340
eprint,0.015647
equal,0.005200
equival,0.006150
ernest,0.009824
error,0.014522
essenc,0.008625
essenti,0.005308
establish,0.004075
evalu,0.006616
event,0.064601
everi,0.004778
everyday,0.008157
everyth,0.007603
evid,0.015621
exact,0.021854
examin,0.005976
exampl,0.047878
exceed,0.008019
exclud,0.007183
exercis,0.006560
exhibit,0.014416
expand,0.010803
expans,0.012885
expect,0.010493
experi,0.103305
explain,0.005140
explan,0.033609
explic,0.025057
exploit,0.007108
explor,0.005818
exposit,0.010465
express,0.029685
extend,0.010073
extent,0.006283
extract,0.007350
extrem,0.021910
f,0.028123
faci,0.014694
fact,0.092118
fail,0.005741
fair,0.007733
fairli,0.017298
fallibl,0.013257
far,0.015533
fashion,0.016073
featur,0.015492
feel,0.007138
fellow,0.008633
field,0.004170
figur,0.056551
final,0.014817
finit,0.007897
fit,0.006952
fix,0.006060
follow,0.013065
form,0.049937
formal,0.005109
foulweath,0.020657
foundat,0.005324
framework,0.011700
fresh,0.009171
friend,0.008110
fundament,0.005228
gain,0.004967
gener,0.008802
gentlest,0.019492
given,0.024572
goe,0.014696
govern,0.003882
graph,0.018169
graphic,0.008782
guess,0.022903
guid,0.005773
gulf,0.009237
ha,0.027227
haack,0.015915
hang,0.010937
hanson,0.014242
happen,0.006453
hartshorn,0.013903
harvard,0.007977
haussler,0.019076
head,0.005271
heath,0.013442
heinemann,0.013608
help,0.004470
hendrick,0.015406
henri,0.006581
hero,0.009684
heurist,0.010447
hi,0.032950
hidden,0.008991
high,0.004134
hint,0.022230
hit,0.008453
hold,0.004918
holt,0.012465
howev,0.009941
hugh,0.009581
human,0.003933
hypothes,0.024358
hypothesi,0.028449
hypothet,0.009181
idea,0.009165
ideal,0.006643
il,0.011385
illinoi,0.009995
illustr,0.013926
imagin,0.008163
immedi,0.012004
immin,0.023473
impel,0.014928
impli,0.006348
implic,0.028457
incept,0.010627
inclus,0.008411
incorpor,0.005607
increas,0.003926
indetermin,0.012245
indiffer,0.011283
induc,0.024256
induct,0.171805
inescap,0.015733
infer,0.133400
inform,0.020297
inher,0.007825
initi,0.008873
inquir,0.012028
inquiri,0.504956
insofar,0.011308
instanc,0.011059
instruct,0.015374
instrument,0.006287
integr,0.004927
intellectu,0.007004
interact,0.010189
intermedi,0.024074
intern,0.003578
interpret,0.005447
interv,0.008602
intrins,0.008645
intro,0.015259
introduc,0.004789
invok,0.009804
involv,0.011875
isbn,0.004826
isol,0.013258
item,0.007490
j,0.005127
jame,0.005758
jo,0.009715
john,0.037154
jon,0.011398
judg,0.022620
judgment,0.034613
just,0.090461
kant,0.009877
kathleen,0.013059
kaufmann,0.013608
kind,0.027194
knowledg,0.159197
known,0.013646
kpre,0.185914
l,0.012019
larger,0.016165
later,0.004059
lattic,0.010179
law,0.004112
lead,0.020365
learn,0.057346
leav,0.005781
legend,0.009617
leonard,0.010518
let,0.076488
letter,0.006738
level,0.012246
lexington,0.013643
librari,0.006413
life,0.004388
light,0.005627
like,0.010938
likelihood,0.009466
limit,0.012506
list,0.007591
littl,0.005224
live,0.004413
locu,0.011092
loeb,0.014109
logic,0.138742
london,0.005747
look,0.033817
lunul,0.019492
ma,0.025866
machin,0.006590
macroinquiri,0.020657
major,0.010864
make,0.017355
man,0.006039
mani,0.015426
manner,0.013175
mass,0.005597
master,0.007284
match,0.029036
mateo,0.015822
materi,0.004783
matter,0.005134
mayb,0.012362
mean,0.022555
meaning,0.008871
mechan,0.005040
mental,0.007386
microinquiri,0.020657
middl,0.056278
mild,0.010536
mind,0.019311
minor,0.018950
misak,0.019492
mit,0.016587
mix,0.006565
mode,0.076845
model,0.021259
modular,0.011424
moment,0.023297
moral,0.006828
morgan,0.019381
morphism,0.014152
nagel,0.013788
natur,0.007288
near,0.005612
nearer,0.038263
necessari,0.010879
necessarili,0.006681
need,0.028723
new,0.015244
newfound,0.013228
nondemonstr,0.041314
norm,0.007917
norwood,0.015915
note,0.023612
notic,0.015494
notion,0.012915
notr,0.026696
novel,0.007554
nowher,0.011752
ny,0.029633
o,0.043188
observ,0.040545
obtain,0.005305
obviou,0.017244
obvious,0.010905
occas,0.016980
occupi,0.006842
occur,0.004336
occurr,0.017657
old,0.005647
onc,0.010200
ongo,0.022406
onli,0.031420
oo,0.079578
openli,0.010338
oper,0.012918
option,0.007122
order,0.007523
origin,0.007565
ourselv,0.022049
outsid,0.005207
oxford,0.019240
paper,0.011264
paradigm,0.008270
parallel,0.014145
particular,0.004272
past,0.027074
pattern,0.017471
paul,0.005933
pedestrian,0.014383
peirc,0.165180
peircean,0.019076
perfect,0.007626
perform,0.004798
perhap,0.006345
peripatet,0.014066
person,0.004466
phase,0.078767
phenomena,0.006517
phenomenon,0.007029
philosophi,0.011622
phrase,0.008224
pictur,0.007641
pitt,0.012778
place,0.015440
plausibl,0.010202
play,0.014784
point,0.011878
portion,0.006859
posit,0.004045
possibl,0.011976
potenti,0.005067
poulo,0.020657
pp,0.025524
practic,0.020938
pragmat,0.066785
pragmatic,0.017694
pragmatician,0.020657
precipit,0.009000
predic,0.021604
predict,0.011454
prefac,0.011234
premis,0.027545
present,0.043050
preserv,0.006597
press,0.032246
presumpt,0.012342
previous,0.005966
prima,0.013943
primarili,0.005350
primit,0.008661
princip,0.018343
principl,0.009396
prior,0.029559
probabl,0.028082
problem,0.020930
problemat,0.009433
proce,0.009529
proceed,0.014325
process,0.025901
product,0.004079
project,0.004843
prometheu,0.012778
proper,0.007022
properli,0.008230
properti,0.004739
proposit,0.062973
prove,0.006040
provid,0.003472
publish,0.004344
pure,0.006189
purpos,0.056004
puzzlement,0.018725
qualiti,0.011663
question,0.014745
quick,0.009672
quicken,0.013983
quickli,0.006371
r,0.010475
rain,0.198782
raini,0.038937
rainstorm,0.017313
random,0.007466
rare,0.006441
ration,0.006745
raw,0.008097
realiti,0.013504
realiz,0.006941
realm,0.007841
reason,0.120290
recogn,0.015968
reconsid,0.012028
reconstruct,0.023337
record,0.005232
rectilinear,0.030813
reduc,0.018810
reduct,0.021427
refer,0.012411
refin,0.007787
reflect,0.010669
reflex,0.010916
regard,0.004762
regim,0.036167
region,0.004445
regul,0.005876
regular,0.006800
relat,0.020081
relationship,0.004738
relev,0.019478
remain,0.004105
remov,0.005731
render,0.016282
repeatedli,0.008833
repertoir,0.012979
repres,0.004072
reprint,0.019093
requir,0.007815
research,0.004027
resolv,0.007210
resourc,0.010050
respect,0.004720
respond,0.006695
respons,0.004305
rhetor,0.009611
riddl,0.013864
risk,0.006107
robert,0.005458
role,0.013045
rough,0.039377
rule,0.145001
run,0.005455
russel,0.008700
s,0.003520
safe,0.008116
said,0.005100
salient,0.024646
sampl,0.022192
san,0.007883
sander,0.033813
satisfactori,0.011296
scale,0.005618
scan,0.009911
schemat,0.012851
scienc,0.033400
scientif,0.009990
search,0.012707
second,0.004074
seed,0.008807
seen,0.009741
seiz,0.008534
select,0.005397
semiconsci,0.018725
sens,0.015323
sententi,0.043918
sequel,0.012189
sequenc,0.006891
serv,0.009882
set,0.019226
settl,0.007055
shape,0.011833
shelter,0.009678
shock,0.008486
shower,0.013507
shown,0.005986
sign,0.017064
similar,0.008250
simpl,0.016467
simpler,0.009211
sinc,0.013818
singl,0.004623
singular,0.009103
situat,0.085105
skill,0.006655
sky,0.029473
slope,0.009973
smaller,0.005874
socal,0.006970
social,0.004394
solv,0.006358
someth,0.006642
somewhat,0.007323
sourc,0.008691
southern,0.006411
speak,0.006624
special,0.004320
specul,0.007187
spring,0.007792
squar,0.007319
stalnak,0.017144
stand,0.006492
start,0.004373
state,0.009068
statement,0.031711
step,0.052723
stori,0.014006
strategi,0.006435
stream,0.008280
strictli,0.008147
strong,0.005019
studi,0.014604
style,0.014236
subject,0.008789
submit,0.008653
success,0.009163
suggest,0.019346
suitabl,0.007669
sum,0.013782
summar,0.017598
sun,0.007664
suppli,0.005632
support,0.016430
suppos,0.015182
sure,0.009466
surpris,0.027254
susan,0.021126
syllog,0.027081
syllogist,0.061038
symbol,0.012544
syntact,0.024197
systemat,0.006600
t,0.012021
taken,0.005058
talk,0.007434
taught,0.015405
term,0.065289
terminolog,0.016270
territori,0.006046
test,0.045182
text,0.011919
theoret,0.005664
theori,0.036445
therebi,0.007150
thi,0.085273
thing,0.027485
think,0.059912
thought,0.014688
thu,0.004038
time,0.012226
total,0.009640
tran,0.010599
transfer,0.041383
transform,0.005655
transit,0.006014
transport,0.005922
treat,0.006049
treatment,0.019065
tredennick,0.017911
tri,0.005437
true,0.027968
truth,0.029401
twostep,0.015564
type,0.041536
typic,0.009269
u,0.016122
uk,0.025329
unavoid,0.012423
uncertainti,0.016079
underli,0.006560
understood,0.006483
unifi,0.007231
unit,0.003484
univers,0.021468
uplook,0.020657
usag,0.014492
use,0.036697
usual,0.008314
util,0.006250
v,0.006160
valid,0.019791
valu,0.004427
varieti,0.010374
variou,0.007865
various,0.010684
veri,0.015781
view,0.017711
vincent,0.010948
vip,0.015121
virtu,0.008753
vol,0.014849
volum,0.011835
volumeparagraph,0.018725
w,0.011593
wa,0.005665
walk,0.026473
warm,0.008828
way,0.044327
weed,0.012486
weiss,0.012953
west,0.005932
whatev,0.008504
whenev,0.009721
wherea,0.005994
wholli,0.020104
wild,0.008863
william,0.011071
word,0.004795
work,0.013547
workshop,0.010225
world,0.007005
worth,0.007822
woven,0.012684
write,0.010803
x,0.036701
xrightarrow,0.092440
y,0.066213
york,0.010641
yrightarrow,0.053083
z,0.101145
zeroth,0.029857
zerothord,0.055262
