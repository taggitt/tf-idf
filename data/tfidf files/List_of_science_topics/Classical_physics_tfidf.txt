accept,0.013971
accord,0.011128
aether,0.035508
agre,0.016778
appear,0.038670
appli,0.011897
applic,0.026581
approxim,0.030332
arithmet,0.027172
assum,0.015417
astronom,0.025058
atom,0.038919
attempt,0.012694
base,0.009696
becom,0.010742
befor,0.011460
begin,0.013084
behavior,0.015740
belong,0.018960
bodi,0.013465
branch,0.014270
break,0.018497
c,0.025684
calcul,0.051371
calculu,0.025082
case,0.011237
centuri,0.011540
chao,0.026008
character,0.017046
classic,0.608826
classicalquantum,0.053069
comparison,0.037696
complet,0.024829
complex,0.013153
comput,0.104261
concept,0.012544
concern,0.025239
condit,0.012577
consid,0.020966
constant,0.017870
consum,0.017898
context,0.047438
contrast,0.015184
correct,0.036203
correspond,0.032636
criteria,0.022511
current,0.011507
deal,0.014481
definit,0.028427
depend,0.011860
deriv,0.013667
describ,0.033192
descript,0.032863
determin,0.024966
determinist,0.028699
differ,0.009529
differenti,0.036402
discover,0.032887
discoveri,0.016836
distinct,0.013844
doe,0.012162
dynam,0.033706
effect,0.010513
ehrenfest,0.042309
electrodynam,0.064987
electromagnet,0.048525
emerg,0.013870
energi,0.060602
equat,0.107996
error,0.020579
essenti,0.015044
everyday,0.023118
exact,0.020646
exampl,0.009692
exclud,0.020359
exist,0.032285
factor,0.013722
fall,0.014772
father,0.019026
field,0.059093
forc,0.011599
formal,0.014481
formul,0.018532
formula,0.020208
free,0.013218
galilean,0.035820
gener,0.058209
geometri,0.023602
glossari,0.028208
ha,0.015433
hamiltonian,0.033573
hand,0.014339
handl,0.021043
high,0.011717
higher,0.013757
hour,0.020335
howev,0.009392
ignor,0.020073
includ,0.024100
interpret,0.015438
introduc,0.013575
introduct,0.015341
just,0.013493
kinet,0.026193
lack,0.014752
lagrangian,0.035210
larg,0.030589
larger,0.030543
later,0.011506
law,0.046621
length,0.019195
level,0.023138
light,0.079751
likewis,0.022915
limit,0.023630
looser,0.042137
low,0.029654
lower,0.014295
luminifer,0.039294
macroscop,0.027715
major,0.010263
mani,0.008744
manual,0.048872
massiv,0.020585
mathemat,0.015208
maxwel,0.028849
mean,0.010654
mechan,0.185721
medium,0.022054
million,0.015195
model,0.096403
modern,0.081226
molecul,0.020889
momentum,0.025432
motion,0.039042
natur,0.010327
need,0.011629
neglect,0.025305
neglig,0.028785
new,0.008641
newton,0.048850
newtonian,0.059874
nonlinear,0.026503
nonrelativist,0.036081
number,0.009677
obey,0.027606
object,0.105795
older,0.020300
ongo,0.021168
onli,0.008905
oper,0.012204
order,0.010660
overview,0.019383
paradigm,0.070319
particl,0.020794
particular,0.024217
particularli,0.013566
passag,0.023047
perform,0.013599
physic,0.430393
physicist,0.022898
planck,0.029370
point,0.011221
possibl,0.011314
practic,0.011868
predat,0.024625
predict,0.016231
previou,0.016728
principl,0.026632
proceed,0.020300
produc,0.011542
propag,0.049433
provid,0.029527
quantis,0.049586
quantum,0.313238
quick,0.027412
rang,0.013316
real,0.014602
realm,0.044448
reconcil,0.027660
reduc,0.013328
refer,0.035176
rel,0.151372
relativist,0.058097
reliabl,0.042073
repres,0.011540
research,0.022827
resort,0.026107
resourc,0.014243
rise,0.014157
satisfi,0.020870
scale,0.031847
second,0.011546
semiclass,0.039987
shift,0.017049
shown,0.016966
significantli,0.018056
situat,0.015075
slightli,0.021221
smaller,0.016648
solut,0.033503
solv,0.036039
space,0.015095
special,0.061228
speed,0.040046
st,0.015783
standard,0.013187
stationari,0.028388
strength,0.019858
suffic,0.034139
superfluid,0.079034
tend,0.016078
term,0.018504
th,0.011227
theorem,0.021949
theori,0.241013
thermodynam,0.025131
thi,0.021970
time,0.017326
today,0.014066
tradit,0.012329
true,0.015853
understood,0.018375
unlik,0.016305
unnecessarili,0.037743
use,0.064004
usual,0.011782
v,0.017458
valid,0.018697
vc,0.037246
veloc,0.077242
view,0.012549
vigor,0.028327
wa,0.016056
welldescrib,0.045107
whi,0.018015
wide,0.012610
world,0.009926
