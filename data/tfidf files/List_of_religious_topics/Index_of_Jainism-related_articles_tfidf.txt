abstin,0.039648
acaranga,0.054758
acharya,0.041584
adipurana,0.056553
agama,0.043317
age,0.013462
ahimsa,0.040229
alcohol,0.024679
altruism,0.032744
anekantavada,0.048443
anim,0.015047
antakrddaasah,0.056553
anuttaraupapatikadasah,0.056553
aparigraha,0.054758
aphor,0.035902
art,0.016446
articl,0.012776
aryika,0.054758
ascet,0.034726
ascetic,0.038064
ascript,0.042837
ashtamangala,0.053365
asteya,0.054758
atom,0.018797
attribut,0.016711
auspici,0.041041
author,0.036311
axial,0.033956
b,0.014239
bhattaraka,0.052226
bhavana,0.049038
bhavyata,0.056553
bibl,0.025994
book,0.049278
brahmacharya,0.048443
brahmin,0.038283
broom,0.043073
c,0.012405
cast,0.022052
categori,0.033130
celesti,0.027521
champat,0.049695
characterist,0.014964
color,0.021628
commun,0.022707
compar,0.025684
compass,0.055948
concept,0.048469
convent,0.016814
cosmolog,0.025386
council,0.015833
cremat,0.037449
critic,0.012772
cultur,0.013056
d,0.014057
dashlakshan,0.056553
day,0.013103
death,0.015208
detach,0.028258
deva,0.037957
devot,0.020525
dharma,0.066845
diagram,0.023887
dialect,0.025543
digambara,0.049038
discipl,0.029019
distinct,0.013373
divakara,0.053365
divin,0.023095
doctrin,0.020823
donat,0.026009
dream,0.025069
e,0.014070
eastern,0.017351
ecospiritu,0.053365
element,0.013295
enlighten,0.022827
epistemolog,0.024845
etern,0.026429
ethic,0.019240
europ,0.014175
euthanasia,0.037163
exist,0.010395
f,0.015399
fast,0.064201
festiv,0.051926
flag,0.024963
footwear,0.037749
forgiv,0.034183
g,0.015123
ganadhara,0.053365
gestur,0.030925
glossari,0.081746
god,0.019565
greet,0.032883
growth,0.014149
guid,0.015805
gunasthana,0.056553
guru,0.033371
gymnosophist,0.047899
h,0.015528
haribhadra,0.049695
histori,0.009310
hong,0.026136
hous,0.014996
humil,0.039511
idolatri,0.037648
india,0.034229
indian,0.074019
indra,0.041217
infin,0.029224
institut,0.011829
intern,0.009796
introspect,0.034300
irreligion,0.044115
j,0.014038
jai,0.041776
jain,0.312540
jaina,0.041776
jainism,0.542415
jainismrel,0.056553
jainolog,0.052226
jainpedia,0.056553
japan,0.018486
jina,0.135093
jinendra,0.056553
jiva,0.043073
jivanam,0.054758
jnana,0.043838
journal,0.014378
k,0.016948
kalpasutra,0.051264
kalpavriksha,0.056553
kanda,0.046103
karma,0.067156
kashaya,0.054758
kevala,0.047899
kong,0.026363
l,0.016452
languag,0.014047
lesya,0.056553
level,0.011175
life,0.024027
lifestyl,0.026615
lipi,0.052226
list,0.010391
live,0.012083
logic,0.018087
lovingkind,0.047400
m,0.013365
maha,0.039648
mahavira,0.039511
major,0.009914
mantra,0.036714
mardava,0.056553
mean,0.010291
meat,0.025932
media,0.017141
medit,0.028394
merci,0.032344
method,0.011573
moksha,0.038283
monastic,0.038510
monk,0.055282
murtipujaka,0.054758
mystic,0.055121
mytholog,0.026136
n,0.016782
namokar,0.054758
naraka,0.049695
nicknam,0.029657
night,0.022921
nirvana,0.036459
noncreation,0.056553
nonkil,0.048443
nontheist,0.037957
nonviol,0.032216
nuditi,0.043572
nun,0.035391
o,0.019706
om,0.038867
omnisci,0.036801
organ,0.020386
origin,0.010356
outlin,0.018189
p,0.014265
pacif,0.021659
page,0.018320
paint,0.023733
pancaparamesthi,0.056553
parasparopagraho,0.054758
paroksha,0.056553
passion,0.056121
peac,0.018022
peacebuild,0.046103
perspectiv,0.039932
philosophi,0.127280
physic,0.012227
plural,0.024033
poem,0.027580
portal,0.026951
practic,0.011464
pramada,0.056553
pratikraman,0.054758
prayer,0.028236
preceptor,0.186027
project,0.013259
prostrat,0.042610
q,0.023999
qualiti,0.015965
queen,0.022489
quot,0.022419
r,0.014339
rai,0.040542
ratnatraya,0.052226
realiti,0.018486
realm,0.021468
reincarn,0.033630
religi,0.104158
religion,0.173091
return,0.013754
rishabhanatha,0.053365
s,0.009638
sacr,0.026025
salvat,0.029356
saman,0.045725
samayika,0.053365
samsara,0.037449
sanctiti,0.039648
sandalwood,0.042837
sarak,0.056553
satya,0.043073
school,0.013826
sculptur,0.030559
search,0.017394
sect,0.056926
self,0.023355
selfless,0.041975
sentienc,0.039377
shave,0.038173
shrine,0.033473
siddhasena,0.049695
simpl,0.015028
singapor,0.024923
site,0.031863
song,0.025083
soul,0.024420
spiritu,0.087172
sramana,0.043073
state,0.008275
statist,0.031034
statu,0.015796
sthanakvasi,0.052226
stotra,0.048443
studi,0.019991
subatom,0.030062
subsect,0.109131
suicid,0.027116
suprem,0.019368
sutra,0.066845
suttam,0.052226
svetambar,0.056553
svetambara,0.051264
swastika,0.047899
symbol,0.051513
symbolog,0.046506
t,0.016456
tattva,0.048443
tattvartha,0.049695
templ,0.050006
tenet,0.027847
terapanth,0.054758
term,0.017874
text,0.048947
timelin,0.044526
tirtha,0.049695
tirthankara,0.043073
topic,0.015006
tradit,0.023820
transtheism,0.052226
tree,0.019482
trishala,0.054758
truth,0.060370
u,0.022069
umaswami,0.056553
unit,0.009539
univers,0.009796
upadhyaya,0.052226
v,0.016864
vallabhi,0.054758
vegetarian,0.036628
vener,0.031314
view,0.048489
virtu,0.095861
vital,0.021123
vow,0.133083
vrata,0.099391
w,0.015869
water,0.014589
welfar,0.021785
wikimedia,0.026363
wikipedia,0.060799
wikiproject,0.048443
wikiquot,0.035827
wikivers,0.040079
wiktionari,0.035533
women,0.018000
world,0.028767
write,0.014788
writer,0.019411
x,0.016746
xenagogu,0.056553
xerophagi,0.056553
y,0.020141
yantra,0.051264
yati,0.056553
yoga,0.034540
z,0.023075
zero,0.019890
zoism,0.056553
zootyp,0.056553
