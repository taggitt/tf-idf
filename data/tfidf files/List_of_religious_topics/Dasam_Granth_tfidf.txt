abov,0.005269
abridg,0.014025
absolv,0.016402
academi,0.007929
accept,0.005167
account,0.004848
acquaint,0.012047
adhyay,0.021654
akal,0.070613
alphabet,0.019823
amrit,0.017972
analysi,0.004962
anandpur,0.039995
ani,0.003627
appear,0.009535
ard,0.017003
armi,0.007080
asfotak,0.021654
attribut,0.012797
aurangzeb,0.016231
authent,0.010426
authorship,0.039676
avail,0.015095
avtar,0.104835
awadhi,0.021654
awaken,0.011652
b,0.005452
bachitar,0.086618
bachitt,0.021654
bachittar,0.021654
bainti,0.021654
bani,0.076451
baptism,0.013498
base,0.003586
basi,0.005127
batey,0.019629
battl,0.023321
bawanja,0.021654
befor,0.004239
benti,0.039995
bhagauti,0.021654
bhai,0.105050
bhalla,0.019997
bhasha,0.038620
bir,0.015851
bisan,0.021654
book,0.004717
braj,0.041934
budha,0.019310
camp,0.008994
ceremoni,0.009176
chamkaur,0.020967
chand,0.017003
chandi,0.150214
charitar,0.020433
charitropakhyan,0.041934
chaubi,0.020967
chaupa,0.021654
chaupai,0.078517
chibber,0.043309
chopai,0.021654
chritra,0.064963
claim,0.015138
classic,0.005492
collect,0.014548
come,0.004754
commiss,0.006618
commonli,0.005521
compil,0.060043
complet,0.004591
compos,0.050461
composit,0.082709
compris,0.006801
contain,0.014309
content,0.006560
convers,0.006859
copi,0.016031
copier,0.019629
court,0.006306
creator,0.019930
cross,0.007256
current,0.004256
daili,0.023328
dal,0.015181
daljeet,0.021654
dar,0.014929
dasam,0.449313
date,0.005556
death,0.005823
defens,0.007297
demis,0.011751
demon,0.011763
di,0.031043
differ,0.003524
digit,0.014849
direct,0.004415
disput,0.006687
divin,0.008843
drama,0.011230
dure,0.007654
durga,0.019028
earli,0.004003
earliest,0.006690
emperor,0.008828
employ,0.005309
engag,0.006338
entir,0.015576
entourag,0.016315
entri,0.007742
epistl,0.014616
evid,0.005458
examin,0.006265
exegesi,0.014616
exist,0.003980
extern,0.003130
fatehnama,0.021654
fear,0.007511
follow,0.010272
gahe,0.021654
gian,0.016785
giani,0.019310
gobind,0.214851
god,0.014983
granth,0.460395
gur,0.017972
gurbila,0.043309
gurmukh,0.019629
gurmukhi,0.017807
guru,0.332230
guruship,0.019629
gyan,0.017653
ha,0.002854
hand,0.005303
hazar,0.019997
hi,0.011513
hikayat,0.079991
hindi,0.015077
hindustani,0.017242
histor,0.014424
histori,0.014260
holocaust,0.012319
huge,0.007960
hymn,0.012645
hypocrisi,0.015077
idolatri,0.014415
ii,0.005601
import,0.007301
includ,0.008914
intern,0.003750
invoc,0.014616
j,0.005375
jaap,0.114209
jab,0.017653
kaal,0.021654
kabiovach,0.021654
kabit,0.020967
kahan,0.016072
kalal,0.020967
kashish,0.021654
kavi,0.057085
ke,0.014658
kesar,0.021654
khalsa,0.017972
khand,0.021654
ki,0.042690
kian,0.043309
knowledg,0.005383
known,0.003576
kosh,0.021654
koyar,0.021654
krishna,0.013472
krisna,0.021654
kup,0.019997
lal,0.015077
languag,0.005378
later,0.004255
length,0.007100
letter,0.007064
letterbut,0.021654
librari,0.006723
line,0.010243
link,0.003111
list,0.003979
liturgi,0.027494
lost,0.012437
magnum,0.013578
mahan,0.016315
mahima,0.021654
major,0.003796
mala,0.033784
malcolm,0.012335
malkaun,0.021654
mani,0.016171
manuscript,0.038839
maryada,0.039258
mata,0.032631
materi,0.005014
mention,0.076202
metric,0.009609
minor,0.006621
mission,0.007134
moder,0.008197
modern,0.004291
morn,0.031192
nabha,0.020967
nam,0.011261
nama,0.017119
nand,0.016315
natak,0.125802
nihang,0.019310
nitnem,0.056330
number,0.003579
offens,0.009971
onc,0.005346
onli,0.003293
opu,0.012938
padishah,0.018548
pae,0.017972
page,0.014029
pahul,0.020967
pain,0.009062
panjab,0.017972
parbodh,0.021654
parchian,0.021654
parentag,0.016231
parkash,0.021654
patshahi,0.041934
pen,0.011465
persian,0.026970
person,0.004682
place,0.004046
play,0.005166
poet,0.059366
possess,0.006430
prais,0.019188
prakash,0.016492
prasad,0.015922
prayer,0.032435
preced,0.007773
prepar,0.006654
prescrib,0.010019
present,0.004102
preserv,0.006915
primarili,0.005608
print,0.015410
publicli,0.008780
punjabi,0.015851
purakh,0.019629
quatrain,0.017653
question,0.005152
quot,0.017168
raam,0.019997
rama,0.014377
read,0.004519
recens,0.017508
recit,0.047525
refer,0.007806
rehat,0.038057
rehitnama,0.043309
rehra,0.019997
religi,0.019941
repeatedli,0.009259
reprob,0.018149
rest,0.006096
river,0.007114
role,0.009117
s,0.003690
sachkhoj,0.021654
sahib,0.151290
sakhian,0.021654
sanchar,0.020433
santokh,0.019310
sarup,0.020433
savaiy,0.077240
savaiya,0.020967
say,0.005637
scholar,0.013217
scientif,0.005236
scriptur,0.010470
second,0.004270
seen,0.005105
senapat,0.021654
separ,0.004754
sever,0.007551
shabad,0.018776
shahib,0.021654
shastar,0.021654
shastarnaam,0.021654
shyam,0.036681
sift,0.015404
sikh,0.176520
singh,0.347142
singular,0.009543
sirsa,0.021654
sketch,0.011616
sobha,0.021654
special,0.004529
sri,0.031613
srvada,0.021654
standard,0.004877
stanza,0.030155
strang,0.010744
structur,0.004231
style,0.007462
suffer,0.006610
sukkha,0.019997
sundari,0.020433
sundri,0.016785
suraj,0.019028
surviv,0.006246
svarup,0.021654
swaiyey,0.043309
swiayey,0.021654
tale,0.010411
tav,0.019028
tavprasad,0.039995
te,0.013133
tell,0.008157
tenth,0.011101
text,0.031237
textual,0.011377
thcenturi,0.008350
thi,0.008126
time,0.003204
titl,0.006500
tradit,0.013681
translat,0.012469
tumr,0.021654
tune,0.011763
twovolum,0.014534
ugardanti,0.021654
unreli,0.011604
use,0.002959
ustat,0.062901
vaar,0.020433
var,0.042690
variat,0.006788
variou,0.008245
verac,0.015235
version,0.012670
view,0.009283
volum,0.006203
wa,0.035633
war,0.010289
weapon,0.008042
wide,0.004664
work,0.014201
write,0.016987
written,0.028669
wrote,0.006170
zafar,0.018340
zafarnama,0.041934
zafarnamah,0.019629
