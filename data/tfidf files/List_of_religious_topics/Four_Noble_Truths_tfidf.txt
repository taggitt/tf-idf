abandon,0.017082
abhisamayalamkara,0.024054
abid,0.013430
accompani,0.008840
accomplish,0.004594
accord,0.070871
accordingli,0.005145
account,0.008078
acknowledg,0.004408
acquir,0.003723
action,0.002809
actual,0.008778
ad,0.020423
add,0.004359
addit,0.009251
address,0.003442
age,0.008589
aggreg,0.009669
agre,0.003446
ajahn,0.039562
alreadi,0.006661
altern,0.008813
amaravati,0.010186
analog,0.012255
analysi,0.002756
ancient,0.003312
anderson,0.053626
andhaka,0.011349
ani,0.004029
anoth,0.002198
anquish,0.012027
apart,0.004189
appar,0.003902
appear,0.005296
appli,0.002444
apprehend,0.007511
aris,0.059677
arisen,0.006243
ariya,0.022214
ariyapariyesan,0.012027
ariyasaccani,0.012027
ariyasaccni,0.011645
aros,0.004540
arsava,0.012027
arya,0.043828
asaraka,0.011349
asava,0.011645
ascet,0.007385
assert,0.007855
assum,0.003167
attain,0.064792
attitud,0.004693
augusta,0.008926
authent,0.005790
awaken,0.038831
awar,0.004136
away,0.003482
ayam,0.032175
background,0.003926
banarsidass,0.009015
base,0.001992
basic,0.022186
batchelor,0.009160
becaus,0.010375
becom,0.015447
beg,0.007209
behav,0.009806
believ,0.002851
belong,0.003895
best,0.006076
better,0.003108
bhavatanha,0.011645
bhikkhu,0.079238
biograph,0.006260
birth,0.032496
bless,0.013067
blow,0.005840
bodhi,0.008523
bodhisattva,0.008346
bodhisattvapath,0.023290
bodi,0.008298
bodili,0.048193
book,0.002620
born,0.011682
bridg,0.004554
brief,0.004369
brill,0.007269
bring,0.006854
bronkhorst,0.129769
buddha,0.239209
buddhadasa,0.020604
buddhathat,0.012027
buddhism,0.123501
buddhist,0.121987
calm,0.006485
came,0.009056
canon,0.055146
capac,0.003827
carol,0.013685
carolyn,0.008190
cattri,0.011645
catvri,0.011645
caught,0.005686
caus,0.014786
causal,0.005140
cautiou,0.007065
ce,0.005059
ceas,0.018655
central,0.025881
centuri,0.004741
certainli,0.005270
cessat,0.123587
chang,0.004163
chapter,0.012909
characterist,0.003182
chgyam,0.010568
classic,0.003050
clear,0.003436
cling,0.115528
cluster,0.005066
combin,0.005266
come,0.007922
comment,0.004422
commentari,0.047908
commonli,0.006133
compass,0.005949
compassion,0.007771
complet,0.005100
compound,0.004161
comprehend,0.006541
concentr,0.003613
concept,0.010308
conceptu,0.008889
condit,0.012919
confin,0.019833
conjunct,0.005160
connect,0.003040
consequ,0.003140
consid,0.002153
consider,0.003103
consist,0.004943
constant,0.003671
constitu,0.004324
constitut,0.005831
contain,0.015895
contempl,0.025497
contemporari,0.010836
content,0.003643
continu,0.002237
conveni,0.004930
convent,0.003575
cook,0.005206
cool,0.015519
cooler,0.007097
correct,0.011156
correctli,0.005306
cosmo,0.006249
cours,0.006437
cousin,0.012146
crave,0.238965
creed,0.007118
critic,0.002716
culmin,0.014679
cultiv,0.004716
cure,0.011328
cut,0.007841
cycl,0.018928
daili,0.004319
dalai,0.008523
danc,0.005922
danger,0.004337
dare,0.007163
day,0.008360
deal,0.002974
death,0.029109
deathless,0.010902
debat,0.003438
deer,0.007013
defil,0.008765
definit,0.002919
delight,0.015113
denot,0.004506
describ,0.011364
descript,0.010127
desir,0.013902
destin,0.005168
destroy,0.011585
destruct,0.026223
develop,0.014775
dhamma,0.026531
dhammacakkappavattana,0.126826
dharma,0.021324
dhyana,0.068634
diagnosi,0.011206
did,0.008223
didnt,0.006068
die,0.007076
differ,0.007830
digha,0.019153
direct,0.007356
directli,0.002967
disagre,0.010618
disappear,0.004695
disbecom,0.012027
disciplin,0.003402
discours,0.030340
disengag,0.008095
dismiss,0.004920
displeas,0.008656
disput,0.003714
dissatisfact,0.020448
distinguish,0.003278
doctrin,0.022143
document,0.003565
doe,0.002498
donald,0.005206
dukkha,0.226937
dukkham,0.024054
dukkhanirodha,0.012027
dukkhanirodhagamini,0.024054
dukkhasamudayo,0.012027
dure,0.002125
e,0.002992
earli,0.008894
earlier,0.003285
earliest,0.029729
edit,0.010439
effect,0.002159
effort,0.003079
eightfold,0.072451
ekavyvahrika,0.022214
element,0.008482
emerg,0.002849
emphas,0.027313
emphasi,0.008065
empti,0.005300
enclos,0.006038
end,0.029170
endless,0.006807
english,0.003096
enlighten,0.077675
enlightenmentstori,0.012027
enter,0.003250
epstein,0.008095
equal,0.009083
equival,0.003581
error,0.004227
eschatolog,0.007701
essenc,0.005022
essenti,0.003090
etern,0.005620
etymolog,0.004732
evok,0.006773
evolv,0.007298
exactli,0.008711
examin,0.003479
exampl,0.001991
exemplifi,0.005664
exist,0.011054
expect,0.003054
experi,0.013670
experienc,0.011974
explain,0.020950
explan,0.003913
explicit,0.004854
express,0.014403
eye,0.008889
eyeconsci,0.023290
eyecontact,0.069872
face,0.003337
fact,0.002822
factor,0.002819
fade,0.006349
fearless,0.008692
feed,0.005103
feel,0.024938
feer,0.012027
felt,0.018773
fetter,0.008656
fever,0.012674
fifth,0.004734
final,0.005751
fit,0.004047
fix,0.003528
float,0.010529
foley,0.009323
follow,0.024723
food,0.003421
form,0.019989
formul,0.015228
formula,0.008303
foundat,0.006200
fourth,0.004148
framework,0.006812
free,0.002715
freed,0.006129
freedom,0.007506
fresh,0.010680
friendli,0.005476
fruit,0.004711
function,0.024340
fundament,0.003044
gain,0.005784
gamini,0.011645
gautama,0.015438
gave,0.003459
gener,0.003416
geoffrey,0.006215
gesh,0.009982
gethin,0.010725
given,0.004768
goal,0.003270
goldstein,0.007884
grammat,0.007209
gratif,0.008346
great,0.005358
greater,0.002994
group,0.002144
grove,0.006897
grow,0.003048
guis,0.007398
ha,0.011096
hanh,0.011107
happen,0.007514
happi,0.026283
harpercollin,0.007482
harvey,0.006029
hearer,0.009015
heart,0.004363
held,0.005621
hi,0.008526
higher,0.002826
hinayana,0.009724
histor,0.005341
hold,0.002863
homogen,0.005354
idam,0.010725
idea,0.008004
identifi,0.011495
ill,0.027697
imperman,0.060844
import,0.014193
incap,0.025624
includ,0.016503
incongru,0.008346
inconsist,0.016574
incorrect,0.005660
increas,0.006858
india,0.010919
indian,0.015741
indic,0.002924
indispens,0.006636
infatu,0.009015
inflam,0.025667
influenc,0.002618
inform,0.002363
initi,0.002583
insecur,0.006824
insert,0.005515
insight,0.110249
instead,0.005569
instruct,0.008951
intend,0.003680
intent,0.004000
internalis,0.008884
interpret,0.006343
interrupt,0.005913
intersect,0.005357
intoxic,0.056293
introduc,0.002788
japanes,0.009061
jhana,0.030241
johann,0.005087
joseph,0.003917
joy,0.006445
just,0.002772
kamatanha,0.011645
kamma,0.010302
karma,0.035705
kataa,0.012027
kathvatthu,0.010725
key,0.006050
khandha,0.010902
kiccaa,0.012027
kindl,0.025385
know,0.018010
knowledg,0.035880
known,0.007945
kondaa,0.011107
kr,0.023771
lama,0.008141
late,0.002751
later,0.028366
lead,0.023715
liber,0.146925
life,0.028104
lifetim,0.005089
lion,0.006187
list,0.002210
liter,0.004570
littl,0.003041
live,0.002569
livelihood,0.006887
long,0.007741
longer,0.006555
lopez,0.007884
lotu,0.031159
ls,0.008214
lust,0.040828
magga,0.034047
mahaparinibbana,0.032707
mahasaccaka,0.024054
mahayana,0.043767
mahsaka,0.010725
mahsghika,0.009804
maintain,0.002771
majjhima,0.049024
make,0.006063
mark,0.003190
master,0.008482
materi,0.002785
matrix,0.004988
matter,0.002989
mean,0.041586
medic,0.007319
medicin,0.003789
medit,0.030193
melvin,0.008214
mental,0.043005
messag,0.014817
middl,0.003276
mind,0.059966
mindcontrol,0.011349
miss,0.005013
mnemon,0.015403
model,0.004951
modern,0.002383
moffitt,0.011645
moksha,0.024425
moment,0.013564
monast,0.006869
motil,0.007426
motion,0.004010
motiv,0.007765
mundan,0.014770
narada,0.010568
natur,0.010608
near,0.003267
need,0.007167
neg,0.003446
network,0.003322
nhat,0.009804
nibbana,0.040747
nichiren,0.027330
nidna,0.010725
night,0.004874
nikay,0.011349
nikaya,0.085885
nirodha,0.020604
nirodho,0.012027
nirvana,0.023261
nirvna,0.022214
nobilis,0.012027
nobl,0.289408
nonattach,0.009982
nonexist,0.006171
nonphys,0.007651
nonreli,0.012027
norman,0.027998
nose,0.013335
note,0.022913
notic,0.013532
notnot,0.011107
nt,0.007541
nurtur,0.006555
obtain,0.003089
occupi,0.003984
old,0.003288
oldest,0.021496
olivel,0.010428
omit,0.006209
onc,0.002969
oneself,0.012441
onli,0.003658
opposit,0.003270
optim,0.004215
order,0.002190
ordinari,0.004333
orient,0.008282
origin,0.028632
ornament,0.006652
ourselv,0.006419
oxford,0.003734
pacifi,0.007651
page,0.003896
pain,0.090606
painfulnorpleas,0.048109
pali,0.089450
pariyatti,0.011349
park,0.004644
particular,0.002487
particularli,0.002787
pass,0.006174
passag,0.004734
path,0.132973
patipada,0.034936
patrick,0.005755
paul,0.006909
peac,0.015331
penetr,0.021236
peopl,0.004382
perhap,0.003694
period,0.004722
person,0.010402
perspect,0.007112
peter,0.003623
phenomena,0.015178
phillip,0.006150
physic,0.002600
physician,0.005013
pitaka,0.009724
pith,0.009443
place,0.006742
pleas,0.006019
pleasant,0.035325
pleasur,0.017727
plu,0.004471
pluck,0.008588
point,0.004610
portray,0.005173
posit,0.009421
possess,0.003571
possibl,0.013946
pp,0.003715
practic,0.024382
prajna,0.048243
pratityasamutpada,0.010080
preced,0.008635
preciou,0.005981
prescript,0.006005
present,0.018229
press,0.008046
prevent,0.003258
priest,0.005395
print,0.004279
probabl,0.013080
proce,0.011096
process,0.006463
produc,0.011856
prognosi,0.007808
progress,0.003106
promin,0.007147
promot,0.003250
proposit,0.041903
provid,0.006065
provision,0.005774
psycholog,0.011653
psychotherapi,0.006851
public,0.002295
publish,0.005058
pure,0.003603
rahula,0.009576
reach,0.017623
read,0.007530
real,0.008999
realis,0.005827
realiti,0.023588
realiz,0.020206
realli,0.004650
reason,0.002593
rebecom,0.011107
rebirh,0.012027
rebirth,0.134305
recogn,0.009297
recognit,0.003961
recommend,0.004260
record,0.012186
redact,0.008692
redeath,0.042900
refer,0.008671
reflect,0.006212
regard,0.019410
rel,0.002591
relat,0.001948
releas,0.010836
reliev,0.006048
religi,0.007383
religion,0.007362
relinquish,0.006636
remainderless,0.012027
remark,0.008866
remind,0.006519
remov,0.003337
render,0.004740
renew,0.016905
repeat,0.025030
replac,0.005890
repres,0.007112
represent,0.003646
reserv,0.003843
respect,0.002748
respons,0.005013
restrain,0.006860
restraint,0.006492
restrict,0.003349
result,0.006039
resultssubject,0.011645
retriev,0.004293
reveal,0.003801
rice,0.010902
right,0.025936
ringu,0.011645
rise,0.002908
rittaka,0.011349
river,0.003951
road,0.004198
rodal,0.010902
root,0.003651
round,0.004578
routledg,0.005150
rule,0.002638
rupert,0.008240
ryasatyni,0.011645
s,0.002049
sacca,0.038307
saccaa,0.012027
saccaka,0.012027
sacr,0.005534
said,0.011878
saint,0.004832
sakhra,0.010725
samsara,0.023893
samudaya,0.022698
samudayo,0.012027
samuel,0.005138
samyutta,0.009648
sanskrit,0.029931
satisfi,0.017149
satya,0.027481
sava,0.018764
say,0.006262
sceptic,0.006925
schmithausen,0.020161
scholar,0.011011
school,0.005881
search,0.003699
sect,0.012106
seek,0.006486
seen,0.005671
self,0.009933
sens,0.005947
sensepleasur,0.011645
sensual,0.008028
sentient,0.006887
separ,0.002640
sequenc,0.004012
sermon,0.021736
serv,0.008631
set,0.033583
sever,0.006290
shakyamuni,0.020373
shambhala,0.018030
shorten,0.006108
shorthand,0.007426
sight,0.005363
signific,0.002567
simpli,0.006564
simplist,0.007186
simultan,0.008430
sinc,0.004022
singl,0.005383
singular,0.010601
sixteen,0.006599
skandha,0.029173
slightli,0.004359
snow,0.006232
sorrow,0.007736
sought,0.003918
sound,0.004235
sourc,0.007590
specif,0.002406
specifi,0.004089
speech,0.004333
sphere,0.009002
spiro,0.019017
spoken,0.005411
stage,0.010538
start,0.005092
state,0.029920
statement,0.014770
step,0.003410
stephen,0.004468
stop,0.015895
stori,0.012232
stra,0.008141
strata,0.006983
stress,0.004124
strictli,0.004743
structur,0.002350
student,0.003423
studi,0.008503
subject,0.017912
subsequ,0.003139
substanc,0.004228
substanti,0.007468
substitut,0.013409
sucitto,0.011645
suffer,0.095454
suffici,0.003718
sukha,0.010568
sumedho,0.011107
sunyata,0.019449
supersed,0.005811
suppress,0.004606
sutra,0.042648
sutta,0.202498
suttapitaka,0.012027
symbol,0.036518
tah,0.010725
taint,0.016531
taken,0.002945
talk,0.004328
tanha,0.010568
tashi,0.011645
tathagata,0.009982
taught,0.017939
teach,0.102452
teacher,0.018438
temporari,0.014231
tend,0.006606
tenfold,0.007943
term,0.022808
text,0.020819
thai,0.007209
theori,0.002357
theravada,0.091432
theravda,0.008804
theravdin,0.009890
thereaft,0.005259
therefor,0.002734
thi,0.075226
thich,0.010725
thing,0.048009
thinker,0.004862
thirst,0.007985
thorson,0.012027
thought,0.022806
thu,0.002351
tibetan,0.042081
tie,0.007847
tilmann,0.011349
time,0.010678
today,0.002889
togeth,0.005547
told,0.005101
tongu,0.013446
topic,0.003191
torment,0.017687
total,0.002806
tradit,0.053192
transcend,0.017070
transcript,0.005940
transform,0.003292
translat,0.048481
trap,0.005270
treat,0.003522
treatment,0.003700
troubl,0.010562
true,0.006513
truli,0.005040
trungpa,0.010302
truth,0.530682
tsere,0.012027
tucchaka,0.011349
tulku,0.010725
turn,0.013904
twelv,0.005306
typic,0.005397
udayabbaya,0.011107
ui,0.008804
ultim,0.006958
unborn,0.008265
undergon,0.012555
understand,0.024573
understood,0.030200
uneas,0.008843
unfett,0.008118
uninfatu,0.012027
uninflam,0.012027
union,0.002989
univers,0.002083
unsatisfactori,0.036107
unsatisfi,0.008095
unspecifi,0.007294
unsurpass,0.008970
use,0.004930
usual,0.002420
vain,0.007635
valuabl,0.004722
variat,0.003770
variou,0.018319
vehicl,0.004567
verbatim,0.008374
veri,0.004594
vers,0.005922
versa,0.005708
version,0.017592
vetter,0.041209
vibhavatanha,0.011645
vice,0.004656
view,0.010312
villag,0.004596
vinaya,0.027330
vinayapitaka,0.012027
vipka,0.022214
vision,0.004356
volum,0.003445
wa,0.034635
wait,0.010908
walpola,0.009648
want,0.003713
way,0.021507
weari,0.008588
websourc,0.009382
wellknown,0.018888
western,0.006051
whatev,0.004951
wheel,0.024233
wherea,0.003490
whi,0.003701
william,0.006446
wisdom,0.015977
wish,0.004447
wonder,0.005827
word,0.005584
world,0.018353
worthi,0.006343
