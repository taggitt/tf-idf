abandon,0.007989
abbrevi,0.010417
abestg,0.067499
abraham,0.010044
accept,0.010739
accord,0.004276
accus,0.008577
acknowledg,0.008247
act,0.009776
actual,0.005473
addit,0.008653
address,0.012881
adurbad,0.020778
afresh,0.017667
afrinagan,0.044999
age,0.005356
ahuna,0.020778
ahura,0.048781
alexand,0.008194
alphabetderiv,0.022499
alreadi,0.006230
alter,0.007671
altogeth,0.010602
analysi,0.005155
andrea,0.022759
angel,0.010256
anoth,0.012340
anquetilduperron,0.083113
aogemadaeca,0.022499
appar,0.007301
appear,0.009907
appropri,0.013711
approv,0.007347
arachosia,0.018858
arama,0.014822
archaic,0.012263
archetyp,0.037596
archiv,0.007871
arda,0.019056
ardashir,0.019509
arein,0.021231
arrang,0.013945
arsacid,0.016865
arsacidera,0.022499
articl,0.005083
asia,0.007343
asid,0.009754
assault,0.010965
assum,0.011850
aton,0.015415
attribut,0.006648
authent,0.010833
author,0.004815
authorit,0.011936
avail,0.005228
avesta,0.398714
avestan,0.242695
avn,0.044999
b,0.016995
base,0.007453
becam,0.009427
begin,0.005028
behaviour,0.007951
believ,0.005333
bibliographi,0.007500
bibliothqu,0.015945
bless,0.012223
bombay,0.014978
book,0.039210
borrow,0.008559
breach,0.011668
brief,0.008173
british,0.005758
brought,0.006357
bundahishn,0.020063
c,0.009871
calendar,0.010575
cama,0.020395
canon,0.018757
care,0.006488
carl,0.008623
categor,0.008780
categori,0.026361
ce,0.018928
central,0.009683
centuri,0.044352
ceremoni,0.057207
certain,0.004727
cf,0.011647
chang,0.003894
chapter,0.024150
charact,0.014275
chariti,0.011207
citat,0.010286
cite,0.007224
code,0.013367
cognat,0.013971
collat,0.060924
collect,0.065504
commentari,0.009958
commit,0.007240
common,0.004177
commun,0.004517
compar,0.010218
compil,0.008912
complet,0.009542
compos,0.019661
composit,0.014323
concept,0.004820
conduct,0.005920
confer,0.006800
confound,0.013422
conquest,0.009308
consid,0.004028
consist,0.027742
constitut,0.005454
contain,0.009911
content,0.020449
context,0.006077
continu,0.004185
contract,0.006929
contribut,0.005070
convent,0.006689
copi,0.024985
corpu,0.011690
correspondingli,0.013045
corrupt,0.008481
count,0.015757
cours,0.006021
creat,0.004225
creation,0.012576
credit,0.006685
criticis,0.010168
crucial,0.008409
d,0.005592
daray,0.022499
date,0.017321
day,0.031279
dead,0.017321
deal,0.011130
death,0.012101
dedic,0.008298
degre,0.005657
deiti,0.011054
demon,0.012223
demonstr,0.006209
denkard,0.058529
denmark,0.010133
depend,0.004558
deriv,0.005252
describ,0.004252
descript,0.006315
destroy,0.007224
destruct,0.008176
develop,0.003455
devot,0.008165
dialect,0.010162
differ,0.021974
digniti,0.012345
discov,0.006083
discuss,0.011273
diseas,0.007236
dismiss,0.009204
dispers,0.009481
dissert,0.011765
distinguish,0.006133
divid,0.010956
divin,0.064319
divis,0.005987
dk,0.119826
dnkard,0.022499
doctrin,0.008284
door,0.010803
dualist,0.013317
dure,0.007953
e,0.005598
earli,0.008319
earth,0.006529
eastern,0.006903
ecclesiast,0.012386
edit,0.006509
effort,0.005760
ehrbadistan,0.022499
emperor,0.009172
empireera,0.022499
end,0.009094
engend,0.013009
ensur,0.006833
entir,0.005394
entireti,0.011889
enumer,0.036060
epagomen,0.022499
epithet,0.014080
error,0.007908
eschatolog,0.014408
especi,0.004829
establish,0.004438
etymolog,0.008853
europ,0.005639
european,0.005180
eventu,0.005911
evil,0.010116
exactli,0.008147
examin,0.006509
exeget,0.016952
exist,0.008271
extend,0.010972
extens,0.011233
extent,0.006843
f,0.006126
faith,0.008314
fall,0.011354
fargard,0.134999
feast,0.013082
fight,0.007804
final,0.005379
finish,0.009570
fix,0.006601
flood,0.009818
fluid,0.008767
folklor,0.012487
follow,0.017788
forgeri,0.014978
form,0.013598
fourth,0.007759
fragment,0.096221
franc,0.006366
french,0.005985
friedrich,0.009096
g,0.006016
gah,0.036099
gatha,0.108299
gathic,0.040127
gener,0.012783
genit,0.014025
geograph,0.007233
gh,0.045693
given,0.008921
great,0.005012
greater,0.005601
greatli,0.007283
greek,0.006297
group,0.016047
ha,0.020759
hadokht,0.022499
haiti,0.012991
hand,0.016532
haptangha,0.021785
hereaft,0.016066
hi,0.003987
high,0.013510
histori,0.011113
historiographi,0.011287
hoffmann,0.029353
honor,0.009200
honour,0.010488
howev,0.007218
hundr,0.013514
hyacinth,0.018502
hygien,0.013009
hymn,0.078836
identifi,0.005376
ii,0.005820
import,0.015172
includ,0.012349
incomplet,0.009691
inconsist,0.010335
increasingli,0.006556
independ,0.004567
india,0.006809
indian,0.007362
indign,0.015666
individu,0.009174
influenc,0.009797
insert,0.020634
interleav,0.016260
interrupt,0.011062
introduc,0.015651
invoc,0.045561
iran,0.010088
iranian,0.011216
jean,0.009223
k,0.013485
kardo,0.022499
karl,0.008364
kayanian,0.022499
kellen,0.019273
khordeh,0.040791
khosrow,0.017915
knowledg,0.005593
known,0.007431
kseri,0.022499
kushti,0.020778
laiti,0.015830
lamb,0.013401
languag,0.067067
larg,0.003918
larger,0.005869
late,0.005148
later,0.013266
law,0.008958
lay,0.008367
learn,0.006246
legend,0.073327
lesser,0.027346
librari,0.027941
lie,0.007285
life,0.004779
line,0.010643
liter,0.008550
literatur,0.006643
littl,0.011381
liturg,0.101080
liturgi,0.042851
long,0.009655
longer,0.018396
lost,0.012922
lseri,0.021231
mahraspandan,0.022499
mainli,0.006172
mainogikhirad,0.022499
make,0.007561
mani,0.003360
manifest,0.008553
manual,0.009391
manuscript,0.050444
mark,0.005969
marriag,0.009399
master,0.015869
materi,0.026050
mazda,0.048200
mean,0.004094
meherji,0.022499
memor,0.012345
memori,0.007733
metric,0.009984
middl,0.018389
mirror,0.010038
mithra,0.018502
mix,0.007151
modif,0.008836
month,0.019706
moon,0.009682
moral,0.014875
mostli,0.006343
mumbai,0.014860
museum,0.008965
myth,0.028483
nask,0.160511
nation,0.004119
national,0.012751
natur,0.003969
navsari,0.021231
new,0.006641
night,0.009119
ninth,0.011637
nirangistan,0.022499
nomin,0.007759
nonavestan,0.022499
nonetheless,0.009299
notabl,0.011409
note,0.008573
ny,0.010758
nyayesh,0.112499
oblat,0.016952
occas,0.009247
old,0.024606
older,0.007801
oldest,0.024128
onc,0.005555
onli,0.047912
oral,0.038429
organ,0.004055
orient,0.007746
origin,0.012360
orthodoxi,0.012458
otherwis,0.007071
ownercol,0.022499
pahlavi,0.048582
parsi,0.036684
parthian,0.029571
parthianera,0.020778
particular,0.018614
partli,0.007994
passag,0.008857
patron,0.022759
penanc,0.017042
peopl,0.004099
perform,0.005226
period,0.004417
persia,0.011070
persian,0.037363
phonet,0.013971
physic,0.004864
pick,0.009677
place,0.004204
poetpriest,0.022499
point,0.008625
poor,0.007350
portion,0.022412
portray,0.009677
posit,0.004406
postth,0.019509
prais,0.019937
prayer,0.089870
prefer,0.006528
presasanian,0.022499
present,0.012788
preserv,0.021557
presid,0.006165
previous,0.006499
priest,0.050472
primari,0.022510
primarili,0.005827
primordi,0.011984
princip,0.006659
print,0.008006
produc,0.004435
prose,0.036171
provid,0.003782
pseri,0.021231
publish,0.004731
puriti,0.012069
pursishniha,0.022499
pystk,0.022499
qualiti,0.006352
quarter,0.008867
question,0.010707
quotat,0.011324
r,0.022819
rana,0.016952
rask,0.063693
rasmu,0.019273
ratavo,0.022499
realm,0.008541
receiv,0.005165
recens,0.054575
recent,0.004682
recit,0.148140
record,0.005699
recount,0.012547
refer,0.002703
reflect,0.005810
region,0.004841
regular,0.007407
rel,0.004847
relat,0.007291
relativ,0.013276
religi,0.020719
religion,0.006886
remain,0.004471
remaind,0.010033
repres,0.004435
requir,0.004256
revis,0.016159
ritual,0.010083
rivayat,0.020063
rout,0.008140
royal,0.014268
run,0.005941
s,0.011504
sacr,0.041416
saddar,0.021785
salvat,0.011679
sanskrit,0.022397
sasanian,0.016260
sasanianera,0.044999
sassanian,0.016328
scatter,0.009984
scholar,0.006866
scholarship,0.019335
scientif,0.005440
script,0.010658
scriptur,0.021758
search,0.013841
season,0.008737
second,0.008875
secondari,0.007699
section,0.026798
select,0.005879
sentenc,0.018487
separ,0.004939
servic,0.010535
set,0.004188
seven,0.014190
sevenchapt,0.022499
seventeen,0.012702
sever,0.019614
shapur,0.035334
short,0.005765
shorter,0.009625
signific,0.004803
similar,0.004493
sinc,0.007525
singl,0.005035
siroza,0.145448
sistan,0.019509
socal,0.007592
social,0.004786
sometim,0.004898
somewher,0.011525
sourc,0.009466
specifi,0.007650
spell,0.020771
spirit,0.017004
stage,0.006571
store,0.007623
stori,0.007627
structur,0.017586
style,0.007753
subdivid,0.010311
suggest,0.021071
summari,0.009055
summer,0.008705
sun,0.008347
supplement,0.009316
supposedli,0.057428
suprem,0.007705
surviv,0.038939
tahmura,0.022499
taken,0.005509
tale,0.010818
tansar,0.040127
term,0.003555
text,0.279125
th,0.043147
theolog,0.009172
theori,0.004410
therefor,0.005114
thereof,0.023130
thi,0.005629
thirti,0.010145
thread,0.024016
threequart,0.013358
thrice,0.037348
thth,0.012562
ththcenturi,0.019771
time,0.003329
today,0.016217
togeth,0.020755
topic,0.005970
tradit,0.023692
traditionalso,0.022499
transcript,0.011112
translat,0.025912
transmiss,0.037183
transmit,0.018439
travel,0.006644
treasuri,0.010000
treatis,0.018156
twentyon,0.015018
unaccept,0.012263
uncertain,0.009947
unconsci,0.011251
undertaken,0.009696
unexpect,0.010989
uniti,0.008271
univers,0.007794
unlik,0.012533
unrecord,0.016129
unusu,0.009123
usag,0.007892
use,0.006149
vairya,0.020778
valaksh,0.022499
vari,0.016641
variou,0.025702
vdavdta,0.022499
vendidad,0.237256
veri,0.004297
vers,0.011079
version,0.006582
vidvdt,0.022499
view,0.004822
vindic,0.014253
violat,0.008325
viraf,0.020395
vishtaspa,0.040791
visparad,0.067499
visperad,0.142768
vologas,0.019771
volum,0.032228
vspe,0.022499
vst,0.019056
wa,0.027768
watch,0.009593
water,0.011609
way,0.004023
wealth,0.007749
wide,0.009693
wikisourc,0.014748
winter,0.009324
wisdom,0.009963
wool,0.012317
word,0.020893
wordlong,0.021231
work,0.029511
worn,0.013045
worship,0.040789
written,0.017873
yaja,0.021785
yasht,0.177942
yasna,0.438308
yazata,0.057819
yazin,0.022499
year,0.007595
yeti,0.021231
yima,0.021785
younger,0.028047
zand,0.019056
zarathushtra,0.020395
zartushtnamah,0.022499
zend,0.019056
zoroast,0.082351
zoroastrian,0.145604
