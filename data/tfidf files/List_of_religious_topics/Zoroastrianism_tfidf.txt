aban,0.007374
abbasid,0.018177
abl,0.001876
abov,0.001902
abraham,0.003489
absenc,0.002731
abstract,0.005228
abu,0.004042
abyss,0.005441
academ,0.002225
accept,0.013056
accommod,0.003449
accord,0.017827
accordingli,0.003343
accumul,0.002850
accur,0.002531
achaemenian,0.006969
achaemenid,0.051489
achaemenidischen,0.007374
acknowledg,0.008594
act,0.006791
action,0.005476
activ,0.010376
actual,0.001901
ad,0.003791
adapt,0.002365
addit,0.004508
address,0.002237
adhan,0.006550
adher,0.018031
adit,0.006427
adjoin,0.004807
administr,0.006231
adopt,0.008200
advantag,0.002306
advent,0.003249
affili,0.003398
afghanistan,0.011434
age,0.003721
agent,0.002533
aggress,0.003310
agre,0.004479
ahriman,0.019460
ahuna,0.014435
ahura,0.192040
air,0.002282
akkadian,0.005405
akm,0.006619
albiruni,0.005098
alexand,0.011385
ali,0.003683
alien,0.003481
alleg,0.003071
allgood,0.007374
allow,0.002954
allud,0.004538
almighti,0.005441
almuqaffa,0.006776
alon,0.002540
aloof,0.005746
aloud,0.005322
alreadi,0.002164
altar,0.004951
altern,0.001909
amend,0.003132
america,0.006246
american,0.005049
americana,0.005050
amesha,0.041816
amic,0.005073
anahita,0.006694
anatolia,0.004469
ancient,0.017220
angel,0.007125
angra,0.083810
angri,0.004669
ani,0.005237
anim,0.010397
anjuman,0.007217
annex,0.003379
anniversari,0.003940
announc,0.002494
anoth,0.004286
ant,0.004031
antagon,0.004550
anthropomorph,0.004597
anticr,0.007374
antiqu,0.006148
antithesi,0.010644
anyon,0.003238
apo,0.006319
appear,0.010324
appli,0.003176
approxim,0.002024
apt,0.004834
arab,0.027440
archaeolog,0.003180
archetyp,0.004353
archibald,0.004920
archiv,0.002734
archriv,0.005888
arda,0.013239
arderbad,0.007374
ardeshir,0.007374
area,0.001475
aredvi,0.007374
argu,0.001976
armenia,0.008939
armenian,0.018075
armi,0.002555
arriv,0.002391
arsaci,0.007374
arsacid,0.017575
articul,0.003588
aryan,0.005559
ascetic,0.015781
ascrib,0.003924
ash,0.004013
asha,0.051893
asher,0.005625
asia,0.007652
asian,0.011321
aspect,0.019525
assault,0.015235
assembl,0.002319
assert,0.005104
assign,0.002668
assirt,0.007374
assist,0.002215
associ,0.005805
astral,0.005920
asura,0.006178
atar,0.006619
athen,0.004106
attempt,0.005083
attend,0.002834
attende,0.005073
attest,0.003911
attribut,0.011547
australia,0.007747
author,0.005018
automat,0.003032
avarayr,0.006969
avesta,0.108390
avestan,0.036130
avoid,0.004438
away,0.004526
azar,0.006021
azerbaijan,0.004557
azi,0.006694
ba,0.003952
babylonian,0.004038
background,0.007654
bactria,0.011972
bad,0.003047
baghdad,0.012378
bait,0.005354
bakr,0.005290
balkh,0.005721
banish,0.004748
base,0.001294
basi,0.007402
basic,0.005406
bath,0.004171
battl,0.011223
bay,0.003355
bc,0.007951
bcad,0.005746
bce,0.032874
bear,0.005560
beauti,0.003403
becam,0.011462
becaus,0.012136
becom,0.008604
befor,0.003059
begin,0.003493
beh,0.006969
behdin,0.007374
behdini,0.007374
behistun,0.006427
belief,0.038368
believ,0.011116
bell,0.003468
belong,0.002531
benefic,0.005423
benefici,0.006574
benevol,0.008904
berlin,0.003190
besid,0.009194
best,0.003948
bestknown,0.003905
better,0.002020
bhadha,0.007374
bibl,0.003592
bibliographi,0.002605
bibliotheca,0.005581
bird,0.003057
birth,0.010558
birthtodeath,0.007374
black,0.002417
blame,0.003558
bodi,0.003595
bodili,0.003914
bond,0.002558
book,0.017025
born,0.020244
bounteou,0.022124
boyc,0.060216
brahman,0.004732
branch,0.001905
brand,0.003620
break,0.004938
bridg,0.014797
briefli,0.003322
brighton,0.005952
brill,0.014172
bring,0.002226
britain,0.002523
britannica,0.003643
british,0.002000
bronz,0.003605
brother,0.003004
brought,0.004416
brown,0.003039
buddhist,0.003603
build,0.003791
bukhara,0.005648
bull,0.020670
bulliet,0.007084
bundahishn,0.013938
buri,0.003542
burn,0.012283
busi,0.002019
c,0.001714
calendar,0.003673
california,0.002748
caliph,0.036513
cambridg,0.002376
cambys,0.012638
came,0.005884
canada,0.005139
canon,0.003257
capit,0.003902
cappadocian,0.006269
captiv,0.003842
captur,0.002463
carri,0.001934
carrol,0.004544
case,0.006000
cast,0.003047
catalogu,0.008181
caucasian,0.004871
caucasu,0.017518
caus,0.001601
ce,0.013149
ceas,0.003030
celebr,0.003092
censu,0.014072
center,0.001913
centr,0.004933
central,0.011773
centuri,0.038516
ceremoni,0.013247
cf,0.008092
chao,0.010416
chaotic,0.004110
charact,0.002479
charl,0.004595
check,0.003110
children,0.002467
chinvat,0.014435
choic,0.009429
choos,0.002581
christendom,0.009743
christian,0.018601
chronicl,0.003778
chronik,0.007374
church,0.005421
circa,0.004031
citat,0.003573
citi,0.008288
citizen,0.002455
citydwel,0.006371
claim,0.005463
claimant,0.004930
clan,0.003933
clark,0.003473
clash,0.003571
class,0.001990
classic,0.001982
clergi,0.004087
climat,0.002561
close,0.001605
coastal,0.003271
coexist,0.003792
collect,0.007001
collin,0.004236
coloni,0.002481
colonist,0.004102
combin,0.003422
come,0.001716
command,0.002517
commentari,0.006918
common,0.002901
commonli,0.005978
commun,0.015690
compar,0.007099
comparison,0.002516
compendium,0.004506
compil,0.003095
complet,0.008286
compos,0.006829
composit,0.002487
compris,0.002454
concentr,0.004696
concept,0.015071
conclus,0.002673
conduct,0.002056
conflict,0.008939
conform,0.003311
confus,0.008102
conglomer,0.004481
connot,0.003966
conquer,0.003249
conquest,0.012933
conscious,0.006113
consequ,0.006121
consid,0.019592
consider,0.004033
constantin,0.004342
constitut,0.001894
construct,0.007433
contain,0.008607
contempt,0.004983
contest,0.003157
contin,0.003092
continu,0.013086
contradict,0.003205
contrari,0.003138
contriv,0.004961
control,0.001594
conveni,0.003204
convers,0.009903
convert,0.019569
copi,0.005786
copyist,0.005986
core,0.005096
coreligionist,0.012446
corpor,0.002354
corps,0.009370
corrobor,0.004700
corrupt,0.005892
cosmic,0.003671
cosmogon,0.006319
cosmogoni,0.005538
cosmolog,0.007016
couliano,0.006867
council,0.002188
count,0.002736
counteract,0.004611
counterbalanc,0.005625
countri,0.004737
coupl,0.002776
cours,0.004182
court,0.002276
cousin,0.003946
cover,0.005927
creat,0.020549
creation,0.024027
creationeven,0.007374
creationevid,0.007374
creativ,0.003100
creator,0.028773
creatur,0.003659
cremat,0.005175
crime,0.005954
critic,0.003530
cross,0.005238
crucial,0.002921
culmin,0.003179
culprit,0.005625
cultur,0.012630
current,0.003072
curtail,0.004353
custom,0.002426
cut,0.002547
cypress,0.006776
cyru,0.016217
dadestanidenig,0.007374
daena,0.029499
daeva,0.013735
dahaka,0.007374
daili,0.002806
daiti,0.007374
daldi,0.007374
damn,0.005136
damnat,0.005800
dan,0.003886
danburi,0.007084
dari,0.013239
dariu,0.010676
dark,0.003103
darken,0.005388
darknesswil,0.007374
data,0.001915
date,0.012033
daughter,0.003293
dawn,0.003797
day,0.005432
daylit,0.007374
dead,0.030084
deadli,0.004332
death,0.008406
debat,0.002234
decad,0.002181
decay,0.009898
deceit,0.005231
decid,0.002338
declar,0.002317
declin,0.011392
decre,0.003446
deed,0.029139
deem,0.003247
deepli,0.003388
defeat,0.002839
defenc,0.002921
defend,0.002684
defens,0.002633
defin,0.003293
degre,0.001965
deiti,0.015359
delight,0.004910
demigod,0.005920
demis,0.004241
demograph,0.006444
demon,0.012737
demonolog,0.005986
denkard,0.033884
denkmler,0.007374
depart,0.002104
depend,0.001583
deriv,0.003649
derogatorili,0.007084
descend,0.014522
descent,0.003442
describ,0.001477
descript,0.002193
desert,0.003412
despis,0.005038
despit,0.004069
despot,0.004816
destin,0.003358
destroy,0.005018
destruct,0.002840
determin,0.003332
detroit,0.005098
deva,0.005245
develop,0.003600
devil,0.008500
devot,0.011346
dhalla,0.014435
dharma,0.004618
dhimmi,0.017015
dialect,0.003530
diaspora,0.008696
diclofenac,0.007374
dictionari,0.011637
did,0.005344
die,0.009197
differ,0.005088
difficult,0.004186
din,0.010776
diod,0.005015
diodoru,0.005648
direct,0.004780
dirham,0.006136
disarm,0.004862
disord,0.003093
disparag,0.005073
dispers,0.003293
displac,0.006158
dispos,0.003550
dissemin,0.003737
dissent,0.004087
distinct,0.005544
distract,0.004307
distress,0.007965
diverg,0.003243
divid,0.003805
divin,0.015958
divis,0.002079
dmoz,0.003178
dn,0.005062
doctrin,0.014389
document,0.002316
doe,0.003247
dog,0.003592
domin,0.003949
dominion,0.004098
doubt,0.006194
downgrad,0.005050
draft,0.003097
draw,0.002459
druj,0.068678
dualism,0.012526
dualist,0.009252
duchesneguillemin,0.014435
dure,0.012432
duti,0.017133
dynasti,0.006240
eager,0.004401
earli,0.010115
earth,0.004536
easier,0.003093
easili,0.002475
east,0.002221
ecolog,0.010890
econom,0.004743
ed,0.008090
edict,0.008835
edit,0.004522
edmund,0.003675
educ,0.001840
effect,0.007017
eggshap,0.006486
egypt,0.003051
elabor,0.003006
elder,0.003550
element,0.005512
eliad,0.011601
elig,0.003614
eman,0.008500
embrac,0.003259
emerg,0.005554
emigr,0.007263
emit,0.003616
emperor,0.009559
emphas,0.007606
emphasi,0.002620
empir,0.022175
enclosur,0.004843
encourag,0.009498
encyclopaedia,0.018256
encyclopdia,0.003747
encyclopedia,0.004976
end,0.007898
endors,0.003419
english,0.014085
enigmat,0.005829
enjoin,0.010847
enorm,0.003111
ensnar,0.006694
ensur,0.004747
enter,0.004224
entir,0.001873
entiti,0.002451
ephedra,0.006694
epic,0.003883
epistl,0.005275
equal,0.001967
equat,0.002402
equit,0.004307
era,0.009130
error,0.002747
eschatolog,0.015014
especi,0.005032
essay,0.005599
establish,0.007709
estim,0.004066
etern,0.010957
europ,0.001959
event,0.005640
eventu,0.002053
everi,0.007231
everyth,0.005753
evid,0.011820
evil,0.073799
evolv,0.004742
exalt,0.004881
examin,0.002261
exampl,0.003881
exchang,0.002042
exclud,0.002717
exert,0.003315
exhort,0.005441
exil,0.003299
exist,0.011493
expans,0.002437
expatri,0.004591
expect,0.001985
expens,0.002580
experi,0.003553
explain,0.003889
explan,0.007629
exposur,0.009632
express,0.007487
expuls,0.004236
extant,0.003822
extent,0.002377
extern,0.001129
extinct,0.003284
extinguish,0.004816
face,0.004336
facilit,0.002604
faith,0.034658
fall,0.007888
fallen,0.003510
falsehood,0.015486
famili,0.008002
far,0.001959
fast,0.002957
fastfad,0.007374
father,0.002540
fault,0.003880
favor,0.004772
fearsom,0.006136
featur,0.003907
fell,0.002790
feminin,0.004626
ferdowsi,0.005829
festiv,0.003588
fewer,0.003063
fiction,0.003081
fictiti,0.004647
figur,0.008558
final,0.024293
finish,0.003324
firdausi,0.007374
firmli,0.003776
fit,0.002630
flat,0.003304
flatten,0.004655
flee,0.003704
fli,0.003224
float,0.003421
focus,0.002011
foe,0.005015
fold,0.003683
follow,0.011122
foltz,0.006319
food,0.002223
forc,0.009290
forefath,0.005581
forev,0.004118
form,0.017713
formal,0.001933
formerli,0.002893
formula,0.002697
fought,0.003276
foul,0.005290
foulsmel,0.006550
fourth,0.008086
fraction,0.003023
fragment,0.003038
franz,0.004079
frashokereti,0.007217
fravashi,0.028870
free,0.008823
freeman,0.003866
french,0.002079
friday,0.004469
frown,0.005245
function,0.003163
fundament,0.003956
furthermor,0.002647
furthest,0.005371
gabr,0.007374
gain,0.005638
gale,0.004853
gatha,0.062698
gathasth,0.007374
gathic,0.013938
gavri,0.007374
gayomard,0.014749
gener,0.004440
genr,0.003647
gentri,0.005004
georgia,0.003692
geti,0.006969
getig,0.014749
gheber,0.007374
gherardo,0.006694
giaour,0.006969
giorgio,0.005136
girdl,0.006178
given,0.003098
gnoli,0.007084
gnostic,0.014423
god,0.021631
godrej,0.007374
gold,0.002686
good,0.042940
goodholi,0.007374
goodversusevil,0.007374
gordon,0.003603
govern,0.005876
governmentthat,0.007374
governor,0.008506
grandson,0.004327
grant,0.002368
grave,0.003765
great,0.012188
greater,0.005837
greek,0.006563
greet,0.004544
grew,0.005163
grief,0.005073
grievou,0.006319
grolier,0.007084
group,0.008361
guardian,0.007634
gueber,0.007374
guid,0.002184
gujarat,0.010709
gujarati,0.012273
h,0.006438
ha,0.019572
half,0.002126
hallucinogen,0.005952
hand,0.003828
haoma,0.013938
happi,0.006831
harass,0.004201
harper,0.003979
harsher,0.005460
harvard,0.009055
haug,0.023433
havewala,0.007374
head,0.003988
heard,0.003673
heaven,0.014799
heavenli,0.008748
held,0.001826
hell,0.029851
hellenist,0.007854
henc,0.002410
herald,0.004342
herdsman,0.006867
heret,0.004469
heritag,0.003159
herodotu,0.013427
hi,0.024933
high,0.006257
hindu,0.003581
hinduism,0.003714
histor,0.010412
histori,0.024448
historian,0.002603
historica,0.006371
hitherto,0.004557
hold,0.001860
holi,0.013084
home,0.006618
homeland,0.003996
homi,0.006319
hope,0.005260
hoshang,0.007374
host,0.005649
hous,0.004144
household,0.003073
howev,0.005015
hukhta,0.022124
human,0.020837
humankind,0.012850
humata,0.022124
humbl,0.004920
husayn,0.011296
huvarshta,0.014749
hvarshta,0.007374
hymn,0.004564
hypostasi,0.012357
hypothes,0.003071
iberia,0.004891
ibn,0.003363
idea,0.010403
ident,0.002212
identif,0.003043
identifi,0.003735
idiom,0.004951
ignor,0.002679
ii,0.004043
iii,0.002954
ilkhan,0.005696
illeg,0.003112
illinoi,0.003781
ilmekshnoom,0.007374
imag,0.002378
imam,0.004765
imman,0.014397
immor,0.008625
immort,0.012272
impact,0.002117
imperi,0.003009
import,0.003952
imposs,0.002612
impot,0.005518
imprecis,0.004790
impregn,0.005290
incent,0.003216
includ,0.016086
inconspicu,0.005800
incorpor,0.002121
increasingli,0.002277
indentur,0.004708
independ,0.003173
india,0.040208
indian,0.010229
indic,0.003800
indict,0.004951
individu,0.014341
indoaryan,0.005800
indoiranian,0.023680
induct,0.003421
indulg,0.004700
infer,0.002968
inferior,0.003760
infidel,0.005245
inflict,0.004205
influenc,0.018717
inform,0.001535
inher,0.002960
inherit,0.005729
iniqu,0.006776
initi,0.001678
injunct,0.005027
ink,0.004591
inschriften,0.007084
inscript,0.012513
insight,0.002865
instanc,0.006276
instantli,0.004618
instinct,0.004087
instrument,0.004757
intellig,0.002410
intens,0.002565
interact,0.001927
intern,0.001353
interpret,0.004122
intoxic,0.006096
introduc,0.005436
introduct,0.004096
invad,0.009269
invoc,0.005275
involv,0.002995
ioan,0.005920
iran,0.080603
iranian,0.140259
iranica,0.018669
iranicaonlineorg,0.007374
iranwho,0.007374
iraq,0.006914
iraqi,0.004716
isbn,0.029220
islam,0.051434
islamic,0.006486
ism,0.005275
isol,0.002508
isolationist,0.010811
istakhr,0.007374
italian,0.002898
j,0.001940
jacqu,0.007167
jamasp,0.007084
jame,0.004357
jean,0.003204
jizya,0.005721
john,0.001757
judaeochristian,0.006550
judaism,0.011085
judgment,0.039287
jurist,0.004557
justic,0.002610
justif,0.003609
k,0.002342
karachi,0.005538
karapan,0.007217
karvi,0.007374
kegan,0.010246
kellen,0.013389
kept,0.002783
kerman,0.012742
kermani,0.007217
kermanshah,0.006867
key,0.001965
khan,0.004020
khawarizmi,0.007374
khawarizmian,0.007374
khorasan,0.017760
khordeh,0.007084
khosrow,0.006223
khrad,0.006969
khrafstar,0.007374
khwaday,0.007374
kindler,0.007084
king,0.014763
kingdom,0.006164
km,0.002907
knew,0.007277
knowledg,0.001942
known,0.015489
konecki,0.014749
kurd,0.011296
kurdistan,0.005625
kushti,0.007217
label,0.002779
laiti,0.005498
lake,0.003046
lament,0.004799
land,0.009774
landtax,0.007084
languag,0.013589
languagespeak,0.006619
larg,0.004083
larger,0.002038
largest,0.004266
late,0.008941
later,0.013824
latest,0.003449
law,0.007779
lay,0.002906
lead,0.006164
leader,0.002258
leadership,0.002802
leagu,0.003169
leav,0.004374
led,0.005000
left,0.008235
legend,0.018193
leiden,0.013575
lesser,0.012665
letter,0.002549
levi,0.010889
librari,0.004852
lie,0.002530
life,0.021583
lifesustain,0.006486
lifetim,0.003307
light,0.006387
like,0.008277
likewis,0.006118
lime,0.004618
link,0.001123
liter,0.002969
littl,0.009883
liturgi,0.004961
live,0.011689
lo,0.003542
local,0.013994
locat,0.005696
london,0.017397
long,0.008384
longer,0.002130
lord,0.012227
lose,0.002716
loss,0.004517
lost,0.002244
low,0.003958
loyalti,0.003742
mada,0.006427
magi,0.034177
mahrespandand,0.007374
maiden,0.010491
maidhyoimanha,0.007374
main,0.003269
mainland,0.003355
mainli,0.004288
maintain,0.001800
mainyu,0.118241
major,0.005480
make,0.003939
malandra,0.013938
male,0.002896
maltreat,0.006058
man,0.006855
manah,0.021254
manchest,0.008106
maneckji,0.007217
mani,0.024513
manicha,0.033358
manichean,0.006371
manifest,0.002971
manner,0.004984
manuch,0.007374
manuscript,0.003504
march,0.002141
margiana,0.006619
margin,0.002830
mari,0.026853
mark,0.002073
marri,0.006475
marriag,0.003265
martin,0.002733
masculin,0.004591
master,0.002756
match,0.002746
materi,0.003619
mathraspenta,0.007374
matter,0.005828
matthew,0.003744
maxim,0.002937
mazd,0.007217
mazda,0.195335
mazdaand,0.007374
mazdaism,0.020603
mazdayasna,0.014169
mazdean,0.006776
mazdeism,0.007217
mazdism,0.007374
mean,0.017067
mede,0.011777
mediaev,0.005371
median,0.016083
medici,0.004910
mediev,0.002754
medium,0.002944
meet,0.002089
melton,0.006269
member,0.001647
menog,0.014435
menogi,0.006969
mention,0.005000
merg,0.003110
mesopotamia,0.003927
mesopotamianinfluenc,0.007374
messian,0.005062
metal,0.002687
metaphor,0.003631
metronom,0.007084
middl,0.021293
middleeastern,0.006178
midst,0.004446
midth,0.003186
migrat,0.016556
mile,0.003001
millennium,0.012893
million,0.002028
mind,0.007306
minneapoli,0.004807
minnesota,0.004162
minor,0.002389
mircea,0.005625
miscreant,0.006867
miscreat,0.007374
misprint,0.006619
missionari,0.007314
mithra,0.006427
mithraic,0.013389
mitig,0.003712
mix,0.002484
mobe,0.022124
mobedyar,0.007374
model,0.001608
modern,0.009294
modernday,0.003675
modernera,0.006776
molten,0.004790
monastic,0.005322
mongol,0.004057
monoth,0.004662
moon,0.006726
moral,0.018085
moreov,0.002962
mortal,0.013806
mortar,0.004825
mosqu,0.004374
mostli,0.002203
motion,0.002606
moulton,0.006178
mountain,0.008335
muhammad,0.003709
mumbai,0.010324
muslim,0.046211
muslin,0.006371
mystic,0.003808
mytholog,0.003611
mzdzm,0.007374
namag,0.021653
narrow,0.002916
nask,0.013938
nation,0.004293
nativ,0.002669
natur,0.005514
navjot,0.007374
nd,0.004805
necessari,0.002058
necessarili,0.002528
nelson,0.003902
new,0.009228
newli,0.002717
night,0.003167
ninth,0.004042
nobil,0.004028
nobleman,0.005388
nomad,0.003989
nomin,0.005391
nonetheless,0.003230
nonmuslim,0.036951
north,0.006151
northeast,0.003678
northeastern,0.007979
northern,0.004828
notabl,0.001981
note,0.002977
noteworthi,0.004363
noth,0.002732
notion,0.004886
novel,0.002858
noxiou,0.005920
number,0.016795
numer,0.001913
nusservanji,0.007374
oblig,0.002851
obligatori,0.004843
obscur,0.003577
observ,0.001704
observableth,0.007374
obtain,0.002007
odd,0.003643
offens,0.003598
offer,0.001920
offici,0.011763
old,0.012821
older,0.008130
oldest,0.002793
omnipot,0.004871
onc,0.005789
oneself,0.004042
oneworld,0.006136
onli,0.023775
onlin,0.002260
onward,0.006547
oppos,0.017265
opposit,0.010624
oppress,0.003808
ordain,0.004716
order,0.011385
orderi,0.007374
ordinari,0.002816
origin,0.012880
orthodox,0.013910
oup,0.010920
outcom,0.002640
outmarriag,0.007374
overlord,0.005148
overritualis,0.007217
overthrew,0.004358
overthrow,0.003750
overthrown,0.004167
overview,0.002587
owe,0.003143
owner,0.002921
oxford,0.014558
p,0.003942
pack,0.003768
page,0.007595
pahlavi,0.033751
paid,0.002633
pairidaeza,0.007374
pakistan,0.007060
palac,0.007385
pantheist,0.005148
pantheon,0.004531
paradis,0.009252
parchment,0.005559
parent,0.002908
pari,0.002824
parse,0.014749
parsi,0.070086
parsisa,0.007374
parsizoroastrian,0.007374
parthian,0.015408
parthianera,0.007217
particip,0.008149
particular,0.006466
particularli,0.012677
partli,0.002776
pasargada,0.007084
pass,0.002006
past,0.004097
path,0.013092
pathfind,0.005721
patriot,0.003768
pattern,0.002203
paul,0.004489
pay,0.002411
peac,0.002490
peak,0.002833
peasantri,0.004834
pejor,0.004506
peopl,0.011390
perceiv,0.002597
peretum,0.007374
perfect,0.005770
perfectli,0.007176
perform,0.001815
perhap,0.002400
period,0.019948
permit,0.002630
persecut,0.007444
persepoli,0.013553
persia,0.023073
persian,0.058405
persianpahlavi,0.007374
persist,0.005856
persobabylonian,0.007374
person,0.005069
personif,0.005441
peter,0.002354
philolog,0.004920
philosophi,0.004397
phoenix,0.004669
phrase,0.003111
physic,0.008449
pivot,0.003848
place,0.004381
planet,0.002930
planinher,0.007374
plant,0.008968
play,0.005593
pleasur,0.003839
poem,0.003811
point,0.002996
poison,0.003609
polici,0.001911
polltax,0.013938
pollut,0.003473
polytheist,0.009944
pope,0.003631
popul,0.018374
popular,0.011085
portion,0.002595
poru,0.006427
possess,0.006962
possibl,0.004531
postul,0.003186
postzoroastrian,0.007374
power,0.007577
practic,0.026935
practis,0.007792
pray,0.004604
prayer,0.011706
precept,0.013899
prechristian,0.005354
precinct,0.005800
precis,0.004878
predestin,0.005354
prehistor,0.007159
prehistori,0.003719
preislam,0.005027
preponderantli,0.006867
prescrib,0.003616
presenc,0.004478
present,0.004442
presentday,0.016630
preserv,0.002496
press,0.006971
pressur,0.006687
pretend,0.004584
pretext,0.004940
prevail,0.003132
priest,0.028051
priesthood,0.010196
priestli,0.005216
primari,0.003909
primarili,0.002024
primordi,0.008325
princ,0.003195
princess,0.004332
princip,0.002313
principl,0.012443
prior,0.002236
probabl,0.002124
proclaim,0.006700
product,0.001543
progress,0.010091
project,0.001832
promin,0.004644
promot,0.006337
propag,0.006599
proper,0.002656
properti,0.001793
prophet,0.023321
prose,0.004188
proselyt,0.015567
protect,0.009868
protector,0.003664
protoindoeuropean,0.005479
protoindoiranian,0.019281
prototyp,0.003820
provid,0.002627
provinc,0.002737
public,0.001491
publish,0.001643
punctuat,0.004618
punish,0.006515
pure,0.002341
purg,0.004142
purif,0.004327
puriti,0.004192
purpos,0.003852
qtd,0.006619
quantit,0.002751
quantiti,0.002412
queen,0.003108
question,0.001859
quickli,0.002410
quit,0.005005
qutaiba,0.007084
qutayba,0.006619
r,0.001981
radic,0.002714
rais,0.002184
rang,0.001777
raphael,0.005062
rapidli,0.002496
rate,0.005505
ratio,0.002670
read,0.004893
readi,0.003451
realign,0.004961
reason,0.001685
receiv,0.008971
recent,0.004879
recogn,0.002013
recogniz,0.004368
record,0.015837
refer,0.007513
reform,0.004594
reformist,0.004500
refug,0.008013
refuge,0.003516
refut,0.003709
region,0.013454
regular,0.002573
reign,0.006495
reincarn,0.009295
reject,0.009261
relat,0.005065
relationship,0.001792
religi,0.031188
religio,0.005858
religion,0.133957
remain,0.015533
remiss,0.005671
renaiss,0.003105
render,0.006160
renew,0.005492
renov,0.028202
report,0.001805
repr,0.030684
repres,0.015406
reput,0.003268
request,0.002854
requir,0.004435
research,0.001523
resid,0.002488
resist,0.002433
respect,0.008930
respond,0.002533
respons,0.009774
rest,0.004400
restor,0.005312
restorationist,0.019460
restrict,0.002176
result,0.002616
resurrect,0.004167
retreat,0.003627
retriev,0.005580
return,0.003801
reunit,0.013075
reveal,0.004940
revel,0.003765
rever,0.008385
revert,0.004110
review,0.002104
revis,0.002806
reviv,0.002984
revolt,0.003419
revolut,0.002326
reward,0.006498
rewritten,0.004677
rich,0.002693
richard,0.004640
right,0.006741
righteou,0.010221
righteous,0.005479
rightli,0.005038
rise,0.005669
rite,0.004171
ritual,0.035025
rival,0.003103
rivayat,0.006969
river,0.005135
robert,0.002065
robinson,0.003851
role,0.009871
roman,0.007557
roni,0.006427
rooster,0.006319
root,0.007117
routledg,0.013387
rowley,0.005986
royal,0.002478
rst,0.006136
rta,0.006096
ruin,0.011204
rule,0.005143
run,0.004127
russel,0.003292
s,0.005328
sacr,0.014386
sacrific,0.011476
safe,0.006141
saga,0.004799
said,0.005789
salt,0.003217
salvat,0.004057
sanjan,0.007374
sanskrit,0.003889
saoshyant,0.028870
sasanidischen,0.007374
sassanian,0.017015
sassanid,0.076648
sassanidera,0.007217
satan,0.004756
savior,0.005479
saviorfigur,0.007374
saw,0.002298
say,0.004069
sayc,0.007084
scatter,0.003468
scaveng,0.004807
schema,0.004618
scholar,0.014311
scholarship,0.003358
school,0.001910
scienc,0.001579
scientif,0.003779
script,0.011107
scriptur,0.026452
se,0.003905
searcher,0.006058
season,0.003035
seat,0.002779
second,0.007707
secondari,0.002674
sect,0.003933
sectarian,0.004862
section,0.002327
seed,0.013329
seek,0.002107
seen,0.001842
select,0.002042
selfcreat,0.006096
semireligi,0.006694
sent,0.005206
separ,0.001715
seri,0.001871
seriou,0.002631
serv,0.003739
servant,0.003620
set,0.001454
settl,0.002669
seven,0.002464
sever,0.006813
shadowi,0.005920
shahrbanu,0.007374
shapur,0.012273
share,0.005490
sharpen,0.005098
sheer,0.004538
shhnme,0.007374
shia,0.004434
shirk,0.005773
shore,0.003571
shortli,0.002923
shpr,0.007374
shrine,0.009252
siculuss,0.007374
signific,0.005005
silenc,0.008376
similar,0.003121
similarli,0.002476
simpl,0.002076
simpli,0.002132
simpson,0.004379
sin,0.003717
sinc,0.011763
singularli,0.005920
siroza,0.007217
situat,0.002012
sixth,0.003470
sixti,0.003976
sky,0.003717
slave,0.003088
slow,0.002698
slowli,0.006025
small,0.001599
smell,0.004269
smerdi,0.007084
snake,0.004150
socal,0.002637
social,0.006651
societi,0.004913
sogdiana,0.006969
sole,0.002653
someon,0.003053
someth,0.002513
sometim,0.006806
son,0.012513
sort,0.002741
sought,0.005092
soul,0.026998
sourc,0.009865
south,0.006140
southern,0.002425
southwest,0.003668
southwestern,0.004079
sow,0.005245
speak,0.002506
speci,0.002403
special,0.003269
specul,0.002719
spenta,0.074546
spine,0.004423
spirit,0.032487
spiritu,0.018070
spitama,0.007217
spoken,0.003516
sport,0.003104
spread,0.002310
stanc,0.003603
stand,0.002456
standard,0.001760
start,0.001654
state,0.010293
statementsasfact,0.007374
statu,0.004366
steadi,0.003350
stem,0.002938
stolz,0.013735
stood,0.003516
stori,0.005299
strabo,0.009881
strength,0.002651
strengthen,0.002956
strife,0.004307
strong,0.003798
strongest,0.003792
stronghold,0.004255
strongli,0.002519
structur,0.001527
struggl,0.008220
studi,0.004144
subcontin,0.012084
subject,0.004988
subsequ,0.008159
substanc,0.005496
substanti,0.002426
subsum,0.004368
success,0.005200
successor,0.002936
sudra,0.007084
suffer,0.002385
suffix,0.004464
suggest,0.003659
suitabl,0.002901
sum,0.002607
sun,0.002899
sunris,0.005306
sunset,0.004951
superfici,0.004122
supersed,0.003776
support,0.003108
suppos,0.002872
suppress,0.002993
suprem,0.010706
sura,0.005581
sure,0.003581
surpris,0.003437
surround,0.002393
survey,0.002512
surviv,0.015780
suspici,0.004464
sussex,0.004930
sustain,0.002556
suzerainti,0.004930
sweetsmel,0.006867
sydney,0.004094
symbol,0.002373
syncret,0.004662
tajikistan,0.005038
taken,0.001913
tale,0.003757
tansar,0.006969
tata,0.005460
taunt,0.006096
tax,0.007369
taxsystem,0.007084
teach,0.020484
technic,0.002324
tehran,0.014981
templ,0.020732
temporari,0.003082
tenet,0.007696
term,0.012351
terminolog,0.003077
territori,0.004575
terror,0.003479
text,0.051861
textual,0.004106
th,0.031474
thcenturi,0.009041
themselv,0.003877
theolog,0.015931
theori,0.001532
thereaft,0.006835
therebi,0.002705
therefor,0.005329
thi,0.029330
thing,0.006239
think,0.002266
thoma,0.002338
thought,0.016672
thousand,0.002305
thread,0.004171
threaten,0.002972
throne,0.003687
thu,0.015279
tie,0.002549
tightli,0.004094
time,0.023130
tishtrya,0.007084
titl,0.009384
today,0.018777
togeth,0.003604
toler,0.003307
took,0.006125
total,0.005471
tower,0.007645
town,0.002684
trace,0.007519
tradit,0.024689
traditionalist,0.014450
trait,0.003097
transcend,0.003697
transcendent,0.004175
transcrib,0.004395
transcript,0.003860
transform,0.002139
transit,0.002275
translat,0.006750
transpir,0.004843
trap,0.006849
treasur,0.004053
treasuri,0.003473
treat,0.002288
tree,0.002692
trend,0.002496
tri,0.002057
tribe,0.006435
tribul,0.006178
tribut,0.003899
triumph,0.012204
troop,0.002950
troubl,0.003431
troublesom,0.004843
trust,0.002857
truth,0.019467
turn,0.009035
twentyon,0.005216
twilight,0.005202
twin,0.003845
twinship,0.006969
twofold,0.004825
typic,0.001753
u,0.003049
ugli,0.005175
ultim,0.015825
umayyad,0.019525
unbeliev,0.005441
uncertain,0.003455
uncreat,0.017760
und,0.007691
undead,0.006319
undergo,0.003030
underneath,0.004807
undershirt,0.007374
uneduc,0.005498
unesco,0.003550
unif,0.007434
uniform,0.003030
uniqu,0.002228
uniranian,0.007374
unit,0.006591
univers,0.021660
universalist,0.004940
unknown,0.002706
unlik,0.002176
unto,0.004708
unwil,0.004544
urbana,0.005952
urvan,0.021653
use,0.013884
usual,0.003145
usurp,0.009280
uzbekistan,0.004692
vairya,0.014435
valax,0.014749
valid,0.002496
valley,0.003022
valu,0.003350
variant,0.003166
variou,0.001488
vasari,0.006178
vassal,0.004353
vast,0.002624
vedic,0.013409
vegetarian,0.010124
vendidad,0.006867
vener,0.004327
ventur,0.003442
verethregna,0.007374
veri,0.004478
verlag,0.004196
version,0.002286
versu,0.009153
vex,0.006178
view,0.003350
vigor,0.003781
villag,0.002986
violat,0.008676
violent,0.003227
viraf,0.014169
virgin,0.004003
virtu,0.003311
virtual,0.002585
vishtaspa,0.014169
vision,0.011324
visperad,0.014169
vital,0.002919
vocabulari,0.003908
vohu,0.014169
von,0.002651
vulner,0.003324
w,0.006579
wa,0.068591
wade,0.005050
wadia,0.007084
wake,0.003487
wall,0.002767
war,0.001856
warburton,0.006371
warren,0.003817
water,0.018146
way,0.006988
weak,0.002596
weh,0.006867
weiner,0.005696
wellestablish,0.004390
went,0.002397
western,0.013763
westerninfluenc,0.006969
wholli,0.003803
wick,0.005027
wide,0.006734
widespread,0.002511
wield,0.004677
wife,0.003369
wildli,0.005085
william,0.008378
winter,0.003238
wisdom,0.010382
wise,0.012249
woman,0.006527
women,0.004975
word,0.019958
work,0.014095
worker,0.002433
world,0.042406
worldlyadvantag,0.007374
worldto,0.007084
worldwid,0.005303
worship,0.024795
worthi,0.012366
wrath,0.005085
wrest,0.005085
write,0.006131
writer,0.002682
written,0.006208
wrong,0.003074
xerx,0.006058
xv,0.004843
yasht,0.020603
yasna,0.066196
yazad,0.014749
yazata,0.033473
yazd,0.034339
yazdi,0.006319
yazdnism,0.006776
year,0.015829
york,0.012079
younger,0.003247
zadspram,0.007217
zaehner,0.006371
zarathushtri,0.007374
zarathushtrian,0.007374
zarathustra,0.018065
zarathustrian,0.007374
zaratosht,0.007374
zardosht,0.007374
zartosht,0.006969
zealous,0.006269
zend,0.006619
zoroast,0.234569
zoroastian,0.007374
zoroastran,0.007084
zoroastrian,0.767864
zoroastrianorg,0.007374
zurvan,0.024547
zurvanist,0.007084
zurvanit,0.013938
