ablut,0.006257
abolish,0.002934
abraham,0.003298
abroad,0.003049
abrupt,0.008616
accompani,0.008144
accomplish,0.002822
accord,0.009829
account,0.006615
achiev,0.001766
acrobat,0.005537
act,0.004815
action,0.001725
activ,0.001401
actual,0.003594
ad,0.008960
adapt,0.002236
add,0.002677
addit,0.002841
administr,0.001963
adopt,0.005813
aesthet,0.003405
afterlif,0.004295
age,0.001758
agent,0.002394
ainu,0.011765
akitsumikami,0.007387
allfath,0.007387
allianc,0.007879
allow,0.001396
alon,0.002401
alreadi,0.002045
altar,0.004680
alway,0.003776
amalgam,0.004081
amatarasu,0.007387
amaterasu,0.072467
amenominakanushi,0.036939
amenotajikaraonomikoto,0.007387
amenouzem,0.014775
america,0.009840
american,0.001591
amulet,0.028133
analog,0.002509
ancestor,0.015091
ancestr,0.007342
ancient,0.018312
ani,0.004950
anim,0.011793
animist,0.004773
anniversari,0.003724
annual,0.006471
anomali,0.003834
anoth,0.005402
antholog,0.007975
anthropologist,0.003602
anyon,0.003061
anywher,0.006874
apart,0.002573
apotheosi,0.006328
appar,0.002397
appear,0.008132
appeas,0.004578
appli,0.004503
approach,0.003232
aragami,0.007387
archaeolog,0.003006
archeolog,0.004076
archipelago,0.007129
architectur,0.005297
area,0.005579
argu,0.001868
arrang,0.002289
arriv,0.006781
arrow,0.003605
art,0.002148
ascend,0.003679
ash,0.007588
ashi,0.006022
asia,0.002411
asian,0.005350
ask,0.002321
asobi,0.014775
aspect,0.001845
assembl,0.002192
assimil,0.018328
assist,0.004187
associ,0.002743
asuka,0.011132
atsuta,0.007387
atsutan,0.013175
attach,0.005317
attempt,0.009611
attend,0.002679
attract,0.002259
attribut,0.002183
august,0.002134
authent,0.007114
avoid,0.006292
awar,0.002540
away,0.002139
awe,0.005030
background,0.002411
bad,0.008642
badli,0.003817
balanc,0.002222
bangaku,0.007387
barrier,0.002857
base,0.014683
basi,0.005247
basic,0.006814
basin,0.003336
bce,0.003107
bear,0.002628
becam,0.004643
becaus,0.003824
becom,0.010844
befor,0.005784
began,0.003381
beget,0.005510
begin,0.011557
begun,0.002692
behalf,0.003123
behold,0.005235
beholden,0.005882
belief,0.047601
believ,0.008756
bell,0.019670
belong,0.002392
beneath,0.003660
beseech,0.006075
better,0.005728
bibl,0.003395
big,0.002563
birth,0.012475
black,0.006855
bless,0.024080
blossom,0.004428
boddhisattva,0.007387
bodhidharma,0.006328
bodhisattva,0.005126
boil,0.007575
book,0.003218
born,0.009568
borrow,0.002810
bosatsu,0.007387
bow,0.016641
box,0.003174
branch,0.007203
breakdown,0.003485
bring,0.004210
brother,0.002839
buddha,0.035740
buddhism,0.041378
buddhist,0.051089
build,0.012543
built,0.006332
bulwark,0.005537
bunkotsu,0.007387
bunri,0.007387
buri,0.003348
burial,0.011547
busi,0.003817
c,0.001620
cake,0.004867
calendar,0.003472
came,0.009271
capit,0.005533
car,0.002996
carri,0.005484
case,0.004254
cast,0.002880
categori,0.002163
caught,0.003492
caus,0.001513
cave,0.003410
center,0.001808
central,0.006359
centuri,0.016019
ceremoni,0.021914
certain,0.006209
challeng,0.002025
chang,0.007671
chaplain,0.005296
character,0.002151
characterist,0.001954
charg,0.002188
charter,0.003050
chase,0.003856
chiba,0.006328
chichibu,0.006822
chief,0.007085
chiga,0.014775
child,0.008171
childbirth,0.004680
children,0.004664
china,0.002178
chines,0.017064
chinkon,0.014775
choos,0.002439
choreograph,0.005726
christian,0.006593
chronicl,0.007144
circular,0.003305
citi,0.005875
citizen,0.002320
civil,0.001945
claim,0.003443
clan,0.044619
clanbas,0.006131
clap,0.012263
classifi,0.002294
clean,0.003320
cleanli,0.009709
cleans,0.008901
clergi,0.003863
click,0.004400
close,0.012140
cocreat,0.005384
code,0.010973
codif,0.018643
codifi,0.007448
coexist,0.007169
cohes,0.003679
collaps,0.002498
collect,0.008272
colon,0.003009
combin,0.001617
come,0.011355
commemor,0.007595
common,0.009600
commonli,0.001883
commot,0.006131
commun,0.004449
compar,0.003355
compass,0.003654
compil,0.005852
complet,0.001566
complex,0.008298
compon,0.001953
compound,0.005111
concept,0.007914
concern,0.001592
conduct,0.003888
conduit,0.009174
confederaci,0.004435
confer,0.002232
configur,0.003133
confucian,0.024749
conglomer,0.004236
congreg,0.004100
connect,0.005602
conquer,0.003071
consent,0.003481
consequ,0.001928
consid,0.006614
consist,0.001518
constantli,0.003162
constitut,0.001791
context,0.003990
contin,0.005845
continu,0.012369
contract,0.002275
contradictori,0.004004
contribut,0.001664
control,0.006028
convers,0.002340
convert,0.002312
convinc,0.003019
core,0.002408
corps,0.004428
correct,0.002284
cosmolog,0.003316
count,0.002587
countri,0.005971
countrysid,0.011154
courag,0.004230
cours,0.003953
court,0.012909
creat,0.013874
creation,0.012388
crimin,0.002873
crop,0.005608
crossbar,0.005840
crucial,0.002761
culmin,0.003005
cult,0.003706
cultiv,0.002897
cultur,0.025583
cup,0.007615
current,0.004356
custom,0.002293
cycl,0.006975
daij,0.014775
daijingu,0.007387
daikagura,0.014775
daikyin,0.007387
daili,0.010611
dainichi,0.020090
danc,0.069118
dancer,0.009973
daruma,0.014775
databas,0.002653
date,0.007583
day,0.006846
dazaifu,0.006971
dead,0.019906
deal,0.003654
death,0.025827
debat,0.002111
decad,0.002061
deceas,0.008053
decid,0.002210
declar,0.006572
declin,0.004307
decreas,0.004467
dedic,0.027247
deed,0.007869
deepen,0.004076
defin,0.009340
defunct,0.004009
deiti,0.025407
delin,0.003896
demis,0.004009
deni,0.002629
denot,0.002767
depart,0.011933
depend,0.004490
deposit,0.002685
descend,0.008236
describ,0.011169
descript,0.004147
design,0.004998
desir,0.004269
destroy,0.002372
develop,0.018151
devic,0.002535
devot,0.002681
did,0.005051
die,0.008693
differ,0.006012
difficult,0.003956
dilig,0.004544
dimens,0.002609
dip,0.004512
dipper,0.040181
direct,0.007531
directli,0.001822
disorgan,0.004819
disput,0.004563
dissolv,0.002817
distinct,0.001747
distinguish,0.010069
divers,0.002181
divid,0.003597
divin,0.033187
divis,0.001966
dmoz,0.003004
doctrin,0.005440
document,0.002189
doe,0.004604
dog,0.003395
dogma,0.007755
doll,0.009810
domest,0.002350
domin,0.003733
donat,0.006795
door,0.003547
dorei,0.007387
dramat,0.005123
drawn,0.002677
dress,0.003768
drink,0.006727
drip,0.005126
drive,0.002437
drum,0.008616
drummer,0.005926
dualiti,0.003892
dure,0.018280
dwell,0.004013
dynam,0.002126
ear,0.003624
earli,0.008195
earlier,0.002017
earliest,0.004565
earnest,0.004308
earthenwar,0.005926
earthli,0.004393
east,0.002099
editori,0.003892
edo,0.015184
educ,0.001739
effect,0.001326
elabor,0.002841
eleg,0.004071
element,0.010421
eleventh,0.004242
elysiumlik,0.007387
ema,0.005658
embodi,0.003157
emperor,0.054214
emphasi,0.002477
emphasis,0.003679
empir,0.001905
employ,0.001811
empress,0.018178
encourag,0.002244
encyclopedia,0.002352
end,0.007465
energ,0.005143
energi,0.003823
enforc,0.002560
english,0.009510
enshrin,0.026838
enter,0.001996
entic,0.004830
entranc,0.007690
era,0.004315
esoter,0.004176
especi,0.007929
essenc,0.012339
establish,0.011659
estim,0.001921
et,0.002347
ethic,0.002513
ethnic,0.005389
event,0.010663
eventu,0.003882
everyth,0.002719
evid,0.001862
evidenc,0.003845
evil,0.006643
evolv,0.002241
exampl,0.003669
exclus,0.004666
exhibit,0.002578
exist,0.009506
explain,0.001838
explan,0.007211
explicitli,0.005560
exposur,0.003035
express,0.010616
extens,0.001844
extent,0.002247
extern,0.002135
extrem,0.001958
eye,0.010920
facilit,0.002462
faction,0.003225
faith,0.005460
faithheal,0.006971
famili,0.041602
famou,0.002256
fan,0.003841
farm,0.008174
fascin,0.004071
fascinan,0.006405
fear,0.002562
femal,0.010967
fenc,0.004544
festiv,0.033916
feudal,0.007055
fewer,0.002896
fight,0.002562
figur,0.002022
financi,0.001949
fish,0.002547
fix,0.002167
flourish,0.006011
focu,0.002003
focus,0.005703
fold,0.003481
folk,0.017651
follow,0.010513
food,0.002101
forc,0.004391
foreign,0.005703
form,0.010046
formal,0.009136
format,0.001991
forth,0.002968
fortun,0.013250
foundat,0.003808
founder,0.005119
fragmentari,0.004944
frequent,0.012317
fridel,0.007387
fruit,0.002893
fujiwara,0.005973
fulfil,0.002953
function,0.002990
fundament,0.001870
funer,0.015985
furi,0.005161
fusokyo,0.007387
futsunushi,0.007387
gain,0.007106
galleri,0.003755
garner,0.004071
gassan,0.006971
gate,0.010605
gather,0.002545
gave,0.004249
gaya,0.005726
genealog,0.003938
gener,0.009444
genesi,0.003674
genmei,0.007387
gift,0.003296
given,0.004393
glass,0.003219
gloomi,0.005510
goal,0.002008
god,0.058786
goddess,0.032880
godshelf,0.007387
gohan,0.007387
good,0.012988
govern,0.015275
governmentown,0.004520
gradual,0.002347
grand,0.011346
gratitud,0.004680
great,0.009875
greek,0.002067
greet,0.004295
grew,0.002440
ground,0.014874
groundbreak,0.004160
group,0.007903
grove,0.004236
growth,0.001848
grudg,0.010966
guardian,0.007216
gyogi,0.029551
gyoki,0.007387
ha,0.012658
hachiman,0.029551
hachimannomikoto,0.007387
hade,0.005926
haiden,0.007387
hakuho,0.014775
half,0.004020
hall,0.011082
hamaya,0.007387
hand,0.019903
handl,0.005310
hara,0.012514
harai,0.007387
hare,0.004465
harm,0.002815
harvest,0.012949
head,0.011311
heal,0.003579
health,0.002035
heart,0.008041
heaven,0.003497
heavenli,0.012404
heian,0.027980
heiden,0.007387
heijki,0.006971
held,0.008632
help,0.007993
heritag,0.002986
hewhoinvit,0.007387
hi,0.010474
hid,0.005126
hierarchi,0.002975
high,0.002957
highli,0.001922
hikawa,0.007387
hindu,0.003385
hirata,0.012811
hiroshima,0.004730
histor,0.008202
histori,0.015812
hitogami,0.007387
hitorigami,0.007387
hokkaido,0.012811
hold,0.007035
holiday,0.006874
home,0.010426
honcho,0.007387
honden,0.022163
honesti,0.004450
honji,0.007387
honor,0.003020
honorari,0.003764
host,0.002670
hous,0.009794
household,0.011619
howev,0.015407
hub,0.003790
human,0.002813
humbl,0.004651
hundr,0.002218
huntergather,0.004076
identifi,0.008826
ii,0.001911
ill,0.002835
illustr,0.002490
imag,0.004497
imi,0.006131
immigr,0.002906
impact,0.002001
imperi,0.105258
import,0.007472
imposit,0.004197
impress,0.005961
impur,0.029736
inaccur,0.003958
inanim,0.004366
incant,0.005726
incipi,0.004867
includ,0.011151
incom,0.002236
incorrectli,0.003950
increas,0.004212
independ,0.001499
indian,0.002417
indic,0.001796
indigen,0.005823
individu,0.001506
induc,0.002891
influenc,0.022518
influenti,0.002348
inform,0.001451
ingrain,0.005255
injur,0.003703
inner,0.002935
innermost,0.005015
innov,0.002529
inou,0.006022
inscrib,0.004488
insid,0.009796
inspir,0.002416
instabl,0.003157
instal,0.002881
instead,0.001710
institut,0.006181
instruct,0.005498
instrument,0.004497
intend,0.002260
interact,0.001822
interchang,0.003342
interfac,0.003179
intern,0.002559
interpret,0.003896
interrel,0.007129
introduct,0.005807
inuhariko,0.007387
invad,0.002920
invok,0.003506
involv,0.002831
ise,0.059337
islam,0.004861
island,0.017315
isonokami,0.007387
issu,0.004701
ita,0.006257
item,0.002678
itsukushima,0.007387
iwashimizu,0.007387
izanagi,0.033484
izanaginomikoto,0.022163
izanami,0.026787
izanaminomikoto,0.022163
izumo,0.064378
januari,0.002084
japan,0.079692
japanes,0.128020
jason,0.004473
jewel,0.004421
jichinsai,0.007387
jikkokyo,0.007387
jind,0.007387
jing,0.014288
jingikan,0.007153
jingu,0.022163
jinja,0.025029
jinjashint,0.007387
jinn,0.005510
jit,0.005596
jmon,0.025029
join,0.002172
jomon,0.019475
josephson,0.005317
jump,0.003371
just,0.001702
kagura,0.221749
kagutsuchi,0.007387
kamakura,0.005840
kami,0.348860
kamidana,0.036939
kamigakari,0.007153
kamimusubi,0.007387
kaminomichi,0.014775
kamu,0.007387
kamui,0.006971
kanagawa,0.006971
kanji,0.005926
kanmu,0.006696
kannagara,0.022163
kannushi,0.007387
karmic,0.005408
kasuga,0.007387
katori,0.006696
ke,0.005001
kegar,0.014775
kept,0.005262
kichufuda,0.007387
kill,0.004821
kind,0.001945
kingdom,0.001942
kiyom,0.007387
kkai,0.012045
kkyo,0.007387
kmei,0.006971
know,0.002212
knowledg,0.001836
known,0.004880
koden,0.007387
kofun,0.012983
kojiki,0.073579
kokugaku,0.032459
kokugakuin,0.007153
konkokyo,0.014306
korea,0.005894
koshint,0.014306
kotoamatsukami,0.007387
kotsug,0.007387
kreiden,0.007387
kshitsushint,0.007387
kumano,0.007153
kuninotokotachi,0.007387
kuninotokotachinokami,0.007387
kunitokotachinokami,0.007387
kura,0.005626
kurozumikyo,0.007387
kusanagi,0.006696
kyhashint,0.007387
kyoto,0.018016
lacquerwar,0.006131
lamin,0.005161
land,0.011086
languag,0.009175
larg,0.010293
larger,0.003854
largest,0.002016
later,0.005807
law,0.007353
lay,0.005494
lead,0.002913
leader,0.008539
leav,0.008270
led,0.003151
left,0.013623
legal,0.002014
legend,0.017197
legendari,0.004062
legitimaci,0.007473
lend,0.003136
level,0.001459
lie,0.002392
life,0.004708
lifeblood,0.005882
light,0.002012
like,0.006520
limit,0.001490
line,0.003494
lineag,0.007227
link,0.003184
lion,0.015203
list,0.006787
lit,0.004067
liter,0.008422
liturgi,0.004690
live,0.012628
local,0.011574
locat,0.001795
long,0.004755
longer,0.002013
lord,0.002889
lore,0.004773
lose,0.005135
lost,0.002121
lot,0.003006
low,0.001871
loyalti,0.003537
luck,0.008705
lunar,0.007770
ma,0.003083
magic,0.003403
main,0.006181
mainland,0.019028
maintain,0.001702
major,0.003885
make,0.003724
makoto,0.005763
male,0.013691
mandat,0.003041
mani,0.015447
manifest,0.016850
manysh,0.006587
mark,0.001960
marriag,0.003086
mask,0.011611
master,0.002605
match,0.002596
matsurifocus,0.007387
matter,0.003672
mean,0.020166
mediev,0.002603
meiji,0.057690
member,0.003115
membership,0.002688
memori,0.002539
mention,0.002363
messag,0.003033
messian,0.004784
metallurgi,0.003975
metalthat,0.007387
metalwork,0.004623
method,0.001511
michi,0.006022
michizan,0.014775
mid,0.004861
mie,0.011020
migrat,0.005216
miko,0.034856
mikoto,0.007153
million,0.001917
mind,0.004604
mindset,0.004700
mingl,0.005161
ministri,0.010499
minzokushint,0.007387
mirror,0.003296
misogi,0.044326
misogikyo,0.007387
mitakekyo,0.007387
miwa,0.007153
mixtur,0.003052
miyagi,0.020090
miyaku,0.007387
mobil,0.002593
mochi,0.007387
modern,0.013178
modernday,0.003474
modest,0.003564
monetari,0.002616
monk,0.007221
monmu,0.006971
mono,0.005317
mononobenakatomi,0.007387
month,0.002156
moon,0.003179
moral,0.002442
moreov,0.005601
mori,0.009123
motoori,0.006405
mound,0.004561
mountain,0.010505
mourn,0.004807
mourner,0.005926
mouth,0.010383
movement,0.003366
multipl,0.003755
multitud,0.003852
music,0.008048
myriad,0.008362
mysteri,0.003222
mysterium,0.005840
myth,0.018705
mythic,0.004067
mytholog,0.027313
nagoya,0.005840
nakatomi,0.007153
nanda,0.005126
naniwa,0.007153
nara,0.047479
nation,0.012174
nationbuild,0.005296
nationwid,0.003709
nativ,0.010092
natur,0.019548
near,0.002007
nearli,0.002126
necessari,0.003890
need,0.007337
neg,0.002116
neighbor,0.002753
netherworld,0.006971
new,0.018537
nihon,0.044768
nihongi,0.027290
nihonshoki,0.013942
niinam,0.007387
nikk,0.013942
ningensengen,0.007387
noisi,0.004544
nomin,0.002547
nonjapanes,0.006257
norinaga,0.006405
normal,0.003891
north,0.005814
northeastern,0.003771
northern,0.002282
nose,0.004095
notabl,0.001873
note,0.001407
notion,0.002309
number,0.004884
numer,0.005425
nun,0.004623
nyorai,0.007387
oath,0.008331
object,0.011681
observ,0.001611
occas,0.006072
occasion,0.002753
occupi,0.002447
occur,0.003101
offer,0.012706
offic,0.003929
offici,0.007413
offspr,0.003662
ofuda,0.014775
ojin,0.007387
okami,0.014775
okuninushi,0.007387
old,0.006059
oldest,0.007922
omairi,0.014775
omamori,0.014775
omikami,0.014775
omikuji,0.007387
omoikan,0.007387
omotokyo,0.007387
onethird,0.003709
onli,0.005618
ontakekyo,0.007387
open,0.003342
oppos,0.004080
opposit,0.002008
oral,0.003154
ordain,0.004457
order,0.005381
ordinari,0.007986
organ,0.009320
organis,0.004622
origin,0.017587
osaka,0.005216
ostens,0.003954
otto,0.003439
outcom,0.004991
outsid,0.016760
overse,0.003640
overshadow,0.004614
overt,0.004473
oyashirokyo,0.007387
pacif,0.008488
pacifi,0.004700
paekch,0.014775
paint,0.006200
palac,0.017452
palm,0.003804
pantheon,0.004283
paper,0.008057
parallel,0.002529
parishion,0.005973
particip,0.003851
particular,0.003056
particularli,0.003423
partnership,0.003072
pass,0.001896
past,0.001936
path,0.007425
pattern,0.004165
peac,0.002354
peak,0.002678
peninsula,0.003247
peopl,0.022880
percentag,0.002720
perfect,0.002727
perform,0.030890
perhap,0.004538
period,0.050767
persist,0.002767
person,0.011181
personalprotect,0.007387
phenomena,0.004661
phenomenon,0.002514
philosoph,0.002142
philosophi,0.002078
phonet,0.004587
physic,0.003194
pictur,0.002732
pinyin,0.005030
placat,0.005457
place,0.020708
placement,0.003831
plan,0.001848
plaqu,0.004720
play,0.003525
plural,0.006279
poetri,0.003405
point,0.002832
pole,0.006763
polici,0.003613
polit,0.004596
pollut,0.006567
popul,0.001736
popular,0.003493
posit,0.001446
possess,0.017549
possibl,0.007138
postwar,0.009960
pot,0.004283
pouch,0.005197
pour,0.011022
power,0.010027
practic,0.046428
practition,0.008813
pray,0.004352
prayer,0.025820
preced,0.002652
precept,0.004379
precinct,0.005483
predat,0.003107
prefectur,0.044654
presenc,0.002116
present,0.002799
presentday,0.003144
press,0.001647
prevail,0.002961
prevent,0.002001
prewrit,0.006696
priest,0.056346
priestess,0.005408
primal,0.009302
primarili,0.001913
princ,0.003020
princess,0.004095
princip,0.002186
principl,0.001680
prior,0.004228
privat,0.002007
process,0.005293
profess,0.002925
profound,0.003193
profoundli,0.008106
project,0.001732
prolifer,0.006892
promis,0.002630
promot,0.001996
propaganda,0.003742
proper,0.002511
proport,0.002314
protect,0.013059
provid,0.002484
psycholog,0.002386
public,0.009868
publicli,0.002995
pupil,0.003651
pure,0.002213
purif,0.040907
purifi,0.007967
puriti,0.011889
purpos,0.003641
quarter,0.002911
quiet,0.004100
quietli,0.004504
quot,0.002928
quran,0.004058
rapid,0.002425
reactiv,0.003344
read,0.001541
real,0.003685
realm,0.002804
reassert,0.004414
rebirth,0.004124
reborn,0.005015
receiv,0.001696
recent,0.003074
recipi,0.006853
recit,0.008106
recognit,0.002433
reconstruct,0.002782
record,0.018713
recreat,0.003428
red,0.002460
reed,0.004219
refer,0.007102
referenc,0.003814
refin,0.002784
reflect,0.001907
reform,0.004343
regard,0.001703
regardless,0.002683
region,0.001589
regist,0.002752
regul,0.004203
regular,0.004864
reign,0.003069
reincarn,0.004393
reinforc,0.003023
rejuven,0.004867
rel,0.003183
relat,0.009576
relationship,0.005083
relianc,0.003559
religi,0.027213
religion,0.052006
religios,0.004905
remain,0.002936
remov,0.002049
renew,0.005192
repeat,0.002562
replac,0.003618
repurifi,0.007387
requir,0.004192
rescript,0.006022
resembl,0.002800
reservoir,0.003996
resid,0.002352
resist,0.002300
respect,0.006753
respons,0.001539
restor,0.015064
result,0.001236
resurrect,0.003938
retain,0.002334
return,0.005390
reveng,0.004326
rever,0.003963
revert,0.003885
reviv,0.005642
rhythm,0.004031
rice,0.023438
ricepaddi,0.007387
right,0.007965
rikkokushi,0.006822
ring,0.003006
rise,0.003573
rite,0.051257
ritsuri,0.006971
ritual,0.105946
ritualist,0.005109
rival,0.002933
river,0.012136
rock,0.008340
role,0.006220
root,0.002242
rope,0.004283
round,0.005624
rudolf,0.003648
rule,0.008102
ruler,0.002966
run,0.003901
ryukyuan,0.006491
s,0.001259
sacr,0.044197
sacrific,0.003616
sada,0.006696
said,0.003648
saitama,0.013645
sakaki,0.007387
sake,0.003709
saki,0.006696
sakurai,0.005926
salt,0.003041
sanctuari,0.017831
sanshin,0.007387
sapporo,0.006022
sato,0.005763
saw,0.006517
sceneri,0.005143
scholar,0.018036
scholarship,0.003174
school,0.005418
scoop,0.005483
scriptur,0.003572
season,0.011475
seat,0.002627
second,0.004371
sect,0.055774
sectarian,0.004596
secular,0.003072
secur,0.001886
seed,0.003150
seek,0.001992
seen,0.001741
selfconsci,0.004443
sell,0.002511
send,0.002776
sendai,0.012263
sens,0.003653
sent,0.002460
separ,0.011353
seri,0.001768
servic,0.001729
set,0.005501
settlement,0.005181
setup,0.004155
seven,0.004659
sever,0.005152
shake,0.004480
shama,0.006022
shaman,0.013285
shape,0.004232
share,0.003459
shendao,0.007387
shewhoisinvit,0.007387
shg,0.006257
shh,0.005973
shhashint,0.007387
shift,0.002151
shiman,0.006971
shin,0.010592
shinbutsu,0.013942
shind,0.007153
shinden,0.007387
shindo,0.007387
shinno,0.007387
shinrikyo,0.007153
shinsen,0.007387
shinshki,0.006587
shinshukyo,0.007387
shint,0.006587
shintai,0.007387
shinto,0.606855
shintoism,0.012656
shintoist,0.006822
shio,0.006822
shiogama,0.007387
ship,0.002519
shishi,0.036939
shizum,0.014775
shmu,0.006971
shn,0.006192
shock,0.003035
shoe,0.004100
shogun,0.005109
shoin,0.013942
shoinjinja,0.007387
shoken,0.007387
shoki,0.049536
shoku,0.014306
shotoku,0.006587
showi,0.006257
shown,0.002140
shrine,0.472263
shtki,0.006822
shugend,0.006971
shugenno,0.007387
shuseiha,0.007387
shushin,0.007387
shwa,0.012656
sign,0.002034
significantli,0.004557
silent,0.003679
similar,0.005901
simpli,0.002016
sinc,0.008648
sincer,0.009246
sing,0.008181
singular,0.006511
site,0.004162
siteshrin,0.007387
slow,0.002550
small,0.009071
social,0.003143
societi,0.003096
soga,0.013393
sold,0.002609
solid,0.002655
solstic,0.005235
someth,0.002375
sometim,0.001608
song,0.009830
sophist,0.002842
soul,0.009570
sound,0.002601
sourc,0.003108
south,0.003869
southern,0.002293
space,0.009524
spear,0.004414
special,0.004635
specif,0.001478
speed,0.002526
spirit,0.047459
spiritu,0.019928
spit,0.005161
spoke,0.003410
sponsorship,0.004488
spous,0.004544
spread,0.002183
sprinkl,0.005384
stabil,0.002304
staff,0.002912
stage,0.002157
stand,0.002322
standard,0.001664
star,0.005480
start,0.004692
state,0.014054
statu,0.006190
stem,0.002777
step,0.006285
stir,0.004372
stone,0.002759
stori,0.007513
storm,0.003461
strengthen,0.005589
structur,0.010105
student,0.002103
studi,0.007834
style,0.010183
subsequ,0.003856
substitut,0.002745
subtl,0.003654
succeed,0.002525
success,0.001638
successor,0.002775
sugawara,0.013175
suijaku,0.007387
suiko,0.006971
suit,0.002872
summon,0.004333
sun,0.024668
sunni,0.003900
supernatur,0.003589
support,0.007345
suprem,0.002530
survey,0.004749
susanoo,0.014775
sway,0.004134
swish,0.007387
sword,0.008097
symbol,0.011215
syncrat,0.006971
syncret,0.008814
systemat,0.002360
systematis,0.005255
t,0.002149
taboo,0.004450
tadashii,0.007387
taih,0.018394
taiho,0.014775
taikyo,0.007387
taiseikyo,0.007387
taisha,0.014306
taisho,0.006257
taiwan,0.003476
takamimusubi,0.007387
taken,0.003618
tale,0.003552
talisman,0.006022
talismansmad,0.007387
tama,0.022150
tamagushi,0.007387
taoism,0.012675
taoist,0.009210
tdaiji,0.006587
teach,0.012102
tear,0.003963
technolog,0.003562
teeth,0.003888
tell,0.002783
temizu,0.014775
templ,0.013064
temporarili,0.003405
tend,0.004057
tenjin,0.007387
tenmang,0.007387
tenmu,0.006971
tenp,0.006971
tenri,0.006971
tenrikyo,0.011853
term,0.008172
testifi,0.004214
text,0.002131
textil,0.003340
th,0.018417
theme,0.002736
themselv,0.005498
theocraci,0.005161
theolog,0.015059
theologian,0.003654
theori,0.002896
therefor,0.001679
thi,0.040663
thing,0.009830
thirteen,0.003706
thought,0.008755
thousand,0.004357
thth,0.004124
thu,0.001444
time,0.017491
tochigi,0.007387
today,0.007099
togeth,0.001703
tohoku,0.006022
tokugawa,0.005093
tokyo,0.023492
took,0.003860
torii,0.027884
torimono,0.029551
touch,0.003261
town,0.002537
trade,0.008936
trader,0.003041
tradit,0.014002
tranc,0.005109
transfer,0.004228
translat,0.006381
treasur,0.003831
treat,0.004327
tree,0.012725
tremendum,0.006405
tri,0.003889
tribal,0.003446
tribe,0.003041
trick,0.004129
tshg,0.014775
tsubaki,0.022163
tsukiyomi,0.007387
tsukuyomi,0.007387
tsurugaoka,0.007387
turn,0.003416
twelv,0.006518
twenti,0.003039
twice,0.005889
type,0.013369
typic,0.008288
uji,0.012983
ujigami,0.014775
ujiko,0.014775
umashiashikabihikoji,0.007387
unclean,0.005566
underground,0.003424
understand,0.001677
undertaken,0.006367
undevelop,0.004536
unifi,0.012931
uninterest,0.004854
uniqu,0.002106
univers,0.006398
unlik,0.004115
unmask,0.005692
unpopular,0.003927
unsuccess,0.003177
unusu,0.005991
upright,0.004400
upset,0.004176
urami,0.007153
usa,0.002758
usag,0.002591
use,0.013124
usual,0.011894
uta,0.005692
util,0.002235
utmost,0.004680
valu,0.001583
vari,0.001821
variat,0.004632
varieti,0.001855
variou,0.008439
veget,0.002949
veri,0.007055
vertic,0.003196
viarocana,0.007387
view,0.006334
visit,0.014894
visitor,0.006759
vital,0.002759
vocal,0.011579
wa,0.045588
wakayama,0.006971
want,0.004562
war,0.014041
ward,0.003781
warfar,0.003133
wash,0.011600
water,0.022871
waterfal,0.012512
way,0.006605
weaken,0.003094
wear,0.006696
weekli,0.003665
welcom,0.003410
went,0.002266
west,0.004243
western,0.001858
whenev,0.003476
wide,0.001591
wife,0.003185
wine,0.003476
winter,0.003061
wish,0.010927
women,0.002351
wonder,0.003579
wood,0.002851
wooden,0.003967
word,0.010290
world,0.010021
worship,0.050224
worshipp,0.005596
worthi,0.003896
write,0.003863
written,0.015649
wrong,0.005812
wwii,0.004435
yamabushi,0.022163
yamagata,0.006696
yamato,0.027287
yang,0.008153
yaoyorozu,0.007387
yasukuni,0.012983
yawata,0.007387
yayoi,0.018394
year,0.006234
yin,0.009008
yomi,0.022163
yominokuni,0.007387
yorishiro,0.007387
yoshida,0.005692
yoshimi,0.014775
yourselv,0.005726
youth,0.003116
yoyogi,0.007387
yudat,0.007387
yushima,0.006971
zka,0.007387
zodiac,0.005126
