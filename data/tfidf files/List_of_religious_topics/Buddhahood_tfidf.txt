abhaya,0.015917
abl,0.004647
abov,0.004710
absolut,0.012481
accord,0.007358
act,0.004205
actual,0.004709
acuiti,0.013988
addit,0.003722
advoc,0.006316
aeon,0.015004
aggaa,0.018265
aliv,0.009193
alway,0.004947
amatassadata,0.018741
amitabha,0.016223
andrew,0.007472
anger,0.009604
ani,0.006485
anoth,0.003538
answer,0.006551
anuradha,0.016784
anuradhawhen,0.018741
anuttara,0.017875
appeal,0.006852
arah,0.016065
arhat,0.095506
arhatship,0.017546
art,0.005628
asana,0.035751
ascet,0.023771
ascrib,0.009718
ask,0.018247
aspect,0.009671
associ,0.003594
attach,0.006966
attain,0.067033
attainmentb,0.018741
attribut,0.005719
avaghoa,0.017260
avail,0.004497
avalokiteshvara,0.017546
avers,0.010693
awaken,0.052078
base,0.003206
bd,0.012720
bearer,0.011973
becaus,0.003339
becom,0.003551
belief,0.005939
believ,0.004588
benefit,0.005383
bestow,0.011492
bhagavat,0.016580
billion,0.006301
bless,0.021031
bliss,0.012236
blossom,0.011603
bodhisattva,0.067162
bodi,0.022260
born,0.006267
bound,0.006638
branch,0.004718
bringer,0.016784
broad,0.005959
broader,0.007397
bud,0.011145
budai,0.017546
budd,0.015528
buddha,0.821980
buddhabhagavan,0.018741
buddhabhva,0.018741
buddhahood,0.072546
buddhalokanatha,0.018741
buddhanatur,0.014661
buddhanet,0.017260
buddhatta,0.018741
buddhatva,0.018741
buddhism,0.063241
buddhist,0.098161
came,0.004858
canon,0.048409
caravan,0.012262
celesti,0.009419
central,0.004165
centuri,0.003815
certain,0.004067
cessat,0.023399
cf,0.010020
champion,0.009224
chant,0.011370
character,0.005635
characterist,0.015365
chi,0.010644
china,0.005708
chines,0.006387
chjp,0.018741
claim,0.004510
class,0.004928
come,0.004250
commentari,0.008566
common,0.017967
commonli,0.009870
compassion,0.012507
complet,0.012313
concept,0.004147
concern,0.004172
condit,0.004158
conduct,0.005093
conflict,0.005535
confus,0.006689
consid,0.010397
consol,0.011822
contain,0.008527
contempl,0.010258
contemporan,0.011070
contrari,0.015543
contrast,0.005020
countless,0.011041
coupl,0.006877
current,0.003804
daili,0.006950
daizki,0.018741
dark,0.007685
death,0.026026
declar,0.005740
deeproot,0.015779
defil,0.014107
deni,0.006889
denot,0.014503
depend,0.011764
depict,0.014826
describ,0.021948
design,0.008730
desir,0.011186
despit,0.005039
destroy,0.006214
deva,0.025983
devamanuya,0.018265
dhamma,0.056932
dhammakaya,0.016784
dhammasami,0.018741
dhammika,0.017009
dharma,0.125834
dharmakya,0.016394
dharmaraja,0.017009
differ,0.006301
digha,0.015412
direct,0.007893
discipl,0.009932
dispel,0.013303
disput,0.005978
divin,0.007904
doctrin,0.035636
doe,0.024128
dream,0.008580
dukkha,0.014047
dure,0.003421
earli,0.007157
earlob,0.017875
earthli,0.011510
edit,0.005600
effort,0.009910
eighti,0.010693
ekavyvahrika,0.017875
element,0.004550
elimin,0.006187
emaci,0.015412
emissari,0.013261
emphas,0.018839
end,0.003912
enlighten,0.039065
epithet,0.012113
equat,0.005951
equip,0.006397
equival,0.005763
essenti,0.004974
everi,0.013432
evid,0.004879
exampl,0.006409
excel,0.007740
exist,0.021348
experi,0.004400
explan,0.006298
explicit,0.007813
expound,0.010632
extern,0.002797
extrem,0.005132
fabl,0.011641
fact,0.009086
faith,0.007153
fallibl,0.012422
far,0.004851
fear,0.006713
fearless,0.013988
figur,0.010598
final,0.004628
follow,0.009182
form,0.005849
fortun,0.008679
fount,0.015198
frequent,0.016136
friend,0.007599
fulfil,0.007737
fulli,0.005768
fundament,0.004899
futur,0.004660
gautama,0.049691
given,0.003837
goal,0.015788
god,0.053573
gokulika,0.018741
gone,0.025471
good,0.004253
got,0.017263
grant,0.005865
great,0.021561
greatest,0.006733
group,0.003451
grove,0.011100
grow,0.004906
grown,0.007368
guang,0.015099
guid,0.005409
ha,0.010205
half,0.005266
handgestur,0.037483
happen,0.006047
hard,0.006279
head,0.004939
healer,0.012626
held,0.013571
hero,0.009074
hi,0.058320
highest,0.005859
hinayana,0.015650
histor,0.008595
hnh,0.015412
hold,0.009217
honor,0.007914
hotei,0.017546
howev,0.006210
hsuan,0.016223
hua,0.013345
human,0.062666
hundr,0.011626
ideal,0.006225
identifi,0.004625
ignor,0.013273
illumin,0.017979
imag,0.005891
immort,0.010131
import,0.006526
includ,0.002656
inde,0.013011
india,0.005857
innat,0.009412
insight,0.007097
inspir,0.006330
instanc,0.010363
instead,0.008962
interpret,0.005104
jack,0.009045
japan,0.006327
jina,0.015412
juli,0.005526
just,0.008922
k,0.005800
kenin,0.018741
kind,0.015289
know,0.017391
knower,0.013988
knowledg,0.014436
known,0.006393
korea,0.007721
lack,0.004877
lao,0.010914
laugh,0.012626
law,0.003853
leader,0.011186
legend,0.009011
length,0.006346
level,0.003825
liber,0.005767
life,0.012335
lifei,0.018741
lifetim,0.008191
light,0.005273
like,0.006833
limitless,0.039909
link,0.002781
list,0.017784
live,0.012407
lokavid,0.018265
loknuvartana,0.017875
lokottara,0.018265
lokottaravda,0.017260
long,0.008306
longer,0.005275
look,0.005281
lord,0.045423
lotu,0.012536
lovingregard,0.018741
luminari,0.013717
m,0.009149
madhupindika,0.018741
maguir,0.014913
mahayana,0.082178
mahparinibbna,0.017875
mahparisa,0.018741
mahpurua,0.018265
mahsaghika,0.017875
mahsghika,0.157798
mahyna,0.014436
main,0.004049
maitreya,0.033568
major,0.003393
make,0.006505
man,0.028296
mani,0.020237
manifest,0.014716
mark,0.020541
master,0.020478
mastergiv,0.018741
mean,0.007045
mediev,0.006822
medit,0.019437
mental,0.006921
mention,0.012384
mere,0.018876
method,0.003961
metteyya,0.017546
millennia,0.009319
mind,0.012063
minor,0.005919
mn,0.011584
modern,0.003836
moment,0.007276
monarch,0.008447
monasteri,0.009718
monk,0.028382
mudra,0.081117
myth,0.008168
natur,0.017073
nayaka,0.016394
nectar,0.013931
need,0.003845
nht,0.015650
nibbna,0.016580
nikaya,0.013822
nirmakya,0.017546
nirvana,0.087353
nomorelearn,0.018741
note,0.003687
number,0.006399
o,0.013489
obes,0.011780
obscur,0.008860
omnipot,0.012065
omnisci,0.037788
onli,0.002944
opinion,0.006798
ordinari,0.006974
origin,0.007089
otherwis,0.006083
overal,0.005853
paint,0.008123
pali,0.119965
palisanskrit,0.017875
particular,0.004003
particularli,0.004485
past,0.005074
path,0.025939
paus,0.011864
peopl,0.014105
percept,0.006808
perfect,0.028584
perfectli,0.008886
person,0.020926
physic,0.016740
physician,0.008068
pin,0.010846
pleasur,0.009509
point,0.007420
poison,0.008940
popular,0.009151
pose,0.015581
posit,0.003790
possessor,0.028094
power,0.015013
prabhva,0.018741
practic,0.011772
pratyekabuddha,0.017009
preach,0.020941
preceptor,0.015917
present,0.003667
press,0.004316
previou,0.005530
pride,0.010094
primarili,0.005013
pronunci,0.009726
proper,0.006580
properti,0.004440
prophet,0.009626
protect,0.004888
protuber,0.016065
pupil,0.009567
pure,0.005799
purifi,0.010437
puruadamyasrathi,0.018265
qualiti,0.010929
question,0.004605
quit,0.012396
rahula,0.015412
rain,0.009313
rank,0.006192
rare,0.006035
reach,0.004727
read,0.004039
real,0.014483
realiti,0.006327
realiz,0.006503
reclin,0.015779
reduc,0.008813
refer,0.013956
regard,0.008925
region,0.004165
regionspecif,0.016394
relinquish,0.010680
remain,0.003847
rememb,0.025618
repli,0.019134
repres,0.003815
represent,0.005868
repudi,0.011457
requir,0.007323
respect,0.004423
rest,0.005449
reveal,0.006118
revis,0.006950
right,0.004174
road,0.006756
ruler,0.015543
said,0.004779
saint,0.007778
samayabhedoparacanacakra,0.056225
sambuddha,0.018741
samdhi,0.015779
samsara,0.012817
samyak,0.016784
samyaksabuddha,0.017260
samyaksambuddhahood,0.018741
sangharakshita,0.017260
sanskrit,0.028902
say,0.010079
school,0.023662
seat,0.006883
seen,0.022820
seer,0.013931
selfenlighten,0.018265
selfexist,0.015099
sens,0.009572
sentient,0.033255
shakyamuni,0.016394
shinsh,0.016784
shorten,0.009830
shown,0.005609
shravasti,0.018741
siddhartha,0.028466
sign,0.015990
signific,0.004132
similarli,0.006133
simpli,0.005282
simultan,0.006784
singl,0.008664
skeptic,0.008526
skill,0.006236
skilton,0.017546
skt,0.212784
sleep,0.009212
sn,0.021801
someon,0.007562
sometim,0.012642
sop,0.015303
sorrow,0.012450
sovereign,0.015474
special,0.016194
specif,0.003873
spectrum,0.007655
spiritu,0.014918
sta,0.015099
stage,0.005653
stain,0.010668
stand,0.012167
starvat,0.010680
state,0.019827
statu,0.021626
stra,0.013103
stress,0.006638
subject,0.004118
suffer,0.017725
sugata,0.033160
superb,0.027644
superior,0.007205
superl,0.061212
superman,0.014298
supernatur,0.018811
supramundan,0.064260
suprem,0.026517
surangama,0.018741
sutra,0.011439
sutta,0.070846
svakabuddha,0.018265
swami,0.013303
swe,0.015412
taish,0.015303
tame,0.011822
tathagata,0.064260
tathagatagarba,0.017009
tathagatath,0.037483
tathagatha,0.018741
tathgata,0.033160
taught,0.007217
teach,0.069758
teacher,0.044512
technic,0.005757
tell,0.007291
tend,0.005316
tenth,0.009923
term,0.012235
text,0.005584
thch,0.015650
theravada,0.073574
therefor,0.004400
thi,0.021792
thing,0.005151
thirtytwo,0.013769
thousand,0.028544
thu,0.007568
time,0.005728
tire,0.011404
titl,0.005810
torch,0.012288
tradit,0.028535
trait,0.007670
transcend,0.009157
transcendent,0.010341
transform,0.005299
tri,0.005095
true,0.015724
truth,0.006887
unawaken,0.018265
uncertainti,0.007533
unconsci,0.009679
understand,0.004394
univers,0.006705
unlimit,0.019374
unsoil,0.018741
unsurpass,0.014436
use,0.005290
usual,0.003895
utter,0.021337
vairocana,0.015917
vajra,0.016394
vakkali,0.036530
varada,0.018741
vari,0.014316
variou,0.003685
vasettha,0.037483
vehicl,0.007350
vener,0.010718
veri,0.007393
victori,0.022492
vidycaraasapanna,0.018265
vietnames,0.010833
view,0.004149
vinayaka,0.018741
virtu,0.008202
wa,0.029197
walpola,0.015528
water,0.004993
way,0.006922
wealth,0.006666
western,0.004869
wherea,0.005617
wisdom,0.042857
wise,0.020225
wish,0.014315
woke,0.014913
word,0.004493
work,0.003173
world,0.039384
worthi,0.010209
write,0.020246
xing,0.013570
yao,0.013523
zen,0.011680
zhihua,0.018741
