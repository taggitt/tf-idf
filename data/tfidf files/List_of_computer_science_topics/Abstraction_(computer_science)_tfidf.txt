abil,0.010143
abov,0.009438
abstract,0.745953
abstractmodel,0.019392
access,0.005100
achiev,0.013911
act,0.008426
action,0.004529
actor,0.008497
actual,0.014153
ad,0.004704
ada,0.012041
add,0.014058
addit,0.014916
administr,0.005154
affect,0.004816
age,0.018465
agre,0.005557
aim,0.005263
algebra,0.007915
algorithm,0.007389
allow,0.025660
alon,0.006303
alter,0.006612
altern,0.004736
analysi,0.039992
analysisactu,0.019392
analyst,0.008365
ani,0.022741
anim,0.051596
anoth,0.017727
answer,0.032817
antipattern,0.016814
anyth,0.014570
aphor,0.024621
appli,0.007881
applic,0.022011
appropri,0.005908
architectur,0.055624
arithmet,0.009000
arrang,0.006009
articl,0.004381
artifact,0.008734
artifici,0.006224
ask,0.006093
aspect,0.009689
assembl,0.011509
assign,0.019863
associ,0.007201
assort,0.011391
automat,0.007525
avail,0.009012
avoid,0.005505
awar,0.013338
away,0.016845
barnyard,0.019392
base,0.006423
basic,0.008943
becaus,0.006691
becom,0.007116
begin,0.004333
behavior,0.020855
behaviour,0.013705
behindthescen,0.015556
belong,0.018841
beneath,0.009607
best,0.004898
better,0.005012
binari,0.071010
bind,0.023026
bit,0.036159
boilerpl,0.016094
bracket,0.010604
break,0.006126
budget,0.006957
build,0.009407
built,0.005541
busi,0.005010
c,0.017015
cabl,0.019379
calcul,0.011343
carri,0.004798
case,0.011166
casl,0.017908
cast,0.007561
central,0.004173
certain,0.008148
chang,0.033562
channel,0.007348
characterist,0.005131
check,0.015435
choic,0.005849
circuit,0.008398
claim,0.009037
clariti,0.010688
class,0.064189
classinst,0.018298
clear,0.016623
client,0.051441
clojur,0.016424
code,0.080649
coder,0.063235
cognit,0.007058
combin,0.004245
common,0.021600
commonli,0.019777
commun,0.007786
compar,0.008807
compil,0.007681
compiletim,0.050444
complement,0.008778
complet,0.008224
complex,0.074064
complic,0.006747
compon,0.035892
composit,0.006172
comput,0.093736
concentr,0.011653
concept,0.033240
concern,0.016720
concis,0.009068
concret,0.048941
configur,0.008225
consequ,0.015188
consid,0.017361
consider,0.010007
constantli,0.008300
constraint,0.014785
consum,0.005928
contain,0.017085
content,0.005875
contin,0.007672
continu,0.003607
contract,0.011945
control,0.039559
conveni,0.007949
convert,0.006069
cooper,0.005690
core,0.006322
correctli,0.008555
correspond,0.005405
coupl,0.006889
cours,0.005189
cow,0.030802
crash,0.008855
creat,0.014567
criticis,0.008763
current,0.003811
dairyanim,0.038784
danger,0.006994
data,0.161562
databas,0.097522
datastructur,0.016094
datatyp,0.015127
deal,0.004796
decid,0.005802
decis,0.015060
declar,0.017252
decompos,0.009651
defin,0.036775
definit,0.018831
degre,0.009752
deleg,0.016814
depend,0.003928
deriv,0.004527
describ,0.018324
descript,0.016328
design,0.069973
determin,0.016539
develop,0.011911
dialect,0.008758
dictionari,0.007218
differ,0.034721
differenti,0.018086
dimens,0.006850
direct,0.003953
directli,0.004784
discuss,0.004858
distinct,0.009171
distribut,0.004912
doe,0.016115
domain,0.040706
domainspecif,0.027004
dont,0.016640
drastic,0.008913
drop,0.006535
dsl,0.014688
duplic,0.019535
dure,0.003427
earlier,0.010594
eat,0.025096
effect,0.010447
effici,0.011225
effort,0.014893
electron,0.005998
eleg,0.010688
elimin,0.006199
email,0.010478
embodi,0.008288
embrac,0.008086
employe,0.007511
enabl,0.017054
encapsul,0.022551
end,0.007838
endus,0.013089
enforc,0.006720
engin,0.010332
enorm,0.007720
ensur,0.005889
entir,0.018598
entri,0.006933
equal,0.004881
essenti,0.004983
establish,0.003825
estel,0.015127
evalu,0.012422
eventu,0.005094
everi,0.004485
exact,0.013677
exampl,0.057788
exchang,0.005067
execut,0.021720
exemplifi,0.009132
exhibit,0.006766
exist,0.007129
exit,0.018806
explicit,0.007827
express,0.023222
extend,0.009456
extern,0.002803
extrem,0.010284
facil,0.020613
fact,0.004551
fail,0.005389
fairli,0.016239
faith,0.007166
fals,0.007270
famili,0.009927
familiar,0.015169
far,0.004860
farm,0.007152
fashion,0.007544
featur,0.024239
feed,0.024687
fiber,0.009311
file,0.016002
final,0.009273
fit,0.006526
flexibl,0.023808
flow,0.011290
flowcontrol,0.019392
follow,0.003066
food,0.022067
forc,0.007684
foreign,0.004989
form,0.008789
formal,0.019186
fragil,0.010277
fragment,0.007539
framework,0.010983
free,0.004378
function,0.074566
fundament,0.004908
gate,0.018558
gener,0.022035
generalis,0.011061
gfdl,0.014769
gist,0.014463
goat,0.010084
good,0.004261
gradient,0.009120
granular,0.013327
great,0.008640
greenspun,0.038784
group,0.003457
ha,0.017891
habit,0.008127
handl,0.027880
hardwar,0.044837
hash,0.012944
haskel,0.025750
heavili,0.006179
help,0.004196
heritag,0.007840
hi,0.003436
hide,0.045370
hierarchi,0.007811
higher,0.013671
higherord,0.023364
highest,0.005869
highlevel,0.010121
howev,0.012443
human,0.011079
hunger,0.010546
idea,0.004302
ideal,0.012474
imag,0.005902
impact,0.015759
implement,0.080052
improv,0.009433
includ,0.015965
incorpor,0.010527
increasingli,0.005650
inde,0.006517
independ,0.011811
indepth,0.011120
individu,0.003953
inevit,0.008445
influenc,0.008443
inform,0.030486
inherit,0.035540
initi,0.004164
insid,0.006428
instanc,0.020765
instanti,0.012589
instead,0.013469
instruct,0.014432
integ,0.018384
intend,0.011867
interact,0.014347
interchang,0.008773
interfac,0.108482
intermedi,0.007533
intermediari,0.009728
intern,0.003359
interoper,0.012680
interpret,0.015341
intuit,0.008202
invers,0.008094
invis,0.009356
involv,0.037159
isol,0.012446
issu,0.004113
java,0.038821
joel,0.011196
kept,0.006906
key,0.009755
keyvalu,0.016814
keyword,0.012530
kind,0.005105
know,0.017423
knowledgebas,0.012135
known,0.006405
label,0.013792
lack,0.004886
lambda,0.021002
languag,0.216764
larch,0.017578
larg,0.003377
larger,0.005058
later,0.007622
layer,0.066392
layout,0.010737
lead,0.003823
leaki,0.015947
leav,0.010854
left,0.005108
legaci,0.040289
let,0.007180
level,0.134123
librari,0.018061
lie,0.006278
like,0.003422
limit,0.003913
linear,0.006818
link,0.005572
linktim,0.019392
lisp,0.072813
list,0.007126
live,0.008287
livingth,0.019392
loadtim,0.019392
locat,0.018846
logic,0.037213
look,0.010582
lookup,0.027004
loop,0.008824
loto,0.017908
low,0.004911
lower,0.004735
lowerlevel,0.013643
lowest,0.007622
lowlevel,0.023100
machin,0.024748
macro,0.022331
main,0.004056
major,0.006799
make,0.026068
manag,0.004589
mani,0.026066
materi,0.004490
mathemat,0.005037
maxim,0.014575
mean,0.007057
meaning,0.008328
meat,0.008892
meatanim,0.019392
meatqual,0.019392
mechan,0.004732
member,0.004088
memori,0.019996
menu,0.012743
messag,0.007963
metaocaml,0.019392
metaprogram,0.017040
method,0.031748
milk,0.018928
minim,0.013526
minimum,0.006989
model,0.031931
modelbas,0.013957
modern,0.007687
modul,0.017304
modula,0.016424
modulo,0.026739
mostli,0.005467
motiv,0.006260
multipl,0.004928
multipli,0.008260
n,0.011509
necessari,0.005106
necessarili,0.006272
need,0.038520
neg,0.005556
negat,0.010198
network,0.010713
new,0.008586
nontrivi,0.022181
notat,0.008610
noth,0.006780
notic,0.007272
notion,0.006062
noun,0.020169
novemb,0.005747
number,0.012822
numer,0.004747
obj,0.018298
object,0.052564
objectori,0.168260
observ,0.008458
obviou,0.016188
ocaml,0.016424
occas,0.007970
offer,0.019059
offic,0.005157
onli,0.029496
onlin,0.005608
oper,0.048509
opposit,0.010545
optic,0.007708
optimum,0.010639
order,0.003531
orient,0.006676
otherwis,0.006095
outsid,0.004888
overal,0.005864
overload,0.025750
packag,0.008123
pair,0.006828
paramet,0.007387
parti,0.004951
particular,0.008021
partit,0.008876
pascal,0.009968
pascallik,0.019392
pattern,0.005467
payrol,0.023211
perform,0.013513
person,0.016771
phase,0.006161
physic,0.025157
piec,0.021228
pig,0.028668
place,0.010871
plan,0.004852
player,0.008386
plu,0.007209
point,0.011150
polymorph,0.066109
portabl,0.010688
possibl,0.011243
potenti,0.004757
precis,0.012105
predetermin,0.010523
present,0.007348
principl,0.017643
print,0.006900
prior,0.005549
privat,0.005268
probabl,0.005272
problem,0.011789
proce,0.017891
procedur,0.012798
process,0.027789
processbas,0.016814
produc,0.011469
program,0.203375
programm,0.120064
project,0.018186
properli,0.007726
properti,0.026693
prove,0.005670
provid,0.026080
purpos,0.009559
python,0.011568
question,0.009228
quit,0.006209
rang,0.008821
rational,0.010370
read,0.004047
real,0.004836
reason,0.004182
recommend,0.006869
reduc,0.004414
reduct,0.006705
refer,0.011651
refin,0.014620
regard,0.004470
regist,0.014452
registerbinarylevel,0.019392
rel,0.012534
relat,0.006284
relationship,0.013344
relev,0.006095
reli,0.022460
relicens,0.016610
remain,0.011562
remov,0.005380
repeat,0.006726
report,0.004478
repres,0.019113
represent,0.017636
requir,0.007336
respect,0.004431
respons,0.004041
result,0.022722
retriev,0.006923
return,0.004716
rice,0.008788
right,0.004181
role,0.008164
routin,0.008365
rule,0.008507
runtim,0.013794
safe,0.015238
said,0.004788
sampl,0.006944
say,0.005048
scala,0.013165
schedul,0.007745
scheme,0.007013
scienc,0.011758
scope,0.006850
screen,0.008605
script,0.009186
sdl,0.018298
search,0.005964
select,0.005067
self,0.008008
selfcontain,0.012743
semant,0.008638
send,0.007287
separ,0.004257
sequenc,0.012938
seriou,0.006528
set,0.018049
share,0.009082
shift,0.005647
ship,0.013224
shore,0.017721
sideeffect,0.012064
sign,0.010679
significantli,0.005980
simarch,0.019392
similar,0.015490
simpl,0.041225
simpler,0.008647
simpli,0.031752
simplifi,0.022659
simul,0.007046
sinc,0.012972
singl,0.013020
small,0.007937
smaller,0.005514
softwar,0.054970
somewhat,0.006875
sound,0.020485
sourc,0.008159
space,0.005000
special,0.004056
specif,0.038803
specifi,0.026375
spell,0.008951
split,0.006665
spolski,0.017040
stack,0.010112
stage,0.011327
stand,0.006095
standalon,0.034434
standard,0.004368
start,0.004105
state,0.008513
statement,0.011907
static,0.016592
steer,0.010712
step,0.027497
stone,0.007243
store,0.026282
strategi,0.012082
strongli,0.006250
structur,0.049260
student,0.005520
studi,0.006854
style,0.006682
subject,0.004125
subroutin,0.029539
substitut,0.014413
subtl,0.009592
suitabl,0.028798
sum,0.006469
support,0.019280
suppos,0.007126
suppress,0.014853
syntact,0.022715
tabl,0.018725
taken,0.004748
tam,0.013369
task,0.048131
techniqu,0.029904
tediou,0.013015
templat,0.021376
tend,0.015977
tenth,0.019883
term,0.018387
termin,0.007598
themeven,0.019392
theorem,0.007270
theori,0.003801
theyr,0.012018
thi,0.050942
think,0.011248
thu,0.011373
time,0.017217
tracebas,0.019392
tradeoff,0.010456
tradit,0.004083
transfer,0.005549
translat,0.005583
tree,0.006680
tri,0.005104
turn,0.008967
type,0.066287
typeth,0.019392
typewhil,0.019392
typic,0.008702
uml,0.013957
unchangedthu,0.019392
undecid,0.023729
underli,0.006159
understand,0.004402
uniqu,0.011060
uppermost,0.014259
usabl,0.019901
use,0.058300
user,0.021584
usual,0.007805
valu,0.024940
vari,0.009562
variabl,0.041500
varieti,0.004869
variou,0.018460
vdm,0.015947
verb,0.010725
veri,0.007407
version,0.011346
view,0.016627
virtual,0.019241
visibl,0.007168
want,0.005987
waterfal,0.010948
way,0.024274
whenev,0.009126
wish,0.007171
work,0.015897
world,0.006576
worth,0.007343
write,0.005070
writer,0.006656
written,0.005134
yield,0.012671
z,0.007912
