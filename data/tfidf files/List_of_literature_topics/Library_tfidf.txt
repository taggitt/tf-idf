abb,0.011000
abbasid,0.003228
abbey,0.003396
abil,0.001451
abl,0.002666
abov,0.002702
abstract,0.003714
academ,0.031613
academi,0.006099
acceler,0.002029
access,0.042345
accommod,0.002450
accomplish,0.004241
accord,0.003165
account,0.003728
accumul,0.004049
accur,0.001797
accus,0.002116
achiev,0.001327
acknowledg,0.002034
acquaint,0.003088
acquir,0.003437
acquisit,0.002281
act,0.014473
action,0.001296
activ,0.004212
actual,0.002701
ad,0.005387
adapt,0.003360
addit,0.003202
address,0.001589
adequ,0.004427
adhud,0.005551
adjoin,0.003415
administ,0.002068
administr,0.002951
admit,0.002225
ado,0.004525
adopt,0.002912
adult,0.006197
advanc,0.002606
advis,0.002133
advoc,0.003623
advocaci,0.014281
aeliu,0.004525
afford,0.006798
afghanistan,0.002707
africa,0.003427
african,0.001849
afterschool,0.004653
afterward,0.002324
age,0.011894
agenc,0.003320
agent,0.001799
agre,0.001591
aid,0.001583
aim,0.006027
air,0.001621
aisl,0.004453
aldaula,0.004878
aldawla,0.004653
aleppo,0.004012
alessandrina,0.005238
alexand,0.004043
alexandria,0.020136
alilm,0.005551
alland,0.005238
alloc,0.002106
allot,0.003205
allow,0.011544
almuqaddasi,0.004525
almutawakkil,0.004755
alnadim,0.005238
alon,0.001804
alreadi,0.001537
amaz,0.003402
amazon,0.003019
ambrosiana,0.005551
amen,0.002972
amend,0.002225
america,0.004436
american,0.011956
americana,0.014349
analog,0.001885
analysi,0.001272
analyz,0.001773
anatolia,0.003175
anawrahta,0.004653
ancient,0.012232
anderson,0.002750
andr,0.002754
andrew,0.002143
angelica,0.004878
angelo,0.003676
anglican,0.003154
ani,0.002790
anim,0.001477
anla,0.005551
annual,0.008105
anoth,0.005075
answer,0.001879
antiqu,0.015286
antoineaugustin,0.005551
antoniu,0.004120
anu,0.003474
ap,0.003187
apollo,0.006602
apost,0.004101
apostol,0.003361
apostolica,0.004878
appar,0.003603
appear,0.008556
appli,0.001128
appoint,0.008674
approach,0.001214
approxim,0.004314
april,0.001614
aquila,0.004878
arab,0.003898
arabislam,0.004878
archaeologist,0.002908
architect,0.008416
architectur,0.007962
archiv,0.019423
ardent,0.003555
area,0.007337
argu,0.001403
argument,0.001773
aristocraci,0.002975
aristocrat,0.002911
aristotl,0.002175
arl,0.004607
armaria,0.005551
aros,0.002095
arrang,0.012043
art,0.001614
articl,0.002508
artizan,0.005238
ash,0.002851
ashurbanip,0.004389
asia,0.003624
asiniu,0.005238
ask,0.003489
aspect,0.002774
assembl,0.004942
assess,0.001797
assist,0.007867
associ,0.027834
astan,0.005551
astronomicastrolog,0.005551
athenaeum,0.004878
atho,0.004488
atrium,0.004161
attach,0.007991
attain,0.002136
attempt,0.004815
attract,0.001698
audiobook,0.003296
audiovisu,0.003780
audrey,0.004303
august,0.001603
augustinian,0.003827
augustu,0.009367
auspic,0.003158
austin,0.003035
australian,0.002194
austrian,0.002527
author,0.002376
avail,0.010320
averag,0.003152
avid,0.003695
await,0.003150
away,0.003215
babylonian,0.002868
backtoback,0.004389
badli,0.002868
bag,0.003276
bagdatiko,0.005551
baghdad,0.005861
bangladesh,0.006025
bank,0.001526
barbara,0.002794
barnard,0.003747
baronio,0.005551
barren,0.003509
bart,0.003780
bartolomeo,0.004028
base,0.001839
basel,0.003271
basi,0.005257
basic,0.003840
basil,0.003201
basilio,0.004565
bath,0.002962
batho,0.005032
bathroom,0.004252
bauen,0.005551
bc,0.020709
bear,0.001974
becam,0.004652
becaus,0.002873
becom,0.005093
befor,0.003260
began,0.002541
begin,0.004963
belang,0.004755
believ,0.001316
bell,0.002463
belong,0.005393
belveder,0.004525
benefici,0.002335
benefit,0.003088
bengal,0.003338
benjamin,0.002412
bequeath,0.007731
berlin,0.002266
besid,0.002177
bessarion,0.004453
best,0.001402
better,0.001434
biblic,0.002733
bibliograph,0.003579
bibliographi,0.003701
bibliophil,0.014635
biblioteca,0.027392
bibliotheca,0.019822
bibliotheken,0.005551
bibliothequ,0.005375
bibliothk,0.005551
bibliothqu,0.015738
bieri,0.005551
bigger,0.002856
bignon,0.010477
bilingu,0.003555
bind,0.002197
biographi,0.002454
birkenhead,0.005551
birkhus,0.003979
birman,0.004813
birmingham,0.003261
bisect,0.004028
bla,0.003827
block,0.001895
bluray,0.004161
boast,0.003201
bodi,0.005107
bodleian,0.008504
bodley,0.004653
bold,0.003126
bolton,0.004120
book,0.102798
bookcas,0.026193
bookmark,0.004330
bookpress,0.005551
bookshelf,0.004252
bookshelv,0.004813
booksmor,0.005551
border,0.003748
borough,0.015515
borromeo,0.004878
borrow,0.025343
boulder,0.003228
bramant,0.004702
branch,0.009472
brand,0.002572
brief,0.002016
briefli,0.002360
bring,0.001581
bristol,0.003338
britain,0.008963
britannica,0.002588
british,0.009945
broader,0.002121
broken,0.002121
brotherton,0.010477
brought,0.001568
brown,0.002159
brows,0.007473
buckingham,0.008410
buddhist,0.002559
budget,0.003983
bug,0.003350
build,0.029624
built,0.017449
bulk,0.002255
bureau,0.002406
burma,0.003162
burn,0.004362
busi,0.001434
businessmen,0.003096
buy,0.002013
byzantin,0.007853
byzantium,0.009843
c,0.001217
cabinet,0.004281
caesar,0.009463
caesarea,0.008281
calabria,0.004420
calif,0.003747
california,0.003904
caliph,0.005763
calligraph,0.004228
calm,0.002993
cambridg,0.001687
came,0.004180
campaign,0.007652
campfield,0.005551
campu,0.003019
campus,0.003630
canadian,0.002163
candl,0.003361
capit,0.002772
capita,0.002313
capitalist,0.002517
capitolin,0.008778
card,0.021793
cardin,0.011176
care,0.001601
career,0.002090
carnegi,0.018466
carol,0.003158
carpet,0.007278
carrel,0.004453
carri,0.001373
casanata,0.005551
casanatens,0.005551
case,0.005328
cassett,0.003979
cassiodoru,0.009404
cast,0.002164
castl,0.002889
catalog,0.002916
catalogu,0.043588
cataloguea,0.005551
categori,0.001626
cathedr,0.003019
catherin,0.003035
caus,0.002275
cd,0.005867
ceas,0.002152
ceil,0.003158
celebr,0.002196
celsu,0.016482
cenl,0.005551
center,0.010874
centerpiec,0.003676
centr,0.003504
central,0.005973
centuri,0.039398
cerrito,0.004950
certifi,0.002780
cesar,0.003555
cesena,0.005551
chain,0.011424
chair,0.002393
chalcedon,0.003852
challeng,0.003044
chamber,0.002261
champlin,0.005551
chang,0.000960
chapel,0.003271
charact,0.001761
characterist,0.001468
charg,0.004934
charl,0.003264
chartist,0.004161
cheap,0.002640
check,0.006628
chetham,0.016655
chicago,0.002077
chief,0.005324
chiefli,0.002763
children,0.021033
china,0.006549
chines,0.001831
chinguetti,0.004950
choos,0.001833
chose,0.002397
chosen,0.001947
christian,0.019820
church,0.009628
cicero,0.003175
cilip,0.005551
circa,0.002863
circul,0.037909
citat,0.005076
citi,0.017662
citizen,0.003488
citru,0.003488
civil,0.001461
claim,0.011643
class,0.011308
classic,0.016897
classif,0.016925
classroom,0.003099
clay,0.008164
clearli,0.001916
clement,0.003059
clergi,0.005806
click,0.003306
clientel,0.012548
clio,0.004161
cloister,0.004488
close,0.009123
cloud,0.002437
club,0.004943
clumsi,0.003827
codex,0.010443
codic,0.003934
coffeehous,0.004303
coin,0.001764
cold,0.002031
collabor,0.001957
collect,0.055948
collector,0.006045
colleg,0.012951
coloni,0.003524
column,0.002421
come,0.001219
comfort,0.005415
comment,0.002041
commentari,0.002457
commerc,0.002123
commerci,0.013063
commiss,0.001696
committe,0.014629
common,0.006183
commonli,0.004246
commonwealth,0.002201
commun,0.012260
compact,0.005029
compani,0.002691
compat,0.002445
compet,0.001757
competit,0.001676
complain,0.005535
complet,0.004709
complex,0.001247
compos,0.001617
compris,0.001743
comput,0.009886
conceiv,0.002238
concern,0.002393
conclus,0.001899
condemn,0.002400
condit,0.004770
conduct,0.002921
confer,0.005033
confid,0.002180
conform,0.004705
conglomer,0.003183
congreg,0.003081
conjunct,0.002382
connect,0.001403
conquest,0.002296
consequ,0.004348
consid,0.003976
consign,0.004101
consist,0.003422
consortium,0.002984
constantin,0.009254
constantinopl,0.023585
constantiu,0.013577
construct,0.009239
consul,0.002814
consult,0.005905
consum,0.001697
contain,0.012228
content,0.010091
contribut,0.001251
control,0.002265
conveni,0.004551
cooper,0.003258
copi,0.028770
coptic,0.003780
copyist,0.004252
corona,0.003758
corpor,0.001672
correct,0.001716
correspond,0.001547
corsini,0.015716
corsiniana,0.005551
cortil,0.005551
cosimo,0.004359
cotton,0.013030
council,0.007771
counterreform,0.003747
counti,0.010146
countri,0.006730
countywid,0.005551
cours,0.004457
courserel,0.005551
coursework,0.003571
cover,0.001403
craft,0.002564
craftsman,0.003934
creat,0.010426
creation,0.004654
crime,0.002114
critic,0.001253
crossw,0.005551
crown,0.002207
crumbl,0.003524
cultiv,0.004354
cultur,0.006408
cuneiform,0.007049
curat,0.003474
curios,0.003046
current,0.002182
cush,0.004120
cut,0.005429
cyclopdia,0.004252
d,0.005519
damag,0.001839
dana,0.013924
danger,0.002002
danub,0.003409
dar,0.003827
dark,0.002204
data,0.002720
databas,0.015954
date,0.002849
day,0.002572
deaccess,0.011103
deal,0.001373
death,0.002985
debat,0.001586
decad,0.001549
decay,0.002343
decid,0.001661
decim,0.005687
decimalbas,0.005032
decis,0.001437
declar,0.001646
declin,0.009710
decreas,0.001678
dedic,0.002047
dee,0.003995
defin,0.002339
definit,0.001347
degre,0.001395
deipnosophista,0.011103
del,0.007720
della,0.003333
demand,0.006087
democrat,0.001670
demonstr,0.003064
demosthen,0.004228
deni,0.001976
depart,0.004483
depend,0.003374
depict,0.002126
deposit,0.006054
depositori,0.003547
depth,0.002238
deriv,0.001296
describ,0.002098
descript,0.001558
design,0.008764
desir,0.001604
desk,0.006781
despit,0.004336
destroy,0.005347
destruct,0.004034
deutsch,0.002881
develop,0.007672
devis,0.002338
devot,0.006044
dewey,0.012650
diagnos,0.002761
dictionari,0.004133
did,0.005061
differ,0.001807
difficult,0.001486
difficulti,0.001841
digit,0.030456
diner,0.004565
direct,0.002263
directli,0.004108
directori,0.002861
disc,0.003103
discontinu,0.002588
discov,0.006004
discoveri,0.001596
discuss,0.002781
dispens,0.002965
dispers,0.002339
display,0.003874
dissemin,0.002654
dissert,0.002903
distec,0.005551
distinct,0.001312
distinguish,0.004540
distribut,0.001406
district,0.002061
divert,0.002993
divid,0.002703
dmoz,0.002257
doctrin,0.002044
document,0.006582
documentalist,0.005238
documentationdocu,0.005551
documentationintern,0.005551
documentationlibrari,0.005551
documentationperform,0.005551
documentationrequir,0.005551
doe,0.005766
dog,0.002551
doisciencea,0.005127
dome,0.003402
domu,0.004565
donat,0.002553
dont,0.002382
door,0.005331
doubt,0.002199
dozen,0.002384
draft,0.002199
draw,0.003494
drawer,0.004359
dream,0.002461
drove,0.002761
du,0.002409
duff,0.004064
dure,0.013737
duti,0.002028
dvd,0.003390
dwindl,0.003421
dynasti,0.008866
e,0.002762
earli,0.011291
earliest,0.010292
easi,0.002084
easier,0.004394
easili,0.001758
east,0.006311
eastern,0.001703
eat,0.002394
ebook,0.014009
ecclesiast,0.003056
eclips,0.005500
econom,0.003369
ecumen,0.003604
ed,0.008620
educ,0.018304
edward,0.001866
edwin,0.008122
effect,0.002990
effici,0.003213
effort,0.001421
egypt,0.008669
el,0.004935
elect,0.001513
elector,0.002201
electr,0.003393
electron,0.020608
element,0.001305
eleventh,0.003187
elimin,0.001774
elish,0.005127
elit,0.002278
ellsworth,0.007959
emerg,0.003945
emperor,0.011316
emphasi,0.003722
empir,0.005728
employ,0.006806
employe,0.004300
empow,0.002889
enabl,0.003255
enclosur,0.003440
encourag,0.003373
encyclopdia,0.002662
encyclopedia,0.008837
end,0.004488
endow,0.012997
endur,0.002504
engag,0.001625
engend,0.003209
engin,0.002957
england,0.008904
english,0.001429
englishspeak,0.005625
enjoy,0.001953
enlighten,0.004481
enlil,0.004653
enrol,0.002822
ensur,0.001686
entertain,0.005046
entir,0.009318
entranc,0.005779
enuma,0.009757
enviabl,0.004607
environ,0.001360
envoy,0.003049
ephesu,0.007333
epic,0.005517
equal,0.001397
equip,0.003669
era,0.001621
erect,0.002782
eresourc,0.005032
escori,0.005551
especi,0.004766
essenti,0.001426
establish,0.027381
estim,0.001444
etho,0.003427
europ,0.009741
european,0.006391
event,0.002671
eventu,0.001458
everi,0.001284
everyon,0.002369
everyth,0.002043
evid,0.002798
evolv,0.005053
ewart,0.011939
examin,0.001606
exampl,0.008272
exceed,0.002155
exclud,0.001930
exist,0.005102
expand,0.002903
expans,0.003463
expect,0.002820
expert,0.004054
export,0.001882
express,0.001329
extend,0.002707
extens,0.004157
extent,0.001688
exterior,0.006065
extern,0.000802
extrem,0.001472
f,0.001511
face,0.003080
facil,0.009835
facilit,0.003700
fact,0.002606
faculti,0.004741
failur,0.001785
fairli,0.002324
faith,0.002051
fall,0.001400
fame,0.002701
famili,0.001421
famou,0.001695
fan,0.002887
fare,0.003166
fashion,0.002159
faster,0.002140
fatherinlaw,0.004012
favor,0.003390
featur,0.002775
februari,0.001667
feder,0.003267
federico,0.003726
fee,0.009608
feet,0.005012
fell,0.001982
felt,0.008665
fiction,0.006566
field,0.003362
fifti,0.002601
fight,0.001925
figur,0.001519
fihrist,0.005551
filippo,0.004028
film,0.002081
final,0.002654
financi,0.004395
fine,0.004387
finland,0.005488
firm,0.003665
fit,0.003736
fix,0.004886
floor,0.012481
florenc,0.005908
flourish,0.009034
flow,0.001616
flower,0.002492
focu,0.001505
focus,0.001428
follow,0.002633
forbad,0.003338
forc,0.002199
foreedg,0.005551
forerunn,0.002914
form,0.009227
formal,0.001373
format,0.002993
formerli,0.002055
fortun,0.002489
forum,0.018031
foundat,0.005724
fragil,0.002942
fraley,0.005238
framework,0.001572
franc,0.001571
franci,0.006304
frank,0.002148
franklin,0.002510
free,0.015042
freedom,0.001732
freeli,0.007160
french,0.007384
frequent,0.006170
friend,0.002179
fruition,0.003747
fuch,0.003840
fulfil,0.006657
fulli,0.003308
fulltim,0.002922
function,0.006741
fund,0.016644
furnish,0.006245
futur,0.004009
gain,0.001335
gaiu,0.003964
galen,0.003146
galleri,0.005644
garden,0.002454
garrison,0.002780
gateway,0.003154
gather,0.003826
gave,0.001596
genealog,0.002959
gener,0.014194
gentleman,0.003595
gentlemen,0.003791
gentri,0.003555
geographi,0.002020
georg,0.004744
gift,0.002477
gilgamesh,0.004082
girolamo,0.004012
given,0.002201
glass,0.002419
glimps,0.003621
global,0.001457
goal,0.001509
godfrey,0.003769
golden,0.004447
golibrari,0.005551
good,0.002440
googl,0.010554
got,0.004951
govern,0.005217
government,0.002296
gp,0.002990
grade,0.002559
grafton,0.004252
grand,0.002131
grant,0.001682
grantham,0.008718
grate,0.003648
grave,0.002674
great,0.008657
greater,0.004146
greatli,0.001797
grecoroman,0.003142
greec,0.004148
greek,0.031080
gregori,0.002669
group,0.005939
grow,0.001407
grown,0.004226
growth,0.001389
guid,0.001551
guidanc,0.002474
guinea,0.002591
gutenberg,0.002939
guy,0.002981
h,0.001524
ha,0.012439
habit,0.006980
half,0.003021
halfpenni,0.005032
hall,0.004164
hampshir,0.003378
han,0.006579
hand,0.001359
handcopi,0.004878
handl,0.001995
harleian,0.005238
harri,0.002307
hast,0.003402
hazel,0.003949
heidelberg,0.003322
height,0.004376
held,0.005189
hellen,0.003158
hellenist,0.008369
help,0.004805
henri,0.003537
herculaneum,0.004205
herreraviedma,0.005551
heyday,0.003563
hi,0.015743
high,0.003333
highest,0.003360
highli,0.002889
hill,0.006548
hire,0.004792
histor,0.001232
histori,0.008226
historian,0.001849
ho,0.002814
hold,0.007931
home,0.004701
homer,0.003077
honor,0.002270
hospit,0.006492
host,0.002006
hot,0.002416
hour,0.009642
hous,0.011777
howev,0.007125
hssedacuk,0.005551
huge,0.002040
humanist,0.005331
hundr,0.001667
iaslic,0.005551
ibiblioorg,0.005238
ibn,0.002389
idea,0.001231
ideal,0.001785
identifi,0.001326
ifla,0.009511
ii,0.007181
iii,0.002098
ila,0.004303
ill,0.002130
illistr,0.005551
illiter,0.003402
illiteraci,0.003666
illumin,0.002578
illustr,0.001871
illyria,0.004607
immedi,0.001613
impact,0.006015
imperi,0.017102
implement,0.003055
import,0.011231
improb,0.003630
impuls,0.002812
inadequ,0.002606
incept,0.002856
includ,0.015997
incom,0.001680
incorpor,0.001506
increas,0.011608
increasingli,0.008089
index,0.003486
indian,0.003633
indic,0.002699
individu,0.004527
industri,0.002505
infant,0.002636
infight,0.003676
influenc,0.003626
inform,0.044731
initi,0.001192
injuri,0.002432
inlaid,0.004653
innerpeffray,0.005551
insid,0.001840
inspir,0.001815
instanc,0.001486
instead,0.003856
institut,0.034837
instruct,0.012395
intact,0.002812
intellectu,0.009413
intend,0.008494
intent,0.001846
interior,0.004307
intern,0.004808
internet,0.032404
interpret,0.001464
interrupt,0.002729
interview,0.002269
intox,0.003747
introduc,0.007723
introduct,0.001454
invas,0.002001
inveigh,0.004813
invent,0.001785
inventori,0.005707
involv,0.001063
ipswich,0.005032
iranian,0.005535
iraq,0.002455
iron,0.001965
irwin,0.003402
isbn,0.009080
islam,0.029228
iso,0.012805
isocr,0.004702
isotr,0.005238
issu,0.005887
itali,0.007899
italian,0.002059
item,0.014091
iv,0.002382
ivori,0.002981
j,0.002756
jacqu,0.002545
jame,0.004643
jerom,0.003074
john,0.008737
join,0.003264
joseph,0.003616
journal,0.007057
judgment,0.002325
julian,0.002829
juliu,0.011167
just,0.002559
justifi,0.004413
juvenil,0.003223
k,0.003327
keeper,0.003579
kendal,0.003934
kept,0.015818
key,0.002792
kidderminst,0.005238
kind,0.004385
king,0.003495
kingdom,0.007298
knew,0.002584
knoll,0.004303
know,0.003325
knowledg,0.009661
known,0.009168
kolkata,0.003949
lab,0.002397
laboratori,0.003723
labori,0.003639
labour,0.001857
labourintens,0.003934
lake,0.002164
land,0.004165
lang,0.003032
languag,0.001379
lanka,0.002928
laozi,0.004046
larg,0.015470
larger,0.002896
largest,0.010607
late,0.008891
later,0.004364
latin,0.017244
laurentian,0.004755
lavishli,0.004453
law,0.006631
lay,0.004129
laymen,0.003815
lead,0.001094
leader,0.003208
learn,0.012329
lectern,0.009901
lectur,0.001994
led,0.007104
lee,0.002494
left,0.001462
legal,0.003027
legendarili,0.005551
legisl,0.001679
legislatur,0.004489
leicest,0.003747
lend,0.040070
lengthi,0.002861
lent,0.006065
let,0.002055
letter,0.001811
lettr,0.004277
level,0.001097
levi,0.005156
lewanski,0.005551
libertati,0.011103
librari,0.920423
librarian,0.078742
librarianship,0.009901
librariesalthough,0.005551
libraryrel,0.005032
libraryth,0.004702
libwebdirectori,0.005551
lieuten,0.002928
lifetim,0.004698
light,0.006050
lightn,0.003146
like,0.005879
lilbrari,0.005551
limit,0.002240
lincolnshir,0.008778
link,0.000797
linkag,0.002892
list,0.008161
literaci,0.018844
literari,0.004601
literatur,0.011474
liu,0.003205
liverpool,0.006921
loan,0.008514
loaningreturn,0.005551
lobbi,0.002819
local,0.004970
locat,0.014838
logan,0.003964
london,0.010813
long,0.002382
longterm,0.001930
lopezgijon,0.005551
lord,0.002171
lose,0.001929
lost,0.004782
loui,0.002070
louvoi,0.011103
louvoiss,0.005551
louvr,0.004277
love,0.006533
low,0.001406
lower,0.005422
luciu,0.003865
lunaci,0.004813
lungara,0.005551
lush,0.003995
m,0.001312
machin,0.001771
maecenass,0.005551
magazin,0.002123
magnific,0.003415
main,0.002322
mainli,0.004569
maintain,0.006396
mainten,0.006602
majesti,0.003205
major,0.004866
make,0.005597
malatesta,0.004389
malatestiana,0.005551
manag,0.010511
manchest,0.011517
manhattan,0.003158
mani,0.029850
manner,0.001770
manuscript,0.029872
map,0.003356
marcellu,0.004755
marciana,0.005551
marcu,0.002900
mari,0.002119
mark,0.002945
market,0.005492
marshal,0.002431
mashhad,0.004565
mason,0.006350
mass,0.001504
materi,0.060421
mathematician,0.002245
matter,0.004139
maxim,0.002086
maximis,0.003276
mazarin,0.009051
mcluhan,0.003892
meal,0.003126
mean,0.008082
meant,0.001881
media,0.006730
medic,0.003378
medici,0.003488
mediev,0.007826
mediterranean,0.007199
meet,0.002968
melvil,0.007784
member,0.014045
membership,0.004041
mercantil,0.002900
merci,0.003175
mesopotamian,0.003209
met,0.001876
metal,0.001909
method,0.002272
metuchen,0.004702
michael,0.001688
microform,0.005032
mid,0.001826
middl,0.009075
middleclass,0.003138
midth,0.002263
milan,0.003077
militari,0.004452
million,0.007204
ming,0.003142
minist,0.003310
mitig,0.002636
mixtur,0.002293
model,0.004570
moder,0.002101
modern,0.017605
modesti,0.003964
modifi,0.001790
monarch,0.002422
monast,0.022196
monasteri,0.022299
monetari,0.001966
money,0.001639
mongol,0.005763
monk,0.021708
montecassino,0.005551
monument,0.005252
monypenni,0.005551
moon,0.002389
moral,0.001835
moreov,0.002104
morton,0.003474
mosqu,0.012428
mostli,0.001565
mount,0.002138
movement,0.005060
mp,0.010822
multipl,0.005643
multistori,0.004228
municip,0.004470
murray,0.002652
museum,0.033182
music,0.002016
muslim,0.004103
muslimchristian,0.005238
mutual,0.001935
myth,0.002342
nation,0.027446
national,0.003146
natur,0.003917
naturalist,0.002499
navig,0.006872
nazianzu,0.004653
nb,0.003547
nd,0.005120
nealschuman,0.005551
near,0.004525
nearbi,0.002257
nearli,0.004793
necess,0.004597
necessari,0.002923
need,0.011028
negoti,0.001920
nelson,0.002771
nephew,0.003070
neri,0.008841
netlibrari,0.005551
network,0.003067
new,0.013110
newli,0.003860
newspap,0.004542
newsroom,0.004488
newyork,0.004359
nicea,0.004653
nichola,0.002467
nineteenth,0.002294
nineveh,0.008365
nippur,0.004702
nj,0.002692
nonconformist,0.004101
nonetheless,0.002294
nonfict,0.003201
normal,0.001462
nort,0.003906
north,0.004369
northern,0.001714
norwich,0.012303
notat,0.002465
note,0.001057
novel,0.006091
novello,0.004950
novemb,0.001645
nucleu,0.002430
number,0.010095
numer,0.002718
oak,0.002954
observ,0.001210
obtain,0.002851
occup,0.001911
occur,0.001165
octavia,0.008907
offer,0.012277
offici,0.002785
old,0.004553
older,0.001925
oldest,0.005953
omen,0.007840
onc,0.004112
onetenth,0.003827
onli,0.012666
onlin,0.014450
onu,0.004565
opac,0.004101
open,0.026373
oper,0.005786
oppos,0.001533
opposit,0.001509
order,0.009098
organ,0.014008
organis,0.001736
orient,0.001911
origen,0.004182
origin,0.007116
ornat,0.004028
orr,0.004228
orthodox,0.002470
otherwis,0.003489
ottoman,0.002594
outbreak,0.002496
outofprint,0.004607
outreach,0.003266
outshon,0.005551
outsid,0.002798
overal,0.001679
overse,0.002735
owe,0.002232
owner,0.002075
oxford,0.001723
p,0.002800
paci,0.004453
pagan,0.008459
pakistan,0.007522
palac,0.005246
palatin,0.015680
palatina,0.004950
palatinu,0.005238
palazzo,0.004565
pamphilu,0.005238
pander,0.004878
papal,0.003179
paper,0.013622
papermak,0.004028
papyri,0.003852
papyru,0.006099
parallel,0.001900
paraprofession,0.004755
parchment,0.003949
parent,0.004131
pari,0.004012
parish,0.006148
parliament,0.005550
parochi,0.012192
partial,0.003375
particip,0.001447
particular,0.005741
particularli,0.006432
partli,0.001972
partner,0.002005
pass,0.007125
passag,0.002185
passmor,0.004565
patern,0.003286
patient,0.002234
paton,0.004565
patriarch,0.012253
patron,0.030887
patronag,0.002987
paw,0.003827
peac,0.001769
peak,0.002013
penc,0.004303
peninsula,0.002440
pennsylvania,0.005496
peopl,0.008091
percent,0.001842
percentag,0.002044
perform,0.002579
pergamum,0.004205
period,0.019620
peripher,0.002824
perman,0.003555
permit,0.001868
perpendicular,0.006112
persian,0.002304
person,0.003601
peter,0.001672
peterborough,0.004653
petersburg,0.003301
phase,0.001764
philadelphia,0.005579
philanthropist,0.003676
phillip,0.002838
philosoph,0.004830
philosophi,0.001561
physic,0.008402
physician,0.002314
pierc,0.003158
pieti,0.003390
pitakataik,0.005551
place,0.003112
plan,0.008335
planet,0.004162
platina,0.004702
plato,0.002416
play,0.002649
plenti,0.002884
plini,0.003019
plugin,0.003995
plume,0.003547
pmid,0.002826
polemaeanu,0.005551
polit,0.001151
pollio,0.014852
pollioss,0.005551
pond,0.003242
poor,0.001813
pope,0.007739
popul,0.006526
popular,0.009187
portal,0.002645
portico,0.004950
porticu,0.011103
posit,0.001087
possess,0.003297
possibl,0.003218
postcivil,0.004277
postsecondari,0.003803
potent,0.003301
potenti,0.004085
power,0.001076
pp,0.001714
practic,0.001125
practicetext,0.005551
praet,0.005551
prayer,0.002771
precipit,0.002419
predecessor,0.002293
prehistori,0.002642
premis,0.002467
premodern,0.003130
prepar,0.003412
presbyt,0.004607
preschool,0.003827
presenc,0.001590
present,0.002103
preserv,0.015957
press,0.006190
prestig,0.002780
prevail,0.002225
prevent,0.001503
previou,0.001586
pride,0.002895
primari,0.001388
primarili,0.001437
primer,0.006085
princ,0.004540
princip,0.003286
print,0.011852
printphys,0.005551
prioriti,0.004436
privaci,0.006222
privat,0.031675
probabl,0.001509
problem,0.001125
problemat,0.002535
process,0.004972
produc,0.003283
product,0.003289
profess,0.006594
profession,0.005113
profound,0.002399
progenitor,0.003460
program,0.014556
progress,0.001433
project,0.007809
promin,0.001649
promot,0.007503
prompt,0.002302
proper,0.001887
properti,0.002547
propos,0.002595
proprietari,0.003146
proprietor,0.010283
protect,0.004206
prototyp,0.002713
provid,0.033600
provis,0.006146
prussian,0.003183
ptg,0.005032
ptolema,0.003322
ptolemi,0.005722
public,0.097466
publicli,0.002251
publish,0.010508
purchas,0.005367
purpos,0.008209
qin,0.003350
qualiti,0.001567
quarter,0.002188
qud,0.004878
question,0.003963
quickli,0.005136
quiet,0.006162
qurn,0.004420
qurra,0.004389
radic,0.001928
rais,0.004654
ralph,0.002829
ran,0.002354
rang,0.002525
ransom,0.007034
rare,0.008656
rate,0.003910
raymond,0.002742
razavi,0.004755
rd,0.003767
reach,0.002711
read,0.031285
reader,0.018199
readili,0.002282
readingenhanc,0.005551
realiti,0.001814
realiz,0.001865
realizedth,0.005551
realli,0.002146
reason,0.002394
rebuilt,0.006419
receiv,0.002549
reciproc,0.002506
recogn,0.001430
recommend,0.005899
reconstruct,0.002090
record,0.012656
rector,0.003878
redefin,0.002951
reduc,0.001263
refer,0.015344
reflect,0.001433
reform,0.003263
regard,0.001279
regist,0.006206
reign,0.006920
rel,0.002392
relat,0.002698
releas,0.001667
reliabl,0.003989
relianc,0.002674
religi,0.006816
religion,0.001699
remain,0.003310
remind,0.003009
remov,0.001540
renaiss,0.002206
renam,0.006948
render,0.002188
renouard,0.005551
rental,0.003306
reorgan,0.002789
repair,0.002375
report,0.006411
reportedli,0.002649
repositori,0.015711
repres,0.002188
republ,0.006263
reput,0.002321
request,0.004055
requir,0.007351
research,0.032469
reserv,0.001774
reshelv,0.005551
resid,0.001767
resourc,0.022960
respect,0.001268
respons,0.001157
restrict,0.009277
result,0.006505
retent,0.003103
retriev,0.013874
retrofit,0.004228
return,0.001350
reunit,0.003096
revenu,0.001946
review,0.001494
reviv,0.002120
revolut,0.004958
rfid,0.004653
richard,0.003296
richest,0.002908
right,0.002394
rigidli,0.003612
rine,0.015097
rise,0.005370
risk,0.001641
rival,0.002204
rluk,0.005551
robert,0.001466
rocca,0.005032
roi,0.003791
role,0.004674
roma,0.003769
roman,0.019685
romanum,0.004755
rome,0.027334
roof,0.003246
room,0.049772
roughli,0.001871
routin,0.002394
royal,0.015842
rule,0.006089
ruler,0.002229
run,0.002932
rural,0.002157
russian,0.001953
ruth,0.003296
s,0.000946
sacr,0.002554
said,0.002741
saint,0.004461
saintegenev,0.005238
salford,0.004755
saltykovshchedrin,0.005375
sanskrit,0.002763
santa,0.002643
save,0.003789
saw,0.011428
say,0.001445
sayf,0.005238
scale,0.001510
scarc,0.002554
scarecrow,0.003686
scatter,0.002463
scene,0.002403
scholar,0.013554
scholarli,0.009446
scholarship,0.002385
school,0.009501
scienc,0.004488
scientif,0.002685
scientist,0.001525
scotland,0.002507
scribbl,0.004453
scribe,0.010342
script,0.005259
scriptoria,0.004702
scriptorium,0.025635
scriptur,0.005368
scroll,0.026248
search,0.018783
searchabl,0.003502
second,0.001094
secondari,0.001899
sect,0.002794
section,0.008265
secular,0.006926
secur,0.004252
seek,0.002994
seeker,0.003488
seen,0.002618
seiz,0.002293
select,0.008704
selfimprov,0.003840
sell,0.001887
seluk,0.005238
senat,0.002288
seneca,0.003595
sens,0.004118
separ,0.008532
septemb,0.001608
serapeum,0.005551
seri,0.002658
serial,0.003022
seriou,0.001868
serv,0.013280
server,0.003154
servic,0.033796
session,0.002538
set,0.005167
seventi,0.003122
sever,0.003871
share,0.001300
sharehold,0.005395
sheffield,0.007110
shelf,0.009769
shelv,0.031394
shift,0.004850
shill,0.011087
shiraz,0.004252
shop,0.002489
short,0.002844
shortterm,0.002425
shown,0.001608
shrewsburi,0.004525
sibirian,0.005551
sicili,0.006133
sign,0.001528
signag,0.004488
signific,0.004741
significantli,0.001712
silenc,0.002975
silk,0.008315
similar,0.004434
simpli,0.001515
sinai,0.003355
sinc,0.002785
sir,0.002095
sit,0.002454
site,0.001563
sixtu,0.008504
size,0.001445
skill,0.008943
skyhors,0.005238
sloan,0.006757
slope,0.002680
small,0.005680
smaller,0.001578
social,0.005905
societi,0.003489
softwar,0.007868
sold,0.001960
sole,0.003769
solut,0.003177
someth,0.001785
sometim,0.006043
son,0.007111
soon,0.001702
sort,0.001947
soter,0.004359
sought,0.009044
sourc,0.008175
southeast,0.002273
sovereign,0.004438
space,0.011452
spain,0.005919
spawn,0.002959
special,0.027869
specialis,0.002690
specialist,0.002303
specif,0.003332
specifi,0.001887
specul,0.001931
spend,0.001927
spent,0.002052
spine,0.003142
spiritu,0.002139
spot,0.002324
spread,0.006564
spreadsheet,0.003579
sri,0.002701
st,0.005987
stack,0.031846
staf,0.003134
staff,0.010944
stall,0.003088
standard,0.011255
stapl,0.002939
star,0.002059
start,0.009402
state,0.016248
station,0.004056
statist,0.001523
steadili,0.004898
steel,0.002325
steer,0.003066
stem,0.002087
step,0.001574
stilt,0.004252
storag,0.004419
store,0.007524
stori,0.003764
storytel,0.003378
street,0.002118
strength,0.001883
stress,0.001904
strife,0.003059
strove,0.003736
structur,0.003254
student,0.020544
studi,0.005887
style,0.001913
suart,0.005551
subject,0.009449
submit,0.002325
subscrib,0.017419
subscript,0.060320
subsequ,0.001448
succeed,0.007592
success,0.002462
suffer,0.001694
sufiya,0.005551
suggest,0.001299
suit,0.002158
suitabl,0.002061
sumer,0.006907
summer,0.002148
sun,0.002059
sunlit,0.004488
supplement,0.002298
support,0.013247
suprem,0.003802
surround,0.001700
surveil,0.002930
survey,0.007138
surviv,0.004804
susann,0.004140
swedish,0.002532
swell,0.003066
switch,0.004559
syme,0.004389
synonym,0.002317
t,0.001615
tablet,0.008879
tag,0.006032
taken,0.002719
task,0.003444
taskrel,0.004950
tate,0.003865
tax,0.005234
taxsupport,0.005127
tc,0.003460
teach,0.005456
teacher,0.004255
technic,0.004953
techniqu,0.001426
technolog,0.002677
tedder,0.005551
temper,0.005602
templ,0.009818
tend,0.003049
tendenc,0.002084
tenet,0.002733
term,0.002632
terri,0.003138
texa,0.002621
text,0.016017
textbook,0.002092
th,0.033004
thailand,0.002805
thcenturi,0.002140
theatr,0.002623
themeistiu,0.005551
themistiu,0.010065
themselv,0.004131
theodor,0.002541
theolog,0.006790
theori,0.002176
therapi,0.002364
therebi,0.001921
therefor,0.002524
thereof,0.002853
thi,0.029862
thing,0.001477
thoma,0.003321
thomait,0.005551
thompson,0.002756
thou,0.003547
thought,0.002631
thousand,0.001637
throw,0.002851
thu,0.004341
thucydid,0.003539
thunder,0.003621
thwart,0.003291
tianyi,0.005375
tiberiu,0.008128
tight,0.002834
timbuktu,0.003995
time,0.014787
titl,0.006666
today,0.002667
toddler,0.008241
toe,0.003474
togeth,0.001280
tomb,0.002957
took,0.001450
tool,0.004460
toor,0.005551
topurchas,0.005551
tor,0.003827
total,0.003886
tour,0.005357
town,0.003813
townsfolk,0.004950
track,0.002054
tradesmen,0.004012
tradit,0.009353
traffic,0.002453
trai,0.004702
train,0.003206
trajan,0.015410
transact,0.003998
transform,0.001519
translat,0.001598
transluc,0.003747
transpar,0.002376
transport,0.001591
travel,0.003278
treasuri,0.002467
treatis,0.002239
trek,0.003350
trend,0.003546
tri,0.001461
triclinu,0.005551
trigg,0.008778
true,0.001503
truli,0.002326
tune,0.003016
turk,0.002933
turkey,0.002421
tusculum,0.005032
twelv,0.002449
twentyeight,0.003949
twice,0.002213
tworoom,0.004878
type,0.010046
typic,0.003736
ugarit,0.004252
uk,0.008509
ulpian,0.009627
ulpiana,0.005551
ultim,0.001605
umayyad,0.003467
unchang,0.002475
uncondit,0.003150
und,0.002731
undergo,0.002152
undergradu,0.009892
understand,0.002520
undoubtedli,0.003276
uneas,0.004082
uniformli,0.003025
uniqu,0.001583
unit,0.009365
univers,0.013463
universitylik,0.005551
unlik,0.004638
unoffici,0.002848
unrestrict,0.003317
unschool,0.004950
unsuit,0.003301
untrain,0.003878
unwil,0.003228
uplift,0.003322
upper,0.001874
urb,0.004525
usa,0.002072
usag,0.007789
use,0.018208
usemethod,0.005551
user,0.035016
userdriven,0.005238
usual,0.007821
v,0.004966
valen,0.004330
vallicelliana,0.005551
valu,0.002380
valuabl,0.010898
van,0.002031
vari,0.002737
varieti,0.001394
variou,0.002114
vast,0.003728
vaticana,0.004950
vend,0.003995
venic,0.003042
ventur,0.002445
venu,0.002810
veri,0.005301
version,0.001624
vespasian,0.013823
vice,0.002149
victim,0.002494
victori,0.002150
victorian,0.002981
videotap,0.004046
vienna,0.002656
view,0.002380
vii,0.002824
villa,0.012930
virtu,0.002352
virtual,0.003672
visibl,0.004104
vision,0.006033
visionari,0.003509
visit,0.003730
visitor,0.007619
vital,0.002073
vivarium,0.010750
vocabulari,0.002776
voic,0.002213
volcan,0.002663
volum,0.011133
wa,0.080698
wait,0.002517
walkway,0.004488
wall,0.011796
walter,0.002214
walther,0.003453
want,0.001714
war,0.003956
warsaw,0.003228
wast,0.002227
water,0.001432
waterway,0.003187
way,0.008935
wealth,0.001912
wealthi,0.002422
weather,0.002276
web,0.006039
webcat,0.005551
websit,0.003511
weed,0.003355
week,0.001951
weekend,0.003440
welbourn,0.005551
welfar,0.002138
wellspent,0.005551
welsh,0.003317
went,0.001703
west,0.007971
western,0.001396
whi,0.001708
wider,0.002114
widget,0.004228
wikisourc,0.007278
william,0.004463
winchest,0.003949
window,0.010437
wing,0.002527
wish,0.002052
women,0.001767
won,0.001879
wood,0.004285
word,0.003866
work,0.016384
worker,0.003456
workshop,0.010993
workstat,0.003865
world,0.016002
worldcat,0.003979
wors,0.002553
write,0.004355
writingth,0.010750
written,0.007350
wrote,0.003163
wulfram,0.005238
www,0.003995
x,0.001643
xiang,0.004252
xii,0.003350
xiii,0.003427
year,0.006559
york,0.004290
young,0.003448
younger,0.004613
youth,0.002341
zauski,0.005551
zawiyat,0.005551
zeno,0.003539
zhou,0.003301
