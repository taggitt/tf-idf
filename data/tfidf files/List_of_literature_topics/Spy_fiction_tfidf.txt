aaron,0.008473
abov,0.003571
account,0.003285
accur,0.004752
action,0.010282
actionori,0.012570
activ,0.019484
actual,0.003570
adam,0.011322
adapt,0.013326
adolesc,0.032465
adult,0.005461
adventur,0.048988
adversari,0.008274
aeroplan,0.010400
affair,0.013837
afghanistan,0.007157
aficionado,0.011772
africa,0.004530
age,0.003493
agenc,0.021942
agent,0.061831
aim,0.003983
airwolf,0.029350
akin,0.007465
alan,0.011827
alarm,0.007770
alex,0.016467
alexand,0.010689
alia,0.009619
alistair,0.010695
allegi,0.007447
alley,0.010695
alli,0.010304
allow,0.002774
allsop,0.014675
alpha,0.006457
altern,0.003584
amateur,0.030471
ambler,0.036539
america,0.003909
american,0.053730
ami,0.009095
analyst,0.006330
anarchist,0.016427
andi,0.027551
angloamerican,0.008582
anglorussian,0.011963
ani,0.002458
anonym,0.014772
anoth,0.002683
antholog,0.007921
anthoni,0.013175
anticommunist,0.008962
antihero,0.012068
antiimperialist,0.010742
antinazi,0.012299
antirepublican,0.013086
antisemit,0.008994
anywher,0.006828
apartheid,0.008900
appear,0.003231
archer,0.008900
aristocrat,0.015391
armi,0.004798
arthur,0.005690
ashenden,0.014675
asia,0.009579
assassin,0.019806
asset,0.005156
assign,0.020042
attack,0.008995
attitud,0.005726
attract,0.004488
audienc,0.006296
australian,0.005801
authent,0.007065
author,0.059676
autumn,0.007422
avallon,0.013847
aviat,0.007338
b,0.007389
backdrop,0.009061
background,0.004790
bagley,0.011772
baldacci,0.013552
bang,0.015034
bank,0.004034
barbar,0.009619
baro,0.023204
barri,0.007386
base,0.004861
batista,0.011057
battl,0.005268
bear,0.005220
becam,0.024596
becaus,0.002532
befor,0.005745
began,0.023511
begin,0.029518
believ,0.006957
bell,0.006512
ben,0.007193
benson,0.009643
benton,0.021890
berenson,0.013552
berlin,0.023962
best,0.003707
bestsel,0.008295
betray,0.008154
big,0.015279
biograph,0.007638
bird,0.011483
bletchley,0.011523
blind,0.006884
bloc,0.006749
bolshevik,0.008825
bomb,0.006368
bond,0.019217
book,0.009590
border,0.004954
bourn,0.010695
bow,0.008264
boyd,0.018478
brad,0.009418
brain,0.005382
bravo,0.022751
brett,0.010218
brian,0.020640
bring,0.004181
britain,0.018954
british,0.071357
brittani,0.010695
brook,0.007404
brought,0.004146
brucepartington,0.014675
bryan,0.017770
buchan,0.011963
buckley,0.022114
built,0.004193
burden,0.012841
bureaucraci,0.007491
burn,0.005765
butcher,0.009376
c,0.003219
ca,0.006187
calcutta,0.009356
callan,0.025449
came,0.003683
camp,0.006095
canada,0.004825
canadianamerican,0.011772
cardin,0.007386
career,0.005526
carl,0.005624
carr,0.051957
carter,0.007955
carterkillmast,0.029350
casablanca,0.010695
casino,0.018331
caspar,0.010695
castro,0.009112
caus,0.003006
ceas,0.005690
cell,0.009690
central,0.003157
centuri,0.005785
charact,0.013966
characterist,0.003883
charg,0.004347
charl,0.021573
charli,0.018225
charteri,0.014675
charwoman,0.014675
cheka,0.012724
cherub,0.013847
chesterton,0.011375
chez,0.011602
childer,0.012299
children,0.009266
china,0.004328
chines,0.004842
chri,0.015167
christoph,0.011808
chronicl,0.007095
chuck,0.011240
cia,0.053548
cinema,0.015126
cinemat,0.020367
circa,0.007570
circl,0.005428
citizen,0.009220
cityst,0.007904
civil,0.015458
civilis,0.007688
claim,0.003419
clanci,0.011685
clash,0.013410
clean,0.006594
cleaner,0.009596
cleev,0.029350
clich,0.010840
climax,0.008713
close,0.003014
club,0.006533
code,0.004359
codi,0.012299
coffin,0.009822
cold,0.064445
cole,0.015680
colin,0.007755
coloni,0.004658
column,0.006400
combat,0.005913
combin,0.003212
comedi,0.016831
command,0.004726
commerci,0.004316
commun,0.002946
complex,0.003297
compton,0.018794
conan,0.010649
concern,0.006326
condor,0.010439
confer,0.004435
conflict,0.008393
congress,0.005048
conrad,0.017139
consequ,0.003831
consid,0.005255
constant,0.004479
contemporari,0.013222
context,0.003963
continu,0.021841
contribut,0.003307
control,0.005987
convey,0.006802
coont,0.014675
cooper,0.004306
coq,0.011447
cori,0.010562
count,0.005138
counterintellig,0.035055
counterterror,0.010288
countri,0.005930
cours,0.003927
cover,0.007419
covert,0.008962
coyl,0.013302
coyot,0.010742
craig,0.023969
creat,0.011024
credibl,0.007012
crew,0.007085
crime,0.005590
crimin,0.005707
critic,0.003314
crown,0.005835
cryptograph,0.009795
cuba,0.007172
cuban,0.008285
cuckold,0.014675
cum,0.009095
curtain,0.009743
cyber,0.009822
damascu,0.017862
dame,0.008811
danger,0.010585
daniel,0.005604
dark,0.005826
david,0.028018
day,0.020401
dead,0.005648
death,0.011839
deaver,0.013847
deceas,0.007998
decept,0.007921
decis,0.003798
declar,0.004352
declin,0.004278
deep,0.010032
defect,0.006651
defelic,0.014675
defenc,0.005485
defin,0.003092
degener,0.007888
deighton,0.013847
delphi,0.010400
demil,0.012895
democraci,0.005019
denni,0.015450
depict,0.005620
deposit,0.005334
deriv,0.003426
derringdo,0.014209
describ,0.005546
design,0.003309
deskman,0.014675
desmond,0.030070
despit,0.007641
detect,0.014844
determin,0.003129
deutschland,0.010840
develop,0.004506
devic,0.005035
dictat,0.006443
die,0.004317
digit,0.005031
diment,0.014675
dimitrio,0.024859
director,0.005417
dirt,0.018593
dirti,0.008672
disast,0.006224
discov,0.003968
disestablish,0.010790
disguis,0.008213
distrust,0.008295
documentari,0.006924
dolli,0.020724
dome,0.008994
domest,0.004668
domin,0.007415
donald,0.006352
door,0.007046
dossier,0.011057
doubl,0.014966
doyl,0.009934
drama,0.015221
dream,0.006505
dreyfu,0.009668
drink,0.006681
duan,0.010695
dudleysmith,0.014675
dunn,0.009277
dure,0.054469
durel,0.014675
dx,0.009527
dzerzhinski,0.013552
e,0.021907
earli,0.016279
earlier,0.004008
east,0.008341
eastern,0.004502
eat,0.006330
edg,0.005918
edgar,0.007929
edward,0.004934
egypt,0.005728
eisler,0.013847
elder,0.006666
element,0.010350
elli,0.008213
elphinston,0.013086
embassi,0.006717
emerg,0.013907
empir,0.007570
employ,0.003598
end,0.002966
enemi,0.012126
engag,0.004295
england,0.004707
english,0.003778
englishlanguag,0.017831
enigma,0.009849
entangl,0.008349
enter,0.003965
episod,0.013325
epitaph,0.010742
era,0.008572
eric,0.006443
erskin,0.011375
especi,0.009450
espionag,0.211128
establish,0.014475
ethic,0.004992
eunuch,0.010649
europ,0.007356
european,0.006757
eurospi,0.014675
evan,0.007583
eve,0.007938
event,0.007060
everi,0.003394
evil,0.006598
exagger,0.007563
examin,0.008491
exampl,0.024295
exclud,0.005103
expert,0.005358
explor,0.004133
explos,0.006013
expos,0.005433
extend,0.003578
extern,0.002121
extravag,0.009130
eye,0.016269
f,0.011987
factor,0.003439
fade,0.007747
faith,0.010846
fall,0.003702
fals,0.011003
familiar,0.005739
famou,0.004481
fantast,0.017622
farewel,0.009692
fascism,0.017740
fastpac,0.011057
faulk,0.014209
fear,0.005090
featur,0.055030
fedora,0.013847
felix,0.007929
fell,0.005239
femal,0.010893
femm,0.012068
fenimor,0.012299
ferguson,0.017681
fertil,0.005686
fiction,0.225652
fidel,0.008223
field,0.002962
file,0.012110
film,0.044014
filter,0.006761
finder,0.010479
firefox,0.012179
firm,0.004845
flag,0.006477
fleme,0.044426
flight,0.005943
flippanc,0.014675
flood,0.006404
flynn,0.010362
focus,0.003776
fog,0.009418
follett,0.023370
follow,0.006961
fop,0.013552
forb,0.023544
forc,0.002907
foreign,0.003776
forev,0.015465
formerli,0.005433
formula,0.010131
forsyth,0.022000
founder,0.005084
fox,0.007472
franc,0.008305
franchis,0.015664
francin,0.011115
frederick,0.013088
freelanc,0.009795
freemantl,0.014675
freez,0.007517
french,0.023423
fresh,0.006515
friend,0.005761
frontier,0.012788
frontperiod,0.014675
fulgencio,0.011375
funer,0.007938
furst,0.013847
fx,0.008393
g,0.003924
gal,0.010252
gallagh,0.009905
game,0.044800
garden,0.006488
gardner,0.008646
garner,0.016176
gaston,0.009527
gave,0.004221
gayl,0.013302
gear,0.014724
gene,0.005628
gener,0.004168
geneva,0.007710
geniu,0.007896
genr,0.082194
geopolit,0.007938
georg,0.004180
gerald,0.007523
german,0.016386
germani,0.030574
ghost,0.007880
gilbert,0.006938
gillen,0.013847
girl,0.014324
given,0.005818
glamor,0.012068
glamour,0.024859
global,0.003852
goodman,0.009482
govern,0.011033
governmentsanct,0.014675
gradi,0.011306
graham,0.014784
gran,0.009095
grard,0.009643
great,0.019616
green,0.009977
greenmantl,0.029350
greenwich,0.009549
gregori,0.007056
griffin,0.018260
grim,0.010085
grit,0.011963
ground,0.004221
group,0.002616
grow,0.007439
guillou,0.013847
gun,0.006811
guy,0.007880
h,0.004029
ha,0.003868
hagberg,0.029350
hall,0.005503
hambledon,0.039259
hamilton,0.021597
hannay,0.014675
hard,0.004761
harlot,0.013847
harm,0.005592
harold,0.006961
harri,0.012201
hashishsmok,0.014675
havana,0.009668
hawk,0.015406
head,0.003744
heart,0.005324
held,0.003429
helen,0.016387
helm,0.019591
help,0.003175
henri,0.009350
hero,0.027520
heroic,0.008496
heroin,0.009130
hi,0.036413
higgin,0.010183
high,0.002937
higson,0.013847
hill,0.011540
hillhous,0.013847
histor,0.013034
histori,0.004832
holm,0.008243
homeland,0.007504
hone,0.010790
honor,0.006000
horowitz,0.010479
horror,0.016591
hors,0.012532
howard,0.013458
howev,0.004708
hugh,0.006807
human,0.002794
humor,0.007856
hunt,0.017563
hurt,0.007604
husband,0.007556
hva,0.014675
hybrid,0.012120
hynd,0.013302
ian,0.028648
id,0.015962
ident,0.004153
ideolog,0.016438
ignatiu,0.010562
ii,0.007592
iliad,0.009692
illeg,0.005845
imag,0.004466
imit,0.006862
immin,0.008338
immor,0.008097
imperi,0.022603
impetu,0.022454
import,0.022266
imposs,0.004904
includ,0.054369
increas,0.002789
independ,0.002979
infiltr,0.016407
influenti,0.004665
inherit,0.010758
initi,0.006303
innoc,0.007667
insid,0.024325
inspir,0.009598
instal,0.005724
integr,0.003500
intellig,0.049787
intern,0.010167
interpret,0.003869
interwar,0.016427
intric,0.008070
intrigu,0.008264
introduc,0.020416
intrud,0.008900
invad,0.005801
invari,0.006413
invas,0.010579
involv,0.011248
ipcress,0.014675
irish,0.006880
iron,0.005195
isaev,0.014675
israel,0.017821
issu,0.006225
itali,0.005220
itw,0.013847
j,0.010928
jack,0.013716
jackal,0.011375
jagger,0.013552
jame,0.036820
jan,0.006374
jason,0.008885
jefferi,0.010252
jeopardi,0.010183
jerusalem,0.007755
jet,0.007095
jim,0.007667
jimmi,0.008595
joe,0.008316
john,0.029694
johnni,0.010288
join,0.004314
jonathan,0.006782
jordan,0.007309
joseph,0.019120
journalist,0.013656
journey,0.006741
jr,0.012322
julian,0.007478
june,0.004226
jurgen,0.012179
k,0.004397
kaplan,0.009296
ken,0.016103
kenneth,0.013017
kid,0.008659
kill,0.019154
killer,0.008740
kim,0.008107
kingsley,0.010479
kipl,0.010840
known,0.002423
kremlin,0.011115
krupp,0.011865
kyle,0.010562
la,0.004841
labyrinth,0.010054
land,0.003670
larg,0.002555
larsson,0.011523
late,0.006715
later,0.008652
launch,0.004884
lauri,0.009795
le,0.046216
lead,0.005787
leadup,0.023204
leagu,0.005950
leather,0.008295
lee,0.006594
leftlean,0.010562
leftw,0.007998
len,0.007356
leon,0.007215
leroux,0.011963
lesli,0.008193
level,0.005799
lewint,0.014675
licenc,0.008306
light,0.003998
lighterton,0.014675
like,0.005180
limit,0.002961
line,0.003470
link,0.002108
lion,0.007550
lisbeth,0.014675
lisbon,0.008496
list,0.008089
liter,0.005576
literari,0.012163
literatur,0.012998
littel,0.027695
live,0.006271
long,0.003148
longhair,0.013302
longrun,0.007896
lose,0.005100
loss,0.004241
love,0.005757
low,0.003716
lucern,0.013302
ludlum,0.013302
lynd,0.012724
m,0.003468
macgyv,0.044025
macinn,0.044025
mackenzi,0.019538
mackintosh,0.022354
maclean,0.011240
mailer,0.013552
maintain,0.003381
major,0.005145
maker,0.006689
maksim,0.013302
male,0.005439
man,0.042905
mani,0.010959
mansfield,0.010085
manuscript,0.006580
march,0.004021
margin,0.005314
maritim,0.006242
market,0.003629
marlow,0.011057
mask,0.015377
masquerad,0.010562
matt,0.017831
matter,0.003647
matthew,0.014063
maugham,0.027104
max,0.011851
mcalpin,0.013552
mccarri,0.041543
mcnab,0.038174
medium,0.005528
meet,0.003923
member,0.003093
memorandum,0.025709
men,0.008993
menac,0.018712
merchant,0.005759
merit,0.006920
metal,0.010094
metamorphosi,0.010520
metaphys,0.006204
meyer,0.008183
mi,0.030571
michael,0.013388
middleag,0.010183
middleclass,0.008295
miernik,0.013847
mike,0.008051
millennium,0.006052
million,0.003808
miniseri,0.010288
ministri,0.005214
mission,0.004835
mitch,0.010790
mitchel,0.016251
mixtur,0.006063
modern,0.005817
mole,0.008713
moment,0.005516
moolman,0.014675
moot,0.010362
moral,0.004851
morpurgo,0.014209
morrel,0.011772
moscow,0.006947
motiv,0.004737
mould,0.008947
movi,0.006632
muchamor,0.014675
munich,0.008415
munro,0.011375
murphi,0.008659
mysteri,0.006400
narr,0.006381
nation,0.005374
nativ,0.005012
natur,0.005177
nazi,0.027046
nbc,0.010085
needl,0.017090
neg,0.004204
neighbour,0.005923
nelson,0.007327
nemes,0.014675
network,0.004053
new,0.017328
news,0.005292
nick,0.016270
night,0.011895
nikita,0.009619
nineteenth,0.006065
nineteenthcenturi,0.008533
nixon,0.008633
nkvd,0.011447
noel,0.009220
nolf,0.014675
nonprofit,0.006993
norman,0.006832
north,0.003850
notabl,0.033488
note,0.005591
noteworthi,0.008193
notic,0.005503
novel,0.171743
novelist,0.058519
number,0.004851
obrin,0.014675
observatori,0.007497
obviou,0.006125
occur,0.012323
octob,0.004193
odyssey,0.008545
offic,0.023419
oiorpata,0.029350
olen,0.013552
oligarchi,0.008978
olivia,0.011375
open,0.003319
oper,0.003059
oppenheim,0.018794
orczi,0.013847
organ,0.002645
organis,0.004591
orient,0.005052
origin,0.002687
oss,0.012570
ostens,0.007856
otto,0.013664
overkil,0.012570
overlap,0.005389
packag,0.006147
padraig,0.014675
pale,0.009418
palestinian,0.008381
pari,0.005302
park,0.017002
parodi,0.026794
parti,0.003747
pascal,0.007543
path,0.004916
patriot,0.007075
patterson,0.009668
paul,0.004215
peac,0.004676
penetr,0.006477
peopl,0.002673
perceiv,0.004876
perfect,0.005417
perhap,0.009015
period,0.043219
person,0.006346
perspect,0.004339
peter,0.004421
phelan,0.012895
phenomenon,0.004994
philip,0.005960
phillip,0.015008
pictur,0.005428
pimpernel,0.028418
piraci,0.008595
pit,0.007681
plan,0.007344
playboy,0.012299
player,0.012692
pleasur,0.007209
plot,0.005943
polit,0.018260
politicomilitari,0.023927
popular,0.020815
populist,0.008620
porter,0.008183
portrait,0.007864
portray,0.012623
post,0.004706
postattack,0.013302
postcold,0.021211
postdx,0.014675
posthum,0.007921
potent,0.008726
potevka,0.014675
power,0.008536
preemin,0.008060
prepubl,0.011685
present,0.005560
press,0.003272
prevent,0.003975
price,0.004116
prison,0.005901
privat,0.011961
prochnow,0.014675
produc,0.002893
programm,0.016034
prolif,0.007929
prolifer,0.006845
propagandist,0.011306
prosaic,0.010945
protagonist,0.042021
prove,0.004291
provinc,0.005140
provok,0.007115
pseudonym,0.018022
psycholog,0.004739
public,0.002800
publicli,0.005950
publish,0.021604
qualiti,0.008286
queen,0.005835
queux,0.027104
quiet,0.008144
quiller,0.044025
r,0.003720
race,0.005462
ramsdel,0.012724
ran,0.006224
rang,0.016689
rapp,0.013086
rat,0.007380
raymond,0.014496
read,0.003062
real,0.003660
realism,0.006880
realist,0.006060
reawaken,0.010439
receiv,0.003369
recent,0.003053
recognis,0.005444
recur,0.015060
red,0.024440
refer,0.001763
reflect,0.007579
reginald,0.009527
reign,0.006097
relat,0.002377
releas,0.004407
reluct,0.007188
remark,0.005408
remembr,0.009905
reminisc,0.008485
remot,0.005970
report,0.003389
repris,0.009147
republ,0.004138
rescu,0.006998
resort,0.006544
restless,0.009993
resum,0.006745
review,0.003951
revolut,0.017474
revolutionari,0.016837
rhi,0.011057
richard,0.013070
riddl,0.019699
ride,0.007667
rider,0.009044
rifl,0.008327
rift,0.008097
rightward,0.010790
rimington,0.013847
ripost,0.012570
risk,0.004339
rivalri,0.021794
robert,0.023265
roger,0.005913
rogu,0.009439
role,0.003089
romanc,0.008203
root,0.004454
ross,0.007215
roug,0.009743
rouletabil,0.029350
royal,0.009306
rudyard,0.011447
run,0.003875
russia,0.005182
russian,0.025815
russoamerican,0.013847
ruthless,0.009258
ryan,0.027083
s,0.035016
sa,0.007770
sabotag,0.009061
saigon,0.010742
saint,0.005896
saland,0.014675
sallust,0.012299
sam,0.007832
sand,0.014562
sandbagg,0.014675
sardonic,0.014675
satir,0.008193
save,0.010015
sawkin,0.014675
scala,0.009963
scandal,0.007447
scarlatti,0.013302
scarlet,0.019986
schirmer,0.013552
schulz,0.010562
scienc,0.002966
scotsman,0.011306
sebastian,0.009316
second,0.017366
secondari,0.005021
secret,0.054411
sector,0.004623
sell,0.004988
seller,0.007046
semyonov,0.036204
send,0.005515
sent,0.009776
separ,0.006443
septemb,0.008505
sequel,0.034638
seren,0.010649
seri,0.105395
seriocom,0.013847
serv,0.010531
servic,0.010307
set,0.010927
seventeen,0.008285
sexi,0.011057
seymour,0.009095
sharp,0.005960
sherlock,0.010479
shooter,0.010520
shot,0.006858
sierra,0.008243
silenc,0.007864
silva,0.009202
similar,0.002930
simon,0.005896
sixpenc,0.012724
skirrow,0.013302
slay,0.011772
smart,0.007710
smiley,0.012570
smithcum,0.014675
snatch,0.010945
social,0.003122
societi,0.006150
sold,0.005182
soldier,0.005584
solid,0.005274
somerset,0.020576
sometim,0.003195
south,0.003843
soviet,0.026580
sovietamerican,0.013086
spain,0.010431
sparrow,0.010520
special,0.003069
spi,0.758773
splinter,0.008947
spook,0.011865
sport,0.005828
spread,0.004338
spring,0.005535
spycatch,0.013847
spyfi,0.027695
spyhunt,0.014675
spyland,0.014675
spyrel,0.014675
spywis,0.014675
staff,0.005785
stain,0.008088
stamboul,0.013847
star,0.010885
start,0.003106
stasi,0.009643
state,0.002147
station,0.016085
steinhauer,0.013847
stella,0.010479
step,0.008323
stephen,0.010903
stieg,0.014209
stierlitz,0.029350
stori,0.049751
stranger,0.008607
strateg,0.005356
strategi,0.004571
stripp,0.013847
structur,0.002867
struggl,0.005144
style,0.010114
subgenr,0.018712
submit,0.006147
subsequ,0.003830
success,0.019528
successor,0.005513
suffer,0.004479
superior,0.005462
superspi,0.014675
support,0.002918
supremaci,0.008024
surviv,0.004232
suspens,0.007624
suspicion,0.007888
swap,0.007484
swede,0.010252
swedish,0.013387
swiftli,0.009078
syphon,0.013847
syria,0.007215
syriana,0.013847
t,0.004270
tactic,0.006526
talbot,0.010085
tank,0.006798
tass,0.012895
team,0.005125
tear,0.007872
technolog,0.007077
technothril,0.025791
ted,0.007981
televis,0.038724
tell,0.005528
templar,0.010892
temporari,0.005788
tension,0.005574
term,0.004638
terror,0.019600
terrorist,0.022285
testament,0.007688
theft,0.008193
themat,0.008557
theme,0.021742
themselv,0.007281
thi,0.025700
think,0.017025
thinli,0.010840
thirdperson,0.010945
thirlbi,0.014675
thirtynin,0.021485
thoma,0.017561
thor,0.010023
threat,0.020591
thriller,0.102180
thu,0.002869
thursday,0.009220
tiger,0.008243
tilt,0.008754
time,0.010857
titl,0.008810
tl,0.010023
tokyo,0.007777
tom,0.006733
tone,0.007447
tool,0.003929
tourist,0.006540
trade,0.003550
tradecraft,0.027695
tradit,0.003090
train,0.004238
transmit,0.006013
trash,0.010790
treason,0.008726
trevor,0.009482
tunnel,0.007410
twelv,0.006474
twelvepart,0.014675
twentieth,0.005790
twentyfourth,0.011963
uk,0.008997
uncanni,0.010288
uncl,0.007848
uncommon,0.007523
undercov,0.010649
underpin,0.007157
unfaith,0.012570
unlik,0.008174
upperclass,0.010439
usa,0.005479
use,0.002005
ussr,0.021272
usual,0.014767
uvr,0.011685
v,0.004376
valeri,0.009795
vehicl,0.005572
vengeanc,0.010218
venic,0.008042
vet,0.009963
vicari,0.009717
video,0.010803
vietnam,0.006651
view,0.003145
villain,0.009527
villier,0.011177
vinc,0.011685
voic,0.005849
volumin,0.010218
von,0.009955
w,0.016472
wa,0.030185
wall,0.005196
wallac,0.007504
war,0.146432
warfar,0.006224
wartim,0.007816
water,0.007571
waterg,0.011865
way,0.002624
wear,0.006651
websit,0.004640
weigh,0.006915
wellwritten,0.011865
west,0.004214
western,0.011075
wheatley,0.022354
white,0.004659
wide,0.006322
wife,0.018982
william,0.035395
wilson,0.012261
women,0.004670
won,0.004968
wont,0.008533
wood,0.005663
work,0.014436
workingclass,0.009277
world,0.037324
worldcat,0.010520
wrap,0.008223
wreath,0.011447
wreck,0.008768
write,0.042211
writer,0.065484
written,0.027200
wrote,0.016726
y,0.005226
year,0.004953
yearold,0.007151
yesterday,0.009668
york,0.003780
younger,0.006097
youngest,0.008449
zaragoza,0.011447
zenda,0.013552
zero,0.005161
zone,0.010145
zoo,0.008885
