account,0.006502
accur,0.018812
actual,0.007066
adenin,0.018558
agct,0.027407
agenc,0.008685
aim,0.015767
algorithm,0.011068
align,0.023854
allianc,0.010325
allow,0.010980
alreadi,0.008043
alway,0.007424
analysi,0.019966
ancienthuman,0.029044
ani,0.004865
anim,0.015455
annot,0.075591
anoth,0.005310
answer,0.009830
anyth,0.010911
approach,0.012708
approxim,0.007524
archaean,0.021662
argu,0.007344
aspect,0.007256
assembl,0.068951
attach,0.010452
autom,0.012354
autosom,0.020024
background,0.009482
bacteria,0.012674
bacterium,0.050737
base,0.019242
becaus,0.010022
becom,0.010658
bee,0.015612
better,0.007507
bgi,0.024879
bioinformat,0.016197
biolog,0.023686
biotechnolog,0.014501
bovin,0.018992
caenorhabd,0.020508
caveat,0.018399
center,0.021334
chang,0.010053
cheaper,0.014356
chimpanze,0.052317
chloroplast,0.017676
chromosom,0.130411
code,0.017256
collect,0.006504
combin,0.006358
commerci,0.017086
common,0.016176
compani,0.014082
compar,0.006595
complet,0.073907
complex,0.006525
comput,0.014778
confin,0.011973
consortium,0.015612
contain,0.012795
contig,0.026328
continu,0.005403
cost,0.015681
cow,0.015378
creat,0.021819
crop,0.011025
cytosin,0.019183
databas,0.041733
decid,0.008691
describ,0.005489
desir,0.008393
detect,0.019586
determin,0.030965
develop,0.013380
differ,0.009455
difficult,0.023334
diseas,0.009341
divers,0.008577
dna,0.161132
domest,0.009240
draft,0.023018
echinobas,0.029044
echinoderm,0.020223
elegan,0.018900
emphasi,0.019477
employ,0.007121
encod,0.013074
endeavour,0.015501
england,0.009316
entir,0.006964
error,0.010209
especi,0.006234
essenti,0.007463
est,0.014916
eukaryot,0.029577
everi,0.006718
evolut,0.008717
exampl,0.009616
extern,0.004198
fallen,0.013045
far,0.007280
featur,0.007261
finish,0.012354
fractur,0.016197
fungu,0.018767
futur,0.006992
gene,0.100258
genet,0.019503
genom,0.795316
genomeencod,0.029044
giga,0.021662
given,0.005758
global,0.007624
goal,0.007896
golden,0.011633
goldgenom,0.029044
grape,0.017354
grown,0.011056
guanin,0.019335
ha,0.030625
hapmap,0.026822
health,0.008003
help,0.012570
high,0.005813
highli,0.007558
histor,0.012898
homo,0.045149
honey,0.017326
hors,0.012401
howev,0.009318
human,0.060844
humans,0.026328
ident,0.008220
identifi,0.013880
illumina,0.026328
impact,0.007867
import,0.024482
improv,0.014129
includ,0.019927
increas,0.005520
indel,0.021557
individu,0.005921
inform,0.028538
institut,0.006075
intern,0.010062
invertebr,0.015245
involv,0.011131
joint,0.010109
junk,0.017585
k,0.008704
knome,0.029044
knowledg,0.007220
known,0.004796
landmark,0.027028
larg,0.015175
larger,0.007576
largescal,0.011184
life,0.006170
like,0.010253
link,0.008347
livestock,0.013547
locat,0.021171
long,0.006231
machin,0.009266
major,0.005091
mammal,0.012150
mani,0.017351
map,0.026340
mean,0.005285
meant,0.019687
medic,0.008838
merg,0.011559
microbiom,0.021771
million,0.007538
mitochondria,0.017382
model,0.011956
molecular,0.020062
mosquito,0.018640
mrna,0.017707
nation,0.005318
neanderth,0.036424
neanderthalensi,0.022806
need,0.005769
new,0.008573
newer,0.014093
noncod,0.019183
novo,0.018176
nrcpb,0.029044
nucleotid,0.030726
number,0.014403
numer,0.007110
obtain,0.007459
occur,0.006097
offer,0.007136
oligonucleotid,0.020155
onli,0.013253
onlin,0.008399
organ,0.047114
organel,0.016746
origin,0.015956
overlap,0.021333
packag,0.012166
pair,0.030682
palaeoeskimo,0.029044
pan,0.015865
partial,0.008828
particular,0.012014
particularli,0.013461
path,0.009730
pathogen,0.015439
percent,0.009639
perspect,0.008588
physic,0.006280
piec,0.063590
place,0.010855
plant,0.033329
posit,0.005687
possibl,0.005613
potenti,0.007125
power,0.005631
predict,0.008052
present,0.005502
previou,0.008299
prior,0.008312
privat,0.015782
problem,0.005885
process,0.026013
produc,0.011452
program,0.006922
project,0.143003
projectwrit,0.029044
proport,0.009099
protein,0.011180
proteincod,0.021662
protist,0.017526
provid,0.004882
question,0.006911
quicker,0.017770
quickli,0.008957
rare,0.009056
read,0.024247
refer,0.010470
region,0.018750
relat,0.004706
relev,0.009129
repeat,0.020148
repetit,0.014237
report,0.006708
repres,0.005725
represent,0.008805
research,0.011324
resequenc,0.049200
result,0.004861
role,0.006114
s,0.004950
sapien,0.033267
scaffold,0.036647
scienc,0.005870
scientif,0.007023
scientist,0.007981
sea,0.008905
secondari,0.009939
separ,0.012753
sequenc,0.426344
seri,0.006953
set,0.005406
sex,0.012084
short,0.022325
shortli,0.010863
shotgun,0.040869
sinc,0.004857
singl,0.013001
small,0.011887
snp,0.020362
softwar,0.030874
sourc,0.006110
spbase,0.029044
speci,0.044668
spur,0.013642
statu,0.008112
steadili,0.012813
structur,0.005675
superfamili,0.020024
technolog,0.028014
term,0.009180
themselv,0.007205
thi,0.018166
thousand,0.017132
thu,0.005678
thymin,0.019183
time,0.004297
togeth,0.006698
tomato,0.016655
tradit,0.006116
troglodyt,0.023126
ultim,0.008401
understand,0.019780
urchin,0.018946
use,0.003969
usual,0.011691
util,0.008787
variat,0.009105
vector,0.011747
veri,0.011094
viru,0.014801
wa,0.007965
way,0.005193
work,0.009524
worm,0.015304
