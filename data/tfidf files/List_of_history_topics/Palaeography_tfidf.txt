abandon,0.002613
abbey,0.004503
abbrevi,0.023853
abolish,0.002923
abov,0.008955
abstract,0.002461
abugida,0.006467
accent,0.028534
accept,0.001756
accid,0.010118
accomplish,0.005623
accord,0.005595
account,0.006591
achaemenid,0.009697
acquir,0.006836
acta,0.008927
activ,0.001395
actual,0.001790
actuaria,0.007126
acut,0.003671
ad,0.010712
adapt,0.004455
addendum,0.005860
addit,0.004246
adequ,0.002934
administr,0.001956
admir,0.003314
admit,0.002950
admittedli,0.005318
adopt,0.021237
adpl,0.014252
advanc,0.001727
advent,0.003059
affect,0.007312
age,0.008760
agent,0.002385
agre,0.002109
agreement,0.002135
ahiqar,0.007126
aid,0.002098
aim,0.001997
akkadian,0.010181
album,0.004561
aleph,0.005318
alexandria,0.003813
alexandrian,0.004939
alexandrinu,0.006467
align,0.003022
alik,0.003770
allow,0.002782
allud,0.004273
alon,0.002392
alphabet,0.060639
alreadi,0.006114
alter,0.005019
altern,0.001797
amaravati,0.006233
amus,0.004578
analog,0.002499
anatolian,0.005235
ancestor,0.006013
ancient,0.034459
andhra,0.005178
angl,0.009473
anglosaxon,0.021849
angular,0.041506
ani,0.008630
anim,0.001958
anima,0.004899
annual,0.002149
anoth,0.008073
antica,0.011999
antiqua,0.006052
antiquari,0.005741
antiquior,0.007126
apabhraa,0.006944
apart,0.007691
apolloniu,0.010356
appear,0.055095
appli,0.002991
applic,0.001670
approach,0.003220
approxim,0.003813
apt,0.004552
apulia,0.006108
arab,0.018088
arabian,0.003889
arama,0.169705
aramaean,0.018701
archaic,0.008022
archetyp,0.004099
architectur,0.005277
archiv,0.002575
area,0.009726
aristot,0.005860
aristotl,0.002884
art,0.006420
articl,0.003325
artifici,0.004725
artificiosi,0.007126
artist,0.008808
ashoka,0.010084
ashokan,0.013889
asia,0.002402
aspect,0.001838
assign,0.007538
associ,0.001366
assum,0.001938
athen,0.003866
attain,0.008496
attempt,0.004787
attent,0.002281
attest,0.007366
atticu,0.006052
attribut,0.002174
augustan,0.005818
augustin,0.007514
austria,0.003236
authent,0.014174
author,0.006300
authoris,0.004149
avail,0.003420
avoid,0.008358
avroman,0.007126
awkward,0.004939
b,0.003706
babylonia,0.004643
babylonian,0.003803
backward,0.003479
badami,0.006304
banavasi,0.006381
barbarian,0.013171
bari,0.004939
base,0.007314
basi,0.005227
bc,0.062394
bear,0.002618
beauti,0.016026
becam,0.016961
becaus,0.002539
becom,0.022956
befor,0.010085
began,0.015160
begin,0.019738
begun,0.002682
believ,0.001744
bello,0.005215
belong,0.014301
benedictin,0.025984
beneventana,0.014252
benevento,0.006796
bengal,0.004426
bernard,0.003285
berthold,0.005670
besid,0.005772
best,0.007437
betray,0.004089
better,0.001902
bibl,0.006765
biblic,0.010872
bibliographi,0.002453
bishop,0.003452
bisticci,0.007126
blacklett,0.013593
block,0.002512
bobbio,0.005904
boccaccio,0.011149
bold,0.012434
boldest,0.005999
book,0.020842
bookhand,0.185281
booksel,0.005011
border,0.002484
borrow,0.005599
boundari,0.004710
boxhead,0.007126
bracciolini,0.006168
brahmi,0.017337
branch,0.001793
breadth,0.008014
breath,0.014507
brhm,0.045271
britain,0.007129
britannica,0.006861
british,0.001883
broad,0.006797
broader,0.002812
broken,0.005625
bronz,0.010185
brought,0.004158
bruce,0.003466
brunhilda,0.006944
buddhist,0.003393
bull,0.003893
bureaucraci,0.003757
burley,0.005904
busi,0.001901
byzantin,0.020823
c,0.006457
calligraph,0.050449
calligraphi,0.005235
cambridg,0.002237
came,0.007389
campaign,0.002536
canterburi,0.004544
capit,0.038587
capitular,0.007126
care,0.004245
career,0.002771
careless,0.004899
carelessli,0.005741
carolin,0.004426
carolingian,0.051288
carri,0.007285
case,0.004237
castigata,0.007126
catalogu,0.003852
caution,0.003976
cave,0.006794
ce,0.003095
ceas,0.002853
center,0.001802
centr,0.004646
central,0.001583
centuri,0.201664
ceremoni,0.003118
certain,0.026288
certainli,0.006450
cf,0.007620
chalukya,0.017115
chancelleri,0.005860
chanceri,0.049403
chang,0.016559
chaotic,0.003870
char,0.005215
charact,0.049029
characteris,0.006773
characterist,0.023369
charlemagn,0.009038
chart,0.003218
charter,0.006078
chera,0.005950
chiefli,0.003663
chisel,0.005462
chisholm,0.004605
chola,0.010249
christendom,0.004587
christian,0.004379
chronolog,0.003368
chutu,0.007126
cicero,0.004209
cil,0.025869
circl,0.005445
circular,0.003292
clara,0.004702
class,0.011244
classic,0.005600
classif,0.002493
classifi,0.006856
clear,0.002102
clearli,0.007621
clerk,0.003893
climax,0.004369
close,0.012094
closer,0.002795
club,0.003276
cluniac,0.005999
coars,0.004503
coast,0.002578
codex,0.004615
codic,0.010431
coin,0.002339
collect,0.001648
coloni,0.002336
coluccio,0.006108
columba,0.005741
combin,0.003222
come,0.004848
comma,0.005215
comment,0.005413
commentarium,0.006944
commerci,0.002164
common,0.010930
compact,0.003333
comparison,0.002369
compet,0.004659
competit,0.004443
complet,0.001560
complex,0.001653
complic,0.005121
compos,0.002143
comprehens,0.009956
compress,0.010466
compris,0.002311
compromis,0.012994
concern,0.003172
conclus,0.005035
concurr,0.003484
condit,0.001581
confid,0.002890
confin,0.003034
confront,0.003121
confus,0.002543
connect,0.003721
connot,0.003734
conquest,0.006089
consequ,0.011529
conserv,0.002243
consid,0.005271
consider,0.013293
consist,0.004537
consolid,0.002967
conson,0.004899
consonant,0.006168
constant,0.004493
constantinopl,0.003908
constantli,0.003150
constitut,0.003568
construct,0.001749
contact,0.002371
contain,0.011348
contemporari,0.006631
content,0.002229
context,0.003975
contin,0.002911
continent,0.003009
continu,0.015061
contract,0.004533
contribut,0.003317
controversi,0.002226
conveni,0.003017
convers,0.004662
copi,0.010897
copper,0.003138
copperpl,0.005637
copyist,0.011274
corbi,0.012608
coroni,0.006467
corpu,0.003823
correct,0.002275
correspond,0.004102
counterpart,0.003030
coupl,0.002614
cours,0.009847
crab,0.004441
cramp,0.010978
creativ,0.002919
credenti,0.004209
criteria,0.002829
critic,0.008311
cross,0.002466
crossbar,0.005818
crossstrok,0.028504
culmin,0.005988
cultur,0.005097
cuneiform,0.018689
curia,0.005489
curious,0.005011
current,0.004340
cursiv,0.508557
curv,0.016681
custom,0.002285
cut,0.002399
d,0.007317
da,0.003008
dalmatia,0.005297
daniel,0.002810
dariu,0.005026
data,0.003606
databas,0.002643
databl,0.005999
date,0.060437
davi,0.003276
dawn,0.003576
day,0.003410
debat,0.002103
decad,0.002054
deciph,0.016727
decipher,0.005462
decis,0.001905
declin,0.010727
decor,0.003668
deed,0.003920
deepli,0.003190
definit,0.005360
degener,0.003956
degre,0.005551
deliber,0.003024
delicaci,0.010594
delisl,0.006168
depend,0.001491
deriv,0.012027
descend,0.002735
describ,0.002781
desir,0.002126
despatch,0.005489
despit,0.001916
deterior,0.006656
determin,0.004707
devanagari,0.006052
develop,0.053117
diachron,0.005637
diagon,0.004226
dialect,0.006648
did,0.015097
differ,0.022761
difficult,0.009854
difficulti,0.004881
diffus,0.006174
digit,0.005046
dignifi,0.005545
diocletian,0.005042
diploma,0.004304
diplomat,0.005369
diplomatica,0.013889
direct,0.006002
directli,0.001815
directori,0.007586
disagr,0.003213
disappear,0.017238
discard,0.003779
discern,0.007426
disciplin,0.010411
discov,0.007960
discuss,0.005531
disintegr,0.011085
disorderli,0.005516
displac,0.002899
display,0.002568
disproportion,0.003943
disput,0.002273
distanc,0.002406
distinct,0.022625
distinguish,0.016050
distort,0.003190
distribut,0.001864
disturb,0.003173
dition,0.005341
diverg,0.003054
divid,0.008959
divis,0.003917
document,0.076360
documentari,0.006945
doe,0.001529
domain,0.002207
domin,0.003719
dot,0.003651
doubl,0.002501
doubt,0.002916
downward,0.003626
draft,0.002916
draw,0.002316
drawn,0.005333
drift,0.003484
droop,0.006233
ductu,0.011808
dura,0.005741
dure,0.013008
dynasti,0.008815
e,0.009155
earli,0.043544
earlier,0.008041
earliest,0.025014
eas,0.003132
easi,0.005526
easier,0.002913
easili,0.006992
east,0.010458
eastern,0.006774
ed,0.003809
edict,0.012481
edit,0.006388
educ,0.003466
edward,0.002474
effect,0.013217
effort,0.001884
egypt,0.031604
egyptian,0.012265
eighti,0.004065
elabor,0.005662
elaps,0.004615
eleg,0.012169
element,0.001730
elephantin,0.013593
elimin,0.004705
elm,0.005318
elong,0.004214
elsewher,0.005609
email,0.003976
embrac,0.003069
emerg,0.001743
emin,0.003870
emphat,0.004734
empir,0.013288
employ,0.014437
enabl,0.002157
encount,0.002679
encyclopaedia,0.004298
encyclopdia,0.003528
end,0.017850
endur,0.003320
engag,0.002154
england,0.002360
english,0.003789
engrav,0.017506
enlarg,0.013900
ensu,0.003326
entail,0.003174
entir,0.001764
epigraph,0.027874
epigraphi,0.011808
epigraphist,0.006944
epistl,0.004967
epistolari,0.040453
equal,0.001852
era,0.010747
especi,0.023697
essay,0.002636
essenc,0.003073
essenti,0.005673
establish,0.014519
europ,0.012913
everyday,0.002906
evid,0.005565
evolut,0.006626
evolutionari,0.002680
evolv,0.015632
exact,0.012977
exactli,0.002665
exampl,0.024369
exceedingli,0.004662
excel,0.002943
excess,0.002629
exclus,0.002324
execut,0.002060
exempla,0.007126
exercis,0.009350
exhibit,0.005136
exist,0.009470
experi,0.001673
expert,0.005374
explainedtwo,0.007126
exploit,0.002532
exposur,0.003023
extant,0.007199
extend,0.005383
extens,0.001837
extent,0.004477
extern,0.002127
extrem,0.003903
exuber,0.010594
exultet,0.007126
eye,0.002719
facil,0.002607
facsimil,0.004848
fact,0.005182
fall,0.005571
famili,0.001883
famou,0.002247
fantast,0.004419
far,0.018448
fashion,0.002863
fate,0.003435
fatigu,0.004519
favour,0.002675
featur,0.012879
feel,0.002543
fellow,0.003076
festal,0.006467
field,0.001485
file,0.006073
final,0.001759
financ,0.002155
fine,0.005817
finest,0.012912
fit,0.002476
flat,0.006223
flatten,0.004383
flee,0.003488
flexibl,0.003012
flight,0.002980
florenc,0.003916
florentin,0.009578
florid,0.006467
flourish,0.011977
flow,0.002142
follow,0.006982
font,0.004873
foreshadow,0.004363
forgeri,0.004899
form,0.076729
formal,0.018204
format,0.009921
formula,0.002540
formulari,0.005462
fortun,0.003300
founder,0.002549
fraction,0.002847
fragment,0.008584
fragmentari,0.004926
frame,0.002780
franc,0.006248
free,0.001661
freedom,0.009186
french,0.015663
frequent,0.006135
fulli,0.002193
fundament,0.001863
furnish,0.004139
furthermor,0.002493
g,0.001968
gain,0.001769
gall,0.004744
gap,0.005664
gaudi,0.006304
gaul,0.009447
gave,0.004233
gener,0.025089
genuin,0.003374
geograph,0.002366
german,0.002054
germani,0.006571
given,0.005836
goal,0.002001
gold,0.002529
good,0.003234
got,0.003282
gothic,0.039086
grace,0.003834
gradual,0.030402
graeca,0.006168
graffiti,0.005411
grammar,0.003618
grant,0.002230
grantha,0.012337
graphein,0.005904
great,0.016396
greater,0.003664
greatli,0.002382
greec,0.002749
greek,0.047382
gregori,0.007077
grew,0.004862
griffin,0.004578
ground,0.002116
group,0.001312
grow,0.007462
grown,0.002801
gupta,0.023216
h,0.012125
ha,0.017461
habit,0.003084
half,0.012015
halfunci,0.064135
halmidi,0.006944
hand,0.192878
handbook,0.002944
handsom,0.036377
handwrit,0.052938
hang,0.003896
hannibalico,0.007126
happen,0.002299
hard,0.002387
hardli,0.007778
hast,0.004511
head,0.001878
heavi,0.007543
heavier,0.003674
heavili,0.002345
hebrew,0.014607
height,0.011602
hellenist,0.011094
help,0.001592
henad,0.006796
henc,0.002269
herculaneum,0.011149
heretofor,0.005318
hi,0.010435
hieroglyph,0.009447
high,0.007365
highli,0.005745
histor,0.008171
histori,0.010905
historian,0.002451
hitler,0.003964
hittit,0.004939
homeland,0.003763
honoriu,0.005950
horizont,0.013420
howev,0.008264
hugh,0.003413
hullman,0.007126
humanist,0.024737
identif,0.002865
identifi,0.005275
ideograph,0.006052
ii,0.005711
iii,0.005564
ikshvaku,0.014252
illeg,0.002931
illustr,0.009923
imit,0.013766
immedi,0.004277
imperi,0.025506
implement,0.002025
import,0.009926
impress,0.008908
improv,0.005370
incept,0.003786
incis,0.004824
includ,0.002019
incomplet,0.003170
incorpor,0.001997
increas,0.002797
inde,0.002473
independ,0.001494
india,0.049000
indian,0.007224
indic,0.007158
individu,0.009003
indologist,0.006052
indu,0.008374
ineleg,0.006233
inequ,0.003060
inexpert,0.006168
infer,0.002795
inferior,0.003541
infinit,0.002848
influenc,0.027240
influenti,0.002339
inform,0.004338
infrequ,0.004390
inherit,0.005395
initi,0.004742
innov,0.005039
innumer,0.004672
inscript,0.121767
inscription,0.006671
inscriptionum,0.006796
insert,0.006749
instead,0.001703
insuffici,0.003298
insular,0.004279
intend,0.002252
interact,0.003630
interlac,0.006108
intern,0.002549
interpret,0.001940
interrupt,0.003618
interv,0.003065
interven,0.003126
intric,0.008094
introduc,0.006826
introduct,0.003857
intrus,0.004198
invas,0.007958
invent,0.004734
involv,0.004231
ireland,0.006081
irish,0.031054
irregular,0.013613
ital,0.004495
itali,0.020944
italian,0.024567
iv,0.006315
ivori,0.003952
j,0.001827
jame,0.002051
januari,0.004152
jean,0.006034
jesuit,0.004075
jewish,0.006031
john,0.001654
join,0.006491
journey,0.003380
just,0.003392
justif,0.003399
kadamba,0.012763
kannada,0.023274
karnataka,0.016310
kashmir,0.004587
kerala,0.004766
kharoh,0.012934
kharoshthi,0.014252
kind,0.003875
king,0.006951
know,0.008816
knowledg,0.007318
known,0.018232
kolezhuthu,0.007126
kosala,0.006381
kua,0.006052
kurrentschrift,0.007126
kushana,0.020014
kutila,0.007126
l,0.002141
la,0.002428
labour,0.002462
lack,0.003709
languag,0.043876
larg,0.006409
larger,0.003839
lastli,0.004149
late,0.010103
later,0.039055
latin,0.029718
latinarum,0.006796
latinspeak,0.012337
laymen,0.005058
lead,0.002902
leather,0.008320
leav,0.002059
lectioni,0.007126
led,0.003139
left,0.007755
legal,0.002007
legend,0.006853
legibl,0.022549
length,0.002413
leopold,0.004084
lesser,0.002981
letter,0.168065
lettera,0.013593
letterform,0.028504
librari,0.009140
libraria,0.052504
life,0.001563
lifeless,0.005489
ligatur,0.022820
liii,0.006796
like,0.012991
limit,0.001485
line,0.027851
linger,0.008838
link,0.009518
literari,0.042701
literati,0.005818
literatur,0.004345
littera,0.006168
litterarum,0.007126
littl,0.011168
liturg,0.004723
local,0.008236
lombard,0.029319
lombardi,0.005545
long,0.009474
longer,0.010029
loop,0.003349
loos,0.003023
lose,0.002557
loss,0.006381
lost,0.004227
low,0.001863
ludwig,0.006679
lux,0.011341
luxeuil,0.006944
luxurian,0.007126
lxxxix,0.006796
lyric,0.008767
m,0.001739
mabillon,0.013593
madhya,0.005860
main,0.006158
mainli,0.002019
maintain,0.005087
majuscul,0.095155
make,0.001236
malayalam,0.011999
mallon,0.006233
mandaic,0.006563
mani,0.008793
manifest,0.002797
manual,0.006143
manuscript,0.125405
manuscriptsand,0.007126
marchalianu,0.007126
margin,0.005330
mario,0.004214
mark,0.033194
markedli,0.004160
martin,0.002574
master,0.002595
masteri,0.004383
materi,0.013633
mathura,0.005741
matr,0.006304
matter,0.005488
maund,0.006796
maurya,0.009673
mean,0.002678
mechan,0.001795
mediaev,0.005058
medici,0.004624
mediev,0.018157
mediterranean,0.003181
medium,0.002772
mention,0.007063
mere,0.004785
merovingian,0.026820
mesopotamia,0.003698
met,0.002487
metal,0.007593
method,0.003012
methodolog,0.002574
metz,0.005574
middl,0.024061
midrd,0.006168
midth,0.003000
migrat,0.002598
militari,0.003934
minist,0.002194
minuscul,0.290644
minut,0.002902
missionari,0.003443
mix,0.009356
mixtur,0.006081
model,0.001514
modern,0.018963
modernday,0.003461
modif,0.014453
modifi,0.009491
moment,0.002766
monarch,0.003211
monasteri,0.011085
mongolia,0.004220
monk,0.021583
montfaucon,0.006944
monument,0.010445
moor,0.003282
moreov,0.005579
morpholog,0.009995
mostli,0.012449
mother,0.002802
movement,0.001676
ms,0.003654
multifari,0.005999
museum,0.005865
n,0.002184
nabataean,0.006168
nagari,0.014252
nagarjunakonda,0.014252
nailhead,0.014252
nandinagari,0.014252
narrow,0.010984
nation,0.010780
natur,0.006491
nd,0.022627
near,0.003999
necessari,0.001938
need,0.002923
neoassyrian,0.005318
new,0.018467
newcom,0.004682
ngar,0.013593
niccol,0.004561
niccoli,0.006671
nobl,0.003162
nonegyptian,0.007126
norman,0.003426
north,0.017378
northern,0.013641
notabl,0.003732
notari,0.010823
note,0.005608
noth,0.005147
notic,0.008280
novelti,0.004304
nt,0.004615
number,0.009733
numer,0.016215
numismat,0.005276
observ,0.001605
obsolet,0.003793
occasion,0.005486
occur,0.003090
occurr,0.003145
odyssey,0.004285
offer,0.005425
offici,0.007385
offshoot,0.004323
old,0.024147
older,0.002552
oldest,0.021046
onc,0.003634
onli,0.017911
onlin,0.006385
onward,0.024664
open,0.004994
oppos,0.002032
order,0.001340
orderli,0.004261
ordinari,0.002651
origin,0.026955
orissa,0.005950
ornament,0.012211
orthograph,0.005904
orthographi,0.009964
otherwis,0.002313
outdat,0.004316
outlin,0.002367
outset,0.004298
outsid,0.001855
oversimplifi,0.004824
overview,0.002436
owe,0.002959
p,0.001856
pack,0.003548
page,0.009536
pain,0.003080
palaeograph,0.069348
palaeographi,0.172722
palaeographia,0.007126
palai,0.005545
palatin,0.005196
paleograph,0.013889
paleographi,0.027187
palestianian,0.007126
palestin,0.003834
pallava,0.011274
palmyren,0.006381
pandya,0.012216
papal,0.025289
papebroch,0.007126
paper,0.010033
papyri,0.117471
papyrolog,0.013343
papyru,0.076811
paragrapho,0.007126
parallel,0.002519
parchment,0.015706
parent,0.002738
parthia,0.005297
particular,0.001522
particularli,0.005116
partli,0.010459
pass,0.001889
past,0.001929
patriarch,0.004061
pdf,0.005629
peculiar,0.003786
pen,0.011690
penetr,0.003248
peopl,0.002681
perceiv,0.002445
percept,0.002588
perfect,0.013585
perhap,0.004521
period,0.065025
perpendicular,0.004051
persa,0.006467
persian,0.012222
persianhav,0.007126
persist,0.008272
petrarch,0.020568
petrarchan,0.006168
petri,0.004812
phaedo,0.005779
phase,0.002338
philolog,0.004633
philologist,0.005196
phoenician,0.008838
phoenicianderiv,0.007126
phonet,0.009140
phrase,0.002930
pi,0.003495
pictograph,0.005489
pinpoint,0.004596
pl,0.004448
place,0.011002
plaster,0.005074
plato,0.003203
play,0.001755
poetri,0.003393
poggio,0.016912
point,0.012696
polit,0.003052
pompeii,0.010873
pope,0.003420
popular,0.001739
portend,0.006563
portion,0.004887
posit,0.002882
possibl,0.002844
postcarolingian,0.006467
postpetrarchan,0.007126
potsherd,0.012934
power,0.005708
practic,0.011936
practis,0.007337
pradesh,0.010023
prakrit,0.060382
precarolin,0.007126
precarolingian,0.007126
preced,0.002642
precis,0.011485
predomin,0.006362
prefect,0.004692
prefer,0.002135
presenc,0.002108
present,0.004183
preserv,0.014103
press,0.001641
presum,0.003113
pretenti,0.005605
prevail,0.008850
preval,0.003070
previou,0.002102
previous,0.002125
priest,0.003301
primit,0.009257
princip,0.021784
principl,0.003348
print,0.002618
printer,0.008659
privat,0.007998
prkrita,0.007126
probabl,0.010005
problem,0.002982
process,0.002636
produc,0.005804
product,0.002906
profus,0.010023
progress,0.005702
project,0.003451
prolong,0.007092
pronounc,0.003368
pronunci,0.003698
proper,0.002501
proport,0.002305
prose,0.003943
protobengali,0.006671
proverb,0.004824
provid,0.007423
provinc,0.002578
ptolema,0.030833
ptolemi,0.003793
public,0.004213
publish,0.003095
punctuat,0.026097
punjab,0.004886
purpos,0.009069
pursuer,0.006108
q,0.006246
qshape,0.007126
qualiti,0.002077
quantiti,0.002271
quarter,0.005801
quasiminuscul,0.007126
queen,0.002926
question,0.001751
quickli,0.002269
quit,0.016497
r,0.001866
rad,0.005215
rais,0.002056
rajan,0.005860
rang,0.001674
rapid,0.007250
rapidli,0.004702
rare,0.002295
ravenna,0.011558
rd,0.034960
rdth,0.006467
reach,0.008987
reaction,0.002267
read,0.010752
readili,0.006052
real,0.001835
realiti,0.002405
realli,0.002845
reason,0.001587
recal,0.006840
recent,0.003063
reciproc,0.003322
recognis,0.008191
record,0.009321
red,0.002451
reduc,0.006702
reedpen,0.007126
refer,0.002653
reform,0.004326
regard,0.001696
region,0.009502
regular,0.016961
regularli,0.003010
reim,0.005860
reinforc,0.003012
relat,0.003577
relev,0.002313
reli,0.002131
relic,0.004404
remain,0.007313
remark,0.002712
renaiss,0.011698
replac,0.010813
repres,0.010155
reproduc,0.003000
request,0.002688
requir,0.001392
rescript,0.005999
research,0.001434
resembl,0.008369
resist,0.004583
resort,0.003282
respect,0.001681
respond,0.002385
rest,0.002072
restrict,0.002049
result,0.006159
retain,0.002325
reus,0.004056
reveal,0.004652
revers,0.002434
revis,0.002642
reviv,0.005621
rich,0.002536
right,0.014284
rigor,0.002978
rise,0.005339
roll,0.013200
roman,0.052193
romana,0.005159
romanesqu,0.005545
rome,0.012079
room,0.002868
root,0.002234
roughli,0.002480
round,0.033621
rounder,0.012608
royal,0.004667
royalti,0.004033
rule,0.008072
rustic,0.029301
s,0.002508
said,0.001817
saintdeni,0.005779
sakakshatrapa,0.007126
salankayana,0.014252
salutati,0.006108
sanskrit,0.040294
sarabhapura,0.007126
saskta,0.006796
satakarni,0.006381
satavahana,0.005950
save,0.002511
say,0.001916
schniedewind,0.006671
scholar,0.011230
scholarli,0.009391
scholast,0.003830
school,0.005398
schubart,0.006796
scienc,0.004462
scientif,0.001779
scottish,0.003395
scribal,0.005860
scribe,0.041131
script,0.257999
scriptoria,0.012467
scriptsin,0.007126
scriptura,0.029301
scrittura,0.006796
search,0.002263
second,0.008709
secretari,0.005779
section,0.004382
secur,0.003758
seen,0.008676
selfcorrect,0.005042
semicurs,0.035631
semigoth,0.007126
semit,0.004682
sens,0.003639
sent,0.004902
sentenc,0.003023
separ,0.011310
seri,0.008809
seriou,0.002477
serv,0.001760
servic,0.003446
set,0.013700
settlement,0.002581
seventi,0.004139
sever,0.014115
shallow,0.003741
shape,0.023188
sharp,0.002989
sheikh,0.004448
short,0.001885
shorter,0.006297
shown,0.008531
siddhamatrika,0.014252
sign,0.006079
signific,0.001571
silver,0.003077
similar,0.007349
simpl,0.003911
simpli,0.002008
simplic,0.003561
simplif,0.004047
simplifi,0.008600
sinaiticu,0.006304
sinc,0.003692
singl,0.006589
singularli,0.011149
site,0.002073
size,0.017251
skill,0.004742
slender,0.005090
slight,0.003674
slightli,0.005335
slope,0.042643
small,0.004518
smooth,0.003264
socal,0.012418
sodasa,0.007126
soft,0.003142
sogdian,0.006052
someth,0.002366
sometim,0.016023
somewhat,0.005218
soon,0.011285
sort,0.002581
sought,0.002397
south,0.011565
southeast,0.003013
southern,0.006853
space,0.007590
spacious,0.012934
spain,0.010462
spanish,0.002661
speak,0.004720
speaker,0.003160
special,0.006157
specialistpalaeograph,0.007126
specif,0.005890
specimen,0.011451
speed,0.002517
spite,0.007092
spontan,0.003181
sporad,0.003927
sprawl,0.004953
spread,0.013054
spur,0.003457
squar,0.023470
sri,0.003581
st,0.021826
stabilis,0.004192
stage,0.004299
stand,0.004626
standard,0.004973
standardis,0.008823
state,0.004308
stcenturi,0.004643
steadi,0.003154
steadili,0.006494
stem,0.002766
stereotyp,0.004042
stiff,0.008331
stiffli,0.006796
stone,0.016494
stoudio,0.007126
straggl,0.007126
straight,0.010241
strength,0.004992
stress,0.002524
strike,0.002737
strikingli,0.009993
stroke,0.102231
strongli,0.002372
struck,0.003500
structur,0.001438
stterlin,0.007126
studi,0.009105
style,0.088767
stylist,0.013192
subcontin,0.007586
subdivid,0.003372
subgroup,0.003874
subject,0.003131
subsect,0.004734
subsequ,0.003841
success,0.006529
successor,0.002764
suffici,0.002275
suggest,0.006892
summari,0.002962
sumptuou,0.006233
superflu,0.004615
supersed,0.010668
supplant,0.015727
supplement,0.006094
suppli,0.002006
support,0.002927
surfac,0.004457
survey,0.004732
surviv,0.025475
sweep,0.003770
syllab,0.005436
syllabl,0.004723
symptom,0.003502
synopt,0.004982
syntax,0.003896
syria,0.007237
syriac,0.009325
systemat,0.004703
t,0.008566
tabl,0.002368
tablet,0.007848
tackl,0.003789
tail,0.003563
taken,0.005406
tall,0.011748
tamil,0.013391
tangl,0.005011
taught,0.005488
taxreceipt,0.007126
technic,0.002189
techniqu,0.001891
technolog,0.001774
telugu,0.017456
telugukannada,0.007126
tend,0.008085
tendenc,0.011056
tenuou,0.004692
terenc,0.004633
term,0.003489
terracotta,0.005364
test,0.002012
text,0.033973
textual,0.003866
th,0.167957
themselv,0.003651
theodelind,0.007126
therefor,0.005019
thi,0.049716
thicken,0.005042
thicker,0.009384
thirtyeight,0.005670
thompson,0.003654
thorough,0.003692
thu,0.008633
thumb,0.004261
tibet,0.004390
till,0.003716
time,0.021781
timotheu,0.013889
tireless,0.006467
titl,0.002209
titulu,0.007126
today,0.003536
togeth,0.003394
tonelotto,0.007126
took,0.007691
tool,0.005912
topographi,0.004015
toubia,0.007126
tour,0.003551
trace,0.021242
tractibu,0.007126
trade,0.001780
tradit,0.004649
transcrib,0.012418
transcript,0.010905
transform,0.008059
transit,0.008571
transylvania,0.005178
traub,0.006052
treati,0.002570
treatment,0.002264
trend,0.002351
triumph,0.003830
truli,0.006168
tuition,0.004848
turn,0.008508
tutori,0.004080
twin,0.003621
type,0.057715
typefac,0.006108
ugli,0.004873
uk,0.004512
ultim,0.002128
unbroken,0.004615
uncertain,0.006507
uncertainti,0.005729
uncial,0.164077
uncouth,0.006796
undeciph,0.006304
undeni,0.004996
understand,0.005012
understood,0.002310
underw,0.014285
undoubtedli,0.004342
unequ,0.007647
unfortun,0.003548
unhamp,0.006304
uniform,0.028539
uninfluenc,0.005904
uniqu,0.002098
uniti,0.008116
univers,0.003824
unknown,0.007645
unligatur,0.007126
unlik,0.002049
unmistak,0.005235
unstress,0.005545
upright,0.048219
upstrok,0.006304
upward,0.010279
usag,0.002581
use,0.062357
usual,0.007406
vakataka,0.006671
vari,0.005443
variant,0.014908
variat,0.002307
varieti,0.016633
variou,0.012611
various,0.003806
vast,0.002471
vaticanu,0.006108
vatteluttu,0.007126
vattezhuttu,0.014252
vav,0.006796
vellum,0.041022
veneto,0.006052
verespatak,0.007126
veri,0.016868
vermont,0.004812
verona,0.005637
vers,0.007248
versa,0.003493
version,0.004306
versu,0.002873
vertic,0.003184
vespasiano,0.007126
vice,0.002849
vicin,0.003972
violat,0.002723
virgil,0.009722
visigoth,0.029981
vital,0.002749
vocabulari,0.007361
vowel,0.019444
w,0.002065
wa,0.074684
wall,0.002606
walter,0.002936
want,0.002272
war,0.003497
warrior,0.003710
watersh,0.003956
wattenbach,0.007126
wax,0.008363
waxen,0.006796
way,0.002632
web,0.005337
went,0.009030
west,0.006340
western,0.012960
whatev,0.003030
whi,0.002264
wide,0.007926
wider,0.002802
wilhelm,0.003098
william,0.001972
wit,0.002998
wood,0.002840
word,0.010251
worddivis,0.006796
work,0.009653
world,0.008735
write,0.169361
writer,0.010105
written,0.074054
writtenwer,0.007126
wrting,0.007126
xx,0.004456
yajna,0.005950
year,0.004968
yield,0.002404
yod,0.007126
youth,0.003104
zenith,0.004479
zeno,0.004692
