abrupt,0.027762
accord,0.009049
acr,0.024220
age,0.022667
algaz,0.047609
anatolia,0.027228
ancient,0.013112
anoth,0.017408
archaeolog,0.019377
archaeologist,0.024943
architectur,0.017070
area,0.008988
arriv,0.014568
arslantep,0.047609
art,0.027689
aruda,0.047609
associ,0.008840
baghdad,0.025134
bc,0.096867
becom,0.008735
begin,0.021280
bevel,0.044924
boston,0.020118
bowl,0.055444
brak,0.041835
bronz,0.043924
ca,0.040143
cambridg,0.014474
caucasu,0.053357
centuri,0.009384
chalcolith,0.034406
chang,0.008239
cheap,0.022643
chicago,0.035625
citi,0.025243
cityst,0.051289
civil,0.025074
civilis,0.024943
climat,0.046817
coin,0.015135
cold,0.017422
comment,0.017507
commun,0.009557
compani,0.011541
compris,0.014953
confer,0.014388
construct,0.011319
continu,0.008857
copper,0.020299
correspond,0.026540
cover,0.012035
crawford,0.032135
cuneiform,0.030224
cylind,0.027192
dam,0.024668
date,0.012217
declin,0.013879
deep,0.016273
defin,0.010031
discard,0.024450
dure,0.016829
dynam,0.013704
dynast,0.054901
e,0.011845
earli,0.061616
earlier,0.013004
east,0.013530
ed,0.049281
elaz,0.044924
emerg,0.022558
enclav,0.054677
encyclopedia,0.015157
end,0.038489
epoch,0.025134
euphrat,0.063053
evid,0.024002
excav,0.023659
exist,0.008751
expand,0.012449
expans,0.059397
explan,0.015491
far,0.011933
follow,0.015056
frankfort,0.040323
g,0.012731
gawra,0.047609
gener,0.006762
given,0.009438
godin,0.037908
govern,0.008949
gradual,0.015128
grew,0.015726
h,0.013072
ha,0.006275
habuba,0.047609
hamoukar,0.044924
hand,0.011660
harriet,0.030763
hassek,0.047609
henri,0.015168
histori,0.062706
holocen,0.029506
houghton,0.028632
hoyuk,0.047609
hubert,0.029450
identifi,0.011376
iii,0.017997
import,0.008026
includ,0.026131
iran,0.021347
iraq,0.021059
isbn,0.044499
item,0.017262
iv,0.020427
jawa,0.040781
jebel,0.035871
jemdet,0.089849
kabira,0.047609
keban,0.046097
kish,0.036062
km,0.017710
konstantin,0.032135
l,0.013850
lamb,0.028356
langer,0.032228
larg,0.016583
largescal,0.018332
late,0.032679
later,0.018714
layer,0.072443
life,0.010113
like,0.008403
link,0.006841
london,0.026494
ma,0.019871
mass,0.012901
massproduc,0.029973
mesopotamia,0.119614
mesopotamian,0.027526
middl,0.012970
mifflin,0.029026
migrant,0.024558
millennium,0.039270
modern,0.009436
nasr,0.064839
nd,0.029274
near,0.012934
need,0.009457
network,0.013151
nineveh,0.035871
north,0.024981
northeast,0.022406
northeastern,0.024303
northern,0.014706
northwest,0.021515
organ,0.008580
orient,0.016392
oscil,0.022765
outing,0.039903
p,0.012009
paint,0.019979
pelican,0.036260
penguin,0.023659
peopl,0.008673
period,0.224337
phase,0.015128
piora,0.047609
pitskhelauri,0.047609
popular,0.011254
potteri,0.024736
press,0.021234
preuruk,0.047609
proper,0.032368
protohistor,0.039151
protoliter,0.041281
provinc,0.016676
qrayya,0.047609
reach,0.011626
recent,0.009906
refer,0.005721
repres,0.009384
research,0.009281
rim,0.030288
routledg,0.020387
samsat,0.044924
saw,0.028001
script,0.022552
seal,0.021455
semit,0.030288
sep,0.031131
settl,0.016260
sign,0.013109
site,0.053647
social,0.010129
sourc,0.020030
south,0.012469
start,0.030236
stratif,0.028267
stratigraphi,0.031208
strong,0.011569
strongli,0.015346
succeed,0.016278
sumer,0.059239
sumerian,0.082352
support,0.009466
syria,0.046815
syrian,0.026980
tell,0.017934
tepe,0.072521
tepecik,0.047609
term,0.007523
th,0.045649
thi,0.029777
thu,0.009307
time,0.007044
trade,0.011518
tribe,0.019601
turkey,0.020765
ubaid,0.110043
univers,0.016493
urban,0.016700
uruk,0.838732
veri,0.009093
w,0.013359
wa,0.026114
wet,0.023770
william,0.012758
world,0.024217
xiviv,0.047609
xviiixiv,0.047609
