abd,0.003216
abdic,0.003112
abduct,0.003187
abdul,0.003178
abdulrahim,0.005434
abil,0.001421
abisaab,0.010869
abl,0.003914
abolish,0.002158
abou,0.004603
abov,0.001322
abrupt,0.003169
absent,0.002517
absorb,0.004130
abstain,0.003231
abuhusayn,0.016304
abus,0.002222
accept,0.003891
access,0.004288
accord,0.008264
account,0.002433
accus,0.012431
achaemenid,0.003580
achiev,0.002599
achrafieh,0.005434
acquaint,0.003023
acr,0.024884
act,0.002361
action,0.007616
activ,0.002061
ad,0.005273
addin,0.049709
addit,0.004180
administ,0.002025
administr,0.002888
adopt,0.002851
advanc,0.001275
advantag,0.001603
advic,0.002248
affair,0.001708
affili,0.002363
afford,0.002218
africa,0.001677
aftermath,0.005149
afterward,0.002275
age,0.002587
agenc,0.001625
agent,0.001761
aggress,0.002302
ago,0.002001
agre,0.003115
agreement,0.028385
agricultur,0.003071
ahm,0.011874
ahmad,0.005977
aim,0.001475
ain,0.003658
air,0.004761
aircraft,0.002294
airlin,0.002629
airport,0.002275
akarli,0.005434
akil,0.014327
akkar,0.010869
akram,0.004655
al,0.012404
alakhbar,0.005434
alansar,0.005128
alarm,0.002877
alasad,0.005128
alassad,0.004267
alawit,0.004394
albar,0.005128
aleppo,0.003927
alexand,0.003958
aley,0.005434
alfr,0.004233
algeria,0.002912
alghajar,0.010869
algier,0.003637
alhoss,0.010869
ali,0.012805
align,0.002231
alislam,0.004327
alkhalidi,0.005434
alkhazin,0.005434
alleg,0.002136
allegi,0.005516
alli,0.020989
allianc,0.005796
allibnaniya,0.005434
allow,0.011301
alltim,0.003472
alnass,0.005128
alon,0.003533
alphabet,0.009950
alqaeda,0.003503
alqaida,0.004775
alreadi,0.001505
altaym,0.005434
alter,0.001853
alterc,0.004360
altern,0.001327
amal,0.024569
amass,0.003263
ambassador,0.002535
ambit,0.002694
ambush,0.003290
amend,0.017428
america,0.001447
american,0.008193
amil,0.005128
amin,0.009401
amir,0.010487
amnesti,0.002978
amr,0.004116
anatolia,0.006216
ancient,0.005987
anger,0.005393
angloegyptian,0.004267
angri,0.003247
ani,0.003642
anjar,0.010524
anka,0.005019
annan,0.019480
annex,0.002350
announc,0.012144
anoth,0.006955
antilebanon,0.005019
antipalestinian,0.005128
antiqu,0.002137
antisyrian,0.021739
aoun,0.025095
ap,0.003120
appar,0.003527
apparatu,0.002571
apparten,0.005434
appeal,0.003848
appear,0.001196
appli,0.001104
appoint,0.010189
approach,0.002377
approv,0.001774
approxim,0.005631
april,0.012644
arab,0.040071
arabia,0.002527
arabicspeak,0.004187
arabisra,0.010585
arafat,0.017070
arama,0.003580
architect,0.002746
architectur,0.001948
archiv,0.001901
area,0.024626
argu,0.001374
arm,0.023061
armenian,0.003142
armi,0.030209
arrang,0.003368
array,0.002254
arrest,0.002270
arriv,0.006652
art,0.003161
arthur,0.002107
articl,0.002455
artifact,0.002447
asbat,0.005434
ascetic,0.003658
asia,0.001773
asian,0.001968
asid,0.002356
ask,0.001707
aspir,0.005194
assad,0.016749
assafadi,0.005434
assassin,0.039122
assembl,0.008063
assert,0.001774
assist,0.009242
associ,0.006055
assum,0.001431
athen,0.002855
attach,0.001955
attack,0.056633
attain,0.002091
attempt,0.010605
attent,0.003369
au,0.002962
august,0.007849
augustin,0.002774
author,0.015121
autonom,0.002089
autonomi,0.004317
avail,0.001262
avoid,0.001543
away,0.001573
azar,0.004187
baaklin,0.005434
baalbek,0.005262
baathism,0.004712
babda,0.005434
bachir,0.009692
backbon,0.002939
backer,0.003723
balanc,0.001635
balkan,0.002930
bania,0.005434
bank,0.004482
bare,0.002544
barrack,0.003465
barrag,0.004094
base,0.001800
bashar,0.012638
bashir,0.120042
basi,0.001286
basic,0.001253
basta,0.005128
bastion,0.003536
battl,0.019510
bbc,0.016452
bc,0.003686
bce,0.004572
bead,0.003211
becam,0.018218
becaus,0.003750
becom,0.003988
bedfordst,0.004510
befor,0.006383
beg,0.003257
began,0.011195
begin,0.004858
begun,0.001980
behavior,0.002922
beida,0.005434
beik,0.005434
beirut,0.193031
beirutallprint,0.005434
beirutfocus,0.005434
bekaa,0.034486
believ,0.007730
belong,0.005280
belt,0.002715
beqaa,0.004926
berkeley,0.004865
berri,0.003401
berytu,0.005128
besieg,0.006160
best,0.002745
bestknown,0.002715
better,0.002809
bey,0.003996
beydoun,0.005434
beyrouth,0.010869
bibl,0.002498
bibliographi,0.001811
billionair,0.003679
bin,0.002872
biographi,0.002402
biqa,0.027174
black,0.003362
blackmail,0.003961
blame,0.002474
blanford,0.004775
blast,0.006098
block,0.001855
blockad,0.002852
blow,0.002639
blue,0.019984
bodyguard,0.003735
boil,0.002786
bomb,0.025944
book,0.003551
border,0.023855
born,0.003519
boundari,0.003478
boutro,0.004846
boy,0.002454
boycot,0.003151
branch,0.001324
break,0.001717
breccia,0.005128
brethren,0.003450
brief,0.001974
briefli,0.004621
bring,0.003097
britain,0.001754
british,0.004172
broader,0.002077
broadli,0.002085
broke,0.004272
broker,0.002735
brood,0.003823
brother,0.008356
brought,0.006142
bu,0.006098
buffer,0.002921
build,0.003954
built,0.001552
bulletproof,0.004603
burn,0.008541
buse,0.006775
bush,0.002569
businessman,0.003197
buss,0.004014
byblo,0.004212
byzantin,0.005125
cabinet,0.008383
cadiz,0.004162
caesar,0.003088
cairo,0.003129
california,0.003821
caliph,0.002821
calm,0.008791
cambridg,0.001652
came,0.004092
camil,0.003700
camp,0.013544
campaign,0.007491
canaanit,0.015464
cancel,0.002363
candid,0.003843
capabl,0.003285
capit,0.002713
capra,0.004073
captur,0.001713
car,0.017637
caravan,0.003443
carbomb,0.005434
card,0.004741
care,0.001567
career,0.004093
caretak,0.003387
carri,0.001344
carrier,0.002505
carthag,0.010203
catastroph,0.002476
cathol,0.004028
catroux,0.005434
caus,0.002227
cave,0.002508
ceas,0.006322
ceasefir,0.005892
cedar,0.015295
cement,0.002751
censu,0.007339
center,0.011976
centr,0.010293
central,0.002339
centuri,0.023569
ceremoni,0.004606
certain,0.001141
certifi,0.005444
challeng,0.001490
chamoun,0.010038
chanc,0.002008
chang,0.005643
channel,0.002059
chant,0.003192
chao,0.002414
chapter,0.001944
charact,0.001724
charg,0.008050
charisma,0.004014
charl,0.003195
cheer,0.003647
chehab,0.021049
chehayeb,0.005434
chevalli,0.005434
chief,0.001737
chip,0.002801
chirac,0.004394
chomski,0.012182
chose,0.002346
chouf,0.016304
christian,0.063059
christianmuslim,0.005128
chronolog,0.002487
church,0.001885
circumst,0.001916
cite,0.001745
citi,0.008645
citizen,0.001707
cityst,0.005855
civil,0.031486
civilian,0.013014
claim,0.003799
clan,0.002735
clash,0.007449
class,0.002767
classic,0.001378
clear,0.007764
clearcut,0.003362
cliff,0.002793
climat,0.001781
close,0.003349
clung,0.004033
coast,0.009521
coastal,0.013647
cohes,0.002707
cold,0.001988
collabor,0.001916
collaps,0.009190
collin,0.002946
colon,0.002213
coloni,0.008626
combat,0.006570
come,0.009547
command,0.001750
commanderinchief,0.002897
commando,0.003511
commerc,0.002079
commiss,0.004983
commission,0.005132
commit,0.001748
common,0.001008
commonli,0.001385
commun,0.016366
compani,0.001317
compat,0.002393
complet,0.012677
complex,0.001221
compos,0.001583
compound,0.001880
compromis,0.002399
concern,0.004686
concert,0.005627
concess,0.002639
conclud,0.005119
concurr,0.002572
condemn,0.004700
condit,0.001167
condominium,0.003784
conduct,0.001430
confer,0.006570
confessionel,0.005434
confid,0.002134
confin,0.002240
confirm,0.001798
conflict,0.037300
confront,0.002305
congress,0.001869
conquer,0.011296
consensu,0.002081
consent,0.002561
consequ,0.002837
consid,0.002919
consider,0.001402
constantinopl,0.005772
constitut,0.021081
construct,0.001292
contact,0.007004
contain,0.005985
contemporain,0.004430
contemporan,0.003108
contest,0.002195
context,0.002935
continu,0.012133
contradict,0.002228
contribut,0.002449
control,0.028826
controversi,0.001644
convalesc,0.004162
convert,0.005103
convoy,0.003307
cooper,0.004784
coopt,0.003679
corm,0.005019
cornel,0.002639
corrupt,0.002048
cosimo,0.004267
cosmopolitan,0.003290
council,0.034996
count,0.001903
counter,0.002256
counti,0.002483
countri,0.024159
coup,0.002272
court,0.003165
cover,0.005495
crackdown,0.006736
creat,0.003062
creation,0.001518
creator,0.002501
credibl,0.002597
credit,0.001614
crime,0.012422
crisi,0.003444
critic,0.002454
criticis,0.002456
crossbord,0.003164
crossroad,0.003178
crowd,0.005201
crusad,0.025484
cultiv,0.002131
cultur,0.013801
currenc,0.001828
cut,0.001771
cycl,0.001710
cyru,0.003759
d,0.001350
dabaq,0.010869
daili,0.003903
dakhlallah,0.010869
dama,0.004655
damag,0.003601
damascu,0.066154
damour,0.004555
dan,0.002702
dana,0.003407
dara,0.004267
date,0.005578
datum,0.003784
daughter,0.002290
day,0.016370
deadliest,0.003712
deal,0.002688
death,0.011692
decad,0.003033
decemb,0.010719
decid,0.006505
decis,0.007034
declar,0.006447
declin,0.004753
decreas,0.001643
deem,0.002258
deepen,0.002998
deepli,0.002356
defeat,0.013820
defend,0.007468
defens,0.003663
degre,0.001366
deir,0.008535
deleg,0.004712
delmar,0.004655
demand,0.007449
demis,0.002949
democraci,0.001858
demograph,0.006721
demonstr,0.007499
deni,0.001934
deniz,0.004655
denounc,0.008108
dentz,0.010869
depart,0.001463
departur,0.010251
depend,0.001101
depict,0.002081
deplet,0.002604
deploy,0.015808
depos,0.002744
deposit,0.001975
depth,0.002191
deputi,0.002234
descend,0.002019
design,0.002451
despit,0.002830
despoil,0.004926
destabil,0.006216
destroy,0.005235
destruct,0.001975
detach,0.002715
deter,0.003056
determin,0.002317
detlev,0.004510
develop,0.003338
diaspora,0.003023
did,0.006193
die,0.004796
differ,0.003538
dioces,0.003301
diplomaci,0.002808
diplomat,0.005947
direct,0.006648
directli,0.002681
disaffect,0.003851
disarm,0.006762
disarma,0.006686
disband,0.014503
disciplin,0.001537
discont,0.002866
discord,0.003528
discourag,0.002519
discov,0.002939
discuss,0.001361
dismantl,0.011697
dispatch,0.005716
displac,0.002141
disposit,0.002842
disput,0.005035
dissatisfi,0.003381
dissent,0.002842
dissolv,0.002072
distanc,0.001777
distribut,0.002753
district,0.012110
divers,0.001604
divert,0.002930
divid,0.011909
divis,0.002892
document,0.001611
doha,0.003618
domain,0.001629
domest,0.003458
domin,0.008239
dominiqu,0.003627
dori,0.003851
doubl,0.003695
downtown,0.003488
dozen,0.002334
draft,0.004307
dramat,0.001884
drawn,0.003938
drive,0.001792
driver,0.002557
druze,0.195546
du,0.002358
duchi,0.003252
duke,0.007894
dure,0.026896
dynasti,0.002169
e,0.001352
earli,0.006029
earlier,0.008907
earliest,0.003358
east,0.018535
eastern,0.011672
econom,0.013193
economi,0.002744
ed,0.001406
edd,0.004033
edg,0.002191
edit,0.001572
effect,0.006832
effort,0.009739
egbert,0.004267
egypt,0.010608
egyptian,0.004528
eightfold,0.003637
eightyearold,0.004360
el,0.007247
elect,0.025190
eli,0.003192
elia,0.006775
elimin,0.001737
elizabeth,0.002390
elsewher,0.004142
eltaym,0.005434
embassi,0.004975
embodi,0.002322
emerg,0.003862
emigr,0.002525
emil,0.002826
emin,0.002858
emir,0.031500
emireh,0.005128
empir,0.021028
en,0.004937
enact,0.002227
enclav,0.003120
encompass,0.001880
encourag,0.004953
end,0.019772
endless,0.003076
enemi,0.006736
enforc,0.001883
engag,0.001590
engin,0.002895
enhanc,0.001860
enjoy,0.005738
enkapun,0.005434
enlarg,0.002566
enlist,0.003146
enorm,0.006491
ensu,0.002456
ensur,0.004951
enter,0.007343
entir,0.007818
entireti,0.002872
entrench,0.003056
entrust,0.003207
environ,0.001331
envis,0.002744
equal,0.004104
era,0.003174
escal,0.007807
especi,0.002333
essay,0.001946
establish,0.009649
estat,0.002138
estim,0.007068
et,0.005180
ethnic,0.001982
eurasia,0.002927
europ,0.013622
european,0.006256
evacu,0.008239
event,0.006537
eventsbbc,0.005434
eventu,0.002855
evergrow,0.003944
everi,0.001257
evolut,0.001631
exacerb,0.005563
examin,0.001572
exampl,0.000899
excav,0.002700
exceed,0.002109
excerpt,0.002776
excess,0.001941
exchang,0.005680
execut,0.004565
exercis,0.003452
exert,0.006915
exil,0.006883
exist,0.002997
existenti,0.002722
exit,0.002635
exminist,0.009692
expand,0.002842
expans,0.001695
expect,0.001380
expedi,0.003394
expedit,0.002272
expel,0.002422
expens,0.001794
experi,0.001235
expert,0.001984
expir,0.002715
explod,0.005911
explos,0.006681
export,0.001843
express,0.002603
expuls,0.002946
extend,0.010601
extens,0.002713
extern,0.002356
extrem,0.001441
extremist,0.003387
eye,0.002008
fabiola,0.005434
face,0.007539
fact,0.003826
faction,0.007118
factor,0.001273
fahkraldin,0.005434
fail,0.004531
faith,0.006025
fakhr,0.055477
fakhraldin,0.065219
fakhreddin,0.016304
fall,0.004114
fallen,0.004882
famili,0.009737
famin,0.002690
famou,0.001659
far,0.004086
farah,0.004267
farm,0.008017
farmer,0.002126
fastest,0.002836
fatah,0.012890
fate,0.007610
father,0.001766
fauna,0.002562
favor,0.003318
fawaz,0.010524
fawwaz,0.005434
fealti,0.004187
fear,0.007540
featur,0.001358
februari,0.016322
fed,0.002688
feel,0.001878
feet,0.002453
ferdinand,0.002715
fernand,0.003319
feudal,0.015571
fewer,0.002130
fight,0.013195
fighter,0.008186
figur,0.002975
final,0.011695
financ,0.004775
financi,0.002868
finger,0.002918
firro,0.005434
fitzgerald,0.003700
flake,0.007216
flare,0.003387
flashpoint,0.004712
fled,0.004863
flee,0.005152
flew,0.003125
flint,0.003247
flourish,0.002211
flower,0.002439
focus,0.004195
follow,0.015468
foment,0.003747
foot,0.002334
forc,0.053839
foreign,0.013985
forg,0.002672
forgiv,0.003285
form,0.007390
formal,0.002688
format,0.002930
formerli,0.002012
fort,0.005297
forti,0.002682
fouad,0.004926
foundat,0.001400
fourfold,0.003759
fragment,0.002112
franc,0.018455
francosyrian,0.005128
franjieh,0.016304
frank,0.002103
frankish,0.007143
free,0.004908
freedom,0.001695
freeli,0.002336
french,0.027470
frequent,0.001510
friedman,0.002776
friend,0.006401
friendship,0.002668
frontier,0.002368
frustrat,0.002755
ft,0.011739
fuad,0.009021
fuel,0.005972
fulli,0.003239
fullscal,0.003129
fund,0.002962
fundament,0.001375
gain,0.007842
galile,0.007401
gallimard,0.004469
gamal,0.003784
garrison,0.005444
gather,0.009364
gaull,0.007236
gave,0.007816
geagea,0.027174
gemayel,0.047758
gener,0.010807
geograph,0.001747
georg,0.004644
germani,0.006470
gestur,0.002972
ghazali,0.004430
gilsenan,0.005434
given,0.002154
goal,0.001477
golan,0.003880
good,0.001194
gorton,0.004327
govern,0.054144
government,0.002248
governor,0.013803
gradual,0.003453
grand,0.008347
grant,0.004940
great,0.004843
greater,0.002706
greatli,0.003518
greek,0.001521
grew,0.005385
grotto,0.004510
ground,0.001563
group,0.015505
grow,0.006887
grown,0.002068
guarante,0.003941
guerr,0.003866
guest,0.002836
guid,0.001518
guil,0.004603
gulf,0.002430
gunmen,0.004073
ha,0.015043
habash,0.004655
habib,0.011277
haddad,0.004430
haffar,0.005434
hafiz,0.012284
half,0.005915
halim,0.004603
halt,0.002447
hamadeh,0.016304
hand,0.003993
handicap,0.003394
harem,0.003961
hariri,0.067040
harper,0.002767
harri,0.002259
harsh,0.002556
harvard,0.002098
hasbaya,0.005434
hassan,0.003257
hawran,0.005434
head,0.006934
headquart,0.002380
heal,0.002633
hear,0.002265
heartland,0.003374
heavi,0.003713
heaviest,0.003421
heavili,0.003463
hebrew,0.002696
heed,0.003700
heel,0.003580
height,0.004283
heighten,0.003108
held,0.007621
help,0.009408
henri,0.001731
hermit,0.003428
hezbollah,0.139872
hezbollahl,0.005434
hi,0.055868
high,0.004351
higher,0.001277
highli,0.002828
hijack,0.003465
hinterland,0.003387
histoir,0.003226
histor,0.004827
histori,0.022369
historian,0.001810
hizballah,0.009853
hizbullah,0.004846
hoayek,0.005434
hobeika,0.005262
hold,0.003882
hole,0.002445
holi,0.004549
holmesmei,0.005434
home,0.006136
homeland,0.002779
homo,0.002816
honour,0.002533
hooijer,0.005434
hope,0.001829
host,0.003928
hostil,0.008912
hous,0.002882
howev,0.006103
hrawi,0.010256
hsrev,0.005434
httplcweblocgovfrdc,0.003618
httptjgortonwordpresscomprinceoflebanonadruzeemiratthecourtofthemedici,0.005434
human,0.002070
hundr,0.003264
hurdl,0.003414
hurt,0.002816
ib,0.028030
ibn,0.002338
idea,0.002411
ident,0.001538
identit,0.004846
idf,0.012638
idntitair,0.005434
ifapo,0.005434
ii,0.033743
iii,0.006163
illeg,0.002164
imad,0.005128
immedi,0.006316
impact,0.002944
implement,0.002991
implos,0.003823
import,0.004581
impos,0.001847
impoverish,0.003030
inchoat,0.004603
incid,0.002125
incit,0.006453
includ,0.013423
incorpor,0.002950
increas,0.014463
increasingli,0.006335
incurs,0.002918
indebt,0.003296
independ,0.015447
india,0.001644
indic,0.003964
indict,0.006886
industri,0.003678
industriel,0.004073
infant,0.002581
inflect,0.003810
influenc,0.008282
influenti,0.001727
influx,0.002772
inform,0.003204
infrastructur,0.003637
infring,0.003221
inhabit,0.001867
initi,0.005836
injuri,0.002381
insid,0.003603
insist,0.006575
instabl,0.002322
instead,0.001258
instig,0.002933
institut,0.003410
insur,0.002071
insurg,0.002781
insurrect,0.003151
intellig,0.006704
intend,0.001663
intens,0.003568
intensifi,0.002633
intent,0.001807
interf,0.003060
interfer,0.002192
interim,0.005155
intern,0.010355
interreligi,0.004327
interspers,0.003618
interven,0.004617
intervent,0.011695
intervention,0.003823
intifada,0.007922
intra,0.003911
intrapalestinian,0.005128
introduc,0.003780
invad,0.006445
invas,0.005876
invent,0.001748
invest,0.001529
investig,0.006021
investor,0.002046
invit,0.002213
involv,0.002082
iranian,0.002709
iraniraq,0.003784
iraq,0.002404
ireland,0.002245
irregular,0.005026
irrig,0.002715
isbn,0.010159
islam,0.005365
islamist,0.003221
isol,0.001744
isra,0.088023
israel,0.079202
israelisyrian,0.005434
israelpalestin,0.004510
issachadi,0.005434
issu,0.002305
istanbul,0.010011
itali,0.003866
italian,0.008063
itch,0.004139
ithaca,0.006788
iv,0.004663
jaafar,0.004712
jabal,0.004296
jamil,0.004360
janissari,0.004239
januari,0.009198
jazzar,0.010869
jeer,0.005262
jerusalem,0.008616
jesuit,0.003009
jezzar,0.016304
jezzin,0.005434
john,0.001221
johnson,0.002454
join,0.004793
joint,0.001891
jordan,0.005414
jordanian,0.007992
joumblatt,0.005434
joumhoriya,0.005434
journal,0.001381
journalist,0.005057
judici,0.002062
juli,0.009310
jumblatt,0.076088
june,0.007825
jurdi,0.005434
just,0.001252
justifi,0.002160
k,0.003257
kai,0.003589
kamal,0.004033
kandji,0.005434
karam,0.004603
karami,0.043479
kataeb,0.010038
katyusha,0.009853
kaza,0.005019
keen,0.002958
kept,0.007742
keserwan,0.005434
key,0.001367
kg,0.002798
khaddam,0.016304
khalaf,0.004655
khalidi,0.005262
khazen,0.005434
khiyam,0.005434
kidnap,0.003049
kill,0.024827
kilomet,0.005431
kingdom,0.004286
kiryat,0.004775
kiss,0.003580
km,0.002021
knew,0.002530
known,0.004487
kofi,0.015584
ksar,0.014327
kuchuk,0.016304
kurd,0.003927
kutshuk,0.005434
l,0.003162
la,0.003585
lack,0.001369
laden,0.003450
laf,0.015385
lahoud,0.044339
land,0.013593
landmark,0.002528
landmin,0.004212
languag,0.002700
larg,0.004732
largest,0.002966
latakia,0.004655
late,0.008704
later,0.017090
latest,0.002399
latin,0.001688
launch,0.009044
law,0.001081
le,0.004279
lead,0.008573
leader,0.026699
leadership,0.003897
leagu,0.011018
leak,0.003012
leav,0.013688
lebanes,0.327821
lebanon,0.669444
lebanonbbc,0.005434
lebanonsyrian,0.005434
led,0.025502
left,0.007158
leftist,0.005534
legisl,0.001644
legitim,0.002230
leila,0.004712
letter,0.003545
leurop,0.004926
levalloisomousterian,0.010869
levant,0.003027
level,0.005369
levi,0.002524
lf,0.003996
lharmattan,0.004033
lhistoir,0.003961
liban,0.014137
libanais,0.009692
libanensi,0.005434
liber,0.009715
librari,0.001687
licenc,0.003076
licens,0.002138
lie,0.001759
life,0.002309
lift,0.002397
like,0.000959
limb,0.002811
limeston,0.003068
limit,0.003290
line,0.012854
link,0.003904
list,0.002996
litani,0.004187
literari,0.002252
lithic,0.003961
live,0.006967
local,0.003649
locat,0.001320
london,0.013610
long,0.010495
longest,0.002574
look,0.002965
lord,0.002125
lorient,0.004555
loss,0.001570
lost,0.001560
loyal,0.002737
loyalti,0.002602
lpoqu,0.005128
luniversit,0.004469
maan,0.023563
macedonian,0.003343
mahdi,0.003927
mahiga,0.005434
main,0.006821
mainli,0.004472
mainstay,0.003285
maintain,0.003756
maintin,0.005434
majdel,0.005262
major,0.009528
makdisi,0.004775
make,0.003653
malek,0.005019
mamluk,0.021593
man,0.006355
manag,0.002572
mandat,0.022376
mandatori,0.005520
mani,0.016234
mansion,0.003545
maoz,0.004926
mar,0.002457
march,0.019360
margin,0.001968
marin,0.008223
maritim,0.002311
marj,0.009310
mark,0.002883
maron,0.009425
maronit,0.193487
maronitecontrol,0.005434
marseil,0.003723
martin,0.001900
martyr,0.012604
marwan,0.008720
mass,0.001472
massacr,0.020949
massiv,0.003821
materi,0.003775
matter,0.002701
maxilla,0.004555
maxim,0.002042
mayhem,0.004555
mean,0.000989
meanwhil,0.004343
measur,0.002352
medic,0.001653
medici,0.010244
mediev,0.001915
mediterranean,0.011745
mehli,0.005434
mehm,0.003978
meir,0.003961
member,0.010312
memoir,0.002897
memorandum,0.003173
men,0.004995
menachem,0.004212
mention,0.001738
mercenari,0.003072
merchant,0.002132
mere,0.003533
merg,0.002163
messag,0.002231
met,0.001836
method,0.001112
metr,0.012772
michael,0.003305
michel,0.007385
mid,0.005365
middl,0.016287
migrant,0.002803
migrat,0.001918
mikati,0.005434
mile,0.010437
milit,0.014030
militari,0.018886
militarili,0.003084
militia,0.038908
militiamen,0.003978
millennium,0.002241
million,0.002821
minist,0.043749
minor,0.003323
misunderstood,0.003428
misura,0.010256
moabit,0.004712
modern,0.014003
modernday,0.002556
modif,0.002134
modu,0.003647
monast,0.003104
monday,0.003313
money,0.003209
monk,0.002656
monopoli,0.002300
monoth,0.003242
month,0.011106
montliban,0.005434
moral,0.001796
morn,0.005219
mosh,0.003810
mostli,0.004596
mother,0.002069
motorcad,0.004603
mouawad,0.005434
mouhamad,0.005434
mount,0.020935
mountain,0.023185
mountlebanon,0.016304
moussa,0.008425
movement,0.009906
mp,0.010594
mr,0.014486
mt,0.016316
muammad,0.004212
muhafiz,0.010869
muhammad,0.005159
multin,0.005440
multipl,0.001381
municip,0.002188
murad,0.008278
murder,0.009123
museum,0.002165
muslim,0.060253
mustafa,0.007125
mutasarrif,0.015787
muthen,0.005434
muto,0.005128
myth,0.002293
naccach,0.005434
nadim,0.009206
nahr,0.004655
najib,0.004775
napl,0.003362
napoleon,0.009797
naqoura,0.005434
narr,0.002363
narrow,0.002027
narrowest,0.003996
nasrallah,0.030114
nasti,0.003851
nation,0.022887
nationalist,0.002328
nationsbrok,0.005019
nativ,0.001856
natur,0.000958
naval,0.002265
navi,0.002149
nazi,0.005008
nd,0.001670
neanderth,0.003407
near,0.002953
nearbi,0.004420
nearli,0.004692
need,0.004318
neg,0.001557
neglig,0.002672
negoti,0.003759
neighbor,0.002025
neighborhood,0.008160
neighbour,0.002193
neoassyrian,0.003927
neobabylonian,0.004239
network,0.003002
neutral,0.001965
new,0.017648
newli,0.001889
news,0.007840
newspap,0.002223
nice,0.003257
nichola,0.002415
nighttim,0.003851
niha,0.005434
nineteenth,0.004492
nineteenthcenturi,0.006320
noam,0.012242
nobl,0.002335
nonlebanes,0.005434
nonsectarian,0.004116
north,0.004277
northeast,0.002557
northern,0.015110
notabl,0.002756
note,0.001035
novemb,0.016107
number,0.003593
nun,0.003401
occas,0.002233
occup,0.014970
occupi,0.010802
occur,0.005704
octob,0.020188
odyssey,0.003164
offens,0.002502
offic,0.008673
offici,0.012270
oilrich,0.003961
old,0.001485
older,0.001884
oldest,0.001942
omar,0.009092
onc,0.001341
onefifth,0.003503
onli,0.008266
onlin,0.001571
open,0.002458
openli,0.002720
oper,0.001132
operandi,0.005019
oppon,0.002230
opportun,0.001696
oppos,0.007503
opposit,0.022165
order,0.007917
organ,0.009795
orient,0.001871
origin,0.004976
ornament,0.012023
orthodox,0.002418
osama,0.003927
osuna,0.005434
ottoman,0.116835
outbreak,0.009775
outcom,0.001836
outnumb,0.002981
outrag,0.003092
outright,0.002781
outsid,0.004110
overcom,0.002205
overlord,0.003580
overlordship,0.004296
overse,0.002678
oversea,0.002141
overthrow,0.002607
overthrown,0.002897
oxford,0.005062
p,0.001370
pad,0.003355
pagan,0.002760
page,0.003521
paid,0.001831
palac,0.005135
paleolith,0.008916
palestin,0.022652
palestinian,0.058980
palmyra,0.004394
panarab,0.004187
parachut,0.003880
paralia,0.005262
paralysi,0.003450
paramilitari,0.006302
paratroop,0.003896
pardon,0.009606
pari,0.011783
parliament,0.021734
parliamentari,0.004113
parti,0.015266
partial,0.001652
particip,0.002833
particular,0.001124
particularli,0.001259
partit,0.002487
partner,0.001963
pasha,0.046666
pass,0.004185
passport,0.002958
past,0.002849
path,0.001820
patriarch,0.008996
patriot,0.002620
pattern,0.001532
peac,0.017319
peacekeep,0.002728
peak,0.003941
peasant,0.002504
pendant,0.004212
peopl,0.017822
perceiv,0.001806
perhap,0.001669
period,0.014939
perioikoi,0.005434
perman,0.001740
permit,0.001829
perpetr,0.003151
persecut,0.005176
persia,0.005348
persian,0.013538
persist,0.004072
person,0.004700
personnel,0.002219
peter,0.003275
pflp,0.015385
phalang,0.009206
phalangist,0.015787
phase,0.001726
philip,0.002207
phoenic,0.010869
phoenicia,0.012890
phoenician,0.016316
picard,0.003797
piec,0.001983
pillag,0.003627
place,0.005078
plain,0.007040
plan,0.002720
plane,0.004326
plant,0.001559
plate,0.004663
play,0.002593
pleas,0.002720
pledg,0.002546
plo,0.040814
ploisra,0.016304
plunder,0.003084
plung,0.003072
pm,0.008160
point,0.006250
polar,0.002200
polic,0.001997
polici,0.002657
polit,0.028178
politician,0.006463
pope,0.005051
popul,0.010222
populac,0.002842
popular,0.006424
port,0.004058
portfolio,0.002470
portion,0.001804
posit,0.004257
possibl,0.001050
postwar,0.002442
pound,0.002581
poverti,0.004236
power,0.023184
powerbas,0.004296
powershar,0.003759
pp,0.001678
practic,0.001101
predecessor,0.002245
predominantli,0.004572
prehistor,0.002489
prehistori,0.002586
presenc,0.012458
present,0.002059
presentday,0.009251
presid,0.038721
presidenti,0.006317
press,0.012120
pressur,0.015501
pretext,0.003435
prevail,0.002178
preval,0.002267
prevent,0.001472
previou,0.003105
previous,0.001569
prewar,0.003084
priest,0.002438
primarili,0.001407
prime,0.039709
princ,0.022223
princip,0.004826
principl,0.001236
print,0.001933
prior,0.003110
prioriti,0.002171
prison,0.004371
privat,0.002953
privileg,0.002350
pro,0.002992
probabl,0.002955
problem,0.002202
process,0.000973
proclaim,0.002329
prodemocraci,0.003837
product,0.001073
professor,0.001840
profil,0.002223
profoundli,0.002981
progovern,0.003810
progress,0.005614
promaronit,0.005434
promin,0.004844
promis,0.001935
promot,0.001469
proper,0.001847
prospect,0.002188
prosper,0.012773
prosyrian,0.055209
protect,0.004117
protest,0.009569
protestor,0.012101
protosinait,0.005128
prove,0.001589
provid,0.003654
provinc,0.007614
provinci,0.002490
provision,0.002609
provok,0.002635
prowestern,0.003759
proxim,0.002380
psp,0.017187
public,0.003111
publicli,0.002203
pull,0.004726
pullout,0.004555
punic,0.003679
puppet,0.003076
purpos,0.002679
pursuant,0.003407
push,0.001958
qa,0.003658
qahtani,0.005128
qamar,0.009425
qaraoun,0.005262
qaysi,0.021739
quarter,0.002141
quartet,0.003735
quest,0.002593
question,0.003879
quick,0.002544
quickli,0.001676
quiet,0.003016
quit,0.003480
quo,0.002872
rachid,0.004655
radiocarbon,0.003368
rafik,0.004712
rafiq,0.018220
raid,0.002452
rais,0.003037
ralli,0.024402
ramp,0.003679
ran,0.002305
rang,0.001236
rashaya,0.005434
rashid,0.003700
rate,0.001276
ratifi,0.007872
ratio,0.003714
reach,0.009291
react,0.002281
read,0.002268
real,0.002711
realiti,0.001776
realiz,0.003652
reason,0.001172
reassert,0.003247
rebellion,0.006995
rebuild,0.008246
rebuilt,0.003142
recal,0.002525
recant,0.003896
receiv,0.004991
recent,0.001130
reclaim,0.003049
recogn,0.007002
recognis,0.002016
recognit,0.001790
reconsid,0.003164
reconstruct,0.002046
record,0.004129
recov,0.004020
recoveri,0.002174
redeploy,0.003961
reduc,0.002474
reestablish,0.002487
refer,0.001959
reflect,0.002807
reform,0.012780
refug,0.005572
refuge,0.017116
refus,0.009529
regain,0.005017
regard,0.010024
regim,0.003806
regiment,0.002943
region,0.022221
registr,0.002543
regret,0.003387
regroup,0.003443
regular,0.003578
reign,0.002258
reiter,0.003155
reject,0.006440
rel,0.003513
relat,0.003522
relationship,0.001246
releas,0.006529
relev,0.001708
religi,0.013346
religion,0.003326
remain,0.016202
remaind,0.002423
remark,0.002003
rememb,0.002397
remind,0.002946
remo,0.008479
remov,0.003015
renaiss,0.004319
render,0.002141
rene,0.003637
renew,0.001909
repair,0.002325
repeat,0.001885
repeatedli,0.002324
replac,0.002661
replic,0.002305
report,0.011297
reportedli,0.005187
repres,0.002142
repress,0.002463
republ,0.004598
reput,0.002272
request,0.001985
requir,0.006168
resent,0.005632
resid,0.001730
resign,0.022318
resist,0.006769
resolut,0.041957
resourc,0.002644
respect,0.003726
respond,0.001761
respons,0.007929
rest,0.003060
result,0.010917
resurfac,0.003450
retain,0.003435
retali,0.011465
return,0.015861
reunit,0.003030
reveng,0.003183
revis,0.003903
reviv,0.004151
revolt,0.002377
revolut,0.008089
reward,0.002259
riad,0.009310
ride,0.002839
rift,0.002998
right,0.002344
rightw,0.003012
rise,0.007885
rival,0.006473
rivalri,0.002690
river,0.001785
road,0.003794
roam,0.003495
rock,0.002045
rocket,0.005548
role,0.003432
roman,0.010511
romanchristian,0.005434
romanpersian,0.004267
rome,0.004459
rose,0.003963
rouassa,0.005434
rout,0.005898
ruin,0.002597
rula,0.004926
rule,0.015498
ruler,0.010910
rumor,0.003226
run,0.001435
russia,0.001919
rvolut,0.004360
s,0.006484
saad,0.003961
sabra,0.009692
sad,0.003173
said,0.016103
sail,0.002470
saladin,0.003896
salibi,0.015385
salim,0.012221
samir,0.012638
san,0.004148
saniora,0.016304
sapien,0.003112
sarafand,0.005434
sarepta,0.005434
sassanid,0.003331
satisfi,0.001937
satrapi,0.003961
saudi,0.002662
savag,0.003080
save,0.003709
saw,0.007991
say,0.009905
sayi,0.005128
scald,0.004846
scale,0.002956
scatter,0.002411
schlicht,0.005019
scholar,0.001658
scholarli,0.002311
school,0.001328
sea,0.001666
seat,0.003865
second,0.004287
secret,0.004030
secretari,0.010668
secretarygener,0.013373
sectarian,0.030431
section,0.001618
sector,0.001712
sectordriven,0.005434
secular,0.002260
secur,0.038852
securitycouncil,0.005434
seen,0.003844
seiz,0.006736
select,0.001420
seleucid,0.003553
selim,0.008233
semiautonom,0.007379
semit,0.006915
send,0.006127
senegales,0.004212
sent,0.005430
sentiment,0.002538
separat,0.003689
septemb,0.020475
sequenc,0.001813
seri,0.007806
seriou,0.001829
serv,0.003900
servic,0.002544
session,0.002484
set,0.006070
settl,0.003712
settlement,0.001905
seven,0.001713
seventh,0.002635
sever,0.002842
sfeir,0.005434
shaba,0.005128
shafik,0.004926
share,0.001272
shatila,0.005128
shatilla,0.005434
shatter,0.003290
shebaa,0.014137
shehadi,0.010869
shell,0.002198
shelter,0.005092
shia,0.012336
shihab,0.055209
shiit,0.018560
shmona,0.005262
short,0.001392
shortcom,0.003045
shorten,0.002760
shot,0.002539
shown,0.001575
shut,0.002728
sicili,0.003002
sidon,0.012802
sieg,0.002707
sign,0.004489
signific,0.003480
significantli,0.001676
silk,0.005427
similar,0.001085
similarli,0.001722
simqania,0.005434
simqanieh,0.005434
sinc,0.006362
siniora,0.004926
sister,0.005017
site,0.010717
situat,0.001399
sixti,0.002764
sixtof,0.005434
siyfa,0.005434
size,0.001415
skeleton,0.002779
skill,0.001751
sla,0.004014
slacontrol,0.005434
slaughter,0.003016
slope,0.002624
slow,0.003753
slowli,0.002095
smash,0.003658
smith,0.001925
social,0.006937
socialist,0.002033
societi,0.005694
sociolog,0.002153
socit,0.003296
soldier,0.008272
sole,0.005535
solh,0.015385
solider,0.005019
solidifi,0.002992
solut,0.001555
solv,0.001672
someon,0.002123
sometim,0.003549
son,0.008701
soon,0.005000
sophist,0.004182
soqban,0.005434
sought,0.005312
sound,0.001913
sourc,0.002286
south,0.015657
southeastern,0.002823
southern,0.018556
southward,0.003041
southwestern,0.005673
sovereignti,0.004383
spagnolo,0.005434
spain,0.001931
spanish,0.001965
spar,0.004094
spare,0.002828
spark,0.002405
special,0.003410
specif,0.001087
specifi,0.001848
speech,0.003916
spell,0.002508
spent,0.004018
spill,0.003211
sponsor,0.002278
spontan,0.002349
spot,0.002275
spread,0.001606
spring,0.002050
squabbl,0.004187
squad,0.003495
squar,0.013480
st,0.001465
stabil,0.008475
staff,0.002142
staffan,0.004775
stage,0.006349
stalem,0.003257
stand,0.005124
star,0.002015
start,0.003451
state,0.013520
statement,0.006674
statesman,0.003146
statist,0.002982
statu,0.003036
statut,0.002670
stay,0.003979
steep,0.002912
stem,0.002043
step,0.001541
stephanorhinu,0.005434
stone,0.002029
stop,0.005387
strateg,0.001983
strategi,0.001693
street,0.002074
strength,0.003687
strengthen,0.004111
strife,0.005991
strike,0.002021
string,0.002414
strip,0.002385
strong,0.001320
stronghold,0.008876
structur,0.001061
struggl,0.001905
studi,0.006724
studyoxford,0.005434
style,0.001872
subayba,0.005434
subgroup,0.002860
subject,0.001156
subsequ,0.002836
succeed,0.011149
success,0.006026
successor,0.002041
suffici,0.001680
suggest,0.003817
suleiman,0.011598
sultan,0.018833
summit,0.002658
summon,0.003187
sunni,0.034431
supplant,0.002903
suppli,0.001481
support,0.016210
suppos,0.001997
surpris,0.002390
surrend,0.002504
surround,0.001664
surviv,0.001567
survivor,0.002823
suspect,0.004857
sustain,0.001777
suzerainti,0.003428
swore,0.004014
sykespicot,0.004712
symbol,0.001650
syracus,0.003472
syria,0.192396
syriac,0.003443
syrian,0.138602
tactic,0.004834
taif,0.030292
taken,0.002661
takeov,0.002813
tanzania,0.003053
tanzimat,0.004469
tarazi,0.010869
target,0.001692
tarif,0.004775
tauri,0.030813
tax,0.005124
taxat,0.002234
teach,0.001780
team,0.001898
technolog,0.001310
televis,0.002048
temp,0.003784
tempera,0.003257
tension,0.016516
tenur,0.002755
term,0.010306
terrain,0.002709
territori,0.025455
terrorist,0.002751
test,0.001485
testimoni,0.002924
th,0.017718
thackston,0.005434
themselv,0.002696
thencommand,0.005434
thi,0.028554
thirteenyear,0.004775
thirti,0.002450
thoma,0.001625
thought,0.001288
thousand,0.008014
threat,0.005719
threaten,0.006200
throw,0.002791
thu,0.001062
thwart,0.003221
tie,0.003545
time,0.008042
timelin,0.004279
timet,0.007073
tj,0.003735
tnt,0.003759
today,0.002611
togeth,0.003760
told,0.002305
toler,0.002299
toni,0.002816
took,0.011359
tool,0.001455
tourism,0.002114
tourist,0.007266
tower,0.002658
town,0.013065
traboulsi,0.005434
trade,0.010519
tradit,0.001144
tragic,0.003355
transform,0.001487
treason,0.003231
treasuri,0.002415
treat,0.001591
trend,0.001736
tri,0.004292
triangl,0.005376
triangular,0.003151
tribe,0.002237
tribun,0.005647
tributari,0.003237
trigger,0.006633
tripoli,0.018735
troop,0.032826
truck,0.002998
tuesday,0.003627
tunisia,0.003068
turk,0.008616
turkish,0.005263
turmoil,0.002834
turn,0.006283
tuscan,0.004162
tuscani,0.030482
twelver,0.003996
twentyf,0.003313
twice,0.002166
twothird,0.002490
twoyear,0.002955
type,0.001092
tyre,0.014803
ugarit,0.004162
ultim,0.001572
umayyad,0.003394
unabl,0.003777
unanim,0.002866
unauthor,0.003627
uncertainti,0.002115
uncomfort,0.003401
uncommon,0.005572
uncontest,0.003797
undercut,0.003495
underestim,0.003034
undergo,0.002107
undermin,0.002409
undisclos,0.003823
une,0.003443
uneas,0.003996
uneasi,0.003435
unifil,0.004327
uniiic,0.016304
unilater,0.002850
unit,0.008251
uniti,0.009989
univers,0.012238
unless,0.002036
unlik,0.001513
unresolv,0.003088
unrest,0.002520
unscr,0.009206
unstabl,0.002424
unsubstanti,0.004014
updat,0.002201
upper,0.005506
upris,0.002524
urban,0.001906
urg,0.007197
usa,0.002029
use,0.006684
ussama,0.005434
v,0.001620
vacuum,0.002371
vagu,0.002688
valid,0.003471
valley,0.014714
variou,0.006208
vehement,0.003450
vehicl,0.004127
venic,0.002978
verg,0.003211
veri,0.002076
verifi,0.002297
vestig,0.003331
veto,0.008848
vicepresid,0.002936
viceroy,0.003307
vichi,0.010395
viciou,0.003187
victori,0.006315
video,0.002000
view,0.002329
vigil,0.003536
vilayet,0.009021
villag,0.014539
villageshav,0.005434
violat,0.010055
violenc,0.015153
violent,0.004488
virtual,0.003595
visa,0.005911
visit,0.001826
vizier,0.003823
voic,0.002166
voluntarili,0.002946
vote,0.015513
wa,0.106574
wadi,0.007992
wage,0.001997
wali,0.018985
walid,0.012982
want,0.008390
war,0.064560
warfar,0.002305
warlord,0.006788
watch,0.002317
watersh,0.002921
way,0.001943
wazzan,0.005128
weak,0.005416
weaken,0.002276
wealth,0.005615
wealthi,0.004743
weapon,0.004036
websit,0.001718
week,0.003820
weight,0.001832
welcom,0.002508
wellfortifi,0.004926
went,0.005001
west,0.003121
western,0.006836
wheeler,0.003178
wherea,0.001577
white,0.001725
wide,0.004682
wife,0.002343
william,0.001456
win,0.002043
winter,0.002252
wish,0.004019
wit,0.006641
withdraw,0.050250
withdrew,0.012772
won,0.005520
world,0.006450
wound,0.014961
x,0.001609
xxiv,0.004267
xxv,0.004510
xxvi,0.004296
ya,0.003689
yasser,0.004469
yassir,0.005434
year,0.018346
yedikul,0.005434
yemen,0.003192
yemeni,0.007792
york,0.001399
young,0.001687
younger,0.002258
yousef,0.004655
youssef,0.004603
yuni,0.004775
yusuf,0.003480
zamir,0.005434
zone,0.001878
