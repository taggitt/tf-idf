abandon,0.009582
abbey,0.008255
abdic,0.007727
abolish,0.005359
abort,0.007165
accur,0.004369
accus,0.005143
action,0.003151
activ,0.005118
ad,0.009819
adapt,0.004084
administr,0.014344
advanc,0.003167
advoc,0.004402
affair,0.004240
affect,0.003351
affirm,0.006190
age,0.022484
ago,0.004969
agrarian,0.006953
agricultur,0.007624
airlin,0.006528
alarm,0.007144
albert,0.036489
alli,0.014211
allianc,0.004796
allow,0.002550
alreadi,0.003736
altogeth,0.006358
ambigu,0.006026
american,0.005811
ancestr,0.020115
ancient,0.003716
ani,0.002260
anna,0.051254
annex,0.029174
announc,0.004307
anoth,0.002466
appear,0.002970
appoint,0.004216
approv,0.008812
arblast,0.012732
archaeolog,0.005491
archbishop,0.007998
archduk,0.010166
ardenn,0.021648
area,0.010189
aristid,0.009751
arm,0.004089
armi,0.013234
artefact,0.016395
ask,0.004239
aspelt,0.013492
associ,0.002505
assum,0.003553
atlant,0.005435
attempt,0.008776
augment,0.006604
augsburg,0.009056
august,0.007794
austrasia,0.011856
austria,0.023735
austrian,0.012287
author,0.005775
autonomi,0.010718
avail,0.003135
away,0.003906
balkan,0.007275
baltimor,0.007551
banqu,0.009187
barbara,0.006790
barrack,0.008602
base,0.006704
basic,0.003111
bath,0.007201
battl,0.004843
bavaria,0.009056
bavarian,0.009244
bbc,0.005834
bc,0.022877
becam,0.031095
bech,0.011557
becom,0.019805
befor,0.002641
began,0.006176
begin,0.006031
belgian,0.036005
belgica,0.021334
belgium,0.036422
belgoluxembourgeois,0.013492
bellicos,0.010525
benelux,0.028378
benot,0.009006
bequeath,0.009394
bismarck,0.017437
blom,0.011557
blood,0.010072
bock,0.009562
bohemia,0.052192
bomb,0.005855
bombard,0.013728
bone,0.005800
boom,0.005867
bordeaux,0.009332
bore,0.006941
borussia,0.013492
bourbon,0.017131
brandenburg,0.037330
bridg,0.005109
brief,0.014704
brill,0.008155
britain,0.004356
british,0.003453
broadcast,0.006000
broader,0.005156
bronz,0.006224
brought,0.003812
bulg,0.008602
buoy,0.008801
burgundian,0.010113
c,0.005919
caesar,0.015332
came,0.003386
camp,0.011208
campo,0.009562
candid,0.004770
capit,0.006737
capita,0.005621
captur,0.004252
carolingian,0.008547
castl,0.007023
casualti,0.013570
cathol,0.005001
caus,0.011058
cede,0.006629
celt,0.009107
celtica,0.012732
centr,0.008517
central,0.005807
centuri,0.039896
challeng,0.003700
chancellor,0.007345
chang,0.002335
characteris,0.018627
characterist,0.003570
charl,0.019835
charlott,0.024508
cheapest,0.009215
childless,0.009711
choic,0.004069
choos,0.004455
chose,0.005826
church,0.004680
cipolla,0.010395
citi,0.014308
citizen,0.004238
civil,0.010659
civilian,0.005384
civilis,0.007069
claim,0.015721
claimant,0.017024
clara,0.008621
clean,0.006063
clearli,0.004657
cleve,0.010220
clipper,0.010525
close,0.002771
coal,0.006040
coalit,0.010038
cold,0.004937
collect,0.006043
combin,0.002953
command,0.004346
commentari,0.005971
commiss,0.008248
committe,0.004444
commun,0.010835
communist,0.005109
communistl,0.010999
complet,0.005722
compos,0.003930
concentr,0.008108
confeder,0.012366
confer,0.004077
confirm,0.017864
congress,0.004641
conomiqu,0.009833
conquer,0.005609
conquest,0.005581
conscript,0.022361
consid,0.009664
consist,0.005545
constitut,0.009813
construct,0.006416
contact,0.004347
contemporari,0.008104
context,0.003644
contin,0.005338
continu,0.007530
control,0.005504
cooper,0.003959
coordin,0.004391
cordial,0.008141
core,0.004399
corp,0.006111
corrupt,0.005086
count,0.004724
counter,0.005601
counti,0.006165
countri,0.068157
coupl,0.004793
court,0.003929
cover,0.003410
creat,0.002534
creation,0.011312
crisi,0.017100
critic,0.003047
cross,0.004521
cultiv,0.005291
cultur,0.006229
currenc,0.004539
custom,0.004189
d,0.003353
dalheim,0.013492
damag,0.004470
danish,0.006551
date,0.006924
daughter,0.028429
day,0.003126
dead,0.005193
deal,0.003337
death,0.007256
decad,0.003765
decemb,0.011405
decis,0.006985
declar,0.016005
declin,0.003933
decor,0.006726
defeat,0.009803
defenc,0.015129
demand,0.003698
denial,0.007151
depart,0.007264
depend,0.002733
deport,0.021390
descend,0.040115
design,0.003042
desir,0.003899
despit,0.003512
destroy,0.004332
deterior,0.006101
develop,0.004143
did,0.006150
die,0.003969
diekirch,0.012460
diminish,0.005609
disapprov,0.007769
discov,0.003648
disput,0.004167
district,0.005011
divis,0.007181
document,0.003999
domin,0.006818
domitian,0.010909
dpartement,0.009056
drastic,0.006201
du,0.005855
dub,0.006664
duchess,0.019269
duchi,0.153426
duff,0.009876
duke,0.052265
dure,0.021463
dutch,0.025440
duti,0.009859
dwell,0.021990
dynasti,0.016161
e,0.003357
earli,0.009978
eastern,0.004139
econom,0.030025
ecsc,0.010525
ed,0.006983
educ,0.003177
eec,0.009081
effect,0.002423
effort,0.003454
eighti,0.007453
elabor,0.005190
eldest,0.016510
elector,0.016052
electr,0.004124
elev,0.005564
elisabeth,0.060890
emerg,0.006393
emigr,0.012540
emperor,0.038506
empir,0.006960
empress,0.008300
encount,0.004912
encourag,0.004099
end,0.019089
engag,0.003949
engin,0.003594
english,0.006947
enjoy,0.004748
enlarg,0.012741
ensu,0.006097
ensur,0.004097
enter,0.007292
enthusiast,0.007109
entiti,0.004231
entourag,0.010166
especi,0.005792
essenti,0.003467
establish,0.005323
etymolog,0.005309
eugenia,0.011856
euro,0.019037
europ,0.016910
european,0.043492
eventu,0.007090
evid,0.023808
exchang,0.010577
execut,0.011334
exil,0.011392
exist,0.002480
expel,0.006013
express,0.003231
extern,0.001950
extinct,0.005670
eyschen,0.013492
fact,0.003166
famili,0.003453
famou,0.008240
far,0.003382
father,0.004385
favor,0.004119
favour,0.009809
februari,0.008104
fell,0.014451
fight,0.004680
final,0.003226
financ,0.003952
financi,0.010682
fluent,0.009187
follow,0.008534
fontana,0.009031
footnot,0.006648
forbidden,0.006594
forc,0.024059
foreign,0.003471
form,0.004077
formal,0.010012
formalis,0.007834
format,0.003637
formid,0.007696
formio,0.012460
fort,0.019726
fortif,0.015012
fortifi,0.007298
fortress,0.051036
fortun,0.006050
foundat,0.003477
franc,0.053454
francia,0.009791
franconia,0.011856
frank,0.005222
franz,0.007042
free,0.003046
french,0.050250
frenchspeak,0.008844
friendli,0.006143
fulli,0.004021
futur,0.003248
gain,0.003244
gallia,0.032998
gallic,0.019502
garrison,0.020274
gau,0.011856
gauleit,0.013492
gaulish,0.012460
gave,0.003880
genealog,0.007193
gener,0.007665
generalgouvern,0.026985
geograph,0.008675
georg,0.003843
german,0.094165
germani,0.036142
gibraltar,0.008088
given,0.002674
gnp,0.008183
goeth,0.008477
good,0.002965
got,0.006016
govern,0.022826
gradual,0.004287
grand,0.093251
granddaught,0.009967
grandmoth,0.018431
great,0.003005
greater,0.006718
greatgrandson,0.010063
greatgreatgrandmoth,0.013492
greatli,0.004367
greatnephew,0.012732
grevenmach,0.013492
group,0.004811
grow,0.003420
guarante,0.004892
gun,0.006262
gustav,0.006824
h,0.003704
ha,0.010670
habsburg,0.049532
hand,0.003304
handicraft,0.008128
harmonis,0.009081
head,0.006886
height,0.005317
heir,0.047770
heiress,0.011428
heirgener,0.026985
heirsgener,0.026985
held,0.006306
henri,0.017195
hereditari,0.006383
hess,0.007926
hi,0.028696
high,0.002700
highest,0.004084
histor,0.002995
histori,0.033321
historian,0.004494
hit,0.005521
hohenzollern,0.022397
holi,0.016940
holland,0.006947
hope,0.004541
host,0.004877
hostil,0.005531
hot,0.005873
hous,0.021466
howev,0.012987
hugo,0.006790
hundr,0.004052
husband,0.013895
ident,0.003818
ii,0.010471
iii,0.015301
immedi,0.003920
import,0.009098
includ,0.009257
independ,0.016436
industri,0.009132
infanta,0.013492
infiltr,0.007542
influenc,0.008812
inhabit,0.018546
inherit,0.009891
initi,0.002897
insight,0.004947
instal,0.010526
institut,0.002822
integr,0.009655
intend,0.004128
intermediari,0.006769
intern,0.014022
interwar,0.015103
introduct,0.003535
invad,0.010668
invas,0.009726
invent,0.004339
iron,0.009553
isabella,0.009492
issu,0.005723
iv,0.011578
jacqu,0.006187
jagiellon,0.011557
januari,0.003806
jch,0.012732
jean,0.016593
jeanclaud,0.009833
jewelleri,0.009160
joep,0.013492
johan,0.007758
johann,0.005707
john,0.003033
join,0.015868
joseph,0.021974
journal,0.003430
juli,0.011556
julich,0.013064
juliu,0.013570
juncker,0.045712
june,0.003885
junior,0.007029
justic,0.004507
king,0.033983
kingdom,0.003547
kmec,0.013492
knive,0.009244
known,0.008913
kossmann,0.012460
la,0.004451
labour,0.004514
laid,0.005032
lampaden,0.026985
land,0.020248
languag,0.020109
larg,0.007049
larger,0.003519
late,0.006174
later,0.010607
law,0.008058
leader,0.003899
leagu,0.010942
led,0.017266
leerssen,0.013492
left,0.010663
leidenboston,0.012732
letter,0.004401
letz,0.013492
level,0.010665
liber,0.012060
lichtburg,0.013492
lieuten,0.007116
life,0.002866
limbourg,0.013492
line,0.015956
linguist,0.005241
link,0.003877
lisbon,0.007812
list,0.004958
liszt,0.011856
lit,0.007428
lite,0.009527
littl,0.003412
live,0.002882
local,0.003019
locat,0.006556
london,0.015017
longer,0.003677
lorrain,0.009363
loss,0.007799
lost,0.003874
lotharingia,0.011856
loui,0.015093
low,0.027337
lower,0.003294
ltzebuergesch,0.026985
ltzelburg,0.013492
luccelemburc,0.013492
lucilinburhuc,0.026985
lutzburg,0.013492
luxembourg,0.864069
luxembourgish,0.124397
luxemburg,0.009056
m,0.003188
machin,0.004304
main,0.005645
mainli,0.003701
mainz,0.009598
majeru,0.012732
major,0.004730
make,0.002267
mallord,0.013492
manag,0.006386
mani,0.004030
manipul,0.004773
map,0.008157
march,0.007394
margu,0.012732
maria,0.006589
marieadlad,0.013492
market,0.003337
mass,0.003656
mast,0.008738
matern,0.007123
maulkuerfgesetz,0.013492
maximin,0.011557
mean,0.002455
mediat,0.010923
mediev,0.014266
member,0.017068
men,0.004134
mend,0.008565
merovingia,0.013492
met,0.004559
metallurgi,0.007260
michel,0.006111
mid,0.008879
middl,0.018380
mile,0.005182
militari,0.014427
millennium,0.005564
minist,0.024136
minor,0.004125
mittelrhein,0.026985
model,0.002777
modern,0.005348
mompach,0.013492
monarch,0.011776
monarchi,0.011430
monetari,0.009559
monk,0.006594
monolith,0.008494
mosel,0.011699
moselland,0.013492
mostli,0.003804
mother,0.015414
mr,0.011988
mudclad,0.013492
multipl,0.003429
museum,0.005376
muzzl,0.009751
mysteri,0.005885
myth,0.005693
napoleon,0.018241
nassau,0.009160
nassauweilburg,0.013064
nation,0.022234
nationbuild,0.009672
nato,0.013484
nazi,0.018650
nd,0.004148
near,0.010997
nearli,0.003883
necessari,0.003553
neighbor,0.005029
neighbour,0.016338
neolith,0.006643
netherland,0.062348
neutral,0.019520
new,0.003983
newspap,0.005519
nieder,0.012732
nineteenth,0.005577
noon,0.008494
north,0.007080
northern,0.004168
norwich,0.009967
nospelt,0.013492
noth,0.004717
novemb,0.007997
number,0.006691
ob,0.009527
observ,0.002942
occup,0.018582
occupi,0.017878
octob,0.007710
oetrang,0.013492
offens,0.006212
offer,0.006630
offici,0.006769
old,0.011067
oldest,0.004823
oligarchi,0.008255
onethird,0.006774
onward,0.005652
opinion,0.004738
oppida,0.013064
opposit,0.014674
opt,0.013871
orang,0.013328
orangenassau,0.024461
order,0.004913
organ,0.002431
organis,0.008442
origin,0.004941
otto,0.006281
outbreak,0.006066
outcrop,0.009081
outlaw,0.007158
owner,0.005043
pact,0.013389
pain,0.005646
painter,0.007961
palatin,0.009527
paleolith,0.007378
palgrav,0.006619
pan,0.007370
panzer,0.010824
paralys,0.009791
pari,0.004875
parti,0.017227
particip,0.003517
particularli,0.003126
partit,0.006176
partli,0.004793
partner,0.004873
pass,0.013854
passiv,0.006143
past,0.003536
patern,0.007986
patrimoni,0.019842
paul,0.007750
peac,0.004299
peak,0.004892
peasant,0.012433
peopl,0.004916
percent,0.013433
period,0.029140
perman,0.008641
perpetu,0.006040
person,0.005834
philip,0.016440
pin,0.007560
pit,0.007062
place,0.005042
plan,0.006752
play,0.006438
poet,0.006165
poland,0.012181
polic,0.014875
polici,0.009898
polit,0.019587
politician,0.010697
popul,0.015860
portug,0.005955
possess,0.008012
possessor,0.009791
potteri,0.014020
power,0.005232
pp,0.004168
pport,0.013492
predominantli,0.011350
preliminari,0.006818
presenc,0.003866
present,0.005112
presentday,0.017226
presid,0.007394
prevail,0.005408
prevent,0.007310
prewar,0.007656
primari,0.003374
prime,0.020538
primit,0.011314
primogenitur,0.010395
princ,0.033103
princip,0.011981
problem,0.002734
proce,0.006224
process,0.002416
proclam,0.007330
produc,0.005320
proper,0.004586
properli,0.005376
proport,0.008454
propos,0.003154
prosper,0.005285
provid,0.004536
provinc,0.009452
provision,0.006478
prussia,0.060561
prussian,0.054162
prussianpossess,0.013492
public,0.002574
pull,0.005867
purpos,0.003325
pursu,0.004649
push,0.004861
queen,0.016097
quell,0.007926
question,0.003210
rail,0.006434
rapidli,0.004310
rare,0.004207
reach,0.003295
read,0.002816
readi,0.005959
readili,0.005547
real,0.010096
reason,0.002910
reassign,0.009876
rebellion,0.011578
receiv,0.009293
recent,0.002807
recess,0.005803
rechtspartei,0.012460
recognis,0.005006
reconstruct,0.005081
record,0.003417
reduc,0.006143
reed,0.007706
refer,0.003242
referendum,0.005629
reform,0.003966
refug,0.006917
refus,0.004731
regain,0.006228
reich,0.007937
reign,0.005606
reintegr,0.008584
relat,0.004372
remain,0.026816
remerschen,0.026985
renaiss,0.010722
reportedli,0.006438
repres,0.002659
represent,0.004090
reprsent,0.010824
republ,0.007610
resign,0.011081
resist,0.012604
resolv,0.004709
resort,0.006016
respons,0.005624
result,0.015810
resuscit,0.010276
retain,0.004263
return,0.006562
reveal,0.008529
revolt,0.005903
revolut,0.004016
revolutionari,0.015480
rhine,0.008410
rhineland,0.009394
richest,0.007069
right,0.002909
rightw,0.007479
rival,0.005357
robert,0.003565
rocki,0.007747
role,0.011361
roman,0.039143
rome,0.016608
roof,0.007890
rose,0.004920
roughli,0.004548
round,0.015409
royal,0.004278
rule,0.023677
ruler,0.010834
ryswick,0.011198
s,0.016097
sale,0.004723
santer,0.011856
save,0.004604
scheme,0.004880
schuman,0.011557
seat,0.004798
second,0.013305
sector,0.034007
sell,0.004586
semiperman,0.010220
separ,0.008886
septemb,0.027371
serv,0.003227
servic,0.009477
settlement,0.004731
seven,0.004254
sever,0.004704
share,0.003159
shorthand,0.008331
sieg,0.006720
siegfri,0.009634
sigismund,0.009751
signatori,0.007801
signific,0.002880
significantli,0.004161
simon,0.005421
sinc,0.006769
sister,0.024912
site,0.011403
situat,0.013897
size,0.007028
slightli,0.004890
small,0.011045
socal,0.009106
social,0.008611
socialdemocrat,0.009459
soldier,0.010269
somewhat,0.004783
son,0.004320
sonja,0.011699
sousa,0.010909
south,0.003533
southern,0.012564
sovereign,0.016180
space,0.003479
spain,0.014386
spanish,0.004880
speak,0.004326
speaker,0.005794
sphere,0.005049
spngelskrich,0.013492
spoken,0.006070
spree,0.010334
squar,0.004780
st,0.018188
stabil,0.004208
start,0.002856
startingpoint,0.010113
state,0.017770
statu,0.011306
stay,0.004939
steadili,0.005952
steel,0.005652
step,0.003826
stone,0.005039
strateg,0.009850
strategi,0.004203
straw,0.008427
street,0.005149
strength,0.004576
strengthen,0.015312
strike,0.020077
striker,0.010113
strong,0.009836
strongest,0.019641
structur,0.005273
subsequ,0.007042
succeed,0.009226
success,0.005985
successor,0.005068
suffrag,0.006633
supergun,0.013064
support,0.005366
supposedli,0.006887
suppress,0.010335
suzerainti,0.008512
sway,0.007551
swept,0.007378
target,0.008403
tax,0.008481
tension,0.005125
term,0.002132
territori,0.027648
th,0.031050
thatch,0.010063
theodor,0.012352
theresa,0.009791
thi,0.032069
thirtyf,0.009751
threat,0.004733
throne,0.006366
thu,0.007913
thuringia,0.032728
till,0.006813
time,0.011979
titelberg,0.013492
titl,0.004050
tne,0.011428
tomb,0.007186
took,0.010575
total,0.009445
town,0.013901
trace,0.008653
trade,0.009793
tradit,0.005683
transform,0.003693
transport,0.003868
travel,0.003984
treati,0.051837
tree,0.004648
trenton,0.011557
treveri,0.050928
tri,0.003551
tribe,0.011110
trier,0.019345
trilingu,0.013064
tripartit,0.007937
troop,0.025467
trunk,0.008088
turbul,0.006997
turn,0.009359
turner,0.007245
twentyfirst,0.007666
uebl,0.013492
ultim,0.003903
uncertainti,0.005251
und,0.006638
underdevelop,0.007542
undermin,0.005981
union,0.036885
unit,0.009104
univers,0.004674
unrest,0.025034
upheav,0.007353
upper,0.004556
urban,0.004733
use,0.001843
uss,0.007579
utrecht,0.008512
v,0.012070
valley,0.005218
valu,0.005784
vauban,0.010909
verdun,0.010667
veri,0.005154
vernacular,0.007801
vi,0.012834
vicin,0.014566
victim,0.006063
victor,0.006073
victori,0.005226
vienna,0.012912
vii,0.027457
violent,0.005572
visa,0.007337
visit,0.004533
visitor,0.006172
vol,0.004849
volunt,0.006220
von,0.009153
voter,0.006053
vri,0.008678
wa,0.120265
wall,0.009556
war,0.064111
watchtow,0.010667
way,0.002412
week,0.009485
weilerlatour,0.013492
west,0.011624
western,0.006788
wherea,0.003915
whi,0.004152
wickerwork,0.013492
wife,0.005817
william,0.025311
withdrawn,0.006991
wolfgang,0.006852
women,0.004294
word,0.006264
world,0.020590
wort,0.011198
wound,0.018572
wrest,0.008780
write,0.003528
writer,0.004631
written,0.003572
xiv,0.007617
yanke,0.009921
year,0.013664
younger,0.011213
youngest,0.007769
zola,0.010824
zollverein,0.011557
