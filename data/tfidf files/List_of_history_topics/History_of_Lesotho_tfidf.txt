abandon,0.008088
abdic,0.013044
abl,0.005469
abroad,0.009402
accept,0.005435
access,0.005990
accid,0.010438
account,0.005099
accus,0.008683
acquir,0.007052
act,0.009897
action,0.005320
ad,0.005525
addit,0.004380
administr,0.006053
adult,0.008476
advic,0.009423
affair,0.014318
affect,0.005658
africa,0.084379
african,0.030356
afrikan,0.017253
agre,0.019584
agreement,0.006609
aim,0.006182
alien,0.010146
aliw,0.044109
alleg,0.008953
allow,0.004305
anc,0.031938
ancient,0.006273
ani,0.011447
annex,0.039401
announc,0.007271
apartheid,0.027630
appeal,0.016127
appoint,0.028470
april,0.013247
arabl,0.026489
area,0.008600
argu,0.005760
arm,0.020711
armi,0.029790
aros,0.008598
arrest,0.009515
arriv,0.006970
ascend,0.022691
assembl,0.047315
assist,0.006455
associ,0.004229
atop,0.014340
attack,0.006980
attain,0.008764
attempt,0.014816
attend,0.008261
august,0.026318
author,0.014625
background,0.007436
ballot,0.012120
bantu,0.014618
basi,0.005393
basotho,0.302479
basotholand,0.105176
basutoland,0.273158
bcp,0.199508
becam,0.023861
becaus,0.015720
bechuanaland,0.020312
becom,0.004179
befor,0.008917
began,0.015639
believ,0.005399
benevol,0.012975
berea,0.019750
better,0.005887
bicamer,0.011760
bnp,0.112999
boer,0.211171
boerrul,0.022778
bolea,0.022054
border,0.007690
bosiu,0.042070
botswana,0.013936
bound,0.007811
boundari,0.014577
britain,0.014710
british,0.110757
brought,0.012871
buri,0.010323
burnt,0.013420
cabinet,0.017567
caledon,0.021035
came,0.022868
campaign,0.007849
capac,0.007248
cape,0.065895
car,0.009239
casualti,0.022909
caus,0.014001
ceas,0.008832
cede,0.033572
centuri,0.004490
ceremoni,0.019305
chair,0.009821
chairman,0.009889
challeng,0.006246
channel,0.008631
charg,0.006747
chief,0.050971
cite,0.007313
citizen,0.007156
civilian,0.018181
claim,0.005308
clan,0.011464
close,0.004679
coalit,0.008473
collabor,0.008032
coloni,0.101233
colonis,0.011845
colonist,0.011956
combin,0.004986
command,0.007336
commiss,0.013924
commission,0.021509
commun,0.004572
complet,0.004830
conclus,0.007792
condit,0.004893
conflict,0.019541
congress,0.047016
conquer,0.009469
consist,0.014042
consolid,0.009182
constitu,0.008189
constituencybas,0.020647
constitut,0.044177
contact,0.007338
contest,0.018403
contrast,0.005907
control,0.023233
convent,0.006772
coordin,0.007413
council,0.038262
countri,0.046024
coup,0.047622
court,0.006633
courtmarti,0.019511
cover,0.005758
creat,0.004277
critic,0.005144
crossbord,0.013263
crown,0.018116
crush,0.010699
culmin,0.018533
damag,0.007547
day,0.010555
death,0.006125
decemb,0.006418
declar,0.013510
decre,0.010043
deem,0.009464
defeat,0.008274
defens,0.007676
defin,0.009599
demand,0.006243
democraci,0.015580
democrat,0.013705
demonstr,0.006286
deni,0.008107
deputi,0.009365
describ,0.004304
despit,0.017791
destroy,0.007313
destruct,0.008277
develop,0.006995
devis,0.009594
dialect,0.010288
did,0.015574
die,0.013402
differ,0.003707
difficult,0.006099
diplomat,0.008309
direct,0.009288
disarm,0.014171
disciplin,0.006444
disord,0.009015
dispers,0.009598
displac,0.008973
disput,0.014069
disrupt,0.009012
dissolv,0.008686
district,0.008459
divid,0.005545
divis,0.006061
domest,0.007246
dr,0.008677
dure,0.016103
dutch,0.008589
earli,0.025268
edmond,0.013838
educ,0.005364
effect,0.008181
elect,0.136627
elector,0.036133
emancip,0.012048
emerg,0.010792
empti,0.010038
encount,0.008293
encroach,0.012975
end,0.009207
endow,0.010665
engag,0.013335
england,0.007306
ensur,0.006917
enter,0.006155
entir,0.005461
establish,0.013480
etherington,0.061943
european,0.005244
eventu,0.005984
everi,0.005268
evolut,0.006836
execut,0.019134
exert,0.009661
exil,0.019233
exist,0.008374
expand,0.005956
expans,0.007104
experi,0.005177
experienc,0.015118
extend,0.005553
extens,0.011371
extent,0.006928
extern,0.006584
fact,0.005346
fail,0.012661
fair,0.008528
fate,0.010631
father,0.007402
favor,0.006954
favour,0.008280
fear,0.007900
februari,0.013681
fertil,0.008826
fight,0.023702
final,0.005446
fled,0.010191
follow,0.025212
forc,0.036103
foreign,0.005861
formul,0.007210
fortress,0.012308
fought,0.009550
fraudul,0.012525
free,0.025714
gain,0.016433
gave,0.013103
gener,0.022647
goe,0.008102
govern,0.047097
grant,0.006902
great,0.010149
greater,0.005671
grew,0.007524
griqualand,0.022054
group,0.012184
gun,0.021144
ha,0.009006
half,0.006197
halt,0.010259
hand,0.005578
handl,0.008187
harlow,0.014684
havoc,0.015806
head,0.017437
health,0.012553
held,0.015970
hereditari,0.010776
hi,0.032296
high,0.004559
higher,0.005352
histori,0.022500
hock,0.017656
hold,0.005423
hospit,0.008878
howev,0.036541
ii,0.035355
iii,0.043052
imag,0.006932
impass,0.015123
implement,0.006268
inabl,0.010869
incid,0.017817
includ,0.009376
incom,0.006896
incorpor,0.012365
increas,0.004329
independ,0.018497
indic,0.005538
individu,0.004644
inflict,0.012256
inhabit,0.007827
initi,0.004892
instabl,0.009735
instal,0.017770
instrument,0.006932
insurrect,0.013206
intensifi,0.011037
interim,0.021609
intern,0.023673
interpret,0.006006
interven,0.009675
investig,0.006309
invit,0.009278
involv,0.004364
ipa,0.015464
irregular,0.031599
isbn,0.005322
isol,0.007310
januari,0.025702
join,0.013394
jonathan,0.063160
june,0.013119
junior,0.023734
justin,0.013654
kamoli,0.045556
kennedi,0.011979
kholo,0.021035
kill,0.014865
king,0.100396
kingdom,0.023955
known,0.011285
korana,0.022778
land,0.079761
landslid,0.038339
langa,0.022778
languag,0.005658
larg,0.003967
largest,0.006217
later,0.008953
lcd,0.084943
ldf,0.021493
leabua,0.066164
lead,0.004491
leader,0.013164
leadership,0.016334
leav,0.025498
led,0.019432
left,0.006000
legisl,0.034458
legislatur,0.018418
lekhanya,0.132328
lerib,0.020647
lerothodi,0.022778
lesotho,0.430497
letsi,0.132328
level,0.004501
lieuten,0.012013
lifaqan,0.042070
link,0.003273
list,0.008371
local,0.010196
longman,0.025591
loot,0.012748
lose,0.007916
loss,0.006583
lost,0.032705
louw,0.020015
lowland,0.013151
lsutu,0.022054
mafeteng,0.020647
major,0.007986
make,0.007655
mani,0.003402
maraud,0.033977
march,0.006241
maseru,0.019292
matter,0.005661
meantim,0.013461
member,0.019209
mfecan,0.038182
migrat,0.008042
militari,0.060888
minist,0.081492
minor,0.006965
missionari,0.010658
modern,0.009029
mohal,0.020647
mokhehl,0.110274
monarch,0.009940
monarchi,0.009647
month,0.006649
moorosi,0.045556
moreov,0.008634
moshoesho,0.378636
mosisili,0.020647
mosiu,0.022778
mountain,0.008097
mowew,0.022778
multiparti,0.019314
mutin,0.031021
mutini,0.065399
natal,0.015204
nation,0.045877
nationalist,0.009758
nativ,0.015559
nek,0.019292
neutral,0.008238
new,0.036982
newli,0.007918
norman,0.021209
north,0.023904
notabl,0.005775
note,0.008679
ntsu,0.022054
nullifi,0.014787
number,0.007530
observ,0.009935
obtain,0.005850
occupi,0.007545
occur,0.009563
oclc,0.010840
octob,0.006508
offic,0.018174
onli,0.006929
open,0.005152
oper,0.009496
oppos,0.006289
opposit,0.055738
orang,0.033750
order,0.008295
orthographi,0.015419
oust,0.011760
outcom,0.007694
outsid,0.005741
overwhelm,0.010091
pakalitha,0.020647
palac,0.010761
paramount,0.037882
parliament,0.045546
parti,0.075614
pass,0.005847
pastur,0.014117
peac,0.021776
peopl,0.037347
period,0.008944
philip,0.009251
phisoan,0.022778
pitso,0.022778
place,0.012769
plain,0.009835
polic,0.025112
polici,0.011139
polit,0.028343
politi,0.011637
popul,0.010710
possibl,0.008804
postindepend,0.029437
power,0.030917
powerbrok,0.019292
preclud,0.012374
presenc,0.006526
present,0.004315
preserv,0.007274
pressur,0.006496
prevent,0.006170
previou,0.006508
prime,0.076278
print,0.008105
prison,0.009160
process,0.004080
produc,0.004490
progovern,0.015969
properti,0.005225
proport,0.021407
proscrib,0.015419
prosper,0.008922
protect,0.011504
protector,0.064072
protest,0.024062
provid,0.003829
provinc,0.007978
publicli,0.009235
purg,0.012072
pursu,0.007849
qacha,0.020647
queen,0.009058
quell,0.013380
quth,0.021035
ramaema,0.022054
reduc,0.005185
refer,0.005474
reflect,0.005882
refus,0.031951
refut,0.010811
regim,0.007976
region,0.014704
regular,0.007498
reign,0.018929
rel,0.004907
releg,0.013911
remain,0.022635
remov,0.006319
republ,0.006424
request,0.016639
resid,0.014503
resist,0.007093
restor,0.023223
result,0.022877
retain,0.014396
retreat,0.010572
return,0.033238
review,0.006133
reviv,0.008698
revolt,0.019931
rival,0.009043
river,0.014967
rldf,0.021035
root,0.006914
rose,0.016612
royal,0.014444
rule,0.054962
s,0.003882
sadc,0.048077
save,0.007773
saw,0.013396
seat,0.064801
second,0.013477
selfappoint,0.018416
selfcoup,0.021493
senat,0.009390
senek,0.022778
sent,0.007587
septemb,0.013202
seqiti,0.022778
seri,0.005452
seriou,0.007668
servic,0.015999
sesotho,0.040624
sesothospeak,0.022778
session,0.010413
set,0.004240
settl,0.023338
settler,0.010299
seven,0.007183
sever,0.007942
shaka,0.016988
shape,0.006524
share,0.005333
sign,0.012544
signific,0.009725
sinc,0.003809
singl,0.005098
size,0.005932
small,0.004661
smaller,0.006477
soldier,0.017336
son,0.021882
sooner,0.013632
sotho,0.037809
soto,0.015331
south,0.065622
southern,0.042421
speak,0.007304
split,0.007829
spoke,0.010513
sporad,0.012156
stabil,0.014209
stabl,0.007497
stage,0.013305
state,0.023332
statebasotho,0.044109
statu,0.012724
strike,0.008473
strip,0.009996
structur,0.004450
struggl,0.007985
subdu,0.012308
subject,0.004846
subsaharan,0.011933
subsequ,0.017834
substanti,0.007071
subvert,0.015204
success,0.005051
suffrag,0.011199
support,0.004529
surround,0.013952
suspend,0.028407
suzerainti,0.014370
swaziland,0.029303
taken,0.005577
takeov,0.011791
task,0.021200
tension,0.008652
term,0.007199
territori,0.066678
tertiari,0.011520
th,0.004368
thaba,0.063106
thaban,0.045556
themselv,0.011301
thi,0.028493
thing,0.006061
thoma,0.006814
threat,0.007990
threaten,0.008661
throne,0.021495
time,0.003370
tlai,0.022778
tona,0.021035
town,0.007822
tradit,0.004797
train,0.006578
transfer,0.019556
transform,0.012471
transit,0.006632
translat,0.006558
treati,0.031821
trek,0.041234
tribe,0.009378
troop,0.017197
tumultu,0.015755
twothird,0.010438
undermin,0.020194
union,0.022643
uniqu,0.006495
univers,0.003945
unpreced,0.010818
unrest,0.010565
unsuccess,0.009797
use,0.003112
vacuum,0.009940
valu,0.004882
variou,0.004336
victori,0.035291
victoria,0.011199
vie,0.013961
view,0.009765
violenc,0.027217
violent,0.018813
virtual,0.007533
voortrekk,0.081248
vote,0.007224
wa,0.121817
wage,0.008370
wane,0.012428
war,0.059527
ward,0.011657
wax,0.012942
wealth,0.007844
wepen,0.022778
west,0.013082
western,0.005730
wide,0.004906
withdrew,0.010706
wodehous,0.018904
won,0.038558
work,0.007469
worth,0.008625
wrought,0.014090
year,0.007689
zimbabwean,0.016265
zulu,0.077097
zululand,0.020647
