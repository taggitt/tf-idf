abund,0.005041
adjectiv,0.007670
administr,0.006951
africa,0.024221
agricultur,0.003694
air,0.003818
airport,0.005475
anthem,0.007224
architectur,0.004688
area,0.002468
arm,0.003963
armi,0.004275
art,0.007605
articl,0.002954
atla,0.005828
atlant,0.010535
bank,0.007189
bengou,0.013077
bicamer,0.006751
bird,0.005116
bn,0.007469
border,0.004415
boundari,0.004184
branch,0.012749
cabinet,0.005042
cameroon,0.016148
capit,0.006529
care,0.003771
central,0.002814
christian,0.003890
cinema,0.006739
citi,0.003466
climat,0.004286
coastlin,0.006244
coat,0.005947
code,0.011654
common,0.007283
commun,0.002625
compani,0.003170
congo,0.013702
countri,0.021138
court,0.003808
cuisin,0.006825
cultur,0.006037
currenc,0.004399
curv,0.004939
demograph,0.005391
demographi,0.006751
densiti,0.004845
depart,0.003520
diplomat,0.009540
divis,0.006959
domain,0.003921
east,0.003716
eastern,0.004012
econom,0.002645
economi,0.006602
ecoregion,0.014317
educ,0.006159
eighth,0.006745
elect,0.003565
endonym,0.015889
energi,0.006768
enforc,0.004531
english,0.006733
environ,0.003204
equat,0.004020
equatori,0.014498
exchang,0.003417
execut,0.003661
extern,0.001890
extrem,0.003467
featur,0.003269
festiv,0.006003
flag,0.005772
follow,0.002067
footbal,0.006468
forc,0.007772
foreign,0.006729
form,0.001975
franc,0.003700
ft,0.005649
ga,0.013046
gab,0.011491
gabon,0.992048
gabones,0.043841
gabonrel,0.026154
gdp,0.004861
gener,0.001857
geograph,0.004203
geographi,0.009516
glacier,0.006757
govern,0.019664
guid,0.003654
guinea,0.018310
gulf,0.005847
hdi,0.009015
head,0.006674
health,0.007207
help,0.002829
hemispher,0.005886
heritag,0.010574
high,0.002617
highest,0.003958
hinduism,0.006215
histori,0.006458
holiday,0.006084
hous,0.010402
hundr,0.003927
index,0.004106
industri,0.002950
infrastructur,0.004376
intern,0.004530
internet,0.008480
invest,0.003680
islam,0.004302
iso,0.024129
judaism,0.006182
judici,0.004962
km,0.029188
land,0.003270
languag,0.003248
law,0.005206
legisl,0.003956
lgbt,0.007301
librevil,0.021146
link,0.001879
list,0.021626
literatur,0.003860
local,0.002926
locat,0.003177
low,0.003311
lower,0.003193
m,0.006181
make,0.002197
mammal,0.005470
media,0.003963
member,0.005514
membership,0.004759
middl,0.003562
militari,0.017478
minist,0.003898
mission,0.008617
mont,0.006478
municip,0.005265
music,0.004749
nation,0.011971
natur,0.004613
nomin,0.004510
north,0.003430
northwest,0.005909
ocean,0.008893
offici,0.006561
oil,0.004312
olymp,0.006524
order,0.002381
organ,0.002356
outlin,0.012617
overview,0.004329
park,0.005050
parliament,0.004358
parti,0.003339
peopl,0.002382
point,0.002506
polici,0.003197
polit,0.008136
popul,0.009223
presid,0.003583
presidenti,0.005067
prime,0.003981
privat,0.003552
pronunci,0.006570
prosper,0.005122
provid,0.002198
provinc,0.004580
public,0.002495
rail,0.006236
rank,0.012550
record,0.003312
refer,0.003142
region,0.011256
relat,0.004237
religion,0.004002
republ,0.022129
resourc,0.003181
right,0.002819
river,0.004296
road,0.004564
senat,0.005391
share,0.003062
sikhism,0.007275
site,0.007367
small,0.002676
south,0.006849
special,0.002735
sport,0.010388
state,0.003827
statesgabon,0.013077
stock,0.004219
subsaharan,0.006851
suprem,0.004478
symbol,0.003970
televis,0.004929
th,0.005015
theatr,0.006178
time,0.003870
togeth,0.003015
topic,0.006940
toplevel,0.007104
tourism,0.005087
transport,0.007498
unit,0.004411
upper,0.004416
utc,0.007449
west,0.011265
wildlif,0.006232
world,0.004434
xaf,0.012076
zone,0.004520
