abstract,0.013640
academ,0.046440
academi,0.009955
accept,0.003243
accomplish,0.005192
account,0.003043
accuraci,0.005603
achiev,0.006500
acoust,0.007218
acquir,0.004208
act,0.002953
action,0.012699
activ,0.010312
actornetwork,0.011177
actual,0.006613
adam,0.005243
addit,0.007841
address,0.007781
adject,0.007062
administr,0.018063
admonit,0.010241
aerial,0.007276
affair,0.004272
affect,0.003376
age,0.006471
agenc,0.004064
agent,0.008810
aid,0.003876
aim,0.014758
al,0.004431
albani,0.017607
alfr,0.005294
allencompass,0.008824
alongsid,0.005373
altern,0.009960
alvin,0.007972
alway,0.010423
amalgam,0.007509
ambient,0.007509
ambigu,0.006071
amend,0.005448
america,0.003621
american,0.008782
amsterdam,0.006572
analys,0.005051
analysi,0.065408
analyz,0.008686
ancient,0.011230
andor,0.004521
andrew,0.005247
angel,0.006196
ani,0.009108
ann,0.006051
annal,0.006932
anonym,0.006841
anoth,0.009940
antholog,0.007337
anthropo,0.011081
anthropolog,0.128872
anthropologist,0.019887
antipositiv,0.032019
apolit,0.009228
appear,0.002992
appleton,0.008704
appletoncenturycroft,0.010296
appli,0.030384
applic,0.015428
approach,0.038657
arbitrari,0.005579
arbor,0.008424
archaeolog,0.027661
architectur,0.014621
archiv,0.009511
area,0.023096
aris,0.004215
aristotelian,0.006703
arm,0.004119
aros,0.010262
art,0.027669
articl,0.006141
artifact,0.006122
aspect,0.030562
assess,0.008802
associ,0.012620
assum,0.007158
assumpt,0.004402
attempt,0.008841
august,0.007852
author,0.002909
autonom,0.005224
autonomi,0.005399
avail,0.003158
avoid,0.007718
award,0.004555
b,0.013689
ba,0.027499
babi,0.006754
bachelor,0.026915
backhous,0.020082
bacon,0.006926
baird,0.009864
balanc,0.008178
balt,0.009706
bank,0.003736
baran,0.009994
base,0.018011
basi,0.009655
basic,0.003134
baxter,0.009174
becam,0.005695
becaus,0.014071
becom,0.002494
befor,0.005321
began,0.003110
begin,0.015189
behavior,0.014618
behaviour,0.076855
belford,0.011512
believ,0.003222
belong,0.004402
berger,0.008803
berkeley,0.006084
better,0.003513
bibliographi,0.004531
bind,0.005380
bioeconom,0.009994
biofact,0.009950
biogenet,0.010472
biolog,0.033255
birmingham,0.007984
birth,0.009181
blackwel,0.006513
block,0.004641
blur,0.007254
bodi,0.003126
bogardu,0.011281
book,0.005922
bordeaux,0.009401
boston,0.017231
boundari,0.008698
bpsi,0.022355
brain,0.004985
branch,0.039758
breadth,0.007400
bridg,0.005147
brief,0.004937
briefer,0.009906
bring,0.003873
british,0.006957
broad,0.025107
broadcast,0.006045
broader,0.010389
broadli,0.020860
brown,0.005286
bruce,0.006401
brunswick,0.008424
bsc,0.026730
build,0.023078
built,0.003883
busi,0.007024
buy,0.009857
byrn,0.009563
c,0.020871
calvert,0.009783
cambridg,0.024795
came,0.003411
canaktanweb,0.011512
capabl,0.004109
captur,0.004284
carey,0.018146
case,0.018262
categori,0.007962
cathol,0.005038
censu,0.006118
centr,0.008580
central,0.002925
centuri,0.037512
certain,0.002855
challeng,0.007454
champion,0.006477
chang,0.011762
charl,0.003996
chemistri,0.004573
chicago,0.020342
child,0.005011
choic,0.012300
chomskyaim,0.011512
choos,0.004488
cinema,0.007005
citi,0.003603
civic,0.006797
civil,0.003579
claim,0.003167
clarif,0.008229
clark,0.006041
class,0.006922
classic,0.013790
classif,0.004604
classifi,0.004221
clearli,0.009383
climat,0.004455
clinic,0.010884
close,0.005584
closer,0.005163
cluster,0.005725
codif,0.008575
cognit,0.014841
coin,0.004321
coland,0.010990
collect,0.015220
colleg,0.013589
coloni,0.004315
combin,0.011902
come,0.008953
command,0.004378
commiss,0.008309
common,0.005046
commonli,0.006931
commun,0.057305
compani,0.006590
companion,0.006019
compar,0.006173
compil,0.005384
complet,0.002882
complex,0.015269
compos,0.003959
comprehens,0.004596
compris,0.004269
comput,0.013832
comt,0.061946
concentr,0.008168
concept,0.008737
conceptu,0.010046
concern,0.017579
condens,0.006038
confer,0.008216
confid,0.005338
confidenti,0.007784
conflict,0.003887
congress,0.004676
connect,0.003436
connot,0.006897
consensu,0.005205
consequ,0.003548
consid,0.009735
consider,0.003507
consili,0.009496
constitut,0.009885
construct,0.009695
construction,0.009633
constructiv,0.008489
consum,0.004155
consumpt,0.004867
contact,0.013138
contemporari,0.024493
context,0.014685
contextu,0.007654
continu,0.012643
contract,0.004186
contrast,0.003525
convers,0.004305
correl,0.009729
correspond,0.003788
council,0.007610
countri,0.002746
cours,0.007274
cover,0.010308
creat,0.012763
credit,0.008077
creol,0.008070
crimin,0.005286
criminolog,0.008743
criterion,0.006266
critic,0.039909
critiqu,0.015906
cross,0.004555
crossdisciplinari,0.017222
crossfertil,0.009783
crossnat,0.010041
crucial,0.010160
cultur,0.056484
cup,0.007005
current,0.005343
curriculum,0.006464
custom,0.012660
cut,0.004430
d,0.020271
daili,0.009762
data,0.026646
databas,0.004882
david,0.003707
deal,0.016810
decad,0.003793
dedic,0.005013
defin,0.020049
definit,0.006600
degre,0.044431
delanti,0.010990
deliv,0.004919
delv,0.008704
demand,0.007452
democraci,0.009297
demographi,0.028071
denslow,0.011512
depart,0.010977
departur,0.006409
depend,0.005507
deriv,0.015866
describ,0.028256
descript,0.019075
design,0.018392
desir,0.003927
determinist,0.006663
develop,0.039657
development,0.005875
devianc,0.009906
dialect,0.018418
dichotomi,0.007509
diderot,0.008472
differ,0.019912
differenti,0.008451
difficult,0.007280
dimens,0.009604
direct,0.011085
disciplin,0.115370
discours,0.017144
discuss,0.003405
dissemin,0.012999
distinct,0.012857
distinguish,0.014821
distribut,0.013774
diverg,0.005640
divers,0.012042
divid,0.013237
divis,0.007234
document,0.012087
doe,0.011295
domain,0.016304
domesday,0.010352
dordrecht,0.008108
drama,0.007049
draw,0.034226
drive,0.004484
durkheim,0.051037
durkheimian,0.010904
dutton,0.009432
dynam,0.019564
e,0.023673
earli,0.007539
earn,0.004722
earth,0.011834
easili,0.004304
eclect,0.008095
ecolog,0.004735
econom,0.098991
econometr,0.007392
economi,0.017157
economist,0.004717
ed,0.014070
edit,0.003932
educ,0.044814
educar,0.011512
edwin,0.006629
effect,0.009764
efferson,0.011512
electron,0.004204
element,0.015978
elementari,0.005668
elsevi,0.007055
embodi,0.005809
emerg,0.006440
emphas,0.008819
emphasi,0.013672
empir,0.014024
empiric,0.013605
employ,0.009998
encompass,0.032927
encyclopedia,0.025964
end,0.005494
endeavour,0.007254
endow,0.006364
enforc,0.009421
engag,0.003978
engin,0.003621
english,0.003499
enhanc,0.004653
enlighten,0.010973
enquiri,0.007607
entir,0.009777
entiti,0.008526
envelop,0.007321
environ,0.019984
environment,0.028277
epistemolog,0.011943
equat,0.008357
era,0.003969
eric,0.005968
erna,0.010904
especi,0.002917
essenti,0.003492
establish,0.018770
estat,0.005348
et,0.004319
ethic,0.018498
ethno,0.010241
ethnolog,0.008523
etholog,0.007996
europ,0.003407
european,0.003129
evalu,0.008707
event,0.006539
everi,0.003144
evid,0.006852
evidenc,0.007075
evolut,0.008159
evolutionari,0.004950
evolv,0.008248
examin,0.031461
exampl,0.011251
exchang,0.007103
exist,0.007495
expand,0.010663
experi,0.018539
experiment,0.016788
explain,0.006765
explanatori,0.007081
explor,0.007656
exposit,0.006886
extens,0.003393
extern,0.001964
eykhoff,0.011512
f,0.014804
facetofac,0.009001
facilit,0.004529
fact,0.003190
factor,0.006371
faculti,0.005803
fall,0.003429
falsifi,0.007337
famili,0.017396
far,0.006814
father,0.008835
featur,0.006796
femin,0.007500
feminist,0.006586
ferdinand,0.006791
feuerbach,0.009464
field,0.090550
fieldbas,0.011281
figur,0.007442
financi,0.003587
firm,0.004487
firmli,0.006567
fit,0.004574
fix,0.011964
flexibl,0.005562
focu,0.011058
focus,0.027981
folk,0.006495
follow,0.012895
fontain,0.018146
forag,0.007635
foreign,0.006995
forese,0.007859
form,0.012322
formal,0.016810
formaldeduct,0.011512
formul,0.017210
forth,0.010923
foundat,0.007007
fourier,0.007011
fourth,0.004687
frame,0.005134
framework,0.011548
frankfurt,0.007424
freestand,0.009432
french,0.007231
frequenc,0.005134
freud,0.007204
friedrich,0.005495
fring,0.007189
function,0.016505
fundament,0.006881
futur,0.009817
g,0.010904
gain,0.003268
galavotti,0.011512
game,0.009221
gap,0.005230
gender,0.005760
gener,0.040544
generaliz,0.009312
geograph,0.017478
geographi,0.098915
geomat,0.009864
georg,0.007744
german,0.003794
gi,0.015035
gisc,0.011512
given,0.008084
global,0.014272
goal,0.014782
good,0.002987
gorton,0.010823
govern,0.012775
gp,0.007321
graduat,0.005034
grand,0.010437
graunt,0.010137
great,0.009084
greater,0.003384
greec,0.005078
greek,0.015219
green,0.004620
ground,0.007819
group,0.007270
grow,0.006890
growth,0.003400
guidelin,0.005959
gulbenkian,0.011512
h,0.018661
ha,0.030456
half,0.003698
hamilton,0.006668
happen,0.004246
hard,0.004409
hargittai,0.011512
harri,0.011301
health,0.007491
heard,0.006388
heavi,0.009287
heavili,0.004331
hegel,0.007337
henc,0.004192
herbert,0.011635
heterodox,0.007881
hi,0.004818
hinton,0.009371
histor,0.036218
histori,0.058184
historian,0.004527
historiograph,0.008377
historiographi,0.006819
holism,0.008377
holist,0.013807
holm,0.007635
hope,0.004574
hopkin,0.006619
horticulturalist,0.010904
houghton,0.008174
household,0.016034
howev,0.019625
human,0.144961
humanist,0.013053
hunt,0.005422
hutchinson,0.016633
hypothesi,0.014040
icr,0.010241
idea,0.015077
identif,0.005292
identifi,0.003247
ii,0.007032
iii,0.005138
ill,0.015651
impart,0.015160
imperi,0.010468
implement,0.003740
implic,0.004681
import,0.002291
improv,0.003306
incept,0.006993
includ,0.037302
incorpor,0.003689
increasingli,0.011882
indepth,0.015589
indic,0.006610
individu,0.036027
industri,0.009200
industrialorganiz,0.010472
inequ,0.011306
inferior,0.006540
influenc,0.029593
inform,0.024040
initi,0.002919
inquiri,0.016077
instanc,0.010916
institut,0.028431
institution,0.014202
instruct,0.005058
integr,0.019454
intellectu,0.018437
intent,0.013564
interact,0.030170
interaction,0.009341
interconnect,0.006495
interdepend,0.006744
interdisciplinari,0.033138
interfer,0.005484
intern,0.028253
interpret,0.025091
interpretiv,0.010241
interpretivist,0.011081
interrel,0.013116
interv,0.005660
interview,0.011111
introduct,0.032056
intut,0.010904
investig,0.015059
involv,0.010418
isbn,0.012704
issn,0.013865
issu,0.005766
itpartli,0.011512
j,0.023619
japan,0.004443
jeanjacqu,0.008008
john,0.012224
johnson,0.006139
join,0.003996
jone,0.005965
journal,0.006911
judgement,0.006791
judgment,0.005693
judici,0.005158
jurisprud,0.022605
justic,0.009081
k,0.008146
karl,0.030319
kegan,0.008910
kind,0.007157
kluwer,0.008148
know,0.004070
knowledg,0.027033
knowledgebas,0.008506
krimerman,0.011512
kuper,0.022162
l,0.019772
label,0.004833
laboratori,0.004558
laboratorybas,0.010137
labour,0.009095
lack,0.003425
lagu,0.011281
laid,0.005070
landform,0.008301
landscap,0.005601
languag,0.030387
lanham,0.008704
lann,0.010673
larg,0.009469
late,0.012440
latin,0.012666
lave,0.010603
law,0.064945
lawmak,0.008174
lead,0.002680
learn,0.011320
leavitt,0.009950
lectur,0.004884
legaci,0.005648
legal,0.014826
length,0.004456
let,0.005033
level,0.002686
lex,0.008910
liber,0.004049
libertarian,0.007108
librari,0.016880
life,0.017325
like,0.016794
limit,0.005486
line,0.003214
linguist,0.100330
link,0.001953
lionel,0.008685
lippincott,0.009597
list,0.007493
literari,0.005633
literatur,0.012039
littl,0.006875
live,0.008713
lo,0.006160
local,0.006084
logi,0.009432
london,0.011346
long,0.002916
longlast,0.008272
look,0.003708
lot,0.011064
loui,0.015205
lubbock,0.009864
luckmann,0.010603
m,0.009637
macmillan,0.006136
macroeconom,0.006068
magnitud,0.005541
main,0.008530
major,0.011914
make,0.006852
malden,0.009228
man,0.003974
manag,0.022517
mani,0.032482
manifest,0.005167
manipul,0.004809
manual,0.005673
mapmak,0.009669
march,0.007449
mark,0.003606
market,0.016810
marriag,0.005678
marx,0.031389
marxist,0.042687
mass,0.014733
massiv,0.004779
materi,0.015737
materialist,0.007261
mathemat,0.042371
max,0.021955
mckean,0.010603
md,0.006648
mean,0.034630
meanwhil,0.005431
measur,0.014709
media,0.020599
mediat,0.005502
medicin,0.008564
member,0.005731
mental,0.024301
merton,0.008258
messag,0.005581
metaphys,0.005746
metatheoret,0.020705
method,0.066761
methodolog,0.052296
michigan,0.006482
microeconom,0.006993
midrang,0.009744
mifflin,0.008287
mile,0.015662
militari,0.003633
million,0.003527
mind,0.008471
minneapoli,0.008361
minnesota,0.007239
mode,0.010113
model,0.050360
modern,0.029635
modul,0.006064
monitor,0.004857
monograph,0.007011
moral,0.008986
morpholog,0.006153
mortal,0.006003
multidisciplinari,0.006718
multipl,0.003454
multistrategi,0.010904
music,0.009872
myriad,0.007693
mytholog,0.006281
narr,0.005910
nation,0.009955
nationalfeder,0.011392
natur,0.069537
necessarili,0.008793
need,0.008100
neg,0.003894
neil,0.007134
network,0.007509
neural,0.012070
neurolinguist,0.009783
neuropsycholog,0.032231
neurosci,0.012385
new,0.018056
ngo,0.007384
nietzsch,0.007589
nisbet,0.010472
nj,0.006590
noam,0.007654
nobl,0.005840
nomo,0.009563
nonacadem,0.009464
nongovernment,0.007182
nonindustri,0.009906
nonlinear,0.006153
nonoverlap,0.009783
nonwestern,0.008377
norm,0.010419
note,0.005179
noun,0.007068
nuanc,0.007972
number,0.002246
numer,0.003327
object,0.009211
observ,0.026679
occup,0.004680
occupi,0.004502
ocean,0.004621
offer,0.010019
offici,0.003409
oiko,0.009950
old,0.003716
olog,0.009744
onli,0.010337
open,0.006149
oper,0.005667
oppon,0.005579
oppos,0.003753
option,0.004686
order,0.007425
organ,0.014699
origin,0.014934
outlet,0.007176
outlin,0.021859
outsid,0.006852
overal,0.004110
overlap,0.009984
overt,0.008229
overwhelm,0.006022
owe,0.005466
oxford,0.004220
p,0.006857
paper,0.003705
paradigm,0.016326
parent,0.005058
pareto,0.007903
parlanc,0.008685
parson,0.008331
particip,0.010629
participatori,0.008095
particular,0.019679
particularli,0.003149
past,0.003563
pastoralist,0.008845
path,0.009107
paul,0.011712
pay,0.004193
peasonallyn,0.011512
pedagogi,0.008392
peopl,0.024763
perceiv,0.004516
perform,0.003157
period,0.010674
perri,0.016662
person,0.005877
perspect,0.032152
persuas,0.014150
pertain,0.006210
peter,0.004095
phenomena,0.038596
phil,0.008202
philadelphia,0.013660
philipp,0.014365
philosoph,0.007884
philosophi,0.068831
phonet,0.016880
phonolog,0.008575
photographi,0.007384
phrase,0.005411
phronesi,0.010673
phronet,0.011512
physic,0.020572
physiocrat,0.009123
pinpoint,0.008489
pioneer,0.009383
place,0.002540
plan,0.003401
planetolog,0.010673
play,0.006485
plural,0.005776
point,0.007816
polici,0.023266
polit,0.093025
politician,0.005388
popper,0.014721
popul,0.009586
posit,0.018633
positiv,0.043884
positivist,0.015271
possibl,0.002626
postcoloni,0.007743
postempiricist,0.011512
posthum,0.007337
postmodern,0.006944
potenti,0.003334
potter,0.008174
power,0.010542
practic,0.019289
practition,0.021621
pragmat,0.006277
precis,0.004242
predict,0.003768
predominantli,0.005717
prepar,0.004177
prescript,0.013573
present,0.007725
preserv,0.004341
press,0.039406
pretend,0.007972
preview,0.008121
price,0.003812
primari,0.010199
primarili,0.010561
primit,0.005699
princip,0.004023
principl,0.009275
problem,0.016527
process,0.031652
produc,0.005359
product,0.010737
profession,0.004173
profound,0.017625
program,0.003239
progress,0.007020
project,0.003186
prolegomenon,0.011281
prolifer,0.006340
promot,0.003674
properti,0.003118
propos,0.006355
protest,0.004786
provid,0.006855
psych,0.008045
psycholinguist,0.009783
psycholog,0.131704
psychopatholog,0.008033
public,0.028532
publish,0.008576
pure,0.004072
purpos,0.003350
pursuit,0.011613
qualit,0.029858
quantifi,0.005787
quantit,0.043065
quantiti,0.004195
quarter,0.005357
quest,0.006486
question,0.003234
questionnair,0.008216
quit,0.004352
r,0.017232
ralston,0.010472
rang,0.009275
rare,0.008477
rate,0.003191
ration,0.013315
reaction,0.004188
read,0.008510
real,0.003390
realism,0.012745
realiti,0.008886
realiz,0.009134
realm,0.005159
reason,0.002931
recent,0.005657
recogn,0.010507
record,0.003443
recoveri,0.005437
redfield,0.010472
refer,0.014700
refin,0.005124
reflect,0.007020
regard,0.006267
regardless,0.004937
regim,0.004759
region,0.008775
regress,0.006443
reject,0.004026
rel,0.005857
relat,0.035238
relationship,0.021825
reli,0.003935
reliabl,0.004884
religion,0.008320
remain,0.008104
remedi,0.006460
remot,0.005529
represent,0.008241
reproduc,0.005541
research,0.105996
resembl,0.005152
resolv,0.004744
resourc,0.009920
respons,0.002833
result,0.004550
retriev,0.004852
reveal,0.004296
review,0.003659
revolut,0.020232
revolutionari,0.005198
rhetor,0.012648
rhetorician,0.010188
richerson,0.010990
rigor,0.005500
rise,0.003286
rival,0.005396
robbin,0.008557
robert,0.007183
roger,0.010955
role,0.002861
root,0.004126
rousseau,0.007635
rout,0.019670
routledg,0.017462
royc,0.009950
rule,0.035779
rupert,0.009312
rural,0.005282
s,0.020850
sabia,0.011392
sage,0.006439
sanction,0.006132
sarah,0.007848
satisfi,0.014536
saussur,0.008704
saw,0.007994
scarc,0.012510
scarciti,0.006824
scholar,0.008296
scholarli,0.005781
scholarship,0.005840
school,0.029909
schumpet,0.008629
schutz,0.010188
scienc,0.508237
sciencebas,0.009401
scientif,0.039443
scientist,0.029881
scope,0.004802
search,0.004180
secondari,0.004651
secondarili,0.009148
secular,0.005653
seek,0.025657
seligman,0.009706
sell,0.004620
semant,0.006054
semin,0.006247
sens,0.016805
sentenc,0.005584
sentiment,0.006348
separ,0.005968
seri,0.003254
servic,0.003182
set,0.015181
sever,0.002369
shape,0.003893
share,0.015914
sharrock,0.011177
shionoya,0.011392
short,0.003482
sigmund,0.007580
signal,0.004699
significantli,0.004192
sill,0.009950
similar,0.002714
simmel,0.009597
simpl,0.003612
simpli,0.007418
sinc,0.013639
singl,0.003042
singleton,0.009256
sir,0.005131
situat,0.003500
situationspecif,0.011281
size,0.003540
skill,0.004379
small,0.008345
smelser,0.010352
smith,0.004816
soci,0.011512
social,0.540785
societi,0.065508
sociobiolog,0.008272
sociocultur,0.008033
sociolinguist,0.018928
sociolog,0.188521
sociologiqu,0.010904
sociologist,0.038710
sociu,0.010990
soil,0.005150
solut,0.003889
someon,0.005310
someth,0.013112
sometim,0.008878
sought,0.004428
soul,0.005869
sound,0.009572
sourc,0.020016
sovereign,0.005433
sowel,0.010352
space,0.010514
spatial,0.005314
speak,0.004359
special,0.014215
specialti,0.006456
specif,0.013599
specul,0.004729
speech,0.009795
spencer,0.007457
sphere,0.005087
split,0.004672
sprang,0.008301
st,0.018323
start,0.008632
state,0.033814
static,0.005815
statist,0.048484
statu,0.003796
statut,0.006678
stem,0.005109
storag,0.005409
stori,0.004608
strait,0.007134
stratif,0.008070
street,0.010374
stress,0.004661
stricter,0.008216
strong,0.003303
strongli,0.004381
structur,0.026560
struggl,0.004765
student,0.015477
studi,0.153756
sub,0.008020
subdisciplin,0.012392
subdisciplinari,0.010603
subfield,0.058520
subject,0.023135
subsist,0.006258
substitut,0.005051
success,0.003014
suffici,0.004202
suffix,0.007763
suicid,0.013035
suni,0.008866
superpow,0.007815
surfac,0.004116
survey,0.021848
symbol,0.012381
synchron,0.007466
syntax,0.007196
systemat,0.013029
tabl,0.004375
tackl,0.006999
taken,0.009985
talcott,0.009464
talent,0.006718
tangibl,0.007344
target,0.004232
taught,0.010136
teach,0.017813
techniqu,0.013973
technolog,0.009832
televis,0.005124
tell,0.005120
tenet,0.006693
teng,0.010411
term,0.027925
terminolog,0.005352
test,0.014865
text,0.007843
textbook,0.010248
th,0.036493
themselv,0.003372
theoret,0.018637
theori,0.093261
therapi,0.005790
thi,0.015303
thing,0.007234
think,0.011826
thinker,0.021982
thoma,0.012199
thought,0.009665
threat,0.004768
thu,0.010629
time,0.010056
tinbergen,0.008910
today,0.003265
togeth,0.006269
tool,0.007280
topic,0.007213
torrey,0.010137
tort,0.009123
total,0.003171
trace,0.013077
trade,0.003288
tradit,0.014313
trajectori,0.006781
transact,0.009789
transdisciplinari,0.008540
transport,0.003897
treat,0.003980
treatment,0.012545
trend,0.004342
tri,0.003578
trigg,0.010746
triumph,0.007075
turkish,0.006581
turn,0.003142
twain,0.009496
twentieth,0.005363
type,0.010932
ua,0.008783
uc,0.008095
uk,0.008333
ultim,0.003931
umbrella,0.006770
undergradu,0.006054
underlin,0.007972
understand,0.037029
understood,0.012798
undertaken,0.011715
unifi,0.014275
unit,0.016050
univ,0.007703
univers,0.042380
unlik,0.003785
upsurg,0.009401
urban,0.009536
usabl,0.013949
usag,0.004768
use,0.046437
usual,0.005471
v,0.004053
vacuum,0.005931
valid,0.004341
valu,0.011654
varieti,0.003413
variou,0.028467
vast,0.004564
veget,0.005426
veri,0.007788
verifi,0.005746
version,0.003976
verstehen,0.020822
view,0.011654
vilfredo,0.009174
virtual,0.008991
vol,0.004885
volum,0.011682
w,0.007628
wa,0.022367
walluli,0.011512
wang,0.007360
want,0.016788
war,0.003229
ward,0.006956
water,0.003506
wave,0.004584
way,0.007292
wealth,0.009362
weber,0.041704
weberian,0.011081
welldevelop,0.007562
western,0.003419
wherea,0.007889
whi,0.004182
wider,0.005176
william,0.003642
willing,0.006944
wisdom,0.006019
wit,0.005536
wolf,0.006962
womb,0.008704
word,0.022089
work,0.013371
world,0.006914
write,0.007108
writer,0.009331
written,0.003599
y,0.004841
year,0.002294
york,0.024508
zone,0.004698
