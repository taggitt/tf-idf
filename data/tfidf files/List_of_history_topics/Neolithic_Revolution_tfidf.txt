abandon,0.029449
abil,0.005422
abl,0.009957
abovement,0.012663
academ,0.005903
accompani,0.007619
accord,0.003941
account,0.009284
accrual,0.014864
accumul,0.007561
activ,0.003932
ad,0.005029
adapt,0.018828
addit,0.007974
adjac,0.009020
administr,0.005510
adopt,0.021756
advanc,0.009736
advantag,0.012235
affect,0.005150
affluent,0.012709
afford,0.016928
africa,0.096011
african,0.034541
afroeurasia,0.016633
age,0.019744
ago,0.076373
agrarian,0.010686
agrarianbas,0.018795
agricultur,0.328064
agriculturalist,0.029969
ahmar,0.018220
akl,0.019148
allow,0.015678
alpaca,0.016072
alreadi,0.005741
altitud,0.010667
altitudin,0.018489
alway,0.005300
amen,0.011099
america,0.016571
american,0.004465
anaemia,0.017760
analys,0.007705
analysi,0.004751
anatolia,0.035575
ancestor,0.008471
ancient,0.011421
andor,0.006897
andrew,0.016009
angora,0.020734
anim,0.187571
anthropocen,0.016174
anthropolog,0.008935
antilebanon,0.019148
appear,0.013695
approxim,0.010742
arabian,0.010957
arc,0.010593
archaeogenet,0.017561
archaeolog,0.025317
archaeologist,0.032589
architectur,0.007434
area,0.035231
areal,0.015316
argu,0.031459
arid,0.011494
arriv,0.019034
art,0.006029
artifici,0.006655
artisan,0.021746
asa,0.012826
asia,0.040604
asian,0.037543
assembl,0.006152
assist,0.011753
associ,0.019251
astronomi,0.008849
aswad,0.035521
atalhyk,0.016281
atlant,0.008352
attach,0.007462
attack,0.006354
attempt,0.004495
attent,0.006427
author,0.004437
avail,0.019271
averag,0.011774
axi,0.027117
bactrian,0.016764
badarian,0.019565
bailey,0.026162
bakkerheer,0.020734
balanc,0.006238
balkan,0.011180
balochistan,0.015464
balter,0.020734
banana,0.032231
bantu,0.026614
barley,0.060196
bartmen,0.020734
baryosef,0.019565
base,0.010302
basi,0.004909
basin,0.028096
bc,0.084375
bce,0.008721
bean,0.022087
becam,0.021720
becaus,0.007155
becom,0.007608
befor,0.012176
began,0.009491
begin,0.032438
behavior,0.005574
bekaa,0.018795
believ,0.009830
bellwood,0.019565
benefit,0.011534
besid,0.008131
better,0.016077
betting,0.020734
bibliographi,0.006911
big,0.007196
binford,0.017209
bioarchaeolog,0.019148
bitter,0.032260
blackwel,0.009935
bocquetappel,0.020076
bone,0.008913
book,0.013550
border,0.007000
bottl,0.011619
bovin,0.013558
boyd,0.013054
bp,0.119064
braidwood,0.020734
break,0.006551
breed,0.009530
brian,0.009720
bring,0.005908
bronz,0.009564
broomcorn,0.020076
brought,0.005858
built,0.011849
builtup,0.017378
bull,0.010967
c,0.009096
cach,0.013219
cal,0.014640
calibr,0.011441
calor,0.013398
came,0.010408
camel,0.049717
capac,0.006598
caribbean,0.009081
carl,0.015893
carpathian,0.015047
carri,0.015393
case,0.015919
catalhoyuk,0.020076
cattl,0.018646
cauvin,0.020076
cave,0.009570
center,0.005076
centr,0.013089
central,0.013385
centuri,0.004087
cereal,0.070414
certain,0.013069
chanc,0.007662
chang,0.017942
characterist,0.005486
chickpea,0.034757
child,0.053513
china,0.030577
circumst,0.007312
citi,0.010994
civil,0.010920
claim,0.004831
class,0.005279
climat,0.088356
close,0.017037
cm,0.040045
cocreat,0.015111
coffe,0.009935
cohen,0.010478
coin,0.006591
coincid,0.008562
colder,0.011938
colocasia,0.019565
colonist,0.010883
come,0.009105
commerc,0.007932
commun,0.024975
compar,0.004708
comparison,0.006675
compet,0.013126
complet,0.004396
complex,0.013975
complic,0.007214
compris,0.006512
conclud,0.006510
concord,0.013108
concurr,0.009815
condit,0.004454
confirm,0.006863
conquistador,0.013766
consequ,0.016240
consist,0.004260
consumpt,0.014850
contact,0.006680
contagion,0.014205
contin,0.016407
contract,0.006386
contrast,0.005377
corn,0.009955
corpor,0.006246
corridor,0.023977
cours,0.005548
cover,0.005241
cow,0.032935
crash,0.009468
crescent,0.083121
crisi,0.006569
crop,0.149548
cropbas,0.020734
csa,0.014694
csiro,0.014864
cultiv,0.089443
cultur,0.052655
current,0.004075
cursiv,0.016281
daniel,0.007918
danub,0.012732
data,0.010161
databl,0.016903
date,0.058529
davi,0.009231
david,0.005655
dawn,0.020151
death,0.005575
debat,0.005927
decis,0.010735
decisionmak,0.009176
decreas,0.006269
deep,0.007087
defici,0.010097
deforest,0.011706
deform,0.010393
degre,0.005213
delay,0.016262
demograph,0.025643
denot,0.007768
dens,0.009062
denser,0.025701
dental,0.024324
depend,0.004200
deperson,0.016392
depress,0.007910
describ,0.007837
desert,0.027159
desir,0.005991
determin,0.004421
detriment,0.010461
develop,0.073231
diamond,0.048328
did,0.023629
diet,0.018827
differ,0.023625
dioscorea,0.019148
direct,0.004227
discov,0.005606
discoveri,0.011925
diseas,0.066689
dispers,0.026212
display,0.007235
distinct,0.004903
distribut,0.015758
ditch,0.013525
divers,0.012246
diversifi,0.009641
divis,0.011035
document,0.006146
doe,0.004307
dog,0.028591
domest,0.356210
domestic,0.017378
domin,0.015716
donkey,0.013135
dormanc,0.016392
douglass,0.029073
downturn,0.010904
drainag,0.012310
dri,0.016677
drier,0.054233
drive,0.006840
driven,0.007411
dromedari,0.035958
drove,0.020624
drunk,0.014437
drya,0.017561
dure,0.032982
dwell,0.011264
earli,0.042169
earlier,0.011327
earliest,0.051252
easili,0.026266
east,0.082501
eastern,0.019085
eastwest,0.024899
eba,0.017760
ebb,0.014864
econom,0.012583
economi,0.010469
eddubb,0.020734
edibl,0.012369
edit,0.005999
editor,0.016318
effect,0.003723
effici,0.006001
effort,0.005308
egypt,0.008094
egyptian,0.008638
einkorn,0.034102
el,0.009216
elit,0.017022
emerg,0.009824
emigr,0.019271
emmer,0.049901
empir,0.010696
enabl,0.006078
encount,0.015098
encourag,0.006299
end,0.004190
energi,0.005365
engag,0.006069
enjoy,0.007297
enset,0.019148
entir,0.004971
environ,0.015242
epipaleolith,0.032145
epoch,0.010946
equip,0.006852
era,0.006055
esculenta,0.018795
especi,0.004450
establish,0.004090
ethiopian,0.036220
euphrat,0.013730
eurasia,0.022337
eurasian,0.023501
europ,0.031183
european,0.028643
event,0.024939
eventu,0.016343
everybodi,0.012091
everywher,0.009955
evid,0.062720
evolut,0.018669
evolutionari,0.007551
evolutionaryintention,0.020734
evolv,0.006291
exampl,0.006865
exclus,0.013095
exert,0.008794
exist,0.003811
expand,0.016265
expans,0.012934
expect,0.005266
experi,0.009426
explain,0.005159
explor,0.011680
extend,0.005055
extens,0.005175
extent,0.012613
extern,0.002997
extinct,0.026140
factor,0.014579
fail,0.005762
fals,0.007773
famou,0.006332
far,0.010394
farm,0.045883
farmer,0.032451
favor,0.006330
favour,0.015074
faynan,0.020734
feast,0.024112
fertil,0.080342
fertilecresc,0.020734
fifti,0.009714
fig,0.012554
figurin,0.014295
finger,0.011134
firearm,0.011858
flank,0.025799
flanneri,0.014486
flax,0.027756
fleisch,0.020076
flint,0.012389
flow,0.006035
follow,0.016393
food,0.123874
foodcrop,0.019148
foodproduc,0.017979
forc,0.008216
form,0.012531
founder,0.014367
foxtail,0.019565
fragment,0.008061
frank,0.008025
free,0.009363
fullfledg,0.013027
fulli,0.006179
futur,0.009983
g,0.005544
gain,0.004986
galile,0.014119
gari,0.010225
gather,0.042870
gave,0.005963
gbekli,0.016510
gene,0.007952
gener,0.005890
genet,0.006961
geograph,0.013331
geolog,0.007832
germ,0.022109
germin,0.013001
gilgal,0.039131
glacial,0.012091
goat,0.075480
goddess,0.011535
good,0.004556
goosefoot,0.020734
gordon,0.028677
got,0.009246
gourd,0.016174
government,0.008577
gradual,0.006588
grain,0.026715
granari,0.028777
grant,0.006283
grass,0.010328
great,0.004619
greater,0.005162
greatli,0.013423
grew,0.006849
grind,0.024858
grinin,0.032562
group,0.007394
grow,0.026277
guard,0.008566
guil,0.017561
guinea,0.048388
gun,0.019247
ha,0.035528
hagdud,0.041469
hand,0.005078
haplogroup,0.095852
hardcov,0.013337
harder,0.010186
harlan,0.014536
harvest,0.027257
haven,0.010377
hayden,0.015316
head,0.005291
health,0.005713
heavi,0.007083
heavier,0.010352
height,0.016343
help,0.008974
henri,0.006606
herald,0.011521
herd,0.021871
herder,0.013108
hi,0.007349
hide,0.019404
hierarch,0.018805
highland,0.055670
highlight,0.008439
hill,0.008153
hilli,0.029073
hillman,0.016510
histor,0.004604
histori,0.017068
hitherto,0.012091
hole,0.018656
holocen,0.025701
hors,0.017707
hour,0.007202
howev,0.009978
httpwwwhortpurdueedunewcrophistorylecturerhtml,0.020734
hull,0.011508
human,0.075025
hunt,0.016543
hunter,0.010385
huntergath,0.016764
huntergather,0.091530
hyk,0.016281
hypothesi,0.014278
ice,0.008940
idea,0.004600
ideal,0.006668
ident,0.005868
identifi,0.034681
ideolog,0.007742
ii,0.010727
immedi,0.006024
immun,0.017942
impact,0.005616
import,0.017478
inadequ,0.009733
inca,0.012619
incept,0.010667
includ,0.017070
incorpor,0.005628
increas,0.007882
increasingli,0.024168
independ,0.012628
indic,0.005041
indigen,0.008171
individu,0.012682
indonesia,0.009708
industri,0.014034
inequ,0.008623
infecti,0.010978
inferior,0.009976
influenc,0.004514
influenza,0.013001
initi,0.004453
inspir,0.006781
instanc,0.011101
intens,0.013614
interv,0.008635
introduc,0.009615
introduct,0.005433
invent,0.013337
investig,0.005743
involv,0.003973
iraq,0.009171
irrig,0.020721
isbn,0.029070
island,0.018224
italica,0.019565
j,0.015441
jack,0.009690
jacqu,0.009507
jare,0.065008
jeanpierr,0.012686
jeff,0.012180
jerf,0.020076
jericho,0.014984
johanna,0.015464
jordan,0.051640
journey,0.009524
jump,0.009462
just,0.004778
k,0.006213
kent,0.012291
khat,0.017979
kindl,0.014588
knowledg,0.015464
kola,0.015881
kuk,0.033529
labor,0.006728
labour,0.006937
land,0.015558
languag,0.005150
larg,0.028890
larger,0.027043
later,0.036676
law,0.004127
lean,0.011192
learn,0.005756
leather,0.011720
leav,0.005802
lebanon,0.022702
lectur,0.007450
led,0.030956
legion,0.012091
legum,0.013591
lentil,0.060986
leonid,0.012850
levant,0.023098
levantin,0.032349
level,0.004097
lewi,0.008600
life,0.017618
lifestyl,0.029274
light,0.005649
like,0.029279
limit,0.012553
lineag,0.010141
linear,0.014581
link,0.002979
littl,0.010488
live,0.008860
llama,0.059938
local,0.013922
locat,0.020151
london,0.005769
long,0.017795
longer,0.011302
lowland,0.011971
m,0.004900
madison,0.012216
main,0.004337
mainli,0.005688
maintain,0.004777
maiz,0.033829
make,0.020905
mammal,0.017347
man,0.012124
mani,0.021677
manifest,0.007882
manioc,0.015705
manur,0.014388
margin,0.015017
mark,0.005501
marker,0.010539
massey,0.014341
massiv,0.007290
mate,0.010272
mathemat,0.005386
matur,0.007972
mean,0.011319
measl,0.013766
meat,0.009507
mechian,0.020734
mediterranean,0.017924
megafauna,0.014749
mehrgarh,0.016072
melanesia,0.014205
men,0.006353
mesoamerica,0.013558
mesolith,0.013460
mesopotamia,0.010418
metal,0.007131
mexican,0.010914
michael,0.006305
middl,0.050841
migratori,0.013525
miliaceum,0.020076
militari,0.005542
milk,0.040478
millennia,0.019966
millennium,0.034206
millet,0.052765
mobil,0.007277
model,0.004267
modern,0.008219
modifi,0.006685
momentum,0.009007
monopol,0.011922
moor,0.009246
morerapidli,0.020734
mother,0.007896
mountain,0.007371
mtdna,0.030094
mutual,0.007227
naquitz,0.019565
narrow,0.015473
nathan,0.012235
nativ,0.007081
natufian,0.016510
natur,0.014630
near,0.028166
necessari,0.005460
need,0.012356
neighbor,0.007728
neighbour,0.008369
neolith,0.347137
netiv,0.041469
new,0.030604
nile,0.011088
nobil,0.010686
nomad,0.042338
nonnomad,0.019565
nonport,0.020076
noog,0.019565
normal,0.005461
north,0.010880
northern,0.012810
northsouth,0.012686
northward,0.011647
notabl,0.005257
note,0.015800
number,0.010282
nut,0.011827
nutrit,0.029525
oas,0.014536
oasi,0.014205
oat,0.013277
observ,0.004521
occur,0.013059
octdec,0.018795
octob,0.005924
ofer,0.018489
offer,0.005094
ohalo,0.038296
oil,0.006838
old,0.005669
older,0.007189
oldest,0.007411
onag,0.018220
onc,0.020478
onli,0.003153
onset,0.010539
open,0.004690
optim,0.007267
orang,0.010240
organ,0.003737
organis,0.012973
origin,0.045563
ostentati,0.017209
otherwis,0.013034
outcom,0.007004
outsid,0.010453
overgraz,0.015111
overpopul,0.037727
ownership,0.024138
oxen,0.014536
pacif,0.007941
page,0.006716
paid,0.006985
paleolith,0.022676
paleopathologist,0.019565
palestin,0.010802
palm,0.010676
panicum,0.018795
paperback,0.011535
papua,0.023977
particular,0.004288
particularli,0.004804
pastor,0.010822
patholog,0.009739
pattern,0.005845
pea,0.023501
peach,0.015178
pearl,0.010575
peninsula,0.018228
peopl,0.018887
pepo,0.020076
perfect,0.007654
perhap,0.025476
period,0.032567
perman,0.019918
permit,0.034891
person,0.008966
peter,0.012495
phase,0.013177
physic,0.004483
pig,0.030652
pioneer,0.014314
place,0.015498
plan,0.005188
plant,0.142761
plantain,0.014984
pleistocen,0.012511
plough,0.013803
point,0.003974
polit,0.004300
politi,0.010593
pollen,0.011827
popul,0.087745
popular,0.009803
posit,0.008120
possess,0.006156
possibl,0.028050
postul,0.016906
potato,0.010435
potenti,0.005086
potteri,0.010773
power,0.004020
powerhous,0.014119
pp,0.006405
practic,0.008406
preced,0.007443
predomest,0.019148
prefer,0.006016
prehistor,0.018992
prehistori,0.029604
preneolith,0.019148
prepar,0.006371
prepotteri,0.017760
presenc,0.005941
present,0.007856
press,0.009247
previous,0.005989
privat,0.005633
probabl,0.005637
problem,0.004201
process,0.022284
produc,0.004087
product,0.036852
profession,0.006366
profit,0.013576
progress,0.010709
promin,0.006160
propag,0.017507
properti,0.014270
propos,0.014541
protect,0.005236
protein,0.007981
provid,0.020914
proxim,0.009081
publish,0.013082
pumpelli,0.020734
pure,0.006212
qaraoun,0.020076
quantiti,0.019197
question,0.004933
quickli,0.019184
quit,0.006639
r,0.005257
radic,0.007202
rain,0.019952
rainfal,0.010812
rais,0.005794
ramad,0.020734
rang,0.014148
raphael,0.013429
rapidli,0.013247
reach,0.010127
reason,0.008943
reassess,0.012532
recent,0.017258
recognis,0.007692
record,0.005252
recov,0.007670
redomest,0.020734
refer,0.007474
refin,0.007816
reflect,0.005354
region,0.053543
regionspecif,0.017561
rel,0.013402
relat,0.003359
relationship,0.009512
reli,0.006003
reliabl,0.007450
religion,0.006346
remain,0.004120
renew,0.007286
replac,0.005077
represent,0.006285
requir,0.019612
research,0.008084
resist,0.006456
resourc,0.005044
respect,0.009477
respons,0.004321
rest,0.011675
result,0.010412
retain,0.006552
return,0.010085
revolut,0.148142
rice,0.028192
richerson,0.016764
rindo,0.020734
rise,0.005014
risk,0.006130
river,0.027249
robert,0.016435
ronald,0.008900
root,0.006294
routledg,0.017758
runoff,0.011363
rye,0.013492
safer,0.012180
sahara,0.011562
sahel,0.027183
sanitari,0.013878
sauer,0.029969
script,0.009822
sea,0.006357
season,0.016104
sebilian,0.019565
second,0.012268
secondari,0.014190
secur,0.005293
sedentari,0.112606
seed,0.114931
seen,0.009777
select,0.027090
semi,0.014341
separ,0.009104
sequenc,0.006917
seri,0.009927
serv,0.004959
set,0.011579
setaria,0.020734
settl,0.028326
settlement,0.021814
settler,0.009375
sever,0.014460
sex,0.008627
share,0.009710
shed,0.010621
sheep,0.050198
shepherd,0.025557
sherratt,0.037590
shift,0.012076
ship,0.007069
short,0.021250
shorter,0.008870
shown,0.006008
sick,0.009783
signific,0.008853
significantli,0.006394
similar,0.008281
similarli,0.006569
simpli,0.005658
singl,0.004640
site,0.093458
sixth,0.009206
size,0.010800
skin,0.008794
small,0.016973
smallpox,0.012291
social,0.039702
societi,0.052136
soil,0.015714
soldier,0.007890
solomon,0.021297
sometim,0.009028
somewher,0.010621
soon,0.006358
sorghum,0.013730
sourc,0.026171
south,0.021722
southeast,0.033956
southern,0.025743
southwest,0.019466
southwestern,0.010822
soybean,0.013027
sp,0.010630
space,0.005346
span,0.008010
spanish,0.007499
speci,0.044643
special,0.026022
speciesdepend,0.020734
spinal,0.012091
spot,0.008681
spread,0.055166
spring,0.007821
springer,0.008393
squash,0.040876
st,0.011180
stabl,0.006824
standard,0.004670
start,0.021947
state,0.003034
steel,0.026057
stockpil,0.013916
stone,0.015489
storag,0.024756
store,0.028102
strain,0.008619
strata,0.012039
structur,0.004051
stuart,0.009617
subject,0.004411
subsequ,0.016234
subsist,0.009547
success,0.027592
sugarcan,0.011811
suggest,0.053401
sumerian,0.011955
summaris,0.012369
sumpwe,0.020734
sunflow,0.014077
suppli,0.005653
support,0.020615
suppos,0.007619
surplu,0.036494
surplus,0.036875
survey,0.006665
surviv,0.023923
swamp,0.012663
switch,0.008514
sword,0.011363
syria,0.010194
taken,0.015232
taro,0.069391
tasian,0.020734
tast,0.009733
tauru,0.016633
taxon,0.013730
techniqu,0.010658
technolog,0.019999
teff,0.017979
tell,0.015621
temper,0.020922
tempera,0.012429
tend,0.005694
tepe,0.015792
term,0.009830
territori,0.006069
theori,0.032517
therefor,0.014140
thi,0.067437
thought,0.014743
thousand,0.018346
time,0.015340
timor,0.013163
tisza,0.017051
today,0.004981
togeth,0.004781
tomb,0.011043
took,0.010834
tool,0.011105
tow,0.013995
town,0.007121
traction,0.013191
trade,0.030098
tradit,0.004366
trait,0.008216
transform,0.005676
transit,0.042260
transnew,0.020734
tri,0.010916
tropic,0.017698
turn,0.009588
twentieth,0.008181
tworow,0.020734
type,0.004169
typic,0.004652
ultim,0.011995
uncertain,0.009167
undertaken,0.008935
undesir,0.010842
undomest,0.018795
unequivoc,0.012826
unexplor,0.013766
unit,0.003497
univers,0.007183
unlik,0.005774
unwant,0.011549
urban,0.007273
use,0.017000
usual,0.004173
v,0.012366
valley,0.040097
valuabl,0.008140
van,0.007585
vari,0.015336
varieti,0.020827
variou,0.015790
various,0.010724
veri,0.003960
verifi,0.008766
vetch,0.034102
vicin,0.011192
view,0.017778
villag,0.023772
virtu,0.008786
vitamin,0.011389
vol,0.014905
wa,0.099515
wadi,0.030493
way,0.014831
weapon,0.007700
weed,0.012532
week,0.007288
went,0.006360
west,0.029771
western,0.010432
wetter,0.014295
whatev,0.008536
wheat,0.059168
wheel,0.010444
wherea,0.012034
whi,0.006380
wi,0.014388
wide,0.004466
widescal,0.015111
widespread,0.006662
wild,0.053378
willem,0.012802
wipe,0.022456
women,0.006599
wool,0.022702
work,0.010198
worker,0.012910
world,0.024610
worldwid,0.007035
wright,0.020576
write,0.005421
yak,0.016510
yale,0.009479
yam,0.039921
ychromosom,0.015316
ydna,0.034757
year,0.073493
yellow,0.009739
yesterday,0.013660
yield,0.006774
york,0.005340
younger,0.017231
youtub,0.010957
zagro,0.016764
zebu,0.017760
zeist,0.019565
zohari,0.019565
