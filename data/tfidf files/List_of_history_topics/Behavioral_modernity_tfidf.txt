abil,0.007316
abov,0.006807
abruptli,0.016796
absent,0.012959
abstract,0.046784
academ,0.007964
acceler,0.010227
accept,0.020027
accord,0.021268
accumul,0.020401
acheulean,0.021426
acquisit,0.011497
adapt,0.025402
addit,0.005379
advanc,0.013135
affect,0.006948
africa,0.189979
african,0.074560
afterward,0.011712
age,0.019977
ago,0.206073
agre,0.016034
alison,0.018429
allow,0.005287
alreadi,0.030986
altern,0.006832
ambigu,0.012494
analog,0.009501
analysi,0.006409
anatom,0.056735
ancestor,0.011428
ancient,0.007704
ani,0.004686
anim,0.014885
apish,0.027973
appar,0.027232
appear,0.049272
approach,0.006119
aquat,0.028658
archaeolog,0.159394
archaeologist,0.058623
archaic,0.030492
archipelago,0.013497
argu,0.049516
arid,0.015507
aris,0.008674
arisen,0.014520
aros,0.031679
arriv,0.008559
art,0.056944
artifact,0.088195
artist,0.011159
asia,0.054780
assembl,0.008301
assemblag,0.048866
associ,0.005194
attribut,0.016532
august,0.008080
aurignacian,0.022115
author,0.041909
base,0.004633
bead,0.066128
bear,0.009950
becam,0.005860
becom,0.020530
befor,0.054759
behavior,0.398621
behaviour,0.039541
beij,0.015435
benchmark,0.014587
better,0.014460
bia,0.024452
bicamer,0.014442
biggam,0.024255
blade,0.048387
blombo,0.118465
bodi,0.012867
bone,0.036077
breed,0.012857
bronz,0.025808
brook,0.014113
burial,0.072870
camp,0.011618
capac,0.008901
captur,0.008817
caus,0.005731
caution,0.015115
cave,0.129121
chang,0.053255
character,0.008144
china,0.024751
chri,0.014455
circumstanti,0.020387
cite,0.008981
claim,0.006518
classifi,0.008686
clearli,0.009655
climat,0.009169
close,0.005746
clue,0.015977
cognit,0.152722
coincid,0.011552
common,0.005193
compar,0.019056
complex,0.075416
composit,0.008903
concept,0.017981
condit,0.012019
confirm,0.009259
conflat,0.016457
consensu,0.010713
consequ,0.007303
consequenti,0.016966
consist,0.005748
contemporari,0.008401
context,0.007555
continu,0.005204
contrast,0.007255
control,0.005706
controversi,0.008463
cooper,0.024627
copi,0.020709
cosmet,0.015792
cousin,0.014125
crayon,0.021552
critiqu,0.021824
cultur,0.148535
cumul,0.040897
current,0.010997
curti,0.017024
darwinian,0.016383
data,0.013709
date,0.028714
deadend,0.023217
debat,0.039981
decor,0.041834
decreas,0.008458
deeper,0.012834
defin,0.011788
definit,0.006791
demograph,0.034596
dendroglyph,0.027973
densiti,0.020729
deposit,0.010168
depth,0.011277
deriv,0.013061
derrico,0.054170
descent,0.012321
describ,0.037006
design,0.006308
despit,0.021849
develop,0.008591
differ,0.009106
diffus,0.023468
discov,0.022691
discoveri,0.008044
discuss,0.007008
dispers,0.011788
disput,0.008639
distanc,0.018292
distinct,0.006615
distinctli,0.015694
distinguish,0.007625
divers,0.041304
document,0.008292
doe,0.011623
dramat,0.009699
du,0.012139
dy,0.019048
earli,0.036204
earlier,0.015282
east,0.031801
eastern,0.017165
ecolog,0.009744
effect,0.015070
elabor,0.010760
emerg,0.019882
emphasi,0.009379
empir,0.014430
enabl,0.008200
engrav,0.016634
environ,0.006854
erectu,0.018247
especi,0.006004
establish,0.005518
ethnograph,0.016557
eurocentr,0.019681
europ,0.063105
european,0.012881
event,0.006729
evid,0.162182
evolut,0.016791
evolutionari,0.040751
evolv,0.025463
exampl,0.032418
excav,0.013901
exist,0.005142
exodu,0.016151
expand,0.007314
expans,0.034899
experiment,0.008637
explain,0.006961
exploit,0.019252
explos,0.011462
export,0.009486
express,0.013399
extens,0.006982
extern,0.004043
extinct,0.011755
extrem,0.014835
facilit,0.009322
fact,0.006565
fall,0.007058
famou,0.008542
featur,0.006993
figur,0.007658
figurin,0.019286
fish,0.057886
focu,0.022758
focus,0.007198
follow,0.008846
forc,0.005542
form,0.016906
forth,0.022481
foundat,0.014420
founder,0.009691
fox,0.014243
foxp,0.022441
fragmentari,0.037446
francesco,0.016242
fulli,0.016672
game,0.009488
gener,0.023839
genet,0.056351
geograph,0.008992
geometr,0.011371
given,0.005545
glacial,0.016312
god,0.009677
gradual,0.044443
gradualist,0.021305
grave,0.013478
greater,0.006964
groov,0.019477
grott,0.023961
group,0.029926
grow,0.007090
h,0.007681
ha,0.040557
hearth,0.057859
heavi,0.009556
help,0.006053
henshilwood,0.027085
high,0.011197
highli,0.007279
histor,0.006211
histori,0.009211
hominin,0.079008
homo,0.043483
honshu,0.023693
howev,0.004487
howieson,0.026396
hudson,0.015812
human,0.308983
hunt,0.022319
hypothes,0.021990
ian,0.013652
idea,0.012411
identifi,0.006684
imageri,0.014869
imit,0.013081
impetu,0.014267
import,0.004715
incisor,0.021552
includ,0.011515
incorpor,0.007593
increas,0.015951
inde,0.009401
independ,0.005679
india,0.008465
indic,0.006802
indigen,0.011024
industri,0.006311
influenc,0.006090
initi,0.006007
inland,0.013268
innov,0.019155
instead,0.006476
intent,0.009305
inter,0.015544
interact,0.006899
interpret,0.007376
invent,0.008997
investig,0.007748
isbn,0.006536
isol,0.008977
issu,0.005933
item,0.010142
japan,0.009143
japanes,0.010537
jewelri,0.017337
joo,0.017370
just,0.012894
kanedori,0.027973
key,0.014072
kin,0.016608
kind,0.007365
klein,0.014642
labor,0.009077
lack,0.007049
languag,0.020845
larg,0.009744
late,0.025601
later,0.021991
lead,0.005515
leap,0.014711
learn,0.031063
levalloi,0.024945
lie,0.009057
life,0.005942
light,0.015242
like,0.024688
line,0.006616
link,0.004019
list,0.015420
literari,0.011592
live,0.011954
local,0.006260
long,0.012004
look,0.007632
low,0.007084
maintain,0.006445
mani,0.008356
marean,0.027973
marin,0.021163
materi,0.006477
maximum,0.010051
mean,0.005090
methodolog,0.009784
metric,0.024827
microlith,0.021965
middl,0.015242
migrat,0.069136
mind,0.017433
miss,0.011660
mithen,0.025357
mobil,0.009818
mode,0.010406
model,0.069093
modern,0.288309
modif,0.010986
morocco,0.014628
multipl,0.014219
multiregion,0.025357
multispeci,0.023961
museum,0.011146
music,0.010158
mutat,0.025669
nation,0.005121
natur,0.004934
neanderth,0.210489
near,0.007600
necessari,0.007366
necklac,0.020477
neolith,0.013774
neurolog,0.013988
neutral,0.010117
nonmainstream,0.022617
norm,0.010721
note,0.021317
number,0.013872
numer,0.006848
nytim,0.025833
object,0.006318
occur,0.017618
ochr,0.098058
offer,0.013746
old,0.030592
oldest,0.009999
onli,0.021274
opportun,0.017462
oppos,0.007724
optim,0.009804
option,0.009645
order,0.010187
ordinari,0.010079
organis,0.008751
origin,0.020490
ornament,0.108300
outlin,0.008997
outsid,0.014102
packag,0.023436
paint,0.011739
paleolith,0.107078
particular,0.017357
particularli,0.006482
pattern,0.007886
peopl,0.010192
perhap,0.017185
person,0.030242
perspect,0.008271
petroglyph,0.021076
philosoph,0.008113
pierc,0.031829
pigment,0.061957
pinnacl,0.036765
place,0.015682
plan,0.014000
point,0.042894
poor,0.009139
poort,0.027973
popul,0.046036
portray,0.012031
possess,0.016612
possibl,0.010812
preciou,0.013912
predict,0.023266
prehistori,0.013313
present,0.010599
presentday,0.011904
pressur,0.007978
primari,0.006996
primarili,0.007245
primat,0.015036
prior,0.008005
problem,0.011337
process,0.015032
produc,0.016544
profound,0.012091
progress,0.007224
prompt,0.011603
protoaurignacian,0.027085
prove,0.008180
punctur,0.020053
pure,0.016762
purpos,0.006894
push,0.010079
python,0.016687
question,0.013312
rang,0.006362
rapid,0.018371
ratchet,0.020133
rate,0.006568
receiv,0.006422
recent,0.011642
recogn,0.007208
record,0.042514
rectangular,0.015914
red,0.009317
reduc,0.006368
refer,0.010084
reflect,0.007224
regardless,0.010162
region,0.018059
rel,0.012054
releg,0.017084
relianc,0.053912
religion,0.008561
remain,0.022238
renn,0.021426
replac,0.020550
research,0.027267
resourc,0.034027
respons,0.017491
rest,0.007875
result,0.004682
reveal,0.008842
revolut,0.083275
revolutionari,0.021396
risk,0.008271
ritual,0.025072
run,0.007387
s,0.009535
sapien,0.048060
scholar,0.008536
school,0.006839
scienc,0.005653
scientist,0.007686
self,0.011552
selfless,0.020762
selfornament,0.027973
selfperceiv,0.026396
separ,0.012282
serial,0.015229
seriou,0.009417
sever,0.009754
shape,0.008012
shea,0.065052
shed,0.014329
shell,0.045257
shellfish,0.057319
shift,0.008146
shown,0.024319
sign,0.015405
signatur,0.027385
signifi,0.013564
signific,0.005972
significantli,0.025882
similar,0.011173
simpl,0.007433
sinc,0.014034
singl,0.012521
site,0.086684
size,0.014571
skeleton,0.014304
slab,0.018828
slow,0.009658
small,0.005724
smithsonian,0.015998
social,0.041660
south,0.051284
space,0.007212
span,0.010807
speci,0.034416
special,0.005851
speciesspecif,0.022804
specif,0.011195
specul,0.009733
spontan,0.012091
standard,0.006301
start,0.005922
step,0.015866
steven,0.011788
stone,0.031345
strategi,0.026144
stress,0.009593
strike,0.010406
subsist,0.038642
subsistencebas,0.024945
sudan,0.014404
sudden,0.026789
suddenli,0.013879
suggest,0.045846
suit,0.010875
support,0.011124
surround,0.008567
switch,0.011487
symbol,0.076441
systemat,0.017876
taken,0.020550
taphonom,0.051666
techniqu,0.007189
technolog,0.067452
teeth,0.014725
term,0.004420
test,0.007648
text,0.008070
thame,0.018117
theori,0.032902
thi,0.062987
think,0.008113
thought,0.013260
thousand,0.016500
tie,0.009125
time,0.028975
timelin,0.011012
timewindow,0.027973
togeth,0.006451
tool,0.112368
trade,0.020303
tradit,0.023564
trait,0.188444
transit,0.032579
transport,0.008020
trinket,0.022804
type,0.011249
underli,0.008884
uniqu,0.007977
univers,0.019381
unlik,0.007791
unquestion,0.019412
upper,0.066132
use,0.045872
utilis,0.015382
variabl,0.008552
varieti,0.035122
veri,0.005342
view,0.029981
voic,0.011150
wa,0.023015
watercraft,0.044882
watt,0.014944
whi,0.008608
wide,0.012051
widen,0.014560
wolf,0.014329
word,0.006494
work,0.009172
world,0.009486
worth,0.010592
year,0.122759
yield,0.027417
zhoukoudian,0.049890
zilho,0.026396
