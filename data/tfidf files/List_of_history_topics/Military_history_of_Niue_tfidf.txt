acp,0.005840
adjectiv,0.006060
administr,0.002746
african,0.003442
agreement,0.002997
agricultur,0.008757
air,0.003017
airport,0.004326
alofi,0.017700
anthem,0.005707
architectur,0.003704
area,0.003901
arm,0.003131
armi,0.003378
art,0.006009
associ,0.003837
atla,0.009209
bank,0.005680
behalf,0.004368
bicamer,0.005334
biospher,0.004937
bird,0.004042
boundari,0.003306
branch,0.010073
busi,0.002669
cabinet,0.003984
capit,0.007738
care,0.002979
caribbean,0.004525
cave,0.004769
chemic,0.003020
christian,0.003073
cia,0.004712
cinema,0.005325
citi,0.002739
climat,0.003386
coastlin,0.004933
coat,0.004698
code,0.009207
command,0.003328
commanderinchief,0.005508
commiss,0.003158
commission,0.004878
common,0.005754
commonli,0.002634
commun,0.006222
compani,0.002504
conduct,0.002718
constitut,0.002504
cook,0.004472
cooper,0.003032
countri,0.012526
court,0.003008
crime,0.003935
cuisin,0.005392
cultur,0.004770
currenc,0.003476
daytoday,0.005413
defenc,0.003861
demographi,0.005334
demonym,0.006213
develop,0.001586
diplomat,0.011306
divis,0.002749
dmoz,0.004201
dollar,0.003706
domain,0.003098
econom,0.004180
economi,0.002608
ecoregion,0.005656
educ,0.004866
elect,0.002817
endonym,0.018831
energi,0.008021
enforc,0.003580
english,0.007980
environ,0.002531
etymolog,0.004065
exchang,0.002699
execut,0.002893
extern,0.001493
extrem,0.002739
factbook,0.004786
fao,0.005195
fauna,0.004872
featur,0.002582
festiv,0.004743
film,0.003873
fjord,0.006440
flag,0.004560
follow,0.001633
food,0.005878
footbal,0.005110
forc,0.006141
foreign,0.002658
form,0.001561
forum,0.004194
free,0.004665
freedom,0.003224
ft,0.004463
fund,0.002815
gdp,0.003841
gener,0.001467
geograph,0.003321
geographi,0.007518
geolog,0.003903
glacier,0.005339
govern,0.017479
group,0.001842
guid,0.002887
head,0.007909
health,0.005694
hemispher,0.009302
heritag,0.008354
high,0.002068
hinduism,0.004910
histori,0.005103
holiday,0.004807
hous,0.008219
human,0.001967
hundr,0.003103
ifad,0.005571
industri,0.002331
infrastructur,0.006915
intern,0.007158
internet,0.006700
islam,0.003399
island,0.018162
islandnu,0.010004
iso,0.019064
judaism,0.004884
judici,0.003920
kilometr,0.004734
km,0.007687
known,0.001706
lake,0.004027
land,0.002584
languag,0.012832
law,0.004113
legisl,0.003126
lgbt,0.005768
link,0.001484
list,0.007594
literatur,0.003050
local,0.002312
locat,0.010041
low,0.002616
lower,0.002522
m,0.004883
mammal,0.004322
mean,0.001880
media,0.003131
member,0.002178
membership,0.003760
mere,0.003358
meteorolog,0.005006
militari,0.011047
minist,0.003080
ministri,0.003670
mission,0.006808
mountain,0.003673
municip,0.004160
music,0.003752
mutalau,0.010004
nation,0.013242
nativ,0.007057
natur,0.001822
navi,0.004086
near,0.002807
new,0.012200
ninetieth,0.009541
niu,0.008353
niue,0.993125
niuean,0.035401
nomin,0.003563
northeast,0.004862
nu,0.018290
nzd,0.008751
ocean,0.017566
oceania,0.010434
offici,0.010367
oil,0.003407
olymp,0.005154
opcw,0.005559
order,0.001881
organ,0.011173
outlin,0.013292
overview,0.003420
pacif,0.035613
park,0.003990
parliament,0.003443
parti,0.002638
peopl,0.005646
pif,0.007597
point,0.001980
polici,0.002526
polit,0.006428
polynesia,0.012327
polynesian,0.006144
popul,0.004858
portal,0.004923
postal,0.005256
predominantli,0.004345
presentday,0.004397
presid,0.002831
prime,0.003145
prohibit,0.003994
pronunci,0.005191
protect,0.002609
provid,0.001736
public,0.001971
rank,0.009916
record,0.002617
refer,0.002483
region,0.006670
relat,0.005022
religion,0.006324
renew,0.003630
reserv,0.003302
resid,0.003289
right,0.006684
river,0.003394
road,0.003606
rock,0.007776
rugbi,0.006756
samoa,0.006106
sanit,0.005594
school,0.002526
scientif,0.002498
secretariat,0.005811
selfgovern,0.004923
senat,0.004259
settlement,0.003623
sikhism,0.005748
site,0.008731
south,0.013530
southern,0.003207
sovereign,0.004130
sparteca,0.008059
spc,0.006859
special,0.002161
sport,0.004103
state,0.006047
stock,0.003333
suppli,0.002817
suprem,0.003538
symbol,0.003137
taught,0.003852
televis,0.003894
territori,0.003024
th,0.003962
theatr,0.004881
time,0.001528
tonga,0.006367
topic,0.002741
toplevel,0.005612
tourism,0.008039
trade,0.002499
transport,0.005924
triangl,0.005110
unesco,0.004693
union,0.005135
unit,0.001742
univers,0.001789
unnam,0.006428
upper,0.003489
upu,0.005265
use,0.001411
utc,0.005885
valley,0.003996
volcano,0.005110
water,0.002665
waterfal,0.005833
weapon,0.003837
western,0.002599
wikimedia,0.004816
wildlif,0.004923
wmo,0.005438
world,0.008759
zealand,0.028761
zealandniu,0.010004
zone,0.003571
