abil,0.007000
abl,0.006427
academi,0.009802
accompani,0.009837
accord,0.005088
accumul,0.009761
acquir,0.008287
acquisit,0.022003
ad,0.025973
addit,0.005147
administr,0.007114
advanc,0.012568
aenea,0.021162
affect,0.006649
africa,0.016526
age,0.070094
al,0.008727
albeit,0.012861
alexand,0.038995
allan,0.014024
alli,0.009398
allow,0.005059
alphabet,0.012252
altar,0.016957
alway,0.006842
american,0.011530
ancestor,0.010936
ancient,0.110582
andokid,0.026767
anoth,0.004893
antiqu,0.126352
apex,0.015843
appar,0.008686
appear,0.005893
april,0.007784
aquina,0.014314
arab,0.018796
arc,0.013676
archaeolog,0.010894
archaic,0.058357
architectur,0.067183
area,0.005053
argo,0.019594
aristocrat,0.014037
aristotelian,0.013200
aristotl,0.031467
armi,0.008752
art,0.015568
arthur,0.010379
asia,0.043682
associ,0.004970
assum,0.007048
astronomi,0.011424
athen,0.042191
athenian,0.033636
attempt,0.005803
author,0.017187
avail,0.006219
background,0.008738
balkan,0.072168
ballet,0.018900
basi,0.006337
battl,0.019218
bc,0.363086
becam,0.022432
befor,0.005239
began,0.012252
begin,0.011964
belong,0.008669
bereft,0.021473
besid,0.010497
bilingu,0.017141
birth,0.009040
blackfigur,0.023521
blend,0.012861
book,0.005831
brand,0.012400
briefli,0.011380
britain,0.008643
britannia,0.019041
bronz,0.012348
brought,0.015125
brutu,0.042036
build,0.006492
byzantin,0.075733
byzantinesasanian,0.023210
c,0.005871
caesar,0.015208
callia,0.023869
came,0.013436
canaan,0.019189
capit,0.020049
carri,0.006624
carthag,0.016751
carthaginian,0.035932
cathol,0.009921
catilin,0.024719
caucasu,0.014999
ceas,0.010379
center,0.013107
central,0.017280
centuri,0.184681
challeng,0.007340
champion,0.012756
chang,0.013898
changeov,0.019964
charact,0.016982
charlemagn,0.016436
christendom,0.016685
christian,0.047781
church,0.018569
circul,0.010751
cisalpin,0.023521
citi,0.042579
citizen,0.016818
cityst,0.014418
civil,0.021146
civilis,0.014024
claim,0.006237
classic,0.217259
cleomen,0.024719
cleric,0.014874
close,0.010997
collaps,0.018105
collect,0.005994
coloni,0.008497
column,0.011675
commonwealth,0.010615
competit,0.008081
complet,0.005676
complex,0.006013
compris,0.008407
concept,0.005735
conclud,0.008405
conduct,0.014087
conflict,0.022963
conqueror,0.015112
conquest,0.066442
consid,0.004793
conspiraci,0.014605
constantin,0.014874
constantinopl,0.156362
construct,0.006364
continu,0.049798
control,0.010921
convent,0.015916
corinth,0.038531
corinthian,0.018455
coron,0.015455
correspond,0.007460
cours,0.007163
court,0.015590
cover,0.006766
creat,0.015081
creation,0.007481
crete,0.016319
crisi,0.008481
crusad,0.013946
cth,0.043643
cuisin,0.013971
cult,0.013428
cultur,0.105054
cypru,0.027399
dacia,0.040129
danc,0.013180
danub,0.016436
dark,0.031884
date,0.034345
david,0.007300
day,0.006202
dc,0.011380
death,0.014396
declin,0.031213
decre,0.023606
deep,0.009149
definit,0.006498
delian,0.020749
democraci,0.018309
depos,0.013516
deriv,0.024996
descend,0.019895
describ,0.010117
desir,0.007735
despit,0.013938
destroy,0.008594
determin,0.005707
develop,0.012331
diadochi,0.023869
dictatorship,0.013281
did,0.024404
dirti,0.015819
disappear,0.031348
dispar,0.012630
disput,0.008266
dissolut,0.012385
divid,0.006517
divinelyappoint,0.050517
domin,0.027053
duncan,0.015288
dure,0.037848
e,0.013320
eagl,0.015654
earli,0.019796
earlier,0.007311
earliest,0.016541
earliestrecord,0.025258
earth,0.007768
east,0.022822
eastern,0.073916
easternwestern,0.026767
eclips,0.013261
econom,0.010830
edgar,0.014464
edshierarchi,0.026767
educ,0.031518
effort,0.006852
egypt,0.052248
elit,0.010987
emerg,0.012683
emperor,0.130955
empir,0.138088
end,0.059510
endless,0.015150
english,0.006891
enrag,0.018701
enter,0.014467
entir,0.019254
entireti,0.014145
epaminonda,0.025258
epic,0.039906
epicurean,0.016818
era,0.015635
especi,0.005745
establish,0.026403
et,0.008505
etruria,0.022671
etruscan,0.073131
europ,0.040257
european,0.018489
eventu,0.014065
evid,0.006747
exemplifi,0.012605
exercis,0.008501
exist,0.014761
expand,0.013999
expel,0.011930
express,0.006411
expuls,0.014510
extend,0.006526
extens,0.006681
extraordinari,0.012915
facad,0.019041
fact,0.012565
facto,0.011995
fail,0.007439
faith,0.009891
fall,0.020262
far,0.013419
father,0.017398
festiv,0.012288
final,0.038400
firmli,0.012933
flee,0.012688
flight,0.010840
flourish,0.010890
follow,0.021162
forc,0.005303
form,0.020221
formerli,0.009911
forum,0.010867
foundat,0.013799
fragment,0.020813
franc,0.015149
franca,0.017590
french,0.007120
fring,0.014159
galen,0.015169
game,0.009079
gaul,0.068717
gave,0.015398
gener,0.003802
genr,0.012493
geographi,0.009739
geometr,0.010880
german,0.007472
germania,0.019867
glori,0.014891
gnostic,0.016466
god,0.009260
got,0.011936
govern,0.020126
gradual,0.025516
graduat,0.009914
grandeur,0.020275
great,0.029816
greater,0.006664
grecopersian,0.021162
grecoroman,0.030300
greec,0.150021
greek,0.157345
greekspeak,0.020880
grinin,0.042036
ha,0.003528
half,0.021849
hand,0.006556
hate,0.015189
head,0.013661
hegemoni,0.042901
hellen,0.030457
hellenist,0.080702
help,0.011585
heracliu,0.019594
hi,0.037953
highestrank,0.020064
hill,0.010525
hippia,0.021314
hippocr,0.015350
hispania,0.019114
histor,0.011887
histori,0.026442
historian,0.026747
holi,0.011202
homer,0.029678
idea,0.011876
ideal,0.025828
idealis,0.019682
ii,0.006924
illyria,0.022216
illyrian,0.020064
imit,0.012517
immens,0.012713
imperi,0.072152
includ,0.003673
inconclus,0.015412
increas,0.005088
inde,0.017993
india,0.016201
individu,0.005457
influenc,0.023311
influenti,0.017019
inhabit,0.009198
inscript,0.014286
inspir,0.008754
instabl,0.011440
integr,0.006385
interact,0.006601
interlock,0.017590
interven,0.011369
invas,0.019296
invit,0.010903
ionia,0.020623
iron,0.009476
irrevers,0.014605
isadora,0.024719
isagora,0.025258
ital,0.016348
itali,0.038087
iudaea,0.022928
jacquesloui,0.022671
john,0.006018
julioclaudian,0.024719
juniu,0.020880
justinian,0.016718
king,0.033708
kingdom,0.014075
kinsman,0.023521
known,0.008841
l,0.015574
label,0.009519
land,0.006695
languag,0.046543
larg,0.018648
largest,0.014612
late,0.042872
later,0.036826
latin,0.124715
latium,0.024264
latterday,0.017725
lead,0.021111
leagu,0.021707
led,0.011418
left,0.021154
legaci,0.011122
legend,0.024924
leuctra,0.024264
liber,0.007975
lifetim,0.011327
like,0.018899
likewis,0.010477
lingua,0.017418
link,0.007692
literaci,0.012979
literatur,0.015806
live,0.005719
long,0.011486
longer,0.007295
look,0.007303
loss,0.007736
lost,0.038434
luciu,0.018637
lucretia,0.048528
lysand,0.020387
macedon,0.077378
macedonian,0.032933
maiden,0.017966
maintain,0.018503
make,0.004497
manag,0.006334
mani,0.011993
mantl,0.014963
marbl,0.015843
marcu,0.013984
mark,0.014203
martial,0.014243
match,0.009406
mathemat,0.006953
mauric,0.026381
maxim,0.010059
maximu,0.018637
mean,0.004871
medicin,0.008433
mediev,0.028302
mediterranean,0.057849
mesopotamia,0.013450
middl,0.029170
midth,0.010912
millennium,0.022079
milton,0.013131
minor,0.024556
mithraism,0.023521
model,0.005509
modern,0.015916
molir,0.021314
monarchi,0.034013
moscow,0.025343
mountain,0.009516
movement,0.006099
muslim,0.029675
mutini,0.015370
mytholog,0.024741
napoleon,0.012063
naval,0.011157
nd,0.016459
near,0.007272
neoclass,0.013385
neoplaton,0.015391
new,0.015803
north,0.014045
notabl,0.013574
note,0.005099
object,0.006046
offic,0.007119
offici,0.006715
oligarchi,0.016377
olymp,0.013354
onli,0.012214
onward,0.011213
order,0.004874
orient,0.009216
origin,0.024508
orthodoxi,0.014821
ottoman,0.012509
oust,0.013820
outlin,0.034437
overthrow,0.038531
paeonian,0.023869
paint,0.011233
painter,0.015795
palatin,0.018900
panthon,0.022671
papaci,0.034593
parliament,0.008920
particular,0.011072
pastor,0.013971
patriarch,0.029540
patrician,0.019773
peac,0.008530
peisistrato,0.025258
pelopida,0.025917
peloponnesian,0.039017
peopl,0.014629
period,0.105110
persia,0.013170
persian,0.022225
persist,0.010028
philip,0.021743
philosophi,0.052713
phoca,0.022012
phoenicia,0.021162
phoenician,0.032144
pirenn,0.020749
place,0.010004
platon,0.028235
play,0.006386
poe,0.017725
poetic,0.028457
poetri,0.049362
point,0.005130
polit,0.055513
polyth,0.017179
pontifex,0.020623
pope,0.012439
port,0.009994
posit,0.010483
postclass,0.017966
potteri,0.041723
power,0.062285
pp,0.008268
practic,0.005426
precis,0.008354
preserv,0.017097
presid,0.007334
prevail,0.010730
princ,0.010945
princip,0.007923
priscu,0.024719
process,0.004794
promin,0.007953
prospartan,0.025258
protect,0.006759
protohistor,0.022012
prove,0.007827
provok,0.012979
publica,0.062248
punic,0.018120
pyrrhic,0.022671
racin,0.020387
rape,0.015055
rd,0.009082
reach,0.006537
reappear,0.015843
recogn,0.013794
redfigur,0.025258
rediscov,0.014010
refer,0.012866
reform,0.007868
refuge,0.012042
region,0.023040
reign,0.022245
reinvent,0.016098
religion,0.016385
remov,0.014853
remu,0.024264
renaiss,0.042545
republ,0.067946
republican,0.057452
respect,0.006117
restraint,0.014448
result,0.022403
resurg,0.013759
retrospect,0.014159
return,0.006510
rever,0.014358
revitalis,0.019508
reviv,0.051112
revolut,0.007968
revolutionari,0.020474
rex,0.017141
rimbaud,0.022435
rise,0.038837
rival,0.010628
rm,0.015288
rock,0.010073
romaioi,0.024719
roman,0.396906
romanian,0.015564
rome,0.175725
romulu,0.020503
rose,0.009761
rule,0.035230
ruler,0.032242
russian,0.009417
s,0.004562
sabin,0.054520
samnit,0.024264
sampl,0.009585
sardinia,0.018067
saw,0.023615
say,0.006969
scholar,0.008168
scholarli,0.011386
scholarship,0.011501
scienc,0.010820
sculptur,0.028928
sea,0.008207
second,0.005279
secular,0.011132
seen,0.006311
selfdescrib,0.018455
senat,0.044140
seri,0.012816
serviu,0.024719
settlement,0.018774
seventh,0.012979
sever,0.009334
sexual,0.010570
shift,0.007795
shrine,0.015843
sicili,0.029574
sieg,0.039999
signal,0.009255
signific,0.011429
significantli,0.008255
similarli,0.008481
slavic,0.015677
slow,0.009242
slowli,0.010318
social,0.011389
societi,0.016826
socioeconom,0.012933
son,0.034286
soninlaw,0.017336
sought,0.008721
sourc,0.005631
southwestern,0.013971
sovereign,0.010699
spain,0.009513
spark,0.011847
sparta,0.057797
spartan,0.130910
speaker,0.011496
specif,0.010712
spread,0.007913
st,0.036083
state,0.031336
statehood,0.016558
stole,0.018900
strict,0.010402
strong,0.006504
stronghold,0.014573
structur,0.005230
studi,0.004731
style,0.036896
subject,0.011389
subordin,0.012355
subvers,0.016818
succeed,0.009152
success,0.005936
successor,0.010056
summon,0.015700
superbu,0.126292
superregion,0.053535
suprem,0.018335
supremaci,0.029275
surviv,0.015442
symbol,0.008127
syncrat,0.025258
syria,0.026321
taken,0.013109
tarpeian,0.026767
tarquiniu,0.049439
task,0.008304
technolog,0.006454
templ,0.011834
term,0.012690
territori,0.015671
th,0.159131
theater,0.028928
theatr,0.012646
theban,0.045343
thebe,0.037665
theme,0.009914
themselv,0.013280
theodosian,0.021473
theolog,0.010912
theori,0.005247
thesi,0.011078
thi,0.023439
thoma,0.008007
thought,0.012689
thrace,0.018455
thracian,0.019867
thth,0.014945
time,0.011882
timelin,0.010537
titl,0.016071
togeth,0.006173
trace,0.017168
trade,0.006476
tradit,0.028186
tragedian,0.021314
trajan,0.018575
transform,0.007328
transit,0.007793
tremend,0.013007
tribe,0.022041
triomph,0.022928
triumvir,0.019189
trojan,0.018120
troop,0.010104
troy,0.016957
truli,0.011218
tsar,0.033117
tulliu,0.024264
turmoil,0.013958
twin,0.013170
tyranni,0.015771
tyrant,0.017296
ultim,0.007743
unconqu,0.024264
undergon,0.013971
unif,0.012730
unit,0.018061
uniti,0.019680
univers,0.009273
unmarri,0.017725
unrest,0.012416
upheld,0.015677
urban,0.018779
urss,0.024264
use,0.014631
usual,0.005387
variou,0.005096
veri,0.005112
vers,0.013180
viceger,0.025258
victori,0.031104
virtu,0.022686
vision,0.019393
visit,0.008994
vote,0.008489
wa,0.121130
war,0.069953
washington,0.009856
way,0.004786
weaken,0.011213
west,0.015373
western,0.040404
wide,0.005765
widespread,0.008600
wield,0.016019
women,0.008519
won,0.009062
word,0.018642
world,0.031770
written,0.014175
wrote,0.007627
year,0.004517
