abil,0.005109
abund,0.015062
abus,0.007988
accord,0.007426
account,0.004373
accus,0.029789
act,0.033954
activ,0.003705
actual,0.004752
ad,0.014217
addit,0.015027
address,0.005592
administr,0.010384
admit,0.007831
adopt,0.015374
adulteri,0.013950
advantag,0.005764
advent,0.008121
affect,0.004852
afford,0.015949
age,0.009301
agenc,0.029210
agent,0.012663
aggress,0.008274
agricultur,0.011039
alderson,0.016065
alleg,0.007678
allow,0.011078
alway,0.004993
america,0.062453
american,0.008415
ancient,0.043043
anglosaxon,0.011599
ani,0.016364
anoth,0.003571
anthracit,0.014963
anyth,0.007339
appear,0.004301
appli,0.003969
appoint,0.036627
apprehend,0.024403
approach,0.008547
architectur,0.007004
area,0.007376
arrang,0.006054
arrest,0.016322
aspect,0.004880
assembl,0.005797
assign,0.013340
assist,0.022148
associ,0.003627
athen,0.010264
attorney,0.061182
attract,0.005975
attribut,0.005772
augment,0.009562
august,0.005643
augustu,0.010988
australia,0.006454
author,0.016724
autocrat,0.012225
autumn,0.009880
avail,0.013618
await,0.022171
babylonian,0.010095
background,0.006377
bad,0.007617
bail,0.013005
bailey,0.012324
banish,0.023738
base,0.012943
basic,0.013514
bay,0.008386
becam,0.028650
becaus,0.006741
befor,0.003824
began,0.022356
begin,0.008732
behalf,0.008259
behavior,0.015757
believ,0.004631
berkeley,0.008745
bermuda,0.011869
besid,0.007661
bestknown,0.009761
biblic,0.009619
big,0.006780
blame,0.008895
blood,0.007292
board,0.006459
bobbi,0.044891
book,0.004255
borrow,0.007431
boston,0.008255
branch,0.004761
brand,0.018101
break,0.006172
breed,0.008979
bribe,0.011459
bring,0.005566
britain,0.006308
britannica,0.009106
british,0.009999
bud,0.011248
build,0.004738
built,0.005582
bureaucraci,0.009972
busi,0.015143
buy,0.007083
byproduct,0.009938
ca,0.008236
california,0.006869
came,0.009806
canada,0.006423
capit,0.009755
car,0.007924
carri,0.009669
case,0.003749
caus,0.020014
cell,0.006449
cemeteri,0.011001
central,0.004204
centrallyorganis,0.018434
centuri,0.038510
certain,0.012314
challeng,0.010714
chanc,0.007219
chang,0.003381
charg,0.017362
chicago,0.007309
chief,0.018735
china,0.028809
chines,0.006446
ching,0.013426
chong,0.014238
choos,0.006451
chow,0.014719
chu,0.012428
circumst,0.006889
citi,0.093228
citizen,0.024550
civil,0.005144
civilian,0.007796
claim,0.004552
clan,0.009832
classifi,0.006066
clerk,0.010333
close,0.008026
cloth,0.007840
code,0.029017
collect,0.021875
coloni,0.167448
colonist,0.133310
command,0.006292
commerc,0.007474
commiss,0.005971
commissair,0.034334
commission,0.036896
commit,0.018859
common,0.021760
commonli,0.004981
commonwealth,0.015495
commun,0.058830
complaint,0.009938
complet,0.004142
compos,0.005690
compstat,0.036869
concentr,0.011739
concept,0.012557
conceptu,0.014439
concern,0.016844
condit,0.008393
conduct,0.020563
confess,0.010894
conflict,0.011173
connect,0.009877
conquest,0.008082
consid,0.013992
consist,0.008029
constabl,0.075397
constitut,0.004736
construct,0.004644
contain,0.008606
contemporari,0.017601
contest,0.007892
contin,0.007729
continent,0.007988
continu,0.003634
control,0.015941
corpor,0.005885
corrupt,0.036822
counti,0.142823
countri,0.019737
court,0.079651
creat,0.018344
creation,0.010919
creativ,0.007750
crime,0.193497
crimefre,0.018434
crimin,0.167155
crowd,0.018696
cultur,0.022550
current,0.003840
custom,0.006065
cut,0.006368
dang,0.016374
danger,0.007045
day,0.018105
deal,0.014496
dealt,0.008990
debt,0.006955
debtor,0.011493
decid,0.017537
decis,0.005057
declin,0.005695
decre,0.008614
defend,0.020134
defens,0.026334
defin,0.008233
delamar,0.018434
demograph,0.008053
depart,0.052593
depend,0.015831
deputi,0.008032
despit,0.005086
detain,0.022686
detect,0.013174
develop,0.035999
devic,0.006703
did,0.017810
differ,0.009539
disappear,0.007626
discourag,0.018112
diseas,0.006283
dislik,0.010706
disput,0.006033
distinct,0.009239
district,0.065299
disturb,0.016846
divers,0.005769
divid,0.004756
dozen,0.008390
drive,0.006444
drunken,0.013897
dure,0.034529
dutch,0.014734
duti,0.028551
dynasti,0.015599
ear,0.009583
earli,0.039732
east,0.005552
econom,0.015808
edict,0.022086
effect,0.007016
effici,0.011309
elect,0.015979
embezzl,0.025365
emerg,0.004628
emperor,0.007964
emphasi,0.013100
empir,0.015117
employ,0.009580
empow,0.010168
enact,0.008005
encount,0.007112
enforc,0.121867
england,0.037599
english,0.020118
ensur,0.005933
environ,0.004787
era,0.005705
escap,0.007250
especi,0.020967
establish,0.030832
ethic,0.006646
europ,0.014690
european,0.008996
eventu,0.005132
everi,0.004518
everyon,0.025013
evid,0.014773
evolv,0.005927
exactli,0.007074
exampl,0.009702
exclus,0.012338
excoloni,0.016939
execut,0.021881
exercis,0.006204
exert,0.008286
exil,0.008247
exist,0.010773
expect,0.004961
expert,0.007133
extend,0.004763
extra,0.008270
extrem,0.015540
facil,0.013844
factor,0.009157
fair,0.007314
famili,0.015001
far,0.004896
fast,0.007392
fault,0.009700
fear,0.006776
featur,0.004883
februari,0.005867
feder,0.011499
fee,0.025357
feed,0.008290
fell,0.013949
feloni,0.028355
felt,0.007623
femal,0.014501
feud,0.012177
field,0.003943
figur,0.016044
financ,0.005722
financi,0.005155
fine,0.007720
fit,0.006574
flog,0.015339
florenc,0.010395
focus,0.010054
follow,0.012356
food,0.005557
forc,0.069670
foreign,0.005027
form,0.014758
formal,0.004832
format,0.005266
formul,0.006184
framework,0.005532
franc,0.011056
free,0.008821
french,0.041575
frequent,0.010857
frontier,0.008512
fu,0.012712
fulfil,0.007809
fulli,0.005822
fulltim,0.010283
function,0.007907
fund,0.005324
fundament,0.004945
furthermor,0.006617
gabriel,0.009746
gener,0.011099
german,0.005453
gibraltar,0.011710
glasgow,0.034029
gnral,0.014060
goal,0.005311
god,0.013517
good,0.008586
govern,0.040394
government,0.008082
governor,0.021264
gradual,0.012415
grand,0.007501
greater,0.004863
greec,0.014598
group,0.010450
grow,0.004951
grown,0.007436
guarante,0.007083
guard,0.024214
guilt,0.011617
guilti,0.010364
ha,0.005149
hammurab,0.018434
hammurabi,0.013343
han,0.007717
handl,0.042131
hang,0.010343
harm,0.007444
harsh,0.009188
harsher,0.013648
head,0.014955
health,0.005383
hear,0.016286
heard,0.009182
heavi,0.006674
heavili,0.012451
held,0.031959
helena,0.011910
help,0.021138
hi,0.010387
high,0.011730
higher,0.004590
highli,0.010167
hire,0.016863
histori,0.019298
hold,0.013954
home,0.005514
homeless,0.011848
homogen,0.008697
hous,0.015540
howev,0.028206
human,0.007440
hun,0.012428
idea,0.008668
ideal,0.006283
idl,0.011279
ill,0.007498
illustr,0.006585
impact,0.005292
implement,0.005376
import,0.019761
impos,0.006641
imposs,0.006529
impoverish,0.010894
imprison,0.018364
incarcer,0.012482
includ,0.045571
increas,0.003713
inde,0.006566
independ,0.003966
individu,0.019915
ineffect,0.009769
inevit,0.008508
infanticid,0.014300
influenc,0.017013
influenti,0.006210
inform,0.015356
informationbas,0.014879
informationl,0.018434
infrequ,0.011654
inhabit,0.013426
inherit,0.007160
inmat,0.013426
innoc,0.010206
inspecteur,0.018434
inspector,0.011599
instead,0.013569
instig,0.010545
institut,0.012258
interior,0.007578
interrog,0.023021
interst,0.011528
introduc,0.004529
invent,0.006283
investig,0.010822
involv,0.007487
irregular,0.009034
island,0.005723
item,0.007083
jail,0.055146
jailer,0.035418
japan,0.006385
jin,0.024403
job,0.013337
judg,0.106964
judici,0.022240
june,0.005626
juri,0.021635
jurisdict,0.007837
just,0.027016
justic,0.130521
juvenil,0.011343
juvenilejustic,0.018434
kansa,0.022817
kept,0.006957
king,0.006150
kingdom,0.015409
known,0.016132
korea,0.007793
la,0.012889
lack,0.019691
ladi,0.010283
land,0.004886
languag,0.004852
larg,0.017012
largest,0.005332
late,0.008939
later,0.011518
law,0.101121
lawenforc,0.016734
lawkeep,0.018434
lawknowledg,0.018434
lawmak,0.011749
lawyer,0.028102
le,0.007690
leader,0.011290
learn,0.005423
leav,0.005467
leavenworth,0.017709
led,0.020833
left,0.015439
legal,0.047946
legislatur,0.007898
legitim,0.008019
lesser,0.007914
lesson,0.009061
liberti,0.008194
liberty,0.018434
lie,0.006325
lieuten,0.030911
life,0.004150
like,0.006896
limit,0.003942
lin,0.012130
liter,0.007423
live,0.004174
local,0.021862
locat,0.004746
london,0.016307
long,0.004191
look,0.005330
lord,0.007640
lose,0.006789
lot,0.015902
loui,0.007284
lowest,0.007678
magistr,0.101411
main,0.012260
mainli,0.005359
maintain,0.022506
major,0.006849
make,0.013131
makeshift,0.013897
male,0.007241
manhandl,0.018041
mani,0.052521
map,0.005905
march,0.010706
marin,0.007390
marshal,0.025668
massachusett,0.008465
matter,0.014567
max,0.007888
mayor,0.029378
mean,0.003555
meant,0.006621
meet,0.005223
member,0.004118
men,0.011972
mention,0.006249
mercantilist,0.012712
merchandis,0.011071
merit,0.009212
met,0.013203
meteropolitain,0.018434
method,0.007996
metropolitan,0.019617
miasma,0.015555
middl,0.010644
militari,0.005222
militia,0.009989
million,0.005070
mimic,0.010817
mind,0.006087
minor,0.005974
minuscul,0.013076
misconduct,0.012040
misdemeanor,0.015051
mistak,0.008884
mix,0.006209
model,0.012063
modern,0.034848
mold,0.011029
money,0.023076
mongolia,0.011203
monopoli,0.008270
montreal,0.011581
moral,0.006458
moralist,0.014060
mostli,0.011015
municip,0.007866
murder,0.016397
mutil,0.012903
napolon,0.014963
narrow,0.007289
natur,0.003446
necessari,0.010289
need,0.007761
new,0.034602
nicola,0.019342
night,0.055427
nobl,0.008394
nod,0.014177
nomad,0.009972
nonpartisan,0.012324
norman,0.009095
north,0.005125
notabl,0.004953
notion,0.012214
notori,0.010244
oblig,0.007128
occur,0.004101
octob,0.005582
offend,0.032414
offens,0.035983
offic,0.025980
offici,0.024504
old,0.005341
omnipres,0.012653
onli,0.020800
onlin,0.005649
oppress,0.009520
ordeal,0.014570
order,0.024901
ordinari,0.007039
organ,0.007042
origin,0.007154
overcrowd,0.012870
overreach,0.015339
oversaw,0.011143
oversea,0.007699
ow,0.035418
owe,0.007856
paid,0.026327
pari,0.056473
parlement,0.014500
parliament,0.013021
parol,0.014500
parti,0.009977
particular,0.004040
pass,0.010030
passag,0.007690
patrol,0.020769
pattern,0.005507
pay,0.012053
payment,0.013915
peac,0.018676
peel,0.026080
peelian,0.018434
penalti,0.009656
penitentiari,0.014570
pennsylvania,0.009671
pennypack,0.018041
peopl,0.024913
perform,0.004538
period,0.030685
person,0.016896
petit,0.009548
philadelphia,0.009816
physic,0.004224
pillori,0.016214
place,0.014602
plan,0.004888
plenti,0.010150
point,0.003744
polic,0.610253
policemen,0.025740
polit,0.016206
polizeiwissenschaft,0.035418
poor,0.006382
popul,0.013778
port,0.007294
posit,0.019128
possibl,0.003775
power,0.011364
practic,0.003960
practition,0.007768
praetorian,0.016374
prefect,0.161923
prefectur,0.059042
premodern,0.011015
prerog,0.011828
present,0.011103
presid,0.010706
pressur,0.005572
prevent,0.015876
previou,0.011164
previous,0.005643
price,0.005480
primari,0.014659
primarili,0.005059
primat,0.010501
principl,0.008887
prior,0.011182
prioriti,0.007806
prison,0.109992
privat,0.026538
probat,0.026449
problem,0.023753
problemcaus,0.018434
problemori,0.017167
procedur,0.006446
proceed,0.033871
process,0.017497
proclaim,0.008374
procur,0.020451
profession,0.053983
progress,0.005045
promot,0.005280
prosecut,0.040028
prosecutor,0.023421
protect,0.029601
protest,0.006879
prove,0.005712
provinc,0.006843
public,0.033551
publicli,0.007921
publish,0.004108
punish,0.171006
purg,0.010354
puritan,0.012085
purpos,0.009630
qu,0.014963
qualifi,0.007784
quebec,0.010868
queen,0.007768
question,0.009297
quickli,0.012050
quiet,0.010842
quit,0.018767
radio,0.007578
rampant,0.011730
rank,0.006249
rape,0.010988
rare,0.006091
reactiv,0.026532
read,0.004077
realiz,0.006564
reason,0.012640
recommend,0.006920
record,0.004948
recruit,0.008655
reduc,0.008894
refer,0.007042
reflect,0.005045
reform,0.017227
refus,0.006851
regard,0.009008
regist,0.007279
regul,0.005557
reign,0.008117
reinforc,0.007995
rel,0.012627
relat,0.006330
reliev,0.009824
religi,0.017990
religion,0.005979
remain,0.007765
reorgan,0.019633
repeat,0.006776
repent,0.025873
replic,0.008286
report,0.013536
repres,0.003851
republ,0.005509
request,0.007135
requir,0.003695
resembl,0.007405
resist,0.006083
resourc,0.009505
respect,0.008929
respond,0.006331
respons,0.040719
rest,0.005500
restrict,0.010882
result,0.006540
return,0.004751
revolut,0.005815
reyni,0.018434
right,0.004212
riot,0.046901
rise,0.004724
robert,0.010323
role,0.024675
roman,0.006297
rome,0.016031
room,0.015230
roosevelt,0.010490
rotat,0.007772
royal,0.012388
rule,0.012856
run,0.005159
s,0.016648
safe,0.007675
salari,0.017367
samuel,0.008345
sanit,0.010578
scheme,0.007065
scholar,0.005962
scienc,0.003948
scotland,0.017647
scottish,0.009012
scythian,0.013303
secretari,0.007670
seek,0.005267
segreg,0.010274
select,0.010209
selfpol,0.017167
sell,0.006641
sens,0.004830
sent,0.006507
separ,0.004289
septemb,0.005661
sergeant,0.012806
sergent,0.016939
seriou,0.039459
serv,0.018692
servic,0.018296
set,0.018183
settl,0.013344
seven,0.006160
seventeenthcenturi,0.013040
sever,0.010218
sexual,0.007714
shame,0.036604
shangtung,0.018434
shape,0.005595
share,0.004574
sheriff,0.151738
sign,0.005379
similar,0.019507
simpl,0.005191
simpler,0.008711
simpli,0.005331
sin,0.018582
sinc,0.022869
sir,0.014749
situat,0.005030
slain,0.013950
slave,0.023161
slow,0.006745
small,0.007996
social,0.020781
societi,0.016374
sociologist,0.009272
solicit,0.011768
solv,0.006013
sometim,0.008506
soon,0.023965
sophist,0.007517
sourc,0.004109
south,0.005116
spare,0.010168
special,0.004086
specif,0.003909
specifi,0.006642
spread,0.011550
spring,0.007369
squad,0.012566
st,0.005266
standard,0.004400
stapl,0.010343
state,0.037164
stateadminist,0.017709
station,0.007137
stay,0.014303
stem,0.014688
step,0.005540
stinch,0.018434
strategi,0.018258
street,0.007455
strength,0.006626
strict,0.007592
strike,0.007267
strong,0.009494
strongli,0.006297
stuck,0.011392
student,0.005561
studi,0.003452
submit,0.008183
subprefect,0.016939
success,0.004332
supervis,0.007812
sure,0.017905
surveil,0.010313
suspici,0.011158
symbol,0.005931
task,0.006061
tax,0.018421
teach,0.006400
technolog,0.004710
telephon,0.008931
tension,0.007421
territori,0.005718
testifi,0.011143
th,0.041210
themselv,0.004846
theodor,0.008942
theoret,0.005357
theori,0.011489
thi,0.048876
thing,0.005198
thought,0.004630
thousand,0.005761
thu,0.007638
time,0.017345
tith,0.013602
today,0.004693
togeth,0.004505
toronto,0.010150
town,0.020128
track,0.007229
tradit,0.004114
train,0.016925
trait,0.007741
transfer,0.005591
transform,0.010696
treatis,0.007882
trend,0.006240
trial,0.096920
troubl,0.017156
troublemak,0.015926
true,0.005290
turn,0.009033
twoway,0.012299
type,0.003928
unabl,0.006789
uncommon,0.010015
understand,0.004435
unemploy,0.007652
unheard,0.013845
uniform,0.015150
uniqu,0.005571
unit,0.026363
unlik,0.005441
unpaid,0.022497
unrest,0.009061
unspeci,0.033468
urban,0.013706
use,0.048054
usual,0.039318
valuabl,0.007670
variou,0.003719
vehement,0.012402
vendetta,0.016065
verdict,0.012455
veri,0.018656
victim,0.043897
view,0.004187
vigilant,0.014797
vigor,0.009452
vill,0.014365
villag,0.007466
violat,0.007229
violent,0.008067
virginia,0.009412
vollmer,0.033093
vstgtalagen,0.018434
vulner,0.008310
wa,0.257179
wall,0.006918
wang,0.010578
war,0.004641
ward,0.009998
watch,0.041649
watchmen,0.032428
way,0.017467
weber,0.009989
websit,0.006178
wergild,0.018434
west,0.011220
western,0.009829
whip,0.025306
whiskey,0.014719
wichita,0.016065
wide,0.004208
wilson,0.016322
wit,0.015915
witchcraft,0.012376
women,0.018654
word,0.013606
work,0.016015
worker,0.006082
workload,0.013112
world,0.016562
wors,0.008984
wude,0.018434
xiv,0.011029
year,0.003297
ying,0.014300
york,0.010064
zealand,0.007768
