ab,0.004604
abridg,0.006262
abroad,0.007982
absorb,0.003674
accept,0.006922
accord,0.005513
account,0.002164
accus,0.003686
achiev,0.002312
acknowledg,0.003544
act,0.004201
activ,0.001834
ad,0.009382
administr,0.005139
adopt,0.005072
aftermath,0.004580
age,0.013811
ago,0.003561
agre,0.002771
agreement,0.011222
aim,0.010498
al,0.006305
aleksandr,0.006164
alexand,0.007043
alleg,0.003800
alli,0.006789
allow,0.005483
altern,0.002361
amend,0.007752
anarchi,0.005537
anarchist,0.005411
anatoliy,0.008384
ancient,0.002663
ander,0.005688
andrew,0.003732
andrusovo,0.018724
ani,0.003239
anna,0.005247
annex,0.012544
announc,0.003086
antholog,0.010438
antiqu,0.003803
antisoviet,0.007542
antonovych,0.009669
anxieti,0.004878
apart,0.003368
appear,0.010644
appli,0.001964
appoint,0.003021
approach,0.002115
approv,0.003157
april,0.005623
archaeolog,0.011806
architect,0.004886
archiv,0.003383
area,0.007302
argu,0.002445
armi,0.015807
arriv,0.005917
asavelii,0.009669
asia,0.003155
askold,0.008765
aslund,0.009669
aspir,0.004620
assembl,0.005738
assert,0.003157
associ,0.005386
attempt,0.008386
attent,0.002997
auditorium,0.007047
august,0.002793
austria,0.004252
austriahungari,0.006112
austrian,0.026415
author,0.014486
autonom,0.003716
autonomi,0.011522
avail,0.011234
azerbaijan,0.005638
balanc,0.002909
balkan,0.005213
ballot,0.005145
baltic,0.005197
ban,0.007406
bandera,0.008104
base,0.006406
battalion,0.005465
battl,0.013884
bc,0.019673
becam,0.020258
becaus,0.001668
becom,0.012419
befor,0.001892
began,0.006639
begin,0.002161
belaru,0.022686
belong,0.003131
belorussia,0.009669
berkhoff,0.019338
besieg,0.010959
bessarabia,0.007364
biaowiea,0.008765
bibliographi,0.003223
bilinski,0.009124
birth,0.003265
bitter,0.010029
black,0.017946
blaj,0.009124
bloc,0.008894
bohdan,0.007882
bolshevik,0.017446
bone,0.008313
border,0.016323
borderland,0.013705
bori,0.005544
boshyk,0.009669
braclav,0.009669
brandon,0.007406
break,0.003054
breakthrough,0.004729
brief,0.003512
briefli,0.004111
broke,0.003800
bronz,0.008921
brzezani,0.009669
build,0.004690
bukovina,0.007757
bulgar,0.014353
bulgaria,0.005023
bullet,0.005479
bum,0.008025
burankaya,0.009669
burden,0.004230
butaft,0.009669
byelorussian,0.008765
byzantin,0.009119
c,0.004242
cabinet,0.003728
came,0.016988
camp,0.008032
campaign,0.003332
canada,0.003179
canadian,0.015070
candid,0.006838
capac,0.003076
capit,0.004828
captur,0.003047
carolina,0.005001
carpathian,0.007017
carpin,0.008496
case,0.003711
caspian,0.006015
cassett,0.006931
castl,0.005033
catacomb,0.007951
categoris,0.005606
catherin,0.010575
cathol,0.003583
catholic,0.004974
caucasu,0.005418
caught,0.004571
caus,0.003962
cave,0.004463
ce,0.004067
ceas,0.003749
center,0.002367
central,0.018726
centralis,0.005796
centrist,0.006164
centuri,0.060994
certain,0.002031
chain,0.003316
chalcolith,0.006987
chang,0.001673
chao,0.004295
chaotic,0.010170
character,0.002815
charg,0.002864
chernihiv,0.017859
chernyakhov,0.009669
cherven,0.009669
choos,0.003193
chorna,0.009669
christian,0.017260
chronicl,0.004675
church,0.006707
cimmerian,0.007882
cite,0.003104
citi,0.033325
citizen,0.003037
civil,0.007638
civilian,0.007717
claim,0.002253
class,0.002462
closer,0.007346
coast,0.006775
coauthor,0.005090
collabor,0.006819
collaps,0.006540
collect,0.002165
coloni,0.012278
colonis,0.005028
come,0.004246
commemor,0.004970
commit,0.003111
common,0.001795
commonwealth,0.065189
commun,0.003882
communist,0.010984
compar,0.002195
compet,0.003060
complet,0.004100
compos,0.008449
compromis,0.012804
concentr,0.002905
concept,0.002071
concern,0.002084
concis,0.004521
condemn,0.004181
confer,0.002922
conflict,0.005530
congress,0.003326
conquer,0.004019
conquest,0.012000
consider,0.002494
constitut,0.007032
contact,0.003115
contemporari,0.002903
contenti,0.005275
context,0.005223
continu,0.005396
control,0.017752
convert,0.006052
copper,0.004122
core,0.003152
correspond,0.002695
corrupt,0.007290
cossack,0.122453
cost,0.002610
council,0.008121
counter,0.004014
countless,0.005515
countri,0.015630
countrysid,0.004866
cours,0.002587
court,0.002815
creat,0.007263
creation,0.002702
crimea,0.045429
crimean,0.054678
crisi,0.021446
croat,0.013918
crossov,0.005970
crown,0.007690
cruel,0.005916
cucutenitrypillian,0.016208
cultur,0.033484
cuman,0.007285
cut,0.003151
czarist,0.007699
czechoslovakia,0.005854
d,0.002403
da,0.003952
dacian,0.007757
dam,0.005010
danieri,0.009669
date,0.002481
day,0.004480
dead,0.003722
deal,0.002391
death,0.013001
debat,0.002763
debatt,0.009124
decad,0.002698
decemb,0.021795
decis,0.002503
declar,0.005735
declin,0.005637
decommun,0.009669
deep,0.003305
defeat,0.010538
degre,0.002431
del,0.004482
delug,0.007142
demand,0.002650
democraci,0.003307
democrat,0.002909
den,0.005323
denation,0.007818
deni,0.003441
denounc,0.004808
depend,0.001958
deriv,0.002257
descend,0.003593
descent,0.008518
desir,0.002794
despair,0.012175
despit,0.002517
destroy,0.006209
destruct,0.003513
determin,0.002061
devast,0.004215
dictionari,0.003599
did,0.008815
die,0.011378
differ,0.007869
difficult,0.002589
dimarov,0.009669
diplomat,0.003527
direct,0.001971
disintegr,0.004854
dismiss,0.007910
displac,0.003809
disput,0.002986
dissemin,0.004623
dissolv,0.011062
distant,0.004126
divid,0.007062
divis,0.005146
dmytro,0.009669
dnieper,0.007364
document,0.008598
doix,0.006710
dokia,0.009669
domest,0.003076
domin,0.007329
dominantli,0.007495
don,0.014782
donbass,0.016565
donetsk,0.024847
dontsov,0.009669
doroshenko,0.009669
drawn,0.003503
dream,0.004286
drevlyan,0.009669
duchi,0.005786
duke,0.004681
duleb,0.019338
dure,0.034180
duti,0.003532
dwell,0.005252
e,0.004811
earli,0.008938
earlier,0.002641
earliest,0.005975
earn,0.003359
east,0.021984
eastcentr,0.007495
eastern,0.029667
eastslav,0.009669
eclect,0.005759
econom,0.005868
economi,0.017087
ecumen,0.006277
ed,0.010009
edit,0.022380
edmonton,0.007364
educ,0.002277
effort,0.009901
eightyear,0.006454
einsatzgruppen,0.008496
elearn,0.007592
elect,0.026363
electron,0.002991
element,0.002273
eleonora,0.008496
elev,0.007975
elit,0.007938
embrac,0.004032
emerg,0.016035
empir,0.044893
empirebuild,0.007951
empti,0.008522
enabl,0.005669
encyclopedia,0.006156
end,0.005862
endur,0.008724
enemi,0.003994
english,0.004978
engulf,0.006322
enlarg,0.004565
enlist,0.005598
ensu,0.004369
ensur,0.002936
entent,0.006564
enter,0.002613
entir,0.002318
entiti,0.003032
envelop,0.005208
envoy,0.005311
era,0.005648
erupt,0.004463
escal,0.004630
escap,0.003588
especi,0.006226
essay,0.003463
establish,0.007630
estim,0.002515
et,0.006145
ethnic,0.003527
eu,0.003891
eurasia,0.005208
eurasian,0.005479
euromaidan,0.008622
europ,0.026660
european,0.024489
eventu,0.012702
everi,0.002236
evolv,0.002933
exampl,0.001600
excerpt,0.014820
exclus,0.006107
exil,0.004082
exist,0.008887
exodu,0.016748
expand,0.005056
expans,0.006031
experi,0.002198
experienc,0.006417
export,0.003278
extend,0.004715
extens,0.002413
extent,0.002941
extern,0.002795
extrem,0.005127
f,0.005265
face,0.002682
fact,0.002269
faction,0.004221
facto,0.004333
factor,0.002266
fall,0.002439
famin,0.014359
far,0.002423
fate,0.009026
father,0.003142
favor,0.008856
fear,0.006707
februari,0.011615
feder,0.002845
fell,0.017260
field,0.001951
fierc,0.004915
fight,0.003353
fighter,0.004854
figur,0.005294
final,0.002311
fled,0.012978
flee,0.004583
flourish,0.007867
flow,0.005629
follow,0.013760
food,0.002750
forc,0.009578
forest,0.003675
form,0.002921
formal,0.011958
formalis,0.005614
format,0.005213
fought,0.012162
foundat,0.004984
fragment,0.003759
frame,0.003652
free,0.004366
freedom,0.003017
french,0.002572
frequent,0.002686
frontier,0.004213
fulfil,0.003865
fullscal,0.005567
ga,0.003215
gain,0.002325
galicia,0.032359
galiciavolhynia,0.027372
gave,0.005562
gdp,0.010784
gener,0.001373
genoes,0.006755
genu,0.004827
geograph,0.003108
georg,0.008263
georgiy,0.008929
geostrateg,0.007406
german,0.005398
germani,0.014389
geta,0.008025
giovanni,0.005192
glacial,0.005638
glacier,0.009993
gobi,0.007247
gogol,0.016379
golden,0.007745
goth,0.006564
gothic,0.005705
govern,0.014540
government,0.004000
gradual,0.003072
grand,0.011137
grandduk,0.009362
grandmoth,0.006604
grant,0.002930
gravettian,0.008104
great,0.019387
greater,0.002407
greatest,0.013453
greatli,0.003129
greek,0.002706
gross,0.003737
ground,0.002781
group,0.003448
growth,0.004838
ha,0.011470
habsburg,0.005916
hadiach,0.009669
half,0.005261
halych,0.017859
halychvolynia,0.009669
hand,0.002368
handbook,0.003868
hardli,0.005109
harvard,0.003734
harvest,0.008474
head,0.004934
headway,0.006852
heartland,0.006004
held,0.006779
help,0.002092
heneza,0.009669
henryk,0.007247
hermonassa,0.009669
hetman,0.024847
hi,0.022278
high,0.001935
highli,0.002516
histor,0.012882
histori,0.073229
historian,0.016103
historiographi,0.019403
hitler,0.005208
holocaust,0.016503
holodomor,0.017245
home,0.002729
homo,0.005010
hord,0.013333
hors,0.004128
host,0.006990
hous,0.002563
howev,0.006204
hrushevski,0.048347
hrytsak,0.009669
httpabimperionetcgibinaishowplstateshowaidartidlangcod,0.009669
huge,0.003554
human,0.005524
humenna,0.009669
hun,0.018454
hundr,0.005807
hungari,0.004733
hungarian,0.004927
hunger,0.010517
hunnic,0.007882
idea,0.004290
ident,0.013683
ideolog,0.014441
ii,0.017509
illeg,0.003851
illustr,0.003259
ilyich,0.014812
immedi,0.002809
impact,0.002619
imperi,0.007446
imperio,0.008104
implement,0.002661
import,0.001630
impos,0.006574
improv,0.002352
impuls,0.004898
incess,0.006959
includ,0.018575
incorpor,0.005249
increas,0.003675
increasingli,0.002817
incumb,0.005124
inde,0.006499
independ,0.041225
indiana,0.004827
individu,0.001971
indoeuropean,0.005864
influenc,0.002105
inform,0.001900
inhabit,0.003322
initi,0.006230
inland,0.004586
inspir,0.006324
instal,0.003771
instead,0.004477
institut,0.004045
insurg,0.019794
intellectu,0.026231
interim,0.004586
intern,0.008374
internecin,0.007017
interpret,0.007649
intervent,0.003468
intimid,0.005472
invad,0.015290
invas,0.010455
invit,0.003938
iron,0.003423
irregular,0.004471
isbn,0.015816
ivan,0.005139
jan,0.004200
januari,0.013638
jew,0.013480
jewish,0.003962
join,0.002842
journal,0.004916
journey,0.008883
judaism,0.004571
jure,0.005723
just,0.002228
karel,0.012908
karlowitz,0.008622
kasianov,0.009669
katchanovski,0.009669
kazakhstan,0.005522
kept,0.003443
khanat,0.025229
khazar,0.037249
khmelnytski,0.008622
khotyn,0.009124
khrushchev,0.012740
kiev,0.173797
kievan,0.061673
kill,0.009465
king,0.003044
kingdom,0.020338
klen,0.009669
km,0.003597
known,0.012775
kohut,0.008104
konashevychsahaidachni,0.009669
kononenko,0.009669
konstantyn,0.009669
kozak,0.008282
kravchuk,0.008384
krushelnycki,0.009669
krypiakevych,0.009669
kuban,0.016565
kubicek,0.019338
kubijovi,0.009669
kuchma,0.035718
kudelia,0.009669
kurgan,0.007951
kutaisov,0.009669
kuzio,0.008929
l,0.002813
laboratori,0.003242
lack,0.002436
ladomir,0.009669
lake,0.003769
land,0.021766
languag,0.012009
larg,0.006736
largest,0.002639
late,0.015486
later,0.015203
latin,0.003003
law,0.009625
leader,0.002794
leav,0.002706
led,0.012373
left,0.002547
leftbank,0.008622
legaci,0.008035
legal,0.002636
legend,0.004501
legitim,0.003969
leonid,0.017978
lessen,0.005080
level,0.001910
liber,0.005761
librari,0.003002
life,0.004108
like,0.005120
limit,0.003902
line,0.002286
linguist,0.003756
link,0.002778
list,0.001776
literari,0.004007
literatur,0.005709
lithuania,0.054452
lithuanian,0.018787
litwin,0.009362
local,0.008656
london,0.002690
long,0.008298
longer,0.005270
longest,0.004580
look,0.007914
lowdnipro,0.009669
lower,0.004722
loyalist,0.005825
lublin,0.007017
luckyj,0.029008
lucr,0.005270
lugansk,0.018724
luhansk,0.008384
lustrat,0.008622
lviv,0.008384
lypa,0.009669
magazin,0.003699
magocsi,0.009362
maidan,0.009124
main,0.004045
maintain,0.004455
major,0.008475
mammoth,0.006624
mani,0.015885
map,0.002923
march,0.005299
margin,0.007003
mark,0.002565
market,0.002391
marquett,0.008104
martial,0.005145
mass,0.007860
massacr,0.004658
massiv,0.006799
mcfaulrevolut,0.009669
mean,0.001759
media,0.002930
mediev,0.006815
melt,0.004559
member,0.012231
memori,0.016618
men,0.002962
merg,0.003848
met,0.003267
michael,0.005880
mid,0.003181
middl,0.010537
midth,0.003942
midwif,0.007110
migrat,0.010241
militari,0.007754
millennium,0.003987
million,0.015057
minist,0.002882
mitchel,0.005354
mix,0.003073
mobil,0.003393
modern,0.017248
molodova,0.009362
molotovribbentrop,0.007110
mongol,0.030116
mongolian,0.006386
monomakh,0.018724
month,0.002822
monument,0.004574
moravia,0.006755
moscow,0.004577
mosendz,0.009669
mostli,0.005452
motiv,0.003121
mountain,0.003437
movement,0.006609
multiethn,0.006205
multifacet,0.006205
muscovi,0.007449
music,0.003511
mutual,0.003370
mykhailo,0.027372
myroslav,0.019338
narrow,0.003607
narvseliu,0.009669
nascent,0.010720
natalia,0.015636
nation,0.047802
nationalist,0.020713
nationst,0.005360
nativ,0.003302
nativeland,0.009669
natur,0.001705
nazi,0.031185
nd,0.005945
ndth,0.009669
neanderth,0.012126
near,0.005254
nearli,0.002782
nebesio,0.009669
nedao,0.009669
neg,0.002770
neighbor,0.003604
neighbour,0.003902
neolith,0.004761
new,0.004281
nezalezhnosti,0.009669
nicaea,0.006545
nikita,0.006338
nikolai,0.011343
nobil,0.004983
nobl,0.008309
nobleman,0.006666
nomad,0.004936
north,0.010147
northeast,0.004550
northeastern,0.004936
northern,0.008960
northwest,0.008739
northwestern,0.009701
notabl,0.004903
note,0.001842
noth,0.003381
novemb,0.005731
novocherkassk,0.009669
null,0.005219
number,0.006393
numer,0.007101
oblast,0.012943
observ,0.002108
occup,0.003329
occupi,0.009609
occur,0.004059
occurr,0.004132
offic,0.005143
offici,0.012128
officialsespeci,0.009669
oium,0.009124
olbia,0.008496
old,0.002643
oleg,0.007017
oleh,0.009124
oleksandr,0.008765
olena,0.009362
olga,0.006878
olzhych,0.009669
onli,0.002941
onlin,0.047539
oper,0.002015
opin,0.006489
opposit,0.010516
orang,0.014327
order,0.003521
orest,0.016050
organ,0.005228
origin,0.003541
orthodox,0.008605
ostrogoth,0.021530
ottoman,0.027112
oust,0.009984
outsid,0.002437
outstand,0.004369
overrun,0.005732
overview,0.003201
oxford,0.003002
pact,0.004797
page,0.006264
painter,0.005705
pannonia,0.007247
panslav,0.023648
papal,0.005537
paper,0.002636
paradigm,0.007742
parliament,0.009667
parti,0.009876
particular,0.001999
particularli,0.004481
partisan,0.005582
partit,0.017703
partli,0.003435
pass,0.007446
past,0.007604
paul,0.011109
pawn,0.006959
peasant,0.022275
peasantri,0.005981
peopl,0.042277
pereyaslav,0.025490
period,0.032273
persist,0.003622
person,0.002090
peter,0.002913
petersburg,0.005750
petro,0.022777
phanagoria,0.008929
philipp,0.005109
pian,0.008282
plokhi,0.018724
plummet,0.005825
plung,0.005465
podolia,0.008104
poet,0.004418
polan,0.009124
poland,0.078569
polanian,0.009669
pole,0.013277
polic,0.003553
polici,0.009457
polish,0.038849
polishlithuanian,0.054423
polishmad,0.009362
polishrussian,0.009669
polit,0.024063
polonis,0.009669
polonskavasylenko,0.009669
polotsk,0.008384
pontic,0.007495
popul,0.011366
popular,0.002285
poroshenko,0.009124
portal,0.004608
portion,0.009631
portray,0.004158
posit,0.001893
possibl,0.005606
postrevolutionari,0.007364
postsoviet,0.018494
potenti,0.002372
power,0.018749
pp,0.029869
practic,0.001960
pravda,0.008189
precursor,0.003991
prefer,0.002805
prehistor,0.004428
prehistori,0.009203
prepar,0.002971
present,0.005495
presentday,0.004114
presid,0.026496
presidenti,0.014986
press,0.028032
previous,0.005586
price,0.002712
primari,0.004836
prime,0.002943
princ,0.023722
princess,0.005360
princip,0.031482
prison,0.003888
probabl,0.002629
process,0.001732
proclaim,0.004144
produc,0.001906
product,0.001909
project,0.002267
prolong,0.004658
promot,0.005227
promulg,0.004672
proorang,0.009669
proper,0.003287
proposit,0.004211
propria,0.008622
propriath,0.009669
prorussian,0.015636
prosper,0.003787
protect,0.007325
protest,0.027239
proukrainian,0.009669
provid,0.003251
public,0.001845
publish,0.002033
pursu,0.003332
pyotr,0.013558
questia,0.007882
quickli,0.008946
race,0.003599
rada,0.007495
raider,0.005970
ran,0.008202
ray,0.003969
rd,0.006561
reach,0.004722
rebellion,0.012446
reced,0.005777
recent,0.006036
recess,0.004158
recogn,0.002491
recognis,0.007175
reconstruct,0.003641
record,0.002449
recov,0.003576
red,0.006441
redlich,0.009124
reduc,0.002201
reelect,0.004733
reestablish,0.004425
refer,0.003485
referendum,0.016136
reflect,0.002497
reform,0.005684
refuge,0.004350
refus,0.003390
regard,0.002229
region,0.031211
reid,0.005750
reign,0.004017
relat,0.004700
reliant,0.005614
religion,0.002959
remain,0.007686
remaind,0.004311
remnant,0.004645
remov,0.002682
renaiss,0.003842
replac,0.007103
report,0.002233
repress,0.013147
republ,0.032725
reput,0.004043
research,0.001885
resid,0.003078
residu,0.004485
resign,0.007941
respect,0.002209
rest,0.008167
restorationist,0.008025
result,0.012948
retain,0.003055
return,0.002351
reunif,0.005786
review,0.002603
revitalis,0.007047
reviv,0.011078
revolt,0.004230
revolut,0.020149
richer,0.005552
rig,0.005425
rightbank,0.017859
rise,0.002338
rivalri,0.004786
river,0.009530
robert,0.002554
roman,0.006233
romania,0.004874
romant,0.004944
rose,0.003526
rostovondon,0.008496
round,0.011042
rout,0.003498
rsfsr,0.008104
ru,0.149257
ruin,0.004620
rule,0.038179
ruler,0.019411
run,0.005107
runoff,0.015898
rurikid,0.009362
rusbyzantin,0.009124
russia,0.105864
russian,0.078243
russianaustrian,0.009669
russif,0.021743
russkaya,0.008765
russophil,0.045620
russophilia,0.009669
russophon,0.008496
russoturkish,0.007542
rutger,0.006027
ruthenia,0.047296
ruthenian,0.023454
ruthenianspeak,0.009669
rybak,0.009669
s,0.011536
sack,0.009670
samchuk,0.009669
sarmatian,0.007324
saw,0.002843
say,0.002517
scandal,0.009813
scatter,0.004290
scholar,0.002950
scholarli,0.008226
school,0.018912
scythia,0.007406
scythian,0.013168
sea,0.017788
search,0.011896
second,0.015256
secur,0.004937
select,0.005053
selfproclaim,0.006545
semiindepend,0.006987
seminomad,0.006732
sentenc,0.003972
separ,0.004245
separat,0.013129
separatist,0.011045
septemb,0.002802
serf,0.006178
serfdom,0.006125
serhi,0.009669
serhii,0.009362
serhiy,0.009669
servitud,0.006205
set,0.003599
settl,0.006604
settlement,0.006781
sever,0.005057
severian,0.009362
shape,0.002769
shimon,0.007211
shkandrij,0.009669
shoah,0.008765
shore,0.013254
shortag,0.004219
shortliv,0.004565
shrank,0.005805
siberian,0.006178
sign,0.005325
signific,0.002064
sinc,0.003234
site,0.008171
situat,0.002489
skull,0.005165
slave,0.007642
slavic,0.050969
slavon,0.006959
sloboda,0.008765
slow,0.003338
small,0.001978
snyder,0.006386
social,0.002057
socialist,0.007235
societi,0.006078
soldier,0.007359
soon,0.002965
sought,0.009451
sourc,0.004068
south,0.010129
southcentr,0.007247
southeastern,0.020095
southern,0.012005
southwest,0.004538
soviet,0.115589
sovietera,0.007364
sovietophil,0.019338
sovietrussian,0.007882
span,0.003735
spark,0.004279
speaker,0.004152
spirit,0.010961
spite,0.004658
sporad,0.005160
spread,0.005716
spring,0.010942
squar,0.003426
sredni,0.008929
ssr,0.051495
st,0.005213
stabil,0.003015
stalin,0.010634
start,0.014329
starv,0.011592
state,0.039619
stay,0.007079
stephen,0.010776
stepp,0.016843
stir,0.011446
stock,0.003119
stog,0.008929
strain,0.004019
street,0.007380
strict,0.007515
strictest,0.006419
strife,0.005329
strikingli,0.006564
strong,0.002349
struggl,0.006779
student,0.002752
studi,0.017090
subdivis,0.004750
subject,0.002057
subsequ,0.012618
subsid,0.004797
subsum,0.005405
subtelney,0.009669
subtelni,0.009669
succeed,0.003306
success,0.002144
successor,0.003632
suffer,0.002951
superpow,0.005560
supersed,0.004672
support,0.015381
suprem,0.006623
surround,0.005922
survey,0.006216
suspend,0.004019
suzdal,0.009124
sway,0.005411
symbol,0.005871
synonym,0.004035
t,0.002813
tailor,0.005347
talerhof,0.009669
tara,0.006604
tatar,0.013809
tchaikovski,0.016379
techniqu,0.002485
teliha,0.009669
tension,0.003673
term,0.006112
territori,0.028305
terrorfamin,0.009124
testimoni,0.005203
text,0.008368
textbook,0.007290
th,0.050066
themselv,0.002398
thenpresid,0.006852
theori,0.001895
ther,0.008025
therefor,0.002198
thi,0.021772
thirteen,0.004850
thought,0.004583
thousand,0.002851
threesid,0.008622
threeway,0.007247
thth,0.005398
thu,0.003780
tie,0.006308
till,0.004882
time,0.018600
timothi,0.005165
titl,0.002902
tiverian,0.019338
today,0.009292
togeth,0.004459
took,0.010105
tool,0.002589
topic,0.002565
toronto,0.025119
total,0.004512
trade,0.004678
tradit,0.004072
transcarpathian,0.009669
transfer,0.005534
transit,0.005630
translat,0.005568
transnat,0.011464
treati,0.023639
tri,0.007636
tribe,0.015924
truth,0.003440
tsardom,0.030797
tsarist,0.006526
tsvirkun,0.016993
turchynov,0.009669
turk,0.010219
turkish,0.004681
turn,0.006706
tymoshenko,0.061355
typic,0.002169
tyra,0.008765
u,0.011319
ukrain,0.647434
ukraina,0.009669
ukraineeuropean,0.009669
ukraineru,0.009669
ukrainian,0.510184
ukrainiansoviet,0.009362
ukrainophil,0.048347
ukrainophilia,0.009669
ula,0.008104
ulich,0.008765
ultim,0.005594
unbalanc,0.006248
underground,0.004482
undermin,0.004286
understand,0.002195
unearth,0.005981
unexpect,0.004722
union,0.060076
unit,0.013048
univers,0.015074
unknown,0.003348
unlik,0.002693
unrest,0.013455
unum,0.008765
upapartizan,0.009669
upris,0.008981
ural,0.006471
use,0.009249
ussr,0.018688
usual,0.003892
vacuum,0.004219
valentin,0.006100
vallentin,0.009669
varangian,0.007818
variou,0.005522
vassal,0.010771
velychenko,0.028087
veri,0.003693
victim,0.004345
victori,0.003745
view,0.002072
viktor,0.030438
villag,0.003695
violenc,0.007702
vladimir,0.023009
void,0.005176
vol,0.003475
volhynia,0.008929
volodim,0.009669
volodymyr,0.033987
volum,0.013850
volunt,0.004457
volyn,0.009669
volynia,0.029008
vote,0.018399
voter,0.013014
wa,0.075578
wake,0.004314
wallachiabessarabia,0.009669
wanner,0.008765
want,0.002985
war,0.052836
warfar,0.008202
warfareth,0.009669
water,0.002494
weaker,0.004604
wehrmacht,0.007449
welcom,0.004463
wendi,0.012773
went,0.002966
west,0.008330
western,0.041353
westward,0.011104
whereabout,0.007406
white,0.012281
wilson,0.004039
winner,0.004452
wise,0.005051
won,0.006547
word,0.002244
world,0.022953
worldwid,0.003280
write,0.002528
writer,0.013275
written,0.002560
wrote,0.002755
xixxx,0.009669
y,0.003443
yahad,0.009669
yakovenko,0.008622
yale,0.013262
yamna,0.008104
yankukovych,0.009669
yanukovych,0.100612
yaroslav,0.031530
year,0.014688
yekelchyk,0.009669
yenisei,0.008622
yulia,0.035060
yuri,0.005768
yurii,0.016208
yurkevich,0.009124
yushchenko,0.067974
zabarko,0.009669
zaporizhian,0.009362
zaporozhia,0.009669
zenon,0.007592
zone,0.003342
