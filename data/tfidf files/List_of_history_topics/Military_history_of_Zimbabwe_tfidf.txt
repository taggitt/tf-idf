aba,0.013254
abil,0.004927
abl,0.004523
abov,0.004584
accompani,0.006923
accord,0.007161
achiev,0.004505
action,0.008800
activ,0.010719
ad,0.004570
addit,0.007245
adept,0.011860
administr,0.010014
africa,0.087233
african,0.069042
africanis,0.017776
age,0.004484
ago,0.006939
aim,0.005113
air,0.022004
aircraft,0.007953
airdef,0.017077
airlin,0.027346
alli,0.006614
allianc,0.013394
alreadi,0.005216
american,0.004057
ancient,0.010376
anger,0.009347
angola,0.010862
anoth,0.013777
antiterrorist,0.015357
apart,0.006562
apartheid,0.011425
approx,0.011610
approxim,0.004880
april,0.005478
arabl,0.010954
arm,0.028549
armament,0.011116
armi,0.073915
armour,0.010774
arriv,0.028823
arrow,0.009194
artilleri,0.009788
assassin,0.008475
assault,0.009180
assert,0.006151
attack,0.057737
attackaircraft,0.018838
attempt,0.004084
attribut,0.005566
averag,0.005348
avoid,0.005348
axe,0.009797
backer,0.012906
bantuspeak,0.014429
base,0.009360
battalion,0.074544
battl,0.020289
bavenda,0.018838
bbp,0.016554
becam,0.015787
becaus,0.003250
bechuanaland,0.050397
becom,0.003456
befor,0.007375
began,0.021558
begin,0.008420
behest,0.012411
believ,0.004465
big,0.006538
black,0.034964
blame,0.008577
bloodi,0.010137
blunderbuss,0.018838
boer,0.074850
boldli,0.014603
border,0.012721
bow,0.010609
breakup,0.010371
brief,0.006843
briefli,0.008009
brigad,0.010832
bring,0.005367
britain,0.012166
british,0.115709
brutal,0.009413
bsacp,0.056516
bsap,0.075355
bulawayo,0.071106
burgher,0.014514
burnham,0.012609
bush,0.017813
buyerwil,0.018838
c,0.004132
came,0.014185
camp,0.039122
cape,0.018166
capit,0.004703
captur,0.023751
car,0.007641
carri,0.013986
casualti,0.028421
cattl,0.008470
cecil,0.022585
centenari,0.012090
center,0.004612
centuri,0.018568
cessna,0.015492
chang,0.003260
chao,0.008369
characterist,0.004984
charter,0.031119
chete,0.018838
chief,0.012044
chiefdom,0.012173
chimoio,0.018838
chimurenga,0.119539
chronicl,0.009108
chumerenga,0.037677
circumst,0.006643
citi,0.009989
civil,0.004960
civilian,0.015036
claim,0.008780
clandestin,0.023094
coast,0.006600
coin,0.005989
collaps,0.006371
collect,0.004218
colon,0.007673
coloni,0.035882
colonli,0.018838
column,0.032867
combat,0.007591
come,0.004136
commandgun,0.018838
commando,0.085212
commiss,0.005758
communisttrain,0.018838
compani,0.036537
complet,0.003994
complex,0.004232
compos,0.010974
comput,0.004792
conceiv,0.007594
concess,0.018295
conduct,0.004957
confer,0.005693
confin,0.007766
conflict,0.010774
confront,0.015981
congo,0.029610
conquer,0.007831
conscript,0.010407
consid,0.006746
consist,0.007742
consolid,0.007594
constabulari,0.014121
constitut,0.009134
contain,0.004149
continu,0.010514
contribut,0.004245
control,0.007686
convinc,0.015399
corpor,0.005675
corrupt,0.007101
count,0.006596
countri,0.030452
courteney,0.018838
cover,0.009524
crash,0.017205
creat,0.003538
creation,0.005265
credit,0.005597
crew,0.009096
cross,0.006313
crossbord,0.010969
crush,0.008849
cultur,0.008698
current,0.003703
custer,0.015956
dak,0.016554
dakota,0.012828
dare,0.011220
date,0.009668
dawn,0.009154
day,0.008729
dc,0.008009
death,0.015198
decemb,0.021232
decid,0.005637
decim,0.009649
declar,0.011173
decreas,0.005696
deepen,0.010395
deeper,0.008643
defenc,0.014082
democraci,0.006443
democrat,0.011335
depart,0.005071
depend,0.003816
deploy,0.007828
deriv,0.004398
descend,0.014002
descent,0.008298
describ,0.007120
desert,0.008225
design,0.004248
did,0.008587
die,0.016627
difaqan,0.017397
differ,0.003066
diminish,0.007831
dingo,0.015635
direct,0.003841
disarray,0.012828
disband,0.020108
discov,0.005093
diseas,0.006059
disrupt,0.007453
distract,0.010383
distribut,0.004772
district,0.006996
disturb,0.008122
divis,0.005013
domin,0.009519
drew,0.007972
driven,0.006734
drop,0.006348
drought,0.009443
drove,0.009369
durat,0.008238
dure,0.019978
dutch,0.007104
earli,0.017415
earlier,0.005145
earnest,0.010985
easi,0.007072
east,0.005354
econom,0.007622
economi,0.004755
effort,0.004822
elect,0.010272
elector,0.007471
eleph,0.009633
elimin,0.006022
elit,0.007732
emerg,0.013389
empir,0.034014
employ,0.004619
encroach,0.010731
end,0.022845
enemi,0.038916
ensu,0.008513
equip,0.006226
equival,0.005609
era,0.005502
erad,0.010126
erupt,0.008695
escap,0.013983
essexval,0.018838
establish,0.011149
estim,0.004900
europ,0.004722
european,0.030362
event,0.004531
eventu,0.004949
everincreas,0.011766
evid,0.004748
exclus,0.005949
exist,0.006925
exodu,0.010877
expans,0.005875
expatri,0.011066
explor,0.015918
express,0.004512
extent,0.005730
extern,0.002723
extrem,0.004995
faction,0.016450
fail,0.005235
fall,0.004753
famin,0.009325
far,0.004722
farm,0.013896
farmer,0.007371
fear,0.006534
featur,0.004709
februari,0.005657
feder,0.011089
ferment,0.010223
fewer,0.007385
fight,0.032671
fighter,0.009458
final,0.009008
firebal,0.016335
flight,0.015258
follow,0.011915
foment,0.012988
food,0.005359
footnot,0.009283
forc,0.082114
forefront,0.010954
forfeit,0.013073
form,0.022771
format,0.010158
fort,0.018361
fortif,0.010480
forward,0.006610
fought,0.007898
frantan,0.018838
frederick,0.025202
gain,0.004530
game,0.006390
gardner,0.011099
gave,0.010837
gener,0.008027
gerrit,0.014269
goal,0.005122
gokomer,0.018240
gold,0.019426
good,0.004140
govern,0.046034
gradual,0.011972
graft,0.012117
grant,0.005708
gravita,0.016137
great,0.008393
green,0.006404
ground,0.005418
group,0.016795
guard,0.007783
guerrilla,0.170392
guid,0.005265
gukurahundi,0.017077
gun,0.043718
hand,0.009228
harar,0.014121
harari,0.015357
harass,0.010126
head,0.004807
held,0.004402
helicopt,0.020253
hendrik,0.011311
hi,0.043405
higher,0.004427
highli,0.004902
hill,0.014815
histori,0.015508
historian,0.006274
hit,0.007709
hold,0.004485
hors,0.016088
hotchkiss,0.016137
hous,0.004995
howev,0.012088
hq,0.013982
huge,0.013850
hundr,0.016973
hunter,0.009435
hurrican,0.009945
ian,0.009194
imbezu,0.018838
immedi,0.021895
impembisi,0.018838
imperialist,0.011348
impi,0.016554
import,0.006352
incid,0.007368
includ,0.002585
incorpor,0.005113
increas,0.025066
increasingli,0.005489
incur,0.008930
independ,0.011474
indic,0.004580
indigen,0.007424
ineffect,0.009420
ineptitud,0.015112
infantri,0.050322
infiltr,0.010531
influenc,0.004101
influx,0.009608
ingubu,0.018838
inhabit,0.006473
insid,0.012491
intact,0.009544
intend,0.005764
intens,0.006184
intern,0.003263
interrel,0.009089
invad,0.007447
invas,0.027161
involv,0.003609
ironwork,0.053604
isol,0.006045
jameson,0.077683
januari,0.015943
jew,0.008754
john,0.004235
join,0.016616
joshua,0.020961
june,0.010850
jure,0.011150
kabila,0.014514
kalahari,0.014121
karanga,0.016335
kill,0.061471
king,0.023724
kingdom,0.024765
knobkerri,0.018838
known,0.037335
kraal,0.017077
kruger,0.014792
laager,0.071106
lack,0.004747
lancast,0.012201
lancecorpor,0.017776
land,0.028271
larg,0.013124
late,0.012931
later,0.029620
launch,0.012540
laurent,0.011909
law,0.003750
lay,0.007005
lb,0.010731
leader,0.027219
leander,0.047868
learn,0.005229
led,0.024108
lee,0.008466
left,0.009925
legal,0.005137
legitim,0.007732
lemba,0.017077
lesotho,0.012716
lexicon,0.011150
liber,0.016839
lieuten,0.009936
lifaqan,0.017397
light,0.020530
like,0.003325
limpopo,0.031912
linguist,0.007318
littl,0.004764
live,0.004025
livestock,0.008787
lobengula,0.109444
local,0.008432
locust,0.014121
longer,0.005134
lose,0.006547
lusaka,0.041557
magaliesberg,0.018838
main,0.003941
mainli,0.010336
major,0.029724
make,0.003165
makololo,0.016799
malayan,0.013401
man,0.005507
mandat,0.007756
mani,0.014068
maoist,0.012867
march,0.020648
marin,0.007126
maritz,0.017776
martinihenri,0.018240
marxist,0.008451
mashona,0.018838
mashonaland,0.127685
massacr,0.018153
massiv,0.006624
masvingo,0.018838
matabel,0.104385
matabeleland,0.170770
match,0.006620
matobo,0.018838
matter,0.004682
maxim,0.021239
mean,0.010284
media,0.005709
member,0.003971
men,0.017317
merg,0.007497
merger,0.009020
met,0.006365
metford,0.018838
mfecan,0.078948
mfengu,0.018240
mig,0.013401
migrant,0.009717
migrat,0.006651
mile,0.007235
milit,0.009726
militari,0.050358
militarist,0.012318
miner,0.006891
minor,0.028803
miss,0.007852
missil,0.019401
mlimo,0.075355
mm,0.009108
moazambiqu,0.018838
modern,0.018669
moffat,0.014792
monomotapa,0.017077
month,0.021999
moroko,0.018838
mosega,0.056516
mostli,0.021244
motto,0.011066
mount,0.014513
mountain,0.006697
movement,0.012877
mozambiqu,0.074639
mthwakazi,0.037677
mugab,0.068073
multiethn,0.012090
munhumutapa,0.017776
musket,0.011985
mutapa,0.033598
mwene,0.018838
mysteri,0.008216
mzilikazi,0.177766
napalmbomb,0.018838
narrowli,0.009675
nation,0.020696
nationalist,0.016142
nativ,0.032170
ndebel,0.297076
near,0.020473
nearbi,0.007661
neargenocid,0.017776
nearli,0.005421
need,0.007484
negoti,0.013031
neighbour,0.007604
new,0.008341
newli,0.006549
nguni,0.015112
nineteenth,0.007786
nkomo,0.049663
north,0.019770
novemb,0.005583
nucleu,0.016493
number,0.021799
nyasaland,0.030001
object,0.004255
obtain,0.004838
occur,0.007910
octob,0.016148
offens,0.008674
offer,0.004628
offic,0.010021
offici,0.009451
onc,0.004651
onli,0.014327
onward,0.007891
op,0.012349
open,0.008523
oper,0.043198
opposit,0.010244
order,0.003430
orderincouncil,0.015492
organis,0.005893
origin,0.017249
outpost,0.010847
outsid,0.004748
overthrow,0.018078
overview,0.006237
overwhelm,0.008346
paid,0.012694
pamw,0.018838
parti,0.014431
particip,0.004910
passeng,0.009262
patriot,0.027249
patrol,0.050070
patu,0.017077
paul,0.005411
peac,0.012006
penetr,0.008315
peopl,0.048049
perhap,0.005786
period,0.014795
perpetr,0.010923
personnel,0.015386
persuad,0.008996
pf,0.013730
pioneer,0.026011
pit,0.009860
place,0.014081
plagu,0.008965
platoon,0.013452
pod,0.012752
polic,0.055385
policeman,0.013852
policemen,0.012411
polici,0.009213
polit,0.015627
pongola,0.016137
poorli,0.008242
popul,0.008858
portug,0.008315
portugues,0.015848
pose,0.007582
posit,0.003689
potgiet,0.075355
power,0.010958
predominantli,0.007924
prepar,0.005789
present,0.007138
presentday,0.008017
presid,0.005162
presidenti,0.007299
pressur,0.010746
pretens,0.012117
pretoria,0.012988
prevent,0.005103
primarili,0.004879
princip,0.005576
probabl,0.005122
problem,0.003817
proclaim,0.008075
promin,0.011195
protect,0.004757
protector,0.008832
provid,0.003167
provinc,0.006598
pull,0.008191
push,0.013576
question,0.008965
raaf,0.014603
racialintegr,0.018838
raid,0.093493
rais,0.005265
ranger,0.025505
rank,0.024106
ravag,0.011099
reach,0.004600
reaction,0.005805
rebuild,0.009528
recognis,0.013979
reconnaiss,0.011220
recov,0.006968
recruit,0.008346
reduc,0.004288
refer,0.002263
reform,0.005537
regim,0.013193
regiment,0.081611
region,0.032431
regular,0.012404
reign,0.007828
relat,0.003052
reliev,0.009473
remain,0.007488
remnant,0.027155
remov,0.005226
renam,0.031438
reorgan,0.009466
repeat,0.006534
replac,0.009226
repress,0.008538
republ,0.037193
repuls,0.009994
requir,0.003563
reservist,0.014348
resist,0.011732
resolv,0.006575
respect,0.008610
respons,0.011779
rest,0.005303
result,0.015767
retali,0.009936
retir,0.007703
retreat,0.008743
return,0.004581
revolt,0.016484
revolutionari,0.014409
rhode,0.062229
rhodesia,0.149701
rhodesian,0.356984
rifl,0.010690
riflemen,0.015956
right,0.004062
rinderpest,0.014893
riot,0.009045
rise,0.013666
rival,0.014959
rivalri,0.018651
river,0.037136
rli,0.085385
robert,0.014933
rocket,0.009616
role,0.003965
roughli,0.006350
rout,0.006815
royal,0.011946
rudd,0.026508
ruin,0.009002
rule,0.041324
russel,0.007935
s,0.012843
sa,0.019949
safeti,0.007296
salisburi,0.026905
sam,0.010054
san,0.035946
satellit,0.007898
scale,0.005123
scarc,0.008669
scatter,0.008360
scene,0.008155
scheme,0.006813
scout,0.105484
second,0.037155
secur,0.009619
seek,0.005079
seiz,0.007783
seller,0.009045
selou,0.167993
sena,0.014121
senior,0.007503
sent,0.037649
septemb,0.005459
sergeant,0.012349
seri,0.004509
servic,0.004410
sesotho,0.016799
settlement,0.013213
settler,0.076668
seven,0.005940
sevenpound,0.018838
sever,0.009853
shaka,0.028101
shangani,0.056516
shona,0.226694
shoot,0.009600
short,0.004826
shortli,0.007046
shortliv,0.017789
shot,0.008804
shoulderfir,0.018838
sieg,0.009383
sinc,0.003150
sir,0.007111
site,0.010614
sixti,0.009584
size,0.014719
sizeabl,0.011789
skill,0.006069
skirmish,0.011406
skymast,0.017077
slave,0.007444
small,0.015421
smith,0.006675
social,0.004008
soldier,0.028676
someth,0.006057
sometim,0.012304
somewhat,0.013358
son,0.006032
sore,0.013559
sothotswana,0.051231
south,0.074009
southern,0.023390
southwestern,0.019666
soviet,0.013648
sovietsupport,0.017776
space,0.004857
spear,0.011256
spearmen,0.016799
special,0.007880
spiritualreligi,0.017397
split,0.006475
squadron,0.010676
stage,0.005502
stand,0.005921
starr,0.041557
start,0.019941
state,0.013783
stay,0.006896
steadili,0.008311
step,0.005342
stone,0.007036
strength,0.006390
strong,0.009156
stronghold,0.010256
structur,0.011043
struggl,0.013209
subdu,0.010179
subinspector,0.017077
subsequ,0.009833
subunit,0.010582
succeed,0.006441
success,0.008356
suppli,0.005136
support,0.018730
suppress,0.007215
surviv,0.010868
tanzania,0.021165
tax,0.005921
team,0.006579
tembu,0.018838
templ,0.008328
term,0.008931
territori,0.022058
terroriststerror,0.018838
th,0.018063
thabanchu,0.018838
themselv,0.009346
theori,0.003693
thi,0.037705
thousand,0.005556
thrasher,0.016137
threeway,0.014121
thu,0.003683
thwa,0.018838
time,0.025088
tire,0.011099
togeth,0.017378
took,0.044297
torn,0.011589
town,0.019410
track,0.006971
tracker,0.013505
trade,0.013673
trader,0.015512
tradit,0.003967
train,0.021762
translat,0.005424
transvaal,0.128428
travel,0.005563
treati,0.006579
trek,0.011367
trekboer,0.016554
trekker,0.016799
tri,0.004959
tribe,0.015512
trigger,0.007664
troop,0.028446
trooper,0.027580
troopi,0.037677
tswana,0.030001
tugela,0.017077
turn,0.004355
twenti,0.007749
type,0.003788
udi,0.015357
uitland,0.017776
unabl,0.006547
unarm,0.012379
underw,0.009141
unesco,0.008557
unilater,0.009879
union,0.009363
unit,0.025422
unrival,0.015492
upris,0.017498
urban,0.013216
urozwi,0.018838
use,0.007723
usher,0.009649
usual,0.007583
vaal,0.034154
valid,0.006016
vast,0.025306
vengeanc,0.013117
veri,0.003598
vest,0.008263
vicin,0.010169
victori,0.014593
victoria,0.018525
vii,0.009584
villag,0.007199
violent,0.007779
vision,0.006824
visit,0.012660
vogu,0.012906
volunt,0.008685
voortrekk,0.050397
wa,0.126583
wagon,0.012063
walk,0.008047
war,0.111892
waror,0.018838
warozwi,0.018838
warrior,0.028491
waterberg,0.016335
wave,0.006353
weak,0.006258
weaken,0.007891
weapon,0.006996
week,0.013244
weekend,0.011675
west,0.005409
whilst,0.008138
white,0.101692
whiterul,0.034795
widespread,0.006053
willoughbi,0.014429
wing,0.008577
withdrawn,0.009761
withdrew,0.008854
worker,0.005865
world,0.006388
wound,0.008643
year,0.022258
yemen,0.011066
zambia,0.046105
zambian,0.014603
zanla,0.018240
zanu,0.112959
zanupatriot,0.018838
zanupf,0.046906
zapu,0.081676
zeerust,0.018838
zimbabw,0.257545
zimbabwean,0.013452
zimbabwecompris,0.018838
zipra,0.102462
zoutpansberg,0.017776
zulu,0.051011
