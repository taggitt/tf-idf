abandon,0.021509
abdic,0.004336
abil,0.005941
abl,0.010908
absent,0.003507
abstin,0.005308
absurd,0.004136
abus,0.012384
accept,0.016263
access,0.007966
acclaim,0.013026
account,0.003390
accultur,0.005220
accumul,0.005522
achiev,0.009053
acknowledg,0.011101
acquiesc,0.005171
acquir,0.002344
act,0.003290
action,0.005305
activ,0.001436
actual,0.001842
ad,0.011021
addit,0.001456
adequ,0.018116
administr,0.016099
admit,0.012140
adopt,0.003972
adrian,0.009048
adrianopl,0.017399
adrianopleedit,0.007571
advanc,0.001777
advantag,0.002234
advic,0.009397
advis,0.002909
aegidiu,0.037859
aemilia,0.007571
aetiu,0.114786
aetiuss,0.007571
afford,0.003090
africa,0.046748
africaedit,0.007571
aftermath,0.007173
afterward,0.006340
age,0.005407
agent,0.002454
aggrav,0.004666
aggress,0.006414
agre,0.008680
agreement,0.017576
agricultur,0.004278
alamann,0.007571
alamannu,0.007571
alan,0.024410
alar,0.275302
alexand,0.002757
alfldi,0.007331
alien,0.003372
alik,0.003878
aliv,0.003596
allevi,0.003842
alli,0.005316
allia,0.006863
allianc,0.005383
allot,0.004371
allow,0.017176
alp,0.013948
alpin,0.004692
alreadi,0.004193
alsoedit,0.004531
altar,0.004796
altern,0.001849
altogeth,0.003568
ambit,0.003754
ambiti,0.007451
ambros,0.010312
ammianu,0.013131
analyz,0.002419
anatolia,0.008661
ancestor,0.003093
ancient,0.006256
andrew,0.002923
ani,0.015222
annex,0.003274
annual,0.004422
anonymu,0.006992
anoth,0.009690
answer,0.005125
anthemiu,0.043990
anticip,0.003201
antiqu,0.026806
antonin,0.005518
anxiou,0.004701
anyon,0.003137
anyth,0.005689
anywher,0.003523
apart,0.002637
apoplexi,0.006413
appear,0.001667
applic,0.001718
appoint,0.011830
approach,0.001656
approachesedit,0.007144
appropri,0.004614
aquitain,0.005869
aquitania,0.006863
arbitrari,0.003108
arbogast,0.034318
arcadiu,0.041955
arcadiuss,0.007571
archaeolog,0.012327
archaeologicallybas,0.007571
archetyp,0.004217
architectur,0.002714
area,0.017154
arel,0.030287
aremorica,0.015143
argu,0.001914
argument,0.002418
aris,0.002348
aristocraci,0.008115
aristocrat,0.027795
arm,0.011474
armi,0.155970
armor,0.004302
armorica,0.029325
arnaldo,0.005767
arnold,0.003658
arrang,0.004693
arrest,0.003163
arriv,0.013901
arrow,0.003695
art,0.002201
arvandu,0.007571
ascend,0.003771
asd,0.012453
asia,0.009885
asid,0.006565
ask,0.007138
aspir,0.003618
assassin,0.003406
assault,0.011070
assembl,0.006740
assert,0.004945
asset,0.002660
assimil,0.003756
assist,0.002146
associ,0.001406
ataulf,0.075719
atlant,0.003050
attack,0.037129
attalu,0.036657
attaluss,0.015143
attempt,0.013134
attend,0.002746
attent,0.002347
attila,0.063481
attrit,0.004927
august,0.010935
augusta,0.005619
augustin,0.003865
augustu,0.051105
augustulu,0.025137
aurelian,0.012058
author,0.017826
authorit,0.008034
auxiliari,0.004061
avail,0.005278
averag,0.002149
averil,0.006565
avitu,0.028579
avituss,0.007571
avoid,0.004299
await,0.004296
awar,0.005208
away,0.008770
badli,0.003912
baetica,0.006653
bagauda,0.065983
baggag,0.005327
balanc,0.002278
balkan,0.012248
balkansedit,0.007571
bandit,0.015563
bank,0.006244
banquet,0.005308
bar,0.003253
barbar,0.004963
barbara,0.003810
barbarian,0.221335
barcelona,0.004502
bare,0.003545
base,0.005016
basi,0.003585
basil,0.004365
basilica,0.004963
basin,0.003420
batavi,0.007331
battl,0.038055
bce,0.003184
beaten,0.004428
becam,0.026968
becaus,0.003919
becom,0.011114
bedford,0.005472
befor,0.014822
began,0.013863
begin,0.001692
behalf,0.003201
belief,0.002323
belknap,0.005054
benefit,0.002106
besid,0.002969
besieg,0.017164
betroth,0.005906
better,0.003914
bi,0.004070
billet,0.013504
birthday,0.004258
bishop,0.021312
bitterli,0.005237
blackwel,0.003628
blame,0.006895
blockad,0.003974
bloodi,0.008148
blot,0.005054
boast,0.004365
boat,0.003459
bodi,0.003483
bon,0.005327
bonifac,0.035003
bonni,0.005366
bononia,0.007144
book,0.006597
border,0.005113
boulogn,0.006122
boundari,0.007268
bow,0.004264
bowersock,0.014289
bowman,0.005386
break,0.004784
breath,0.003731
bridg,0.002867
brief,0.008252
briefli,0.003219
bring,0.004315
britannia,0.064635
british,0.005813
briton,0.004767
brm,0.006413
broke,0.011904
brother,0.002910
brotherinlaw,0.009902
brought,0.008557
brown,0.005889
bryan,0.004584
build,0.005509
built,0.002163
burden,0.003312
burdigala,0.007571
bureaucrat,0.003845
burgundian,0.051081
buri,0.003431
burk,0.004365
burn,0.017850
busi,0.005869
bypass,0.004024
byzantin,0.007140
c,0.001660
ca,0.003192
caduta,0.007331
caesar,0.004302
california,0.002662
cambridg,0.009208
came,0.011402
cameron,0.004502
campaign,0.028701
campania,0.006485
canna,0.005945
cannib,0.004729
capabl,0.002288
cape,0.003650
capit,0.003780
captiv,0.003722
captur,0.011933
care,0.004367
carri,0.003747
carthag,0.009477
carthageedit,0.007571
carthago,0.007331
case,0.002906
cashpoor,0.007571
cassiu,0.005906
castinu,0.007571
casualti,0.007615
catalaunian,0.007571
catastroph,0.003449
catch,0.003499
caus,0.007757
cavalri,0.008507
ce,0.006369
ceas,0.014680
cede,0.007440
celebr,0.008987
center,0.001853
centr,0.007170
central,0.009776
centuri,0.031344
centurion,0.006074
centuryedit,0.006172
ceremoni,0.003208
challeng,0.002076
chang,0.002620
chapter,0.002709
charact,0.002402
character,0.002204
charg,0.006729
charlemagn,0.004649
chase,0.003952
chastiti,0.005833
cheap,0.003601
check,0.006027
chicken,0.004131
chief,0.002420
chieftain,0.004657
child,0.002791
children,0.009562
china,0.002233
choir,0.005406
christ,0.003709
christian,0.051811
christianityedit,0.006863
church,0.005252
cimbrian,0.007331
citi,0.054201
citizen,0.004757
civil,0.035891
civilian,0.006043
civit,0.014289
claim,0.014116
claimant,0.009553
classic,0.007682
clear,0.004327
cliqu,0.005068
close,0.001555
cloth,0.003038
coast,0.002653
cockerel,0.006284
coconspir,0.006565
cohes,0.003771
coin,0.002407
collaps,0.015364
collect,0.006782
cologn,0.004796
coloni,0.002403
color,0.002895
combin,0.003315
come,0.008313
command,0.026828
commanderinchief,0.008074
commit,0.002436
commodu,0.006226
commonplac,0.004149
commun,0.001520
compar,0.001719
comparison,0.002437
compel,0.003633
compens,0.002938
compet,0.004793
competit,0.002285
complain,0.007549
complet,0.001605
complex,0.005103
complianc,0.003731
comprehend,0.004117
comprehens,0.005121
comrad,0.005254
conced,0.008486
concentr,0.006825
concept,0.003244
concess,0.007353
condemn,0.003274
confid,0.002973
confirm,0.002506
confisc,0.008165
conflict,0.002165
conform,0.003208
connect,0.001914
conniv,0.006565
connolli,0.005799
conquer,0.003147
conquest,0.006264
consensu,0.002899
consequ,0.001976
consid,0.001355
consider,0.001953
consolid,0.003052
conspir,0.009367
conspiraci,0.008262
constant,0.004622
constantin,0.067319
constantinopl,0.044230
constantiu,0.092592
constantiuss,0.022715
constitut,0.001835
construct,0.003600
consul,0.015357
contemporari,0.002274
contest,0.003058
context,0.004090
conting,0.003376
continu,0.032399
contradictori,0.004104
contrast,0.001963
contribut,0.006825
contributor,0.003400
control,0.058696
controversi,0.004581
conveni,0.006208
convent,0.004502
convuls,0.005308
coordin,0.004929
cornel,0.003676
corps,0.004538
corrupt,0.028543
cost,0.008176
council,0.012719
count,0.002651
counterattack,0.009333
countrysid,0.011432
coup,0.006332
coupl,0.002690
courag,0.004336
court,0.055128
courtier,0.005366
cousin,0.003823
cover,0.001914
cow,0.004009
creatur,0.003545
cri,0.004291
crime,0.005768
crise,0.007173
crisi,0.011995
crop,0.002874
cross,0.012687
cruel,0.004632
cruelti,0.004710
crumbl,0.004806
crush,0.003556
ctesiphon,0.006074
cuirass,0.032066
cult,0.003798
cultur,0.027969
culturallyroman,0.007571
cursu,0.005833
custom,0.004701
cut,0.009872
cyprian,0.006172
d,0.001882
dacia,0.011351
dalmatia,0.038148
damasu,0.006413
danger,0.005461
danub,0.027897
date,0.003886
daughter,0.012763
david,0.002065
day,0.008771
dc,0.003219
dead,0.002914
deal,0.003745
death,0.024434
debat,0.002164
decad,0.006340
decay,0.003196
decis,0.005880
decisionmak,0.003351
declaim,0.006752
declar,0.017964
declin,0.033110
decreas,0.002289
deem,0.003146
deepli,0.003282
defeat,0.068767
defect,0.003431
defenc,0.022640
defend,0.002601
defenseless,0.006284
degre,0.005711
delug,0.011186
demand,0.010378
demandt,0.007571
demis,0.004109
demor,0.005495
deni,0.002695
depend,0.001534
depos,0.011470
depth,0.003052
descent,0.003335
describ,0.011447
descript,0.002125
desert,0.006612
desper,0.016436
despit,0.019713
despot,0.004666
destitut,0.005366
destroy,0.017018
destruct,0.016509
detach,0.007567
deterior,0.003423
determin,0.003229
dethron,0.005593
deu,0.005068
devast,0.009902
develop,0.008139
di,0.003618
did,0.025887
die,0.033414
differ,0.004929
difficult,0.002027
difficulti,0.002510
dignitatum,0.007571
diminish,0.003147
diminut,0.004904
dio,0.005254
dioces,0.078205
diocletian,0.036313
diplomaci,0.003912
direct,0.006175
directli,0.003736
disadvantag,0.003447
disagre,0.003342
disappoint,0.003989
disast,0.006423
disciplin,0.019280
discord,0.004915
discordedit,0.007571
discours,0.003183
discov,0.002047
diseas,0.002435
disembark,0.005327
disintegr,0.003801
disord,0.005993
dispens,0.004045
displac,0.002983
display,0.007926
dispos,0.006879
disrupt,0.002995
dissens,0.005406
dissolv,0.002887
distant,0.003231
distract,0.008346
distrust,0.004280
disunit,0.012453
divers,0.002236
divert,0.004082
divid,0.005530
divis,0.002014
divisionedit,0.007571
divisionsedit,0.006992
divorc,0.003905
doctrin,0.002788
domain,0.004541
domin,0.009565
dominion,0.003970
dominu,0.006284
dowri,0.005472
draft,0.003000
dramat,0.002625
dread,0.010002
drill,0.003852
drive,0.004995
drove,0.011297
drown,0.004632
dure,0.009368
duti,0.008299
dynasti,0.003023
eadi,0.006863
eagerli,0.005290
ear,0.007429
earli,0.008399
earn,0.007892
earthli,0.004502
easili,0.007193
east,0.032280
eastedit,0.007144
eastern,0.067373
eastward,0.004285
eaten,0.004222
ecclesiast,0.004168
econom,0.007658
economi,0.011469
ed,0.003918
edit,0.006572
edward,0.007638
effect,0.044872
effici,0.002191
effort,0.001938
egypt,0.002955
eighteenth,0.003577
elabor,0.005825
eldest,0.004632
elect,0.002064
elev,0.021860
eleventh,0.004348
elit,0.006216
els,0.002882
embitt,0.006122
emerg,0.001793
emperor,0.166697
emphas,0.007369
emphasi,0.002538
empir,0.162105
empirea,0.007571
empireedit,0.007571
employ,0.001856
empress,0.004657
en,0.003439
encourag,0.004601
end,0.026016
endem,0.003899
endow,0.003545
enemi,0.037540
energet,0.003792
enforc,0.007872
engag,0.008866
english,0.001949
engross,0.006346
enlist,0.004384
enmiti,0.005254
enorm,0.006029
ensign,0.005518
ensu,0.003421
ensur,0.004599
enter,0.002046
entir,0.005446
entitl,0.002889
entourag,0.005705
enumer,0.004045
envoy,0.004159
equal,0.001906
equat,0.002327
equip,0.002502
equival,0.004509
era,0.002211
escap,0.002810
escort,0.004729
especi,0.001625
essay,0.005424
essenti,0.005837
est,0.003888
establish,0.013443
estat,0.005959
et,0.002406
eudocia,0.021434
eugeniu,0.011351
eunuch,0.016485
europ,0.011387
european,0.001743
eutropiu,0.021434
event,0.003643
eventu,0.001989
everi,0.003502
everywher,0.003635
evict,0.009697
evid,0.003817
exact,0.002670
excel,0.003027
exchang,0.001978
excus,0.004641
execut,0.027563
exempt,0.003638
exercis,0.007214
exhaust,0.003272
exil,0.006393
exist,0.004175
expand,0.001979
expect,0.001923
expedi,0.004729
expel,0.006749
expenditur,0.003094
expens,0.017499
exploit,0.002605
export,0.002567
expos,0.005607
express,0.001813
extend,0.001846
extent,0.006909
extermin,0.009169
extern,0.001094
extort,0.043262
extra,0.003205
extract,0.002694
extraordinari,0.003653
extrem,0.002007
face,0.010504
factor,0.001774
fail,0.012626
failur,0.009741
failureedit,0.007571
fall,0.043943
fallen,0.006801
famili,0.011628
familiar,0.005923
famin,0.007496
famous,0.007271
far,0.007591
farm,0.008377
father,0.002460
fatherinlaw,0.005472
favorit,0.004126
favour,0.008257
fear,0.002626
fed,0.003745
feder,0.011142
feed,0.003213
felix,0.016366
fell,0.013516
felt,0.002954
feroc,0.005675
feroci,0.005327
fertil,0.002933
fervour,0.006122
fetch,0.005096
fever,0.003989
fewer,0.002968
fiction,0.002985
field,0.015285
fifth,0.002980
fifthcenturi,0.006413
fight,0.013131
figur,0.004145
filter,0.003488
final,0.007241
financ,0.006653
financi,0.015985
finish,0.003220
fireship,0.007331
fiveday,0.006074
flatli,0.005647
flatteri,0.005906
flaviu,0.017958
fled,0.013550
flee,0.025124
fleet,0.026924
fli,0.003124
flow,0.002204
fold,0.003568
folli,0.005187
follow,0.011972
food,0.012924
foot,0.006503
footarch,0.007571
forbad,0.004553
forbid,0.004024
forbidden,0.003700
forc,0.055506
foreign,0.001948
form,0.011440
formal,0.001872
formid,0.008638
formul,0.004793
forti,0.003736
fortif,0.008424
fortifi,0.008191
fortress,0.004091
fortun,0.003395
fought,0.009524
foulsmel,0.006346
foundat,0.001951
fourth,0.018280
fowl,0.005054
frank,0.017584
frankish,0.004975
freedom,0.007088
fresh,0.010085
fright,0.005495
frigidu,0.014662
frontier,0.032992
frugal,0.005054
fulli,0.002256
fullscal,0.004359
function,0.003064
futur,0.001822
g,0.002024
gaddi,0.006172
gain,0.007283
gaina,0.037859
galinski,0.007144
galla,0.053229
gallic,0.027360
gallienu,0.006653
galloroman,0.006226
game,0.002568
garrison,0.030340
gate,0.003623
gather,0.002609
gaul,0.174944
gauledit,0.007571
gave,0.010889
gener,0.030113
generidu,0.015143
genser,0.045431
geograph,0.002434
german,0.019023
germani,0.002253
germania,0.005619
gerontiu,0.022715
gerontiuss,0.007571
gibbon,0.032202
gibraltar,0.009077
gieben,0.007571
gift,0.003378
given,0.006004
glen,0.010002
glimps,0.004939
glyceriu,0.022715
god,0.010478
gold,0.007808
goldrich,0.007144
goldsworthi,0.012826
gone,0.003321
good,0.001664
goth,0.113097
gothic,0.058085
govern,0.011386
governor,0.005494
grabar,0.006992
grain,0.019511
grande,0.007331
grant,0.016061
gratian,0.038078
great,0.015181
greater,0.003770
greatest,0.005267
greatli,0.004902
greec,0.005658
greed,0.004632
greedi,0.005254
greek,0.002119
greenhil,0.006992
grew,0.002501
groan,0.006346
gross,0.002926
ground,0.002177
group,0.013501
grow,0.003838
grysztar,0.007571
guard,0.003128
guardian,0.011094
gundobad,0.027970
guy,0.004065
gza,0.006122
h,0.008316
ha,0.010978
habit,0.003173
haemorrhag,0.006284
half,0.012361
halsal,0.006565
halt,0.006820
han,0.005982
hand,0.014836
haphazard,0.005567
happen,0.004731
happi,0.003309
hardback,0.011239
harmoni,0.003490
harper,0.003855
harvard,0.005848
hasta,0.006653
hastili,0.005140
hatch,0.004608
head,0.005796
health,0.002086
hear,0.009468
heather,0.005237
heaven,0.003584
heavi,0.010347
heavili,0.009651
height,0.005968
heir,0.003829
held,0.005308
hellespont,0.006172
helmet,0.025628
help,0.014747
helpless,0.004951
hen,0.005156
henc,0.002335
henceforward,0.006122
henri,0.002412
heraclianu,0.015143
hereditari,0.003582
heret,0.012991
heroic,0.004384
heruli,0.006565
hi,0.187880
higher,0.001779
hinterland,0.004719
hispania,0.108139
histor,0.003362
histori,0.007479
historian,0.007566
historiographi,0.007597
hodg,0.004632
hold,0.007211
holi,0.003168
homag,0.004939
home,0.010686
honor,0.003096
honoria,0.022715
honoriu,0.177541
honoriuss,0.053003
honorum,0.006565
honour,0.003529
hope,0.005096
hord,0.005220
hostag,0.004441
hostil,0.009312
hous,0.002007
household,0.008932
howev,0.010932
hsia,0.006992
httpswwwgutenbergorgfileshhhtm,0.007571
hudson,0.004280
huge,0.008350
humbl,0.004767
humili,0.009232
hun,0.081892
hundr,0.006822
hunedit,0.007571
huner,0.030287
hungarian,0.003858
hunnic,0.037036
hunt,0.006041
idea,0.005039
ident,0.002143
identifi,0.003618
ideolog,0.002827
ii,0.015670
iii,0.014311
illyricum,0.025943
imag,0.002304
immedi,0.008800
immens,0.003596
immin,0.004302
immun,0.003276
impair,0.003888
imperi,0.084555
impero,0.006992
implac,0.005869
impli,0.002326
import,0.002553
imposs,0.002530
imprint,0.004342
inadequ,0.003554
incap,0.008066
incent,0.003116
inclin,0.003703
includ,0.015584
inclus,0.003083
incom,0.006877
incompet,0.004415
incorrupt,0.005906
increas,0.011514
increasingli,0.006619
inde,0.005089
indecis,0.005203
independ,0.007686
index,0.002377
indiana,0.003780
indic,0.007364
indict,0.004796
indigen,0.008952
indisciplin,0.007331
individu,0.004631
induc,0.002963
ineffect,0.018932
inequ,0.003149
infant,0.003596
infantri,0.008090
inferior,0.003643
infest,0.005027
inflat,0.005637
inflict,0.008148
influenc,0.006594
influenti,0.002407
influx,0.003861
inform,0.002975
inhabit,0.002601
inherit,0.002775
initi,0.003252
injustic,0.004353
innoc,0.003955
inquir,0.004409
inscriptionsand,0.007571
insecur,0.008593
insignia,0.005290
insist,0.006107
insofar,0.004145
inspir,0.002476
instabl,0.003236
instal,0.011814
instead,0.010518
institut,0.003167
insult,0.004468
intact,0.003836
intend,0.004633
intent,0.002518
interact,0.001867
intern,0.003934
interpret,0.001996
interregnum,0.005647
interven,0.006432
intoler,0.008719
intric,0.004163
intrigu,0.008528
introduc,0.001755
invad,0.044902
invas,0.032750
invasionsedit,0.007571
invit,0.003084
involv,0.002901
iron,0.005361
irredeem,0.006122
irrevers,0.004131
irrupt,0.006485
isauria,0.007571
isaurian,0.006863
isbn,0.037155
island,0.004436
issu,0.001606
ista,0.007144
italia,0.233409
italiaedit,0.007571
italian,0.008425
italiana,0.004963
iv,0.003248
j,0.005638
jame,0.002110
januari,0.002135
jerom,0.004192
jewish,0.003102
joann,0.021160
john,0.006809
join,0.008904
joint,0.002635
jointli,0.003610
jone,0.003323
journey,0.003478
jovianu,0.007571
jovinu,0.021994
jr,0.003179
judaea,0.006346
judgement,0.003783
judici,0.002873
juli,0.002161
julianu,0.033760
juliu,0.026654
junior,0.003944
just,0.003490
justic,0.002529
justinian,0.009458
karl,0.002814
keeper,0.004881
kept,0.005393
key,0.001904
kidnap,0.004248
kill,0.014824
kind,0.001993
king,0.021454
kingdom,0.011944
klav,0.007144
knew,0.003525
know,0.002267
known,0.003751
kohlhamm,0.006992
kokhba,0.006122
kurdistan,0.005449
kyle,0.005449
la,0.002497
labelsedit,0.007571
lack,0.007632
laeti,0.007331
laetu,0.007331
land,0.020832
landown,0.011889
languag,0.003761
larg,0.023737
larger,0.003950
largescal,0.002915
late,0.027719
later,0.025299
latest,0.003342
launch,0.010080
lavish,0.009951
law,0.009044
lay,0.002815
leader,0.013128
leadership,0.005429
leav,0.019071
led,0.017764
left,0.011968
legacyedit,0.006863
legal,0.004129
legend,0.003525
legion,0.008830
legisl,0.002290
legitim,0.006216
legitimaci,0.022978
leo,0.007490
lessen,0.003978
let,0.002803
letki,0.007571
levant,0.004217
level,0.005985
levi,0.003516
liabl,0.004371
libiu,0.007571
licenti,0.005869
life,0.012868
liguria,0.006565
limit,0.004584
line,0.003581
list,0.001391
liter,0.005754
literaci,0.003671
literatur,0.004471
litoriu,0.007571
littl,0.009575
live,0.004853
loath,0.005735
local,0.020336
logist,0.007221
loir,0.006226
long,0.008123
longer,0.002063
longliv,0.004859
longstand,0.003615
longterm,0.005266
look,0.004131
lord,0.002961
loss,0.032827
lost,0.028267
lot,0.003081
lower,0.005546
loyal,0.007628
loyalti,0.007251
lucr,0.004126
lugdunum,0.007331
luxuri,0.003754
lydian,0.005906
lynn,0.004232
m,0.003578
macedonia,0.008719
macgeorg,0.007571
machin,0.002415
macmullen,0.006752
maghreb,0.005096
magist,0.072350
magistr,0.003930
magnu,0.004365
main,0.006336
mainli,0.002077
maintain,0.013957
mainten,0.003001
majesti,0.004371
major,0.006637
majorian,0.068147
make,0.007634
man,0.006641
manag,0.003583
mani,0.015832
manli,0.005735
manufactur,0.002386
maraud,0.011294
marcellinu,0.044424
march,0.020748
maria,0.007396
mark,0.004017
marker,0.011546
marri,0.018819
marriag,0.006326
marshland,0.005799
martin,0.005296
martindal,0.007144
martyrdom,0.005567
mass,0.004103
massacr,0.010944
master,0.005340
masterslav,0.006226
materi,0.003506
materna,0.007571
matthew,0.007256
matur,0.002911
mauretania,0.011813
maximu,0.036905
maximuss,0.007571
mean,0.001377
meanwhil,0.009076
mediev,0.005337
mediolanum,0.007571
mediterranean,0.029455
meet,0.004048
member,0.001596
membership,0.002755
men,0.037121
menac,0.004827
mental,0.002707
mention,0.004844
mercenari,0.012840
merci,0.008661
merit,0.003570
mesopotamia,0.011414
messag,0.003109
messeng,0.003967
met,0.007675
metal,0.007812
method,0.003099
michael,0.002302
michigan,0.007221
middl,0.004125
migrat,0.005346
militari,0.054649
militum,0.078594
millennium,0.003122
milman,0.006413
minist,0.004514
minor,0.009261
misfortun,0.005110
mistrust,0.004870
mob,0.009092
mobil,0.005315
mode,0.005633
moder,0.002866
modern,0.006003
modest,0.014613
modii,0.007571
moham,0.004070
moment,0.002846
momigliano,0.006565
mommsen,0.006172
momyllu,0.007571
money,0.013416
monk,0.003700
month,0.015474
mortal,0.013376
mostli,0.006404
mother,0.011533
multipl,0.001924
multipli,0.003225
multitud,0.003948
mundan,0.004649
murder,0.041309
muslim,0.005596
muster,0.016220
mutil,0.010002
mutual,0.010556
nake,0.003993
namatianu,0.007331
napoleonv,0.007571
narbo,0.022715
nauseou,0.007144
naval,0.003156
navi,0.008983
near,0.008228
nearextermin,0.007571
nearli,0.002179
necess,0.003135
necessari,0.001993
need,0.010528
neglig,0.007445
negoti,0.018332
nel,0.005675
nephew,0.004187
nepo,0.045401
network,0.002091
new,0.016764
news,0.010923
nibelungenli,0.006485
nicasi,0.007571
nicen,0.011294
niec,0.005110
nisibi,0.012692
nobel,0.002912
nobl,0.003253
noblemen,0.005366
noiseless,0.006653
nomin,0.015668
non,0.003606
nonchristian,0.009655
noncombat,0.005619
nonorthodox,0.006565
nonroman,0.006284
noricum,0.020591
normal,0.003988
north,0.009933
northeast,0.003563
northeastern,0.003865
northern,0.016373
northwestern,0.003798
nose,0.004197
notabl,0.001919
note,0.001442
notesedit,0.005140
notion,0.002367
notitia,0.006863
notwithstand,0.004441
nova,0.003771
number,0.013768
numer,0.001853
numidia,0.024488
nun,0.004738
nutrit,0.003594
obedi,0.004359
object,0.001710
oblig,0.008288
obscur,0.003465
obsequi,0.006752
observ,0.004953
obsess,0.004390
obtain,0.001944
obviou,0.003160
occasion,0.011289
occupi,0.002508
octob,0.002163
odoac,0.103264
odor,0.004719
offer,0.014884
offic,0.018125
offici,0.028492
officiorum,0.007571
oleg,0.005495
olybriu,0.015143
olymp,0.003777
olympiu,0.030287
onc,0.001869
onethird,0.003801
ongo,0.002737
onli,0.026489
onward,0.012687
open,0.001712
openli,0.003789
oper,0.006313
opportun,0.007089
oppos,0.002090
opposit,0.002058
oppress,0.011070
order,0.009651
orest,0.037706
organ,0.002729
organis,0.002368
origin,0.006932
orion,0.004951
ornament,0.008375
orthodoxi,0.008385
ourselv,0.004041
outbreak,0.003404
outdat,0.004441
outli,0.004827
outnumb,0.004154
outsid,0.007634
overal,0.002289
overcam,0.004701
overcom,0.003072
overthrew,0.004222
overtur,0.005327
overwhelm,0.006709
owicim,0.007571
oxford,0.009403
pact,0.007513
pactio,0.007571
pagan,0.057685
page,0.002452
paid,0.007653
palmyren,0.006565
pan,0.004136
panegyrist,0.007144
pannonia,0.022702
paperback,0.008424
partial,0.018412
partli,0.002690
pass,0.011662
passag,0.002980
past,0.001984
patrician,0.016780
patronag,0.008148
pattern,0.002134
paulinu,0.006284
pavia,0.011037
pawel,0.006284
pax,0.005096
pay,0.021022
payment,0.005393
payoff,0.004313
payrol,0.004531
peac,0.024129
peacetim,0.004806
peak,0.002745
peasantri,0.009367
pellaeu,0.007571
penal,0.004197
penni,0.004546
pension,0.003476
peopl,0.009656
perceiv,0.005032
perhap,0.009303
period,0.013379
perish,0.017056
perman,0.002424
permit,0.002548
persecut,0.007212
persia,0.003725
persian,0.006287
person,0.019646
persuad,0.007231
peter,0.011407
petroniu,0.012971
phoenix,0.004524
physic,0.001637
piacenza,0.006653
pike,0.005272
piotr,0.005767
piou,0.009926
pirat,0.007971
pirenn,0.017608
piti,0.005308
place,0.005659
placentia,0.014289
placidia,0.068147
plagu,0.010810
plain,0.006539
plan,0.009474
play,0.001806
plea,0.009315
plead,0.004806
plenti,0.003934
plot,0.003066
plu,0.002814
plunder,0.021483
pochia,0.007571
poem,0.007385
point,0.005805
poison,0.003497
polem,0.004776
polici,0.007406
polit,0.021984
politi,0.007737
pollentia,0.007571
polyphon,0.006346
polytheist,0.004817
pompeia,0.007571
pontifex,0.005833
pool,0.003276
poor,0.004947
pope,0.010556
popul,0.012461
port,0.005654
posit,0.013345
possess,0.004496
possibl,0.010243
possiblybarbarian,0.007571
post,0.004856
postclass,0.005082
postpon,0.004074
potent,0.009005
potenti,0.001857
poultri,0.004441
pound,0.003596
power,0.044047
poweredit,0.007144
practic,0.004605
praesentali,0.007571
praetorian,0.006346
preced,0.002718
preciou,0.011297
predecessor,0.003128
predominantli,0.003184
prefect,0.004827
prefer,0.004394
prepar,0.011634
preroman,0.006172
presenc,0.002169
present,0.001434
press,0.028706
pressur,0.002159
prestigi,0.004078
presum,0.003202
previou,0.006490
previous,0.002187
price,0.002123
princ,0.009288
princess,0.020988
princip,0.002241
principl,0.001722
priscu,0.006992
prison,0.003045
privat,0.012343
privileg,0.003274
prizewin,0.004649
probabl,0.020587
problem,0.006137
proceed,0.005251
process,0.010850
proclaim,0.019473
procopiu,0.005906
produc,0.004478
profession,0.004649
project,0.001775
promis,0.008087
promot,0.004093
prompt,0.006281
propagan,0.007571
propaganda,0.003836
properti,0.005211
proscript,0.005705
prosopographi,0.006863
prospect,0.003048
prosper,0.014829
protect,0.011473
protector,0.003549
protest,0.002666
prove,0.004428
proven,0.003030
provid,0.003818
provinc,0.039783
provis,0.005588
provok,0.003671
public,0.020228
publish,0.001592
pulmonari,0.004776
puppet,0.025713
purchas,0.004880
purpl,0.004409
pursu,0.002609
qualif,0.003645
quantifi,0.003223
quarrel,0.009333
quickli,0.002335
quit,0.002424
r,0.021118
radagaisu,0.015143
raetia,0.006653
raft,0.004939
raid,0.003416
rais,0.010580
ramsay,0.005068
ran,0.003211
randsborg,0.007571
rank,0.007266
rape,0.008517
rapidli,0.007256
rare,0.007083
rathbon,0.006992
ravag,0.035690
ravenna,0.035673
reach,0.009245
reactionedit,0.007571
readi,0.003344
real,0.005665
realiti,0.002475
realli,0.002927
realm,0.002874
reason,0.009798
reasonsedit,0.007571
rebel,0.033060
rebellion,0.012995
rebuild,0.003829
rebuilt,0.008755
recal,0.010556
receiv,0.010430
recent,0.006302
receptio,0.007571
rechiar,0.029325
recogn,0.003902
recognit,0.002494
reconcili,0.003807
reconquest,0.010002
reconstitut,0.004827
record,0.013425
recov,0.014004
recoveri,0.003029
recoveriesedit,0.007571
recruit,0.043612
redevelop,0.005406
redirect,0.004481
reduc,0.018961
reemerg,0.004421
reestablish,0.024261
refer,0.000909
referencesedit,0.004481
reform,0.004451
refrain,0.004187
refug,0.003881
refuge,0.006813
refus,0.029208
regain,0.003495
regard,0.006982
regent,0.004371
regim,0.021211
region,0.003258
regrad,0.007571
regul,0.002154
regular,0.012464
reign,0.018877
reinforc,0.006197
reinstat,0.008272
reinstitut,0.005619
reject,0.006729
rel,0.004894
relationship,0.001736
relaunch,0.005449
relax,0.006956
relev,0.002379
reli,0.006577
relianc,0.003648
relief,0.009732
reliev,0.003807
religi,0.023242
religion,0.009270
remain,0.030097
remark,0.011163
remit,0.004848
remov,0.008403
render,0.005968
renew,0.002660
reorgan,0.003804
repair,0.003239
repeat,0.010505
repel,0.003948
replac,0.007416
repli,0.003742
report,0.006995
repres,0.004477
repress,0.003431
republ,0.004271
republican,0.003250
request,0.011062
requir,0.007162
resent,0.007846
reserv,0.002419
resettl,0.008661
resid,0.002410
resist,0.007073
resolv,0.002643
resourc,0.003684
respect,0.001730
respit,0.005705
respond,0.002454
respons,0.014203
rest,0.002131
restor,0.015439
result,0.002534
resum,0.003480
resurg,0.007784
retain,0.009571
retak,0.009458
retali,0.003993
retir,0.006192
retook,0.005346
retreat,0.010543
retrench,0.005767
return,0.016573
reunif,0.004531
reunit,0.012667
rev,0.004065
revenu,0.018582
review,0.002038
revis,0.005438
revolt,0.009938
reward,0.003147
rhetor,0.003523
rhine,0.051919
rhinedanub,0.007331
rhone,0.006565
rich,0.010437
richard,0.002247
richest,0.019835
ricim,0.104888
ricimeredit,0.015143
rife,0.005327
riot,0.003635
riothamu,0.015143
ripuarian,0.007331
rise,0.007324
risen,0.003832
ritual,0.016966
rival,0.006012
river,0.002487
rivista,0.005767
rode,0.005428
role,0.003187
roma,0.025703
roman,0.278245
rome,0.127376
romeedit,0.015143
romulu,0.040598
rosenwein,0.007331
rout,0.002739
routledg,0.003242
rufinu,0.027454
ruin,0.007236
rule,0.013287
ruler,0.027361
rumor,0.004495
rump,0.015816
rural,0.002942
rust,0.004904
rutiliu,0.014662
s,0.006452
sack,0.015146
sacr,0.010453
safe,0.002975
safeti,0.002932
said,0.003739
sail,0.010324
saint,0.003042
salona,0.007144
sanctuari,0.009138
sank,0.004524
sardinia,0.010221
sarmatian,0.005735
saru,0.050014
sassanid,0.018564
savag,0.004291
save,0.002583
say,0.005914
scale,0.004118
scamp,0.007331
scatter,0.003360
scholar,0.006932
scienc,0.003060
scrape,0.005156
sea,0.011608
search,0.002328
seat,0.005385
second,0.001493
secretari,0.002972
sect,0.003810
secular,0.006298
secunda,0.006653
secur,0.001933
sed,0.005054
seek,0.002041
seen,0.003570
seiz,0.003128
seldom,0.008262
selfdef,0.005495
senat,0.021850
senatori,0.028237
send,0.014227
senior,0.018095
sens,0.003744
sent,0.030265
senza,0.007571
separ,0.001662
septemb,0.004388
seri,0.005438
seriou,0.005098
serv,0.001811
servic,0.012409
servitud,0.004859
servituti,0.007571
set,0.004228
settl,0.015516
settlement,0.015932
seven,0.002387
sever,0.007921
severinu,0.006992
severu,0.005619
sex,0.003150
sheep,0.007332
shield,0.010704
ship,0.007745
shock,0.006221
short,0.003880
shortag,0.003304
shortli,0.008496
shortterm,0.006615
shout,0.005156
shower,0.004951
shown,0.002194
sicili,0.029280
sicininu,0.007571
sidoniu,0.006653
sieg,0.011314
sigh,0.012058
signific,0.011315
significantli,0.002335
sile,0.006653
simpl,0.002012
simpli,0.002066
sinc,0.010130
singl,0.001694
sister,0.010485
situat,0.001949
sixti,0.003852
size,0.001972
skinclad,0.007571
skupniewicz,0.007571
slain,0.021627
slaughter,0.012607
slave,0.014961
slaveri,0.006674
sleep,0.003603
slip,0.004182
sloth,0.005593
slow,0.002614
slowli,0.002918
small,0.004648
smaller,0.002153
smith,0.002683
social,0.004832
soisson,0.013307
sold,0.002674
soldier,0.054747
solidi,0.006863
solv,0.002330
sometim,0.003297
somewhat,0.002684
son,0.033945
soninlaw,0.004904
soon,0.009288
sooner,0.004531
sort,0.005312
sought,0.012334
sourc,0.001592
south,0.005949
southerli,0.005799
southern,0.009401
sovereign,0.003026
span,0.002925
spare,0.007882
speedili,0.005495
spent,0.002799
split,0.005205
spot,0.003170
spread,0.002238
spring,0.002856
squadron,0.004291
st,0.002041
stab,0.005237
stabil,0.002361
stanc,0.003490
standard,0.001705
standoff,0.005110
start,0.003205
starvat,0.012534
state,0.012188
statesedit,0.006565
statu,0.014804
stave,0.005237
steadi,0.003245
stilicho,0.249494
stilichoedit,0.007571
stimul,0.005667
stimulu,0.003709
stir,0.004481
stop,0.007505
stori,0.010267
storica,0.006992
storm,0.003547
strait,0.003974
strang,0.003756
stranger,0.004441
strateg,0.002763
strength,0.017978
strengthen,0.008592
stretch,0.003192
strife,0.004173
strip,0.013292
strong,0.005520
structur,0.001479
studi,0.002676
style,0.002609
subdu,0.008183
subgroup,0.003985
subject,0.008054
submit,0.003171
subordin,0.006990
subsidiari,0.003829
subsist,0.003486
substanti,0.004701
suburb,0.004396
succeed,0.002588
success,0.016793
successor,0.028445
suev,0.022715
suevi,0.006565
suevic,0.022715
suffer,0.011556
suffici,0.007023
suggest,0.001772
suicid,0.007261
sum,0.005052
supergroup,0.018087
superior,0.002818
supernatur,0.003679
superpow,0.004353
superrich,0.006284
suppli,0.037160
support,0.031618
suppos,0.002782
suppress,0.002899
suprem,0.012966
sure,0.003469
surnam,0.004584
surpris,0.003330
surrend,0.010466
survey,0.002434
surviv,0.006552
survivor,0.007868
suspect,0.003383
suspicion,0.004070
syagriu,0.006992
symbol,0.002299
synesiu,0.015143
tackl,0.003899
tactic,0.003367
taken,0.003708
tarifa,0.006413
tax,0.042838
taxat,0.006226
techniqu,0.001946
tediou,0.005082
tempera,0.004538
templ,0.013390
temporarili,0.010472
tension,0.002876
term,0.002393
territori,0.022165
tetrarchi,0.006752
textbook,0.002854
th,0.001452
thame,0.004904
theme,0.008413
themselv,0.009392
theoder,0.035439
theodor,0.003465
theodosian,0.006074
theodosiu,0.070937
theodosiuss,0.037859
theori,0.004453
thereaft,0.013244
thermantia,0.007571
thermopyla,0.006172
thesi,0.003133
thessali,0.005945
thi,0.035993
thing,0.002014
think,0.004392
thirtyninth,0.007571
thirtyseven,0.005675
thoma,0.006795
thoroughfar,0.006653
thought,0.003589
thousand,0.008932
thrace,0.010440
threat,0.023905
threaten,0.005758
thrive,0.003505
throne,0.021436
throw,0.003888
thu,0.008882
ticinum,0.007571
till,0.003823
time,0.024649
timeh,0.007571
timespanedit,0.007571
titl,0.009092
today,0.001819
togeth,0.001746
toler,0.012816
took,0.015826
total,0.008834
touch,0.003342
toulous,0.005346
town,0.005200
toynbe,0.005406
tract,0.003610
trade,0.005495
tradit,0.001594
train,0.015307
trait,0.006000
traitor,0.005156
trajan,0.005254
transfer,0.002167
transform,0.006218
translat,0.002180
trap,0.003317
travel,0.004472
treason,0.004502
treasur,0.003926
treati,0.007933
trend,0.002418
tri,0.029898
tribe,0.006234
tribesmen,0.005290
tribigild,0.007571
trier,0.005428
tripolitania,0.006284
triumvir,0.005428
troop,0.105758
troublesom,0.004692
truce,0.004434
true,0.002050
trust,0.005536
truth,0.002694
trystan,0.007571
turmoil,0.003948
turn,0.005252
tuscani,0.010617
twilight,0.005040
typic,0.001698
ultim,0.008761
unabl,0.010526
unaccustom,0.006752
unassail,0.006172
undamag,0.006226
understood,0.002376
unexpectedli,0.004569
unfortun,0.003650
unfre,0.011813
unifi,0.007952
unit,0.007663
uniti,0.008350
univers,0.020985
unjust,0.004468
unmanag,0.005187
unmatch,0.005346
unrest,0.007024
unstabl,0.003378
unsubdu,0.007331
unsuccess,0.006513
unsupervis,0.005171
unusu,0.003070
unwil,0.004402
unworthi,0.005386
upper,0.002557
urban,0.005312
urg,0.003342
use,0.010347
useless,0.004149
usurp,0.031470
utterli,0.004719
vagrant,0.005986
valen,0.029533
valentinian,0.075412
valesianu,0.007571
valley,0.002928
valu,0.001623
vandal,0.109644
vanish,0.003714
vanquish,0.005542
vari,0.001866
vast,0.012714
vegetiu,0.006992
verbatim,0.005272
veri,0.007230
verona,0.005799
version,0.002215
veteran,0.003855
vice,0.005863
viceroy,0.004608
victim,0.006805
victori,0.014664
vigor,0.010991
vigour,0.011972
villa,0.004409
violat,0.002801
violenc,0.009047
virtual,0.002504
visigoth,0.082252
vol,0.002721
volum,0.006507
von,0.002568
voyag,0.003463
w,0.002124
wa,0.140173
wait,0.006867
wall,0.024132
wallia,0.030287
wander,0.004243
want,0.002337
wanton,0.006074
war,0.028782
warband,0.021434
wardperkin,0.007331
warfar,0.003211
warlord,0.004729
warlordsedit,0.007571
warrior,0.015268
warsedit,0.007571
way,0.004062
waymark,0.006992
weak,0.005030
weaken,0.003171
wealth,0.010431
wealthi,0.006608
weapon,0.005624
wed,0.004280
week,0.005323
weight,0.007657
wellequip,0.005542
wellreward,0.007571
welsh,0.004524
went,0.006968
west,0.045662
western,0.059051
westrom,0.007144
westward,0.004348
whatev,0.006234
wheat,0.007202
whi,0.006990
whitehous,0.006172
wick,0.004870
wide,0.004893
wider,0.002883
widerang,0.004421
widespread,0.012164
wield,0.009063
wife,0.009794
wilson,0.003163
win,0.008542
wisdom,0.003352
wish,0.008400
withdraw,0.003043
withdrew,0.007117
women,0.002410
won,0.002563
word,0.001757
wore,0.004531
work,0.002482
worker,0.004714
world,0.008987
worldli,0.004608
wors,0.003482
worsen,0.003780
worst,0.003492
worthwhil,0.004600
wound,0.006948
wreck,0.009048
write,0.001979
written,0.004009
wrote,0.008630
wydawnictwo,0.006172
x,0.002242
yale,0.003461
year,0.021726
yearold,0.003690
young,0.007054
youth,0.003193
zeno,0.014482
