abaco,0.043412
abandon,0.016335
aboard,0.008562
abolit,0.007656
abov,0.003732
absolv,0.011616
abund,0.005911
acceler,0.005606
accept,0.018298
accident,0.007539
accus,0.005846
achiev,0.003667
acklin,0.014470
activ,0.002908
ad,0.003720
addison,0.009416
addit,0.011796
adventur,0.007313
african,0.030656
afro,0.013297
afterward,0.012842
ahead,0.006805
aid,0.008746
airfield,0.009349
airport,0.012842
alabama,0.009317
alburi,0.014470
alli,0.005384
alreadi,0.004246
ambergri,0.026594
america,0.028597
american,0.039633
amerindian,0.009842
amnesti,0.008404
ancestor,0.006265
ancestri,0.008004
andor,0.005101
andrew,0.011840
andro,0.012727
ani,0.007707
ann,0.006827
annul,0.019156
anoth,0.005607
anticip,0.006483
antil,0.009049
antisubmarin,0.010950
appal,0.010351
appeal,0.005429
appoint,0.023960
approach,0.003354
approv,0.005007
april,0.004459
arawakanlanguag,0.014470
arawakanspeak,0.013475
arbitr,0.008260
archipelago,0.007399
area,0.005790
argu,0.007755
arm,0.009296
arrang,0.004752
arriv,0.037540
asia,0.005005
ask,0.004818
assembl,0.004550
assign,0.005236
associ,0.002847
assum,0.004038
assur,0.006641
attack,0.028199
attempt,0.006650
august,0.008859
author,0.013128
avail,0.003563
averag,0.004354
averi,0.010442
aviat,0.007668
award,0.005139
away,0.004440
axel,0.009842
baccalaur,0.011225
bad,0.005979
bahama,0.698105
bahamian,0.132456
bahamianswreck,0.014470
bank,0.004215
bark,0.009049
barrel,0.008151
base,0.012700
batter,0.010950
becam,0.025703
becaus,0.005291
becom,0.008441
befor,0.003001
began,0.017549
begin,0.003427
believ,0.007270
belong,0.009933
benefit,0.004265
benjamin,0.019995
bermuda,0.083853
best,0.003874
better,0.003963
black,0.014231
blackbeard,0.026951
blame,0.006982
blew,0.010789
block,0.005236
blockaderun,0.014470
board,0.005070
boat,0.007007
bond,0.005020
bonnet,0.034663
bonni,0.076076
boom,0.006668
borrow,0.005833
boston,0.006480
breed,0.007048
bribe,0.008995
bridgeman,0.014470
bring,0.004369
britain,0.039614
british,0.054944
britishamerican,0.012302
brought,0.030329
brown,0.005964
bruce,0.007221
brutal,0.007662
build,0.003719
built,0.004381
burn,0.012050
butler,0.008806
c,0.006727
caico,0.034848
calico,0.024248
came,0.015396
campus,0.010027
canada,0.020168
canadian,0.017926
cano,0.009049
cape,0.007394
capit,0.003828
captain,0.014209
captiv,0.007539
captur,0.014501
care,0.004422
caribbean,0.047014
carolina,0.015863
carr,0.009049
carri,0.003795
caught,0.014500
caus,0.003142
cay,0.032142
cede,0.007534
center,0.007509
centr,0.009681
central,0.009900
centuri,0.003022
challeng,0.004205
chang,0.005308
charl,0.009017
chose,0.006621
christoph,0.018509
citi,0.004065
civil,0.008076
civilian,0.006120
claim,0.014294
clear,0.004381
climat,0.005026
close,0.009450
cloth,0.006154
coast,0.005373
coastwis,0.014470
cob,0.012501
cocain,0.009842
colin,0.008103
collect,0.003434
colleg,0.010221
colon,0.031234
colonel,0.008057
coloni,0.048682
colonist,0.016099
columbu,0.056347
combat,0.006179
combin,0.006714
come,0.003367
command,0.004939
commerc,0.005866
commiss,0.009374
commod,0.005714
common,0.002846
commonwealth,0.018245
commun,0.006157
communist,0.005807
compani,0.037177
compris,0.009633
conceiv,0.006182
conch,0.010993
condent,0.014470
condit,0.003294
confeder,0.007027
confin,0.006322
confirm,0.005076
conflict,0.008770
confus,0.005299
constitut,0.003717
construct,0.007292
contact,0.004940
contemptu,0.011382
continu,0.011411
contribut,0.003455
control,0.003128
controversi,0.009279
convict,0.013647
convinc,0.006267
corpor,0.004619
cotton,0.014397
countri,0.012394
cowardic,0.011037
craton,0.011225
creation,0.004285
creditor,0.008217
creol,0.009105
crew,0.044426
crook,0.009366
crop,0.011642
cross,0.010278
crown,0.006098
cruis,0.008340
cuba,0.059963
cuban,0.008657
cultur,0.003540
current,0.003014
cut,0.004998
damag,0.010162
date,0.007870
daughter,0.006462
deadlin,0.009596
deal,0.003793
debt,0.010920
debtor,0.009021
decad,0.004280
decid,0.004588
declin,0.004470
deep,0.005241
defens,0.020672
defer,0.008135
degre,0.003856
densiti,0.011364
depopul,0.009635
depos,0.007743
dept,0.009777
descend,0.017097
descent,0.006754
despit,0.003992
deter,0.008625
deveaux,0.014470
develop,0.007064
did,0.006990
die,0.009023
differ,0.002496
difficulti,0.005085
diplomat,0.005594
direct,0.003126
discont,0.008088
discov,0.004146
discoveri,0.004410
diseas,0.004932
dissolv,0.005848
distribut,0.003885
divert,0.008269
draft,0.006076
drier,0.020055
drive,0.005059
drove,0.007626
drug,0.005553
duchess,0.021901
dugout,0.011962
duke,0.037127
dure,0.018973
dwindl,0.009451
dyewood,0.012399
e,0.003815
earli,0.017012
earliest,0.014215
earn,0.005328
east,0.004358
eastern,0.004705
econom,0.003102
economi,0.003871
edmond,0.009317
educ,0.003611
educationtertiari,0.014470
edward,0.015470
effect,0.002753
effort,0.015703
eleph,0.007841
eleuthera,0.101295
eleutheria,0.013901
eleutherian,0.027802
elizabeth,0.006744
emancip,0.016223
empir,0.007911
end,0.012397
enemi,0.006335
england,0.034433
english,0.019740
enjoy,0.010793
ensu,0.006930
enter,0.004144
entir,0.003676
era,0.004478
escap,0.022766
escort,0.009578
especi,0.003291
establish,0.009076
estim,0.003989
europ,0.007687
european,0.024715
event,0.003689
eventu,0.012087
everi,0.007094
exceedingli,0.009715
exchang,0.008014
exclud,0.005332
execut,0.012882
exhaust,0.006628
exil,0.006474
exist,0.002818
expand,0.004010
expedit,0.012824
expel,0.006834
expir,0.030651
export,0.005200
extend,0.007478
extern,0.002216
f,0.004175
fail,0.008524
failur,0.004932
famili,0.011775
fanci,0.020055
farmer,0.012000
fate,0.007157
feder,0.004513
fell,0.005475
field,0.003095
fight,0.005319
film,0.011498
financ,0.004491
financi,0.004046
fish,0.010578
flagship,0.009654
fleet,0.020449
flight,0.012420
flood,0.006692
floodwat,0.013136
florida,0.044647
flow,0.004464
follow,0.004849
food,0.004362
forc,0.021268
foreign,0.011838
forest,0.011659
forfeit,0.010642
form,0.004634
fort,0.014947
fortun,0.013752
fourth,0.005289
franc,0.021698
franci,0.005805
free,0.006925
freed,0.007815
freedmen,0.011437
freedom,0.009570
freeport,0.012302
french,0.024477
frenchspanish,0.013675
frequent,0.004261
friend,0.018062
frigat,0.009635
fulli,0.004570
fullscal,0.008830
function,0.003103
fund,0.004179
gain,0.011063
gallant,0.012041
galvez,0.013136
gari,0.007562
garrison,0.015362
gave,0.004410
gazett,0.009148
gener,0.004356
georg,0.013106
gold,0.010542
good,0.006740
govern,0.017295
governor,0.077895
governorgener,0.008668
granberri,0.028941
grand,0.011776
grant,0.009294
great,0.013665
green,0.005213
gring,0.011746
group,0.005468
grow,0.007774
growth,0.007673
guanahani,0.028941
guard,0.006335
guild,0.008395
guilti,0.008135
gunpowd,0.008942
ha,0.008085
haiti,0.008854
hall,0.005751
hamper,0.008034
hand,0.007511
hang,0.008119
hanoverian,0.013297
happi,0.006702
harass,0.008243
harbor,0.022757
harbour,0.007795
harcourt,0.009253
hardwood,0.010950
harri,0.006375
haul,0.010027
havana,0.020206
head,0.003913
health,0.004225
hear,0.006392
heavili,0.004887
held,0.003583
help,0.003318
henri,0.024429
hermann,0.007409
hermosa,0.013475
hi,0.051641
higher,0.007207
highest,0.004641
highland,0.008234
hispaniola,0.074494
histor,0.003405
histori,0.027772
historian,0.005108
hold,0.003651
home,0.008657
honest,0.008929
hornigold,0.041703
hous,0.016265
howard,0.007031
howev,0.009840
hrh,0.013297
human,0.002920
hundr,0.009211
hunt,0.006117
hurrican,0.016192
idea,0.003402
ident,0.004340
identif,0.005970
identifi,0.007328
ii,0.011901
illeg,0.006108
immigr,0.012064
imperi,0.005905
import,0.002585
impos,0.005213
improv,0.011190
inagua,0.054700
includ,0.010521
incom,0.004642
increas,0.002914
increasingli,0.004468
independ,0.015567
independentmind,0.012727
indi,0.015386
inform,0.003013
inhabit,0.010539
inherit,0.005621
initi,0.003293
insect,0.006887
instal,0.005982
instead,0.003550
insur,0.005844
intellig,0.004729
intern,0.007969
invas,0.011054
invest,0.004315
involv,0.002938
isbn,0.025084
island,0.202179
issu,0.013011
j,0.003806
jack,0.014333
jacobit,0.010714
jamaica,0.026106
jamaican,0.010950
jame,0.012825
januari,0.004326
jeann,0.011382
jedidiah,0.012302
jen,0.018765
jewish,0.006284
job,0.005234
john,0.003447
johnson,0.006926
join,0.004508
joseph,0.004995
juli,0.013135
juliu,0.007712
june,0.008832
just,0.003534
keegan,0.024248
kind,0.004037
king,0.028968
kingdom,0.008063
km,0.005704
known,0.005065
labor,0.009952
labour,0.010261
lack,0.003864
land,0.030685
landfal,0.011082
languag,0.003809
larg,0.010683
larger,0.004000
largest,0.004185
late,0.003508
latecoloni,0.013901
later,0.015070
law,0.003053
lead,0.003023
leader,0.004431
learn,0.004257
leas,0.007650
leav,0.017166
led,0.009812
left,0.024238
legal,0.004181
letter,0.005002
liber,0.013707
life,0.006515
lighthous,0.010003
like,0.005413
link,0.002203
list,0.005635
littl,0.003878
live,0.016383
load,0.013908
local,0.003432
locat,0.003726
long,0.006580
loos,0.006300
loot,0.008583
loss,0.004432
lost,0.004403
low,0.003883
lower,0.003744
loyalist,0.055429
lucayan,0.079784
lucr,0.008358
luftwaff,0.011176
lumber,0.009635
lushli,0.013901
lynden,0.041025
macmillan,0.006922
madagascar,0.008842
magnat,0.010321
mainland,0.006583
mainli,0.004206
major,0.005376
make,0.010307
maker,0.006990
mani,0.029775
march,0.004202
mari,0.005854
maria,0.007489
marriag,0.006406
mass,0.008311
massachusett,0.006645
master,0.005408
materi,0.003551
mayaguana,0.013901
medicin,0.004831
membership,0.005581
men,0.018795
mention,0.004905
merchant,0.012036
michael,0.004663
mid,0.005046
midcenturi,0.010128
middlemen,0.010442
migrat,0.016243
militari,0.004099
millennium,0.006324
millionair,0.010642
milo,0.011129
minist,0.013716
mischief,0.011328
mixedrac,0.011225
mm,0.007414
modern,0.003039
money,0.009057
montagu,0.009798
month,0.004477
morison,0.012727
mors,0.009238
mosquito,0.009842
movement,0.013977
munit,0.009285
murder,0.006435
narcot,0.009349
nassau,0.322761
nation,0.011231
nativ,0.005237
navi,0.036390
nearli,0.013240
need,0.003046
neighborhood,0.007674
nest,0.007848
netherland,0.005905
new,0.031688
news,0.005530
nia,0.011437
nichola,0.020449
nineteen,0.009091
nonwhit,0.011437
north,0.020117
northeastern,0.007828
northern,0.004737
note,0.002921
notifi,0.009504
notori,0.008042
novemb,0.004544
o,0.005343
oak,0.016319
object,0.003464
obtain,0.003938
occup,0.010560
occur,0.003219
octob,0.013145
offer,0.015072
offic,0.004078
offici,0.011541
offshor,0.014777
onli,0.006997
open,0.006938
oper,0.006393
opportun,0.004786
opposit,0.004169
order,0.002792
organ,0.005528
outnumb,0.008414
overcrowd,0.010103
paid,0.005166
pardon,0.054213
pari,0.005541
parliament,0.005110
parti,0.019579
partli,0.005448
pass,0.003936
patent,0.006762
pattern,0.004323
paul,0.004404
peac,0.009774
pensacola,0.013297
pension,0.007040
peopl,0.016762
percent,0.005089
period,0.009032
perman,0.009821
person,0.006631
persuad,0.007323
peter,0.004620
pictou,0.013901
pillar,0.007681
pindl,0.041025
pinta,0.013675
piraci,0.062872
pirat,0.193744
place,0.002865
plan,0.007675
plantat,0.029077
planter,0.009349
plead,0.009735
plenti,0.007967
plot,0.006210
plunder,0.008702
polit,0.012721
poor,0.005010
popul,0.032448
port,0.005726
posit,0.003003
possibl,0.002963
post,0.009835
postemancip,0.014470
postindepend,0.009909
postwar,0.013783
poverti,0.005977
power,0.002973
prais,0.020383
precolumbian,0.008955
predominantli,0.012900
pregnant,0.008866
prehistori,0.014596
premier,0.014548
prepar,0.004712
presenc,0.004394
present,0.002905
presentday,0.006526
presid,0.008404
press,0.017099
pretend,0.008995
pretext,0.009694
primarili,0.007943
prime,0.014005
prior,0.004388
prison,0.018501
privat,0.045831
probabl,0.004169
process,0.002746
proclam,0.008331
progress,0.011881
promin,0.009113
proprietor,0.056812
prosper,0.018021
protect,0.003872
provid,0.028359
provis,0.005659
provok,0.007436
public,0.002926
puritan,0.009486
queen,0.006098
race,0.005708
racial,0.014270
rackham,0.082708
raid,0.006918
raider,0.009468
rain,0.007378
rais,0.004285
rapid,0.005035
rare,0.004782
reach,0.007490
read,0.016003
readi,0.006772
realm,0.005821
rebel,0.006695
recalcitr,0.011082
recaptur,0.008376
reced,0.009163
receipt,0.007946
receiv,0.003520
recent,0.003191
reclaim,0.008604
record,0.003884
recov,0.011345
red,0.005108
reduc,0.003491
reef,0.016176
refer,0.003685
reflect,0.003960
refus,0.016133
regard,0.003535
regiment,0.008304
regrown,0.012988
reinforc,0.006275
relat,0.002484
releas,0.004605
relief,0.006570
reliev,0.007712
religi,0.004707
reluct,0.007511
remain,0.021334
remov,0.008509
renam,0.006397
repair,0.006560
repatri,0.008968
repay,0.007788
replac,0.007510
replant,0.011495
report,0.010625
reportedli,0.007318
repres,0.003022
republ,0.017300
republican,0.006583
research,0.002989
resettl,0.008770
resid,0.009764
resign,0.006297
resist,0.004775
resolut,0.005637
resourc,0.003730
retain,0.014538
retaliatori,0.011495
retir,0.006270
retroact,0.009559
return,0.014918
revel,0.007388
revolt,0.006709
revolut,0.009130
richard,0.004552
rigid,0.006857
riot,0.014726
road,0.005352
robberi,0.010155
roger,0.129776
roland,0.008854
root,0.004655
rose,0.005592
rout,0.022192
royal,0.019449
ruler,0.006157
rulersorg,0.011616
run,0.004049
s,0.028750
sack,0.007668
sail,0.034851
saint,0.006162
salt,0.012627
salvador,0.042457
salvag,0.038619
samuel,0.006551
san,0.029261
santa,0.007303
sayl,0.013475
sea,0.004702
seal,0.006910
seamen,0.010993
seaplan,0.011225
seawat,0.008929
second,0.012098
secreci,0.009035
sector,0.004831
secur,0.003915
seed,0.006538
seek,0.004135
seiz,0.019007
selfgovern,0.014616
seminol,0.027350
send,0.005763
sent,0.025540
septemb,0.004444
serv,0.007336
servic,0.003590
servitud,0.009842
set,0.008564
settl,0.020950
settlement,0.032267
settler,0.076279
sever,0.008021
ship,0.099349
shipwreck,0.009301
shirley,0.010236
shorter,0.006560
shortli,0.005735
shortliv,0.007240
sign,0.008445
signific,0.003273
silver,0.006412
simpli,0.004185
sinc,0.002564
sir,0.023156
site,0.004320
situat,0.003948
slave,0.103025
slaver,0.011495
slaveri,0.006758
slightli,0.005558
slip,0.008471
slow,0.005295
small,0.012553
smaller,0.008722
societi,0.012853
soil,0.011622
soldier,0.005835
sometim,0.003338
soon,0.018812
sourc,0.003226
south,0.004016
southcentr,0.011495
southernmost,0.009820
spain,0.043601
spanish,0.122023
spars,0.007730
spend,0.005323
spur,0.007203
stand,0.004820
start,0.003246
state,0.008976
stede,0.038966
steel,0.006424
stori,0.005198
strain,0.006375
stratif,0.009105
strenuous,0.011437
stronger,0.006174
structur,0.002996
struggl,0.010752
stuart,0.035566
studi,0.005420
submit,0.006424
success,0.003401
sugarcan,0.008736
suggest,0.003590
sumptuou,0.012988
sun,0.005689
suppli,0.012543
support,0.009148
suppress,0.029366
surrend,0.028262
swamp,0.009366
swedish,0.006994
swept,0.008385
symonett,0.013675
tackl,0.007896
taino,0.012399
taken,0.003755
tano,0.011616
tax,0.009640
teach,0.010048
tension,0.005825
term,0.002423
territori,0.008978
th,0.002940
thcenturi,0.005913
therefor,0.003486
thi,0.007673
thirdclass,0.012853
thirteen,0.007693
thirti,0.006914
thoma,0.004587
thought,0.003634
thousand,0.004522
threaten,0.005831
threetier,0.011495
thu,0.002998
thunderbal,0.014470
time,0.013615
tinker,0.011554
titl,0.009207
titular,0.009864
ton,0.013908
toss,0.010573
total,0.003578
tour,0.007399
tourism,0.017899
tourist,0.006834
trade,0.022260
tradit,0.003229
traffick,0.008521
train,0.004428
transport,0.004396
trap,0.006719
treati,0.010712
tree,0.005283
tri,0.020184
trial,0.005852
tripl,0.007065
trott,0.026272
troubl,0.006733
turk,0.016207
turn,0.007091
turtl,0.026637
tusk,0.010908
twin,0.007545
uncl,0.008201
unequ,0.007967
unidentifi,0.010714
unit,0.018108
univers,0.010625
unknown,0.005310
unlik,0.004271
unrest,0.007113
upper,0.005179
uproot,0.010442
use,0.004191
usual,0.003086
vane,0.082708
ventur,0.006754
vesceliu,0.028941
veteran,0.007808
virginia,0.014777
visit,0.010305
voyag,0.014030
wa,0.088322
wage,0.005635
wake,0.006842
walker,0.008080
want,0.009470
war,0.036433
warship,0.008395
wartim,0.008168
water,0.003956
watl,0.025977
wealth,0.005281
week,0.005390
welcom,0.007078
wennergren,0.013297
went,0.009408
west,0.008807
whale,0.007989
white,0.009738
widespread,0.004927
william,0.020548
window,0.007207
windsor,0.022353
winter,0.006355
wood,0.017754
woodard,0.013901
word,0.007120
work,0.010057
world,0.010400
wreck,0.082467
wrecker,0.027802
wrongli,0.009842
x,0.009082
yacht,0.019554
year,0.031060
zone,0.005301
