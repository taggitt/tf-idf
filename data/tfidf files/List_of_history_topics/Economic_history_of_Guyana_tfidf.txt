abandon,0.005952
abat,0.005467
abolish,0.016646
abolit,0.012554
abruptli,0.005032
abus,0.003427
acceler,0.003064
accept,0.006001
accompani,0.006160
accomplic,0.006777
accomplish,0.006404
accord,0.004779
accus,0.006390
achiev,0.004008
acquisit,0.003445
acrimoni,0.005962
act,0.007284
activ,0.006359
activist,0.007602
acut,0.004181
ad,0.002033
adapt,0.005074
addit,0.008059
address,0.002399
adjust,0.003049
administ,0.003123
administr,0.031188
admir,0.003775
adopt,0.006596
adult,0.003119
advanc,0.001967
advantag,0.002473
advis,0.006441
advoc,0.005470
aegi,0.005522
affair,0.010538
affect,0.006246
africa,0.002587
african,0.039096
afroguyanes,0.255440
aftermath,0.003970
age,0.001995
agenc,0.002506
agenda,0.003711
agit,0.004738
ago,0.003087
agre,0.012011
agreement,0.002432
agricultur,0.011840
aid,0.004780
aifld,0.015481
aifldtrain,0.008115
ailment,0.005066
aim,0.004550
air,0.002447
airplan,0.004674
airport,0.003509
alfr,0.009794
alien,0.003733
align,0.003442
alleg,0.006589
allevi,0.004253
alli,0.008828
allianc,0.011919
allow,0.012675
alonso,0.005707
alreadi,0.002321
alter,0.002857
altern,0.002047
alway,0.002142
amend,0.006719
america,0.020096
american,0.010831
amerindian,0.026897
amien,0.006032
angola,0.004832
ani,0.002808
animos,0.005550
ankoko,0.008115
announc,0.008026
anoth,0.010727
answer,0.002836
anticapitalist,0.005940
antijagan,0.008115
antil,0.034622
antiportugues,0.008115
apan,0.008115
appeal,0.014836
appear,0.003690
appli,0.001703
appoint,0.031430
apprehens,0.010389
appropri,0.002554
approv,0.005474
april,0.007312
arawak,0.032966
arbitr,0.013544
area,0.014242
arguabl,0.003883
armi,0.002740
aros,0.003164
arrang,0.002597
arrest,0.003501
arriv,0.007694
arson,0.005985
articl,0.001893
articul,0.003848
artilleri,0.004354
ashton,0.006082
asia,0.002735
asid,0.003633
ask,0.007901
assembl,0.017411
assert,0.002737
assign,0.005723
assimil,0.004158
assist,0.004751
associ,0.007782
assum,0.006621
atlant,0.003376
attack,0.002568
attempt,0.005452
attent,0.002598
attract,0.007691
august,0.007263
author,0.016145
authoritarian,0.016351
autochthon,0.006581
automat,0.003252
aveburi,0.006892
avow,0.006135
awaken,0.004510
awar,0.008648
award,0.005617
away,0.002427
backer,0.005742
background,0.005472
backward,0.003962
badli,0.004331
bail,0.005579
balanc,0.005043
ballot,0.013379
barbado,0.005000
base,0.004164
basi,0.001984
basic,0.001932
bauxit,0.016973
bauxitewer,0.008115
becam,0.035121
becaus,0.004338
becom,0.012303
befor,0.006563
began,0.024938
begin,0.007493
behavior,0.002253
bellicos,0.006538
belt,0.004188
benn,0.006191
berbic,0.079947
bglu,0.024347
bharrat,0.016231
bicamer,0.004327
bicker,0.006626
big,0.002908
bind,0.003317
bitter,0.013041
black,0.012963
blackout,0.005707
blatant,0.006383
bloodi,0.004510
bloodless,0.005024
board,0.002771
boast,0.004832
bodi,0.003855
bomb,0.007274
bondag,0.005724
border,0.022640
born,0.005427
boulder,0.004873
boundari,0.016092
box,0.003601
boycot,0.004859
boycott,0.009464
branch,0.002043
brand,0.003883
bridg,0.003174
brief,0.006089
briefli,0.007127
brindley,0.008115
bring,0.007165
britain,0.027065
british,0.175897
britishappoint,0.008115
briton,0.005277
broke,0.016472
broken,0.003203
brother,0.003221
brought,0.016577
brush,0.005049
brutal,0.004188
budget,0.003007
build,0.004066
bulletin,0.004319
burnham,0.269290
busi,0.012994
businessmen,0.004674
came,0.016830
campaign,0.014441
candid,0.005927
canj,0.008115
capit,0.006278
capitalist,0.007602
car,0.003400
care,0.002417
career,0.006312
carib,0.041558
caribbean,0.047722
caricom,0.005146
carifta,0.007365
carl,0.003212
carter,0.009087
case,0.004826
castro,0.005204
catalyst,0.004201
cathol,0.003106
caus,0.008587
ccile,0.007365
cede,0.004118
celebr,0.003316
center,0.006156
centerleft,0.005625
centerright,0.005778
central,0.003607
centuri,0.018175
ceremoni,0.007103
certain,0.003522
chairman,0.003639
challeng,0.004597
chang,0.013056
character,0.002440
charg,0.009932
charter,0.006922
chase,0.004375
che,0.005940
check,0.003335
cheddi,0.021803
chief,0.002679
childhood,0.008223
children,0.002646
chines,0.011063
chosen,0.005880
christ,0.008211
christoph,0.003372
church,0.002907
cia,0.003823
circl,0.003100
circumst,0.005911
citi,0.002222
citizen,0.007899
civil,0.015450
claim,0.015626
clash,0.011489
class,0.040550
clearcut,0.005185
clearli,0.005786
cleveland,0.005074
clinic,0.003355
close,0.008608
cloth,0.003364
club,0.003731
coalit,0.009354
coast,0.005873
coastal,0.003507
coastlin,0.004002
coercion,0.005049
colleg,0.019553
colo,0.007099
colon,0.017071
coloni,0.170295
colonialguyana,0.008115
colonist,0.017598
colour,0.007345
columbu,0.004399
columbuss,0.006135
combin,0.018349
come,0.003680
commerci,0.007396
commiss,0.028181
committe,0.013804
commod,0.003123
commun,0.025240
communist,0.012696
compani,0.022352
compar,0.003806
compel,0.004021
competitor,0.007972
complaint,0.004264
complet,0.005332
compos,0.002441
compromis,0.007399
concentr,0.002518
concept,0.005387
concern,0.010840
conciliatori,0.006315
condit,0.010804
conduct,0.002205
confer,0.007599
conflict,0.007190
confront,0.014221
congress,0.008650
congressman,0.005897
conquer,0.003484
conquest,0.003467
conserv,0.012772
consid,0.010506
consider,0.006488
consist,0.013779
consolid,0.020274
constitu,0.006027
constitut,0.052833
construct,0.005978
consult,0.002971
contact,0.002700
contest,0.020316
continu,0.015593
contract,0.002581
contribut,0.001888
control,0.041037
convent,0.004984
convert,0.005246
convinc,0.003425
convincingli,0.005816
cook,0.003628
cooper,0.009839
coopt,0.005674
corbin,0.007025
cost,0.002262
council,0.023466
counter,0.003479
counterpart,0.003450
countri,0.037259
countrysid,0.004218
court,0.068348
cover,0.002118
creat,0.012593
crisi,0.002655
critchlow,0.007740
critic,0.007572
crop,0.006363
crowd,0.012032
crown,0.003333
crush,0.003937
cuba,0.016387
cuban,0.014196
cuffi,0.007365
cult,0.008410
cultiv,0.003287
cultur,0.007740
curb,0.004674
curtail,0.004668
cut,0.002732
cuyuni,0.008115
daili,0.003009
dali,0.010665
damag,0.005554
date,0.004301
davson,0.007740
day,0.011652
dead,0.006452
death,0.009016
debacl,0.006163
debat,0.002395
decad,0.002339
decemb,0.023617
decis,0.004339
decisionmak,0.003709
declar,0.012428
declin,0.009774
decolon,0.009452
deep,0.002865
deepen,0.004625
defeat,0.006089
defin,0.001766
degrad,0.003692
degre,0.004215
deleg,0.003633
demand,0.013786
demerara,0.094878
demeraraessequibo,0.008115
demis,0.004548
democraci,0.002866
democrat,0.017651
demonstr,0.011566
denial,0.004442
dentistri,0.010783
depart,0.002256
departur,0.007904
depend,0.001698
deposit,0.003046
depress,0.003197
descent,0.007384
describ,0.001584
design,0.001890
desmond,0.011449
despit,0.013093
destabil,0.009587
destroy,0.008073
detach,0.004188
deterior,0.003790
determin,0.001787
detractor,0.005235
develop,0.011583
did,0.011462
die,0.014795
difficult,0.002244
difficulti,0.002779
diplomat,0.006115
direct,0.006835
directli,0.002067
disappoint,0.004416
disburs,0.005403
discoveri,0.002410
discrimin,0.007147
discuss,0.004199
diseas,0.002695
disenfranchis,0.005836
dismal,0.005641
disord,0.003317
dispers,0.003532
displac,0.003302
displeasur,0.006251
disput,0.018120
disrupt,0.003316
dissatisf,0.008115
distress,0.004271
disturb,0.014455
divid,0.004081
divis,0.004461
dockwork,0.008115
doctor,0.003071
doctrin,0.003086
document,0.002484
domain,0.002513
domest,0.007999
domin,0.014824
donald,0.003628
downpour,0.006777
dozen,0.003599
drainag,0.004976
dramat,0.002906
draw,0.002638
drawn,0.003037
drew,0.003546
driver,0.003944
dure,0.005925
dutch,0.104307
duti,0.006124
eager,0.004720
earli,0.013947
earlier,0.004579
eas,0.003567
easier,0.003317
easili,0.005309
east,0.019057
econom,0.022043
economi,0.025392
ed,0.002169
educ,0.005921
effect,0.007526
effort,0.004291
eighteen,0.004598
elect,0.109694
elector,0.039888
element,0.009852
elig,0.007752
elit,0.006881
elsewher,0.003194
emancip,0.008867
embargo,0.004652
embodi,0.003582
embrac,0.003495
emerg,0.019858
emphas,0.002719
emphasi,0.005620
empir,0.002162
enact,0.003434
encompass,0.005801
encourag,0.002546
end,0.018634
engag,0.004907
english,0.004315
englishspeak,0.004246
enhanc,0.002869
enjoy,0.005899
enlarg,0.003957
enmesh,0.006538
enmor,0.008115
enslav,0.039903
ensur,0.002545
enter,0.002265
entir,0.002009
entrench,0.004714
epoch,0.004425
equal,0.002110
equip,0.005540
era,0.004896
ernesto,0.005522
erod,0.004459
eros,0.004178
escal,0.008027
escap,0.006221
especi,0.008995
essenti,0.002153
essequibo,0.141986
establish,0.016535
estat,0.006596
estim,0.006541
ethnic,0.033633
european,0.021228
evanston,0.006626
eventu,0.004404
evil,0.003768
evolv,0.002543
ex,0.004191
exacerb,0.004289
exceedingli,0.005309
execut,0.016429
exist,0.001540
expand,0.004383
expedit,0.003504
expeditionari,0.005224
expenditur,0.003425
experienc,0.002781
expir,0.004188
explor,0.007082
export,0.005684
exportssugar,0.008115
expos,0.009310
express,0.004015
exslav,0.006777
extend,0.002043
extens,0.002092
extent,0.002549
extrem,0.002222
face,0.004651
fact,0.001967
faction,0.021958
fade,0.004425
fail,0.006988
fair,0.003138
fall,0.002114
famili,0.004290
famou,0.002559
fanat,0.005816
far,0.004201
farmer,0.003279
farreach,0.004774
father,0.005448
favor,0.010236
fear,0.011629
februari,0.005034
feder,0.009867
feed,0.003556
feel,0.005793
fell,0.002992
fidel,0.004696
field,0.003384
fieldwork,0.005235
fierc,0.004260
fifteenth,0.004832
fiftythre,0.014730
fight,0.002907
final,0.008016
financ,0.002455
financi,0.004423
fishermen,0.004938
flaunt,0.007267
fled,0.003750
fledgl,0.005214
fli,0.003458
flood,0.007315
flow,0.002440
follow,0.014579
food,0.002384
foray,0.005856
forb,0.017930
forc,0.013285
foreign,0.012940
form,0.016463
formal,0.008293
formid,0.004781
forsook,0.008115
fortyon,0.007597
fortytwo,0.006163
forward,0.002941
fought,0.007028
foundat,0.004321
fourteen,0.004505
franc,0.011859
franchis,0.004473
francisco,0.003575
franciscoarea,0.008115
fraud,0.011449
fraudul,0.009218
free,0.011355
freed,0.004271
freedom,0.005231
french,0.017838
frequent,0.002329
friday,0.004793
friendli,0.003816
function,0.001696
fund,0.004568
futur,0.002017
gain,0.016125
gather,0.002888
gave,0.012054
gdp,0.003116
gener,0.007143
georgetown,0.069323
german,0.004679
germani,0.002494
gerrymand,0.006892
given,0.001661
giwu,0.040578
glasgow,0.004866
goal,0.002278
gold,0.005762
gordon,0.003864
govern,0.070899
government,0.006935
governor,0.069945
governorsgener,0.007179
gradual,0.007990
graduat,0.003104
grant,0.005079
grassroot,0.005119
great,0.007469
greater,0.006260
greatli,0.002713
grenada,0.005522
grew,0.005537
grievanc,0.010115
group,0.032879
grover,0.006032
grow,0.010622
growth,0.004194
guevara,0.006420
guiana,0.204409
guianes,0.013913
guyana,0.290015
guyanes,0.109330
ha,0.002209
half,0.006841
halfcenturi,0.005856
hand,0.008211
harass,0.004505
hard,0.005438
hardli,0.008858
hardship,0.004702
harmoni,0.003864
havoc,0.005816
head,0.010694
headmast,0.006135
health,0.002309
heart,0.003041
heir,0.004239
held,0.013712
help,0.005441
henri,0.005340
heritag,0.003388
hermann,0.004049
hero,0.003929
hi,0.037139
high,0.003355
higher,0.001969
highli,0.002181
highrank,0.005175
hind,0.005287
hindi,0.005836
hint,0.004510
hinterland,0.020899
histori,0.015179
historian,0.002791
hit,0.003430
hn,0.005610
hold,0.009978
home,0.007097
hope,0.005641
host,0.003029
hostil,0.003436
hous,0.002222
howev,0.016135
hoyt,0.065815
human,0.003192
hungari,0.004102
hunter,0.004198
idea,0.001859
ident,0.004744
identifi,0.002002
ideolog,0.006259
ignit,0.004826
ignor,0.002873
ii,0.008673
ill,0.003217
illinoi,0.004055
imag,0.002551
immedi,0.007306
immigr,0.009891
impact,0.002270
imperi,0.003227
implement,0.011533
implic,0.002886
import,0.007065
imposs,0.002801
impress,0.003381
improv,0.012233
inabl,0.003999
inadequ,0.003934
inasmuch,0.005522
includ,0.010351
incom,0.002537
inconsist,0.003850
incorpor,0.002275
increas,0.015932
increasingli,0.007327
incumb,0.008884
inde,0.002817
indentur,0.030296
independ,0.028929
independentmind,0.006956
indi,0.016820
india,0.030439
indian,0.016455
indic,0.004076
indigen,0.013213
indoguyanes,0.164437
industri,0.007564
influenc,0.003649
influx,0.004275
inform,0.001647
infrastructur,0.008415
inhabit,0.005760
initi,0.009001
injur,0.008403
inland,0.003975
inner,0.003330
insight,0.003073
instabl,0.003582
instanc,0.006731
instead,0.001940
institut,0.003506
instrument,0.005102
intellectu,0.005684
intend,0.002564
intens,0.002751
intent,0.002788
interamerican,0.005494
interchang,0.003792
interim,0.011927
interior,0.009754
intermingl,0.005918
intern,0.011615
interpret,0.002210
interven,0.003560
intervent,0.003006
intimid,0.004744
intraunion,0.008115
intrigu,0.004720
introduc,0.013604
introduct,0.008785
inund,0.005897
investig,0.006964
investor,0.003156
involv,0.001606
irrig,0.004188
isbn,0.013710
island,0.009822
issu,0.010667
j,0.004161
jagan,0.443347
jagdeo,0.032463
jalil,0.006674
janet,0.023937
januari,0.011822
jb,0.004976
jhaat,0.008115
jim,0.004379
jimmi,0.009818
job,0.005722
jockey,0.006538
join,0.014786
jone,0.007357
jonestown,0.038703
joseph,0.002730
judici,0.006361
judiciari,0.003686
juli,0.007179
june,0.004827
jurisdict,0.003362
just,0.003863
justic,0.016799
k,0.002511
kaituma,0.016231
kemp,0.005657
kerosen,0.005760
key,0.006324
kill,0.008205
kilogram,0.004679
kilomet,0.008376
kind,0.002206
king,0.002638
kitti,0.005940
known,0.005537
labor,0.043519
labour,0.025239
lachmansingh,0.008115
lack,0.006336
land,0.006289
landownership,0.006497
landscap,0.003453
laps,0.004931
larg,0.017518
larger,0.002186
largest,0.004575
late,0.011506
later,0.013179
latin,0.005207
launch,0.005579
law,0.006674
lead,0.004958
leader,0.041176
leadership,0.009016
leagu,0.013594
lean,0.004524
learn,0.002326
leav,0.002345
led,0.025028
left,0.013248
leftist,0.017071
legaci,0.003482
legal,0.006857
legisl,0.022824
legislatur,0.010166
legitim,0.003440
lendleas,0.006251
leo,0.004146
lessdevelop,0.006282
lesser,0.020375
lesson,0.003887
let,0.006207
lethem,0.007740
level,0.003312
liber,0.007492
librari,0.002602
licens,0.006596
life,0.016025
lift,0.003697
like,0.004438
limit,0.005074
linden,0.012994
line,0.003964
linger,0.005032
link,0.003613
list,0.007701
litani,0.006457
live,0.008954
loan,0.003213
lobbi,0.004257
local,0.007504
locat,0.004073
london,0.018658
long,0.001798
longchamp,0.014948
longer,0.002284
loos,0.003443
lord,0.006556
lose,0.002913
loss,0.002422
lost,0.004814
low,0.006368
lower,0.004093
lowerclass,0.012383
lowest,0.006589
loyalti,0.004013
lure,0.005309
machineri,0.003415
macmillan,0.007567
madeira,0.005918
main,0.003506
mainland,0.003598
mainli,0.002299
maintain,0.011587
major,0.030858
make,0.005633
mandat,0.003450
mani,0.013770
manipul,0.005931
manpow,0.004866
mao,0.005066
map,0.007601
mar,0.011370
march,0.013780
margin,0.006070
maritim,0.003565
mark,0.008895
market,0.002073
marxist,0.007520
marxistleninist,0.005119
mask,0.004391
mass,0.002271
massacr,0.012115
massiv,0.002947
master,0.002955
matter,0.002083
mayb,0.005016
mean,0.003050
meanwhil,0.006698
mediat,0.003393
meet,0.002241
member,0.031808
membership,0.009152
memori,0.002881
merchant,0.006579
merger,0.004013
messag,0.003442
methodist,0.005156
mid,0.005516
middl,0.025119
middleclass,0.028428
midst,0.004768
migrat,0.011837
militari,0.006721
million,0.004350
miner,0.003066
minist,0.032486
ministeri,0.009229
minor,0.002563
mix,0.005328
mob,0.005032
mobil,0.002942
model,0.001725
moder,0.009518
modern,0.004983
modifi,0.002702
moment,0.003151
momentum,0.007282
monarch,0.003657
monarchi,0.003550
monetari,0.002969
money,0.002475
monitor,0.002995
monopoli,0.003548
monro,0.005298
month,0.004894
moratorium,0.005522
mortal,0.003701
mosaic,0.004614
mostli,0.002363
motiv,0.005411
mouth,0.015708
movement,0.009549
moyn,0.035496
mpca,0.038703
mulatto,0.005918
multiethn,0.010758
multitud,0.004370
municip,0.010124
munster,0.007474
murder,0.003517
mysteri,0.003655
napoleon,0.003777
nation,0.035298
nationalist,0.003591
natur,0.001478
ndp,0.018490
near,0.004554
nearmonopoli,0.006626
necessari,0.002207
need,0.006659
negoti,0.005798
neighbor,0.003124
neighbour,0.003383
netherland,0.019365
neutral,0.003031
new,0.045774
newer,0.004067
newli,0.011655
newspap,0.003428
nineteen,0.004968
nineteenth,0.003464
ninetyon,0.007474
nomad,0.004278
nomin,0.005781
nomine,0.005224
nonalign,0.009146
noncooper,0.006724
north,0.002199
northward,0.004708
northwestern,0.004205
novemb,0.009936
nucleu,0.003669
number,0.012470
oakvil,0.007740
object,0.003786
observ,0.005483
obtain,0.004305
occup,0.002885
occupi,0.008329
occur,0.001759
octob,0.016765
offer,0.002059
offic,0.013376
offici,0.014719
officio,0.005594
oil,0.002764
oilprospector,0.008115
ojeda,0.006349
old,0.002291
onc,0.004139
oneday,0.005918
onli,0.006374
ont,0.006315
onward,0.003511
open,0.005688
openli,0.004194
oppon,0.006881
opportun,0.013080
oppos,0.002314
opposit,0.022789
oppressor,0.006497
opt,0.004308
order,0.007631
ordin,0.004084
organ,0.021150
origin,0.004604
orinoco,0.012065
oscar,0.004826
ostens,0.004487
outbreak,0.003768
outburst,0.011485
outlaw,0.004446
outmaneuv,0.006251
outnumb,0.004598
outsid,0.004225
outspoken,0.005128
overal,0.002534
overlook,0.004278
oversea,0.006606
overtur,0.005897
overwhelmingli,0.004491
owner,0.015664
ownership,0.003252
p,0.004228
pac,0.057077
pace,0.003737
paget,0.006349
paid,0.002823
panic,0.004646
paralysi,0.005321
paramountci,0.007025
parent,0.003119
pari,0.006057
parliament,0.002793
parliamentari,0.009515
parti,0.089895
particip,0.019664
particular,0.010401
pass,0.002151
past,0.002197
patrol,0.008911
payrol,0.005016
peac,0.008013
peasant,0.003861
peopl,0.036648
percent,0.025035
perhap,0.002574
period,0.011519
persist,0.003140
person,0.003624
personalitiescheddi,0.008115
petit,0.008193
philadelphia,0.004211
place,0.001566
placid,0.006892
plagu,0.003989
plan,0.014682
plank,0.005778
plant,0.002404
plantat,0.063571
planter,0.102204
plantocraci,0.014359
platform,0.006783
play,0.005999
pledg,0.003927
plu,0.003116
pnc,0.207915
point,0.006426
polic,0.009240
policemen,0.005522
polici,0.049190
polit,0.093868
politic,0.005480
politician,0.006645
poll,0.003854
pool,0.007253
poor,0.008215
poorli,0.014668
popul,0.017735
popularli,0.004155
populationwer,0.008115
port,0.006259
porter,0.004674
portugues,0.035256
posit,0.006565
possess,0.004977
post,0.013439
postal,0.004264
postburnham,0.008115
postpon,0.004510
postwar,0.003766
potenti,0.002056
poverti,0.009801
power,0.037381
ppp,0.246424
practic,0.001699
precoloni,0.005000
predecessor,0.003463
predominantli,0.003525
prefer,0.007296
preindepend,0.005940
preoccupi,0.005245
present,0.003176
presentday,0.003567
presid,0.036749
press,0.005607
pressur,0.002390
pretext,0.005298
previou,0.004790
previous,0.002421
price,0.004702
primari,0.002096
primarili,0.004341
prime,0.025517
principl,0.001906
privat,0.004554
privi,0.004839
problem,0.001698
proceed,0.002906
process,0.003002
proclaim,0.007185
produc,0.008262
product,0.003310
profession,0.005146
professor,0.005677
program,0.009989
programm,0.003052
progress,0.004329
prohibit,0.003240
promis,0.005968
promot,0.004531
prompt,0.006953
promulg,0.004049
properti,0.001922
proport,0.002625
propos,0.011756
proppp,0.008115
proprietor,0.005175
prospect,0.003374
protest,0.008854
proudli,0.006135
prove,0.009804
provid,0.009863
provis,0.006186
provok,0.008128
proxi,0.004451
public,0.007997
publish,0.005288
purchas,0.002701
purpos,0.002065
qualif,0.012107
qualiti,0.002366
queen,0.003333
quell,0.014771
question,0.001994
quickli,0.005170
quietli,0.005110
race,0.006240
racial,0.007799
radic,0.017468
rainfal,0.004370
rais,0.004685
ralli,0.004181
ramif,0.005024
rang,0.001906
rank,0.002681
rankl,0.007179
rapid,0.005504
rapidli,0.013387
rate,0.001968
ratifi,0.004047
reach,0.006140
read,0.001749
real,0.002090
reason,0.001807
rebel,0.014638
rebellion,0.025174
rebuf,0.005797
receiv,0.005773
recent,0.001744
recogn,0.008639
recognis,0.003109
recognit,0.002760
recommend,0.011876
reconcili,0.004215
record,0.002123
recoveri,0.003353
redress,0.016442
reduc,0.003816
reduct,0.002898
reelect,0.004102
refer,0.002014
referendum,0.010490
reflect,0.002164
reform,0.041885
refus,0.017636
regain,0.003868
regard,0.007729
regim,0.008805
region,0.019840
rehabilit,0.004343
reignit,0.011970
reject,0.002483
rel,0.005417
relat,0.010864
relax,0.003850
releas,0.002517
remain,0.013326
remov,0.002325
renam,0.010490
renew,0.005890
repeat,0.002907
repeatedli,0.003584
replac,0.008210
report,0.007743
repres,0.019827
represent,0.010164
republ,0.007092
resent,0.004343
resign,0.006884
resist,0.010440
resolv,0.002925
resourc,0.002039
respect,0.003831
respond,0.005433
respons,0.008735
restor,0.002848
restrict,0.002334
result,0.014030
resum,0.003852
resumpt,0.005204
retail,0.007285
retain,0.005297
retreat,0.003890
retriev,0.017955
return,0.018346
revenu,0.002938
revers,0.002773
review,0.002256
revis,0.009029
revit,0.009789
revolut,0.004990
revolutionari,0.006411
rice,0.022793
richer,0.004813
rift,0.004625
right,0.009037
riot,0.040245
rival,0.003327
rivalri,0.004149
river,0.041307
roam,0.005391
robert,0.004429
rock,0.006308
rodney,0.025331
role,0.007058
roman,0.002701
ronald,0.003598
rose,0.003056
rosenberg,0.005255
round,0.003190
royal,0.002657
ruimveldt,0.016231
rule,0.018386
rung,0.005610
rupununi,0.014948
rural,0.009772
rush,0.004399
russian,0.002948
ruz,0.007365
rvauger,0.007740
ryan,0.005156
s,0.018571
said,0.002069
salari,0.003725
sale,0.002934
samuel,0.003580
san,0.006397
sank,0.005008
sat,0.004768
save,0.002860
saw,0.007394
say,0.002182
scarc,0.003857
schism,0.004901
schomburgk,0.014050
school,0.004098
scrutini,0.004578
sea,0.002570
seat,0.062595
second,0.004959
secret,0.003107
secretari,0.003290
sector,0.010562
secur,0.006420
seed,0.003573
seek,0.002260
segment,0.006901
seiz,0.010389
selfgovern,0.007989
selfreli,0.005194
sell,0.002849
semin,0.003852
senat,0.003455
senior,0.003338
sent,0.011167
separ,0.005520
seri,0.002006
serv,0.004010
servant,0.007766
servic,0.015699
set,0.007801
settl,0.014313
settlement,0.020575
settler,0.015160
seven,0.007929
seventeen,0.004732
sever,0.008768
shape,0.002400
share,0.005888
shield,0.003949
shift,0.002441
shoot,0.008542
short,0.004295
shortag,0.014631
shortliv,0.003957
shot,0.007834
shrink,0.004416
sign,0.006924
signatori,0.004846
signific,0.005368
significantli,0.002585
similar,0.001673
simpl,0.002227
sinc,0.002803
singh,0.004976
sir,0.006328
situat,0.006474
sixteen,0.004598
skill,0.002700
slave,0.039748
slaveri,0.014776
slowli,0.003231
small,0.008576
smallest,0.003648
smoothli,0.005128
soar,0.004543
social,0.026749
socialist,0.009408
societi,0.021075
soil,0.006352
sole,0.005690
someth,0.002695
son,0.005368
soon,0.025706
sought,0.013654
soundli,0.005940
south,0.013171
southeast,0.003431
southern,0.002601
southwest,0.003934
sovereignti,0.003380
spain,0.011915
spanish,0.024252
special,0.001753
specul,0.002916
spencer,0.004598
spinner,0.006833
spinoff,0.005194
split,0.002881
splitoff,0.006724
spokesmen,0.006349
spread,0.002477
springboard,0.006108
sprinkl,0.006108
spun,0.005266
stabil,0.005228
stabroeck,0.008115
staff,0.003304
stagnant,0.004931
stalin,0.004609
stand,0.005269
standard,0.003776
start,0.001774
state,0.024531
statu,0.007023
steepli,0.005856
step,0.007131
stevedor,0.013076
stifl,0.005204
stone,0.003130
stood,0.003770
stop,0.002769
strain,0.006968
strata,0.004866
strategi,0.002611
stratum,0.011381
stream,0.003359
street,0.003198
strengthen,0.003170
stretch,0.003533
strident,0.006221
strike,0.037417
strip,0.007357
strong,0.004073
struck,0.003986
structur,0.004913
struggl,0.008815
studi,0.005925
subject,0.001783
submit,0.003511
subsequ,0.002187
substanti,0.002602
succeed,0.002865
success,0.001859
suddenli,0.008317
suedbadillo,0.008115
suffrag,0.004121
sugar,0.054652
sugarcan,0.009549
suggest,0.001962
suicid,0.004019
suppli,0.002285
support,0.030001
surgeri,0.003952
surinam,0.010642
surplu,0.003688
surrend,0.003861
survey,0.002694
suspend,0.003484
suspens,0.004354
suspici,0.004787
sworn,0.008928
sydney,0.004391
sympath,0.005403
sympathi,0.004593
t,0.004878
tactic,0.003727
taken,0.002052
takeov,0.013017
tamper,0.005428
task,0.002600
tax,0.005269
team,0.002927
teekah,0.008115
templ,0.014823
tension,0.012736
tenur,0.004249
tenyear,0.005467
term,0.003973
territori,0.007360
terrorist,0.004242
th,0.014466
theori,0.001643
therebi,0.002901
therefor,0.001905
thi,0.020970
thirteenmemb,0.007597
thirti,0.003779
thirtyfivememb,0.008115
thoma,0.002507
thorn,0.015098
threaten,0.015937
threefold,0.005092
threetotwo,0.008115
thrive,0.003880
throat,0.005041
tie,0.008203
tight,0.004278
time,0.007441
timehri,0.008115
tobacco,0.003949
togeth,0.005799
toler,0.003546
took,0.004379
torrenti,0.007099
total,0.005867
tour,0.004044
town,0.005757
trade,0.030417
tradit,0.003530
tragedi,0.004714
tranquil,0.005277
transfer,0.004797
transit,0.004881
transport,0.002403
treasur,0.004347
treati,0.014637
tri,0.002206
tribun,0.008709
trinidad,0.009761
troop,0.025312
turbul,0.004347
turmoil,0.013112
turn,0.007751
turnout,0.014214
twenti,0.003447
twentieth,0.003307
twentyf,0.015330
twentyfour,0.016739
twentytwo,0.005494
twothird,0.003841
uf,0.068928
umbrella,0.004175
unabashedli,0.006833
unasur,0.006315
unconnect,0.005641
underli,0.002662
underw,0.004067
uneasi,0.010597
unemploy,0.006566
unesco,0.003807
unexpectedli,0.005057
unfavor,0.004968
unhappi,0.004916
unicamer,0.004275
unifi,0.002934
uninterest,0.005508
uninterrupt,0.005266
union,0.035412
unionist,0.005185
unit,0.024036
univers,0.004355
universitair,0.006251
unprepar,0.005816
unquestion,0.005816
unrest,0.007775
unsatisfactori,0.005032
unsuccess,0.007210
unwant,0.004668
upheld,0.004909
upris,0.015570
upstream,0.005119
urban,0.008820
urbanrur,0.006956
urg,0.003699
use,0.004581
usual,0.003373
utopian,0.004873
vacanc,0.005245
vacuum,0.003657
variou,0.003191
vast,0.002814
venezuela,0.051611
venezuelan,0.015799
vere,0.013554
veteran,0.004267
veto,0.009097
vice,0.003245
victori,0.016233
view,0.001796
viewpoint,0.003690
vigor,0.004055
villag,0.006406
vincent,0.004442
violenc,0.020031
violent,0.010384
vision,0.003036
visit,0.002816
vocal,0.008758
voic,0.003341
volum,0.004802
vote,0.029241
voter,0.026323
voyag,0.003834
wa,0.165513
wage,0.003080
walter,0.003343
want,0.002588
war,0.031861
warlik,0.005876
water,0.004324
way,0.004496
weak,0.002784
weaken,0.003511
weapon,0.003112
week,0.002946
wellknown,0.003290
went,0.020569
west,0.038512
western,0.010543
westview,0.005156
wherea,0.002432
whilst,0.003621
white,0.007984
wholli,0.004078
wide,0.001805
widen,0.008725
widespread,0.008079
widow,0.004726
wife,0.003613
win,0.006304
wing,0.015265
wit,0.003414
women,0.002667
won,0.034052
word,0.005837
wore,0.005016
work,0.024736
worker,0.049580
workingclass,0.010597
world,0.011369
worsen,0.008369
wpa,0.052652
wreak,0.006163
year,0.026880
young,0.002602
zedong,0.005298
zone,0.002897
