ab,0.018594
ac,0.020212
account,0.008741
actium,0.033444
ad,0.028414
addit,0.007508
address,0.011176
adriat,0.026062
advoc,0.012740
advocatu,0.039044
aegyptu,0.036842
africaand,0.039044
afterward,0.016348
al,0.012730
alexandr,0.022834
alexandria,0.080924
alexandrini,0.039044
alexandrinu,0.034309
anonymi,0.039044
antoni,0.044030
antoninu,0.059474
appear,0.008596
appian,0.823431
appiani,0.078088
appiano,0.039044
appianu,0.039044
appien,0.078088
appoint,0.012200
approach,0.008541
archiv,0.013660
articl,0.008820
aureliu,0.026133
author,0.008356
autobiographi,0.022420
avail,0.009072
b,0.009830
background,0.012746
barker,0.025171
barrist,0.030082
battl,0.014016
becam,0.008180
becaus,0.020210
befor,0.030572
began,0.008936
behalf,0.016506
bekker,0.031322
believ,0.009255
belong,0.012644
bibliotheca,0.027880
biograph,0.020323
biographi,0.017262
black,0.012077
boast,0.022512
bohn,0.033444
book,0.076548
born,0.025283
britannica,0.018200
broke,0.015346
bryn,0.029574
bud,0.022481
c,0.017129
cad,0.050005
cambridg,0.011870
candido,0.035392
capabl,0.011802
capit,0.009748
carsana,0.039044
carter,0.021165
case,0.014988
centuri,0.015393
certain,0.016407
chiara,0.033069
chief,0.012481
chisholm,0.024434
circumst,0.013769
citizen,0.012266
civil,0.082254
civili,0.029418
claim,0.009098
class,0.019883
classic,0.019806
close,0.008020
collect,0.008743
commento,0.039044
compendium,0.022512
complet,0.016558
comprehens,0.013204
concern,0.008416
conflictbas,0.036842
connect,0.009870
consider,0.010074
constantini,0.039044
coreg,0.035392
corneliu,0.070920
corpu,0.020286
correct,0.012072
countless,0.022271
countri,0.007889
cours,0.010448
cover,0.009870
datum,0.027185
davi,0.017382
definit,0.009479
dell,0.025407
della,0.023443
delluniversit,0.039044
depriv,0.019254
di,0.055972
dictionari,0.014534
did,0.008899
differ,0.019065
digit,0.013386
discord,0.025347
distil,0.021470
divis,0.010390
domain,0.011708
dure,0.020702
e,0.009714
earliest,0.024127
ed,0.050519
edit,0.022593
editio,0.032405
edizioni,0.031829
educ,0.009194
egypt,0.060968
emperor,0.063671
empir,0.020142
encyclopdia,0.018721
end,0.015782
english,0.020104
enormouslyso,0.039044
equestrian,0.031089
especi,0.016761
et,0.012406
ethnograph,0.023110
event,0.018785
exampl,0.006463
excessu,0.039044
expect,0.009916
extern,0.005643
extrem,0.010353
facilit,0.013011
facolt,0.035392
faction,0.017047
famili,0.009993
fell,0.013939
filosofia,0.030657
fisci,0.039044
flourish,0.015884
follow,0.006173
foreign,0.010046
fragment,0.015179
french,0.010386
friend,0.030658
fronto,0.106178
goukowski,0.078088
greek,0.054644
guerr,0.055548
gulf,0.017459
ha,0.005146
hadrian,0.059149
half,0.010623
harmondsworth,0.029120
hi,0.069199
histoir,0.046364
histor,0.026008
histori,0.044997
historia,0.088622
historian,0.013005
honorif,0.028101
horac,0.023110
howev,0.012527
hugh,0.018110
ii,0.010100
iii,0.014759
import,0.006582
incorpor,0.021196
increas,0.007421
infer,0.014831
inform,0.015345
internet,0.012660
interpret,0.010296
introduct,0.010231
involv,0.007481
ioannem,0.039044
j,0.009692
jame,0.010884
job,0.013327
john,0.008778
knightli,0.034817
knowledg,0.009706
known,0.012896
lacuscurtiu,0.034309
later,0.007673
latin,0.036382
leigh,0.026508
lepidu,0.073685
letter,0.050948
librari,0.024243
libro,0.029574
lie,0.012641
life,0.016588
like,0.006891
link,0.005610
literari,0.016180
litterateur,0.036842
littl,0.009875
liviusorg,0.034309
livr,0.055138
lost,0.011212
ludwig,0.017716
mainli,0.010710
marcu,0.040797
mawr,0.030082
member,0.008231
men,0.011963
mendelssohn,0.029574
mention,0.012490
meticul,0.026062
middl,0.010637
monograph,0.020140
multitud,0.020360
mytholog,0.018044
notabl,0.009899
note,0.014876
occur,0.016393
octaviusshortli,0.039044
offic,0.041538
onli,0.035633
open,0.008832
order,0.007109
orient,0.013443
origin,0.014299
parent,0.014529
partitionedbi,0.039044
paul,0.022429
pavia,0.028456
pay,0.012044
penguin,0.019402
peopl,0.014226
period,0.030663
phillipp,0.032724
pin,0.021879
pisa,0.025171
piu,0.048868
place,0.007296
plead,0.049574
polit,0.008097
posit,0.007646
possess,0.011593
possibl,0.007545
pp,0.024122
practis,0.019463
praeterea,0.039044
precis,0.012186
present,0.007397
press,0.008707
primari,0.009765
primarili,0.010112
princep,0.030082
princip,0.011556
probabl,0.021231
procur,0.061310
provinc,0.027352
pubblicazioni,0.039044
public,0.014901
publio,0.039044
quarrel,0.024063
question,0.009290
rank,0.012490
reader,0.015999
real,0.009738
receiv,0.008964
recommend,0.013830
refer,0.004691
regard,0.009001
reign,0.032447
relationship,0.008956
reli,0.011305
remain,0.015519
remnant,0.018760
republ,0.011012
request,0.014260
resembl,0.014800
result,0.006535
retriev,0.013939
reveal,0.012341
review,0.021026
rhomaik,0.039044
romain,0.063659
roman,0.151027
romana,0.054746
rome,0.080099
schweighus,0.039044
second,0.007700
section,0.011625
sequenc,0.013025
seri,0.009347
shakespear,0.023074
signific,0.008335
sinc,0.013059
smith,0.013834
solv,0.012017
sometim,0.008500
son,0.012502
sourc,0.049282
special,0.008166
stand,0.012272
state,0.011427
storico,0.033855
strachandavidson,0.039044
strife,0.021519
structur,0.015258
surviv,0.033786
syria,0.019196
tell,0.014708
testimoni,0.021009
teubneriana,0.039044
text,0.022528
th,0.007487
themselvesoctaviusfirst,0.039044
thi,0.043957
time,0.005777
titl,0.011721
today,0.009380
tome,0.024734
took,0.010200
torren,0.030868
trajan,0.054190
tran,0.020034
translat,0.022483
turmoil,0.020360
uncertain,0.017262
unclear,0.017153
uncrit,0.025468
unit,0.006586
univers,0.006763
upper,0.013186
use,0.016006
usqu,0.034817
valuabl,0.015329
variou,0.007433
veri,0.007457
vol,0.014033
w,0.010956
wa,0.048186
war,0.055656
warsbook,0.039044
wealthi,0.017038
wellknown,0.015329
went,0.011976
white,0.012397
wikisourc,0.025593
william,0.020927
won,0.013218
work,0.057614
world,0.006620
wors,0.017957
write,0.020419
written,0.020676
wrote,0.011125
x,0.011561
xiiixvii,0.039044
xv,0.024198
xxiii,0.028979
year,0.006590
