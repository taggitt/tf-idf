abolish,0.030909
abroad,0.025698
abstain,0.009255
access,0.004093
accord,0.002958
accus,0.005933
achiev,0.014887
acqui,0.013878
act,0.006762
activ,0.011808
ad,0.007551
addit,0.002993
administr,0.016546
adopt,0.024496
adoptionin,0.015563
advoc,0.025392
affair,0.004891
age,0.011115
agenda,0.006891
agreement,0.004515
ahead,0.006906
alexand,0.005668
allow,0.005884
alreadi,0.004310
alter,0.010613
amend,0.043672
amnesti,0.017059
andrej,0.011726
ani,0.005214
announc,0.004968
ant,0.008028
antonio,0.007704
appeal,0.027549
appli,0.006325
applic,0.014132
appoint,0.043770
approxim,0.004031
april,0.004526
arm,0.009434
arrang,0.004823
arrest,0.006502
assassin,0.007002
assembl,0.009237
assign,0.005314
attorney,0.056866
august,0.013487
austriahungari,0.029517
austrian,0.007086
austrohungarian,0.010104
author,0.006662
autonom,0.005982
autonomi,0.018545
autonomist,0.034333
bajamonti,0.015563
baji,0.014373
ban,0.023843
banovina,0.026663
banski,0.015069
base,0.010311
basi,0.007370
becam,0.006521
becaus,0.002685
becom,0.005711
befor,0.006093
begin,0.006956
belong,0.015121
bjelovar,0.015563
bodi,0.007159
border,0.005254
boundari,0.014940
branch,0.015174
branko,0.011853
breach,0.008071
breakup,0.025705
bridg,0.005893
brought,0.004397
budget,0.022335
budia,0.015563
calcul,0.004552
came,0.003906
campaign,0.005363
cancel,0.006767
candid,0.033019
capit,0.003885
case,0.011949
cast,0.024275
central,0.003349
centuri,0.003068
challeng,0.004268
chamber,0.031699
chang,0.021549
charg,0.009221
chief,0.009950
chosen,0.005459
citi,0.057767
citizen,0.009779
civil,0.016394
claim,0.003626
close,0.003197
closer,0.005912
coalit,0.046318
collaps,0.005263
combat,0.006271
command,0.010026
commenc,0.007228
commerci,0.018311
committe,0.010253
commun,0.006249
communist,0.082511
complet,0.003300
complianc,0.007669
compos,0.004533
compound,0.005384
compromis,0.006870
conduct,0.004095
conflict,0.004450
conquest,0.006438
consecut,0.008079
consent,0.022002
consequ,0.004063
consider,0.004015
consist,0.009595
constitu,0.061555
constitut,0.128287
continu,0.008686
contribut,0.003507
control,0.003174
corrupt,0.005867
council,0.095861
count,0.005450
counti,0.248900
countri,0.022013
court,0.167704
crime,0.005928
crimin,0.012106
croat,0.044807
croatia,0.505539
croatian,0.410714
croatianhungarian,0.013676
croatiaslavonia,0.041636
croatserb,0.015069
crownappoint,0.014108
culmin,0.006331
current,0.003059
cvetkovimaek,0.014108
czech,0.007658
dabevikuar,0.015563
dali,0.009901
dalmatia,0.067211
damir,0.015069
davor,0.015069
day,0.007212
deal,0.003849
death,0.004185
decemb,0.021926
decentralis,0.009739
decid,0.009314
decis,0.012087
declar,0.027693
dedic,0.011480
defeat,0.011307
defenc,0.005817
defin,0.022956
demand,0.012799
democrat,0.042141
depart,0.004189
deputi,0.031997
despit,0.004052
deterior,0.007037
determin,0.006637
develop,0.002389
dhondt,0.011789
dictatorship,0.015444
direct,0.003173
directli,0.007679
divid,0.011368
divis,0.016567
dom,0.010446
domest,0.004951
draen,0.015563
drakovi,0.014373
dramat,0.005396
dravno,0.014686
dubrovnik,0.012064
dure,0.005501
duti,0.005686
dvori,0.015069
earli,0.002877
earlier,0.004251
economi,0.003929
effect,0.008384
effort,0.003984
eightyear,0.010388
elect,0.288552
elector,0.018516
eleventh,0.035749
elig,0.007197
emerg,0.003687
emphasis,0.007752
empir,0.008029
enact,0.031888
end,0.003145
endors,0.006809
enforc,0.005393
entir,0.007463
era,0.004545
escal,0.007452
establish,0.024563
ethnic,0.011354
eu,0.006264
eugen,0.007033
europ,0.003901
european,0.003583
event,0.011232
evolv,0.004722
execut,0.043581
exercis,0.004943
exist,0.002860
exit,0.007546
extra,0.006589
fact,0.003652
faction,0.006795
februari,0.004674
feder,0.004580
federalis,0.013676
femal,0.005776
figur,0.004260
fiveyear,0.021018
fix,0.004566
follow,0.007382
forbidden,0.007607
forc,0.009250
foreign,0.020024
form,0.007054
formal,0.003849
formula,0.005372
fouryear,0.078269
fragment,0.006050
framework,0.013223
franjo,0.051195
frano,0.015563
freedom,0.004856
fulli,0.004638
gain,0.007485
gener,0.006631
gospi,0.015563
govern,0.078989
grabarkitarovi,0.030139
grant,0.014148
greater,0.003874
greguri,0.015069
grow,0.003944
guid,0.008699
ha,0.020513
half,0.004234
harmonis,0.010475
hdssb,0.015563
hdz,0.093322
hdzled,0.015563
head,0.027801
hear,0.012974
held,0.032736
heritag,0.006292
hi,0.008275
highest,0.009422
histor,0.003455
histori,0.002562
hn,0.020834
holder,0.020982
hour,0.005406
howev,0.004993
hrvatin,0.015563
hsl,0.026990
hss,0.098437
hundr,0.004674
hungari,0.015236
hungarian,0.007931
idssdp,0.015563
ii,0.008052
inaugur,0.007107
incapacit,0.010765
includ,0.008542
independ,0.041077
individu,0.003173
influenc,0.010165
instanc,0.004166
instead,0.003603
institut,0.003255
intens,0.005109
intern,0.008087
introduc,0.010826
irredent,0.012798
issu,0.009903
istria,0.013331
italian,0.011545
ivan,0.033092
ivica,0.013676
ivo,0.011726
janko,0.013044
januari,0.035123
jasna,0.015563
jelai,0.014108
jointli,0.007422
josip,0.033341
josipovi,0.029372
judg,0.034086
judgment,0.006519
judici,0.041342
judiciari,0.027379
juli,0.008887
june,0.017928
juraj,0.030139
jurisdict,0.006243
king,0.004899
kingdom,0.036828
kn,0.031000
kolinda,0.030139
kovai,0.015069
krstievi,0.015069
kukuljevi,0.015563
kvaternik,0.031127
land,0.003892
largest,0.008496
late,0.003561
later,0.003058
latest,0.020610
law,0.024787
lead,0.003068
leader,0.004497
leadership,0.005580
leagu,0.012621
led,0.036514
left,0.004099
legal,0.025465
legalis,0.010598
legisl,0.084762
legislatur,0.025170
level,0.003075
liber,0.004637
limit,0.006282
line,0.003681
link,0.004472
list,0.008579
live,0.006651
local,0.024384
looser,0.011201
loss,0.004498
lower,0.003800
lowerrank,0.012392
maek,0.014373
main,0.009767
maintain,0.007172
major,0.040927
marijan,0.015563
martina,0.013495
matter,0.007737
maurani,0.014686
maximum,0.005592
mayor,0.046810
meet,0.004161
member,0.052501
membership,0.005664
mesi,0.014108
middl,0.012720
miko,0.014686
militari,0.004160
minist,0.069603
minor,0.052352
misdemeanour,0.058745
mladen,0.013182
moder,0.005891
modern,0.009254
movement,0.003546
mp,0.075848
multiparti,0.026393
multiseat,0.022227
municip,0.094001
nation,0.045595
nationwid,0.007814
negoti,0.005383
neoabsolut,0.014686
new,0.002297
nomin,0.005367
notabl,0.003946
note,0.002965
number,0.018009
oath,0.008775
oblast,0.010417
obsolet,0.008021
octob,0.022235
offenc,0.009075
offic,0.045535
offici,0.003904
ogulin,0.015563
omejec,0.015563
onc,0.003842
onli,0.002367
open,0.003520
oper,0.009733
oppos,0.008595
opposit,0.012694
organis,0.014607
origin,0.002850
osijek,0.014686
ottoman,0.007273
outsid,0.003923
parliament,0.093362
parliamentari,0.082453
parti,0.178844
particular,0.006438
peac,0.009919
peasant,0.007170
peopl,0.022683
period,0.012223
perman,0.009967
petit,0.007607
place,0.002908
plenkovi,0.014373
plural,0.013228
poega,0.015563
polici,0.011417
polit,0.064555
poll,0.014316
popular,0.014717
power,0.048286
practic,0.003155
pre,0.008568
predsjednikica,0.031127
prefect,0.089307
present,0.002948
presid,0.089561
presidentelect,0.011853
presidenti,0.048245
prevent,0.004216
pribievi,0.015563
primarili,0.004031
prime,0.042643
prison,0.006259
privaci,0.008721
procedur,0.015407
proceed,0.005396
professor,0.005270
promin,0.004624
proport,0.024379
propos,0.010915
protect,0.003930
provid,0.007849
provis,0.005744
provok,0.007546
public,0.008909
publicli,0.006310
pursuant,0.009759
raan,0.024971
radi,0.019803
raki,0.013878
rang,0.003540
ranko,0.015069
recaptur,0.008501
recent,0.006477
recognis,0.011548
reestablish,0.007124
refer,0.001870
referenda,0.021601
reflect,0.008038
reform,0.009150
regard,0.003588
regist,0.005799
regul,0.004427
regular,0.010247
reintroduc,0.008628
rel,0.003353
relat,0.002521
releas,0.004674
relev,0.004891
relief,0.013336
remain,0.009279
renam,0.006493
repeal,0.008464
replac,0.003811
report,0.003594
repres,0.033748
represent,0.023591
republ,0.048285
republik,0.024441
requir,0.008832
resid,0.004955
respect,0.003556
respons,0.016219
result,0.002605
resum,0.007153
retain,0.004918
return,0.003785
revis,0.005589
reviv,0.005943
right,0.016781
round,0.029624
rule,0.027312
runoff,0.042649
s,0.007957
sabor,0.186849
sakcinski,0.015563
savka,0.015563
scheme,0.005629
sdp,0.057758
sdpled,0.015069
seat,0.011069
second,0.009208
sector,0.004903
secur,0.007947
select,0.004066
semipresidenti,0.019803
sentiment,0.007268
septemb,0.013531
serb,0.060482
serv,0.026061
session,0.014231
set,0.005794
settlement,0.005458
seven,0.004908
seventeen,0.008786
seventi,0.008753
sever,0.008140
sfr,0.012392
shape,0.004457
signific,0.003322
significantli,0.014400
silenc,0.008340
similar,0.003108
sinc,0.020822
singl,0.010450
singleparti,0.010950
situat,0.004007
sixtyseven,0.015563
slavic,0.009115
slovak,0.010279
sloven,0.033472
social,0.019867
socialist,0.011646
societi,0.003261
soon,0.009546
sought,0.005070
south,0.004076
sovereignti,0.006277
speaker,0.006684
special,0.003255
specialis,0.015082
specif,0.003114
splinter,0.009488
spring,0.011742
standard,0.003505
starevi,0.030139
state,0.036440
statehood,0.009627
station,0.005686
statu,0.013041
stier,0.013878
stjepan,0.039995
strongli,0.010033
strossmay,0.031127
subdivid,0.021397
subdivis,0.007646
subsequ,0.008124
sud,0.045179
sudbeno,0.015563
superior,0.028968
supervis,0.012447
supilo,0.015563
suppress,0.005960
suprem,0.037312
svetozar,0.013182
switch,0.006391
task,0.009657
technic,0.004629
term,0.049191
territori,0.018224
th,0.002984
themselv,0.007722
thi,0.005840
thirteen,0.015616
thirtythre,0.011201
threetier,0.034998
threshold,0.007250
ticket,0.008753
tie,0.010154
time,0.018424
toler,0.006586
took,0.008132
trade,0.003765
tradit,0.003277
traffic,0.006877
transfer,0.008908
transleithanian,0.015563
transport,0.004462
tri,0.004097
trial,0.005939
tripalo,0.015563
trivial,0.007770
tuman,0.041636
twenti,0.019207
twentythre,0.011608
twentytwo,0.010202
type,0.003129
unconstitut,0.019219
unicamer,0.015876
unif,0.007402
uniform,0.006035
uninterrupt,0.009778
union,0.019339
unit,0.013127
unitari,0.016339
uniti,0.005721
univers,0.002695
unoppos,0.010388
upanija,0.015563
upanijski,0.015563
upheav,0.008482
uskok,0.014373
ustavni,0.031127
varadin,0.014686
vari,0.003837
veri,0.002972
vest,0.006827
vije,0.014373
violat,0.005759
vlada,0.027352
vladko,0.014686
vote,0.064168
voter,0.013965
vrhovni,0.031127
vukovar,0.013331
wa,0.070430
war,0.018488
win,0.005852
withdraw,0.006256
won,0.068499
work,0.002551
world,0.005277
year,0.002626
yugoslav,0.040321
yugoslavia,0.087978
zagreb,0.056965
