abil,0.010657
abolish,0.008092
abolit,0.010172
absolut,0.006569
accept,0.009724
accord,0.015490
accredit,0.009809
accus,0.007766
achiev,0.004872
act,0.013279
action,0.009517
activ,0.003864
actual,0.004956
addit,0.007836
adigesdtirol,0.018815
administ,0.022776
administr,0.010830
adopt,0.005344
adversari,0.022976
affect,0.005060
age,0.014550
agre,0.011678
agricultur,0.011512
aldo,0.028878
align,0.008366
alleanza,0.018468
alleg,0.024024
alli,0.021459
allianc,0.050702
alloc,0.015457
allow,0.003851
alter,0.013893
altern,0.004976
amato,0.017666
american,0.004388
anarchist,0.011403
andreotti,0.037630
ani,0.020479
anni,0.013046
anticonstitut,0.039454
antonio,0.010085
aosta,0.033509
appeal,0.014425
appear,0.004485
appoint,0.038199
appointe,0.013563
approach,0.004457
approv,0.013306
april,0.035549
arbitrarili,0.010694
area,0.003846
aris,0.006318
arm,0.012350
art,0.005924
articl,0.018411
ask,0.006402
assassin,0.018332
assembl,0.018137
attack,0.006244
attempt,0.022088
attract,0.012463
august,0.005885
aurelio,0.016609
authoris,0.022976
authorit,0.010808
autonom,0.007831
autonomi,0.016185
avanguardia,0.019727
averag,0.017354
azeglio,0.059181
ballot,0.032523
bank,0.016802
bare,0.009539
base,0.013498
bear,0.007247
becam,0.012805
becaus,0.007030
befor,0.011965
began,0.013989
belong,0.019795
bend,0.010918
berlingu,0.039454
berlusconi,0.186008
bersani,0.018815
beset,0.014283
bettino,0.019727
bicamer,0.021038
bipolar,0.038175
black,0.006302
blame,0.018553
blind,0.009557
bodi,0.004686
bologna,0.012465
bomb,0.070733
branch,0.054627
breach,0.010566
break,0.006437
brigad,0.070292
broke,0.008008
brought,0.011512
build,0.004941
bullet,0.011546
businessmen,0.011361
cabinet,0.054998
calm,0.010986
candid,0.007204
capabl,0.006159
capac,0.006483
carlo,0.026700
carri,0.010083
casa,0.013355
case,0.027374
center,0.014965
centerleft,0.068373
centerright,0.042140
centreleft,0.113110
centreright,0.068184
centrist,0.077939
ceo,0.011166
certain,0.012842
challeng,0.011174
chamber,0.024898
chang,0.014104
charg,0.018107
chart,0.008909
charter,0.016827
che,0.014439
chernobyl,0.014186
choic,0.018436
chosen,0.007147
christian,0.084859
ciampi,0.098636
circl,0.007536
circumst,0.007185
citi,0.005401
citizen,0.038404
civil,0.010730
clement,0.011229
clich,0.015050
close,0.004185
coalit,0.128848
code,0.006052
coerc,0.012989
coincid,0.008413
cold,0.014912
collaps,0.006890
collect,0.004562
come,0.004473
comfort,0.009936
commanderinchief,0.021725
commenc,0.009462
commiss,0.006227
commun,0.004090
communist,0.185165
commut,0.010391
compagni,0.013599
compar,0.004626
competitor,0.009690
complet,0.004320
compli,0.020626
compos,0.011869
composit,0.006484
compris,0.006399
compromesso,0.019727
compromis,0.035973
compulsori,0.009893
comrad,0.014138
concept,0.004365
conclud,0.006397
condit,0.008754
conduct,0.005361
confer,0.006157
confid,0.056014
conflict,0.005826
connect,0.005150
consecut,0.010576
consequ,0.010638
consid,0.003648
consider,0.005257
consiglio,0.017666
consist,0.004186
constitu,0.029301
constitut,0.118544
constitution,0.012934
construct,0.004844
consult,0.007223
contain,0.004487
contest,0.008230
continu,0.007580
contribut,0.004591
controversi,0.006164
correct,0.006299
corrupt,0.015360
cort,0.012880
cossiga,0.019727
costituzional,0.019225
council,0.051336
countri,0.012350
coup,0.008519
cours,0.005452
court,0.077134
craxi,0.059181
creat,0.007652
creation,0.005694
crime,0.007761
crisi,0.025822
critic,0.004601
cultur,0.004703
current,0.012014
curtail,0.011348
daisi,0.016107
dalema,0.037630
daniel,0.007780
day,0.023603
dc,0.017324
deal,0.005039
death,0.005478
debat,0.005823
debt,0.007254
decadelong,0.015517
decemb,0.011481
decis,0.010548
declar,0.030210
decre,0.008983
defeat,0.007401
defenc,0.007615
degre,0.015369
del,0.009444
deleg,0.008832
dell,0.013258
demo,0.013491
democraci,0.041808
democrat,0.134850
depriv,0.010047
deputi,0.050265
deriv,0.004756
design,0.004594
despit,0.010609
destabil,0.011652
destroy,0.006541
develop,0.006257
di,0.009735
did,0.009287
diermeier,0.018815
differ,0.006632
difficult,0.005456
dini,0.016223
diplomat,0.007432
direct,0.008308
director,0.007521
disapprov,0.011731
disast,0.008641
disband,0.010873
disciplinari,0.011561
discredit,0.010884
discuss,0.005104
disench,0.014785
dissolv,0.031078
distinct,0.004818
doi,0.016563
domin,0.020591
draft,0.008073
dramat,0.007064
drew,0.008621
dtre,0.015997
dure,0.010803
duti,0.022332
earli,0.007533
earlier,0.005565
econom,0.004121
edg,0.008217
effect,0.003658
effort,0.005215
eighteen,0.022357
elect,0.288856
elector,0.072719
elig,0.018843
emerg,0.033788
emiliaromagna,0.018468
employ,0.004995
end,0.004117
enjoy,0.007170
enrico,0.036521
enter,0.011012
entir,0.004885
entranc,0.010605
equilibrium,0.007871
era,0.005950
eraslan,0.018815
especi,0.013120
establish,0.032155
ethic,0.006931
eurocommun,0.016754
europ,0.010213
european,0.004690
event,0.009802
everi,0.004712
everyon,0.008695
excerpt,0.010409
exclud,0.007085
execut,0.039936
exercis,0.038825
exist,0.011235
experi,0.004631
exploit,0.007011
expos,0.007544
express,0.004879
extend,0.004967
extens,0.015257
extent,0.006197
extrem,0.010805
extremist,0.025400
face,0.005652
facilit,0.006789
fact,0.009563
failur,0.006552
falter,0.013388
famou,0.006221
farreach,0.011606
fascism,0.012315
fascist,0.022332
fashion,0.007926
favor,0.006220
februari,0.018356
feder,0.005996
ferdinando,0.014548
fierc,0.010356
fifti,0.028637
final,0.004871
fiveparti,0.017666
follow,0.022551
fontana,0.013636
forbidden,0.009958
forc,0.028256
foreign,0.005242
forg,0.010017
form,0.024626
formal,0.005039
format,0.005492
forti,0.010055
fortytwo,0.014981
forza,0.051229
fragment,0.007920
franc,0.005765
francesco,0.011829
freedom,0.050861
freeli,0.008758
frequenc,0.007696
frequent,0.005661
friulivenezia,0.018815
function,0.004123
fundament,0.005157
gain,0.004899
gener,0.005787
gentiloni,0.019727
germani,0.006064
gianfranco,0.017256
giorgio,0.066943
giulia,0.017452
giuliano,0.017076
giulio,0.013792
giusepp,0.012115
given,0.004039
gladio,0.039454
govern,0.164678
governor,0.007392
gradual,0.006474
grand,0.007822
grant,0.012347
great,0.004539
greek,0.005702
grow,0.005164
guarante,0.022162
guardian,0.009950
guess,0.011294
ha,0.026854
hand,0.004990
head,0.046792
held,0.009523
hereditari,0.009638
hi,0.043332
high,0.016311
highest,0.006167
hisher,0.011576
histor,0.013571
histori,0.020126
historian,0.006786
hlya,0.018815
hold,0.009701
honor,0.008331
honorari,0.010382
hotli,0.013832
hous,0.027012
howev,0.022879
hung,0.012556
idea,0.004520
ideat,0.014849
ideolog,0.007607
ii,0.015811
impeach,0.013165
imped,0.010851
imperfect,0.009914
impli,0.006261
imposimato,0.019727
includ,0.013978
inclus,0.008295
incorpor,0.005530
increas,0.003872
increasingli,0.005937
inde,0.006847
indefinit,0.009462
independ,0.012409
indestruct,0.013290
influenc,0.008871
influencecollect,0.019727
initi,0.004375
innov,0.006975
inquisitori,0.017256
insist,0.008217
inspir,0.006663
instabl,0.008707
instead,0.004717
institut,0.004261
instruct,0.007581
intern,0.010587
introduc,0.004724
introduct,0.005338
investig,0.016929
involv,0.019520
irrit,0.012059
issu,0.008643
itali,0.224674
italia,0.043817
italian,0.249369
italicum,0.019727
iv,0.008741
januari,0.011494
joint,0.014182
journal,0.010359
journalist,0.009479
judg,0.029747
judgement,0.010180
judici,0.030925
judiciari,0.053762
juli,0.011633
june,0.011734
jurisdict,0.008173
just,0.004695
justic,0.040836
kidnap,0.022862
kill,0.013296
king,0.006414
kingdom,0.005356
known,0.006729
krata,0.019727
labor,0.006611
labour,0.006816
lack,0.010268
lamberto,0.018815
lanaro,0.018468
landscap,0.041978
larg,0.010645
largest,0.016683
later,0.020021
law,0.060842
lawmak,0.012253
lead,0.016069
leader,0.035325
leadership,0.021915
leagu,0.008261
led,0.047800
left,0.048304
leftist,0.020748
leftlean,0.014664
leftright,0.013636
leftw,0.044418
lega,0.032946
legisl,0.043151
legislatur,0.008237
legitim,0.016726
legitimaci,0.020609
lent,0.011129
lepr,0.018468
letta,0.039454
liber,0.006070
liberalsoci,0.019727
libert,0.014914
life,0.004328
like,0.007192
limit,0.008223
linguist,0.007915
list,0.011231
load,0.009239
longer,0.005552
loss,0.005888
lost,0.011701
luigi,0.024008
lunion,0.016909
magnat,0.013713
main,0.012786
mainli,0.005589
maintain,0.004694
major,0.042861
majoritarian,0.016909
make,0.006847
mani,0.027387
maoist,0.013915
march,0.005582
margin,0.014756
mario,0.035004
mark,0.005405
massacr,0.009816
massimo,0.014138
massiv,0.007163
mastella,0.019727
mattarella,0.019727
matteo,0.028004
meanwhil,0.008140
media,0.006175
member,0.025772
merlo,0.018168
messag,0.008366
met,0.006884
metal,0.007007
milan,0.022589
minimum,0.007343
minist,0.188305
ministri,0.021716
minor,0.012460
missil,0.010491
mistaken,0.011116
mix,0.006475
moder,0.015425
modern,0.004038
modifi,0.013138
monarch,0.008891
monarchi,0.008629
month,0.017844
monti,0.045366
moratorium,0.013422
moretti,0.016344
moro,0.063991
moscow,0.009645
movement,0.027854
mr,0.009051
msi,0.032689
multiparti,0.017275
murder,0.008550
napoleon,0.009181
napolitano,0.098636
narrow,0.007602
narrowli,0.020927
nation,0.055957
nationwid,0.010229
nato,0.010180
nazional,0.030701
nd,0.006263
near,0.005535
need,0.004047
neofascist,0.054505
network,0.005628
new,0.054129
news,0.007348
nicknam,0.021368
nicola,0.010085
nord,0.028469
north,0.005345
notabl,0.010331
note,0.003881
novemb,0.006038
nuclear,0.013758
number,0.010103
nuovo,0.016223
observ,0.004443
obtain,0.015698
octob,0.017464
odd,0.009497
offic,0.021675
offici,0.010222
old,0.005570
older,0.042389
oliv,0.028637
onehalf,0.012675
onethird,0.020458
onli,0.030990
openli,0.010196
oper,0.008494
oppos,0.005626
opposit,0.005539
order,0.011129
ordin,0.009929
ordinari,0.007341
organ,0.011016
origin,0.007461
oust,0.021038
oversea,0.016059
owner,0.007615
palmiro,0.019727
paolo,0.012627
paralysi,0.012934
pardon,0.012004
parliament,0.190117
parliamentand,0.019727
parliamentari,0.015419
parliamentarian,0.012651
parti,0.249728
partial,0.006193
particip,0.005310
partli,0.007238
pasquino,0.018168
pass,0.015690
past,0.010681
pci,0.031211
peopl,0.040829
peoplethey,0.019727
perform,0.009465
period,0.012000
persh,0.016107
person,0.013215
phaseout,0.016223
piazza,0.015195
pier,0.012533
pinelli,0.018168
piombo,0.019727
place,0.003807
plant,0.011689
plateau,0.010940
plu,0.007574
plummet,0.012273
point,0.011715
pole,0.009325
policemen,0.013422
polici,0.004982
polit,0.126761
politician,0.016153
poor,0.006656
popular,0.009633
posit,0.015959
possibl,0.007875
postelect,0.017452
postwar,0.027468
postworld,0.011621
power,0.079013
practic,0.008260
pragmat,0.018820
predecessor,0.016835
preiti,0.019727
premiership,0.014664
presenc,0.005837
present,0.007720
presid,0.212151
president,0.015697
previou,0.011643
previous,0.011770
primarili,0.005277
prime,0.130254
principl,0.004634
privileg,0.008810
problem,0.004128
procedur,0.006723
process,0.003649
prodi,0.141332
professor,0.006899
progress,0.005261
promulg,0.019688
propaganda,0.010321
proper,0.006926
properti,0.004674
proport,0.019148
propos,0.009526
prosecutor,0.012213
prove,0.005957
provid,0.003425
psi,0.011747
publica,0.015793
publish,0.008569
pulit,0.019225
pulitedemand,0.019727
push,0.014682
quarterli,0.009438
radic,0.007077
railway,0.009135
raison,0.014605
ratifi,0.009837
reach,0.009951
read,0.004252
reagan,0.011591
receiv,0.009355
red,0.040718
reduc,0.009276
reelect,0.009972
refer,0.009793
referendum,0.051000
reform,0.011978
reformist,0.011731
region,0.056996
regul,0.011592
reincarn,0.012115
reintroduc,0.011294
reject,0.018107
rel,0.004389
relationship,0.004673
remain,0.012147
renzi,0.037630
repeat,0.007066
repeatedli,0.008712
replac,0.004989
report,0.004705
repres,0.044178
represent,0.018529
republ,0.109180
requir,0.015417
reshuffl,0.013527
resign,0.025100
resist,0.012688
resourc,0.004956
respect,0.004656
respons,0.021232
rest,0.005736
result,0.003410
retract,0.012533
return,0.014865
reveal,0.006440
review,0.010971
right,0.026361
rightw,0.033884
riot,0.009782
role,0.008578
roman,0.006567
romano,0.044172
ronald,0.008745
roughli,0.006868
rule,0.022346
s,0.010417
salari,0.009056
sardinia,0.013752
saw,0.011983
sbagliano,0.019727
scandal,0.020678
scienc,0.004117
scrutini,0.011129
seat,0.007245
second,0.008036
secondarili,0.013713
secret,0.007554
secretari,0.007999
seemingli,0.009607
select,0.005323
selfrenew,0.016107
senat,0.092392
send,0.007656
sens,0.005037
sentenc,0.008370
separ,0.004473
separatist,0.011637
sergio,0.013527
seri,0.004877
serv,0.019494
servant,0.009438
servic,0.004770
session,0.018629
set,0.003792
seven,0.006424
sever,0.003552
shall,0.008937
shift,0.011866
shortliv,0.009619
shot,0.009521
sicili,0.022510
signific,0.008699
silvio,0.087292
sinc,0.027258
situat,0.005246
sixtyfour,0.018168
small,0.004169
socal,0.013750
social,0.013004
socialdemocrat,0.014283
socialist,0.083851
societi,0.004269
solemnli,0.016107
sovereignti,0.016434
soviet,0.014760
special,0.004261
specif,0.008153
spectrum,0.008057
ss,0.010472
stabil,0.006354
stabl,0.006705
stanc,0.009392
start,0.004313
state,0.050684
station,0.007444
statut,0.010009
stay,0.022375
staybehind,0.018815
step,0.011555
stigmatis,0.016909
stock,0.006573
stop,0.020195
storico,0.017666
strategi,0.012694
strong,0.004951
strongli,0.006567
structur,0.003981
student,0.011599
studi,0.003601
subject,0.008669
submit,0.008534
substanti,0.006325
succeed,0.013932
suffrag,0.030052
sunday,0.011308
support,0.040513
suprem,0.020933
surpass,0.009619
surviv,0.005876
swear,0.013792
sweep,0.010436
swept,0.011141
sworn,0.010851
sympathet,0.010895
sympathi,0.011166
tambroni,0.019727
tangentopoli,0.037630
technic,0.006060
temporarili,0.009392
tension,0.015479
term,0.006439
terror,0.009070
terrorist,0.041253
thi,0.030583
thirdlongest,0.019727
thousand,0.006009
thu,0.003983
time,0.024118
tobagi,0.019727
togeth,0.004698
togliatti,0.018468
took,0.015969
touch,0.008993
trade,0.004929
tranfaglia,0.018468
transform,0.005577
transit,0.005932
treason,0.012115
treasuri,0.009056
treat,0.005966
treati,0.007115
tree,0.021056
trend,0.006508
trentinoalto,0.018815
tri,0.016090
trial,0.023325
turn,0.004710
turnov,0.021903
tuscani,0.014283
twelv,0.008988
twenti,0.008381
twentyf,0.024843
twothird,0.018673
unaccept,0.011104
uncov,0.009690
underw,0.009886
unelect,0.014785
union,0.025317
unit,0.020621
uniti,0.037449
univers,0.017645
unless,0.007635
use,0.002784
usual,0.008201
valley,0.015760
valu,0.004367
vari,0.005023
veltroni,0.018815
veri,0.003891
vest,0.008937
victori,0.015783
vincenzo,0.014785
vinciguerra,0.019727
violat,0.007539
vote,0.109848
voter,0.091407
vow,0.011986
wa,0.108961
walter,0.016255
war,0.029042
washington,0.007502
weapon,0.007566
went,0.006249
werent,0.013832
western,0.020502
wide,0.008777
withdrew,0.009576
won,0.027591
world,0.003454
worth,0.007715
year,0.048143
youngest,0.046925
