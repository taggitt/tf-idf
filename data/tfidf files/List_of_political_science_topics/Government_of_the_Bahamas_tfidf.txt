acp,0.045360
act,0.017433
advic,0.199182
affect,0.019932
agreement,0.023283
agricultur,0.022672
america,0.021377
american,0.034565
ani,0.013443
anoth,0.014671
answer,0.027160
appel,0.045894
applic,0.018216
appoint,0.075225
assembl,0.071437
associ,0.014900
away,0.023235
bahama,0.345550
bahamian,0.126018
bank,0.044117
base,0.026582
bay,0.068893
becam,0.033624
biggest,0.034531
black,0.049644
boy,0.072489
branch,0.039119
british,0.061609
cabinet,0.092835
caribbean,0.105433
caricom,0.049274
cecil,0.048102
centuri,0.015818
ceremoni,0.034005
challeng,0.022005
chamber,0.032687
chang,0.013888
chemic,0.023456
chief,0.025652
christi,0.055325
close,0.016483
coalit,0.029851
coloni,0.025474
commiss,0.049054
committe,0.026431
common,0.014897
commonwealth,0.031823
commun,0.016109
confer,0.024252
consist,0.049471
constitu,0.028851
constitut,0.038908
consult,0.028450
control,0.016369
corpor,0.048346
costli,0.038397
council,0.022465
countri,0.048642
court,0.046738
crescent,0.091910
cross,0.053782
custom,0.024913
death,0.021579
decis,0.020773
democraci,0.027444
depart,0.021602
develop,0.061611
disenfranchis,0.055873
dissid,0.043881
dissolv,0.030601
diversifi,0.037313
domin,0.040550
econom,0.032466
economi,0.040516
elect,0.109392
elizabeth,0.070582
english,0.020659
execut,0.112351
exercis,0.025486
explp,0.077697
feder,0.023617
fee,0.034719
financ,0.023504
fiveseat,0.075720
fiveyear,0.036122
fnm,0.290961
follow,0.012688
food,0.022828
form,0.024248
format,0.021634
framework,0.022725
free,0.036236
freedom,0.025040
function,0.016239
fund,0.021870
gave,0.023081
gener,0.011397
govern,0.060334
governorgener,0.136082
governorincouncil,0.074105
group,0.028616
half,0.021834
head,0.081908
health,0.022113
hi,0.014222
highest,0.024289
hous,0.063834
hubert,0.049639
hundr,0.024099
iadb,0.056063
icao,0.042655
icftu,0.049546
ii,0.041517
import,0.013528
includ,0.022022
independ,0.048874
individu,0.016360
industri,0.018104
ineffici,0.036730
influenti,0.025510
ingraham,0.148211
inmarsat,0.053001
intellectu,0.027211
intelsat,0.044177
intern,0.166797
interpol,0.037062
judici,0.030450
judiciari,0.035291
jurisprud,0.044485
justic,0.053612
kingdom,0.021098
known,0.013252
labour,0.026848
landslid,0.045022
larg,0.013975
latin,0.049849
law,0.031950
lawyer,0.038476
lead,0.031644
leader,0.139132
leadership,0.115088
led,0.017115
legal,0.021882
legisl,0.072837
legislatur,0.064886
liber,0.047818
licenc,0.045418
london,0.022328
lost,0.023043
lower,0.019594
lynden,0.071557
major,0.042203
margin,0.029059
maritim,0.034133
member,0.084590
merchant,0.031492
meteorolog,0.038882
minist,0.191395
monarchi,0.033989
monetari,0.028425
movement,0.091421
multiparti,0.034021
nation,0.102849
nonalign,0.043784
olymp,0.040033
opan,0.057525
open,0.018152
opposit,0.065453
organ,0.159095
organis,0.025104
parliament,0.080227
parliamentari,0.060731
parti,0.143437
particip,0.020917
partner,0.028986
perform,0.018640
perri,0.049185
pindl,0.071557
place,0.014995
plp,0.203898
polit,0.083209
postal,0.040824
power,0.046680
premier,0.038063
press,0.017895
prime,0.171006
privat,0.021801
privi,0.046332
progress,0.041446
prohibit,0.031024
properti,0.018409
protect,0.020264
queen,0.063822
reconstruct,0.030218
red,0.106914
reelect,0.039278
refer,0.009642
repres,0.031636
rest,0.022592
resurg,0.041248
retrain,0.060150
return,0.019515
revenu,0.028132
roland,0.046332
selfgovern,0.038241
senat,0.033081
serv,0.038390
societi,0.016814
speech,0.028914
state,0.035228
stateown,0.039513
street,0.061246
suprem,0.027482
symonett,0.071557
tariff,0.036861
tax,0.025221
telecommun,0.034982
term,0.012681
th,0.015388
thi,0.010038
time,0.011874
tommi,0.054480
took,0.020965
trade,0.058241
tradit,0.016899
turn,0.018553
turnquest,0.077697
ubp,0.139162
unesco,0.036452
union,0.039885
unit,0.081217
univers,0.013899
vest,0.035199
victori,0.062163
wa,0.033011
wallac,0.041033
way,0.014349
weapon,0.029802
went,0.024615
westminst,0.089499
white,0.050960
whitfield,0.133201
worker,0.024982
world,0.068030
worship,0.036368
young,0.024919
