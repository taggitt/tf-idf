abil,0.021894
account,0.018742
accredit,0.020153
act,0.072750
administr,0.011124
advantag,0.024701
advic,0.069265
advis,0.016083
affair,0.013156
agreement,0.012145
agricultur,0.011826
allianc,0.014880
allow,0.007912
alon,0.013606
alway,0.010699
america,0.011150
american,0.018029
ani,0.028049
anoth,0.007652
appeal,0.029636
appel,0.023939
appoint,0.078478
archiv,0.014644
area,0.015805
aris,0.012980
assent,0.050101
attent,0.012974
author,0.044793
autocraci,0.027238
bank,0.011506
bankruptci,0.020768
basic,0.009651
becom,0.007680
begin,0.018709
belong,0.013556
bench,0.026041
bicamer,0.021611
block,0.014291
bodi,0.009627
borrow,0.015922
branch,0.020405
british,0.010712
briton,0.026352
build,0.010152
busi,0.010815
cabinet,0.080708
canada,0.275252
canadath,0.040528
canadian,0.277264
capit,0.020900
case,0.024102
ceas,0.016230
censu,0.018842
centr,0.013212
centuri,0.008251
ceremoni,0.017737
certain,0.008794
chamber,0.051151
chang,0.007244
charit,0.023631
chief,0.026761
civic,0.020932
coalit,0.015571
collect,0.009373
come,0.009190
comment,0.015392
common,0.054394
commonli,0.010672
commonwealth,0.033199
commun,0.008403
complex,0.009403
compos,0.012192
confederationthrough,0.041857
confid,0.065759
conjunct,0.017959
consist,0.025805
constitut,0.121771
construct,0.019904
convent,0.024889
copyright,0.022714
core,0.013647
corpor,0.012609
council,0.035156
councilconsist,0.041857
countri,0.033830
coup,0.017502
court,0.134089
crime,0.015945
crimin,0.016279
crisi,0.013262
crown,0.149809
currenc,0.014083
current,0.032910
curricula,0.024035
david,0.011416
daytoday,0.021929
decis,0.010835
declar,0.012413
deem,0.017392
defeat,0.015205
defin,0.008820
democraci,0.028631
democrat,0.025185
depart,0.045073
develop,0.006427
diplomat,0.015268
direct,0.017068
directli,0.020653
dissolut,0.038735
dissolv,0.015962
district,0.015545
divid,0.010191
divorc,0.021591
doe,0.008696
domin,0.010575
dtat,0.020626
du,0.018164
duti,0.030586
earlier,0.022867
educ,0.019714
elder,0.019014
elect,0.114123
elector,0.016599
element,0.009840
elig,0.019356
elizabeth,0.018408
employ,0.010263
enact,0.017152
end,0.016919
english,0.021552
ensur,0.012711
establish,0.008257
exampl,0.006929
exclus,0.013218
execut,0.035162
exercis,0.026588
expir,0.020915
explicitli,0.015751
extend,0.010205
extern,0.006050
fashion,0.016285
favour,0.015215
feder,0.209431
figur,0.011459
fish,0.014436
follow,0.013237
foreign,0.021541
form,0.006324
formal,0.020706
foundat,0.010789
fount,0.032866
fouryear,0.021049
french,0.022269
function,0.008471
gcca,0.041857
gener,0.107016
given,0.008298
gouvern,0.064499
govern,0.267511
governor,0.182240
guid,0.011698
ha,0.027585
harper,0.063938
head,0.042725
hear,0.034894
hi,0.014837
hill,0.016458
hold,0.059796
hous,0.077694
howev,0.033574
ideal,0.013462
ii,0.010828
import,0.007056
impos,0.014229
includ,0.017230
inde,0.014068
independ,0.008498
individu,0.008534
inform,0.016451
instal,0.016328
instanc,0.011205
instead,0.009691
institut,0.035020
instruct,0.015576
intellectu,0.014194
intern,0.007250
irrelev,0.020549
issu,0.008878
issuanc,0.023370
john,0.009410
johnstonwho,0.041857
judici,0.063535
jurisdict,0.016792
justic,0.069913
justin,0.050182
kind,0.011020
king,0.026355
kingdom,0.011005
late,0.009577
law,0.049998
leader,0.024191
leadership,0.015008
led,0.008927
legal,0.022828
legisl,0.063322
legislatur,0.101539
letter,0.013654
liber,0.024942
limit,0.016895
link,0.006014
local,0.009368
locat,0.010170
lose,0.014547
lowel,0.027939
lower,0.020441
main,0.008756
majest,0.029889
majesti,0.048335
major,0.022014
mandat,0.051700
marriag,0.034973
matter,0.020808
maximum,0.030080
mean,0.015234
measur,0.009059
media,0.012686
meet,0.011191
member,0.052949
mere,0.013606
militari,0.011189
minist,0.212150
minor,0.012799
mistaken,0.022838
misunderstand,0.022714
moment,0.015735
monarch,0.146130
monarchi,0.070917
money,0.012360
mostli,0.011800
motion,0.013956
motiv,0.013512
multipl,0.010638
municip,0.016853
murray,0.020001
nation,0.038320
natur,0.014767
navig,0.017270
nearbind,0.041857
necessari,0.011022
negoti,0.014477
new,0.018534
nonconfid,0.070905
north,0.010981
note,0.015948
notion,0.013085
number,0.006919
occas,0.017203
offic,0.055664
offici,0.010500
onc,0.010334
onli,0.025467
oper,0.008725
opin,0.028093
oppos,0.011558
opposit,0.011380
order,0.015243
ottawa,0.076292
outlin,0.026925
parliament,0.292939
parliamentari,0.031678
parti,0.096197
particip,0.010911
partieswil,0.041857
partisan,0.024167
partyeith,0.041857
pass,0.021490
passport,0.022788
patent,0.036913
pdf,0.016008
perform,0.019446
period,0.008218
permit,0.014087
person,0.027151
phrase,0.033330
place,0.015643
plebiscitari,0.041857
polit,0.052084
politician,0.033186
poll,0.038501
populac,0.043777
postal,0.021294
power,0.089280
practic,0.016971
precis,0.013064
prerog,0.152061
present,0.007930
press,0.018669
prime,0.165657
privat,0.011372
privi,0.072503
proclam,0.022739
prorog,0.041857
prorogu,0.038655
provinc,0.043985
provinci,0.134274
public,0.023962
punish,0.017447
queen,0.166455
queenincouncil,0.083715
queeninparlia,0.041857
rare,0.013052
ratif,0.024813
reach,0.010222
read,0.008736
realm,0.015889
receipt,0.021688
recommend,0.014827
refer,0.015089
regard,0.009650
regul,0.035724
reign,0.034785
relat,0.013564
relationship,0.009601
releas,0.012571
relinquish,0.023096
remov,0.011613
render,0.032993
repres,0.016502
representativeth,0.041857
requir,0.023755
reserv,0.013377
resign,0.017188
resortha,0.041857
respond,0.013566
respons,0.026173
rest,0.011784
result,0.007006
review,0.011270
revok,0.024269
robson,0.031537
role,0.008811
royal,0.132719
rule,0.027545
s,0.007134
sa,0.022162
scope,0.014787
seat,0.059540
select,0.010937
senat,0.086279
sens,0.010350
separ,0.009189
servic,0.009800
session,0.057410
set,0.015583
share,0.009801
ship,0.014272
signmanu,0.040528
sinc,0.007000
singl,0.009368
situat,0.010778
sole,0.014209
solemn,0.030249
solid,0.015045
sometim,0.018226
sovereign,0.083658
specif,0.016751
standpoint,0.022294
state,0.024500
statesmenrar,0.041857
statut,0.020564
stem,0.015735
stephen,0.015550
stipul,0.042341
structur,0.008179
subgroup,0.022034
subject,0.017810
succumb,0.025215
suggest,0.019600
summon,0.073655
support,0.008323
suprem,0.028671
sworn,0.022294
taken,0.010250
tantamount,0.028767
tax,0.026312
term,0.013229
terminolog,0.016484
territori,0.024506
theoris,0.026091
thereaft,0.018303
thi,0.026180
thing,0.011138
thu,0.049099
time,0.018581
topic,0.011107
total,0.009767
tradit,0.017630
treati,0.014619
tri,0.011018
trudeau,0.065303
truli,0.017542
trust,0.030606
typic,0.028174
undemocrat,0.029243
understand,0.028507
unfamiliar,0.024303
unilater,0.021950
unit,0.007060
untrammel,0.037325
unwritten,0.029243
upper,0.014136
urg,0.018476
usag,0.044049
use,0.040040
usual,0.008424
vagu,0.020704
variou,0.007969
vest,0.036721
vicereg,0.031880
viceroy,0.076424
vote,0.013275
voter,0.056337
wa,0.011479
war,0.009944
way,0.007485
wayback,0.026299
websit,0.013237
weight,0.014109
westminsterstyl,0.036294
won,0.014171
word,0.009717
work,0.006862
written,0.011083
wrote,0.011926
year,0.021194
