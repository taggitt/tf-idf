abil,0.005375
abl,0.004934
abrog,0.077324
accept,0.019619
accord,0.003906
accordingli,0.008792
acp,0.011617
act,0.004465
action,0.004800
actual,0.005000
addit,0.003952
adequ,0.008195
administr,0.021849
adopt,0.005391
advis,0.015793
advisori,0.009604
affair,0.012919
affect,0.005105
aftermath,0.009735
aggreg,0.008261
agre,0.005890
allianc,0.036532
allow,0.007770
altern,0.005020
amend,0.008238
amnesti,0.022527
analysi,0.004709
ani,0.003443
anniversari,0.010360
announc,0.039363
anoth,0.003757
anthoni,0.018451
anticip,0.008689
appeal,0.058207
appoint,0.077066
appointe,0.013681
approv,0.006711
april,0.029883
arbitr,0.011070
argu,0.005197
arm,0.006229
aros,0.007758
arrest,0.008586
asdb,0.014359
ask,0.006458
assert,0.006711
assum,0.010824
attempt,0.008912
attend,0.014908
attorneygener,0.029240
australia,0.006790
australian,0.008125
author,0.004398
authoris,0.011588
automat,0.007975
autonomi,0.008163
avoid,0.005835
bainimarama,0.147285
banker,0.010668
bar,0.008831
bare,0.009623
basi,0.004866
bavadra,0.058180
becam,0.025835
becaus,0.003546
becom,0.007542
befor,0.008046
behest,0.013540
bicamer,0.010611
bloc,0.009452
board,0.013591
bodi,0.004727
bose,0.013311
boycott,0.011603
branch,0.020038
brawl,0.017407
british,0.005259
broadcast,0.009140
broker,0.010344
buy,0.007452
cabinet,0.071331
cakobau,0.018060
came,0.005158
campaign,0.007082
candid,0.043603
cap,0.009610
capit,0.005131
carnavon,0.019899
cast,0.008014
caus,0.004211
central,0.008845
certain,0.004318
chair,0.008861
chairman,0.008923
chamber,0.041859
characteris,0.009458
charg,0.006088
chaudhri,0.115413
chief,0.085411
chines,0.006781
choos,0.006787
chose,0.008874
citi,0.010897
citizen,0.006456
citizenship,0.029503
civilian,0.016404
clan,0.010344
clear,0.005872
close,0.004221
coalit,0.061165
col,0.012887
collect,0.004602
colleg,0.006849
command,0.013240
commanderinchief,0.010957
commiss,0.006281
commission,0.009704
commodor,0.054875
commonwealth,0.024451
commun,0.020630
composit,0.013083
compris,0.006455
compromis,0.009072
concern,0.008860
condit,0.004415
confer,0.006211
confirm,0.006802
confront,0.008717
consist,0.008447
constitu,0.007389
constitut,0.154459
consult,0.007286
consum,0.037700
content,0.006226
contenti,0.011214
continu,0.003823
control,0.016770
controversi,0.018655
core,0.006701
council,0.120833
countri,0.020763
coup,0.103126
coupl,0.007302
court,0.113722
cp,0.012055
crescent,0.011770
crippl,0.011588
crisi,0.019535
crop,0.007801
cross,0.006887
current,0.008079
daniel,0.015697
day,0.019047
deal,0.005083
debat,0.005874
decemb,0.011581
decis,0.015961
declar,0.006095
decre,0.009062
defeat,0.014932
defect,0.009314
degre,0.005167
deleg,0.008910
demand,0.005633
democrat,0.006183
depend,0.008327
determin,0.008764
develop,0.003155
did,0.004684
differ,0.003345
director,0.007587
disabl,0.009190
disappoint,0.010829
disestablish,0.015112
disgruntl,0.014565
dismiss,0.025222
disput,0.006347
district,0.007632
divis,0.043754
domin,0.010385
dr,0.023488
draft,0.016288
drag,0.010925
drop,0.006926
dtat,0.010127
duavata,0.018060
dure,0.007265
earli,0.003799
eastern,0.006305
economi,0.005188
elect,0.145692
elector,0.024451
element,0.004831
elig,0.009504
elizabeth,0.009038
end,0.004153
enjoy,0.007232
entir,0.004927
entitl,0.023529
erod,0.010935
escap,0.007627
establish,0.016218
ethnic,0.097463
european,0.009464
event,0.004944
everi,0.004754
evid,0.005180
exampl,0.003402
exclud,0.007147
execut,0.034530
exercis,0.006527
exist,0.003777
exodu,0.011866
exofficio,0.015742
expand,0.005374
express,0.009844
expuls,0.011141
extens,0.005130
eye,0.007595
faction,0.008973
fail,0.005712
fall,0.005185
fao,0.010335
farmer,0.016082
fatiaki,0.038787
fault,0.010205
fear,0.007128
feder,0.012098
fiji,0.445382
fijian,0.362701
fijifirst,0.073309
final,0.004914
fiveperc,0.036654
fiveyear,0.009251
follow,0.019498
forc,0.024431
forcibl,0.011093
foreign,0.015865
form,0.012421
formal,0.025418
fourteen,0.011047
fouryear,0.010335
framework,0.005820
frank,0.015909
frozen,0.010777
fuell,0.012861
fulli,0.006124
function,0.004159
g,0.005495
galvanis,0.016754
ganilau,0.019393
gate,0.019668
gener,0.011676
georg,0.011710
govern,0.115897
governorgener,0.023235
grand,0.007891
great,0.022893
group,0.007329
gunmen,0.015405
ha,0.029798
hand,0.005033
harbour,0.010447
head,0.031467
hear,0.008566
held,0.019212
hi,0.021855
hierarchi,0.008278
high,0.016454
highest,0.006221
hold,0.004893
home,0.005801
honorari,0.010473
hostag,0.012055
hous,0.070847
howev,0.019782
human,0.003914
ibrd,0.010914
icao,0.010925
icc,0.011850
icftu,0.012690
icrm,0.011263
ida,0.010678
identifi,0.004910
ifad,0.011082
ifc,0.010957
iho,0.013439
ii,0.005316
illeg,0.016372
ilo,0.010717
iloilo,0.070419
imf,0.008896
imo,0.011603
impass,0.013645
implement,0.011312
impos,0.006986
inabl,0.009807
includ,0.011280
inconclus,0.011834
indentur,0.012381
independ,0.033380
india,0.006219
indian,0.020174
indianl,0.019393
indigen,0.040500
individu,0.012571
indofijian,0.236618
industri,0.009274
inflow,0.011129
influenc,0.004474
inform,0.008077
initi,0.004414
instead,0.004758
integr,0.004902
intelsat,0.011314
intercommun,0.015254
interim,0.068241
intern,0.014240
interpol,0.009492
intervent,0.014742
ioc,0.010592
island,0.012042
iso,0.009480
issu,0.013078
itu,0.010659
jai,0.015182
januari,0.005797
jockey,0.016032
join,0.006042
josefa,0.051173
judg,0.037509
judgement,0.010269
judici,0.015598
judiciari,0.045193
juli,0.017603
june,0.005918
justic,0.034327
kamises,0.089105
kingdom,0.005403
labasa,0.018327
labour,0.034382
laisenia,0.067019
land,0.041124
landslid,0.011531
larg,0.007159
largest,0.011219
late,0.004702
later,0.008078
lautoka,0.035642
lead,0.004052
leader,0.041573
leadership,0.022107
leas,0.041013
lebanon,0.011251
led,0.021917
left,0.005414
legal,0.005604
legisl,0.024873
legislatur,0.024928
levu,0.017407
lewenivanua,0.018060
lifememb,0.019899
line,0.004860
littl,0.005198
local,0.009200
locat,0.009987
london,0.005718
longer,0.011202
look,0.005607
lower,0.005018
lt,0.012508
magistr,0.010668
mahendra,0.032497
main,0.012898
mainli,0.005638
maintain,0.009471
major,0.046840
make,0.003453
manag,0.014591
mandat,0.008461
mani,0.012278
manoeuvr,0.013833
mara,0.153011
march,0.005631
matter,0.005108
mayor,0.010302
measur,0.004448
media,0.006229
member,0.034664
membership,0.007480
midoctob,0.016365
midst,0.011692
militari,0.065927
militaryback,0.033509
minist,0.183825
ministeri,0.011314
minor,0.012569
minu,0.010037
model,0.008460
monarchi,0.008705
month,0.006000
mostli,0.005794
movement,0.004683
multiparti,0.008713
multipl,0.005223
municip,0.008275
narrow,0.007668
narrowli,0.010555
nation,0.026342
nationalist,0.017610
nationwid,0.020637
near,0.005583
negoti,0.014216
new,0.030335
nfp,0.074521
ni,0.059752
nomin,0.021265
nonrecognit,0.016754
northern,0.012697
note,0.003915
novemb,0.018273
number,0.013589
occas,0.008447
octob,0.017617
odditi,0.015485
offer,0.005049
offic,0.032798
offici,0.015467
onli,0.009378
opcw,0.011059
open,0.004649
oper,0.004284
oppos,0.005675
opposit,0.016764
order,0.014969
organis,0.025718
outright,0.010518
owner,0.007681
ownership,0.015950
paramount,0.011393
parliament,0.089041
parliamentari,0.046663
parteuropean,0.019899
parti,0.141701
particip,0.010714
partylist,0.031306
pattern,0.005794
pca,0.012401
peacekeep,0.010318
peak,0.007452
penaia,0.019899
pend,0.011461
peopl,0.003744
percent,0.013641
percentag,0.007567
period,0.004035
permit,0.013833
person,0.008887
persuad,0.009814
peter,0.012385
pif,0.015112
place,0.011522
pliabl,0.015328
polici,0.005025
polit,0.051148
poll,0.028357
pool,0.008892
popul,0.009663
posit,0.016099
possibl,0.003972
post,0.006590
postindepend,0.013280
power,0.043837
practic,0.012499
prerog,0.012443
present,0.003893
presid,0.135163
presidenti,0.007963
press,0.004583
pretend,0.012055
previous,0.011873
primarili,0.005323
prime,0.156422
prior,0.011763
problem,0.004164
problemat,0.009385
produc,0.008103
prohibit,0.015892
project,0.004818
promulg,0.009930
proport,0.012877
proportion,0.011475
propos,0.009609
prospect,0.008275
protest,0.007237
prove,0.006010
provid,0.003455
provinc,0.021597
provinci,0.018837
provis,0.007585
qaras,0.099704
queen,0.008173
rabuka,0.178211
race,0.015301
radio,0.007972
ram,0.011603
rare,0.006408
ratu,0.172259
readmit,0.014915
reappoint,0.041268
receiv,0.004718
recent,0.004276
recognis,0.007625
record,0.005205
red,0.013691
reddi,0.014915
reduc,0.009357
reflect,0.010615
reform,0.006041
refus,0.021622
regim,0.014393
reinstat,0.022452
reject,0.012177
relat,0.006660
releas,0.006172
remain,0.004084
remov,0.011404
renew,0.021666
replac,0.005032
report,0.004746
repres,0.048616
represent,0.012461
republ,0.034779
requir,0.011664
research,0.004006
reserv,0.039411
resid,0.006543
resign,0.050639
resolut,0.007555
resolv,0.007173
resourc,0.005000
respons,0.004283
restor,0.013969
retain,0.006494
return,0.009996
review,0.016601
revok,0.011916
right,0.013296
role,0.012979
roll,0.009215
rotuma,0.034451
rotuman,0.019393
rule,0.009016
run,0.005427
rural,0.007987
sack,0.010277
sale,0.007194
saw,0.006043
say,0.010701
seat,0.058470
second,0.012160
section,0.006119
secur,0.005247
seen,0.004846
seiz,0.008491
senat,0.042364
septemb,0.029780
seriou,0.006918
serv,0.024581
session,0.009396
set,0.007651
seventeenth,0.010727
sever,0.003583
short,0.005266
shortliv,0.019408
simmer,0.014979
sinc,0.006874
singl,0.004600
sir,0.054308
sit,0.009086
sitiveni,0.035642
situat,0.010584
size,0.005352
slight,0.010261
slightli,0.007449
small,0.004206
smaller,0.005844
soqosoqo,0.054180
sovereign,0.008215
sparteca,0.016032
spc,0.013645
speaker,0.008826
speight,0.034115
splinter,0.012530
stage,0.006002
standoff,0.013872
state,0.006015
statist,0.005639
statu,0.005740
statutori,0.022922
street,0.007843
strictli,0.008106
structur,0.004016
subdivid,0.009418
subscrib,0.010747
subsequ,0.021456
success,0.009116
suffrag,0.020210
sugar,0.016751
sugarcan,0.011707
support,0.012260
suppos,0.007552
suprem,0.028155
surround,0.006294
suva,0.045986
svt,0.018060
taken,0.010065
talk,0.014792
taukei,0.038787
tension,0.007807
tenur,0.020841
term,0.019487
themselv,0.005098
thenpresid,0.029130
therebi,0.007114
thi,0.020567
threeyear,0.011276
threshold,0.019149
ticket,0.011559
time,0.009123
timoci,0.019393
today,0.004937
took,0.005369
tool,0.005503
total,0.009591
town,0.014117
trade,0.004972
tradit,0.008656
trigger,0.008361
tui,0.014459
turn,0.004751
twenti,0.008454
unaccept,0.022403
unctad,0.010688
undermin,0.009110
uneas,0.015112
unesco,0.009336
unicamer,0.010482
unido,0.010968
unifil,0.016365
unikom,0.016365
unit,0.006933
uniti,0.007555
univers,0.007120
unmibh,0.015328
unmik,0.013912
upper,0.006941
upu,0.010473
use,0.005617
usual,0.008272
vakaturaga,0.019899
vakavulewa,0.038787
valid,0.019691
verdict,0.026207
veri,0.003925
vest,0.018030
vicepresid,0.011105
victori,0.007960
villag,0.007854
violat,0.007605
viti,0.017057
vote,0.084736
voter,0.027662
wa,0.118369
wco,0.012241
week,0.007224
wellington,0.013718
went,0.006304
western,0.010340
wftu,0.011866
widespread,0.013207
win,0.023187
winston,0.011588
wipo,0.010914
wmo,0.010819
won,0.034790
work,0.003369
world,0.003484
wtoo,0.012164
wtro,0.012037
year,0.031220
zealand,0.024519
