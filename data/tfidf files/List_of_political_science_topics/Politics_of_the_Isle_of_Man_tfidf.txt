abolish,0.016366
access,0.010837
accomplish,0.015741
account,0.009224
act,0.035807
adversari,0.023233
advic,0.034091
advoc,0.026889
affair,0.051803
age,0.009808
alreadi,0.011410
ani,0.006902
appeal,0.058348
appli,0.008373
appoint,0.025750
area,0.015558
assent,0.024659
attorney,0.021507
autonomi,0.016366
ayr,0.029902
bailiff,0.064707
ban,0.015780
becaus,0.007109
behalf,0.017420
bicamer,0.021273
birch,0.027077
bishop,0.019329
board,0.013624
branch,0.040173
british,0.052724
broadcast,0.018324
callow,0.037350
candid,0.014569
capit,0.030861
celtic,0.048628
ceremoni,0.017460
certain,0.008657
chamber,0.033568
chancellor,0.022433
chang,0.007131
chief,0.052687
chosen,0.014453
citizen,0.012944
citizenship,0.039432
civil,0.043402
clerk,0.021795
combin,0.009020
commiss,0.012594
committe,0.013572
commut,0.021015
concern,0.008881
consensu,0.015780
consent,0.019416
consequ,0.010757
consist,0.033869
constitu,0.014814
constitut,0.009989
continu,0.007665
contrast,0.010686
convent,0.012250
cooper,0.012091
corpor,0.012412
council,0.080750
counsellor,0.030885
court,0.143995
cover,0.010416
crimin,0.032050
crown,0.081928
current,0.008099
dealt,0.018961
death,0.011080
deemster,0.224103
defeat,0.014968
defenc,0.046201
defin,0.008682
deliveri,0.018285
democraci,0.014092
depart,0.022185
depend,0.016695
deputi,0.033884
develop,0.006327
directli,0.020330
directlyelect,0.036207
disput,0.012725
divis,0.032895
doe,0.025680
domest,0.026217
dougla,0.037392
east,0.011710
effect,0.014799
elect,0.157277
elig,0.019053
elizabeth,0.018121
employe,0.031919
england,0.013216
equival,0.012268
establish,0.008128
eu,0.016584
european,0.018973
everi,0.019062
exampl,0.006821
exclud,0.014328
execut,0.069227
exercis,0.013086
exist,0.022722
extend,0.010046
extern,0.017867
final,0.009851
financ,0.012069
fiveyear,0.055643
follow,0.006515
forc,0.008163
foreign,0.010602
formal,0.010191
formerli,0.015257
free,0.009303
gaol,0.035728
gener,0.052672
good,0.018110
govern,0.100686
governor,0.059798
governorincouncil,0.038051
grant,0.012485
greater,0.020517
group,0.014693
guarante,0.014940
ha,0.038016
harmonis,0.027733
head,0.042058
held,0.009629
hereditari,0.019493
hi,0.014605
high,0.041235
honour,0.019207
hous,0.087407
howev,0.006610
human,0.007846
ii,0.010659
imprison,0.019366
improv,0.010022
includ,0.022615
independ,0.033461
indirectli,0.017551
influenc,0.008970
instrument,0.012541
insular,0.023958
intern,0.007137
interven,0.017501
intervent,0.014778
iomelectionscom,0.041204
isl,0.376373
island,0.120717
issu,0.008739
joint,0.014341
judg,0.045120
judici,0.031271
judiciari,0.036242
justic,0.055057
key,0.072548
kingdom,0.119166
labour,0.027572
languag,0.010235
largest,0.022493
lastev,0.041204
late,0.009427
law,0.041014
lay,0.015323
leagu,0.016707
leav,0.011531
legisl,0.124667
legislatur,0.033317
liber,0.024553
lieuten,0.086927
life,0.008753
link,0.005920
locat,0.010011
longstand,0.019676
lord,0.064462
lowest,0.016195
main,0.008619
man,0.228887
mann,0.076182
manx,0.403086
marin,0.015586
matter,0.020483
mec,0.033314
member,0.060809
mhk,0.035294
michael,0.012530
militari,0.011014
minimum,0.014851
minist,0.098276
monarch,0.035962
mostli,0.011616
movement,0.009388
nation,0.015088
natur,0.007268
nearli,0.011858
newli,0.014324
nomin,0.014210
nonvot,0.030885
normal,0.010852
number,0.006811
nurs,0.019811
obtain,0.010582
occasion,0.015359
offenc,0.048054
offic,0.032877
oldest,0.014728
oper,0.008589
optout,0.031382
overal,0.012461
paramount,0.022842
parliament,0.109853
parliamentari,0.015592
parti,0.094695
pass,0.010577
peac,0.013130
peopl,0.015013
person,0.008909
pirat,0.021690
place,0.007699
polic,0.015142
polici,0.010075
polit,0.068361
politician,0.016334
popul,0.019374
posit,0.008069
power,0.039948
presid,0.033872
pressur,0.023504
price,0.011558
prime,0.012543
privi,0.047581
promot,0.011137
prompt,0.017091
protocol,0.017653
public,0.007862
punish,0.034349
queen,0.081928
radio,0.015983
rang,0.009371
read,0.008599
recent,0.008574
refer,0.014854
regul,0.011722
relat,0.006676
relationship,0.009451
remain,0.008189
report,0.009516
repres,0.024367
represent,0.012491
republ,0.011621
residu,0.019112
respons,0.051528
result,0.006897
right,0.026656
role,0.008674
roll,0.018475
royal,0.026129
run,0.010881
said,0.010173
seat,0.043958
second,0.024379
secondari,0.014099
secretari,0.032354
sector,0.025962
sentenc,0.016927
separ,0.018092
septemb,0.011940
seriou,0.013870
serv,0.019712
servic,0.038589
sever,0.007184
shortag,0.017981
sit,0.036433
slightli,0.014935
social,0.008766
sodomi,0.031746
sodor,0.037350
sometim,0.008970
somewhat,0.014608
sovereign,0.016470
staff,0.016245
stand,0.012950
state,0.024118
station,0.015054
statutori,0.045955
strong,0.010013
subject,0.008766
subordin,0.019019
taxat,0.016942
teacher,0.015792
term,0.019534
territori,0.012061
themselv,0.010221
theoret,0.011299
thi,0.020617
tie,0.013441
togeth,0.009502
took,0.010765
total,0.009615
town,0.014151
treati,0.014390
tricamer,0.038051
turn,0.009526
turnout,0.023291
twoseat,0.038880
tynwald,0.453687
uk,0.101047
unaffili,0.029656
union,0.010240
unit,0.076456
vannin,0.112051
vest,0.036147
veto,0.022360
virtu,0.017460
vote,0.026135
voter,0.018485
wa,0.022600
water,0.010629
westminst,0.022977
wish,0.015236
withhold,0.023660
won,0.041849
work,0.006755
world,0.006986
year,0.013909
