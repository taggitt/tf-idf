ab,0.019071
absolut,0.025823
action,0.009353
actual,0.009742
administr,0.031930
adopt,0.010504
affair,0.037760
agenc,0.047900
alik,0.020512
alway,0.010236
anew,0.026953
ani,0.026835
announc,0.025566
appoint,0.050053
arbetsmarknadsdepartementet,0.040045
arm,0.012137
articl,0.036187
author,0.025712
automat,0.015539
bck,0.035710
bear,0.014245
befor,0.015678
beforementiond,0.040045
begin,0.008949
bibliographi,0.013349
bio,0.024246
board,0.013241
bodi,0.009210
briefli,0.017025
cabinet,0.200757
candid,0.014159
cast,0.046846
ceremoni,0.016969
chamber,0.016312
chanceri,0.029867
chang,0.006930
chapter,0.071638
charter,0.016537
chosen,0.014047
coast,0.014031
code,0.011896
collect,0.008968
collegi,0.025369
confid,0.047184
confirm,0.013255
consent,0.018871
consequ,0.010455
consid,0.007170
consist,0.008229
constitut,0.019416
constitutedand,0.040045
continu,0.007449
contract,0.024668
council,0.022422
counti,0.036595
court,0.011662
cultur,0.009245
current,0.023614
custom,0.012432
defeat,0.014547
defenc,0.014967
den,0.022046
directli,0.009879
directorgener,0.030853
discret,0.014501
dismiss,0.049144
doe,0.008319
duti,0.014631
economi,0.010109
educ,0.009430
effect,0.007191
elect,0.076427
els,0.015244
employ,0.009819
energi,0.010363
energidepartementet,0.037787
english,0.010309
ensur,0.012161
enter,0.021644
enterpris,0.013933
entir,0.009601
entiti,0.012560
environ,0.009812
eu,0.016117
european,0.009220
europeiska,0.038774
execut,0.022427
exercis,0.012718
extern,0.005788
fail,0.011129
featur,0.010011
financ,0.011729
finansdepartementet,0.037787
fiscal,0.016782
follow,0.031660
forc,0.015868
foreign,0.020609
form,0.006050
formal,0.019810
format,0.010796
fr,0.019544
frfattningssaml,0.040045
frlag,0.035710
frordn,0.040045
frsvarsdepartementet,0.040045
frvaltningsavdelningen,0.040045
frvaltningsmyndighet,0.040045
function,0.008104
fundament,0.020273
gener,0.017063
govern,0.270985
guard,0.016544
ha,0.021112
handl,0.014393
head,0.040875
health,0.011035
held,0.009358
henri,0.012758
hi,0.007097
high,0.016030
histori,0.006593
hold,0.009534
howev,0.006424
includ,0.010989
independ,0.008130
individu,0.032659
innov,0.013711
instrukt,0.040045
instrument,0.097508
interfer,0.016157
intern,0.013873
irrespect,0.022995
isbn,0.018714
issuanc,0.022358
januari,0.022593
judiciari,0.017611
justic,0.013377
justitiedepartementet,0.040045
kansli,0.040045
kingdom,0.010528
known,0.013227
kommun,0.033236
konungariket,0.040045
kulturdepartementet,0.040045
kunglig,0.040045
larsson,0.031444
law,0.031889
leader,0.011572
link,0.005754
list,0.014716
local,0.008962
longer,0.010914
longform,0.032125
lose,0.013917
lund,0.027698
madeand,0.040045
main,0.008377
majestt,0.040045
major,0.014040
makten,0.040045
matter,0.009953
mean,0.014575
med,0.027698
member,0.025328
mention,0.012810
milj,0.040045
minist,0.274601
ministerappoint,0.040045
ministeri,0.066139
ministerstyr,0.040045
ministri,0.184963
monarch,0.052426
monarchi,0.016961
municip,0.016124
nation,0.007332
new,0.041374
nomin,0.069056
nomine,0.024962
nringsdepartementet,0.040045
och,0.028174
offentliga,0.040045
offic,0.127812
offici,0.020091
olof,0.029061
onli,0.012182
oper,0.008347
oppos,0.011058
ordin,0.058547
organ,0.021653
organis,0.025056
organiz,0.017334
origin,0.007333
otherwis,0.025173
palac,0.018920
parlanc,0.025587
parliamentari,0.015153
parti,0.030677
pejor,0.023089
perform,0.009302
perman,0.012823
petersson,0.037787
polic,0.014716
polit,0.016609
portfolio,0.018201
power,0.007765
prescrib,0.018529
present,0.022761
previous,0.011567
prime,0.182869
procedur,0.013214
prohibit,0.015482
promulg,0.038698
provid,0.013464
publish,0.008422
realm,0.030403
realmset,0.040045
reappoint,0.026803
refer,0.004812
referendum,0.016707
reger,0.034723
regeringen,0.037787
regeringsformenon,0.040045
regeringskansliet,0.120137
reject,0.023726
relat,0.006488
report,0.009249
represent,0.036420
republ,0.011294
research,0.007806
resign,0.032889
respect,0.009151
respons,0.033386
result,0.013406
riksbank,0.030333
riksdag,0.323781
role,0.008430
royal,0.038092
rule,0.008784
safeguard,0.020638
scope,0.014147
secretari,0.015722
sens,0.009902
separ,0.008791
serv,0.009579
servic,0.009376
short,0.010260
shortform,0.032378
sinc,0.006697
singl,0.008962
site,0.022562
sn,0.022551
social,0.008519
socialdepartementet,0.040045
sole,0.013594
sort,0.028095
sovereign,0.016007
speaker,0.085993
special,0.008376
specif,0.008013
staff,0.015788
start,0.008477
state,0.035160
statliga,0.040045
statsminist,0.037787
statsrd,0.080091
statsrdsberedningen,0.040045
statut,0.019674
stndiga,0.040045
stockholm,0.022551
strictli,0.015794
structur,0.007825
studentlitteratur,0.040045
subsequ,0.010451
succeed,0.013692
summari,0.016117
support,0.015926
suprem,0.013714
svensk,0.032931
sverig,0.090999
sweden,0.284403
swedish,0.547964
talk,0.014411
technic,0.011911
term,0.006328
thereof,0.020584
thi,0.015028
thu,0.015658
torbjrn,0.033917
treati,0.027972
union,0.009952
unionen,0.040045
uniqu,0.011419
unless,0.030016
use,0.010944
usual,0.008059
utbildningsdepartementet,0.040045
utrikesdepartementet,0.040045
vernacular,0.023153
vest,0.035131
vid,0.033917
vote,0.088903
wa,0.010982
week,0.014076
ye,0.021372
