abbasid,0.003698
abcclio,0.004635
abdic,0.003642
abil,0.001663
abl,0.001527
abolish,0.128833
abolit,0.066683
abolitionist,0.045858
abort,0.003377
aboutcom,0.004596
abov,0.001547
absenc,0.002222
absolut,0.004101
abstain,0.007564
abstent,0.008635
academ,0.001810
accept,0.010624
accompani,0.002337
accord,0.010880
accordingli,0.002720
account,0.004271
accus,0.007273
achiev,0.001520
aclu,0.005330
acquit,0.008981
act,0.016580
action,0.002970
activ,0.004825
activist,0.005768
actor,0.002786
actual,0.003094
ad,0.001542
addit,0.003669
address,0.005461
adolf,0.003335
adopt,0.008341
adult,0.004733
adultera,0.006359
adulteress,0.006359
adulteri,0.009082
advanc,0.002986
advic,0.002631
advis,0.002443
advoc,0.004150
affair,0.001998
affirm,0.011672
afford,0.002596
afghanistan,0.006203
africa,0.009816
african,0.008475
age,0.015140
aggrav,0.011758
aggress,0.002693
aggressor,0.026944
agoni,0.004817
agre,0.001822
ahimsa,0.004524
ahinsa,0.006359
aim,0.001726
aisha,0.004993
akin,0.003235
al,0.002073
albert,0.002457
ale,0.004577
alhudud,0.006359
allah,0.003857
alli,0.002232
allow,0.007213
allud,0.003692
almutadid,0.005765
alongsid,0.002514
alreadi,0.001761
altern,0.004660
altogeth,0.005994
amend,0.012747
america,0.010165
american,0.010957
americancatholicorg,0.012719
amnesti,0.034855
analog,0.002160
analysi,0.001457
ancestor,0.002598
ancient,0.005254
andi,0.003980
anesthesiolog,0.004720
anger,0.006311
anglican,0.003613
anguish,0.004961
angulimala,0.005671
ani,0.007458
anim,0.003384
announc,0.004060
annual,0.001857
anoth,0.011627
answer,0.006457
answerscom,0.005671
antideath,0.019079
antiqu,0.002501
anyon,0.002635
anyth,0.002389
apolog,0.007364
apostasi,0.021854
appeal,0.002251
appear,0.001400
appleton,0.004072
appli,0.010339
applic,0.007218
approach,0.001391
approv,0.006230
aquina,0.006802
arab,0.002232
arabia,0.044359
arabiahirabah,0.006359
arbitr,0.017128
arbitrarili,0.003338
archbishop,0.007540
area,0.001200
argu,0.016082
arguabl,0.002946
argument,0.002031
arm,0.001927
armenian,0.003677
arson,0.004541
arthasastra,0.005386
articl,0.004310
asgari,0.006359
ashoka,0.008714
asia,0.008303
asian,0.002303
asid,0.002757
ask,0.005995
aspir,0.003039
assault,0.003099
assembl,0.016985
assert,0.002076
assess,0.002059
associ,0.003542
assum,0.001674
assur,0.002754
atheist,0.006596
athenian,0.003995
atkin,0.004474
aton,0.004357
atroci,0.006001
attack,0.003898
attain,0.002447
attempt,0.004136
attend,0.002306
attent,0.001971
attitud,0.004963
audio,0.003172
augsburg,0.004268
august,0.003674
australia,0.008405
australian,0.002514
austria,0.002797
author,0.013611
authoritarian,0.009305
avail,0.007389
aveng,0.004357
avoid,0.005417
away,0.005524
ayaz,0.005514
b,0.001601
background,0.006228
bad,0.004959
baghdad,0.003357
ban,0.021921
bangladesh,0.003451
banish,0.003863
baptist,0.003529
bar,0.002732
bargain,0.003274
base,0.008427
basi,0.004517
baze,0.006359
bbc,0.002750
bc,0.004313
becam,0.006662
becaus,0.007681
beccaria,0.020112
becom,0.007001
befor,0.006224
begin,0.002842
begun,0.002317
behaalotcha,0.006359
behead,0.012538
belaru,0.018652
belief,0.003902
believ,0.013569
belong,0.002059
benefit,0.001768
benin,0.003956
bentham,0.003919
best,0.001606
better,0.003287
bhutan,0.003941
bia,0.002779
bibl,0.002923
biblic,0.003131
bibliographi,0.002120
bind,0.002517
bishop,0.008950
blacklook,0.006359
blackston,0.004698
blasphemi,0.013572
blecker,0.006359
bleed,0.003898
block,0.002171
blood,0.028486
bloodi,0.003422
bloodthirsti,0.005671
blow,0.006176
bodi,0.005851
boil,0.003260
book,0.012468
borg,0.004370
botch,0.009230
botswana,0.003891
box,0.002732
boy,0.002872
brahmin,0.004305
brazen,0.005514
brazil,0.002604
break,0.002009
brethren,0.004037
bride,0.004330
bring,0.001812
britain,0.004107
british,0.001627
brought,0.001796
brunei,0.011437
brutal,0.006355
bucket,0.004004
buddha,0.013674
buddhism,0.017810
buddhist,0.017592
buddhistmajor,0.006359
buggeri,0.010895
bull,0.003364
bullet,0.003604
burden,0.005565
buri,0.002882
burial,0.003313
burn,0.007496
burundi,0.004119
byof,0.006359
ca,0.002681
cakkavatti,0.005873
caliph,0.003301
calvin,0.003637
cambodia,0.003555
cambridg,0.011601
came,0.003192
camp,0.002641
campaign,0.010958
camu,0.004635
canada,0.006273
canon,0.010603
capac,0.002023
capit,0.188953
capitali,0.006001
capric,0.005230
caput,0.005386
cardin,0.003200
caribbean,0.005570
carri,0.026755
cart,0.003837
carter,0.003447
case,0.028076
catech,0.013423
categori,0.003725
cathol,0.042430
cattl,0.005719
caught,0.003006
caus,0.007818
ceas,0.004932
celebr,0.002516
center,0.003114
centuri,0.012536
certain,0.012026
certainli,0.005573
certainti,0.003081
cesar,0.008145
cf,0.003292
chad,0.003764
chair,0.005484
chamber,0.002590
chanc,0.002350
chang,0.004402
chao,0.002825
chapter,0.009101
charl,0.005609
charter,0.002626
cheek,0.008635
cherri,0.004119
chi,0.003497
child,0.021103
children,0.002007
china,0.031888
chines,0.004197
choic,0.001918
choos,0.002100
christ,0.021807
christendom,0.003964
christian,0.026489
church,0.066178
circumst,0.024671
cite,0.008168
citi,0.006744
citizen,0.001998
civil,0.025121
claim,0.002964
clark,0.002826
class,0.004858
classgroup,0.006359
classic,0.001613
classifi,0.007899
clear,0.001817
clinic,0.002546
close,0.003919
closer,0.002415
cloth,0.002552
coalit,0.009463
code,0.009446
codifi,0.003206
coffin,0.004256
cogen,0.005514
collect,0.001424
collin,0.003447
coloni,0.002018
columbia,0.002715
combat,0.005125
come,0.005585
command,0.006145
commemor,0.006538
commiss,0.003887
commit,0.024558
committe,0.004189
common,0.009445
commonli,0.001621
commun,0.006383
communion,0.004029
communist,0.004816
compani,0.001541
compel,0.003051
compens,0.027147
compet,0.002013
complet,0.006743
concentr,0.001910
concept,0.002725
concern,0.002741
conclus,0.004351
concret,0.002675
condemn,0.030253
condit,0.002732
condon,0.008490
conduct,0.001673
confer,0.009610
confess,0.007093
confin,0.002621
confirm,0.002105
conflict,0.001818
conform,0.002695
congo,0.003332
conquest,0.007893
consensu,0.004871
consequ,0.001660
consid,0.015943
consider,0.004923
constitut,0.010792
contain,0.002801
contemporari,0.005730
contenti,0.003470
context,0.003435
contextdepend,0.004743
contextindepend,0.006359
contin,0.002516
continu,0.007099
contradict,0.002608
contrari,0.012767
contrit,0.005765
control,0.001297
controversi,0.017318
convent,0.017018
convers,0.002014
convert,0.003981
convict,0.025469
cord,0.003485
corpor,0.003831
correct,0.003932
corrupt,0.007192
council,0.026708
countercultur,0.004222
counti,0.002905
countri,0.082243
countryspecif,0.004399
coup,0.002659
cours,0.001701
court,0.024077
courtsmarti,0.006157
coven,0.017975
cover,0.001607
cowardic,0.009154
creat,0.002388
creatur,0.002977
crime,0.169594
crimin,0.054416
criminolog,0.004090
critic,0.002872
crucifixion,0.004443
cruel,0.019456
crush,0.005974
culprit,0.004577
cultur,0.005873
current,0.006250
custodi,0.003857
customari,0.003254
cut,0.012438
dalai,0.004507
danger,0.004587
databas,0.002284
date,0.001632
david,0.001734
day,0.008841
deal,0.001573
death,0.398493
debat,0.012725
debt,0.002264
decad,0.001775
decapit,0.027811
decemb,0.001791
decid,0.003806
decim,0.006515
decis,0.006585
declar,0.005658
declin,0.001854
decreas,0.001922
dedic,0.002345
defeat,0.002310
defect,0.005764
defenc,0.004754
defend,0.026218
defens,0.002143
defin,0.001340
definit,0.001544
degrad,0.008404
dei,0.003905
delay,0.002494
deleg,0.002757
deliber,0.002613
delitti,0.005671
dell,0.004138
demand,0.003486
democraci,0.006525
democrat,0.001913
democratis,0.004458
demonstr,0.005265
deni,0.002263
denial,0.006741
denomin,0.011637
depart,0.001712
depend,0.003865
depriv,0.006272
deriv,0.005939
describ,0.001201
desecr,0.004615
desert,0.008330
deserv,0.003458
desir,0.003675
despit,0.006623
destruct,0.006933
detaine,0.004961
detect,0.004288
deter,0.010731
determin,0.002712
deterr,0.015537
develop,0.004883
dhamma,0.018706
dhammapada,0.010556
dharmasastra,0.005386
dharmastra,0.005671
dhimmi,0.004615
dhuhulow,0.006359
dialogu,0.002726
dicken,0.004370
did,0.004348
die,0.003742
differ,0.005176
difficult,0.001703
digha,0.005064
digniti,0.010468
disabl,0.002843
disagr,0.002776
disapprov,0.003662
disciplin,0.001799
discomfort,0.003972
discourag,0.002948
discret,0.002303
discrimin,0.013558
discriminatori,0.004072
disembowel,0.005514
disloc,0.003919
dismember,0.004676
disobey,0.004490
disord,0.005034
dispens,0.003397
display,0.002219
disput,0.003928
disrespect,0.004458
dissent,0.003325
dissid,0.003477
distanc,0.002079
distast,0.004541
distinct,0.001503
distinguish,0.001733
distort,0.002757
district,0.002361
disturb,0.002742
divers,0.001878
divid,0.003096
divin,0.005194
diyya,0.012719
dna,0.007560
dockyard,0.004698
doctor,0.002330
doctrin,0.004683
documentari,0.006001
doe,0.011891
doicbo,0.012002
domest,0.002023
door,0.003053
doubt,0.002520
downward,0.003134
dp,0.004046
draco,0.020112
draconian,0.004698
draft,0.005040
draw,0.002001
driven,0.002273
drop,0.006429
drown,0.003891
drug,0.013818
drum,0.003708
dtat,0.003134
duchi,0.003806
duel,0.004507
duke,0.003079
dure,0.017985
duti,0.002323
dynam,0.001830
dynasti,0.020313
e,0.001582
eagl,0.003719
earli,0.005879
earliest,0.003930
eastern,0.003902
econom,0.001286
ed,0.006583
edelstein,0.005184
edict,0.003595
edit,0.003680
effect,0.004568
effort,0.003256
egypt,0.002482
eichmann,0.005184
eighteen,0.003489
elabor,0.002446
elca,0.006001
electr,0.003887
electrocut,0.005102
element,0.002990
eleph,0.003252
elimin,0.006099
emerg,0.006027
emir,0.003351
emot,0.002415
emperor,0.015557
emphasi,0.002132
emphat,0.004090
employ,0.006237
enact,0.005212
encod,0.002862
encount,0.004631
encourag,0.005796
encyclopedia,0.004049
end,0.006427
endors,0.002782
enemi,0.013138
enforc,0.002204
engag,0.001861
england,0.008160
englishmen,0.004929
enjoy,0.002238
enlighten,0.002567
entail,0.002743
enter,0.005156
enthusiast,0.003351
entir,0.001524
entri,0.004548
entrust,0.003752
equit,0.003505
equival,0.001893
era,0.009287
error,0.008942
esoter,0.003595
especi,0.006825
espionag,0.011437
establish,0.003764
estim,0.003308
et,0.002020
ethic,0.006491
ethnic,0.009279
etymolog,0.002502
eu,0.020478
europ,0.027100
european,0.023428
euthanasia,0.004179
evangelium,0.010556
event,0.003059
eventu,0.003341
everi,0.005884
everyon,0.005428
everyth,0.002341
evid,0.011222
exampl,0.018952
excess,0.002271
exchang,0.003323
exclud,0.004423
execut,0.208364
execution,0.010204
exercis,0.006059
exhibit,0.002219
exhort,0.004428
exil,0.002685
exoner,0.009534
expans,0.001983
experi,0.001445
expert,0.002322
explain,0.003165
explicit,0.002567
explicitli,0.002393
expound,0.003493
express,0.001523
extend,0.003101
extermin,0.007701
extern,0.000919
extraleg,0.005064
extramarit,0.010369
extrem,0.003372
face,0.007058
faci,0.004524
fact,0.002985
facto,0.008550
factor,0.001490
fail,0.003535
failur,0.004091
fair,0.002381
faith,0.002350
fall,0.004814
fallibl,0.004081
famili,0.004883
famou,0.005826
fanat,0.004413
far,0.003188
fasad,0.006001
fascist,0.003485
father,0.002066
favor,0.003883
favour,0.011559
fear,0.004411
februari,0.003820
feder,0.007487
fell,0.002270
fellowship,0.003374
femal,0.002360
feud,0.023786
fifth,0.005007
fiftysix,0.005184
figur,0.001741
final,0.004561
fine,0.002513
finn,0.004158
fiqh,0.008886
firearm,0.003637
firm,0.002099
fit,0.002140
fiveyear,0.002862
flay,0.005330
florida,0.003086
focu,0.003449
fodder,0.004357
folk,0.003039
follow,0.010056
food,0.001809
footbal,0.003145
forbid,0.010142
forc,0.003780
foreign,0.001636
forfeit,0.008826
forgiv,0.007688
form,0.020179
formal,0.009438
foster,0.002687
foundat,0.001639
founder,0.004406
fraction,0.002460
franc,0.005399
franci,0.009629
franciss,0.005765
free,0.002871
freedom,0.003969
french,0.001691
frequent,0.003534
friend,0.002496
fulli,0.005685
fundament,0.003219
furman,0.005230
furthermor,0.002154
futil,0.003831
futur,0.003062
ga,0.004230
gabon,0.004020
gail,0.009270
gallup,0.004233
garrot,0.005514
gauntlet,0.005671
gaza,0.004090
gender,0.002695
gener,0.015356
genesi,0.003162
genocid,0.013604
genuin,0.002916
georgia,0.006009
germani,0.001892
gift,0.002837
girl,0.006207
given,0.003782
global,0.003338
glossip,0.006359
god,0.008801
golden,0.002547
good,0.002795
gospel,0.010602
govern,0.011954
governmentsemploy,0.006359
grade,0.002932
grand,0.004883
grandpar,0.004458
grant,0.005781
grave,0.006128
great,0.008501
greec,0.002376
gregg,0.004559
groom,0.004158
gross,0.007373
ground,0.005487
group,0.006803
grow,0.003224
growth,0.001591
guarante,0.004612
guardian,0.003106
guatemala,0.007600
guillotin,0.009687
guilt,0.003782
guilti,0.023618
guiltshamefear,0.006359
guinea,0.002968
gun,0.005903
ha,0.037721
habsburg,0.003891
hadith,0.004004
half,0.003460
hamascontrol,0.005671
hammurabi,0.004343
hand,0.006230
handbook,0.002544
hang,0.023571
happen,0.001986
hardlin,0.004280
harm,0.007270
hast,0.003898
head,0.011360
heal,0.009244
held,0.001486
help,0.002752
henc,0.001961
henri,0.002026
herbermann,0.004490
heterosexu,0.004458
hi,0.020289
high,0.003818
highway,0.003172
hindu,0.008743
hinduism,0.006045
histor,0.008473
histori,0.004188
historian,0.002118
hitler,0.003425
hodgkinson,0.005447
hold,0.004542
holi,0.007985
holiday,0.002959
homicid,0.012953
homosexu,0.007274
honour,0.002964
horizon,0.003064
horribl,0.004559
hospit,0.002479
hostil,0.002607
hotli,0.004317
howev,0.013263
hudud,0.028356
human,0.048447
humanita,0.005142
humili,0.003877
hypnot,0.004330
ibrahim,0.003687
iccpr,0.005671
icu,0.004817
idea,0.001410
ident,0.003600
ideolog,0.004749
ii,0.011516
ill,0.004882
imagin,0.002513
imit,0.002974
immanuel,0.003172
immedi,0.001847
impal,0.005230
imperfect,0.003094
import,0.003216
impos,0.015133
imprison,0.005978
improp,0.003708
improv,0.003093
inadmiss,0.004961
incest,0.004524
incid,0.002487
includ,0.020944
incompar,0.004541
incompat,0.003203
incompet,0.003708
increas,0.006044
increasingli,0.009266
india,0.011548
indiana,0.003175
indic,0.004639
individu,0.005186
indonesia,0.011911
industrialis,0.003555
inflict,0.027377
influenc,0.005538
influenti,0.002021
inform,0.006249
infrequ,0.003794
inhabit,0.002185
inhal,0.003891
inhuman,0.016363
iniqu,0.005514
inject,0.015519
injunct,0.004090
injuri,0.002786
injustic,0.007314
inmat,0.008741
inner,0.002527
innoc,0.026581
insan,0.003877
inscrib,0.003863
insid,0.002108
instanc,0.003405
instead,0.004417
institut,0.002660
instrument,0.003871
insubordin,0.010204
insuffici,0.002850
intact,0.006444
intellectu,0.002156
intent,0.006346
inter,0.003534
interim,0.003016
intern,0.031947
interpret,0.003354
interven,0.002701
introduc,0.004424
inviol,0.004843
involv,0.006093
ip,0.003919
iran,0.054183
iraq,0.005626
irrevers,0.013880
isbn,0.001486
islam,0.037668
islamist,0.003770
israel,0.005149
issu,0.006744
italian,0.004717
j,0.001578
jail,0.003590
jamaica,0.003608
jame,0.001773
japan,0.018710
jare,0.003987
jeremi,0.003316
jesu,0.015397
jew,0.005910
jewish,0.020848
john,0.010009
joseph,0.002071
ju,0.004280
judaism,0.003006
judg,0.004642
judgement,0.003177
judgment,0.002664
judici,0.016893
juli,0.003631
june,0.001831
jure,0.011293
juri,0.003521
jurisdict,0.005102
jurisprud,0.003525
jurist,0.003708
just,0.007329
justic,0.044615
justifi,0.012640
juvenil,0.048005
kant,0.003041
karl,0.002364
karma,0.007552
kashif,0.006359
kazakhstan,0.007265
keelhaul,0.006359
keeper,0.004100
kenneth,0.002820
kenya,0.003497
key,0.001599
khmer,0.004100
kick,0.003912
kidnap,0.010704
kill,0.031128
killer,0.007575
kind,0.001674
king,0.018020
kingdom,0.005016
kitab,0.003941
knife,0.004063
known,0.006302
korea,0.012685
kronenwett,0.006359
l,0.001850
label,0.002261
lack,0.001602
ladder,0.003642
lama,0.004305
lambeth,0.006001
lancet,0.004615
land,0.006362
lane,0.003214
lanka,0.010063
lankan,0.004233
larg,0.004430
largest,0.001735
late,0.001455
later,0.004999
latin,0.005926
latterday,0.004211
latvia,0.003708
laud,0.004037
law,0.037984
lawyer,0.003049
lay,0.002365
lead,0.007524
leader,0.001837
leav,0.008899
led,0.002712
left,0.001675
legal,0.019077
legisl,0.003848
legitim,0.010442
legitimaci,0.006433
leopold,0.007059
lest,0.004128
lethal,0.021737
level,0.002513
levi,0.002953
leviticu,0.014301
liberia,0.003782
liberti,0.008003
lieu,0.003964
life,0.032424
like,0.003367
likewis,0.002489
limit,0.003850
ling,0.004524
lingnan,0.005671
linguist,0.002470
link,0.005483
list,0.003505
lit,0.003501
littl,0.001608
live,0.008153
lobbi,0.003230
local,0.002846
lock,0.002722
lodg,0.003568
lollard,0.005447
long,0.005458
longer,0.003466
loot,0.003559
lord,0.002487
lose,0.004420
loss,0.001838
lost,0.001826
love,0.004990
lower,0.001552
luke,0.007701
lunar,0.003344
lushan,0.005588
luther,0.003657
lutheran,0.007564
m,0.001503
magic,0.002930
magistr,0.003301
mahmoud,0.004305
maimonid,0.007853
maimonidess,0.006359
mainli,0.001744
maintain,0.005861
major,0.008919
make,0.004274
makwan,0.006359
makwanyan,0.006359
malawi,0.004090
malaysia,0.003070
male,0.002357
malevol,0.004072
man,0.013015
mandat,0.005236
mandatori,0.003230
mani,0.016148
manner,0.004056
manor,0.004370
manslaught,0.009440
mao,0.003844
march,0.001742
marginalis,0.004211
marhoni,0.006359
marian,0.004490
marino,0.008379
martin,0.002224
martyr,0.003687
marx,0.002937
mass,0.001723
massacr,0.006128
massiv,0.002236
materi,0.001472
matter,0.009484
matthew,0.006094
mauritania,0.003806
maximum,0.002285
mazzatello,0.006359
mean,0.010416
measur,0.004129
media,0.001927
mediev,0.004483
meet,0.003400
megalaw,0.006359
member,0.017431
membership,0.004629
mennonit,0.004720
mental,0.002274
merci,0.018186
mere,0.002067
messag,0.010446
method,0.010412
methodist,0.011737
michael,0.003868
michigan,0.006065
middl,0.003465
milit,0.003283
militari,0.010200
millennium,0.002623
million,0.003301
minimum,0.002292
minist,0.005688
minor,0.015558
miscarriag,0.009082
mischief,0.004698
misus,0.003466
mix,0.002021
mode,0.007097
model,0.001309
modern,0.013865
modernday,0.002991
modifi,0.002050
moharebeh,0.012719
moksha,0.004305
moloudzadeh,0.006359
moment,0.004781
monarch,0.002775
monarchi,0.002693
monetari,0.004505
money,0.007512
mongolia,0.003647
monk,0.003108
monkhood,0.005588
monster,0.003752
month,0.001856
monthli,0.002864
moral,0.006307
moratorium,0.050279
moreov,0.004821
mormon,0.004148
morn,0.003053
mose,0.003429
mostli,0.003586
mother,0.002421
motiv,0.002053
mount,0.002449
movement,0.002898
multipl,0.001616
murder,0.074732
muslim,0.014101
muslimmajor,0.004767
mutini,0.003652
myanmar,0.003912
n,0.001887
nafeek,0.006359
nation,0.024454
nationst,0.003525
natur,0.003365
nazi,0.002930
nd,0.001955
nearest,0.006774
nearli,0.005491
necess,0.007900
necessari,0.001674
necessarili,0.002057
neck,0.010342
necklac,0.004655
need,0.005053
negoti,0.002199
neighbour,0.005134
neutral,0.004600
new,0.007509
newli,0.004422
news,0.002293
ngo,0.006910
nigeria,0.003364
nikaya,0.004541
nobil,0.003277
nonbind,0.009534
nonetheless,0.002628
nonexist,0.006526
nongovernment,0.003360
nonleth,0.004961
nonmuslim,0.011275
nonnegoti,0.005142
nonpain,0.005765
nonstat,0.004305
nonviol,0.007245
norman,0.005922
norsemen,0.005028
north,0.006674
norway,0.002953
notabl,0.011288
note,0.004846
notori,0.003335
notwithstand,0.003730
novemb,0.013194
nowaday,0.003180
nsw,0.004507
number,0.008410
nuremberg,0.004280
obedi,0.003662
object,0.001436
objection,0.004541
oblig,0.002320
observ,0.002773
occur,0.008011
octob,0.003634
odd,0.002964
offenc,0.085298
offend,0.066832
offens,0.002928
offer,0.006250
offic,0.003383
offici,0.019145
oftentim,0.004428
oklahoma,0.007624
old,0.006955
oligarchi,0.003891
omiss,0.004054
onc,0.003140
onli,0.024184
onlin,0.007357
onward,0.002664
open,0.001438
oper,0.002651
opin,0.004268
opinion,0.011167
oppon,0.013052
oppos,0.026343
opposit,0.012104
oppress,0.006198
option,0.010964
orchard,0.004292
order,0.008106
ordinari,0.004583
organ,0.008024
organis,0.005968
origin,0.002329
orthodox,0.005659
otherwis,0.001998
ought,0.003016
ourselv,0.003394
outcom,0.002148
outlaw,0.003374
outrag,0.003618
outright,0.003254
outsid,0.001603
overberg,0.006359
overrepresent,0.009922
overwhelmingli,0.006816
pacif,0.002435
page,0.004120
pain,0.005323
painless,0.005028
pakistan,0.020108
palestinian,0.003632
pali,0.015766
pancasila,0.005588
panic,0.003525
parad,0.003980
paradox,0.002687
paramount,0.003525
parent,0.004733
parliament,0.004239
parliamentari,0.002406
parol,0.009440
parshat,0.006359
parti,0.008120
particip,0.001657
particularli,0.007368
partli,0.002259
pass,0.006530
passag,0.005007
past,0.008335
paul,0.005480
pay,0.001961
payment,0.002265
peac,0.012160
peacetim,0.012112
penal,0.010577
penalti,0.597272
penaltycapit,0.006359
penaltycom,0.006359
pend,0.003546
pene,0.005447
penitenti,0.005386
penitentiari,0.004743
pentateuch,0.004655
peopl,0.020855
perceiv,0.008453
percent,0.008442
perform,0.004432
pericop,0.005765
period,0.011238
perman,0.006109
permiss,0.002921
permit,0.008561
perpetr,0.018437
persia,0.003129
person,0.031627
perspect,0.001880
peter,0.001916
petti,0.007438
phenomenon,0.002164
philippin,0.003014
philosoph,0.001844
photograph,0.002892
physic,0.002750
physician,0.002650
pillar,0.003185
piraci,0.003724
pitaka,0.005142
place,0.016638
plain,0.002746
plan,0.003183
plea,0.003912
pli,0.004081
poem,0.003101
point,0.002438
poland,0.002870
pole,0.002911
polic,0.009348
polici,0.009331
polit,0.019784
poll,0.005850
poor,0.008311
pope,0.011821
popul,0.004485
popular,0.001503
portug,0.002807
posit,0.006227
possibl,0.008603
post,0.002039
posthum,0.003432
postsecond,0.005330
potent,0.003782
potenti,0.003120
power,0.009865
pp,0.003929
practic,0.028365
practis,0.025363
pratt,0.004179
prc,0.011694
preach,0.003440
preced,0.002283
precept,0.003770
precipit,0.002771
predomin,0.002748
prefer,0.001845
prematur,0.003429
prerequisit,0.003485
prescrib,0.017656
presenc,0.001822
present,0.003614
preserv,0.002031
presid,0.001742
press,0.004254
presum,0.002690
presumpt,0.003800
prevent,0.005168
priest,0.002853
prima,0.004292
primari,0.004772
primarili,0.003294
primit,0.002666
princ,0.002600
principl,0.001446
prior,0.001820
prison,0.017903
privat,0.005183
privileg,0.005500
pro,0.014005
problem,0.001288
procedur,0.004197
process,0.002278
proclaim,0.002726
prodeath,0.012315
professor,0.002153
program,0.001515
programm,0.002316
progress,0.001642
prohibit,0.019671
promot,0.005157
prompt,0.002638
promulg,0.003072
proof,0.007371
properti,0.005836
proport,0.001992
propos,0.002973
prosecut,0.009773
prosecutor,0.011437
protect,0.008030
protest,0.008958
protocol,0.032697
proven,0.002545
provid,0.006415
psycholog,0.004108
public,0.027913
publicli,0.005157
publish,0.006687
punish,0.402947
purg,0.003370
purpos,0.003135
pursu,0.002191
qatar,0.007505
qisa,0.023060
qisasrel,0.006359
quarter,0.005013
question,0.003026
quick,0.002977
quot,0.002521
quran,0.006986
r,0.003225
rabbi,0.012012
rabbin,0.008423
racial,0.005918
radelet,0.005765
rais,0.001777
randomli,0.003257
rang,0.005786
rank,0.002034
rape,0.010731
rapid,0.002088
rapist,0.004900
rare,0.005949
rate,0.001493
ratif,0.003770
ratifi,0.030707
ration,0.002076
ratzing,0.005142
reach,0.003106
reaction,0.001959
read,0.002654
real,0.001586
reason,0.008229
rebellion,0.005457
reborn,0.004317
receiv,0.007300
recent,0.014557
recogn,0.003277
recognit,0.004189
recommend,0.002252
reconcili,0.003198
record,0.006443
recours,0.007575
ree,0.004211
refer,0.003821
refin,0.002397
reflect,0.006569
reform,0.005608
refus,0.004460
regard,0.008797
regim,0.002227
region,0.008211
regul,0.003618
regularli,0.002601
rehabilit,0.003295
reign,0.002642
reinstat,0.006947
reintroduc,0.003525
reintroduct,0.004384
reiter,0.003692
rel,0.004110
relat,0.003091
relev,0.003997
religi,0.021474
religion,0.017518
remain,0.005055
remot,0.002587
remov,0.005293
render,0.002506
renounc,0.006865
renunci,0.004148
reorgan,0.003195
repeal,0.003458
repel,0.003316
repent,0.016846
replac,0.003114
reporedli,0.006359
report,0.002937
reportedli,0.003034
repres,0.002507
repress,0.002882
reprieveorg,0.006359
republ,0.017937
request,0.002322
requir,0.010828
research,0.002479
reserv,0.002032
resist,0.001980
resolut,0.025717
resort,0.002836
resourc,0.003094
respect,0.005813
respons,0.006627
restat,0.003667
restitut,0.004443
restor,0.006484
restrain,0.003627
restrict,0.001771
result,0.008516
resum,0.005846
retain,0.016078
retali,0.010063
retent,0.003555
retentionist,0.038159
retort,0.004871
retribut,0.025076
return,0.001546
reveal,0.002010
revel,0.003064
reveng,0.018624
rever,0.003411
review,0.001712
revis,0.002283
reviv,0.004857
right,0.054858
righteou,0.004158
risk,0.001880
rizana,0.006359
rld,0.006001
robberi,0.008423
robert,0.005041
rod,0.006608
role,0.001338
roman,0.014350
roper,0.010460
rosicrucian,0.004961
roug,0.004222
roughli,0.002143
row,0.014777
royal,0.004033
royalti,0.003485
rule,0.004185
run,0.003359
russia,0.013476
rwanda,0.003919
s,0.003251
sabbath,0.004458
safeti,0.004926
saga,0.007810
saint,0.005111
san,0.004854
sanction,0.002869
satan,0.003870
satisfactori,0.006955
saudi,0.049845
save,0.002170
saw,0.005610
say,0.003311
scaphism,0.006359
schaba,0.006001
scheme,0.002300
scholar,0.007763
school,0.006219
scotland,0.002872
scourg,0.009583
scriptur,0.006150
second,0.007526
sect,0.003200
secur,0.004871
sedit,0.004720
seek,0.006859
seen,0.001499
select,0.003323
sell,0.002161
send,0.002390
sent,0.002118
sentenc,0.065320
separ,0.002792
separat,0.004317
seri,0.001522
seriou,0.014986
sermon,0.011493
servic,0.001489
session,0.002907
set,0.002367
settlement,0.004460
seven,0.002005
sever,0.015524
sex,0.005292
sexual,0.007534
shah,0.003538
shahzada,0.006359
shall,0.005579
shalt,0.009687
shame,0.003972
sharia,0.022586
shariarul,0.006359
shia,0.003608
shield,0.002997
shift,0.003704
shiit,0.004343
shinto,0.004179
shirk,0.004698
shoot,0.006482
shoplift,0.005588
shorter,0.002720
shortli,0.002378
shot,0.002972
shown,0.001843
shreveport,0.006001
shun,0.007868
sign,0.007005
significantli,0.001961
sihanada,0.006359
silvano,0.005514
similar,0.001270
similarli,0.004030
simmon,0.008687
simpli,0.003471
sinc,0.012763
sing,0.007043
singapor,0.014014
singl,0.005693
sir,0.002400
situat,0.009825
sixth,0.008471
sj,0.004399
slaughter,0.003529
slave,0.005026
slaveri,0.002802
slay,0.005102
slice,0.003758
slipperi,0.004559
slope,0.003070
slow,0.004391
small,0.006507
smith,0.004507
socal,0.002146
social,0.012177
societi,0.007995
societiesboth,0.006359
socioeconom,0.003072
sodomi,0.019600
solar,0.005074
soldier,0.004840
sole,0.004318
solon,0.004961
somalia,0.014588
somebodi,0.004119
someon,0.007453
sometim,0.002769
soon,0.001950
sorceri,0.004507
soul,0.005492
sound,0.002239
sourc,0.001337
south,0.013325
southeast,0.002603
southern,0.001974
soviet,0.004607
spare,0.003310
speak,0.006118
spear,0.003800
special,0.005321
specif,0.001272
specifi,0.002162
spectacl,0.003980
spectrum,0.005030
speech,0.002291
sphere,0.002380
spike,0.003682
spinal,0.003708
spiral,0.003254
spur,0.002987
squad,0.004090
sri,0.012379
st,0.003429
stadium,0.004138
stake,0.003235
stalin,0.003497
stanc,0.011728
stand,0.007995
start,0.002692
state,0.077245
statement,0.007810
statesfor,0.006359
statist,0.005235
statu,0.001776
statut,0.009373
statutori,0.003546
steadi,0.005452
steal,0.007018
stem,0.004781
stereotyp,0.003493
steven,0.002680
stipul,0.003216
stolen,0.003572
stone,0.019003
stool,0.004507
stop,0.002101
stori,0.006468
strangul,0.020920
stress,0.004362
strict,0.002471
strictli,0.002508
strong,0.001545
strongli,0.004100
stuart,0.002950
student,0.001810
studi,0.006744
subcommiss,0.005765
subject,0.006765
submerg,0.003776
subsaharan,0.003332
substanti,0.001974
substitut,0.004727
succeed,0.002174
sudan,0.016374
suffer,0.007765
suffici,0.005899
suffoc,0.004767
suggest,0.004467
suicid,0.003049
suit,0.002472
summari,0.002559
sunni,0.006715
superior,0.002367
supersed,0.003072
support,0.030351
suppress,0.002435
suprem,0.008712
sure,0.002914
surviv,0.001834
suspend,0.005287
suspens,0.003304
sutta,0.009311
swell,0.003513
swept,0.003477
symbol,0.001931
syria,0.006253
taiwan,0.002993
taken,0.012459
talmud,0.016846
tang,0.037802
tape,0.003470
target,0.001980
teach,0.018752
tell,0.002395
temper,0.003208
templ,0.005623
tend,0.005239
tendenc,0.002388
tension,0.004831
term,0.002010
terribl,0.003677
territori,0.001861
terror,0.014157
terrorist,0.003219
testament,0.009996
texa,0.003002
text,0.005504
th,0.013415
thai,0.003812
thailand,0.006428
thcenturi,0.002452
theft,0.010652
thenindepend,0.006359
theologian,0.003145
theori,0.002493
theravadan,0.005765
therefor,0.004337
thi,0.034209
thiev,0.008585
thing,0.003384
think,0.001844
thoma,0.003805
thompson,0.003158
thou,0.008127
thousand,0.009378
threat,0.004461
threaten,0.002418
throatcut,0.006001
thu,0.003730
tie,0.002074
time,0.014116
timor,0.004037
today,0.009168
tokyo,0.003370
told,0.002697
tomasi,0.005330
tomb,0.003387
took,0.004984
tool,0.003406
topic,0.001687
torah,0.003926
tortur,0.019205
total,0.002968
totalitarian,0.003489
town,0.002184
trace,0.002039
trade,0.001538
tradit,0.004018
traffick,0.014136
transcrib,0.007154
transfer,0.003640
transform,0.001741
transgress,0.004222
transit,0.001851
translat,0.001831
transport,0.001823
treason,0.026474
treati,0.004442
treatis,0.002566
treatment,0.001956
tree,0.002190
trend,0.008126
trial,0.014562
tribal,0.011866
tribe,0.013092
true,0.001722
truncat,0.004029
trust,0.002325
turkey,0.013869
turkish,0.006158
turn,0.007352
tuscani,0.008917
tutsi,0.005142
tv,0.002940
twelv,0.002805
twothird,0.002914
type,0.003836
typic,0.001426
ukrain,0.003177
ultim,0.003679
unaccept,0.003466
unanim,0.003354
unanticip,0.004343
unavail,0.003477
uncivil,0.004791
unconsci,0.003180
unconstitut,0.011780
underscor,0.003934
underway,0.003418
uneduc,0.004474
unequ,0.003304
unfair,0.003538
unfairli,0.004413
unicef,0.004399
unifi,0.002226
unintent,0.003752
union,0.017386
uniqu,0.001813
unislam,0.005278
unit,0.034330
univers,0.004406
unjust,0.003752
unknown,0.002202
unless,0.002383
unnecessari,0.003141
unpunish,0.005765
unreason,0.003949
unworthi,0.004524
upcom,0.003662
updat,0.005153
uphold,0.003497
urg,0.002807
usa,0.002374
use,0.035633
usual,0.002559
utah,0.007701
utilitarian,0.006741
utopia,0.003747
v,0.017068
valid,0.002031
valu,0.002726
van,0.004653
vander,0.005278
vari,0.007840
variat,0.001993
variou,0.007265
vatican,0.003590
vendetta,0.010460
venezuela,0.003263
vengeanc,0.013284
veri,0.002429
verifi,0.002688
versu,0.002482
victim,0.031439
victori,0.002463
vietnam,0.005764
view,0.014995
viewpoint,0.002799
viii,0.003387
villag,0.002430
violat,0.014120
violenc,0.025331
violent,0.013132
virginia,0.003064
visit,0.002136
vita,0.008585
volunt,0.002932
voluntarili,0.003447
vote,0.008068
wa,0.053199
wage,0.004674
waist,0.004676
wake,0.002837
wali,0.004443
walk,0.002716
wall,0.002252
want,0.003927
war,0.021153
warfar,0.008092
warn,0.005115
warrant,0.006561
wartim,0.006774
washington,0.004683
watch,0.005423
watchlist,0.005588
way,0.005686
wc,0.004490
wealthi,0.002775
weapon,0.002361
websit,0.002011
welcom,0.002935
welfar,0.002449
wellknown,0.002496
went,0.001950
west,0.001826
western,0.004799
weyd,0.005765
wheel,0.003203
wherea,0.001845
wherebi,0.002403
whi,0.005871
white,0.004038
wick,0.004090
wide,0.005479
wider,0.002421
widespread,0.002043
william,0.003408
win,0.002391
wing,0.002895
wit,0.007772
witch,0.007675
witchcraft,0.012087
woman,0.002655
women,0.002024
wont,0.003698
word,0.004429
work,0.002085
worker,0.003959
world,0.022645
worldwid,0.004315
worst,0.005867
worth,0.007225
wrestl,0.004020
write,0.001663
written,0.003368
wrong,0.010006
wrongdo,0.004305
wrongdoer,0.005330
wrote,0.005436
xuanzong,0.015554
year,0.017175
yemen,0.026151
york,0.003276
yosef,0.004961
youth,0.002682
zambia,0.007782
zealand,0.005058
zedong,0.004020
zen,0.003837
zimbabw,0.003477
zina,0.016544
