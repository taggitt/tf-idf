abus,0.027023
accord,0.018843
achiev,0.007902
activ,0.025070
addit,0.012709
administr,0.026347
aerial,0.017689
agenc,0.029644
agreement,0.067115
agricultur,0.018672
ai,0.015779
aid,0.018846
albeit,0.015877
altern,0.008071
amend,0.013246
america,0.035212
american,0.042701
andean,0.122191
andr,0.016395
ani,0.005535
announc,0.010548
antarct,0.019000
antarcticenvironment,0.031995
antidrug,0.027695
antiguerrilla,0.033044
antimoneylaund,0.031181
appli,0.006714
applic,0.007501
approv,0.010790
april,0.009609
arbitr,0.017799
archipelago,0.015943
area,0.018715
arm,0.010015
arrest,0.041414
articl,0.014930
asset,0.023223
associ,0.012272
atom,0.010983
attempt,0.007164
august,0.019090
author,0.014144
aviat,0.016524
ayacucho,0.027987
background,0.010787
bajo,0.026124
ban,0.012655
bank,0.072669
becam,0.006923
becom,0.006063
biggest,0.014219
bilater,0.057622
biodivers,0.015695
biolog,0.008982
bodi,0.015200
bolivia,0.038939
border,0.055785
boundari,0.010573
brazil,0.013534
broad,0.010173
broaden,0.016150
build,0.008014
bureau,0.014325
cali,0.028652
cannabi,0.020479
caribbean,0.072361
caricom,0.020290
cartel,0.061674
case,0.006342
censu,0.014874
chain,0.011332
chair,0.014247
challeng,0.009061
chamber,0.013460
chang,0.005719
chase,0.017247
chemic,0.019318
chile,0.065633
china,0.019492
cicad,0.031181
cite,0.010609
civil,0.008701
claim,0.007700
claus,0.016897
climat,0.010831
close,0.006787
coca,0.048596
cocain,0.106037
code,0.009816
collabor,0.023306
colombia,0.674553
colombiaecuador,0.033044
colombian,0.278226
colombiau,0.033044
commerc,0.037926
commerci,0.009719
commiss,0.010100
commission,0.015602
commit,0.010633
committe,0.021768
common,0.006134
commun,0.019901
complic,0.011497
comprehens,0.011175
concern,0.021368
confeder,0.030286
confer,0.009987
confisc,0.017818
conflict,0.009449
congress,0.034103
conserv,0.010071
consist,0.013581
constitut,0.008010
contadora,0.033044
continu,0.012294
control,0.033704
convent,0.029473
cooper,0.029091
cordial,0.019939
corpor,0.009954
corrupt,0.012456
council,0.009251
councila,0.033044
countri,0.026707
court,0.028869
cover,0.008353
credibl,0.031580
crime,0.012588
crimin,0.051406
critic,0.007463
crop,0.012543
cross,0.011073
csar,0.023688
cultiv,0.025917
cultur,0.007628
current,0.006495
czar,0.025167
death,0.008886
decad,0.009222
decemb,0.018621
decid,0.009887
declar,0.009799
decreas,0.009991
deepen,0.018233
depart,0.008895
desertif,0.022639
despit,0.008603
develop,0.060890
did,0.015063
diminish,0.013736
diplomat,0.036161
disput,0.061232
distanc,0.010804
distribut,0.008371
doe,0.006865
domain,0.009909
drug,0.071798
drugcontrol,0.033044
dump,0.019873
dure,0.005840
econom,0.040108
economi,0.008342
ecuador,0.078115
ecuadorian,0.070789
educ,0.007781
effort,0.008459
emerg,0.007828
endang,0.016970
energi,0.008551
engag,0.009673
enter,0.008930
entiti,0.010364
environ,0.008097
erad,0.035525
escobar,0.025614
estim,0.017191
eu,0.093098
europ,0.008282
european,0.007608
evid,0.008329
expand,0.008640
extens,0.008248
extradit,0.129645
face,0.009168
farc,0.098104
favor,0.020177
februari,0.009924
feder,0.009725
figur,0.009046
financ,0.009679
financi,0.008720
focus,0.008502
follow,0.015675
food,0.009400
footsoldi,0.030516
fora,0.023418
forc,0.006546
forfeitur,0.050919
form,0.004992
fourthlead,0.031181
free,0.007460
freetrad,0.022304
fund,0.018012
gain,0.007946
gaviria,0.029953
gener,0.004693
germani,0.009835
gil,0.022241
given,0.006551
govern,0.062113
great,0.007361
group,0.070703
grow,0.008375
guerilla,0.021303
guidelin,0.014487
gulf,0.029552
ha,0.065330
hand,0.008093
hazard,0.014916
health,0.009105
help,0.007150
herbicid,0.024526
heroin,0.020558
hide,0.015462
high,0.006613
homeland,0.016897
host,0.011943
hour,0.011477
hous,0.008762
howev,0.015903
httplcweblocgovfrdc,0.021997
human,0.006292
ideolog,0.012338
ii,0.008548
illeg,0.052646
illicit,0.019712
imf,0.028608
immigr,0.038994
immun,0.014297
implement,0.009093
import,0.005570
improv,0.016075
includ,0.022671
incorpor,0.008969
increas,0.012562
indic,0.008035
individu,0.006737
industri,0.007455
influx,0.016854
initi,0.014194
institut,0.013823
integr,0.015764
intellectu,0.011205
interamerican,0.021660
interchang,0.014950
interdict,0.049052
intern,0.183161
intimid,0.018702
introduc,0.007662
invest,0.009299
involv,0.006332
island,0.009681
issu,0.035045
itali,0.011754
japan,0.021602
join,0.029146
jointmilitari,0.033044
kerlikowsk,0.033044
kingpin,0.029037
km,0.012292
known,0.005457
korea,0.026364
labour,0.022112
lack,0.008326
late,0.007560
latin,0.071847
launder,0.040806
law,0.013157
layer,0.012570
lead,0.013031
leader,0.009548
left,0.008704
leftist,0.016825
leftistterrorist,0.033044
legisl,0.009997
legitim,0.013563
lend,0.014029
lenient,0.023596
librari,0.010259
libyan,0.020217
life,0.007019
limit,0.013337
list,0.012143
longer,0.009006
lvaro,0.022639
main,0.006912
mainli,0.009064
major,0.017379
make,0.005552
mani,0.004935
manual,0.013792
marin,0.025000
maritim,0.056223
market,0.016346
materi,0.007651
measur,0.007151
medelln,0.027173
medic,0.010055
meet,0.008834
member,0.020900
membership,0.024054
meteorolog,0.016011
mexico,0.027245
mi,0.013767
migr,0.069259
migrat,0.011666
militari,0.008833
millennium,0.027257
million,0.008576
ministeriallevel,0.029953
ministri,0.011740
mission,0.021774
monetari,0.011705
money,0.019516
moneylaund,0.025776
monitor,0.011808
movement,0.015058
muchneed,0.022710
multilater,0.017328
narcot,0.060438
narcotraffick,0.029466
nation,0.048403
need,0.006563
negoti,0.045715
netherland,0.012724
nicaragua,0.039178
nonalign,0.036059
nonmonitor,0.033044
nonprolifer,0.024083
notabl,0.016757
novemb,0.009793
nuclear,0.044628
nuevo,0.023418
number,0.021849
oa,0.058061
observ,0.007206
offens,0.015215
offer,0.008119
offic,0.008788
offici,0.008289
olymp,0.016485
onli,0.005026
open,0.007475
oper,0.020665
opium,0.019650
organ,0.136983
outlaw,0.017530
ozon,0.019159
pablo,0.020558
pacif,0.012655
pact,0.032791
panama,0.017818
paramilitari,0.038318
particip,0.017227
particularli,0.007657
pass,0.016965
pastrana,0.059907
peac,0.031591
penalti,0.016333
pend,0.018427
peopl,0.006020
perman,0.010581
permit,0.022242
peru,0.051695
place,0.006175
plan,0.016538
play,0.007883
polic,0.024287
polit,0.006853
pollut,0.014687
poppi,0.023332
pose,0.013299
possibl,0.006386
postal,0.016811
postsummit,0.033044
potenti,0.008106
presid,0.027164
pressur,0.009425
prevent,0.017902
primarili,0.008558
procedur,0.010903
process,0.023676
produc,0.013029
product,0.006525
program,0.007876
prohibit,0.012775
properti,0.007580
prosecutor,0.019808
protect,0.025034
protocol,0.014157
provid,0.005555
providencia,0.026938
provis,0.012195
public,0.006305
quit,0.010581
quita,0.063990
r,0.008378
rang,0.007515
ratifi,0.047864
reach,0.008069
reason,0.007126
recent,0.013752
recogn,0.017029
reconstruct,0.012443
reduc,0.007522
reef,0.017428
reelect,0.016174
reentri,0.023086
refer,0.003970
refin,0.012456
refuge,0.014866
regard,0.015236
regardless,0.012004
region,0.021332
regularli,0.013517
relat,0.042832
reli,0.009568
remain,0.013134
repres,0.019541
research,0.006442
reserv,0.031682
resid,0.010520
respect,0.007551
rest,0.009303
restrict,0.018406
retroact,0.020598
return,0.016073
revis,0.011866
right,0.007125
rim,0.021022
rio,0.036301
role,0.006956
rural,0.012842
s,0.016895
san,0.012610
santo,0.018318
satellit,0.027709
savehaven,0.033044
scienc,0.013357
scientif,0.007990
sea,0.010131
secretari,0.012973
secur,0.042183
seek,0.008910
sentenc,0.027151
septemb,0.019152
serranilla,0.033044
serv,0.007904
sever,0.005761
ship,0.011267
shipment,0.018233
sign,0.045495
signatori,0.019105
signific,0.007054
significantli,0.010191
sinc,0.011052
social,0.007030
soit,0.029953
solut,0.009455
sophist,0.012715
sourc,0.013903
south,0.008654
sovereignti,0.026654
speci,0.010163
spillov,0.023783
spray,0.020804
sq,0.016498
stand,0.010386
standard,0.007443
state,0.067697
statesmilitari,0.031181
steadili,0.014578
stem,0.012422
strategi,0.020589
strengthen,0.012500
stronger,0.013305
studi,0.005840
submerg,0.019619
subsequ,0.008624
subsidiari,0.016713
sueo,0.066088
suggest,0.007736
summit,0.032325
supplier,0.015561
support,0.013141
surrend,0.015225
sweden,0.013804
taken,0.008091
technic,0.009828
technolog,0.007968
telecommun,0.028811
tension,0.012552
territori,0.019346
terror,0.014711
terrorist,0.016727
test,0.018069
thi,0.008267
thu,0.006460
timber,0.033102
tlatelolco,0.028305
took,0.008633
toughen,0.026311
tourism,0.012856
trade,0.079945
tradit,0.006959
traffick,0.073447
transit,0.009621
transnat,0.019589
transship,0.021714
treati,0.080787
treatment,0.010166
trial,0.012610
tripl,0.015225
tropic,0.028205
union,0.049273
unit,0.078037
univers,0.005723
unrestrict,0.019744
urib,0.082276
usd,0.016564
vastli,0.017280
venezuela,0.152603
voic,0.013172
vote,0.010480
wa,0.027187
wast,0.013256
water,0.008524
weapon,0.036816
websit,0.010450
wetland,0.019558
white,0.010492
wholesal,0.017200
withdrew,0.015531
world,0.061632
y,0.011768
year,0.005577
