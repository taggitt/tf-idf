abov,0.012775
absolut,0.016925
academ,0.014945
achiev,0.012553
act,0.034214
action,0.012260
activ,0.009956
actor,0.046005
ad,0.025468
administr,0.111616
affect,0.026079
agenc,0.015697
alway,0.013418
amend,0.021042
ani,0.008794
antiqu,0.020649
appear,0.011557
appli,0.010667
appoint,0.016403
approach,0.011484
area,0.029732
arguabl,0.024319
ask,0.016495
asymmetr,0.027299
attempt,0.011382
austria,0.023086
austrian,0.023901
author,0.044940
award,0.017591
basi,0.012429
basic,0.024209
bear,0.018673
begin,0.011732
belong,0.017000
benefit,0.014600
bodi,0.060368
border,0.017724
borderlin,0.033920
branch,0.038386
brand,0.024319
budget,0.018833
bureaucrat,0.026661
busi,0.013563
case,0.010075
central,0.022592
centuri,0.020695
citizen,0.049475
civil,0.096766
claim,0.012232
clear,0.014999
code,0.031188
combin,0.011492
commit,0.016892
common,0.019490
commonli,0.013384
compet,0.033233
compris,0.016487
concern,0.067892
consequ,0.013705
consid,0.037598
constitut,0.101810
contract,0.016168
control,0.010708
countri,0.031820
court,0.030575
creation,0.014671
crime,0.019997
crimin,0.081664
date,0.013471
deal,0.012984
debat,0.015005
decis,0.040768
defin,0.022122
delin,0.027686
depend,0.010634
describ,0.009920
develop,0.016121
differ,0.034178
direct,0.010702
discuss,0.013151
disput,0.016212
distinct,0.074482
divid,0.012781
divis,0.013969
doctrin,0.019329
document,0.031122
doe,0.010905
elect,0.014312
element,0.012341
emphas,0.017030
employ,0.051486
endow,0.024579
enforc,0.018192
enlighten,0.042377
enshrin,0.031783
entiti,0.016464
entrench,0.029525
equal,0.013214
era,0.015331
est,0.026959
et,0.016680
everi,0.012142
evolv,0.015928
exactli,0.019010
exampl,0.017381
exclus,0.033155
execut,0.029398
exhaust,0.022689
exist,0.009649
explain,0.013063
extens,0.013103
fail,0.014589
feder,0.015450
field,0.021194
fit,0.017667
focus,0.013507
form,0.007931
foundat,0.013531
franc,0.014854
french,0.013964
function,0.010623
fundament,0.013287
germani,0.015624
germanlanguag,0.039762
govern,0.128276
greek,0.014693
ha,0.020757
hand,0.025714
histor,0.011656
howev,0.016842
human,0.009997
idea,0.011646
imperium,0.079525
impli,0.016131
impos,0.017845
individu,0.053514
inspector,0.031167
inspir,0.017167
instanc,0.014052
institut,0.010980
intern,0.009092
intra,0.037782
investig,0.014539
involv,0.020118
issu,0.011134
iu,0.075874
judici,0.039840
judiciari,0.023086
jurisdict,0.063179
jurist,0.030611
justic,0.017535
kant,0.025101
kingdom,0.013801
know,0.015721
lat,0.038962
latest,0.023171
law,0.731549
lay,0.019521
legal,0.042945
legem,0.052494
legisl,0.047648
legislatur,0.021223
level,0.010373
like,0.027797
literatur,0.015498
local,0.011749
look,0.014323
make,0.008821
manageri,0.028217
mandatori,0.026661
mani,0.007840
manufactur,0.016542
matter,0.013048
meet,0.014035
membership,0.019106
modern,0.031213
municip,0.021136
mutual,0.018297
natur,0.018520
nineteenth,0.021697
note,0.010000
obey,0.024753
offic,0.013962
onli,0.015969
order,0.019117
origin,0.009612
otherwis,0.016499
paragon,0.039552
parti,0.026809
particular,0.043429
partli,0.018651
payment,0.018696
peaceabl,0.042112
person,0.056751
philosoph,0.015225
pick,0.022578
play,0.012523
point,0.010061
polit,0.021773
pollut,0.023333
posit,0.020560
postul,0.021401
power,0.030536
practic,0.010642
precis,0.016384
prevail,0.021042
privat,0.256719
privatum,0.052494
procedur,0.034644
protect,0.013256
provid,0.008825
public,0.310536
publicum,0.104989
pure,0.015728
question,0.012490
quod,0.081383
reason,0.011321
refer,0.006308
regul,0.044802
rei,0.033396
relat,0.008505
relationship,0.108372
respons,0.010941
review,0.014134
revit,0.030655
revolut,0.015627
right,0.090560
rise,0.012694
roman,0.050763
romana,0.036802
rousseau,0.029488
rule,0.046060
safeti,0.020332
sanction,0.023683
say,0.013667
secondli,0.027740
secundum,0.044461
seen,0.024755
separ,0.011524
set,0.029316
sever,0.009152
signific,0.011207
sinc,0.008778
singulorum,0.052494
situat,0.013517
social,0.022336
societi,0.021999
sometim,0.034286
special,0.010980
spectat,0.033615
spread,0.015518
state,0.076817
statum,0.052494
strong,0.012756
subcategori,0.030022
subject,0.055842
subordin,0.048461
suppli,0.014312
suppos,0.019291
suprem,0.017978
supremaci,0.028705
tax,0.016499
taxat,0.021584
th,0.010066
theoret,0.028790
theori,0.092615
thi,0.065666
thirdli,0.033325
togeth,0.012106
trade,0.012700
tradit,0.022110
ulpian,0.045518
understand,0.011917
unequ,0.027274
unhappi,0.030790
unilater,0.027528
unit,0.008855
unwritten,0.036675
use,0.007173
utilitatem,0.052494
wa,0.014396
weak,0.017439
welfar,0.020221
wherea,0.030468
word,0.012186
work,0.008606
workabl,0.033325
workplac,0.027554
written,0.027799
