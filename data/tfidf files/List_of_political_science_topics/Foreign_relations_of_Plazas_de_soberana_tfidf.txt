access,0.010804
acknowledg,0.015056
action,0.009594
activ,0.007791
administ,0.015306
africa,0.012680
agreement,0.011918
aid,0.011714
alcan,0.037236
alhucema,0.070373
alli,0.014422
altern,0.010033
america,0.054716
american,0.008847
andorra,0.027727
ani,0.006881
antiamerican,0.032254
april,0.011945
arab,0.057689
area,0.007755
argument,0.013118
articl,0.027840
asean,0.025556
asian,0.029751
aspect,0.020525
assert,0.013413
assist,0.011642
atlant,0.016546
author,0.008791
autonom,0.015789
awar,0.028255
aznar,0.033212
b,0.010342
badajoz,0.140747
balkan,0.022150
base,0.013607
basi,0.019452
basic,0.009472
basqu,0.026247
becaus,0.014175
begun,0.014970
best,0.010377
bilater,0.053723
black,0.012706
boost,0.018460
border,0.041608
breach,0.021304
break,0.012978
briefli,0.017464
broaden,0.020077
broker,0.020674
canari,0.025269
cancel,0.017862
cantalapiedra,0.039773
captur,0.012947
case,0.007884
caus,0.008416
cede,0.020181
central,0.017679
ceuta,0.084963
chafarina,0.067560
chang,0.014218
chavez,0.031649
china,0.024230
christian,0.012220
claim,0.105297
close,0.008438
closer,0.015604
coast,0.014392
coastal,0.017191
colombia,0.021501
coloni,0.026080
commerc,0.015715
commun,0.024740
concept,0.017603
concern,0.008854
conciliatori,0.030950
consensu,0.015732
consider,0.021198
contact,0.026469
contemporari,0.012336
contest,0.016594
context,0.011094
continu,0.015284
contrast,0.010654
controversi,0.012428
cooper,0.036164
coordin,0.013370
cortada,0.070373
countri,0.099601
cuba,0.040154
cultur,0.037933
david,0.011203
deal,0.010160
death,0.011046
debat,0.011742
decolonis,0.026927
defin,0.008655
demand,0.011260
demarc,0.044070
democraci,0.028098
derek,0.025178
despit,0.021389
dictatorship,0.020381
diplomaci,0.084910
diplomat,0.059938
directli,0.010134
disagr,0.017935
disput,0.088805
doi,0.016697
dominican,0.022692
dure,0.014520
eas,0.017481
east,0.035024
eastern,0.012603
econom,0.016619
ed,0.021260
effect,0.007376
effort,0.031548
elect,0.011199
elliott,0.050177
elsewher,0.015654
emphas,0.013326
enclav,0.023588
encyclopedia,0.013077
endeavour,0.021923
english,0.010575
enhanc,0.014064
enter,0.011101
equal,0.010340
equatori,0.022772
essay,0.029430
essenti,0.010555
establish,0.008103
eta,0.025706
ethnic,0.014984
eu,0.066133
europ,0.041185
european,0.056747
exampl,0.006800
excerpt,0.083949
exchang,0.010734
expand,0.021483
extremadura,0.033212
facto,0.018408
favor,0.012541
fellow,0.017168
follow,0.012990
foreign,0.063421
formal,0.020321
franc,0.011624
franco,0.069573
francospanish,0.033487
frequent,0.011414
friction,0.020004
g,0.010984
ga,0.013660
garcia,0.026304
gener,0.023338
geoffrey,0.021227
gibraltar,0.123120
gillespi,0.030344
gomera,0.062572
good,0.036109
govern,0.007721
gradual,0.013052
great,0.009151
ground,0.035446
group,0.007324
grow,0.010412
guinea,0.019172
h,0.022558
ha,0.097457
hand,0.010060
help,0.008889
hispan,0.025706
hispanoamericanismo,0.039773
histor,0.036484
histori,0.020289
hm,0.021562
howev,0.006589
hugo,0.020674
humanitarian,0.020429
iberian,0.022825
iberoamerican,0.033487
identif,0.015993
iglesiascavicchioli,0.039773
ii,0.010626
illeg,0.016361
immigr,0.016158
imperi,0.015817
import,0.013850
incid,0.016066
increas,0.007808
indonesia,0.019233
institut,0.008592
integr,0.019597
intern,0.028461
interpret,0.010832
introduct,0.010764
invad,0.016239
invest,0.023121
iraq,0.018170
isla,0.024705
islam,0.013516
island,0.048139
isol,0.013182
issu,0.034852
j,0.020394
jame,0.022903
japan,0.013427
jeremi,0.021421
join,0.012077
joint,0.014297
journal,0.020887
jure,0.072941
king,0.012932
kingdom,0.021600
known,0.013568
korea,0.016387
la,0.027102
langer,0.027807
languag,0.020407
larg,0.007154
later,0.024220
latin,0.051036
latinamerican,0.033487
lead,0.008099
left,0.010821
legaci,0.017069
legitim,0.016861
liber,0.012239
like,0.014501
linguist,0.015958
link,0.005902
list,0.015096
longstand,0.019615
madrid,0.022745
main,0.017186
maintain,0.047324
major,0.014402
malaysia,0.019833
mali,0.022935
manag,0.009721
mandatori,0.020863
mani,0.012270
manuel,0.021707
mauritania,0.024584
mckay,0.027648
meanwhil,0.016413
mechan,0.010023
melilla,0.087999
membership,0.014951
mexico,0.016934
middl,0.033574
mightiest,0.035186
minist,0.012247
mission,0.027068
moroccan,0.025269
morocco,0.107405
mostli,0.011581
mowat,0.033212
multilater,0.021541
municip,0.016540
narrow,0.015327
nation,0.022564
nato,0.020525
neighbour,0.016580
nineteenthcenturi,0.023885
notabl,0.020831
number,0.013580
occupi,0.013607
ocean,0.013967
oil,0.013547
olivena,0.037236
olivenza,0.038761
olivenzaolivena,0.397737
opinion,0.014426
organ,0.007403
outlin,0.013211
pacheco,0.030950
pardo,0.032476
parker,0.024130
parti,0.010489
particip,0.010707
particular,0.008496
partner,0.014838
past,0.010767
payn,0.028506
pdf,0.015709
peninsula,0.018056
peninsular,0.025556
peon,0.104376
perhap,0.012617
period,0.016130
philip,0.016683
philippin,0.019472
place,0.007676
plaza,0.050821
point,0.007873
polici,0.060268
polit,0.008519
popul,0.009657
portug,0.217587
portugues,0.120949
posit,0.016088
power,0.015930
pp,0.012689
prefer,0.011918
pressur,0.011716
prime,0.012505
principl,0.018686
prioriti,0.032827
program,0.029373
public,0.023516
pursuit,0.017548
question,0.019548
quit,0.013154
r,0.010415
rais,0.011480
ramon,0.027062
read,0.008573
reason,0.008859
recent,0.034191
recogn,0.010584
reconquest,0.027131
recoveri,0.016433
reestablish,0.018802
refer,0.009872
regim,0.014384
region,0.053038
relat,0.106491
relationship,0.009422
remain,0.008164
renew,0.014435
repres,0.008097
republ,0.023171
resid,0.013077
resolv,0.014338
return,0.029971
revok,0.071452
richard,0.012194
rise,0.019866
routledg,0.017590
russia,0.014507
savag,0.023279
scott,0.018268
search,0.050539
seat,0.014608
section,0.012231
secur,0.031463
seneg,0.023750
sensit,0.015899
set,0.007646
sever,0.021486
shown,0.011904
sign,0.011311
sinc,0.013739
soberana,0.061582
solut,0.011753
sought,0.026767
south,0.021517
southern,0.012750
sovereignti,0.115969
spain,0.554769
spanish,0.222856
spanishspeak,0.061274
spanishu,0.039773
special,0.008592
stand,0.012911
stanley,0.018094
state,0.012022
statu,0.011473
strait,0.021562
strateg,0.014994
subsaharan,0.021521
substanti,0.025506
success,0.009110
summerfal,0.036096
support,0.024504
suspend,0.017076
technic,0.012218
term,0.006491
territori,0.072148
text,0.059255
th,0.015755
thailand,0.020759
thi,0.010277
thu,0.024092
tie,0.026800
time,0.012156
today,0.019738
town,0.014107
trade,0.019876
transit,0.011960
treati,0.129121
tri,0.010813
trip,0.018983
turbul,0.021304
twentiethcenturi,0.023750
union,0.010208
unit,0.020787
velez,0.035618
venezuela,0.063234
venezuelanspanish,0.039773
veri,0.015691
victori,0.015910
vienna,0.058968
vietnam,0.018617
violenc,0.016361
vlez,0.032254
w,0.023054
wa,0.016898
war,0.019518
weaker,0.019563
western,0.010334
whilst,0.017746
whitehead,0.022640
wide,0.008848
wider,0.015643
william,0.011008
win,0.015447
world,0.034825
worsen,0.020509
year,0.020800
zapatero,0.034093
