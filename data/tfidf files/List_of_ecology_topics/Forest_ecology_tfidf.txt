abiot,0.062001
abov,0.012726
absenc,0.018274
abund,0.020158
accumul,0.038139
addit,0.010056
affect,0.038969
age,0.024897
agent,0.016948
ahead,0.023206
align,0.021474
alter,0.017830
anatomicalphysiolog,0.052294
andor,0.017395
anim,0.027827
approach,0.011440
area,0.049364
arid,0.028989
arrang,0.016205
base,0.008661
becaus,0.009022
behavior,0.014060
bibliographi,0.017432
biodivers,0.074514
biomass,0.090834
biota,0.031608
biotic,0.085980
bioticallyori,0.052294
boreal,0.038630
branch,0.050986
burton,0.031502
c,0.011471
calcul,0.015295
canopi,0.034906
capabl,0.015808
capac,0.033281
categori,0.015317
central,0.011253
chang,0.018101
characterist,0.027673
chemistri,0.017594
christopherson,0.047403
circumst,0.018441
classif,0.035427
clearcut,0.032349
climat,0.034283
close,0.021484
coars,0.031995
cold,0.019137
combin,0.011448
commun,0.041993
compact,0.023687
compar,0.011875
comparison,0.016835
compet,0.016553
complex,0.023497
compon,0.041481
concentr,0.015712
connot,0.026537
consid,0.018727
consider,0.013493
consist,0.010746
convers,0.016565
convert,0.016367
death,0.014062
debri,0.030764
decay,0.022077
deforest,0.029523
depriv,0.025788
design,0.011793
diminish,0.021739
distanc,0.017098
disturb,0.022547
divers,0.061771
dri,0.042060
duff,0.038280
ecolog,0.309683
ecosystem,0.175407
ed,0.013532
edit,0.015129
effect,0.009391
element,0.012294
enabl,0.015330
energi,0.040598
enhanc,0.017904
environ,0.051256
environment,0.062164
eros,0.026068
especi,0.022450
ethic,0.017791
europ,0.013107
evapor,0.027072
everi,0.012096
exacerb,0.026763
exampl,0.025972
excelsior,0.048292
exist,0.009612
factor,0.049027
fagu,0.049345
fauna,0.024658
flood,0.022820
floor,0.023513
flora,0.026130
flux,0.025429
focal,0.028989
forest,0.695765
forestri,0.080570
form,0.015802
foundat,0.013479
fraxinu,0.049345
freez,0.026786
frugal,0.034906
function,0.010583
gallopavo,0.052294
geograph,0.016811
geographi,0.019027
geosystem,0.044291
given,0.010367
global,0.013727
globaltwitchercom,0.052294
graze,0.029024
great,0.023300
greatli,0.016927
groundwat,0.032534
group,0.009324
grow,0.026509
growth,0.026168
ha,0.013785
habitat,0.024108
hall,0.039224
hansjrgen,0.043003
height,0.041219
heterogen,0.051974
high,0.041867
highli,0.054431
hogan,0.036662
howev,0.016778
human,0.009958
humid,0.027527
hydrolog,0.082193
import,0.052896
importantli,0.023687
individu,0.010662
induc,0.020468
infinit,0.020240
inhabit,0.017969
instanc,0.013999
instantan,0.027120
intact,0.026493
intens,0.017167
intermedi,0.020315
interrel,0.025231
intric,0.028757
introduct,0.013703
inventori,0.026880
involv,0.010020
jame,0.014578
joseph,0.017033
journal,0.013295
juglan,0.049345
kimmin,0.047403
kinet,0.023396
known,0.008636
landscap,0.021548
larg,0.045538
larger,0.013641
lastli,0.029486
late,0.011965
leaf,0.028023
level,0.051669
life,0.011108
lifeform,0.033128
lignin,0.037199
litter,0.066682
local,0.011704
logic,0.016725
long,0.011220
longev,0.030952
low,0.026487
lower,0.012768
major,0.009167
make,0.017574
manag,0.086631
mani,0.015620
materi,0.024218
mean,0.019033
measur,0.011318
meleagri,0.049345
meteorolog,0.025338
methodolog,0.018290
michael,0.015902
microenviron,0.039002
microorgan,0.025808
mountain,0.018590
n,0.015518
natur,0.027674
nj,0.025356
nonliv,0.029127
note,0.009962
number,0.034578
numer,0.012801
nutrient,0.049032
occupi,0.017323
occur,0.010978
oldgrowth,0.044794
oppos,0.014440
organ,0.037701
organiz,0.045274
otto,0.024346
overus,0.033198
p,0.013190
page,0.033880
paramet,0.039842
particular,0.010815
pattern,0.029486
perhap,0.016062
period,0.010267
philip,0.021239
physic,0.033920
physiognomi,0.037488
plan,0.013086
plant,0.075010
point,0.010023
popul,0.024588
possibl,0.010106
potenti,0.051314
prentic,0.053573
presenc,0.029968
primari,0.013079
process,0.028101
product,0.020654
proport,0.016382
qualiti,0.029526
quantifi,0.022265
quantiti,0.016139
radiat,0.019844
radic,0.018164
rainfallsnowfal,0.052294
rapid,0.017172
rate,0.024558
rd,0.017742
refer,0.012568
regener,0.083897
regia,0.036662
region,0.011253
regul,0.014877
rel,0.033802
relat,0.008473
relationship,0.011995
remain,0.010393
remov,0.014509
repositori,0.029598
repres,0.010308
requir,0.009892
research,0.010194
resist,0.065136
resourc,0.012722
result,0.008753
right,0.011276
risk,0.015462
river,0.017180
robert,0.013817
saddl,0.030363
sampl,0.018727
scientif,0.012645
score,0.046069
season,0.020307
shade,0.028628
share,0.012245
silvicultur,0.039198
sinc,0.008745
site,0.014731
size,0.027239
slower,0.025126
slowli,0.040317
small,0.010701
soil,0.059448
solar,0.020861
sometim,0.022770
space,0.013483
spatial,0.020445
speci,0.112593
specif,0.020928
speed,0.017884
stabil,0.016310
stand,0.016436
storag,0.020812
store,0.035437
storm,0.024500
strategi,0.016291
stromberg,0.049345
structur,0.030655
studi,0.101669
sustain,0.034213
sylvatica,0.046632
tall,0.027825
temperatur,0.035360
term,0.008264
terrestri,0.047620
themselv,0.012972
therefor,0.011887
thi,0.019624
thing,0.013916
thrive,0.024211
thu,0.051118
timber,0.052385
togeth,0.012060
toler,0.066387
tree,0.180153
treefal,0.052294
tri,0.013766
trunk,0.031347
turkey,0.045618
turn,0.012090
type,0.010514
underground,0.024241
uneven,0.029449
uniqu,0.014912
unit,0.008821
upper,0.017661
usa,0.019525
use,0.007146
valu,0.022418
vari,0.038678
variabl,0.015987
variat,0.016394
varieti,0.026263
variou,0.009956
veri,0.009987
vertic,0.022625
w,0.014674
water,0.067454
way,0.009351
wide,0.022528
wild,0.044874
wildlif,0.074764
wind,0.020779
windic,0.052294
wood,0.020181
woodi,0.066538
woodland,0.031826
