abbrevi,0.006895
abolish,0.005915
abolit,0.014872
abound,0.009265
abov,0.010873
abridg,0.009646
access,0.007834
accommod,0.006574
accomplish,0.005689
accord,0.005661
acr,0.007576
act,0.006471
activ,0.011299
ad,0.003612
adjac,0.006479
administ,0.005549
administr,0.015833
adolph,0.009265
adopt,0.003906
advic,0.006161
age,0.003545
agenc,0.004453
agr,0.011858
agricultur,0.004207
aircraft,0.006287
airlin,0.021618
airport,0.031180
ajo,0.012914
allow,0.002815
alo,0.021032
altern,0.003637
alway,0.003807
america,0.007935
amidst,0.008987
amnag,0.012482
ancestr,0.007401
anchor,0.007703
anchorag,0.009762
ancien,0.020344
ancient,0.004101
anemon,0.010763
angevin,0.011694
anglican,0.025386
anglophon,0.020223
anguilla,0.032563
ani,0.002495
annual,0.008697
anoth,0.002722
ans,0.203113
anthem,0.008227
antil,0.008788
antillean,0.011108
antiqu,0.005858
apart,0.005188
appear,0.003279
appoint,0.004653
approxim,0.003858
april,0.021655
aquafauna,0.027001
arawak,0.009762
architectur,0.016020
area,0.044989
arid,0.016512
arm,0.022570
armi,0.004869
armistic,0.018065
arriv,0.009114
art,0.004331
articl,0.003364
asco,0.013500
ash,0.022946
asian,0.005393
assumpt,0.004823
atlanta,0.009715
attack,0.009129
attain,0.005730
attract,0.018221
august,0.012906
author,0.003187
autonomi,0.005915
avail,0.010382
averag,0.021143
avifauna,0.027001
awar,0.005122
away,0.004312
background,0.004862
bacon,0.007589
bag,0.008788
baie,0.024722
ball,0.006991
banana,0.007717
bananaquit,0.013500
band,0.006155
bank,0.016376
barb,0.010635
barbari,0.009968
barracuda,0.024283
bart,0.050708
barth,0.100781
barthelemi,0.024283
barthlemi,0.471529
barthlemyrel,0.012757
barthlmi,0.013087
bartolomeo,0.010808
bastil,0.022327
bay,0.051146
beach,0.184887
beak,0.010111
beauti,0.006486
becam,0.012481
becom,0.005465
beef,0.008275
befor,0.005830
began,0.010226
begin,0.003328
bell,0.046264
belong,0.004823
belt,0.007441
better,0.003849
bibliographi,0.004964
bibliothqu,0.010554
bigger,0.007662
bight,0.011544
bird,0.005827
birthplac,0.009112
black,0.004607
blanc,0.010676
blanchard,0.009914
bless,0.008090
bloom,0.008462
blue,0.024339
board,0.004924
boat,0.054440
bonhomm,0.013281
bonito,0.012141
bonnet,0.011221
boom,0.006476
born,0.004822
botan,0.008335
bougainvillea,0.013500
bought,0.006522
brazil,0.006100
breton,0.010111
brief,0.005410
briefli,0.006332
british,0.019057
brittani,0.010854
broadcast,0.006623
broadwing,0.013500
brother,0.005724
brought,0.012623
brown,0.011584
bruyn,0.012614
bucket,0.009376
build,0.025286
built,0.038300
buoy,0.009715
burn,0.005851
burrow,0.009112
c,0.026135
cacao,0.010235
cacti,0.022003
cactu,0.010301
calm,0.008030
calypso,0.012361
candl,0.009017
capabl,0.004502
capit,0.007436
capita,0.012410
car,0.006041
cardiolog,0.010081
carib,0.027691
caribbean,0.071750
carnag,0.011544
carniv,0.020344
carrier,0.006866
categor,0.005811
cater,0.008387
cathol,0.011040
catholiqu,0.023895
cay,0.010405
ceas,0.005775
celebr,0.005892
censu,0.013408
centenair,0.013500
center,0.003646
centimetr,0.019477
central,0.003204
centuri,0.017615
cereu,0.013281
certain,0.006258
chang,0.005155
channel,0.016931
charter,0.006150
cheval,0.012141
chevreau,0.013500
chiefli,0.007412
children,0.009404
chines,0.004914
christma,0.016112
christoph,0.011983
chromi,0.027001
church,0.041326
circa,0.007682
citi,0.007897
citizen,0.018715
classic,0.003777
classifi,0.004624
clear,0.004255
clearli,0.005140
climat,0.009763
clock,0.007156
close,0.003059
club,0.013261
clube,0.013281
coast,0.015655
coastal,0.012466
coastlin,0.007112
coat,0.013546
coco,0.009516
coconut,0.016753
collect,0.026682
collectivit,0.021902
collector,0.008108
colombi,0.066405
colon,0.006066
coloni,0.018911
colour,0.006526
columbu,0.007817
com,0.038233
come,0.003270
comit,0.010554
commerc,0.005697
commerci,0.008761
commiss,0.004552
common,0.011059
commun,0.011959
compagni,0.009941
compani,0.010831
compar,0.003382
compos,0.004338
comprehens,0.005036
compris,0.014033
concarneau,0.013500
concern,0.003210
conch,0.021353
conclud,0.004676
conduct,0.003919
congest,0.009320
conic,0.008987
connect,0.007529
consid,0.005333
consider,0.007685
consist,0.009181
constitut,0.003610
contact,0.004798
contain,0.003280
continu,0.005541
contraband,0.021353
control,0.003038
conveni,0.006105
converg,0.006023
cook,0.019341
coral,0.030231
corossol,0.027001
correspond,0.004151
costum,0.018830
cotton,0.006991
council,0.020848
cove,0.040689
cover,0.015059
crab,0.026961
creat,0.002797
creol,0.035371
crest,0.018991
creux,0.013087
crole,0.025828
cross,0.004991
crown,0.011845
cucumb,0.010405
cuisin,0.077738
cul,0.038742
culdesac,0.048167
cultiv,0.005840
cultur,0.010315
cup,0.007675
curaao,0.009862
currenc,0.005011
current,0.002927
cut,0.004854
d,0.003701
daniel,0.005687
day,0.058663
death,0.008010
decemb,0.020982
declar,0.008833
deep,0.010181
defenc,0.011133
defens,0.005019
degre,0.003744
demograph,0.006139
dentist,0.010111
depart,0.008018
depend,0.003017
depot,0.009320
descend,0.005534
design,0.003358
destin,0.006399
destroy,0.014345
destruct,0.005412
determin,0.003175
devast,0.006492
develop,0.004574
diagnost,0.014645
dial,0.010111
diamet,0.014943
diet,0.013523
difficult,0.003988
difficulti,0.004938
direct,0.006073
discov,0.004027
discoveri,0.004282
dish,0.025979
dispens,0.007956
display,0.005197
dissolv,0.005679
distanc,0.004869
distinct,0.003521
distinctli,0.008356
divid,0.010878
dockyard,0.011001
doe,0.003094
dog,0.006845
dolphin,0.008870
domest,0.004738
domin,0.007526
dove,0.010111
dpartement,0.009996
dragnet,0.012141
dri,0.017968
drive,0.004913
du,0.051706
dure,0.026323
dutyfre,0.010235
earli,0.005507
earlier,0.004068
east,0.021164
eastern,0.018278
eastsoutheast,0.011947
eat,0.006424
econom,0.006025
economi,0.003759
eden,0.009283
edg,0.012013
eel,0.010024
effect,0.002674
egg,0.013319
egret,0.012361
elect,0.032485
electr,0.004552
eleg,0.008208
elev,0.006142
emphas,0.009663
employ,0.003651
encircl,0.018567
encompass,0.005154
encount,0.005422
encourag,0.004525
end,0.018060
endang,0.015297
english,0.011503
ent,0.011474
entir,0.007142
entitl,0.005683
entranc,0.007752
environment,0.008852
epithet,0.009320
equip,0.004922
eros,0.007424
especi,0.006393
essenti,0.007654
establish,0.005876
estim,0.003874
et,0.004732
eu,0.017983
eugni,0.013281
euro,0.021014
european,0.017145
eustatiu,0.021189
eve,0.016112
event,0.014331
everi,0.024115
exchang,0.019459
exclus,0.004703
execut,0.004170
expens,0.009834
experienc,0.004942
explain,0.003706
extend,0.003631
extermin,0.009017
extern,0.002152
f,0.032442
face,0.008264
facil,0.026386
facilit,0.004963
factbook,0.006899
famili,0.003812
famou,0.004548
far,0.011199
fashion,0.005794
fathom,0.010951
fauna,0.014045
favour,0.010827
favourit,0.018531
feast,0.017319
februari,0.008945
feet,0.040339
femal,0.005527
feroci,0.010478
ferri,0.008387
festiv,0.061536
fewer,0.005838
fig,0.009017
fillet,0.012914
film,0.011167
final,0.003560
finest,0.008710
firework,0.010370
fish,0.061639
fiveyear,0.006704
fix,0.004369
flag,0.006574
flamand,0.040501
flamboy,0.022562
flash,0.007731
flat,0.006296
fleursd,0.013500
flight,0.006031
flora,0.022325
fm,0.008898
focal,0.008256
follow,0.007064
food,0.008474
foot,0.012792
forc,0.008852
forchu,0.013500
form,0.018002
formal,0.003683
formerli,0.005514
fort,0.065323
forth,0.005984
fortun,0.006678
fourchu,0.013500
franc,0.054789
frangipani,0.013500
franoi,0.007279
free,0.010088
freed,0.007589
freedom,0.004647
french,0.150554
frequent,0.004138
fresh,0.013225
frgate,0.013500
frigatebird,0.011694
fring,0.007878
frond,0.011281
ft,0.019302
fte,0.012041
fulli,0.008876
galet,0.040501
garbag,0.009435
gastronom,0.013281
gastronomiqu,0.013500
gave,0.004283
gdp,0.016610
gendarm,0.011221
gendarmeri,0.010405
gener,0.014808
gent,0.011221
geograph,0.004787
geographi,0.005419
ghost,0.007997
gigantea,0.012141
given,0.008858
glise,0.026562
goat,0.007745
gold,0.010238
golden,0.005965
good,0.003273
gothenburg,0.011947
gourmet,0.011343
gouverneur,0.025228
govern,0.011198
governor,0.005403
grand,0.062902
grandculdesac,0.013500
grant,0.009026
grape,0.017797
grass,0.014837
greater,0.003708
green,0.020251
greener,0.011694
greenthroat,0.013500
grid,0.007453
grill,0.021526
gro,0.011407
gross,0.005756
group,0.007966
grow,0.003775
grown,0.005669
guadeloup,0.096237
guardhous,0.013087
guid,0.004162
gustaf,0.010902
gustav,0.022598
gustavia,0.255149
gustavialoppet,0.013500
gut,0.008440
ha,0.058890
habitat,0.013732
hall,0.011171
handicraft,0.017944
harbour,0.037853
hardship,0.008356
hat,0.008237
hatch,0.009064
haven,0.007453
hawk,0.007817
hawklik,0.013500
hawksbil,0.012361
head,0.007601
health,0.004104
hectar,0.008672
height,0.011739
held,0.055691
heraldri,0.011544
herbivor,0.008722
hermit,0.009395
heron,0.010235
hi,0.010558
hid,0.010335
high,0.014904
highend,0.010554
higher,0.003499
highest,0.004508
hill,0.040993
histoir,0.008842
histor,0.006613
histori,0.007356
hit,0.006095
hold,0.003546
holiday,0.034648
holland,0.007669
honour,0.020828
hooligan,0.012614
hopit,0.013500
horizont,0.006789
hospit,0.005805
host,0.010766
hotel,0.070881
hour,0.005173
hous,0.031593
howev,0.011946
hub,0.007642
hull,0.008266
humanitarian,0.007406
humid,0.007840
hummingbird,0.023388
hundr,0.008945
hurrican,0.023588
iii,0.011259
ile,0.011001
imag,0.004533
immedi,0.004327
impetu,0.007596
import,0.007532
inch,0.031730
includ,0.024523
incom,0.004509
increas,0.002830
index,0.004676
indi,0.014943
india,0.009014
indian,0.014619
indigen,0.017609
industri,0.003360
influenc,0.003242
inform,0.008780
inhabit,0.010235
inherit,0.005459
inland,0.007064
inse,0.012614
instal,0.005809
instead,0.003448
intact,0.007545
integr,0.003552
intellig,0.004593
inter,0.008275
interior,0.017331
intern,0.015478
invad,0.011775
invest,0.004191
irrig,0.007441
island,0.370888
islet,0.043550
italian,0.005523
item,0.010800
jacqu,0.006829
jan,0.006469
januari,0.016805
jean,0.030527
jellyfish,0.009888
jet,0.007201
job,0.005083
john,0.003348
journal,0.007572
juan,0.007247
juli,0.025513
juliana,0.011001
jurisdict,0.005974
just,0.003432
karl,0.005536
kichnott,0.013500
kilometr,0.061427
king,0.014066
kingdom,0.003915
kingfish,0.011947
kitesurf,0.025828
kitt,0.019430
known,0.022137
la,0.024566
laboratori,0.004995
labour,0.004983
lack,0.003753
lagoon,0.009320
laid,0.005555
lamriqu,0.011774
land,0.007450
landmark,0.006929
languag,0.014798
larg,0.005187
largest,0.008130
later,0.005854
law,0.002965
lay,0.011077
le,0.082081
lead,0.002936
leather,0.008418
leatherback,0.012141
led,0.006353
leeward,0.044423
left,0.003923
legallyown,0.013500
legend,0.013868
length,0.024415
lenni,0.011617
let,0.005514
level,0.008829
lglise,0.025228
librari,0.004623
lie,0.028933
lieutenantgovernor,0.012361
life,0.012655
light,0.004057
lighthous,0.019430
like,0.005257
limit,0.003005
line,0.010567
lineag,0.007284
linguistiqu,0.011474
link,0.002140
list,0.002736
lit,0.008199
littl,0.007533
live,0.028640
lobster,0.010081
local,0.010000
locat,0.018093
long,0.009586
look,0.004063
loot,0.016671
lorient,0.049931
lot,0.006061
loud,0.008856
loui,0.011107
low,0.007543
lowest,0.005853
lux,0.011474
luxuri,0.007384
m,0.003519
maarten,0.009455
magnific,0.009161
main,0.012462
maintain,0.006863
major,0.007832
maltes,0.010141
mamillaria,0.013500
mammal,0.006230
manag,0.003524
manchineel,0.013500
mangrov,0.009646
mani,0.031141
manner,0.004749
map,0.004502
marathon,0.010141
march,0.012243
marigot,0.024965
marin,0.067606
marlin,0.011163
marseillais,0.013087
marshi,0.012041
martin,0.036464
martiniqu,0.009558
materi,0.003448
mean,0.005420
measur,0.003223
meat,0.006829
media,0.004514
medicin,0.009384
mediterranean,0.006437
member,0.009419
men,0.009126
metr,0.063002
metropolitan,0.007477
mexican,0.007840
mi,0.043436
mid,0.004900
migrat,0.010516
mile,0.005720
militari,0.003981
milk,0.007268
millimetr,0.009455
million,0.011596
minist,0.004440
mmoir,0.020810
model,0.003065
modern,0.002951
montbar,0.027001
month,0.008696
monument,0.014091
moor,0.006641
morn,0.007151
mostli,0.012596
mountain,0.005294
mringu,0.013500
municip,0.005996
munit,0.009017
muse,0.027288
museum,0.023737
music,0.021634
napoleon,0.006711
narrow,0.016671
nation,0.008180
nativ,0.010173
natur,0.005254
naturel,0.010301
navig,0.006145
nd,0.004578
near,0.016185
nearer,0.009195
nearest,0.007932
nearli,0.008572
necessit,0.007924
neighbor,0.005551
neighbour,0.006011
network,0.008228
neutral,0.005386
new,0.015387
night,0.012072
nineteen,0.008829
nivosa,0.013500
nobl,0.006399
nonextract,0.013500
noodl,0.010808
norman,0.006934
north,0.035167
northeast,0.007009
northeastern,0.007602
northwest,0.006730
notabl,0.018881
note,0.014186
notion,0.004656
novemb,0.022069
nudist,0.012482
number,0.007385
nun,0.009320
obgyn,0.011694
observ,0.009744
occas,0.018363
occup,0.010255
occupi,0.004933
oct,0.027386
octob,0.008511
offic,0.003961
offici,0.011208
offshoot,0.008748
offshor,0.014351
old,0.004071
older,0.005164
oldest,0.015971
onli,0.018122
open,0.003369
oper,0.012418
opportun,0.004648
order,0.018983
ordin,0.007258
orient,0.005127
origin,0.005454
oscar,0.008575
ouanalao,0.027001
outermost,0.008599
outlin,0.004790
oven,0.009475
overlook,0.007602
oversea,0.052827
ownership,0.005779
pacif,0.005704
paediatr,0.010951
pain,0.006233
paint,0.006250
palm,0.038345
parad,0.009320
paradis,0.008815
parallel,0.010198
pari,0.005381
parish,0.008246
park,0.005751
parliament,0.004963
parliamentarian,0.009248
paroiss,0.013500
parti,0.003803
particip,0.003882
particular,0.003080
particularli,0.003451
pass,0.003823
passport,0.008108
past,0.003904
patch,0.016078
patient,0.005994
patoi,0.024722
patron,0.007532
pattern,0.004198
peak,0.005400
pear,0.010235
pearlypink,0.013500
pebbl,0.009762
pelican,0.011343
pen,0.007885
peninsula,0.006546
peopl,0.024419
perform,0.003459
period,0.014620
perman,0.004769
perpendicular,0.008199
person,0.003220
petit,0.029117
pharmaci,0.008815
photograph,0.006773
pineappl,0.010203
pirat,0.015680
pizza,0.010902
place,0.011132
plane,0.005927
plant,0.017090
plantat,0.007059
play,0.003553
poinci,0.025828
point,0.005709
poitevin,0.013087
polic,0.005473
policemen,0.009811
polit,0.003088
pond,0.008697
poor,0.004865
popul,0.024510
popular,0.035208
porpois,0.011054
port,0.033365
portion,0.004945
portug,0.006574
posh,0.013087
possess,0.004422
possibl,0.002878
post,0.009552
potato,0.007495
practic,0.003019
preciou,0.007406
prefect,0.009495
prefer,0.004321
presbyter,0.013500
presbytr,0.027001
present,0.011286
presid,0.012243
prickli,0.012041
prime,0.004534
princess,0.008256
privat,0.012139
produc,0.002936
product,0.002941
progress,0.003846
prohibit,0.005758
promin,0.004425
promot,0.004025
pronounc,0.006817
pronunci,0.007483
prosper,0.023335
protect,0.048895
provid,0.007511
public,0.011368
publish,0.006264
puerto,0.016234
purpos,0.007341
quartier,0.011947
quebec,0.008285
quiet,0.008266
quit,0.004769
race,0.027720
racial,0.006929
radio,0.005777
rainfal,0.015533
raini,0.028073
rampart,0.011774
rate,0.013988
rd,0.005053
reach,0.003637
read,0.003108
receiv,0.006838
recent,0.006198
record,0.007544
recov,0.005509
red,0.019843
reef,0.078551
referendum,0.018640
reflect,0.003846
refurbish,0.010370
regatta,0.038272
regga,0.011054
regim,0.005215
region,0.016024
regularli,0.006092
relat,0.009652
religi,0.004571
remain,0.002959
remembr,0.010052
remot,0.006059
renaiss,0.005918
renam,0.006213
rent,0.006891
repeat,0.005165
replac,0.007294
repres,0.011743
reproduct,0.006079
republiqu,0.012247
reserv,0.042839
resid,0.023707
respect,0.003403
respons,0.003104
rest,0.004193
restaur,0.039743
restrict,0.008296
result,0.004986
retail,0.006473
retain,0.004706
retreat,0.006912
reveal,0.004707
rgion,0.010951
rheumatolog,0.011054
rich,0.015396
rico,0.016902
right,0.006423
ring,0.006061
rise,0.003601
road,0.036390
rock,0.022418
rocki,0.017104
roll,0.006678
room,0.017416
roqu,0.011544
roughli,0.005020
round,0.011339
royal,0.004722
rue,0.032289
rugbi,0.009738
ruin,0.028467
rule,0.006533
run,0.003933
rung,0.009968
runway,0.009357
s,0.010153
saba,0.009811
sabal,0.013500
sac,0.028246
safe,0.005851
safeti,0.005768
sailor,0.007655
saint,0.161585
saintbarthelemi,0.012757
saintbarthlemi,0.095582
saintbarthlemois,0.013500
saintbartholomew,0.013500
saintjean,0.012914
saintongeai,0.013500
salad,0.021189
salin,0.034541
salt,0.012263
san,0.005683
sand,0.029559
sandi,0.026365
sandwich,0.008913
satellit,0.006244
savaku,0.027001
saw,0.004379
sbatelemi,0.013500
scatter,0.006609
scenic,0.012041
schoelcher,0.024722
school,0.007282
scientif,0.003601
scuba,0.009738
sea,0.073064
seafar,0.009691
seagrass,0.011407
season,0.034702
secess,0.017319
second,0.002937
sector,0.004692
secur,0.003802
seen,0.010535
senat,0.018419
separ,0.022888
septemb,0.008632
sergeant,0.009762
serv,0.014250
servic,0.003487
settl,0.005086
settlement,0.010445
settler,0.026938
seven,0.004696
sever,0.007790
sewerag,0.011858
shade,0.008153
shallow,0.022711
shape,0.004265
sheep,0.007211
shell,0.048191
shelter,0.006977
shield,0.007018
ship,0.005078
shore,0.034025
short,0.007632
shrimp,0.009265
shrub,0.018160
sight,0.013283
signific,0.006359
sinc,0.009962
singl,0.006666
sint,0.009715
situat,0.003835
skin,0.006317
slave,0.023542
slaveri,0.026254
slope,0.014382
small,0.024383
smart,0.007825
snack,0.022327
snail,0.009601
snorkel,0.010902
snowi,0.010951
soca,0.012614
societi,0.003120
soil,0.005643
sophist,0.005730
sought,0.004852
soul,0.006431
sound,0.005244
south,0.003900
southeast,0.024390
southwestern,0.007773
spanish,0.010773
speci,0.032066
specialist,0.006180
spend,0.005170
spent,0.005505
spicier,0.013500
spit,0.010405
spoken,0.006700
sporad,0.007948
sport,0.035494
spread,0.004402
spring,0.005618
sq,0.007436
squar,0.010554
st,0.244933
stanc,0.006866
standard,0.006709
stapelia,0.012757
star,0.016571
starch,0.008987
state,0.008717
station,0.016324
statu,0.020800
stbarth,0.013500
stbarthlemi,0.026562
steam,0.007418
stone,0.016688
strewn,0.012041
stripe,0.009080
strong,0.007238
structur,0.008730
subject,0.003168
subsequ,0.003887
succul,0.011858
sucr,0.011001
suffer,0.004546
suffici,0.004604
sugarloaf,0.013087
suitabl,0.016588
summer,0.005762
sunshin,0.009691
superfici,0.007855
supplement,0.012333
support,0.005923
surf,0.009537
surfer,0.011108
surgeon,0.007766
surround,0.009122
survey,0.004787
surviv,0.004295
swamp,0.009096
swede,0.062432
sweden,0.018665
swedish,0.054345
sweet,0.007759
swim,0.008090
symbol,0.004522
syndar,0.013500
takeov,0.007710
tall,0.007924
tang,0.008047
tano,0.011281
team,0.010403
telephon,0.006809
temperatur,0.025177
temporari,0.005874
tend,0.004090
tenni,0.036449
term,0.004707
terrain,0.022273
territori,0.047957
territorial,0.011108
testifi,0.008495
th,0.014280
therefor,0.003385
thi,0.020493
thirteen,0.007471
thoma,0.004455
thousand,0.004392
till,0.007520
time,0.006611
toc,0.011694
today,0.003578
toini,0.040501
tongu,0.016651
took,0.011673
tool,0.003988
topographi,0.008126
tortu,0.026174
total,0.003475
tour,0.007186
tourism,0.052150
tourist,0.039827
tournament,0.009213
tower,0.036423
town,0.066495
trade,0.025222
tradit,0.006273
transat,0.013500
transfer,0.004262
transport,0.004270
travel,0.004398
tree,0.030784
tremend,0.007237
tropic,0.006356
trumpet,0.032158
tuna,0.008987
tunnel,0.007520
turn,0.003443
turtl,0.068986
tv,0.006887
twostorey,0.012757
twoyear,0.008099
type,0.002994
typic,0.003341
understood,0.004674
undertaken,0.006418
underw,0.007226
unicamer,0.007596
union,0.007402
unlik,0.004148
unoffici,0.007642
urchin,0.009715
use,0.002035
ushap,0.021438
usual,0.002997
valley,0.005760
vari,0.003671
variat,0.004669
varieti,0.011219
vaval,0.013500
veget,0.017838
ver,0.011281
vera,0.009537
veri,0.019911
version,0.004357
vessel,0.006064
vicin,0.008039
victor,0.006704
view,0.003192
vill,0.010951
villa,0.026017
virgin,0.007628
visibl,0.011011
visit,0.005004
visitor,0.013626
vitet,0.012757
voil,0.013281
volcan,0.007146
vote,0.014170
vulner,0.006335
wa,0.079650
wahoo,0.012041
wall,0.015822
war,0.007076
water,0.046106
waterfront,0.010676
wave,0.005023
wealth,0.005129
wealthi,0.006499
weather,0.006108
weav,0.008408
websit,0.009419
wednesday,0.029145
week,0.010470
weekli,0.014779
weigh,0.014036
west,0.034215
western,0.007493
wetter,0.010268
whale,0.007759
wherea,0.004322
white,0.033103
wide,0.003208
wikivoyag,0.009646
wild,0.012780
wildlif,0.007097
wind,0.005918
windi,0.011001
windsurf,0.011617
windward,0.028016
wingspan,0.012614
winter,0.018516
wire,0.007196
wireless,0.008073
wit,0.006066
women,0.009480
woodburn,0.012482
work,0.002441
world,0.005050
xvi,0.008801
yacht,0.028486
year,0.055303
yellow,0.020987
zenaida,0.013500
zone,0.020593
zouk,0.012041
