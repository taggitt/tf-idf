abil,0.015311
abl,0.014056
account,0.026213
agricultur,0.066162
approxim,0.015165
area,0.033158
associ,0.010871
attract,0.035812
austria,0.128734
austrian,0.026656
bank,0.080466
barley,0.033992
bearer,0.036215
becaus,0.010101
benefit,0.016283
besid,0.022958
billion,0.095298
border,0.138366
bridg,0.022169
budget,0.021003
busi,0.030253
calendar,0.027516
capit,0.014616
capita,0.024391
capitalist,0.026548
centr,0.018479
ceram,0.031601
charter,0.072530
chf,0.046615
commod,0.043630
commut,0.059719
compani,0.056771
connect,0.014799
consum,0.017898
consumptionabout,0.058543
contribut,0.013193
control,0.011942
corn,0.028109
corpor,0.052907
countri,0.106462
cultur,0.013515
currenc,0.059093
current,0.011507
custom,0.054527
dairi,0.033093
day,0.013564
decemb,0.032991
dental,0.034339
develop,0.026969
dollar,0.021003
doubl,0.019901
drive,0.019313
easi,0.021978
econom,0.059216
economi,0.029559
eea,0.040109
effici,0.016945
efta,0.083937
electr,0.089471
electron,0.018109
employe,0.022676
energi,0.045450
entiti,0.073448
especi,0.012566
est,0.030066
establish,0.011549
eu,0.094251
europ,0.014674
european,0.053916
exchang,0.015298
expenditur,0.047856
export,0.099263
extern,0.008462
factor,0.013721
fiduciari,0.037829
financ,0.017148
financi,0.061797
fiscal,0.024535
food,0.016655
foodstuff,0.033394
forc,0.034796
foreign,0.060257
foundat,0.015090
franc,0.099400
france,0.058543
free,0.013218
fuel,0.021445
fund,0.031911
gdp,0.065292
germani,0.087122
gone,0.025679
good,0.025731
govern,0.022008
growth,0.029295
guard,0.048375
ha,0.038581
hardwar,0.027072
harmon,0.028129
healthcar,0.027256
high,0.011717
highli,0.030468
histori,0.009638
hold,0.013938
hydro,0.035326
illicit,0.034924
import,0.069088
includ,0.016066
incorpor,0.031782
industri,0.079251
inflat,0.021793
infrastructur,0.019592
institut,0.012245
instrument,0.017818
insur,0.022309
integr,0.013964
intermediari,0.029370
intern,0.020281
invest,0.032952
issu,0.024835
itali,0.041650
japan,0.038273
join,0.017212
kingdom,0.015392
known,0.009668
knowyourcustom,0.056684
labor,0.037995
laidout,0.058543
late,0.013395
launder,0.036147
law,0.023310
lawyer,0.028071
legisl,0.017713
licens,0.023037
liechtenstein,0.602515
like,0.010333
link,0.008412
list,0.010757
livestock,0.027307
loos,0.024051
low,0.029653
machineri,0.047715
mainli,0.016060
maintain,0.013489
make,0.009837
manag,0.013854
manufactur,0.018448
market,0.028961
materi,0.013556
maximum,0.021036
member,0.037028
metal,0.040268
million,0.151946
modern,0.011603
money,0.017288
motor,0.024949
mutual,0.020405
mwh,0.308666
na,0.058506
nation,0.021438
nativ,0.019994
nearbi,0.023807
neighbor,0.021822
new,0.008640
nomine,0.036493
nonbank,0.039865
nonliechtenstein,0.058543
nuclear,0.019766
number,0.009677
occup,0.020157
offic,0.015571
onethird,0.029393
onli,0.008904
open,0.013243
order,0.010660
organ,0.010551
outsid,0.014757
oversight,0.030643
overtaken,0.037657
pariti,0.027843
particip,0.015260
partli,0.020800
partner,0.042295
period,0.011494
permit,0.039405
pharmaceut,0.026353
place,0.010940
polici,0.014315
posit,0.011464
potato,0.029465
potteri,0.030417
power,0.011351
ppp,0.063746
practic,0.011868
precis,0.018272
price,0.016421
primarili,0.030326
princip,0.069314
privat,0.015905
produc,0.011541
product,0.092491
proport,0.018340
public,0.011171
purchas,0.018866
purpos,0.014429
rate,0.096228
raw,0.022949
rd,0.019863
real,0.014601
recent,0.012182
refer,0.007035
regul,0.016655
regulatori,0.024348
reinsur,0.040901
rel,0.012614
repres,0.011540
requir,0.044300
research,0.011413
resid,0.018638
rest,0.016482
revenu,0.041048
rose,0.021348
rule,0.025684
safe,0.023002
second,0.011546
secreci,0.034493
sector,0.036888
secur,0.029893
serv,0.028008
servic,0.054828
sfr,0.233078
share,0.013709
short,0.015000
sign,0.016120
significantli,0.018056
sinc,0.019581
small,0.011980
solarwind,0.055242
sourcefossil,0.042308
special,0.012245
specialti,0.027806
spend,0.020323
stamp,0.028958
state,0.042834
station,0.021389
statist,0.016063
strengthen,0.022145
strict,0.045503
strong,0.014226
success,0.012984
successori,0.051444
swiss,0.195562
switzerland,0.200755
taiwan,0.055104
tax,0.073603
textil,0.052945
therefor,0.013308
thi,0.007323
total,0.027322
tourism,0.022777
trade,0.028327
tradit,0.012329
transfer,0.016754
treati,0.020446
trust,0.021403
trustworthi,0.037657
uk,0.017946
unemploy,0.022932
union,0.029098
unit,0.019751
use,0.016000
vaduz,0.048143
vehicl,0.022231
vulner,0.024902
western,0.014727
wheat,0.027843
work,0.028795
world,0.029779
worth,0.022169
year,0.049406
