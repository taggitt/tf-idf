absent,0.033503
accept,0.017258
access,0.076082
accord,0.013746
achiev,0.034587
achira,0.072318
acquir,0.022390
administ,0.026948
agricultur,0.122595
alpaca,0.056058
altiplano,0.056413
amazon,0.039330
andean,0.267419
ani,0.012115
anim,0.057724
anthropologist,0.035269
appar,0.023467
aqueduct,0.046949
arabl,0.042050
archaeolog,0.029433
archaeologist,0.037888
archipelago,0.069786
area,0.054613
arid,0.080181
array,0.029997
asid,0.031352
autarki,0.050880
axemoni,0.068241
ayllu,0.072318
aymara,0.054208
ayni,0.068241
barter,0.080088
base,0.011978
basic,0.033351
basin,0.032664
bed,0.035800
belong,0.023421
bird,0.028295
bolivian,0.050021
border,0.024417
brightli,0.051641
build,0.017540
bureaucraci,0.036917
camelid,0.062707
case,0.027761
certain,0.015194
church,0.025084
civil,0.019044
class,0.018413
coast,0.050677
coastal,0.090800
coca,0.053177
coin,0.022990
cold,0.026465
collect,0.032391
coloni,0.045915
color,0.027657
commun,0.014518
complementar,0.046310
compos,0.042130
concept,0.030990
condit,0.015536
construct,0.017194
contrast,0.018756
corv,0.106354
creat,0.013581
creation,0.020211
crop,0.137262
cultur,0.033391
currenc,0.024332
descend,0.026876
describ,0.013667
despit,0.018828
destroy,0.023220
determin,0.015420
develop,0.011105
did,0.032966
differ,0.035314
distribut,0.018321
domest,0.046014
drawn,0.026205
drew,0.030602
eastern,0.022188
ecolog,0.025192
econom,0.029259
economist,0.025098
ecoregion,0.039590
ecozon,0.246974
effici,0.020932
especi,0.031046
essenti,0.018583
ethnohistor,0.061946
evid,0.018229
exchang,0.018897
exist,0.013293
exploit,0.049771
fall,0.018247
farmer,0.056591
feather,0.041698
fine,0.028579
fisherman,0.050525
food,0.041147
forc,0.014328
forest,0.027491
form,0.010926
frost,0.043422
ft,0.062486
fundament,0.018305
gener,0.010271
given,0.014337
good,0.031786
grain,0.031059
grassland,0.041584
great,0.048333
ground,0.020801
group,0.012894
grow,0.036661
grown,0.027530
guanaco,0.117911
ha,0.019063
head,0.018454
high,0.014474
higher,0.016994
highland,0.077667
humid,0.114207
import,0.036576
inca,0.176054
incan,0.053177
inde,0.024306
individu,0.014744
influenc,0.015744
inherit,0.026508
innumer,0.045911
institut,0.030253
intens,0.023741
interethn,0.052938
intern,0.012526
intralineag,0.072318
irrig,0.036136
john,0.016259
just,0.016668
kaniwa,0.072318
karl,0.053770
kind,0.038081
kiwicha,0.072318
known,0.071661
labor,0.140807
lack,0.018223
land,0.054264
languag,0.017963
larg,0.037786
level,0.014290
like,0.012765
lineag,0.212236
lineagebas,0.072318
littl,0.018290
llama,0.052263
local,0.016186
long,0.015516
low,0.018315
m,0.034182
macaw,0.060613
maiz,0.039330
marketbas,0.043861
mashua,0.072318
mean,0.013160
meant,0.024510
meat,0.033161
mediat,0.029274
member,0.015246
mesoamerica,0.047288
mesoamerican,0.050525
microclim,0.105414
migrat,0.025533
militari,0.019331
millennia,0.034820
mindal,0.072318
minga,0.144637
miniatur,0.042050
mitmaqkuna,0.072318
model,0.029770
moietal,0.072318
money,0.021356
montaa,0.175415
moray,0.056785
mountain,0.051419
muchpriz,0.072318
murra,0.072318
nativ,0.049398
natur,0.012757
need,0.014365
newli,0.025141
nobil,0.037271
northern,0.022339
northwest,0.032683
oblig,0.052778
oca,0.058955
onli,0.022000
oper,0.015075
order,0.013168
organ,0.013034
pack,0.034868
panaqa,0.072318
parallel,0.024761
particularli,0.016758
pastur,0.044820
peopl,0.013175
perform,0.016799
peru,0.037712
plant,0.020746
pochteca,0.068241
polanyi,0.098198
popul,0.017002
possibl,0.013976
potato,0.036397
precoloni,0.043142
present,0.013701
prevent,0.019590
principl,0.016449
produc,0.028515
product,0.014281
project,0.016955
proper,0.024584
provid,0.012157
public,0.013800
puna,0.188123
qollqa,0.072318
quechua,0.212708
quinoa,0.060021
rais,0.020211
rang,0.032898
reciproc,0.032646
record,0.018318
refer,0.017380
rel,0.046745
requir,0.013680
resourc,0.087968
rest,0.020360
retain,0.022854
rise,0.017487
road,0.025243
sapa,0.118942
scholar,0.022070
season,0.028084
secur,0.018463
selfsuffici,0.036517
sent,0.024088
share,0.033869
simpl,0.019217
slope,0.034917
societi,0.045460
sociologist,0.034326
sought,0.023562
speak,0.023191
speci,0.022243
special,0.015126
specif,0.014470
split,0.024857
state,0.042330
storag,0.028781
strateg,0.026397
strong,0.017574
structur,0.014131
stuff,0.043422
substantiv,0.064489
suggest,0.050796
suitabl,0.053698
suni,0.047174
tampu,0.072318
tax,0.045460
techniqu,0.018586
temperatur,0.048901
term,0.011428
terrac,0.093678
test,0.019772
therefor,0.016439
thi,0.027139
tobacco,0.034079
trade,0.069984
trader,0.059549
tradit,0.015230
transact,0.026042
transhum,0.060613
truequ,0.072318
tuber,0.103686
ulluco,0.065555
undomest,0.065555
use,0.079061
valley,0.055940
variat,0.022671
varieti,0.054480
vertic,0.093869
victor,0.032554
vicua,0.116943
wa,0.039667
warm,0.030909
waru,0.136482
waterfal,0.040828
western,0.036386
wide,0.015577
wild,0.093087
wool,0.079180
work,0.011857
yanakuna,0.072318
young,0.022457
zone,0.099994
