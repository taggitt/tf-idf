abil,0.017803
account,0.045720
accur,0.022046
activ,0.025823
adag,0.051025
adopt,0.017856
aggreg,0.191552
american,0.014660
amplif,0.040168
anoth,0.012445
answer,0.023039
anyth,0.025573
approach,0.074459
argu,0.017213
argument,0.021740
articl,0.015378
associ,0.037921
attempt,0.014759
background,0.022223
base,0.011274
becaus,0.011745
behav,0.027752
behavior,0.036604
behaviour,0.024055
believ,0.016137
benefit,0.018933
berkeley,0.030472
best,0.017196
bound,0.023346
boundedli,0.062864
brown,0.026474
budget,0.024422
busi,0.105532
california,0.023935
carnegi,0.037737
caus,0.013947
center,0.016667
challeng,0.018667
chang,0.047125
chicago,0.076406
citizen,0.021385
civil,0.017926
coast,0.023851
columbia,0.029066
compet,0.021548
compris,0.021380
concentr,0.020453
connect,0.017208
consensu,0.052142
consist,0.041966
constraint,0.025950
contrast,0.035311
counteract,0.040168
cultur,0.015715
cycl,0.128556
debt,0.024236
decis,0.035244
defend,0.023385
deficit,0.029010
degre,0.017116
demand,0.018660
dichotomi,0.037605
differ,0.055401
disagr,0.029721
discretionari,0.212999
discretionarili,0.068072
doe,0.014142
dynam,0.019595
e,0.016936
earli,0.012585
earlier,0.018594
east,0.019346
econom,0.220334
economi,0.068740
economicsth,0.065911
economist,0.212623
economistcom,0.060702
effect,0.048898
effici,0.019703
efficientmarket,0.046216
element,0.016003
els,0.025913
emerg,0.016127
emphas,0.022084
employ,0.016691
equival,0.020269
establish,0.013429
exampl,0.011269
expect,0.051867
extent,0.020705
extern,0.009839
faculti,0.029066
failur,0.087576
finetun,0.045562
firm,0.022474
fiscal,0.028528
fluctuat,0.056199
follow,0.010763
frame,0.025714
fresh,0.030224
freshwat,0.426772
funer,0.073645
gener,0.067682
govern,0.089569
great,0.015165
greater,0.016948
greg,0.040674
group,0.036412
ha,0.035889
hall,0.025529
hard,0.022084
harvard,0.026289
hi,0.012064
hold,0.032415
howev,0.021840
hypothesi,0.023437
ident,0.019266
identifi,0.032531
implic,0.046889
import,0.068857
includ,0.009340
individu,0.041637
inflat,0.025340
insist,0.027454
instead,0.015760
institut,0.028477
insur,0.025941
interact,0.050365
intern,0.035373
irrat,0.034660
key,0.017122
keynesian,0.073881
lake,0.026534
larg,0.011855
lead,0.013422
life,0.014460
like,0.024031
link,0.009781
locat,0.033079
longer,0.037105
longterm,0.023672
loos,0.027966
macroeconom,0.243113
macroeconomist,0.099225
main,0.014240
make,0.022877
mani,0.010167
mankiw,0.049612
market,0.134701
mean,0.012387
mechan,0.016611
mellon,0.041357
merg,0.027091
methodolog,0.071428
minneapoli,0.041876
minnesota,0.036257
mitig,0.032332
model,0.056045
near,0.036989
nearer,0.042030
neoclassicalkeynesian,0.068072
new,0.030142
nomin,0.046955
north,0.017859
note,0.012968
old,0.018611
older,0.023604
order,0.012395
otherworldli,0.053101
overview,0.022538
particular,0.014079
pennsylvania,0.033698
peopl,0.012401
phenomena,0.021476
philosoph,0.019743
pittsburgh,0.041575
play,0.016240
polici,0.099874
possibl,0.013155
potenti,0.016699
preced,0.024437
prevail,0.027287
primarili,0.017631
princeton,0.028476
problem,0.013794
program,0.016225
progress,0.035159
propag,0.028739
protagonist,0.038984
provid,0.022888
psychologist,0.029556
public,0.051959
question,0.016197
rate,0.031968
ration,0.088915
real,0.016978
redistribut,0.034014
refer,0.016360
reform,0.020009
regul,0.019365
reject,0.020166
rel,0.014667
replac,0.016669
repres,0.013418
requir,0.025755
research,0.079625
respect,0.015556
respond,0.022062
retir,0.111339
ricardian,0.047893
robert,0.035972
rochest,0.082715
role,0.014330
rule,0.014932
s,0.011601
salt,0.028026
saltwat,0.379180
saltwaterfreshwat,0.068072
say,0.017723
school,0.149787
scienc,0.027516
seemingli,0.032099
shock,0.027966
shortterm,0.059473
situat,0.017528
socal,0.045943
sometim,0.014820
spend,0.094526
stabil,0.063695
state,0.019922
strength,0.023090
strive,0.065280
structur,0.039904
sweetwat,0.065911
synthesi,0.050834
target,0.021197
tax,0.021395
tend,0.018695
term,0.010757
theori,0.066722
therefor,0.015474
thi,0.008515
thought,0.016134
time,0.010072
today,0.016355
true,0.018433
typic,0.015273
unavoid,0.040940
uncertainti,0.026494
understand,0.030907
unit,0.011482
univers,0.153286
usbas,0.043034
use,0.009302
utmost,0.043124
view,0.014591
vivid,0.041724
wa,0.046673
waldmann,0.062864
water,0.035123
way,0.036518
welfar,0.026222
west,0.019548
write,0.017800
yale,0.031121
younger,0.028285
