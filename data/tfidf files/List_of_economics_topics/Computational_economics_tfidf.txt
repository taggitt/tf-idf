accord,0.017682
ace,0.115750
adapt,0.084475
agent,0.241198
agentbas,0.305656
agglomer,0.061285
analyt,0.032238
andor,0.030944
appli,0.018903
appropri,0.028345
area,0.035125
articl,0.021016
associ,0.034548
assumpt,0.030127
autom,0.039570
bamberg,0.070463
befor,0.018210
biolog,0.025288
bound,0.031904
build,0.022563
carri,0.023021
chair,0.040109
complex,0.020899
comput,0.544330
computerbas,0.057875
condit,0.019984
context,0.025125
control,0.037953
coordin,0.060556
cumul,0.045335
data,0.022795
design,0.041958
determin,0.019835
develop,0.014284
difficult,0.024911
disciplin,0.026319
driven,0.033252
dynam,0.080336
econom,0.489291
econometr,0.101183
economi,0.023484
empir,0.023994
encompass,0.032192
end,0.018801
entiti,0.029177
equat,0.028600
equilibrium,0.035939
exampl,0.015400
extend,0.022682
extern,0.013446
financ,0.054496
follow,0.014709
forc,0.018430
formul,0.029446
forward,0.032641
gametheoret,0.069730
generalequilibrium,0.076499
germani,0.027687
gone,0.040805
ha,0.012261
heterogen,0.046228
includ,0.038294
initi,0.019979
interact,0.137656
interfac,0.040030
internet,0.030164
invers,0.038827
journal,0.165554
lectur,0.033426
leigh,0.063157
linear,0.032709
link,0.013367
macroeconom,0.041528
maintain,0.021434
manag,0.022015
market,0.046019
mathemat,0.024165
matrix,0.077172
method,0.038075
model,0.134032
nding,0.087780
nonlinear,0.042113
numer,0.022773
object,0.042026
offici,0.023336
oper,0.019391
optim,0.032603
paradigm,0.037244
pedagog,0.057438
peopl,0.016947
permit,0.031307
physic,0.020113
polici,0.022747
postul,0.037925
problem,0.037702
process,0.016663
program,0.044345
publicdomain,0.087780
publish,0.019564
python,0.055494
ration,0.030377
rationalexpect,0.085908
real,0.023202
realworld,0.046446
refer,0.022357
region,0.020018
replac,0.022780
repositori,0.052652
repres,0.018337
research,0.054406
restrict,0.025909
rule,0.020405
scienc,0.075205
scientif,0.022495
seri,0.022269
simul,0.033804
social,0.019791
societi,0.019492
softwar,0.032962
sole,0.031580
solut,0.079853
solv,0.057265
special,0.019457
specif,0.018614
start,0.019693
statist,0.051048
studi,0.049325
subject,0.019791
support,0.018498
teach,0.030477
tesfats,0.087780
test,0.025433
theoret,0.051020
theori,0.018236
thi,0.011636
time,0.027530
tool,0.124559
tradit,0.019591
ultim,0.026909
uniqu,0.026527
univers,0.016113
urban,0.032632
use,0.038137
variou,0.017711
visit,0.031257
way,0.016635
work,0.030504
