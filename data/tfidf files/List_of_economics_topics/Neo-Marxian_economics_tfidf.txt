absorb,0.030043
accord,0.015027
accumul,0.028830
actual,0.076935
adam,0.060998
adopt,0.020738
advertis,0.035313
advis,0.030377
affirm,0.036273
alan,0.031859
alien,0.035217
analysi,0.036232
analyt,0.027398
analyz,0.025261
approach,0.051887
archiv,0.055321
argument,0.025249
associ,0.044042
avail,0.036741
averag,0.022447
baran,0.348804
bell,0.035084
big,0.027438
bourgeoi,0.091679
bowl,0.046036
busi,0.040855
capac,0.025158
capit,0.078954
categori,0.069473
center,0.038714
chapter,0.028286
chosen,0.027733
class,0.020130
communist,0.029938
compet,0.025026
competit,0.047736
complex,0.017762
concentr,0.023754
concept,0.084698
condit,0.016984
conflict,0.022608
consist,0.016246
consum,0.024170
consumpt,0.084936
contain,0.017414
continu,0.029416
contrast,0.020505
cost,0.021342
countri,0.015974
cours,0.021156
critic,0.017855
current,0.062161
cut,0.025771
david,0.021563
deal,0.019555
decis,0.020466
declin,0.023047
defin,0.016659
demonstr,0.021819
denounc,0.039319
despit,0.020583
destroy,0.025384
develop,0.036420
differ,0.038606
distinct,0.018695
dobb,0.060913
domin,0.039951
drive,0.026081
earner,0.049775
easiest,0.048998
econom,0.271892
economi,0.099794
economist,0.054876
elabor,0.030410
elit,0.032452
elster,0.067721
employ,0.038770
end,0.015978
environ,0.038745
equal,0.019902
especi,0.016970
essenti,0.020315
evalu,0.025322
exploit,0.054411
extern,0.011427
figur,0.021643
focus,0.020343
format,0.021314
function,0.016000
game,0.026817
gear,0.039662
gener,0.011229
ginti,0.062078
given,0.031347
goal,0.021495
gordon,0.036448
govern,0.029721
grossman,0.054507
group,0.028193
growth,0.019780
hand,0.019363
help,0.017108
henc,0.024383
henryk,0.059261
herbert,0.033839
hi,0.014012
high,0.015824
histor,0.017554
home,0.022315
hostil,0.032410
howev,0.012683
human,0.015056
idea,0.017539
imperialist,0.047627
import,0.013328
includ,0.010848
individu,0.016119
industri,0.017837
influenti,0.025133
inform,0.015536
innov,0.027069
integr,0.018858
intern,0.013694
introduc,0.036663
introduct,0.020717
irrat,0.040255
joan,0.042497
john,0.017775
jon,0.043625
kalecki,0.122569
keynesian,0.042903
labor,0.051310
lang,0.043185
larg,0.027539
lead,0.031177
lessdevelop,0.059261
level,0.015623
like,0.013955
limit,0.015955
link,0.022720
long,0.016963
low,0.020022
luxemburg,0.053063
maintain,0.018216
make,0.013284
mani,0.011808
market,0.058666
marketclear,0.065616
marx,0.109544
marxian,0.234338
marxism,0.040855
marxist,0.106409
mathemat,0.020537
mauric,0.038959
micha,0.055235
militarist,0.051697
minor,0.024175
model,0.016272
modern,0.015669
monopoli,0.066942
monopolist,0.047315
monthli,0.035608
natur,0.041839
neoclass,0.039536
neocoloni,0.054864
neomarx,0.065616
neomarxian,0.463845
neoricardian,0.064451
nonmarxist,0.058965
novel,0.028913
offici,0.019833
onli,0.012025
operation,0.053676
optim,0.055418
optimist,0.044035
optimum,0.043377
orthodox,0.035179
oskar,0.047239
outlin,0.025428
output,0.081801
parti,0.020188
particularli,0.018320
paul,0.022708
piero,0.051571
plan,0.059352
polici,0.057997
polish,0.035294
polit,0.065584
posit,0.015482
postkeynesian,0.054335
postmarx,0.068553
postmarxian,0.073011
potenti,0.019394
pressur,0.022549
price,0.044353
produc,0.015586
product,0.078064
profit,0.025882
przeworski,0.138945
pursu,0.027243
radic,0.054923
rais,0.022095
ration,0.051633
refer,0.019000
reform,0.023239
regard,0.018227
relat,0.012809
relationship,0.018135
resourc,0.038467
restraint,0.042675
result,0.013233
review,0.021287
revolut,0.023535
robinson,0.038959
roemer,0.061284
rosa,0.048033
s,0.026949
samuel,0.033774
save,0.026979
school,0.096646
schwartz,0.094630
sell,0.026875
serv,0.018911
share,0.018513
sinc,0.013221
socialist,0.059159
societi,0.049698
spend,0.027445
sraffa,0.056902
stabil,0.024658
stress,0.027114
supplementari,0.048033
surest,0.060913
surplu,0.243515
surplus,0.046867
surplusa,0.079060
sweezi,0.125903
technic,0.023515
techniqu,0.020319
technolog,0.019063
tendenc,0.029690
term,0.012494
theori,0.092990
theorist,0.030817
therefor,0.017971
thi,0.049449
thought,0.018739
threat,0.027733
throw,0.040603
time,0.011698
tradit,0.016649
ultim,0.022869
underconsumpt,0.061672
underdevelop,0.044195
use,0.021607
util,0.023920
utilis,0.043475
valu,0.067787
variou,0.015052
volum,0.022648
wa,0.021682
wage,0.029054
way,0.014137
websit,0.025002
wider,0.030106
wood,0.030510
work,0.012962
worker,0.024613
world,0.013405
