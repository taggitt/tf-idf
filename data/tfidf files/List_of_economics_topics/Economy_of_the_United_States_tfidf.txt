abandon,0.004181
abov,0.002865
absolut,0.022779
abund,0.004539
ac,0.006095
academ,0.003352
academia,0.005772
access,0.018581
accid,0.005396
accord,0.042525
account,0.028998
accumul,0.004293
accuraci,0.004854
achiev,0.005631
acquisit,0.004839
act,0.010232
activ,0.020100
acumen,0.008441
ad,0.002856
adapt,0.003564
add,0.004268
addit,0.009057
address,0.003370
administr,0.015647
adolesc,0.006512
adult,0.013145
advanc,0.013822
advantag,0.006948
advis,0.004524
advisori,0.005502
aerospac,0.006431
affect,0.008774
afford,0.009613
africa,0.003634
african,0.015692
africanamerican,0.007178
age,0.028031
agenc,0.003521
agre,0.003374
agreement,0.010249
agribusi,0.008408
agricultur,0.029941
ahead,0.005225
ailment,0.007117
air,0.003438
airbuss,0.011774
aircraft,0.009942
airlin,0.022789
airlinessuccess,0.011774
airplan,0.006566
airport,0.014790
airway,0.007256
al,0.003839
alamo,0.008171
albeit,0.005657
alcohol,0.005138
alexand,0.004288
alli,0.004134
alloc,0.004466
allow,0.008903
alon,0.011483
altern,0.002876
amazon,0.006403
amazoncom,0.008171
amen,0.006303
america,0.037642
american,0.144552
americanorigin,0.011774
ampl,0.006927
amtrak,0.011111
analys,0.008751
analysi,0.008094
angel,0.021470
ani,0.013808
annual,0.020630
anoth,0.010764
antimoney,0.009446
antiterror,0.008474
antitrust,0.021934
antiusuri,0.010874
apart,0.004101
appear,0.005185
appl,0.017486
approach,0.002575
approxim,0.033553
april,0.003424
arabl,0.006846
area,0.008892
argu,0.011910
arkansa,0.008092
artifici,0.003779
artist,0.004697
asian,0.021320
asid,0.005104
assembl,0.006988
asset,0.028964
associ,0.013119
assum,0.003100
atlant,0.004743
atlanta,0.007680
att,0.007881
attack,0.007217
attempt,0.002553
attitud,0.004594
attract,0.010804
attribut,0.006958
auction,0.006446
audretsch,0.010500
australia,0.003890
automobil,0.033646
avail,0.002736
averag,0.050148
award,0.003945
babi,0.005851
balanc,0.003542
bank,0.051789
barrel,0.012518
base,0.015602
basic,0.002715
bay,0.005054
bear,0.004188
becam,0.004933
becaus,0.010158
becom,0.012962
beef,0.006543
befor,0.004610
began,0.013474
begin,0.007894
begun,0.004291
believ,0.005582
bell,0.005225
belong,0.007626
beneficiari,0.006424
benefit,0.003275
berkshir,0.016134
best,0.002974
bestsel,0.006656
better,0.009130
betterknown,0.009019
biggest,0.005066
billion,0.115004
billionair,0.015941
bind,0.004660
biotech,0.008171
biotechnolog,0.035274
birth,0.003976
black,0.010927
blame,0.005361
bloc,0.005415
bloomberg,0.007256
board,0.003893
boe,0.014567
bond,0.007709
boom,0.010240
boot,0.007859
border,0.007951
bordo,0.009245
borrow,0.013437
borrowingcap,0.011774
boston,0.004975
bostoncambridg,0.011774
brand,0.005455
brazil,0.009645
bring,0.003355
britain,0.007604
british,0.006026
broader,0.009000
brook,0.005940
brother,0.004525
brought,0.003326
brown,0.004579
brussel,0.006504
budget,0.016898
build,0.002855
bulb,0.007093
burden,0.005151
bureau,0.015314
busi,0.118654
busiest,0.016628
businessfund,0.011774
buy,0.004269
c,0.002582
calcul,0.006888
calendar,0.005534
california,0.037262
californian,0.008698
came,0.005910
camera,0.005842
canada,0.023229
canadaprovid,0.011774
cancer,0.009942
capac,0.003746
capit,0.058796
capita,0.019623
capitalist,0.005339
captur,0.003711
cardin,0.005926
care,0.030561
career,0.004434
cargil,0.008871
carri,0.008741
case,0.004520
categori,0.003449
caucasian,0.007339
caus,0.016888
cbo,0.019738
ceil,0.033495
censu,0.021201
cent,0.028038
center,0.005766
centr,0.003716
central,0.010135
centuri,0.041780
certain,0.002474
cessna,0.009683
challeng,0.003228
chang,0.020379
channel,0.004462
character,0.003428
charg,0.003488
charit,0.006647
chase,0.006146
chemic,0.006883
chevron,0.016949
chicago,0.008811
child,0.004341
childbear,0.008658
children,0.018587
chile,0.005846
china,0.024310
cholesterol,0.007680
choos,0.003888
christoph,0.004737
chronic,0.005556
chronolog,0.005389
cia,0.005370
cigarett,0.006969
circul,0.004729
cite,0.003780
citi,0.021852
citizen,0.007398
civil,0.003100
civilian,0.009398
claim,0.005487
class,0.002998
climat,0.007719
clinton,0.006246
close,0.007256
cloth,0.004725
cluster,0.009919
coal,0.005271
coast,0.004125
coastlin,0.005622
coincid,0.004862
coloni,0.014951
combat,0.004745
combin,0.015466
come,0.012927
commerc,0.004504
commiss,0.003599
commit,0.007578
committe,0.003878
commod,0.004387
common,0.004371
commonplac,0.006453
commun,0.004727
compani,0.085638
compar,0.018717
comparison,0.007581
compens,0.004569
competit,0.014219
competitor,0.005600
complex,0.007936
compon,0.003113
composit,0.003747
comprehens,0.003982
compress,0.005581
compris,0.011095
comput,0.002995
concentr,0.017689
concern,0.015228
conclud,0.003697
conclus,0.004028
condit,0.007588
conduct,0.003098
confeder,0.005396
confid,0.004624
confin,0.004854
congress,0.016203
congression,0.012835
connect,0.002976
conocophillip,0.010210
consecut,0.006112
consid,0.008433
consider,0.009114
consist,0.002419
constant,0.003594
constitut,0.002854
constrain,0.005256
construct,0.008398
consum,0.043198
consumpt,0.012650
contend,0.005211
contin,0.004658
conting,0.005250
continu,0.021905
contract,0.007253
contribut,0.021228
contributor,0.010577
control,0.026422
converg,0.004762
convinc,0.004812
cooper,0.006910
copror,0.011774
corn,0.005653
corpor,0.060301
cost,0.031786
cotton,0.005527
counterpart,0.004847
counti,0.010760
countri,0.085651
cours,0.003151
court,0.006858
cover,0.005953
coverag,0.010876
cowen,0.008826
creat,0.013268
creation,0.009872
creator,0.005418
credit,0.017493
creditworthi,0.008042
crest,0.007507
crime,0.004485
crimin,0.004579
crise,0.005577
crisi,0.026116
critic,0.002659
crude,0.005677
ct,0.006259
cultur,0.002718
currenc,0.067350
current,0.006943
curv,0.004448
custom,0.003655
cut,0.011514
cutback,0.008376
dairi,0.006656
dakota,0.008018
dam,0.012202
danger,0.008493
data,0.008656
david,0.003211
day,0.005456
dc,0.005006
dcision,0.011774
deal,0.029125
death,0.015832
debat,0.006731
debt,0.155119
debttogdp,0.009127
decad,0.023005
decemb,0.016588
decis,0.006096
declar,0.003491
declin,0.041192
decreas,0.010680
deep,0.004024
deepli,0.005104
defens,0.007936
deficit,0.060218
deficitat,0.011774
defin,0.007443
deflat,0.006639
degre,0.002960
delay,0.004617
deloitt,0.009599
demand,0.006455
democracysoci,0.011774
demograph,0.004854
denmark,0.005303
densiti,0.004362
depart,0.003169
depend,0.004771
deposit,0.012840
depot,0.007368
depress,0.031443
dept,0.007507
depth,0.004746
deregul,0.020045
deriv,0.005497
descend,0.008751
describ,0.002225
design,0.005310
despit,0.003065
deterior,0.005324
determin,0.005021
detroit,0.015361
develop,0.050627
devic,0.008081
devis,0.004959
devot,0.017094
diabet,0.006396
diagnos,0.011712
did,0.005367
diego,0.006123
differ,0.001916
difficult,0.003153
diminish,0.004894
direct,0.014404
directli,0.002904
disabl,0.005265
disadvantag,0.005361
disagre,0.005197
disclosur,0.013009
discount,0.005219
diseas,0.003787
dispos,0.005348
disput,0.003636
distant,0.005025
distinct,0.002784
distract,0.006489
distribut,0.011932
district,0.013119
divid,0.005733
doctor,0.008629
document,0.006980
doe,0.002446
dollar,0.063367
domest,0.033714
domin,0.008925
door,0.005653
doubl,0.012008
dow,0.007035
downturn,0.018577
dramat,0.004083
drive,0.011653
driver,0.005541
drop,0.011904
drug,0.025584
durabl,0.006090
dure,0.033298
dynam,0.006779
e,0.002929
earli,0.021770
earlier,0.003216
earn,0.020456
earner,0.007413
eas,0.025055
east,0.003346
econom,0.119102
economi,0.157549
economiesand,0.011774
economist,0.032692
edison,0.007881
educ,0.008318
effect,0.004229
effort,0.009043
elect,0.003210
electr,0.017995
electron,0.010927
elig,0.005445
elsewher,0.008974
emerg,0.022317
emphas,0.003820
empir,0.003037
employ,0.072179
employe,0.022804
enabl,0.006903
encourag,0.010732
encroach,0.006707
end,0.014279
endow,0.005513
energi,0.024377
enforc,0.008161
engag,0.010340
engin,0.006273
england,0.003777
enhanc,0.004031
enjoy,0.008287
enrol,0.011970
ensur,0.003575
enter,0.006364
enterpris,0.020485
entir,0.005646
entiti,0.007386
entrepreneur,0.005482
entrepreneuri,0.006770
entrepreneurship,0.046075
environ,0.005770
environment,0.010498
equal,0.002964
equip,0.011674
equiti,0.004980
equival,0.003506
era,0.010317
erod,0.006265
especi,0.002527
estat,0.009267
estim,0.030630
et,0.003741
eu,0.014217
euro,0.005538
europ,0.008854
european,0.018977
eurozon,0.007178
event,0.002832
eventu,0.006187
everi,0.010894
evid,0.002968
exampl,0.005848
exceed,0.022854
excel,0.004708
exchang,0.030769
exclud,0.004094
exercis,0.003739
exist,0.002164
expand,0.009237
expans,0.018363
expect,0.008971
expenditur,0.043314
expens,0.007775
experienc,0.015630
experiment,0.003635
expir,0.005883
explain,0.008790
explor,0.003316
export,0.031943
express,0.002820
extend,0.005742
extens,0.011757
extern,0.003404
extrem,0.003122
extremepoverti,0.011774
exxon,0.008581
exxonmobil,0.009520
face,0.006534
facil,0.008344
fact,0.008290
factbook,0.005455
facto,0.010553
factor,0.013799
factori,0.004762
fail,0.003272
failur,0.003787
faith,0.004351
fall,0.008913
fallen,0.021154
famili,0.012055
fanni,0.008018
far,0.008854
fare,0.006716
farm,0.021714
farmer,0.004607
farmland,0.006958
fast,0.004455
faster,0.004540
favor,0.003595
fear,0.004084
featur,0.002943
februari,0.010609
fed,0.005824
feder,0.086640
fee,0.005094
feed,0.004996
feet,0.005315
fell,0.042038
felt,0.004594
femal,0.008740
fertil,0.009125
fewer,0.004615
field,0.002377
fifth,0.013906
figur,0.012894
film,0.004414
final,0.005630
financ,0.037939
financi,0.052825
finland,0.005820
firm,0.023325
firsttimeearlystag,0.011774
fiscal,0.054282
fish,0.004061
fiveyear,0.010600
flat,0.004978
flight,0.004768
flood,0.005138
flow,0.003427
fluctuat,0.009721
focu,0.003193
focus,0.003029
follow,0.022342
food,0.020099
forb,0.012594
forc,0.006998
ford,0.012326
foreign,0.021209
foremost,0.006079
forest,0.004476
form,0.010674
formerli,0.004360
fortun,0.010559
fossil,0.010089
foster,0.004975
fought,0.004937
foundat,0.009105
fourth,0.012183
fraction,0.004555
fragment,0.004577
franc,0.013328
francisco,0.005023
fraser,0.007166
fraud,0.005361
free,0.010634
freedom,0.025720
freest,0.008739
frequenc,0.008896
frequent,0.003271
fruit,0.004612
fruition,0.007947
fuel,0.017253
fulli,0.003509
fulltim,0.018595
function,0.002382
fund,0.032092
fundament,0.002980
funder,0.009019
furthermor,0.003988
futur,0.002834
g,0.012594
ga,0.007831
gadget,0.008474
gain,0.014158
gap,0.013592
garner,0.006489
gave,0.006773
gdp,0.196984
gdpwa,0.011774
gener,0.025087
georg,0.003354
gerd,0.032169
gerdgdp,0.010673
german,0.003287
germani,0.017523
gift,0.005253
given,0.004668
global,0.052546
goal,0.006402
gold,0.004047
good,0.043991
goodman,0.007608
googl,0.016789
govern,0.112880
governmentand,0.009973
grade,0.005428
gradual,0.007483
graham,0.005931
grain,0.010114
grant,0.003568
great,0.028855
greater,0.011726
greatest,0.004095
grew,0.042784
gross,0.036406
group,0.004199
grow,0.011938
grown,0.017929
growth,0.055976
guarante,0.008538
guinea,0.005495
gulf,0.005265
gwp,0.011111
h,0.003233
ha,0.116399
habit,0.004934
half,0.038446
hand,0.008651
happen,0.003678
harbor,0.005824
harder,0.005785
harm,0.017948
hartsfieldjackson,0.011111
harvard,0.009094
hathaway,0.017479
head,0.003004
headquart,0.005157
health,0.064895
healthcar,0.016446
healthcaremed,0.011774
healthi,0.005081
heart,0.012816
heavi,0.004022
heavierthanair,0.010086
heavili,0.007504
hedg,0.005811
height,0.004640
held,0.013759
help,0.012740
henri,0.003751
heritag,0.009521
hernndez,0.008067
hewlettpackard,0.008782
hi,0.002086
high,0.065989
higher,0.060874
highest,0.060591
highestgross,0.009683
highincom,0.015148
highli,0.015320
highopportun,0.011774
highspe,0.006906
hightech,0.042013
highway,0.011748
hire,0.005081
hirsch,0.008067
hispan,0.022106
histor,0.015687
histori,0.005815
hold,0.005607
home,0.029912
homeless,0.014283
homicid,0.007994
hoover,0.007354
horizon,0.005673
host,0.021280
hour,0.016360
hous,0.012489
household,0.115750
howev,0.030223
hub,0.006042
huge,0.004328
human,0.004484
hydroelectr,0.006743
hypertens,0.007970
ibm,0.023941
idea,0.005224
ii,0.012184
ill,0.004519
illeg,0.004689
illinoi,0.005697
imf,0.005097
immedi,0.003421
immigr,0.037054
impact,0.009569
imped,0.006271
import,0.021836
importantli,0.005333
improv,0.014320
inadequ,0.005527
incarcer,0.007523
incid,0.004605
includ,0.043624
incom,0.156859
incomeearn,0.010086
incorpor,0.003196
incorrectli,0.006297
increas,0.047002
increasingli,0.003431
indefinit,0.005468
independ,0.007171
index,0.044370
india,0.003563
indic,0.008589
indirectli,0.005015
individu,0.012003
induct,0.010308
industri,0.108923
industrialenergi,0.011774
industriestruck,0.011774
inequ,0.063663
infant,0.005592
infect,0.005282
inflat,0.021916
inflationadjust,0.026615
influenc,0.005127
influenti,0.011230
inform,0.009255
infrastructur,0.003940
inherit,0.004316
initi,0.002528
injuri,0.005159
inland,0.011170
inner,0.004678
innov,0.040316
input,0.004216
insecur,0.013363
instabl,0.005032
instanc,0.006304
instead,0.005452
institut,0.022166
instrument,0.003583
insul,0.006336
insur,0.053846
integr,0.002808
intel,0.015014
intellectu,0.011978
intellig,0.003631
intend,0.003603
intens,0.011596
interc,0.019040
interf,0.006631
interior,0.004567
intern,0.032633
internet,0.003818
internetspecif,0.011774
interpret,0.003105
intrapreneurship,0.010086
invert,0.006290
invest,0.096102
investor,0.017736
involv,0.004512
ireland,0.004864
israel,0.004766
issu,0.009990
itali,0.004188
januari,0.019929
japan,0.034640
jeffrey,0.006047
jersey,0.005763
job,0.068328
jobless,0.026346
john,0.005294
johnson,0.021273
jone,0.005167
journal,0.011974
jpmorgan,0.009019
jr,0.004943
juli,0.013447
jump,0.005373
june,0.010172
just,0.008141
kearneyforeign,0.011774
keyn,0.006438
kind,0.003100
kingdom,0.006191
known,0.005834
kof,0.010874
korea,0.014092
kroger,0.011774
kronick,0.011774
labor,0.045852
laboratori,0.007898
lack,0.005934
lag,0.006057
lake,0.004589
lakesf,0.011774
land,0.008835
larg,0.047168
larger,0.009214
largest,0.138200
late,0.029635
later,0.004628
latestag,0.009973
latin,0.003657
launder,0.007270
law,0.014065
lawrenc,0.005205
lawsuit,0.006424
lead,0.018573
leader,0.013610
leadership,0.004221
learn,0.003268
leav,0.006590
led,0.007534
legal,0.006421
legisl,0.007125
lend,0.009998
lender,0.006290
let,0.004360
level,0.065152
levi,0.005468
liabil,0.005468
liberia,0.007002
licens,0.013900
life,0.007504
lifestyl,0.005541
light,0.009624
like,0.018705
limit,0.009505
lindert,0.010347
line,0.008354
link,0.001691
list,0.012982
littl,0.002978
live,0.035223
lo,0.010673
loan,0.004514
local,0.013177
locat,0.005721
lockhe,0.008474
long,0.005052
longer,0.009627
longest,0.005577
longlast,0.007166
longstand,0.005622
longterm,0.008189
look,0.006425
lose,0.008184
loss,0.006806
lost,0.016906
lot,0.004792
louisiana,0.006725
low,0.041749
lower,0.028751
lowerincom,0.009375
lowest,0.018512
lowincom,0.007002
luca,0.006222
lumber,0.014796
lung,0.006084
m,0.002782
machineri,0.014395
macroeconom,0.005256
mae,0.008042
magazin,0.009009
main,0.004926
mainli,0.006460
maintain,0.024417
mainten,0.004667
major,0.024771
make,0.005935
maker,0.010735
male,0.013093
mani,0.036931
manufactur,0.070500
march,0.012906
margin,0.008528
market,0.078638
marri,0.004877
marriag,0.004919
martin,0.004118
maryland,0.019381
mason,0.006734
mass,0.006381
massachusett,0.025511
master,0.004152
materi,0.002726
mathemat,0.003058
matur,0.009055
maynard,0.006336
mcdonald,0.014950
mckesson,0.011774
mean,0.014999
meaning,0.005057
meant,0.007981
meanwhil,0.004704
measur,0.007645
mechan,0.002873
median,0.066637
medic,0.028664
medicaid,0.049027
medicar,0.030296
medicin,0.003709
meet,0.003148
meissner,0.009520
member,0.004965
membership,0.004285
men,0.014431
merchandis,0.013346
merger,0.005638
method,0.004819
methodolog,0.004118
metropolitan,0.005911
mexico,0.029125
michael,0.003580
michigan,0.016845
microsoft,0.030816
mid,0.019372
middl,0.003208
migrat,0.008314
mild,0.006005
mile,0.009045
militari,0.012590
militaryindustri,0.008919
million,0.140581
millionair,0.024513
millward,0.011111
miner,0.004307
minimum,0.008488
minor,0.003600
minorityown,0.011774
minu,0.005750
mirror,0.005253
mix,0.007484
mobil,0.024797
moder,0.008914
modern,0.002333
monetari,0.012513
money,0.020863
monitor,0.004207
monopoli,0.014955
month,0.006875
mortal,0.010400
mortgag,0.017240
mostli,0.003319
motor,0.015054
movement,0.002682
movi,0.005321
mri,0.006743
multin,0.011786
multipl,0.002992
municip,0.004741
music,0.004276
muslim,0.008702
mutual,0.004104
nafta,0.008118
nasdaq,0.023042
nation,0.116423
nationorigin,0.011774
nationwid,0.005911
nativ,0.004021
natur,0.008308
nber,0.008509
near,0.003199
nearli,0.027111
nearpoverti,0.011774
need,0.004677
neg,0.010121
negoti,0.004072
neighborhood,0.011786
neoliber,0.006543
net,0.053170
netherland,0.004534
network,0.003252
nevada,0.007491
new,0.053876
newli,0.004093
news,0.004246
night,0.004772
niip,0.011774
nikola,0.007574
nineteenth,0.004866
ninthhighest,0.011111
nobel,0.004529
nomin,0.016244
noncitizen,0.008474
nonfarm,0.017838
nonfinanci,0.008255
nonprofit,0.011223
nonu,0.008042
nordic,0.006716
normal,0.003101
north,0.003089
northern,0.003637
notabl,0.005971
novemb,0.003489
nuclear,0.003975
number,0.033089
numer,0.008647
nyse,0.007838
obama,0.006504
obes,0.007166
observ,0.002567
obtain,0.003024
obviou,0.004914
occup,0.004054
occur,0.004944
ocean,0.004003
octob,0.016822
oecd,0.040315
offer,0.005786
offic,0.003131
offici,0.020677
oil,0.027184
oklahoma,0.014116
old,0.003219
older,0.008166
onc,0.002907
onequart,0.007368
onethird,0.005911
onli,0.021492
onlin,0.003405
open,0.005327
oper,0.009818
opportun,0.011025
oppos,0.003251
optim,0.004126
organ,0.014855
organis,0.003683
origin,0.002156
outlay,0.007591
outpac,0.007859
output,0.040610
outsid,0.002968
overal,0.039171
overhead,0.006927
overtak,0.007413
owe,0.014206
owner,0.004401
ownership,0.018277
pace,0.015752
pacif,0.004509
page,0.011443
paid,0.019835
pain,0.004928
panic,0.006527
paper,0.003210
papua,0.006808
par,0.005970
parallel,0.004031
parent,0.004381
pari,0.004254
pariti,0.005600
parti,0.006013
particip,0.003069
particular,0.004870
particularli,0.002728
partieshav,0.011774
partli,0.008367
partner,0.008506
parttim,0.027386
passag,0.004635
passeng,0.023157
past,0.006173
patent,0.020768
patient,0.004739
paul,0.003382
pay,0.010897
payment,0.020968
payrol,0.007047
peak,0.025616
pearl,0.006005
peer,0.005425
penal,0.013055
pennsylvania,0.005828
pension,0.005405
peopl,0.034322
percapita,0.022571
perceiv,0.003912
percent,0.054708
percentag,0.013007
perform,0.005470
period,0.023118
perman,0.003770
person,0.030551
pester,0.011111
peter,0.003547
petrodollar,0.009446
petroleum,0.025007
pew,0.007838
pfizer,0.009245
pharmaceut,0.021201
phenomenon,0.004007
philadelphia,0.005916
philippon,0.010874
phonograph,0.009185
physic,0.002545
physician,0.014724
piketti,0.009683
pioneer,0.004064
place,0.011002
plan,0.008839
planet,0.004414
play,0.014046
plu,0.004377
point,0.004513
polic,0.004327
polici,0.043189
polit,0.007325
politician,0.004667
poll,0.005415
poor,0.026929
popul,0.035987
popular,0.011134
pork,0.007256
portion,0.003909
posit,0.011529
possess,0.003496
post,0.007552
postal,0.005990
postindustri,0.008284
postwar,0.005291
potent,0.007002
potenti,0.002888
poultri,0.006906
poverti,0.068846
power,0.011416
ppaca,0.011111
ppp,0.012821
practic,0.002387
predict,0.006529
pregnanc,0.006482
prerecess,0.010673
prescript,0.005879
presid,0.009679
press,0.002625
prestigi,0.006342
prevent,0.003189
previou,0.010093
previous,0.006802
price,0.039635
pricecut,0.011774
primari,0.008835
primarili,0.009149
princip,0.006970
prior,0.003369
privaci,0.006598
privat,0.057584
privileg,0.005092
prize,0.004161
probabl,0.003201
problem,0.011930
process,0.008436
produc,0.023214
product,0.062784
professor,0.007975
profit,0.034693
profoundli,0.006460
program,0.019646
progress,0.012163
prohibit,0.009104
promis,0.004192
promot,0.003182
prompt,0.004884
properti,0.016208
proport,0.014755
prosper,0.004612
protect,0.017841
provid,0.019795
provis,0.004345
provision,0.005653
proxi,0.006252
psycholog,0.003803
public,0.024716
publicli,0.004774
publicsector,0.007994
publish,0.004952
puerto,0.006417
purchas,0.003794
purpos,0.002902
pursuit,0.005030
q,0.009993
qualiti,0.009972
quantit,0.004145
quarter,0.018562
question,0.002801
quickli,0.007263
quota,0.006106
radio,0.004567
rail,0.028076
rais,0.026326
ran,0.004994
rang,0.008034
rank,0.030135
ransom,0.007459
rapid,0.011599
rapidli,0.007522
rate,0.129951
ratio,0.016095
ration,0.003845
raw,0.004615
rd,0.115859
reach,0.025880
real,0.026432
realloc,0.008171
reason,0.005079
reced,0.014071
receipt,0.018303
receiv,0.016220
recent,0.024502
recess,0.106354
recommend,0.004171
record,0.029825
recov,0.013067
recoveri,0.023552
recreat,0.005465
red,0.003922
redistribut,0.005883
reduc,0.010722
reduct,0.008142
refer,0.007074
reflect,0.006081
regard,0.008144
region,0.002533
registri,0.007058
regul,0.100495
regulatori,0.004897
reimburs,0.007368
rel,0.030444
relat,0.003815
relax,0.005408
releas,0.007072
relev,0.003700
reli,0.006819
relianc,0.005673
remain,0.018721
remaind,0.010501
remitt,0.006927
remov,0.003267
renew,0.016551
renown,0.006036
report,0.048953
repres,0.016247
republ,0.003320
requir,0.002227
research,0.066570
reserv,0.030106
resid,0.007497
resourc,0.011458
respect,0.008072
respons,0.014725
rest,0.006630
result,0.011826
resum,0.005412
retail,0.030705
retir,0.004814
retrench,0.008968
reveal,0.003722
revenu,0.037152
revit,0.006876
revolv,0.005767
reward,0.004894
reynold,0.006743
rich,0.016230
richard,0.003495
richest,0.006169
rico,0.006681
ridership,0.010210
right,0.010156
rise,0.014236
risen,0.005960
risk,0.020889
river,0.003868
road,0.008220
robert,0.006222
role,0.019830
rose,0.051527
roughli,0.015877
rout,0.004260
royalti,0.006453
rub,0.007256
rule,0.005165
run,0.012438
s,0.056192
sacrific,0.005763
safeti,0.018243
said,0.008721
salam,0.007626
salari,0.010467
sale,0.032976
salient,0.007024
san,0.008987
save,0.016072
saw,0.010388
say,0.009197
scale,0.012810
scan,0.005649
scholar,0.010780
school,0.008636
scienc,0.014279
scientif,0.005694
score,0.020746
scoreboard,0.019366
scotland,0.005318
screen,0.005225
screenbas,0.011774
sear,0.007838
seattl,0.006876
second,0.032512
sector,0.077903
secular,0.004897
secur,0.015031
seedstag,0.011774
seek,0.009525
segment,0.009695
selfemploy,0.014383
semiconductor,0.006174
send,0.004425
sent,0.003922
septemb,0.006824
seriou,0.007927
servic,0.049624
set,0.008767
settlement,0.012387
seven,0.007426
seventh,0.005709
sever,0.022582
sexual,0.004649
shape,0.006745
share,0.035845
sharehold,0.011443
sharpli,0.011133
shelter,0.016550
shift,0.013716
shiller,0.009019
ship,0.008029
shock,0.014512
shoe,0.006535
short,0.003017
shortfal,0.007413
shortli,0.004404
shoulder,0.006606
shown,0.003412
shrink,0.012408
sick,0.005556
sign,0.006484
signific,0.010055
significantli,0.021789
silicon,0.012021
similar,0.004703
similarli,0.007461
simpl,0.003129
simultan,0.004126
sinc,0.068921
singl,0.013177
singlefamili,0.008871
situat,0.003032
size,0.012266
skill,0.022762
skunk,0.008871
slave,0.004653
slid,0.008782
slide,0.006146
slightli,0.017072
slow,0.008131
slower,0.005657
slowli,0.004539
sluggish,0.008042
small,0.031326
smallest,0.005125
smoke,0.005851
smokingrel,0.010347
social,0.027556
societ,0.005538
societi,0.007401
softwar,0.008344
soil,0.004462
sole,0.003997
sometim,0.002563
sort,0.004130
sought,0.007672
sound,0.004146
sourc,0.004954
south,0.009251
southern,0.003654
space,0.003036
spain,0.008369
spawn,0.006277
speak,0.003776
specialpurpos,0.008871
specif,0.002356
spend,0.102192
spent,0.004352
spinoff,0.007297
spring,0.008883
squar,0.004172
st,0.003174
stabil,0.014690
stabl,0.011626
stage,0.006878
stagnant,0.013854
stagnat,0.005897
standard,0.023871
stanford,0.004574
start,0.009971
startup,0.020539
state,0.217108
stateregul,0.009072
statist,0.016154
statu,0.006577
stay,0.012931
steadi,0.010094
steadili,0.005194
steel,0.009865
stock,0.056989
stockhold,0.007608
stood,0.010595
store,0.003989
street,0.022467
strengthen,0.008908
strong,0.002861
struggl,0.004128
studi,0.020811
style,0.004057
su,0.006052
subject,0.002505
subprim,0.007626
subsidi,0.005485
substanti,0.018278
suburban,0.015217
success,0.013057
suffer,0.003594
suffici,0.010922
suggest,0.002756
sum,0.003928
summari,0.004739
summer,0.004555
suppli,0.019262
supplier,0.005545
support,0.002341
supposedli,0.006010
supremaci,0.006438
surg,0.012158
surgeri,0.005552
surinam,0.007475
surpass,0.011119
surplu,0.010362
survey,0.011355
surviv,0.006792
sustain,0.011555
suv,0.009185
swath,0.007699
sweden,0.004919
switzerland,0.010094
t,0.003426
tab,0.008919
tabl,0.011369
tackl,0.006063
taiwan,0.005541
taken,0.005766
target,0.011000
tax,0.055514
taxat,0.038733
taxpay,0.006574
technolog,0.022714
telecommun,0.020533
telephon,0.005383
temper,0.005940
tend,0.016169
tenthhighest,0.011774
term,0.011164
territori,0.003446
tesla,0.007540
texa,0.011119
textil,0.005324
textron,0.010347
th,0.042903
themselv,0.002921
thenrecordhigh,0.011774
thi,0.032405
think,0.003415
thoma,0.010567
thu,0.004604
tie,0.003841
till,0.005945
time,0.026135
tobacco,0.011097
today,0.008487
togeth,0.010862
tokyo,0.006240
took,0.006152
tool,0.006306
topperform,0.010874
toprank,0.008968
total,0.063196
tourism,0.009162
township,0.008018
toy,0.006446
track,0.004357
tract,0.005615
trade,0.079764
tradit,0.002479
trail,0.006460
trajectori,0.011748
transact,0.008480
transform,0.009670
transit,0.013713
transmiss,0.004864
transmit,0.004825
transport,0.020255
travel,0.006954
treasuri,0.026169
treat,0.003448
treatment,0.014490
trend,0.022568
tri,0.003099
tricar,0.010874
trillion,0.192114
trip,0.005441
tripl,0.005425
truck,0.012994
tse,0.008871
turkey,0.005135
turn,0.002722
twentieth,0.004646
twothird,0.016188
twoyear,0.006403
tyler,0.007737
typic,0.005283
u,0.009189
uk,0.010828
ultim,0.003406
unanim,0.006210
unclear,0.005173
underemploy,0.008042
undergo,0.004565
underli,0.003739
underreport,0.008919
undertak,0.005345
unduli,0.008092
unemploy,0.124534
unequ,0.006117
unesco,0.005348
uninsur,0.035128
union,0.020484
unit,0.210544
univers,0.018356
unjust,0.006948
unless,0.004413
unnecessari,0.011631
unrespons,0.009019
unrival,0.009683
unshelt,0.011774
unusu,0.004774
upward,0.005482
urgent,0.006699
usa,0.008793
usbas,0.014887
usborn,0.010673
use,0.028963
usual,0.002369
util,0.007125
vacat,0.006761
valley,0.009108
valu,0.015144
valuabl,0.004622
valuead,0.007270
van,0.004307
vari,0.002903
variat,0.003691
variou,0.002241
various,0.006090
vast,0.003954
veget,0.004701
vehicl,0.017885
ventur,0.067426
venturesom,0.010874
veri,0.002248
verizon,0.009973
versu,0.018386
veteran,0.005995
viabl,0.005418
vigor,0.005697
visitor,0.005386
voic,0.004693
volum,0.016866
vs,0.033283
wa,0.093651
wage,0.056254
wait,0.005339
wall,0.016679
walmart,0.031977
war,0.030772
warn,0.004735
washington,0.021678
water,0.003037
waterway,0.006761
way,0.008422
weaken,0.004932
wealth,0.044608
wealthi,0.015415
wealthiest,0.006896
welcom,0.005435
welfar,0.022679
welldevelop,0.006550
went,0.014448
western,0.005924
wheat,0.005600
wherea,0.003417
whi,0.003623
white,0.011216
wide,0.002536
widest,0.007475
wildli,0.007662
williamson,0.007204
willing,0.006016
withdraw,0.004733
women,0.014991
work,0.027028
worker,0.080648
workforc,0.016614
workingag,0.009772
workplac,0.012361
world,0.165711
worldwid,0.011985
worri,0.005802
worrisom,0.009869
worst,0.005431
worth,0.008917
wright,0.005842
wyom,0.008408
yale,0.005383
year,0.097384
yearend,0.009127
yearround,0.007838
york,0.030330
young,0.007313
youth,0.004966
