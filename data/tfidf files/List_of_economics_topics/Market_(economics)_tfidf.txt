absolut,0.009835
abstract,0.020406
accept,0.003639
access,0.004011
accord,0.008697
account,0.006829
act,0.003313
action,0.003562
activ,0.002892
actor,0.013366
actual,0.011131
ad,0.007399
adam,0.005883
adapt,0.004616
add,0.005528
address,0.004365
adjust,0.005549
advanc,0.003580
advantag,0.004500
advers,0.006790
affect,0.003788
agegend,0.015251
agenc,0.004560
agent,0.019772
aggreg,0.012262
air,0.004453
akerlof,0.021954
alfr,0.005940
alien,0.006793
aliv,0.007244
allencompass,0.019803
alloc,0.023142
allow,0.011532
alreadi,0.004223
altern,0.018626
altruist,0.009218
alway,0.011695
amelior,0.009399
amex,0.012783
analog,0.005180
analys,0.005668
analysi,0.003494
andor,0.005073
angloamerican,0.035679
ani,0.012775
annual,0.004453
anoth,0.011154
answer,0.005162
antagonist,0.008435
anthropolog,0.032864
anthropologist,0.014876
antoin,0.007953
anyon,0.006320
appadurai,0.014767
appear,0.006716
appli,0.006198
applic,0.003462
approach,0.006673
approxim,0.003951
argonaut,0.012144
argu,0.015427
argument,0.009741
aris,0.004729
arithmet,0.007078
arjun,0.014084
arm,0.004622
aros,0.011514
arrang,0.004726
articul,0.007001
artifici,0.009791
ask,0.004792
aspect,0.007620
asper,0.014391
asset,0.005359
associ,0.008496
assum,0.004016
assumpt,0.009878
asymmetr,0.007931
asymmetri,0.025897
attempt,0.013228
attent,0.009455
auction,0.025048
augustin,0.007785
author,0.009792
avail,0.003543
away,0.013249
b,0.003840
backdrop,0.009417
bad,0.011894
baker,0.015693
balanc,0.009177
barter,0.025335
base,0.007578
basic,0.007033
bauer,0.009901
baumol,0.036174
bazaar,0.011616
becam,0.003195
becaus,0.005263
becom,0.005596
began,0.010472
begin,0.006817
behavior,0.020503
bertrand,0.007615
better,0.007884
bid,0.029796
bidder,0.020709
bigbox,0.013600
billion,0.014896
black,0.004717
bloc,0.007014
boilerpl,0.012658
bond,0.014979
book,0.006644
booth,0.009662
borden,0.011432
borrow,0.005801
bought,0.006679
boundari,0.004880
bourdieu,0.010849
bracelet,0.011682
bracket,0.008340
branch,0.007435
brand,0.007065
break,0.004818
broadcast,0.006782
broken,0.005828
bronislaw,0.023645
build,0.003699
built,0.004358
busi,0.023644
buy,0.011060
buyer,0.118610
c,0.003345
calcul,0.013382
callon,0.015251
cambridg,0.004637
came,0.007656
candid,0.005393
capac,0.004853
capit,0.015231
capitalist,0.055331
captur,0.004807
car,0.012373
carbon,0.011412
care,0.004398
carl,0.005845
carri,0.003774
case,0.011709
cast,0.017841
caus,0.003125
ceil,0.017354
center,0.007468
central,0.003282
centuri,0.015032
ceremoni,0.006463
certain,0.006409
cf,0.023687
challeng,0.008364
challon,0.014391
chamberlin,0.022750
chang,0.002639
channel,0.011559
charact,0.009676
characterist,0.004035
charg,0.004518
chew,0.009703
choos,0.005036
cigarett,0.027082
circumst,0.005378
citi,0.004043
civil,0.004016
class,0.003883
classic,0.007736
classif,0.010332
classifi,0.004736
clear,0.004358
clearli,0.005264
close,0.003133
closest,0.007087
cluster,0.006424
coach,0.009641
cohes,0.015194
collect,0.003415
colleg,0.005082
colloqui,0.008148
come,0.003348
command,0.004912
comment,0.005608
commerc,0.005835
commerci,0.008972
commod,0.068200
commodif,0.022533
common,0.005662
commun,0.003061
communitybas,0.010584
compar,0.006926
compass,0.007544
compatriot,0.011320
competit,0.105904
competitor,0.014507
complet,0.006468
complex,0.013706
composit,0.004854
comput,0.003880
concentr,0.009165
concept,0.032679
conceptu,0.005636
concern,0.009862
conclud,0.004789
confront,0.006469
confus,0.005270
connect,0.003855
consid,0.008193
consider,0.003935
consist,0.003134
construct,0.007252
consum,0.032640
consumerori,0.013224
consumerwork,0.014391
contain,0.003359
contemporari,0.013741
content,0.004620
contest,0.030806
conting,0.006801
continu,0.008512
contract,0.014092
contractu,0.008321
contrast,0.023734
contribut,0.003437
controversi,0.004614
converg,0.006168
cooper,0.017903
coordin,0.004964
corpor,0.018378
correct,0.004715
cost,0.016468
costless,0.012144
costli,0.007298
countri,0.015408
cournot,0.021461
cover,0.003855
creat,0.008593
creation,0.004262
critic,0.006889
crown,0.006065
cs,0.014309
culliton,0.015251
cultur,0.007042
currenc,0.015394
current,0.002997
curv,0.034568
custom,0.004735
danger,0.005500
daniel,0.005824
data,0.003737
david,0.016639
deal,0.007545
debat,0.021798
decis,0.011844
decisionmak,0.006750
deconstruct,0.008688
defin,0.012855
definit,0.003702
degrad,0.006718
degre,0.019175
deliber,0.012537
deliveri,0.013536
demand,0.029266
democrat,0.009177
demograph,0.006287
demonstr,0.004209
denomin,0.006977
denot,0.005714
depend,0.009269
deriv,0.010681
describ,0.008647
design,0.003439
despit,0.003970
determin,0.003252
develop,0.021078
devic,0.005233
diamond,0.007109
dictat,0.006697
did,0.003476
differ,0.027308
differenti,0.009483
disciplin,0.008630
discount,0.006761
discours,0.006412
discrimin,0.013006
discuss,0.003821
disembed,0.015251
disembedd,0.015251
disentangl,0.010208
dispers,0.006427
distinct,0.003606
distinguish,0.004157
distort,0.013224
distribut,0.011591
divers,0.009008
domin,0.011560
don,0.007772
dont,0.006543
draw,0.014401
drive,0.005031
drug,0.011046
duopoli,0.020771
durat,0.006669
dure,0.002695
dynam,0.004390
e,0.003794
earli,0.002819
ebay,0.011068
econom,0.129587
economi,0.073157
economicu,0.010619
economist,0.031759
ed,0.003946
edit,0.004412
edward,0.005128
effect,0.005477
effici,0.022072
effort,0.003904
eighteenth,0.007206
electron,0.009435
element,0.003585
els,0.005806
embedded,0.011897
embodi,0.006518
emerg,0.032520
emphas,0.014844
emphasi,0.005113
empir,0.003934
employ,0.003739
enabl,0.017884
encompass,0.005278
end,0.003082
endogen,0.008536
engag,0.013394
engin,0.004063
enhanc,0.010444
enigmat,0.011375
enter,0.008243
enterpris,0.005306
entir,0.007313
entrant,0.009682
entri,0.016360
entrypoint,0.014084
environ,0.011211
environment,0.009065
equal,0.007679
equilibrium,0.058924
especi,0.003273
essai,0.010294
essenc,0.006368
establish,0.009026
estat,0.006001
estim,0.003967
ethic,0.005189
european,0.003511
evalu,0.009770
event,0.003669
evolv,0.004627
examin,0.004412
exampl,0.017675
exchang,0.175360
exhaust,0.006592
exist,0.008410
exit,0.014791
exogen,0.026310
expans,0.009514
expect,0.007747
experi,0.006934
explain,0.011386
exploit,0.010496
extens,0.003807
extern,0.008818
extort,0.009682
extrem,0.004044
f,0.004152
face,0.004231
facil,0.005404
facilit,0.020331
fact,0.003579
factor,0.010724
failur,0.044149
fair,0.017130
fals,0.005718
famili,0.003903
farm,0.005625
farmer,0.005967
father,0.004956
fee,0.006598
field,0.006157
financi,0.008049
firm,0.030212
fiscal,0.006391
fish,0.005260
fit,0.005133
flea,0.010294
flow,0.004439
focu,0.008272
focus,0.007849
follow,0.009646
food,0.013017
footbal,0.007544
forc,0.012087
forg,0.007498
form,0.020739
formal,0.018862
format,0.012335
foucault,0.008722
founder,0.005284
frame,0.005761
framework,0.012958
frank,0.005903
free,0.030992
freedom,0.009518
french,0.008114
function,0.009259
fundament,0.007721
futur,0.018359
game,0.010346
gave,0.004386
gener,0.012998
genesi,0.007585
geograph,0.063739
geographi,0.011098
georg,0.004345
germani,0.004539
gibsongraham,0.030503
gift,0.088466
giftgiv,0.011975
gilgen,0.015251
given,0.039308
giver,0.010933
global,0.024022
goat,0.007931
goe,0.005425
good,0.113962
govern,0.022935
grade,0.007031
greater,0.011391
greek,0.008538
grey,0.008036
groceri,0.009901
group,0.002719
gum,0.009810
h,0.004187
ha,0.030154
hall,0.017159
hand,0.003735
harold,0.007234
harvey,0.007645
hast,0.009348
heirloom,0.025317
held,0.010693
help,0.003300
henc,0.009407
herbert,0.006528
hi,0.037844
high,0.003052
highest,0.009233
highli,0.007937
highlight,0.006207
histor,0.010159
histori,0.020088
hoc,0.025306
homo,0.007902
horizon,0.007348
hotel,0.014517
howev,0.012233
huge,0.005606
human,0.014522
hybrid,0.025194
hypermarket,0.013825
hypothesi,0.005251
idea,0.016918
ident,0.004316
identifi,0.007288
ideolog,0.005694
ignor,0.005229
illeg,0.030374
illicit,0.018197
imagin,0.006027
immedi,0.004431
impact,0.008263
imped,0.008123
imperfect,0.029688
implement,0.004197
impli,0.004686
implicit,0.007061
impos,0.005184
improv,0.007419
includ,0.014649
incorpor,0.008279
increas,0.005798
incumb,0.008083
independ,0.003096
index,0.004789
indian,0.004990
individu,0.031096
industri,0.020646
influenc,0.006641
inform,0.023977
infrastructur,0.015313
infring,0.009041
ingredi,0.023751
inner,0.012120
insid,0.005056
insist,0.006151
instabl,0.006518
instead,0.003531
institut,0.041472
integr,0.010914
intellig,0.004703
intens,0.005007
interact,0.018807
interf,0.008589
interfer,0.006153
intermedi,0.005925
intern,0.005283
internet,0.014836
interpenetr,0.010808
interrelationship,0.008770
intersect,0.006793
interven,0.006478
intervent,0.005470
introduc,0.003536
introduct,0.003996
invent,0.004905
invest,0.012877
investor,0.005743
involv,0.002922
irreplac,0.012058
island,0.004468
issu,0.003235
item,0.022120
j,0.007572
jame,0.004251
japan,0.004985
jerom,0.008445
jevon,0.019620
jewel,0.009127
joan,0.008198
john,0.003429
jonathan,0.007048
joseph,0.004967
jurisdict,0.006118
just,0.003515
k,0.009141
karl,0.005670
keller,0.010073
kin,0.009055
kind,0.004015
kindergarten,0.010482
king,0.004801
klau,0.009027
know,0.004567
koichi,0.012433
kotler,0.011822
kula,0.012783
l,0.008874
labor,0.019797
labour,0.025515
lack,0.003843
laissezfair,0.008677
landscap,0.006284
larg,0.002656
larger,0.007957
late,0.003489
later,0.008992
lauterborn,0.014391
le,0.006004
lead,0.006014
led,0.003252
legal,0.008318
lemon,0.009901
lend,0.006475
leonard,0.007765
level,0.021097
liber,0.013632
liberaldemocrat,0.012658
life,0.006479
like,0.008076
likewis,0.005969
limb,0.007888
limit,0.003078
line,0.010821
link,0.006574
liquid,0.010805
list,0.002802
local,0.017068
locat,0.014823
logic,0.014634
logist,0.007273
lon,0.009027
long,0.006544
look,0.004161
loss,0.004408
luhmann,0.011682
lukc,0.011068
macpherson,0.011897
macroeconom,0.006808
main,0.003190
mainli,0.004184
mainstream,0.006116
major,0.008021
make,0.007688
makro,0.013064
malinowski,0.056601
mall,0.009582
manag,0.010828
mani,0.013667
manufactur,0.009612
marcel,0.017787
margin,0.005523
marginalist,0.020964
marijuana,0.010619
markedli,0.008621
market,0.807323
marketplac,0.015936
marriag,0.006371
marshal,0.020039
marx,0.014088
marxist,0.006842
mass,0.008266
mathemat,0.011885
mati,0.013064
matter,0.003791
mauss,0.068252
maxim,0.034390
mccarthi,0.009142
mcgrawhillirwin,0.013600
mean,0.005551
mechan,0.007443
media,0.013868
meet,0.004077
men,0.004673
menger,0.010294
merchandis,0.008643
mere,0.004957
michael,0.018552
michel,0.013817
microeconom,0.047080
mikro,0.014767
militari,0.004077
milk,0.007443
minim,0.010638
minimum,0.010994
minor,0.004663
mix,0.019390
mixer,0.011616
mode,0.039716
model,0.053368
modern,0.003022
money,0.031528
monopoli,0.051657
monopolist,0.045639
monopsoni,0.021461
moral,0.005041
motiv,0.009847
movement,0.006950
multiform,0.011432
multin,0.007633
multipl,0.007752
nasdaq,0.009948
nathau,0.015251
nation,0.008377
natur,0.013452
necessarili,0.004933
necklac,0.011164
need,0.006059
neg,0.004370
neglect,0.006592
negoti,0.015825
neil,0.008005
neoclass,0.015254
neoliber,0.016950
network,0.029492
new,0.011255
newspap,0.006239
nich,0.007627
night,0.006181
nikla,0.011751
nineteenth,0.012608
non,0.007263
nonaltruist,0.013600
nonmarket,0.010354
nonperfectli,0.015251
nonphys,0.009703
nonwestern,0.009399
notion,0.009536
novel,0.005577
number,0.012606
nyse,0.010153
object,0.010335
observ,0.003326
occur,0.028817
occurr,0.006518
ocean,0.005186
offer,0.018737
offset,0.007328
older,0.005288
oldest,0.005451
oligopoli,0.009602
oneofakind,0.014084
onli,0.009279
onlin,0.013232
oper,0.006358
oppos,0.008423
opposit,0.004146
optim,0.005345
order,0.013886
ordinari,0.005495
organ,0.010995
origin,0.005585
outcom,0.005152
outgrowth,0.009250
outsid,0.007689
ownership,0.011837
p,0.003847
pacif,0.005841
paper,0.016633
parallel,0.005222
paramet,0.005810
parri,0.010619
parti,0.023367
particip,0.011927
particular,0.015772
patent,0.006725
path,0.010219
pathbreak,0.012542
pathdepend,0.012331
patrik,0.015251
peasant,0.007027
pecuniari,0.011214
peopl,0.008335
perfect,0.056307
perform,0.003542
period,0.008983
permit,0.005133
person,0.019786
personhood,0.010655
personnel,0.006228
peter,0.004595
philosophi,0.004290
physic,0.013190
pictur,0.005641
pierr,0.006487
pindyck,0.013402
pioneer,0.005264
place,0.022801
playground,0.010449
point,0.002923
polar,0.006174
polic,0.005605
polici,0.007459
polishbritish,0.012918
polit,0.015815
politi,0.007792
pollut,0.020337
popul,0.003585
popular,0.003605
possibl,0.008842
postcommunist,0.011022
poststructuralist,0.009925
potenti,0.003741
power,0.014787
practic,0.024735
predict,0.004228
predispos,0.009855
prefer,0.008850
prefix,0.015531
prentic,0.015625
prepar,0.009373
presenc,0.004370
present,0.002889
press,0.003401
pressur,0.004350
prestat,0.013402
previous,0.004405
price,0.094121
principalag,0.011320
principl,0.003469
privat,0.020718
privatepubl,0.013600
problem,0.012362
problemat,0.006964
procedur,0.010065
process,0.021856
produc,0.009020
product,0.048191
productsservic,0.011822
profession,0.004682
profit,0.019972
prolifer,0.021342
promin,0.004531
promis,0.005430
promot,0.008244
pronounc,0.006981
properti,0.003499
propos,0.014262
protect,0.003851
provid,0.010256
provis,0.005628
ps,0.017736
public,0.014552
publish,0.003207
purchas,0.004915
pure,0.004569
pursu,0.005255
qualiti,0.012917
quash,0.010808
question,0.007258
radio,0.005916
rang,0.006938
rate,0.003581
reach,0.007449
reaction,0.004699
read,0.006366
real,0.011412
realiz,0.005124
receiv,0.003501
recent,0.006347
recip,0.028251
reciproc,0.020654
reconstruct,0.011487
recur,0.007826
reduc,0.006944
refer,0.010996
refere,0.031446
reflect,0.003938
regard,0.007032
region,0.006564
registr,0.007136
regul,0.026034
rel,0.006572
relat,0.024712
relationship,0.017492
reli,0.013249
religi,0.009363
remov,0.008463
reperform,0.014391
repres,0.009019
repress,0.006912
reput,0.006377
research,0.002973
resembl,0.005781
resourc,0.014842
restrict,0.004247
restructur,0.007239
result,0.007659
retail,0.039772
return,0.011127
revolut,0.004540
ricardo,0.016662
right,0.016444
ring,0.012415
rise,0.007376
risk,0.004509
rival,0.012111
robert,0.012089
robinson,0.007515
role,0.012842
rubinfeld,0.014084
rule,0.020073
run,0.024166
s,0.015596
sage,0.007225
said,0.011297
sale,0.021357
saren,0.013825
say,0.007941
scale,0.008296
scarciti,0.007657
scatter,0.006768
schema,0.009013
scheme,0.005516
scholarli,0.012975
school,0.011186
scienc,0.003082
scientif,0.003688
scope,0.005388
second,0.006016
seek,0.004112
seen,0.003596
segment,0.006279
seizur,0.016346
select,0.003985
selfinterest,0.016571
selfreal,0.011022
selfunderstand,0.011432
sell,0.036293
selladvertis,0.015251
seller,0.146463
semifeud,0.012542
sens,0.018856
serv,0.003648
servic,0.064277
set,0.014195
seventeenth,0.007960
sever,0.002659
share,0.003571
sharper,0.010655
shift,0.008883
shimizu,0.013064
shop,0.020516
short,0.003907
sideeffect,0.018977
signal,0.010547
signifi,0.007395
similar,0.009137
simpl,0.008105
simultan,0.005345
sinc,0.015303
singl,0.013654
singular,0.006721
situat,0.015709
size,0.015889
skeptic,0.013436
small,0.012485
smith,0.005404
social,0.055163
societi,0.015979
sociolog,0.024175
sold,0.016159
solut,0.004364
solv,0.004694
sometim,0.013282
soskic,0.014084
souq,0.014767
sourc,0.006417
spatial,0.005963
special,0.006380
specif,0.009155
specul,0.015920
spenc,0.020771
spend,0.005294
sphere,0.017125
spirit,0.005763
spontan,0.019777
squar,0.005404
stabl,0.010039
stake,0.007759
standard,0.003435
stanley,0.006718
start,0.003228
state,0.015623
station,0.005572
statist,0.004184
stigler,0.010126
stiglitz,0.009622
stock,0.024605
store,0.031006
strategi,0.004751
stream,0.006113
street,0.011640
stress,0.010461
structur,0.044703
struggl,0.005347
student,0.004341
studi,0.029652
subfield,0.006566
subject,0.012979
subordin,0.007040
subsidi,0.014210
substitut,0.005668
subsum,0.008525
suggest,0.014283
superimpos,0.009382
supermarket,0.009282
supplant,0.008148
suppli,0.029108
sur,0.008189
surround,0.004671
surviv,0.004399
talk,0.005488
target,0.009498
tax,0.009587
taxat,0.006271
teach,0.004996
team,0.015980
techniqu,0.003919
televis,0.005749
temporari,0.012031
term,0.021692
text,0.008800
th,0.008774
theme,0.022597
themselv,0.003783
theori,0.035878
theorist,0.011890
theoriz,0.006940
therefor,0.006934
thi,0.038158
thing,0.004058
think,0.004423
thought,0.014460
thread,0.008139
threat,0.005350
thu,0.008945
time,0.002256
timeinconsist,0.012783
tool,0.008168
topic,0.008094
topolog,0.007018
total,0.014236
toto,0.012783
trace,0.004891
trade,0.059038
tradeabl,0.023232
tradit,0.012848
transact,0.060414
transform,0.012526
translat,0.008782
transport,0.004372
travel,0.004504
trend,0.004872
tri,0.004014
trinket,0.012433
trobriand,0.012144
trust,0.005576
turn,0.007052
twentieth,0.006017
type,0.036801
unauthor,0.020361
underground,0.007070
underli,0.014532
undermin,0.006761
understand,0.003462
understood,0.004787
undu,0.010180
uneven,0.017178
unifi,0.005339
unintend,0.008599
uniqu,0.004349
unit,0.002572
unoffici,0.007826
unpasteur,0.013402
untax,0.011897
uphold,0.008387
urban,0.005350
use,0.033347
user,0.011317
usual,0.006139
util,0.004614
valu,0.022885
vari,0.022561
variabl,0.004662
varianc,0.007657
variat,0.014344
varieti,0.038299
variou,0.002903
veri,0.002913
version,0.004461
versu,0.005954
view,0.013077
viewpoint,0.006714
violenc,0.006074
virtu,0.006463
virtual,0.005044
volatil,0.006786
volum,0.004369
wa,0.025097
wage,0.016815
walra,0.020416
want,0.004709
wast,0.006118
water,0.003934
way,0.019091
wealth,0.005252
welfar,0.005875
welldevelop,0.008485
wellfunct,0.022983
western,0.003836
wet,0.007615
whenev,0.007177
wherebi,0.017290
wherev,0.008181
whi,0.009386
wholesal,0.039694
wide,0.016426
widespread,0.004900
widest,0.009682
william,0.008174
win,0.005735
withdrawn,0.007902
word,0.003540
work,0.007501
worker,0.004748
world,0.012930
worldwid,0.005174
worthless,0.009622
wrote,0.004345
year,0.005148
zero,0.005364
