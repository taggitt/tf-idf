aaron,0.006067
abandon,0.011195
aberth,0.009526
abil,0.002748
abingdon,0.025292
abl,0.002523
abolish,0.004174
abroad,0.004338
absolut,0.003388
abulafia,0.019052
academ,0.005984
accept,0.002508
access,0.008292
accompani,0.003862
accord,0.001997
acquir,0.009761
acquisit,0.004319
act,0.002283
activ,0.003986
actual,0.007670
acut,0.010486
ad,0.005098
addit,0.006063
administr,0.002793
adopt,0.005513
advanc,0.004934
advent,0.004368
adventur,0.005011
affair,0.006606
affect,0.005221
aftermath,0.004978
age,0.020014
agenc,0.006285
aggress,0.004451
agrarian,0.037914
agre,0.006023
agricultur,0.106893
aid,0.005993
aim,0.005705
alan,0.012705
aldershot,0.014841
ale,0.007564
alfr,0.004093
allen,0.004612
allianc,0.003736
allow,0.007946
alluvi,0.014187
alon,0.003416
alreadi,0.005820
alter,0.010750
altern,0.007700
altogeth,0.009904
amanda,0.007659
analysi,0.002408
anarchi,0.030092
ancient,0.005788
anderson,0.010412
angevin,0.008252
anglonorman,0.008430
anglosaxon,0.043678
ani,0.003521
anim,0.008388
anmol,0.009526
ann,0.004678
annual,0.009206
annum,0.006279
anoth,0.003842
anthoni,0.004717
anthropolog,0.004529
antisemit,0.012883
apocalyps,0.006822
appar,0.006820
appear,0.002313
approach,0.006897
appropri,0.003202
approv,0.003431
arabl,0.018332
arbitrari,0.004313
archaeolog,0.038496
archer,0.012747
area,0.005952
argu,0.015945
aris,0.003259
aristocraci,0.011263
armi,0.003436
armour,0.006010
armstrong,0.012328
arrang,0.003256
arriv,0.009647
ashgat,0.013777
assess,0.003402
assimil,0.005214
assumpt,0.003403
astil,0.042037
attack,0.006441
attempt,0.025065
augustinian,0.014491
author,0.013495
authoris,0.005925
autumn,0.005315
avail,0.007325
away,0.015215
b,0.005292
bad,0.004098
badli,0.010861
bailey,0.013260
balanc,0.003161
baldock,0.009916
baltic,0.016947
ban,0.008049
band,0.004343
bank,0.002888
bare,0.004920
barley,0.012204
barn,0.005978
baron,0.005315
barrel,0.005586
barron,0.007800
barter,0.005819
bartlett,0.006959
base,0.006962
basi,0.004976
basketmak,0.009705
bastard,0.008252
battl,0.007545
bayley,0.008642
bean,0.005597
beard,0.006441
becam,0.046237
becaus,0.003626
becom,0.009641
befor,0.014400
began,0.050511
begin,0.004697
begun,0.019149
bell,0.004663
bellpit,0.021018
belong,0.003403
ben,0.005151
benefit,0.005846
berkeley,0.004704
best,0.005309
beverley,0.008308
bibliographi,0.003503
biggest,0.009044
birdhunt,0.010509
birrel,0.009526
birth,0.003549
bishop,0.004930
black,0.048763
blacksmith,0.007367
blackwel,0.005035
blair,0.025907
blanchard,0.006996
blast,0.005896
block,0.003588
blood,0.003922
bloomeri,0.009002
bolton,0.015600
bond,0.003440
book,0.018314
boom,0.018279
border,0.007096
bork,0.018469
borough,0.022027
borrow,0.007995
boserup,0.009705
boston,0.004441
bound,0.003604
boydel,0.061658
brabant,0.007918
bread,0.011274
break,0.003320
breed,0.004830
bridg,0.015918
brill,0.012704
bring,0.005989
brink,0.006657
bristol,0.018961
britain,0.016967
british,0.002689
britnel,0.058231
broader,0.004016
broadli,0.004032
broken,0.004016
broker,0.005289
brought,0.029692
brown,0.012261
bruce,0.004949
build,0.012745
built,0.012011
bulk,0.008540
bullion,0.006822
buoyant,0.007269
burgag,0.009112
burst,0.005425
burton,0.006331
busi,0.005430
buy,0.011431
buyer,0.004807
bypass,0.005586
byproduct,0.005346
c,0.009221
caen,0.008722
calai,0.008497
calamin,0.009002
california,0.007390
cambridg,0.070293
came,0.015826
campaign,0.014486
cannib,0.006563
cannon,0.005603
cantor,0.031980
cap,0.004914
capit,0.005247
captur,0.003312
care,0.003030
carl,0.004027
carlisl,0.007763
carolin,0.006320
carpent,0.006310
carri,0.002600
carrier,0.004845
cart,0.012683
carta,0.029172
carter,0.005697
cartwright,0.007420
carucag,0.009112
case,0.010085
cash,0.024193
castiron,0.008308
castl,0.016411
cat,0.005117
catastroph,0.004787
catch,0.009713
cathedr,0.011430
cattl,0.009450
ceas,0.020375
centr,0.036489
central,0.011307
centralis,0.006299
centuri,0.182306
certain,0.002208
certainli,0.004605
challeng,0.011527
champagn,0.007918
chancellor,0.005721
chang,0.012732
channel,0.003982
chao,0.009337
charcoal,0.012704
charg,0.003113
charl,0.003089
charter,0.065101
chase,0.010970
cheaper,0.010389
chester,0.006630
children,0.003318
chingley,0.010509
choic,0.009510
chose,0.009076
chri,0.005430
christoph,0.016912
church,0.051033
circul,0.016885
circumnavig,0.006822
circumst,0.003706
cistercian,0.031510
citi,0.027862
civic,0.005255
claim,0.002449
clarendon,0.005867
class,0.032111
clear,0.003002
clearli,0.003627
clergi,0.021982
clergyman,0.007420
cleric,0.005839
clock,0.005049
cloth,0.080139
coal,0.018818
coalfield,0.008430
coast,0.018411
coastal,0.008796
cod,0.006616
code,0.003121
cog,0.007135
coin,0.030069
coinag,0.011443
cold,0.003845
collaps,0.010662
colleagu,0.004361
collect,0.009414
colonis,0.005465
combin,0.016105
come,0.011538
commenc,0.009761
commerc,0.012061
commerci,0.012364
commiss,0.003212
common,0.009755
commun,0.037976
commut,0.005360
compani,0.020382
compar,0.002386
comparison,0.003383
compel,0.005042
compet,0.003326
competit,0.006345
complex,0.002361
complic,0.003656
compris,0.006601
concern,0.002265
concert,0.005440
condemn,0.004544
condit,0.009030
conduct,0.005531
confer,0.006352
confirm,0.003478
conflict,0.006010
confront,0.004457
conqueror,0.017799
conquest,0.004347
consequ,0.019206
conserv,0.003202
consid,0.003763
consider,0.024405
consist,0.002159
constitut,0.002547
constrain,0.004691
construct,0.007496
consult,0.003726
consum,0.022490
consumpt,0.026344
contain,0.002314
contemporari,0.009468
contin,0.004158
continu,0.035192
contract,0.009710
contrast,0.008177
contribut,0.004736
control,0.015007
conveni,0.004308
cooper,0.003084
copper,0.004480
corn,0.005046
cornish,0.016099
cornwal,0.013613
coronet,0.009002
corpor,0.003165
coss,0.029750
cost,0.014184
costli,0.005028
cotswold,0.008901
counter,0.004363
countri,0.012740
countrysid,0.005289
court,0.027545
coventri,0.016396
cover,0.005313
craft,0.014561
craftsmen,0.025199
creat,0.025658
creation,0.017622
credit,0.003122
creek,0.006576
crise,0.004978
crisi,0.026638
critic,0.002373
critiqu,0.004099
croom,0.008252
crop,0.011968
cross,0.007043
crouch,0.015920
crowd,0.005028
crown,0.033434
crusad,0.005475
crush,0.004936
ct,0.005586
cultiv,0.037091
cumberland,0.007504
currenc,0.007071
current,0.002065
curtail,0.005853
custom,0.016314
customari,0.010757
d,0.005224
damag,0.003482
dan,0.005226
danni,0.007293
danzig,0.007838
david,0.020064
day,0.004869
dc,0.004468
dean,0.015779
death,0.050870
decad,0.011732
decentralis,0.006576
decis,0.002720
declin,0.027573
decor,0.010477
deepsea,0.007135
defeat,0.003817
defenc,0.003928
defin,0.002214
deflat,0.005925
degre,0.002642
delhi,0.005569
demand,0.031689
demesn,0.131202
demis,0.005703
demograph,0.004332
demographi,0.021703
demolit,0.007838
depart,0.002829
depend,0.006387
deplet,0.010071
deposit,0.007640
depress,0.016036
descend,0.003905
desir,0.003036
despit,0.024625
destroy,0.006748
determinist,0.005151
develop,0.020979
devon,0.023881
diana,0.006538
diarmaid,0.008722
did,0.009581
die,0.012367
diet,0.019085
differ,0.003421
difficult,0.011257
difficulti,0.006969
diminish,0.026213
direct,0.010713
directli,0.007778
disast,0.013373
discont,0.011085
discontinu,0.004899
discoveri,0.003022
discredit,0.005614
discuss,0.002632
diseas,0.003380
disloc,0.006476
dispar,0.004958
disrupt,0.016632
dissolut,0.004862
distinctli,0.005896
distribut,0.002662
dite,0.009526
divers,0.003103
divid,0.007676
divis,0.002796
dobbin,0.019052
document,0.009345
dodd,0.013882
dog,0.004830
domesday,0.032017
domest,0.010030
domin,0.021242
dori,0.007447
doubl,0.017862
doubt,0.004164
dougla,0.004768
dozen,0.004513
drain,0.011128
dramat,0.003644
draper,0.007800
draw,0.006615
drawn,0.003808
drew,0.008894
drive,0.003466
driven,0.003756
droitwich,0.010509
drop,0.003541
drought,0.005268
dunstabl,0.010509
dure,0.055724
durham,0.027101
duti,0.015359
dwell,0.005709
dweller,0.006775
dy,0.007156
dyer,0.014951
e,0.007844
earli,0.038861
earlier,0.002870
easier,0.004159
east,0.014934
easter,0.006173
eastern,0.012898
eat,0.009067
econom,0.112679
econometr,0.005715
economi,0.148574
ed,0.135981
educ,0.002474
edward,0.035340
effect,0.007549
effici,0.003041
effort,0.013452
eileen,0.007800
elabor,0.004042
elbl,0.042037
elimin,0.003359
elit,0.008627
elsewher,0.004004
emerg,0.012449
empir,0.002710
employ,0.005153
emporium,0.008252
enact,0.004306
encourag,0.009579
encroach,0.005986
encyclopaedia,0.006137
end,0.046729
enforc,0.018210
engag,0.006152
england,0.300023
english,0.259743
englishheld,0.009916
enjoy,0.011095
enshrin,0.006363
ensu,0.009499
enter,0.002840
entir,0.002519
entireti,0.005553
epidem,0.016177
equal,0.005291
equival,0.003129
ergot,0.009234
ermin,0.009526
especi,0.018046
essay,0.011294
essenti,0.002700
establish,0.022805
estat,0.049626
ester,0.006538
estim,0.010935
europ,0.026342
european,0.004839
event,0.010112
eventu,0.008283
everi,0.002430
everyday,0.004149
evid,0.007947
evolut,0.003154
evolv,0.003188
exacerb,0.005378
exagger,0.005416
exampl,0.006959
excess,0.007508
exchang,0.005492
exchequ,0.014445
execut,0.002942
exempt,0.010099
exet,0.014538
exhaust,0.004542
exist,0.009659
exot,0.005174
expand,0.013740
expans,0.009833
expect,0.005338
expel,0.004683
expenditur,0.008590
expens,0.020818
explicitli,0.007909
exploit,0.018082
export,0.057020
extend,0.005124
extens,0.010493
extent,0.003196
extort,0.006671
extract,0.007479
extrem,0.008360
f,0.008584
fabric,0.004859
face,0.005831
factor,0.002463
fail,0.005841
failur,0.003380
fair,0.086563
fall,0.010607
falter,0.006906
famin,0.062429
famou,0.012837
far,0.010536
farm,0.046512
farmer,0.008223
fashion,0.008177
fastexpand,0.010175
fatal,0.005049
favour,0.003820
featur,0.005254
feed,0.004459
fell,0.026264
fen,0.033472
fertil,0.004072
fertilis,0.006822
feudal,0.090333
feudalismu,0.010509
field,0.023336
fierc,0.010684
fifteenth,0.024238
fifth,0.004137
fight,0.003645
final,0.007538
financ,0.015391
financi,0.027733
fine,0.033225
finish,0.004470
firmli,0.005077
fiscal,0.004404
fish,0.057993
fisheri,0.005280
fix,0.009250
flander,0.006513
flaw,0.004952
flee,0.004981
fleet,0.014013
fletcher,0.006906
flour,0.006259
flourish,0.004275
fluctuat,0.004338
focu,0.011400
focus,0.010817
follow,0.009970
food,0.017938
forbidden,0.005136
forc,0.010410
foreign,0.029746
forest,0.135831
forey,0.010509
forg,0.005167
form,0.012702
formalis,0.012204
formerli,0.007782
fortif,0.005846
forward,0.007375
foss,0.008049
fought,0.004406
foundat,0.002708
fourteen,0.005649
fourteenth,0.018748
fourweek,0.009112
fragil,0.005569
franc,0.029739
frank,0.008135
fratern,0.006373
freder,0.006173
free,0.004745
freedom,0.006558
french,0.008387
frequent,0.005840
fresh,0.018664
freshwat,0.016471
fryde,0.038821
fuel,0.003849
fuller,0.006084
fulli,0.003131
function,0.004253
fund,0.005728
fundament,0.002660
fungu,0.006790
fur,0.011481
furnac,0.012704
fuse,0.005114
g,0.002810
gail,0.007659
gain,0.002527
gap,0.004043
gascon,0.009526
gasconi,0.017802
gate,0.005028
gather,0.003621
gedd,0.007800
gegenwart,0.009371
geld,0.034889
gem,0.006501
gener,0.008956
gentri,0.060568
geographi,0.003823
gerald,0.005387
german,0.005867
germani,0.012511
gerrard,0.008308
gillingham,0.010509
givenwilson,0.021018
glastonburi,0.010509
godwinson,0.009916
goldsmith,0.012641
good,0.027715
govern,0.035558
gradual,0.010018
grain,0.013540
grang,0.018469
grant,0.003184
graze,0.005832
great,0.042143
greater,0.002616
greatli,0.003401
greenwood,0.005819
grenvil,0.024149
grevil,0.008722
grew,0.024300
grind,0.012599
ground,0.003022
group,0.013117
grow,0.034629
grown,0.008001
growth,0.055218
gttingen,0.006790
guild,0.097806
h,0.002885
ha,0.001385
half,0.014297
hambledon,0.037486
hamilton,0.010311
hamlet,0.006941
hand,0.002573
hans,0.010509
hanseat,0.014400
hard,0.006819
harder,0.005163
harlow,0.020325
harmondsworth,0.023514
harold,0.004985
harper,0.005351
harri,0.004368
harriss,0.008097
harsh,0.009885
harvest,0.013815
harwich,0.009705
hast,0.006441
hatcher,0.050208
hatfield,0.017802
hay,0.005799
heath,0.006838
heavili,0.010047
held,0.007368
helm,0.007015
help,0.004548
henley,0.008049
henri,0.046875
heyday,0.006744
hi,0.013038
hick,0.012726
hide,0.004917
hierarchi,0.004233
high,0.012620
higher,0.002469
highqual,0.012460
highstatu,0.008567
hillabi,0.009916
hilton,0.007533
hinton,0.007245
hire,0.004535
histor,0.011667
histori,0.022493
historian,0.014002
historiographi,0.010544
hodgett,0.009112
hold,0.012511
holder,0.004722
homer,0.005826
honnecourt,0.009916
honour,0.014697
hors,0.013462
hosebondri,0.010509
hospit,0.004096
hospital,0.008146
hous,0.013933
household,0.012397
howev,0.003371
hub,0.010785
huge,0.027043
hull,0.005832
humanist,0.005046
hundr,0.025250
hunt,0.016770
husband,0.005411
huscroft,0.010509
ian,0.005129
iceland,0.005342
icknield,0.010509
idea,0.002331
ii,0.021749
iii,0.023836
illeg,0.004185
illustr,0.007085
imag,0.003198
immedi,0.009161
immens,0.009983
immigr,0.004133
impact,0.028468
implic,0.010858
import,0.038978
impos,0.007145
impract,0.005994
imprison,0.004939
improv,0.015337
inbetween,0.007800
incens,0.007073
includ,0.031725
incom,0.019090
increas,0.055933
increasingli,0.058186
incred,0.005940
inde,0.003532
independ,0.006400
indigen,0.008283
indirect,0.009027
individu,0.006428
industri,0.016597
ineffect,0.005255
inevit,0.004576
inflat,0.003912
inflex,0.006872
influenc,0.006864
influx,0.005360
inherit,0.007704
initi,0.013542
inland,0.004985
innov,0.007196
instanc,0.002813
instead,0.019465
institut,0.008792
instrument,0.003198
insuffici,0.009419
integr,0.002506
intens,0.003450
intensifi,0.010184
interlop,0.009916
intern,0.010922
interpret,0.008314
introduc,0.007310
introduct,0.011015
invad,0.004154
invas,0.071971
invest,0.011830
involv,0.014096
ipswich,0.009526
iron,0.037205
ironproduc,0.009916
ironwork,0.022427
isaac,0.004496
isbn,0.137520
isol,0.003372
issu,0.011145
italian,0.007795
iv,0.004509
ivana,0.009002
ive,0.013645
ivori,0.005643
j,0.010435
jame,0.002929
jane,0.005475
janet,0.006002
janken,0.010509
jean,0.004308
jenni,0.006888
jew,0.029302
jewish,0.077516
joe,0.005955
john,0.040167
jone,0.004612
jordan,0.005234
journal,0.002671
juli,0.003000
jurisdict,0.008432
jurist,0.006128
k,0.006298
kathryn,0.007659
keen,0.011443
kenneth,0.004661
kept,0.003742
kermod,0.009371
key,0.015860
kill,0.003429
king,0.076098
kingdom,0.002763
knight,0.005202
known,0.005206
kowalski,0.008308
l,0.009172
laborator,0.010509
labour,0.063292
lack,0.002648
laid,0.003920
land,0.118286
landbas,0.013847
landlord,0.012102
landown,0.104516
landscap,0.030314
langdon,0.025491
larg,0.023794
larger,0.016448
largest,0.002868
late,0.038473
later,0.028917
laurenc,0.006363
lavish,0.006906
law,0.027198
lawler,0.016994
lawrin,0.010509
le,0.004137
lead,0.039371
leagu,0.008522
lean,0.005672
leas,0.005243
leav,0.005882
led,0.004482
lee,0.004722
left,0.008305
legal,0.017195
legisl,0.003179
legum,0.006888
leiden,0.012169
lent,0.011481
leonard,0.016053
level,0.018690
levi,0.029284
lex,0.006888
life,0.008930
like,0.003710
limit,0.002120
lincoln,0.035378
lincolnshir,0.016617
link,0.006040
literari,0.004355
littl,0.013290
live,0.013473
livelihood,0.006018
liveri,0.008808
liverpool,0.006550
livestock,0.004902
lloyd,0.010711
loan,0.008058
local,0.054100
localis,0.006671
locat,0.012767
london,0.131590
longerterm,0.006790
lord,0.024662
loss,0.015187
lost,0.003017
lot,0.004277
lothian,0.008901
low,0.010646
lower,0.010264
lucr,0.005727
luxuri,0.020841
m,0.019869
macculloch,0.008642
macmillan,0.004744
magna,0.026575
magnat,0.007073
maidenhead,0.010509
main,0.004397
maintain,0.004843
mainten,0.004166
maitland,0.016396
major,0.044217
make,0.019425
malthusian,0.006959
manag,0.019897
manchest,0.021801
mandat,0.008653
mani,0.051797
manipul,0.003718
manor,0.043336
manori,0.030637
manpow,0.006102
manufactur,0.043052
march,0.002879
marco,0.006026
mari,0.004012
mark,0.008364
market,0.028594
marriag,0.004390
marsh,0.011911
martin,0.003675
martinontorr,0.021018
marxist,0.009429
maryann,0.008901
mass,0.002847
massacr,0.005063
master,0.003706
masteri,0.006259
match,0.003693
materi,0.012167
matter,0.002612
matur,0.004040
mcfarlan,0.008642
meanwhil,0.004199
meat,0.014456
mechanis,0.007504
mediev,0.174086
meet,0.014049
member,0.004431
mend,0.006671
mercatoria,0.008497
mercenari,0.005940
merceri,0.021018
merchant,0.144356
merfold,0.010509
metal,0.021686
metallica,0.008004
metalwork,0.013153
method,0.006452
michael,0.009587
middl,0.025768
middlemen,0.014312
midfifteenth,0.009234
midland,0.007342
midmediev,0.021018
midth,0.004284
militari,0.016855
miller,0.014544
million,0.013638
mind,0.003274
minor,0.006427
mint,0.034109
mirror,0.004689
mitig,0.004991
mixtur,0.008683
mode,0.003909
model,0.008652
modern,0.006248
modest,0.010141
monarch,0.013758
monast,0.018007
monasteri,0.015829
money,0.049656
moneylend,0.015676
monk,0.005136
monnet,0.016861
monopoli,0.004449
moorland,0.010509
mortal,0.004641
motiv,0.003392
mott,0.007918
munro,0.008146
murag,0.010509
murrain,0.021018
myer,0.006513
myrdal,0.008198
n,0.003118
natali,0.016194
nation,0.005772
natur,0.003707
navig,0.004336
near,0.002855
nearli,0.006049
necessari,0.002767
necessarili,0.003399
need,0.006262
neopositivist,0.008642
network,0.008709
neutral,0.003801
new,0.051188
newbridg,0.010509
newcastl,0.007222
newer,0.005099
nigel,0.013153
nightingal,0.007317
nobil,0.059580
nobl,0.031610
nomin,0.003624
nonetheless,0.026063
norfolk,0.006855
normal,0.002767
norman,0.141894
normandi,0.013550
north,0.019301
northampton,0.007800
northeast,0.004946
northern,0.006492
northsouth,0.006430
northumberland,0.008430
northwest,0.004749
norway,0.004880
norwich,0.023290
note,0.004004
novemb,0.003114
number,0.031270
numer,0.005145
oat,0.006729
obstacl,0.005011
occup,0.003618
occupi,0.003481
occur,0.011031
ocean,0.003573
odd,0.004899
odel,0.009002
oexl,0.021018
offer,0.002582
offici,0.010545
oil,0.003466
old,0.014366
older,0.021865
onc,0.005189
oner,0.007763
onethird,0.005276
onli,0.015985
onward,0.022011
open,0.004754
oper,0.013144
opportun,0.006560
opt,0.005402
option,0.003623
orator,0.008567
order,0.028705
ordin,0.010243
ore,0.009950
organis,0.006575
origin,0.001924
otherwis,0.003303
oto,0.008497
outbreak,0.014176
output,0.010873
outsid,0.002649
overal,0.003178
overlap,0.003859
overpopul,0.006373
overse,0.010357
oversea,0.004141
overtook,0.006941
owe,0.004226
owner,0.003928
ownership,0.004078
oxen,0.029471
oxford,0.019576
oxhorn,0.010509
paid,0.010622
paint,0.004410
pallis,0.017616
palm,0.005411
pamela,0.006775
paper,0.002865
parish,0.011638
park,0.008117
parliament,0.024516
partial,0.003194
particular,0.013041
particularli,0.017047
partner,0.003796
pass,0.008093
past,0.002754
pastur,0.019540
paten,0.009371
patricia,0.005986
patronag,0.005655
paus,0.006441
pavag,0.010509
pax,0.007073
pay,0.019452
payabl,0.006210
payment,0.007485
peac,0.003349
peak,0.007621
pearson,0.016411
peasant,0.130737
peasantri,0.032505
peatcut,0.010509
penguin,0.026112
penni,0.012620
pennin,0.009526
peopl,0.007658
perambul,0.010509
percentag,0.007739
perform,0.002441
period,0.072218
persecut,0.005005
person,0.002272
perspect,0.003107
peter,0.006333
pewter,0.041840
pewterwork,0.010509
philip,0.004268
physic,0.002272
pick,0.009040
pierr,0.004470
pig,0.010357
pilkinton,0.010509
pipe,0.005812
piraci,0.012310
place,0.007855
plagu,0.010003
plan,0.005259
play,0.012536
player,0.009089
plough,0.020988
plung,0.005940
poach,0.007178
point,0.002014
polit,0.013077
poll,0.009666
pond,0.018411
pontag,0.010509
poorer,0.028364
popul,0.042002
popular,0.007453
popularis,0.006137
port,0.023544
portsmouth,0.007564
posit,0.006174
possess,0.009361
possibl,0.002031
post,0.003370
postan,0.079275
postinvas,0.009371
postplagu,0.010509
postwar,0.004722
potenti,0.005156
pound,0.004991
poundag,0.010509
power,0.014264
practic,0.014913
pray,0.006192
pre,0.005785
preciou,0.005226
predominantli,0.004420
preepidem,0.010509
prefer,0.003049
prenorman,0.010509
present,0.005973
preserv,0.003356
press,0.067966
pressur,0.011990
prestigi,0.005660
previou,0.015014
previous,0.006071
price,0.044219
primari,0.002628
primarili,0.027219
princeton,0.008792
princip,0.006221
prior,0.009023
prioritis,0.007727
privat,0.002855
privileg,0.004544
probabl,0.002857
problem,0.002129
proceed,0.007288
process,0.011294
produc,0.031078
product,0.049810
profess,0.004161
profit,0.027524
profound,0.004542
profoundli,0.005766
progress,0.005428
properti,0.019288
propos,0.002456
prosecut,0.005383
prosper,0.028816
protect,0.007962
prove,0.009219
provid,0.015901
provinc,0.003681
provinci,0.004816
public,0.002005
punish,0.004380
purchas,0.010160
pure,0.006297
quantiti,0.012973
r,0.007994
raban,0.010509
rabbit,0.006164
rahman,0.006744
rain,0.010113
rais,0.005874
ramsay,0.035170
ran,0.013373
rang,0.004780
rapid,0.013804
rapidli,0.010071
rare,0.003277
raw,0.016479
razor,0.006230
reach,0.012832
read,0.002193
reaffirm,0.005994
real,0.005242
realli,0.004063
reason,0.004533
reassign,0.007693
rebel,0.013765
rebellion,0.027054
receiv,0.004825
recipi,0.004874
reclaim,0.005896
record,0.018634
recov,0.003887
recoveri,0.008408
reduc,0.009569
reduct,0.010901
refer,0.001262
refin,0.007923
reflect,0.010856
region,0.020353
regul,0.008969
rehren,0.021018
reigat,0.010509
reign,0.026201
reinforc,0.004300
reinstat,0.005740
reinvest,0.006201
rel,0.013586
relat,0.003405
relationship,0.002410
relev,0.003303
religi,0.009677
remain,0.052217
remaind,0.009373
remint,0.009526
renaiss,0.004175
rent,0.087530
reoccur,0.008252
repair,0.004496
replac,0.007720
report,0.002427
repres,0.008286
requir,0.005964
rescu,0.005011
resent,0.010890
resourc,0.012783
respond,0.010218
respons,0.006571
rest,0.002958
restor,0.003571
result,0.028146
resurg,0.005402
retail,0.004567
retain,0.006642
retreat,0.004877
revenu,0.051581
revers,0.006953
review,0.002829
revolt,0.045979
revolut,0.003128
reyerson,0.010509
richard,0.018719
richardson,0.006146
richer,0.012069
ride,0.005490
right,0.018130
rise,0.017789
risen,0.010639
river,0.013811
road,0.003668
robert,0.011107
rodney,0.006352
role,0.015486
roll,0.004712
romney,0.008198
ronald,0.004511
roof,0.006146
room,0.004096
rose,0.015329
rotat,0.004180
rouen,0.015676
rout,0.007604
routin,0.004533
routledg,0.018001
rowena,0.010509
royal,0.106631
royston,0.010509
rule,0.009221
ruler,0.012658
run,0.011101
ruprecht,0.008901
rural,0.032674
russel,0.004426
rye,0.013677
s,0.037614
sack,0.010510
sage,0.004978
said,0.002594
sale,0.014716
salt,0.021634
sandra,0.006806
saw,0.033995
saxon,0.006759
scale,0.005716
scarborough,0.007659
scarlet,0.007156
scaveng,0.006465
scholar,0.003207
scholarship,0.004515
school,0.002569
scienc,0.002124
scope,0.003712
scutag,0.009112
sd,0.006729
sea,0.003222
seaport,0.006373
second,0.002072
sector,0.006621
secur,0.005366
seek,0.005667
seen,0.007433
seiz,0.004341
selfgovern,0.005008
sell,0.003572
selv,0.006996
semicircl,0.008097
sent,0.003500
separ,0.004614
septemb,0.003045
sequenc,0.007012
serf,0.013430
serfdom,0.013315
seriou,0.007075
serv,0.002513
servant,0.004868
servic,0.017224
servitud,0.013489
set,0.011738
settl,0.007178
settlement,0.014741
seven,0.003314
sever,0.007329
shadow,0.004920
shaken,0.006959
share,0.002460
sharp,0.008536
sharpli,0.009937
sheep,0.025443
sheriff,0.007420
shift,0.009181
ship,0.014333
shipbuild,0.024805
shipment,0.005799
shook,0.014030
short,0.008078
shortag,0.009172
shrank,0.006310
shrink,0.022150
shrunk,0.013744
sick,0.009917
signifi,0.005095
signific,0.024680
significantli,0.025930
silk,0.005247
silver,0.039549
similar,0.004197
simon,0.004222
sinc,0.001757
singl,0.007056
sink,0.005234
sixti,0.005346
size,0.019159
skinner,0.018153
slaughter,0.005832
slave,0.008306
slightli,0.003809
slow,0.003628
slowli,0.004051
slump,0.006289
small,0.006452
smaller,0.020920
smelt,0.006525
social,0.008943
societi,0.011010
sociolog,0.008329
soil,0.007964
sold,0.003711
somerset,0.014735
sometim,0.002288
sourc,0.002210
south,0.013762
southeast,0.004302
southwest,0.029599
special,0.015387
specialist,0.004361
specif,0.002102
spectacl,0.006576
spend,0.003648
spent,0.003884
spice,0.017000
sporad,0.005608
spread,0.009320
sprung,0.006888
spur,0.009872
st,0.005666
stabil,0.003277
stabl,0.003458
stacey,0.008198
stamford,0.016099
standard,0.004734
stanford,0.008165
stannari,0.010509
stapl,0.005564
start,0.004449
static,0.004496
statu,0.005870
statut,0.005163
steadili,0.009272
steelyard,0.009705
stem,0.007901
stenton,0.019833
step,0.002980
stephen,0.003904
sterl,0.005839
stimul,0.003933
stock,0.006781
stone,0.003925
stoni,0.006977
store,0.014243
stour,0.010509
strain,0.004368
stratford,0.009371
stream,0.008425
street,0.020053
stress,0.007208
strict,0.004084
strike,0.003909
strip,0.013836
structur,0.006160
struggl,0.011053
student,0.002991
studi,0.013002
subject,0.006707
subordin,0.004850
subsequ,0.002742
subsidis,0.006822
subsist,0.004839
substanti,0.006525
success,0.011654
suffer,0.006415
suffici,0.003249
sugarcandi,0.010509
suggest,0.007381
suitabl,0.003901
summer,0.004066
sumptuari,0.008808
superimpos,0.006465
supplement,0.008703
suppli,0.014326
support,0.014628
suppress,0.008049
surfac,0.003182
surg,0.005425
surpris,0.004621
survey,0.003378
surviv,0.006062
sussex,0.006630
sutton,0.006906
swanson,0.007877
swedberg,0.008567
sweden,0.004390
swell,0.005805
taillag,0.010509
tait,0.007447
taken,0.007720
tallag,0.026703
task,0.006521
tax,0.072670
taxat,0.051855
technic,0.006251
techniqu,0.016206
technolog,0.002534
templar,0.015600
temporari,0.012435
temporarili,0.004845
tenant,0.006110
tend,0.005772
tension,0.003992
tenur,0.005328
term,0.006643
text,0.003031
th,0.143091
thame,0.006806
thank,0.009666
thcenturi,0.008105
thegn,0.010509
theme,0.003892
themselv,0.010428
theori,0.002060
thereaft,0.004595
therefor,0.002388
thetford,0.010509
thi,0.046012
thilo,0.009916
think,0.006096
thirteenth,0.023702
thirti,0.009477
thought,0.004981
thousand,0.003099
thrive,0.004865
thrown,0.005853
tighten,0.005860
timber,0.005263
time,0.020216
tin,0.054854
tith,0.065857
togeth,0.004847
toler,0.004447
toll,0.005603
tonn,0.005812
took,0.005491
tool,0.002814
toronto,0.010920
torrenti,0.008901
total,0.002452
town,0.155201
track,0.003888
tract,0.005011
trade,0.129670
trader,0.004326
tradit,0.004426
transfer,0.003007
transform,0.005754
transmut,0.006059
transport,0.009039
travel,0.006207
treacl,0.009526
treasuri,0.004671
tree,0.007240
trench,0.005933
trend,0.013428
triall,0.009526
tudor,0.006888
tunnel,0.005306
turn,0.017009
twelfth,0.012292
twenti,0.008646
twentyf,0.006407
twothird,0.004816
tyne,0.008146
typic,0.002357
uk,0.061210
ultim,0.003040
unabl,0.003652
uncertain,0.009292
uncommon,0.005387
underli,0.003337
undermin,0.004658
underpin,0.005125
understand,0.011928
unequ,0.005460
unfre,0.040990
unimport,0.006630
unit,0.001772
univers,0.038228
universalist,0.006643
unknown,0.003638
unpick,0.010509
unpopular,0.005586
unsuccess,0.004520
uplift,0.006289
upper,0.003549
upward,0.004892
urban,0.029492
use,0.033031
useless,0.005759
usual,0.016920
usuri,0.020279
utensil,0.007034
valley,0.008129
valuabl,0.004126
vandenhoeck,0.009371
vari,0.007773
variat,0.003294
varieti,0.005278
variou,0.006002
vast,0.007058
veri,0.008028
vessel,0.008558
vestig,0.006441
vi,0.004998
victorian,0.011286
view,0.004505
vigor,0.005085
vii,0.010693
vilifi,0.008049
villag,0.020081
villard,0.009002
violenc,0.008371
visit,0.007062
volum,0.009032
wa,0.159965
wage,0.034759
wainscot,0.010509
wale,0.004892
walker,0.011075
wall,0.003721
walnut,0.007533
walter,0.004192
war,0.032458
warfar,0.004457
warren,0.010265
wash,0.005500
washington,0.003869
wasteland,0.007564
water,0.005422
watermil,0.016617
waterpow,0.015920
watl,0.008901
wave,0.003544
wax,0.005971
way,0.018793
weak,0.003491
weaker,0.005005
weald,0.035604
wealth,0.018097
wealthi,0.022931
wealthier,0.019931
wealthiest,0.006155
weapon,0.007806
wear,0.004763
weather,0.008620
weav,0.011866
weaver,0.006441
welfar,0.004048
wellestablish,0.005903
welsh,0.018838
west,0.012071
westport,0.006576
westward,0.006034
wet,0.005247
wheat,0.019993
wherebi,0.003971
wherev,0.005637
whiggish,0.016008
whilst,0.013620
wide,0.011318
widen,0.005470
widespread,0.016884
william,0.039429
winchest,0.022427
windmil,0.007200
windsor,0.007659
wine,0.019784
winter,0.013066
wokingham,0.009112
wood,0.024334
woodbridg,0.060496
woodland,0.025584
wool,0.069039
woolgar,0.008808
woollen,0.007877
work,0.020676
worker,0.019630
workmanship,0.008146
wornout,0.008430
worship,0.004763
worst,0.004848
writer,0.003607
written,0.002782
x,0.003112
xi,0.005416
xii,0.006341
yale,0.004804
yarmouth,0.008808
year,0.054988
yield,0.006867
york,0.021656
yorkshir,0.022692
