abil,0.003791
ablebodi,0.010475
abov,0.007055
abraham,0.006471
absent,0.006715
absolut,0.004673
access,0.015250
accord,0.002755
accru,0.008045
acocella,0.011628
act,0.006298
action,0.016928
actor,0.006351
actual,0.017632
ad,0.010549
adapt,0.008775
addit,0.016725
address,0.016597
administr,0.003852
advanc,0.003403
advantag,0.004277
advertis,0.006474
affect,0.014403
afterlif,0.008428
aggreg,0.011654
aim,0.003934
air,0.021164
alaska,0.008335
algorithm,0.005524
allianc,0.005153
alloc,0.005498
allow,0.008220
allvolunt,0.012569
alon,0.004712
altern,0.007081
altruism,0.033572
altruist,0.061333
alway,0.007410
american,0.012488
amnesti,0.007944
analys,0.005387
analysi,0.009964
analyz,0.004631
andrew,0.005596
ani,0.021856
anim,0.003856
anoth,0.018551
answer,0.004906
anticommon,0.012738
anyon,0.006007
apart,0.005049
appeal,0.005131
appear,0.003191
appl,0.007175
appli,0.005891
applic,0.009872
area,0.005473
argu,0.014662
argument,0.004629
aris,0.008990
arkyd,0.014495
armi,0.004739
arsen,0.007824
arthur,0.005620
asham,0.010433
aspect,0.003621
associ,0.005383
assum,0.007634
assur,0.031390
asteroid,0.007962
atmospher,0.005397
attach,0.010433
attack,0.004442
attempt,0.003143
attract,0.008867
attribut,0.004283
audienc,0.006219
author,0.009307
authorship,0.008853
autom,0.006166
avail,0.016841
averag,0.004115
away,0.004197
bad,0.011305
badli,0.007490
bank,0.003984
bargain,0.007464
base,0.012004
basic,0.003342
bat,0.008153
beauti,0.006313
becaus,0.035015
becom,0.010638
behav,0.011819
behavior,0.031179
behaviorfor,0.013678
belief,0.013342
believ,0.006872
beneficiari,0.031637
benefit,0.100798
benefitcost,0.012416
berkeley,0.006489
besley,0.025852
best,0.007323
better,0.014987
bid,0.007079
bind,0.005737
biolog,0.015762
biologist,0.006216
block,0.004949
blood,0.005410
board,0.004793
border,0.004894
bread,0.007775
brief,0.005266
broadcast,0.019340
bu,0.008133
buchanan,0.009107
build,0.017579
builder,0.008083
built,0.004141
bundl,0.007387
busi,0.022472
buy,0.010512
buyout,0.010162
cabl,0.007243
calcul,0.004239
california,0.005096
came,0.003638
cannib,0.009053
capit,0.003619
car,0.005880
card,0.006322
career,0.005458
carnegi,0.008036
caron,0.011460
case,0.025041
catalyt,0.008540
categori,0.008492
cathedr,0.007883
caus,0.011880
centuri,0.014287
certain,0.009137
challeng,0.003975
chanc,0.005356
chang,0.007526
chapter,0.015559
character,0.004220
characterist,0.023013
charg,0.008588
chariti,0.014441
chat,0.009574
children,0.004576
choic,0.008744
christian,0.017250
church,0.005027
cinema,0.007470
circumst,0.005112
citi,0.003843
citizen,0.013662
civil,0.003817
claim,0.006756
classic,0.007353
classif,0.004910
classifi,0.009003
clean,0.019543
clear,0.016567
close,0.002977
closer,0.005506
club,0.083898
clubgood,0.014495
coas,0.096759
coasian,0.066934
code,0.004306
coercion,0.008732
collabor,0.020448
collect,0.016231
coloni,0.004601
combat,0.005841
combin,0.006346
come,0.006365
commit,0.004664
commodit,0.011460
common,0.024219
commonli,0.003695
commonpool,0.025138
commun,0.023281
communist,0.005489
compani,0.003514
compar,0.003291
competit,0.004376
competitor,0.013788
complement,0.013124
complet,0.003073
complex,0.003256
complic,0.005043
comput,0.003687
conceiv,0.005843
concept,0.009317
conceptu,0.010713
concis,0.006778
concret,0.006097
condit,0.006228
conduct,0.003814
confus,0.005009
congest,0.018143
conscriptionto,0.014495
consid,0.023360
consider,0.003740
consist,0.002978
constitut,0.003514
construct,0.010339
consum,0.048749
consumpt,0.051911
contagi,0.009841
contain,0.003192
contend,0.006415
conting,0.006464
continu,0.010787
contract,0.044647
contrast,0.015038
contribut,0.032667
contributor,0.006510
control,0.005914
controversi,0.004385
convent,0.004309
convert,0.004537
convinc,0.005924
cooper,0.017015
copyright,0.047198
corpor,0.013100
correct,0.008964
cost,0.136959
costli,0.013872
countermeasur,0.010127
countri,0.011715
cours,0.007758
court,0.004221
cover,0.010993
cowen,0.010865
creat,0.016334
creation,0.016205
creativ,0.011501
credit,0.008614
crew,0.006999
crisi,0.004592
cronyism,0.011628
cross,0.004857
crowd,0.006936
crowdfund,0.032124
csfd,0.012926
culprit,0.010433
current,0.002849
curv,0.016427
custom,0.009000
cut,0.004725
cyberspac,0.020866
danger,0.005228
darwin,0.006228
david,0.007907
deal,0.003585
death,0.003898
debat,0.008287
decid,0.004337
decisionmak,0.006415
declin,0.004225
decod,0.009001
dedic,0.005346
deduc,0.006646
deduct,0.006439
deep,0.004954
deepseat,0.010433
defenc,0.005418
defend,0.004979
defens,0.034196
defin,0.009163
definit,0.014077
degrad,0.006385
degre,0.003644
deliv,0.010492
demand,0.023841
democraci,0.004957
depart,0.003902
depend,0.008810
deriv,0.010152
describ,0.010958
desertif,0.009931
despit,0.003774
destruct,0.005267
deter,0.008153
determin,0.006181
detract,0.020546
develop,0.013355
devic,0.004974
did,0.006607
die,0.004264
differ,0.009438
difficult,0.019409
difficulti,0.004806
diminish,0.018078
directli,0.003576
disabl,0.012963
disappear,0.005658
disapprov,0.008346
discard,0.007444
discrimin,0.006180
discuss,0.007263
diseas,0.004662
disproportion,0.007767
disput,0.004476
disrupt,0.005735
distinguish,0.003951
distribut,0.011017
doctor,0.010623
doe,0.024092
dog,0.006662
doi,0.005892
dollar,0.010401
domin,0.007325
don,0.007387
donat,0.013333
dont,0.012439
dossier,0.010921
download,0.007858
downsid,0.009624
downtown,0.009303
dramat,0.005026
drug,0.010498
du,0.006290
dub,0.014319
dunbar,0.010351
dure,0.007686
dysfunct,0.007962
e,0.003606
earli,0.002680
earth,0.008413
easi,0.010884
easili,0.004590
econom,0.061582
economi,0.003659
economici,0.014495
economicu,0.020186
economist,0.025154
ed,0.007502
edit,0.004194
educ,0.006827
edward,0.004874
effect,0.010412
effici,0.029370
effort,0.011133
electron,0.004484
element,0.003407
elimin,0.004633
els,0.005518
elsewher,0.005524
emerg,0.006868
emot,0.005504
empir,0.007478
encourag,0.017617
encrypt,0.018484
encyclopedia,0.009230
end,0.008789
enforc,0.030141
engag,0.004243
enhanc,0.004963
enjoy,0.040811
ensur,0.022011
enter,0.003917
entir,0.006951
entiti,0.004546
entrepreneur,0.033744
environ,0.010656
environment,0.008616
epidem,0.014876
equal,0.014596
equilibrium,0.011200
equival,0.004316
erect,0.029065
erik,0.008054
especi,0.003111
establish,0.005719
everi,0.006706
everyon,0.024746
everywher,0.013920
evolutionari,0.005279
evolv,0.004398
exact,0.005112
exagger,0.007470
examin,0.004194
exampl,0.091194
exce,0.006999
exceed,0.005627
excess,0.010356
exclud,0.080655
exclus,0.032044
execut,0.008118
exert,0.012297
exhibit,0.005058
exhort,0.010093
exist,0.010658
expect,0.003681
expenditur,0.005924
expens,0.009571
experiment,0.008951
explain,0.010821
explan,0.009433
explor,0.008165
extens,0.003618
extent,0.008818
extern,0.008381
extra,0.012274
face,0.004021
factor,0.003397
factual,0.007720
fail,0.004028
failur,0.013986
faith,0.005356
fallen,0.006510
far,0.003633
farther,0.007484
fashion,0.005639
fear,0.005027
feasibl,0.006510
fed,0.007170
feder,0.008532
fee,0.012543
feed,0.006151
feel,0.005009
fight,0.010055
financ,0.012738
financi,0.003825
finit,0.005541
firework,0.010093
firm,0.004785
fish,0.009998
fisheri,0.007283
fit,0.009757
fix,0.004253
flame,0.007953
flew,0.008335
flood,0.006325
follow,0.006876
forc,0.002871
forcibl,0.007824
forest,0.005510
form,0.006570
fortuneshav,0.014495
fortyeight,0.011236
forum,0.005885
foundat,0.003736
fourth,0.004999
francisco,0.006183
free,0.078550
freeli,0.006231
freerid,0.072186
frequent,0.012084
fresh,0.006436
friction,0.007059
friend,0.005691
fulfil,0.005794
fund,0.031606
fundament,0.007338
funder,0.011103
futur,0.003489
gain,0.010458
gainseek,0.014495
game,0.004916
gametheoret,0.010865
gener,0.018530
geograph,0.004660
gettysburg,0.011542
ghatak,0.026280
given,0.008621
global,0.030441
goal,0.007882
god,0.005015
golf,0.009035
good,0.656250
goodcommonpool,0.014495
goodher,0.014495
govern,0.098092
government,0.005996
governmentfinanc,0.013386
grain,0.006225
graphic,0.006163
gravel,0.009702
green,0.004927
group,0.028431
guarante,0.005256
guid,0.004051
ha,0.034391
hallow,0.012926
hand,0.014201
happen,0.004528
hardin,0.010475
hardli,0.007660
hart,0.016537
hawaii,0.007623
held,0.003387
help,0.003136
henderson,0.008036
hi,0.020553
high,0.017408
higher,0.003406
highli,0.003772
highqual,0.008593
histor,0.009656
hold,0.010354
homo,0.015022
hope,0.004878
horizont,0.006607
household,0.005699
howev,0.016278
huge,0.005328
human,0.016563
hummel,0.012277
hundr,0.008707
hungri,0.009388
hybrid,0.005986
icbm,0.010093
idea,0.003215
ideal,0.004662
identifi,0.010391
ideolog,0.010825
imit,0.006778
immens,0.006885
impact,0.015707
imperfect,0.007054
impli,0.008909
implic,0.004992
implicitli,0.007594
import,0.014662
impos,0.009855
imposs,0.014534
incent,0.059656
includ,0.015912
inclus,0.005902
incom,0.013166
incomplet,0.012487
increas,0.008266
increment,0.007237
inde,0.004872
independ,0.002942
indianapoli,0.008323
indispens,0.007998
individu,0.094576
individualist,0.016581
individualssom,0.014495
ineffici,0.006635
inequ,0.006028
inevit,0.006313
infinit,0.011221
inform,0.034183
infrastructur,0.004851
ing,0.009871
inher,0.005491
initi,0.003113
injur,0.007266
innov,0.009926
insepar,0.008540
insid,0.004805
instanc,0.023283
instead,0.003356
institut,0.003032
insuffici,0.006496
intellectu,0.004915
intend,0.004435
interact,0.003575
interestth,0.014035
intern,0.005021
internet,0.023502
intervent,0.015597
intrins,0.006066
introduc,0.010083
introduct,0.003798
invent,0.013986
inventor,0.007090
invest,0.020398
investor,0.010917
invok,0.006880
involuntari,0.017706
involv,0.002777
isbn,0.003387
isnt,0.007775
issu,0.012298
jame,0.004041
jeffrey,0.007444
join,0.004261
joint,0.005045
jointproduct,0.013140
jonathan,0.006699
joseph,0.004721
journal,0.003685
julia,0.008566
june,0.004174
just,0.010023
justic,0.004842
kaul,0.014495
kickstart,0.021843
kill,0.004730
killer,0.008633
kin,0.008606
king,0.004563
know,0.004341
knowledg,0.010811
known,0.007182
labor,0.004704
lack,0.003652
languag,0.003600
larg,0.012623
larger,0.003781
late,0.003316
later,0.002849
lavoi,0.012926
law,0.023086
lawrenc,0.012817
le,0.005706
lead,0.005716
leader,0.004188
learn,0.004024
leav,0.004056
lectur,0.005208
left,0.003818
legal,0.015811
leisur,0.007858
lessig,0.024062
letter,0.004728
level,0.025780
liberti,0.006080
librari,0.009000
life,0.006158
lifelong,0.008393
light,0.047391
lighthous,0.075646
like,0.025586
likewis,0.005673
limit,0.008776
lincoln,0.008133
lindahl,0.025852
linfo,0.014035
link,0.004165
linux,0.039026
lipsey,0.012926
literatur,0.008559
litter,0.036968
live,0.009291
loaf,0.010921
lobbi,0.007362
local,0.006488
long,0.003110
lose,0.005038
lost,0.004162
low,0.011013
lower,0.007079
lowthat,0.014495
lubric,0.008869
m,0.003425
maintain,0.003340
mainten,0.011492
major,0.005082
make,0.031665
manag,0.003430
mandat,0.011936
manger,0.014495
mani,0.019485
manifest,0.005510
margin,0.068242
market,0.025098
marketlik,0.012738
materi,0.006713
matrix,0.006012
matter,0.007206
mean,0.007913
measur,0.003137
measurestax,0.014495
mechan,0.024761
media,0.004393
member,0.018337
mercenari,0.008194
mere,0.004712
merg,0.005769
met,0.019593
method,0.011866
microsoft,0.007587
mild,0.007393
militari,0.007749
million,0.015049
minor,0.008865
minut,0.005717
mirror,0.006467
misclassifi,0.012926
missionari,0.006782
model,0.008951
modern,0.005746
modif,0.005693
monetari,0.005134
money,0.021403
monopoli,0.042959
motiv,0.023397
multilevel,0.009303
multin,0.007254
multitud,0.007559
murray,0.006926
museum,0.005776
music,0.010528
musician,0.008358
n,0.004301
nation,0.023887
natur,0.020457
nd,0.004456
nearbi,0.005894
necessari,0.007634
necessarili,0.009377
need,0.011517
neg,0.004153
negoti,0.005013
neighbor,0.016210
neoclass,0.007249
new,0.014976
ngo,0.031499
nicola,0.007175
night,0.005875
noneconom,0.010433
nonexclud,0.161420
nongovernment,0.007660
nonindividu,0.014495
nonpay,0.010759
nonreligi,0.009222
nonriv,0.024555
nonrivalr,0.038779
nonrivalri,0.025852
nonstat,0.009812
nonzero,0.007866
norm,0.033334
normal,0.003817
note,0.008284
noth,0.005068
novel,0.005301
number,0.004792
nurs,0.020909
observ,0.012645
obtain,0.003723
obviou,0.006050
occasion,0.010806
oclc,0.006898
offend,0.008017
offici,0.014545
older,0.005026
onc,0.003579
onli,0.013229
onlin,0.008384
open,0.003279
oppos,0.008005
opposit,0.003941
optim,0.010161
optimum,0.023860
orbit,0.006075
order,0.002639
organ,0.020901
orphan,0.008984
ostrom,0.022081
otherwis,0.009112
outcompet,0.009812
outsiz,0.012277
overcom,0.011765
overproduct,0.009994
overtli,0.009478
overus,0.009202
owner,0.027090
ownership,0.011250
paid,0.004883
paper,0.011856
paradigm,0.005803
parcel,0.008661
pareto,0.016857
paretooptim,0.012926
park,0.027991
parti,0.018508
partial,0.004406
particip,0.015114
particular,0.008994
particularli,0.003359
patent,0.044743
patient,0.005834
patrick,0.006936
patriot,0.013978
paul,0.004163
pave,0.006774
pay,0.049190
payment,0.005162
peer,0.006678
peertop,0.010093
penguin,0.007203
peopl,0.042254
percept,0.010196
perfectli,0.006654
perform,0.003367
perhap,0.008905
perimet,0.009455
period,0.002846
perish,0.008163
person,0.028208
persuad,0.013844
pharmaceut,0.006525
phenomenon,0.004933
philanthropi,0.009702
philosophi,0.004078
philosophicalpolit,0.014495
physic,0.006268
pick,0.006234
place,0.002708
play,0.003458
pleasur,0.014243
pledg,0.027166
plu,0.005388
poem,0.014139
point,0.005556
polici,0.007089
polit,0.006012
pollut,0.012886
pool,0.018815
port,0.010825
posit,0.011354
possess,0.004304
possibl,0.016809
potenti,0.035560
power,0.002810
pp,0.004477
practic,0.005877
precis,0.004524
predetermin,0.015732
predict,0.004018
prepar,0.004454
prescrib,0.006707
present,0.002746
presid,0.003972
pressur,0.004134
presum,0.006131
pretti,0.008933
previou,0.004141
price,0.028463
principalag,0.010759
principl,0.003297
print,0.005158
privat,0.122089
privatecollect,0.014035
privileg,0.025074
probabl,0.007882
problem,0.073438
procedur,0.004783
process,0.005193
produc,0.057157
product,0.048665
profession,0.004450
profit,0.028473
profitmaxim,0.010311
profitmind,0.014495
program,0.003455
progress,0.011230
prohibit,0.011208
project,0.016993
promis,0.005161
proper,0.004927
properli,0.005776
properti,0.023279
propos,0.020332
prosper,0.005678
protect,0.007321
protocol,0.006210
prove,0.004238
provid,0.070672
provis,0.064198
public,0.392798
publicgood,0.014495
publicspirit,0.013140
publish,0.012194
punish,0.042295
purchas,0.009342
pure,0.030402
purest,0.009649
qualiti,0.004092
quantiti,0.008947
quasipubl,0.011920
question,0.010347
quorum,0.029438
r,0.003675
radio,0.011246
rais,0.012153
ransom,0.009183
rate,0.017019
ratio,0.009907
ration,0.009467
raw,0.005682
reach,0.014160
read,0.003025
reader,0.005940
readili,0.005960
real,0.014462
realiti,0.009476
realiz,0.004870
reap,0.009071
reason,0.009379
receiv,0.006656
recent,0.012065
reciproc,0.019631
red,0.004828
reduc,0.026400
ree,0.009599
refer,0.008709
refund,0.019404
refus,0.005083
regard,0.006684
regardless,0.026330
region,0.003119
regul,0.008247
regurgit,0.010198
rel,0.009369
relat,0.004697
releas,0.008707
reli,0.004197
relief,0.006210
religi,0.013348
religion,0.004436
remain,0.005761
remedi,0.006889
remot,0.005897
remov,0.004021
remuner,0.009089
rent,0.006707
replac,0.003549
reproduct,0.011834
requierdesjardin,0.014495
requir,0.013711
research,0.002825
resist,0.004513
resourc,0.031739
resourcesthat,0.012569
respons,0.003021
restrict,0.012111
result,0.024264
revel,0.006984
revenu,0.015246
revolut,0.004315
revolutionari,0.005543
reward,0.012052
richard,0.004303
ride,0.030293
rider,0.160808
right,0.006251
ring,0.005899
risk,0.004286
rivalr,0.026280
rivalri,0.014351
road,0.005059
rockefel,0.008661
roger,0.005841
role,0.006103
roman,0.014017
ronald,0.018667
room,0.011300
rothbard,0.009549
rule,0.003179
russel,0.006105
s,0.002470
sacrific,0.014190
samuelson,0.017494
san,0.005531
sanction,0.013080
satellit,0.006077
save,0.014840
saw,0.004262
scale,0.003942
scarc,0.006670
schumpet,0.009202
schumpeterian,0.011307
se,0.007243
sea,0.004444
seat,0.005154
second,0.002858
secret,0.005374
sector,0.018267
secur,0.003700
seek,0.003908
seen,0.006835
select,0.003787
selfi,0.013678
selfinterest,0.015749
selfishextrem,0.014495
selfishli,0.012569
sell,0.009855
semin,0.006662
sens,0.025090
separ,0.003182
seri,0.003470
servic,0.023757
set,0.002698
sever,0.005054
shall,0.006358
share,0.020366
shavel,0.013678
shed,0.007425
ship,0.014827
shop,0.012999
short,0.003714
shorthand,0.008950
shown,0.008401
sick,0.013679
signal,0.005012
signific,0.003094
significantli,0.004470
similar,0.008684
similarli,0.009185
simpl,0.007704
simpli,0.011867
simplifi,0.005646
simultan,0.010161
sinc,0.024242
singl,0.003244
site,0.004083
size,0.003775
sloan,0.008822
small,0.005933
smaller,0.008244
social,0.074017
societ,0.006817
societi,0.021262
softwar,0.015409
soldier,0.005516
solicit,0.008732
solut,0.053921
solutionsther,0.014495
solv,0.008923
someth,0.009322
sometim,0.018935
sourc,0.003049
space,0.007475
speci,0.004458
special,0.009096
specif,0.005801
spend,0.010064
spent,0.005358
split,0.004982
sponsorship,0.008807
spring,0.005468
st,0.003908
stake,0.007374
standard,0.013061
stanford,0.005631
starvat,0.007998
state,0.021212
statist,0.015909
statu,0.004048
steal,0.007998
stephen,0.005385
steppingston,0.010980
steven,0.006108
stigler,0.009624
stock,0.004677
stop,0.004789
strategi,0.004516
street,0.071915
strong,0.003522
strongli,0.004672
structur,0.002832
studi,0.017934
subject,0.009252
subsequ,0.003783
subsid,0.028770
subsidi,0.054025
substitut,0.010774
subtract,0.029831
success,0.006430
sue,0.008885
suffer,0.004424
suffici,0.013446
suggest,0.006787
sum,0.024179
suppli,0.007904
support,0.005764
suppos,0.005327
sure,0.006643
surround,0.004439
surviv,0.008362
taken,0.007099
talk,0.005216
tax,0.027337
taxat,0.005960
technic,0.004311
technolog,0.027963
telescop,0.007451
televis,0.016393
temporari,0.005717
temptat,0.009035
tendenc,0.005443
term,0.016035
terminolog,0.011417
th,0.008339
thcenturi,0.005590
themselv,0.003596
themthat,0.013140
theorem,0.016303
theoret,0.007950
theori,0.019891
theorist,0.011300
therebi,0.005017
thi,0.061653
thing,0.003857
think,0.008408
thmatiqu,0.012926
thought,0.003435
thousand,0.004275
threshold,0.006753
thu,0.019837
tie,0.009457
time,0.010725
today,0.003482
toll,0.007728
tool,0.003881
topic,0.003846
total,0.006765
trade,0.003507
tradit,0.012211
traffic,0.006405
tragedi,0.016306
transact,0.041760
transform,0.007936
trash,0.010659
treasur,0.007517
treat,0.004245
treatis,0.005848
tribe,0.005968
true,0.003925
truth,0.005158
turn,0.003351
tv,0.006703
tyler,0.009525
type,0.008744
typic,0.003252
ubiqu,0.009841
ultim,0.004193
unabl,0.005038
unconsci,0.007249
underli,0.004603
underproduc,0.013140
underproduct,0.052560
underprovis,0.014495
understand,0.003290
unfund,0.023634
ungener,0.012569
union,0.007205
unit,0.012226
univers,0.002510
unless,0.010865
unlik,0.008074
unquestion,0.010059
unwil,0.008428
use,0.049523
user,0.032269
usual,0.005834
v,0.004322
valu,0.018643
valuabl,0.005691
valuat,0.006707
vampir,0.009324
vari,0.007147
variat,0.004544
varieti,0.003640
variou,0.002759
vast,0.009736
veri,0.016611
vertic,0.006271
view,0.006214
virtual,0.009588
visibl,0.005358
vital,0.005414
volunt,0.020048
voluntari,0.046951
voluntarili,0.023574
wa,0.019877
war,0.003443
wartim,0.007720
wast,0.005815
water,0.003739
way,0.005184
waysand,0.013678
wealthi,0.006325
websit,0.004584
welfar,0.005583
wellknown,0.005691
whi,0.008921
wiki,0.008103
wikipedia,0.007792
willing,0.014812
wilson,0.006055
win,0.005451
work,0.014260
worker,0.004512
world,0.029494
worldwid,0.009836
worri,0.007143
wors,0.006666
write,0.007581
writer,0.009951
wrong,0.005702
wrote,0.008260
year,0.002446
york,0.003733
zalta,0.009931
zero,0.025492
zittrain,0.013140
