aar,0.040054
abl,0.023594
abnorm,0.232445
academi,0.017992
account,0.011000
acquisit,0.020194
actual,0.023906
adjust,0.017877
affect,0.012204
alpha,0.021619
altern,0.012001
analys,0.018259
analysi,0.067552
analyz,0.031398
ani,0.008231
announc,0.062736
anomali,0.025505
antitrust,0.030509
appli,0.019969
applic,0.011154
approach,0.010748
ar,0.026868
area,0.009276
argu,0.012424
aspect,0.012275
assess,0.015908
attribut,0.029037
base,0.024414
basic,0.011329
believ,0.011647
benefit,0.013666
besid,0.019268
bet,0.028488
beta,0.023034
bodi,0.011300
breadth,0.026750
built,0.014039
busi,0.012695
caarlevel,0.049133
calcul,0.043113
capm,0.036645
car,0.019930
case,0.009430
catastroph,0.022384
challeng,0.013473
chang,0.017007
chosen,0.017235
coeffici,0.043538
commiss,0.015018
common,0.036485
commonli,0.012527
commun,0.009863
compani,0.011911
compar,0.011157
conduct,0.038788
consist,0.010096
construct,0.011682
context,0.013270
convers,0.015564
corpor,0.044403
cover,0.012420
creat,0.009227
crisi,0.015567
critic,0.011096
crsp,0.045374
d,0.012212
dalkir,0.147401
data,0.012039
databas,0.017649
day,0.045536
debt,0.017493
decis,0.012719
deduct,0.043655
depend,0.009954
depot,0.030747
deriv,0.011470
design,0.011080
destroy,0.015775
determin,0.010476
differ,0.031990
difficult,0.013157
direct,0.010017
distinct,0.011619
divid,0.011962
dollar,0.017627
drift,0.023259
drive,0.016208
dure,0.017368
earn,0.051214
easili,0.015560
econom,0.019879
economywid,0.038844
effect,0.035294
effici,0.014221
elicit,0.027111
empir,0.025346
entiti,0.015410
equiti,0.020782
error,0.017270
estim,0.076686
event,0.484608
eventprob,0.049133
eventstudytool,0.049133
eventu,0.025818
evid,0.024770
exampl,0.016268
excel,0.019648
exist,0.009031
expect,0.012478
explain,0.024453
explic,0.029799
f,0.013378
fall,0.012397
feder,0.014461
financ,0.014391
financi,0.077797
firm,0.129772
fluctuat,0.020281
focal,0.027238
follow,0.023307
gener,0.006978
given,0.009740
goe,0.017477
h,0.013491
hand,0.024067
high,0.009834
highli,0.025571
howev,0.015764
httpwwwjstororgst,0.089076
idea,0.010900
identifi,0.011740
immedi,0.014276
impact,0.039929
implement,0.040565
impli,0.030198
import,0.008283
improv,0.011951
includ,0.013483
increas,0.009339
independ,0.009975
index,0.046286
individu,0.010017
industri,0.011085
inform,0.009655
invest,0.013827
investig,0.027218
investor,0.037005
issu,0.020843
journal,0.012491
kothari,0.049133
larg,0.008557
law,0.009781
level,0.009709
like,0.008672
limit,0.009916
logic,0.015714
longer,0.013391
longhorizon,0.092726
lower,0.011997
mackinlay,0.213020
magnitud,0.020031
make,0.008256
manag,0.034883
mani,0.014676
market,0.194450
marketplac,0.025669
match,0.017265
matlab,0.032050
mcguckin,0.044538
mcwilliam,0.081558
mean,0.017882
measur,0.021268
merg,0.039108
merger,0.282321
mergerrel,0.049133
method,0.040220
methodolog,0.120297
metric,0.043608
mitchel,0.027206
model,0.080905
ms,0.024397
multipl,0.012487
multius,0.037634
need,0.039039
netter,0.045374
normal,0.116468
note,0.009360
observ,0.010715
offic,0.013068
onli,0.007473
order,0.008946
organ,0.008855
overal,0.014859
overview,0.016267
p,0.012393
packag,0.020582
paramet,0.018717
parti,0.012546
period,0.019293
place,0.009181
polit,0.010189
post,0.015756
predict,0.027243
price,0.151606
prior,0.042185
probabl,0.013358
product,0.009703
project,0.011519
propos,0.011486
purpos,0.012109
r,0.024916
rais,0.041195
ration,0.016044
receiv,0.011280
refer,0.023617
refin,0.018521
reflect,0.012688
regress,0.093162
regulatori,0.020434
rel,0.010586
relationship,0.022540
relev,0.015443
reliabl,0.035309
reorganis,0.030451
requir,0.009294
research,0.038314
respect,0.011228
respons,0.030722
result,0.008224
retriev,0.017541
return,0.334586
review,0.013229
rival,0.058524
root,0.014915
scienc,0.009930
secur,0.025088
sequenc,0.032783
seri,0.011762
sever,0.008566
share,0.023011
shock,0.020185
short,0.012589
shorthorizon,0.045374
siegel,0.067969
sigma,0.025322
signific,0.031468
significantli,0.015153
simplist,0.029357
singl,0.010996
size,0.012796
social,0.010453
softwar,0.034819
solut,0.014058
sourc,0.010336
specif,0.029494
specifi,0.016707
squar,0.017409
stapl,0.026015
stata,0.043814
statist,0.067406
stem,0.018471
step,0.027867
stock,0.126827
structur,0.009600
student,0.013986
studi,0.199734
suffici,0.015191
tailor,0.027174
taken,0.012031
tdistribut,0.041181
technolog,0.011847
tend,0.013493
test,0.040300
theoret,0.013473
thereaft,0.021485
thi,0.030731
thu,0.038423
time,0.021811
tool,0.026315
trade,0.011887
truli,0.020591
ttest,0.074434
tvalu,0.041615
type,0.019759
typic,0.033072
use,0.087286
valid,0.015691
valu,0.052659
variou,0.028064
veri,0.009384
versatil,0.029405
vol,0.035320
volatil,0.021862
wa,0.006737
waldstein,0.049133
warner,0.030281
warrenboulton,0.196535
window,0.115469
withdrawn,0.025458
zero,0.017281
