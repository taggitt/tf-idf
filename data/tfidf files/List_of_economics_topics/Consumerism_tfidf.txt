abil,0.006626
abov,0.012331
abstract,0.008474
acceler,0.009263
accept,0.012093
accord,0.004816
achiev,0.006058
acquisit,0.010413
act,0.005504
action,0.005917
activ,0.033640
ad,0.012292
adapt,0.007669
adbust,0.025336
addict,0.029901
address,0.007252
admir,0.011411
adopt,0.006646
advanc,0.005948
advent,0.010532
advertis,0.113171
advoc,0.008267
aesthet,0.011680
affluenc,0.017641
aid,0.007225
aim,0.013754
align,0.010404
allow,0.009578
alongsid,0.010016
altern,0.012377
alway,0.012952
america,0.026998
american,0.021827
ancient,0.020934
andi,0.031711
ani,0.004244
annual,0.007398
anoth,0.004632
answer,0.008575
anthropolog,0.010918
anticonsumer,0.062506
anticonsumerist,0.023398
antiglob,0.018465
appar,0.016443
appeal,0.017939
approach,0.005542
appropri,0.007720
approxim,0.006563
aram,0.019406
arbit,0.016866
architect,0.012804
argu,0.038442
aristocraci,0.013577
aristocrat,0.013286
aspect,0.018989
assembl,0.022555
associ,0.018819
astonish,0.015644
attent,0.007853
attitud,0.009887
attract,0.007749
audienc,0.010870
august,0.007318
austrian,0.011536
author,0.005422
automobil,0.012066
avail,0.023549
averag,0.007193
awar,0.008713
away,0.007336
barbon,0.021235
base,0.008393
basic,0.005842
beacon,0.015732
becam,0.026541
becom,0.032541
bee,0.013619
befor,0.009919
began,0.023195
behavior,0.013624
behaviour,0.017907
believ,0.006006
benedict,0.014861
bernard,0.011310
better,0.006548
billboard,0.018387
biocapac,0.022264
biolog,0.006887
blackwel,0.024280
block,0.008651
blur,0.013522
bombard,0.012889
book,0.011038
boot,0.050736
bought,0.011096
boulder,0.014732
brand,0.082165
break,0.008005
bricolag,0.065908
britain,0.008181
british,0.012968
brodi,0.018023
brought,0.014317
buga,0.025336
bugass,0.025336
build,0.030726
built,0.007239
busi,0.006546
buy,0.018373
calkin,0.020655
came,0.006359
campaign,0.008731
capit,0.031628
capitalist,0.045959
car,0.020555
card,0.011051
caribbean,0.011096
carl,0.009710
case,0.004863
catalyst,0.012700
catch,0.011709
cater,0.014268
caus,0.010382
cecil,0.015188
celebr,0.020048
center,0.031017
centr,0.007997
centuri,0.069923
cereal,0.014340
certain,0.015970
chang,0.043850
choic,0.038212
choos,0.008367
christin,0.015188
circl,0.018744
cite,0.008135
citi,0.006717
civil,0.020016
claim,0.011808
class,0.025805
classs,0.021235
climat,0.008305
cloth,0.010168
coexist,0.012294
coffe,0.012140
coghlan,0.023398
cohes,0.012620
coin,0.016109
coke,0.033054
collect,0.011348
coloni,0.016086
colorado,0.013720
columbia,0.010818
combat,0.010210
comfort,0.012356
commerci,0.007452
commod,0.056647
commun,0.010173
compani,0.012284
compar,0.005753
compel,0.012157
compens,0.009831
competit,0.015298
compon,0.006699
concept,0.027143
concern,0.005461
configur,0.010746
conflict,0.014490
confound,0.015114
confusingli,0.017890
conscious,0.029728
consid,0.004536
consider,0.013075
conspicu,0.030475
constantli,0.010844
construct,0.006024
consum,0.348572
consumer,0.622115
consumerdriven,0.043939
consumerist,0.123930
consumerium,0.025336
consumpt,0.208687
context,0.013686
continu,0.004713
contribut,0.011419
control,0.010337
controversi,0.007665
core,0.008261
corollari,0.015212
corpor,0.030530
cost,0.006839
counterbricolag,0.025336
countri,0.020477
cours,0.006780
cranfield,0.019406
creat,0.033308
creativ,0.010051
credit,0.007528
critic,0.062945
cultur,0.099438
cultureideolog,0.025336
custom,0.007866
daili,0.018197
dali,0.016119
damag,0.008395
darkli,0.020655
data,0.006208
deadlock,0.016410
debat,0.007242
debt,0.009021
decis,0.006558
declin,0.007386
deep,0.008660
deepli,0.010984
defin,0.005338
definit,0.036907
degrad,0.011160
demand,0.013890
depart,0.006820
deplet,0.012140
depth,0.010214
describ,0.023941
design,0.022855
desir,0.014643
despit,0.013193
destin,0.010886
destroy,0.008135
develop,0.019453
devot,0.009195
did,0.005774
differ,0.004124
digit,0.017374
direct,0.010331
directli,0.006250
disciplin,0.007168
disconnect,0.015066
discuss,0.006347
display,0.017682
dispos,0.011509
disposit,0.013249
divid,0.006168
doc,0.088207
dollar,0.009090
domest,0.032242
domin,0.006401
dr,0.009651
dramat,0.008785
drinker,0.019297
drive,0.008358
driven,0.009056
driver,0.023848
drug,0.018350
duhamel,0.021703
dure,0.008956
earli,0.014053
earnest,0.014774
eclips,0.012552
ecoconsci,0.025336
ecolog,0.044130
econom,0.071757
economi,0.038378
economist,0.026379
ed,0.006556
effort,0.006486
egypt,0.009890
eighteenth,0.011970
elmo,0.019191
embrac,0.010565
emerg,0.018008
emeritu,0.015420
emphasi,0.008495
emphasis,0.012620
emul,0.040284
encompass,0.008768
encourag,0.015396
encyclopedia,0.008066
end,0.015362
endors,0.011085
engin,0.006749
ensur,0.007694
entertain,0.023032
entir,0.006075
entiti,0.007946
entrepreneur,0.011796
environ,0.012417
environment,0.030119
epidemiologist,0.019406
era,0.007399
especi,0.010877
establish,0.004998
estat,0.009970
europ,0.012701
eventu,0.006656
everday,0.025336
evergrow,0.018387
everincreas,0.015824
everyon,0.010813
everyth,0.009326
evid,0.006386
ewen,0.021235
examin,0.007330
exampl,0.025167
exceed,0.009835
excess,0.018102
exchang,0.013241
exclus,0.008001
execut,0.007094
exemplifi,0.023863
exist,0.009314
exot,0.012476
expand,0.006625
expans,0.007902
expens,0.025094
experi,0.005759
explicitli,0.009534
exploit,0.017437
expos,0.028144
express,0.012136
extend,0.012355
extens,0.006324
extern,0.003662
extrem,0.020155
fabl,0.015237
factor,0.011877
fair,0.009486
far,0.006350
fashion,0.039430
featur,0.006334
fellow,0.010589
field,0.010229
figur,0.006936
filter,0.011673
financi,0.013372
firm,0.008364
flexibl,0.010369
focu,0.006871
focus,0.006519
follow,0.008012
food,0.007208
forc,0.010039
ford,0.026523
form,0.011484
founder,0.008778
framework,0.007175
frederick,0.022597
free,0.005720
freedom,0.007906
freegan,0.022593
freeli,0.010892
french,0.013480
frivol,0.018023
fruition,0.017101
function,0.005127
furthermor,0.008582
gain,0.012186
gener,0.010796
gentri,0.016224
georg,0.014436
german,0.007072
ginni,0.020835
given,0.005023
glass,0.011039
global,0.053207
globalloc,0.023908
good,0.105794
goss,0.019297
gratif,0.017582
great,0.005644
greater,0.006308
group,0.013552
grow,0.032110
grown,0.009645
growth,0.025357
guarante,0.009186
ha,0.023376
harm,0.009655
healthi,0.010935
hegemoni,0.013535
held,0.005921
henri,0.008072
heritag,0.010243
herman,0.013376
hern,0.021459
hi,0.031433
hierarchi,0.010205
high,0.010142
higher,0.011907
histor,0.011251
histori,0.008342
historian,0.008439
home,0.007151
honest,0.014753
household,0.009962
howev,0.004064
human,0.019300
humor,0.013563
icon,0.013481
idea,0.033726
ident,0.035855
identifi,0.024216
ideolog,0.028381
ignor,0.008687
imag,0.007711
imit,0.023696
impact,0.013726
implic,0.008726
import,0.029900
imposs,0.008468
improv,0.012325
impuls,0.025672
imr,0.016050
includ,0.013906
incom,0.023012
increas,0.043344
increasingli,0.014766
incred,0.014322
inde,0.008515
individu,0.046492
individualist,0.014490
industri,0.051448
inequ,0.010537
inexor,0.017582
influenc,0.005516
influenti,0.008054
inform,0.024895
infrastructur,0.008479
inher,0.009597
instant,0.012944
intang,0.014711
integr,0.012087
intensifi,0.024553
intent,0.016856
interact,0.006248
intern,0.004388
invent,0.008149
inventor,0.012392
irrat,0.012900
item,0.045934
jackson,0.012956
jam,0.016691
jame,0.014126
jewelri,0.015702
john,0.005696
jonathan,0.011709
jorg,0.014861
josiah,0.014732
judg,0.009248
just,0.011679
justic,0.008463
justifi,0.010071
kind,0.006670
lack,0.019153
landscap,0.010440
larg,0.008825
late,0.011594
later,0.004979
law,0.005044
lay,0.009422
lebow,0.022593
leisur,0.013734
lesli,0.014146
level,0.010013
levin,0.014588
life,0.026911
lifestyl,0.023848
like,0.017888
likemind,0.017053
likewis,0.009917
line,0.029962
link,0.007281
lionel,0.016189
live,0.027068
local,0.011341
localvorebuy,0.025336
lock,0.010844
london,0.021149
long,0.005436
lost,0.007275
low,0.012833
lower,0.006186
luxuri,0.087932
mac,0.029137
macroeconom,0.022621
madelin,0.022967
magazin,0.019386
mainstream,0.030481
majfud,0.025336
major,0.004441
make,0.025545
maker,0.023099
mall,0.015919
man,0.007407
manag,0.005996
mandevil,0.018547
mani,0.018920
manipul,0.026892
manufactur,0.047905
market,0.062670
marketplac,0.079422
marten,0.092735
marxian,0.015019
mass,0.027463
massproduct,0.020174
materi,0.046935
matter,0.006297
mean,0.027664
meat,0.011617
mechan,0.006182
media,0.038397
mediocr,0.017101
medium,0.019089
meet,0.013548
menger,0.017101
merchant,0.009943
mere,0.008236
messag,0.031213
method,0.005185
michael,0.007704
middl,0.013805
middleclass,0.028644
mind,0.007895
mobil,0.008893
model,0.005215
moder,0.009591
modern,0.015065
money,0.007482
mood,0.013200
motiv,0.008179
motor,0.010797
movement,0.051958
nearli,0.021876
necess,0.031472
necessari,0.013344
need,0.035230
neg,0.021779
network,0.013998
new,0.018698
nichola,0.011261
norm,0.009710
note,0.004826
notic,0.019004
notion,0.007920
notsowealthi,0.025336
number,0.012564
oat,0.016224
obedi,0.014588
observ,0.016576
oil,0.008356
older,0.008785
oneself,0.013104
onli,0.015415
onlin,0.014655
open,0.005731
opinion,0.008898
oppon,0.010400
oppos,0.027986
order,0.013840
organ,0.018266
orient,0.017447
origin,0.018558
oswald,0.015793
outstand,0.011450
outstandingli,0.023398
overshoot,0.018310
overview,0.008388
ownership,0.019663
pack,0.012216
packag,0.010613
packard,0.018805
pamphlet,0.014146
paradigm,0.010144
particular,0.010480
particularli,0.005871
partli,0.009002
pattern,0.007143
paul,0.014554
pc,0.013591
peopl,0.073854
pepsi,0.040651
perceiv,0.008419
percept,0.008911
period,0.004974
person,0.005478
phenomena,0.007993
phenomenon,0.008622
phrase,0.010087
pioneer,0.008745
place,0.033143
placement,0.013140
planetwid,0.019521
plantat,0.012010
platform,0.010252
play,0.006044
point,0.009712
polici,0.018586
polit,0.036781
pollut,0.011261
poor,0.008277
pope,0.011774
popular,0.029948
porritt,0.025336
posit,0.019846
postconsum,0.021459
postconsumerist,0.025336
potteri,0.013164
practic,0.020545
preexist,0.013069
prefer,0.022054
present,0.019201
prevail,0.010156
preval,0.031711
previous,0.007318
prey,0.013081
price,0.007107
primarili,0.013124
principl,0.005762
privat,0.006883
process,0.018153
produc,0.029970
producer,0.023908
product,0.135096
productiv,0.021235
profit,0.016589
promin,0.007528
promot,0.020545
prone,0.013261
propel,0.026909
proper,0.008613
propos,0.011846
prosper,0.029774
protect,0.019195
protest,0.017843
provid,0.004259
provok,0.012285
public,0.009669
publish,0.015985
punk,0.017825
purchas,0.057155
push,0.009129
quaker,0.016334
quantiti,0.007819
rang,0.011525
rapid,0.008319
rapidli,0.008093
rate,0.005949
rational,0.013549
reach,0.006187
realiz,0.017026
reap,0.015855
recogn,0.026114
recogniz,0.014163
redress,0.016567
reduc,0.023072
ree,0.050334
refer,0.021312
reflect,0.006543
reformul,0.013779
regul,0.014416
regular,0.008341
reinforc,0.010369
relat,0.024631
relationship,0.017435
rememb,0.011178
renegad,0.017305
report,0.005851
repres,0.009989
reproduct,0.010342
requir,0.004793
resid,0.008066
resist,0.007889
resourc,0.024655
respons,0.005280
restor,0.008610
revolut,0.015085
rise,0.006126
ritzer,0.023398
robert,0.006694
role,0.005333
rome,0.010395
root,0.007691
rose,0.009239
rush,0.013299
ryan,0.015586
s,0.012954
sacrific,0.012401
safe,0.009954
safeti,0.009813
said,0.006255
sale,0.008869
saw,0.014901
say,0.026386
scale,0.006891
scandal,0.012857
scarciti,0.025442
scath,0.018387
scerri,0.018310
scientif,0.006126
scientist,0.006962
search,0.007793
sector,0.007982
seek,0.013664
seen,0.005974
segment,0.010431
self,0.041853
selfinterest,0.013764
selfish,0.014268
sell,0.008613
seller,0.012165
sens,0.050119
serv,0.006060
servic,0.029660
set,0.009433
seventeenth,0.013224
sever,0.004417
shift,0.022136
shop,0.090886
shopper,0.037610
short,0.006491
signific,0.005409
silk,0.012650
similar,0.010119
simpl,0.013465
sinc,0.008474
sinnreich,0.047816
situat,0.006524
sklair,0.025336
slowli,0.019534
smulyan,0.076010
social,0.086248
socialis,0.017701
societi,0.084944
socioeconom,0.024484
sociolog,0.010040
sociologist,0.012026
sold,0.008948
sole,0.017202
solidar,0.013035
someth,0.008147
sometim,0.005516
soon,0.007770
sought,0.008254
sovereignti,0.010218
space,0.006533
speak,0.008125
specif,0.010139
spectrum,0.010020
specul,0.008815
speech,0.009129
spend,0.017591
spengler,0.018630
spent,0.009366
spiritu,0.009763
spot,0.010608
spread,0.022470
st,0.027323
standard,0.022829
start,0.005363
state,0.018538
statement,0.007779
statu,0.042462
statussymbol,0.050673
steadi,0.010860
steadili,0.022356
steel,0.010613
stock,0.008175
store,0.008584
strand,0.012082
strateg,0.009248
stratif,0.015043
straut,0.025336
strength,0.008594
strengthen,0.009584
strive,0.024297
strongli,0.016334
structur,0.004950
studi,0.004478
style,0.008731
subconsci,0.016153
subcultur,0.048782
subject,0.005390
substitut,0.018832
success,0.011238
sugar,0.020650
suppli,0.006907
support,0.010076
surplu,0.011148
surviv,0.007308
sustain,0.041441
symbol,0.023078
szeman,0.023908
t,0.007372
target,0.023669
tast,0.035680
taylor,0.010662
tea,0.013549
techniqu,0.006511
technolog,0.018328
tendenc,0.009515
term,0.052052
test,0.006927
textbook,0.009551
th,0.053447
theatr,0.011970
themselv,0.031427
theori,0.009933
theorist,0.029628
therapeut,0.012504
therefor,0.005759
thi,0.041202
thing,0.006742
thinker,0.010243
thorstein,0.035780
thought,0.006005
threaten,0.009634
thu,0.014860
tim,0.013350
time,0.026244
tobacco,0.011939
today,0.006087
took,0.006619
touch,0.011183
tradit,0.010671
traffick,0.014078
transform,0.013872
transport,0.007264
trend,0.016187
tri,0.013339
trickl,0.017582
turn,0.017574
typic,0.005684
ultim,0.007329
unawar,0.013779
undergon,0.013224
underpin,0.012356
underw,0.012294
uniqu,0.014450
unit,0.008547
univers,0.008777
unleash,0.015887
unnecessari,0.025027
unpreced,0.012034
upper,0.017113
use,0.041548
user,0.018800
util,0.015331
valu,0.016293
valuat,0.011723
vanc,0.018630
varieti,0.006362
variou,0.014471
vast,0.017017
vastli,0.013249
veblen,0.049949
vehicl,0.009621
vice,0.009810
viciou,0.014861
victor,0.011405
view,0.005431
viewer,0.044584
virtual,0.008380
visibl,0.018732
vision,0.009178
visual,0.008940
voic,0.020199
wa,0.055590
want,0.007823
warm,0.010829
warren,0.012374
wast,0.030493
way,0.031715
waysconsumer,0.025336
wealthi,0.033170
websit,0.008012
wedgwood,0.019894
welcom,0.011695
went,0.007772
west,0.007275
western,0.006374
wherebi,0.009574
wide,0.005457
widespread,0.008141
wiki,0.028326
william,0.006790
wilmerd,0.025336
winslow,0.018235
wit,0.010320
word,0.017645
work,0.020770
worker,0.015775
world,0.012888
write,0.026501
writer,0.008696
xvi,0.014973
yosef,0.019764
