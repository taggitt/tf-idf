abstract,0.011994
academ,0.010209
accept,0.008557
achiev,0.017149
activ,0.006801
aggreg,0.028828
aim,0.009733
allen,0.015736
amherst,0.023223
annandaleonhudson,0.034718
aresti,0.034718
argu,0.009067
argument,0.011451
associ,0.006658
attempt,0.007774
attent,0.011114
australia,0.023694
automat,0.013914
bank,0.019713
bard,0.023934
base,0.023756
basi,0.008489
battl,0.012872
bent,0.020879
berlin,0.014637
bucknel,0.030370
budget,0.025729
cambridg,0.054508
canada,0.023579
capit,0.017904
capitalist,0.016260
carri,0.008873
centr,0.022636
central,0.007716
chartal,0.031091
circuit,0.015529
citi,0.028519
class,0.009130
close,0.007365
closest,0.016663
coffe,0.051543
coher,0.015173
collabor,0.012645
colleg,0.011949
columbia,0.045932
competit,0.021650
conduct,0.009435
consequ,0.009361
continent,0.014661
continu,0.006670
contrari,0.014396
contrast,0.009300
contribut,0.008080
control,0.007314
controversi,0.010849
cours,0.009595
credit,0.010654
crisi,0.011361
critiqu,0.013987
current,0.014096
date,0.009201
david,0.009779
davidson,0.129177
debat,0.010249
degre,0.018032
deliv,0.012976
demand,0.058975
depend,0.007264
determin,0.007645
develop,0.033036
differ,0.023346
disquisit,0.031508
distanc,0.011723
distinct,0.016958
distribut,0.009084
divis,0.009542
doioxfordjournalscjea,0.035857
domin,0.018119
drew,0.015173
earli,0.006629
eatwel,0.028551
econom,0.304660
economi,0.054313
economicsgeoff,0.035857
economist,0.074666
ed,0.009279
educ,0.008444
edward,0.024116
effect,0.019317
eichner,0.060740
elgar,0.043059
emphas,0.034898
emphasi,0.012022
emphasis,0.017860
empir,0.009248
employ,0.052752
endow,0.016789
equit,0.019763
equiti,0.015166
essay,0.012844
establish,0.007073
europ,0.008987
extend,0.008742
extern,0.005182
failur,0.011532
fallaci,0.018839
fatal,0.017228
feder,0.010553
field,0.014477
financ,0.021006
financi,0.028387
flow,0.010438
focu,0.009724
follow,0.011339
forc,0.007104
forward,0.012581
foundat,0.009242
fragil,0.019004
function,0.014513
fundament,0.009076
gener,0.025465
geoff,0.025227
germani,0.021344
global,0.009412
godley,0.028991
greater,0.008927
group,0.006393
growth,0.008971
guid,0.020042
ha,0.028356
harcourt,0.043274
hay,0.019785
hi,0.006355
hick,0.021710
hill,0.014099
histori,0.005903
historian,0.011943
holt,0.043274
homogen,0.015963
hous,0.009508
household,0.014099
howev,0.005752
hyman,0.095738
idea,0.023864
imperfect,0.017449
impos,0.012189
includ,0.004920
incom,0.010856
incorpor,0.009733
increas,0.006815
industri,0.008090
influenc,0.023420
influenti,0.011399
insight,0.013147
inspir,0.011726
institut,0.007500
instrument,0.010913
intern,0.006211
introduct,0.018792
invest,0.010091
isbn,0.025136
islm,0.025913
issu,0.007605
jan,0.015576
je,0.022139
jei,0.035857
joan,0.077097
john,0.040308
journal,0.072929
kaldor,0.083913
kalecki,0.083385
key,0.027057
keyn,0.333336
keynesian,0.272420
keynespost,0.035857
keynsian,0.071714
king,0.011288
kregel,0.090160
l,0.010431
labour,0.023994
lake,0.013976
lane,0.018120
larg,0.012490
later,0.007047
laurentian,0.030714
lavoi,0.031975
law,0.007138
led,0.007647
levi,0.016652
lewisburg,0.035857
light,0.009769
like,0.006329
link,0.005152
literatur,0.010586
long,0.007693
look,0.009783
luigi,0.021126
ma,0.014966
macmillan,0.016186
macroeconom,0.048022
mainstream,0.014379
maintain,0.008261
major,0.012572
manag,0.016971
marc,0.019564
market,0.026607
massachusett,0.015537
master,0.025290
matter,0.008912
maynard,0.057883
mcgraw,0.022308
mean,0.013050
mg,0.021673
micha,0.050103
minski,0.086694
misrepres,0.021978
missourikansa,0.031975
model,0.022141
modern,0.014213
monetar,0.023744
monetari,0.088911
money,0.063533
natur,0.012650
neoclass,0.053793
neokeynesian,0.085654
neoliber,0.019924
neoricardian,0.029231
new,0.058217
newcastl,0.049286
nichola,0.047814
note,0.006831
number,0.005927
occasion,0.013365
offshoot,0.021063
onli,0.005454
organis,0.011217
origin,0.019698
orthodox,0.015955
ottawa,0.021785
outcom,0.012113
p,0.009044
palgrav,0.017590
paradigm,0.014356
particular,0.007416
pasinetti,0.030714
path,0.012013
paul,0.061794
payment,0.012770
pennsylvania,0.017750
philip,0.014563
piero,0.046779
pksg,0.034718
place,0.013401
play,0.008554
polici,0.043840
polit,0.007436
posit,0.007021
post,0.091991
postkeynesian,0.640726
practic,0.007269
prefer,0.010404
present,0.006793
press,0.023989
pressman,0.057530
price,0.010058
primari,0.008968
princip,0.010613
principl,0.008155
prior,0.010262
product,0.007081
professor,0.012142
progress,0.009260
provid,0.006028
publish,0.022623
quantiti,0.033199
randal,0.021191
rate,0.016839
read,0.007483
real,0.008943
rebuild,0.018135
receiv,0.008232
recent,0.014923
redistribut,0.017917
refer,0.008617
regard,0.008266
region,0.007716
reject,0.010622
rel,0.007725
remain,0.007126
renew,0.012600
research,0.020971
reserv,0.011459
respond,0.011621
restor,0.012186
return,0.017441
review,0.019309
revolutionbi,0.035857
ric,0.052732
ricardo,0.019586
rigid,0.016033
robert,0.018948
robinson,0.106017
roke,0.035857
role,0.015096
routledg,0.015355
run,0.009469
s,0.018334
salt,0.014762
samuelson,0.021637
scale,0.009752
school,0.052600
second,0.007072
sector,0.011296
seen,0.008454
sever,0.006251
sharp,0.014563
short,0.009187
sidney,0.020761
signific,0.007655
simpli,0.009785
sinc,0.011993
skidelski,0.060106
sought,0.011682
south,0.009391
spirit,0.013549
sraffa,0.077423
state,0.005247
steven,0.030220
sticki,0.021785
strand,0.068398
strong,0.008713
structur,0.014013
studi,0.006337
subsequ,0.009358
superior,0.013348
suppli,0.009776
target,0.011165
technolog,0.008646
tendenc,0.013466
term,0.005666
themselv,0.008895
theoret,0.019665
theori,0.119496
thi,0.008970
thought,0.025497
time,0.005305
togeth,0.008269
took,0.009368
trade,0.008675
tradit,0.007551
treatment,0.011031
type,0.007209
typic,0.008045
uk,0.032975
uncertainti,0.013955
unit,0.012097
univers,0.074532
use,0.009800
utah,0.021710
variou,0.006826
veri,0.013697
vickrey,0.030370
view,0.023058
wa,0.014751
wage,0.013177
wale,0.016694
weintraub,0.028348
wherea,0.010406
william,0.009609
win,0.013484
work,0.035273
worker,0.011163
workerfriendli,0.035857
world,0.006079
wraywhi,0.035857
write,0.009376
wynn,0.026488
year,0.006052
york,0.018472
