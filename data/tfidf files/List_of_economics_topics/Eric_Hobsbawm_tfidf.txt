abandon,0.008924
abelov,0.012567
abid,0.007016
abil,0.003286
abneg,0.011858
aboutturn,0.012567
abov,0.003058
abroad,0.005187
absent,0.005822
absolut,0.012156
academ,0.014312
academi,0.013806
academia,0.006160
accept,0.014996
accus,0.004790
achiev,0.006010
acknowledg,0.004606
action,0.002935
actual,0.006114
add,0.004555
adher,0.004832
admir,0.005660
admit,0.015113
adolf,0.006590
adopt,0.006593
advoc,0.004100
affair,0.003950
affect,0.003121
afraid,0.007558
age,0.047868
agreement,0.003646
al,0.004097
alan,0.005064
alexandria,0.006511
alli,0.004412
allianc,0.004467
alon,0.004085
alreadi,0.003480
alter,0.004285
america,0.003347
american,0.008120
amid,0.006335
analysi,0.011519
analyt,0.004355
anarch,0.006966
ancillari,0.008120
andi,0.007864
ani,0.006316
anoth,0.002297
answer,0.004253
anticommun,0.009571
antifasc,0.011392
antifascist,0.009045
anyon,0.005208
anyth,0.009442
apologis,0.010245
apostl,0.007533
appal,0.016965
appeal,0.004449
appear,0.005534
appoint,0.007854
appreci,0.005300
approach,0.002749
approv,0.008207
april,0.003654
archiv,0.004396
argu,0.015889
arguabl,0.005822
arm,0.003809
armi,0.004109
art,0.007309
arthur,0.004873
articl,0.014196
ascherson,0.023717
ash,0.006454
asid,0.005448
ask,0.007898
assess,0.004069
associ,0.002333
assumpt,0.008140
attack,0.007703
attempt,0.002724
attend,0.004558
au,0.006849
audienc,0.005392
aunt,0.008217
austria,0.005527
austrian,0.005722
author,0.002689
autobiographi,0.007216
autumn,0.006356
award,0.004211
background,0.004102
bad,0.004900
balanc,0.003780
balzan,0.020490
banal,0.010006
bandit,0.008610
banditri,0.010006
barbar,0.008238
barcelona,0.007473
bbc,0.010869
beast,0.007450
beatl,0.009468
beatric,0.008557
becam,0.007898
becaus,0.006505
bed,0.006221
befor,0.014760
began,0.002876
begin,0.011234
believ,0.005958
ben,0.006160
benevol,0.007159
bennathan,0.012567
berlin,0.025650
best,0.006349
bestknown,0.006279
betray,0.006983
better,0.009744
bia,0.005492
big,0.004361
billi,0.008300
bio,0.007609
birchal,0.012168
birkbeck,0.051226
birth,0.004244
blackledg,0.011206
blair,0.015490
bleaki,0.012567
blind,0.005895
bloch,0.007896
block,0.004291
blood,0.004690
bloodi,0.006762
bloodland,0.012567
bochum,0.009935
bodley,0.010533
bolshevik,0.007558
bomb,0.005453
book,0.024639
born,0.016276
bound,0.004310
bourgeoisi,0.007912
breakdown,0.005929
brief,0.004565
brilliant,0.014262
bring,0.003580
britain,0.016232
british,0.045027
broader,0.004802
brought,0.007101
brutal,0.006279
buchprei,0.025134
budapest,0.007944
build,0.003048
bulletin,0.006477
cambridg,0.034388
came,0.012617
campaign,0.004330
campbel,0.006459
canadian,0.004896
capac,0.003999
capit,0.015688
capitalist,0.011398
captain,0.005822
care,0.003624
career,0.009465
carlin,0.010245
case,0.002412
catalan,0.007849
caus,0.005150
caut,0.012567
caution,0.006790
celebr,0.004972
cemeteri,0.007077
central,0.005408
centuri,0.032205
cessat,0.007596
ch,0.006156
champaign,0.010081
chanc,0.004644
chang,0.004350
chapter,0.004496
characterist,0.006650
charg,0.003723
charl,0.003694
cheerlead,0.010897
chief,0.004017
childhood,0.012330
children,0.003967
chill,0.008411
chri,0.006494
circumst,0.004431
cite,0.004035
citizen,0.003948
citizenship,0.006013
civil,0.013237
claim,0.008785
class,0.012799
clearhead,0.012168
cleric,0.006983
cling,0.008047
close,0.002581
coin,0.003995
cold,0.004599
colleagu,0.005215
collect,0.002814
collectivis,0.009327
colleg,0.016752
column,0.005481
combin,0.002751
comintern,0.009741
command,0.008096
comment,0.009242
commun,0.032799
communist,0.128492
companion,0.016695
compar,0.002853
complet,0.002664
complic,0.004372
comrad,0.008721
conced,0.007042
concept,0.002692
concern,0.002708
conclud,0.011839
condit,0.002699
condon,0.008388
confess,0.014016
confirm,0.004159
conflict,0.003593
congress,0.004323
conquest,0.005199
conserv,0.015320
consid,0.002250
consist,0.002582
consolid,0.005066
constitut,0.006093
contemporan,0.007187
context,0.003394
continu,0.002338
contracorrient,0.012567
contribut,0.011328
contributor,0.005644
control,0.002563
controversi,0.003802
corp,0.005692
corrupt,0.004737
cost,0.003392
counter,0.005217
countri,0.007618
cp,0.007371
cpgb,0.022086
crack,0.006420
creat,0.004720
cremat,0.008322
crematorium,0.011043
crime,0.004787
critic,0.014191
criticis,0.017038
critiqu,0.014707
cronin,0.009682
crossick,0.012567
crush,0.011806
cultur,0.005802
currenc,0.004228
current,0.002470
cut,0.004096
czar,0.009571
czechoslovakia,0.007609
daili,0.004513
damn,0.008258
daniel,0.004799
databas,0.004514
daughter,0.005295
david,0.023994
day,0.008735
dead,0.004837
deal,0.006217
death,0.040554
decad,0.003507
deceit,0.008411
decis,0.003253
defeat,0.004565
defend,0.004317
defunct,0.006819
degre,0.009480
deliv,0.004548
demand,0.003445
demis,0.013639
democraci,0.004298
democrat,0.007561
demograph,0.005180
demonstr,0.006936
deni,0.008946
denial,0.006660
denounc,0.006250
denunci,0.009082
depart,0.003383
deris,0.008750
descent,0.005535
describ,0.009500
desert,0.010974
desir,0.003631
despit,0.006543
detain,0.007297
determin,0.008039
deutscher,0.017879
develop,0.001929
dialect,0.005676
dialogu,0.005386
did,0.020050
didnt,0.025362
die,0.014789
difficult,0.003365
digit,0.004308
diminish,0.005224
direct,0.002562
disagre,0.005547
disappear,0.004906
disc,0.014050
disciplin,0.003555
discuss,0.003148
disgrac,0.008664
dismay,0.017167
dismiss,0.010282
disparag,0.008158
disputandum,0.011605
disquisit,0.011043
dissert,0.006571
dissolut,0.005815
distanc,0.004109
distemp,0.010161
divers,0.003711
divorc,0.006482
doctor,0.004605
document,0.003725
doe,0.010443
donald,0.005440
dont,0.005392
doublestar,0.011858
doubt,0.014940
dowhatev,0.012567
downhil,0.009199
dream,0.005571
drive,0.004145
drop,0.004235
dual,0.005440
dure,0.006663
duti,0.004591
e,0.006253
earli,0.013941
earlier,0.003432
east,0.003571
eastern,0.003855
echo,0.006471
economi,0.009518
ed,0.003252
edit,0.007272
editor,0.009890
educ,0.008878
effect,0.002256
egypt,0.009812
elect,0.010279
elegantli,0.009120
elimin,0.004017
elliott,0.007675
els,0.004784
elsewher,0.004789
emerg,0.002977
emeritu,0.015297
emphasis,0.006259
empir,0.009724
emul,0.006660
encycloped,0.007912
end,0.015240
endors,0.005498
endur,0.005669
enemi,0.010384
engag,0.003678
engin,0.003347
english,0.019413
englishman,0.008030
enlighten,0.005072
enrol,0.006388
ensur,0.003816
entrench,0.007068
episod,0.005705
eric,0.126922
ernest,0.005977
ernst,0.005632
error,0.004417
erudit,0.028260
essay,0.018007
essenc,0.005247
establish,0.002479
estim,0.003269
et,0.003993
eugen,0.011359
eurocommunist,0.010765
europ,0.009450
european,0.011574
europeh,0.012567
europischen,0.025134
evani,0.011858
evas,0.007509
everyth,0.009252
everywher,0.012068
evid,0.006335
exactli,0.004551
exalt,0.007849
exampl,0.002080
exclus,0.003968
excommun,0.008532
execr,0.011392
execut,0.007038
exist,0.004620
experiment,0.003880
explain,0.003127
extens,0.003137
extent,0.003822
extern,0.001816
extraordinari,0.006063
extrem,0.026659
extremesfrom,0.012567
fabian,0.008458
fact,0.008848
faction,0.005487
factthat,0.012567
fail,0.003492
faith,0.004644
fall,0.003171
fals,0.004711
famili,0.016084
famin,0.012442
faminew,0.012567
fanfar,0.010006
far,0.003150
fascism,0.022788
fascistantifascist,0.012567
fashion,0.004889
father,0.004084
favour,0.009136
favourit,0.015637
fba,0.010006
featur,0.003141
februari,0.003774
feel,0.008685
fellow,0.036768
felt,0.004904
ferguson,0.015141
fight,0.026154
figur,0.003440
film,0.004711
final,0.006009
firm,0.004149
firmli,0.006072
fiveyear,0.005657
floud,0.010765
fluentli,0.009571
focus,0.003233
folk,0.006005
follow,0.011923
foner,0.011043
forc,0.004979
forecast,0.005473
foreign,0.006467
forgotten,0.006559
form,0.001898
fortun,0.005635
forward,0.004409
fought,0.005269
framework,0.003559
franc,0.003556
franci,0.014271
franki,0.011392
frankli,0.009682
free,0.005675
freedom,0.003921
french,0.013372
friedrichlistschool,0.012567
friendli,0.005722
frsl,0.012168
fuss,0.009009
futur,0.003025
g,0.006721
gareth,0.009082
gather,0.004330
gelber,0.012168
gener,0.005355
genoves,0.010245
genuin,0.005762
georg,0.003580
german,0.003508
germani,0.018702
germanspeak,0.007896
gina,0.020163
given,0.002491
globalis,0.008120
golder,0.011858
gone,0.005512
good,0.005523
got,0.005604
govern,0.002362
grace,0.006547
grammar,0.006179
grandfath,0.007122
grasp,0.006197
gray,0.006197
great,0.036397
greatest,0.004371
greatgrandfath,0.009519
green,0.004272
greet,0.007307
gregori,0.006042
gretl,0.011858
grn,0.010006
ground,0.003614
group,0.004481
guardian,0.018414
guilt,0.007473
guilti,0.006667
gulag,0.009082
h,0.006901
ha,0.016564
habit,0.005266
halt,0.005660
hampson,0.010765
happen,0.011778
hauptprei,0.012567
hbzbm,0.012567
head,0.003207
heart,0.004559
heavi,0.004293
hed,0.009045
heinrichgymnasium,0.012567
held,0.002937
help,0.005439
herrmann,0.020861
hi,0.135870
hideou,0.009935
hierarchi,0.005062
higham,0.009571
highgat,0.010533
highli,0.003270
hiroshima,0.008047
histor,0.022324
histori,0.045519
historian,0.083720
historiannot,0.012567
historiograph,0.007745
historiographi,0.006304
historyfrom,0.011858
hitler,0.040617
hitlerstalin,0.011858
hobsbaum,0.034817
hobsbawm,0.854102
hobsbawn,0.012567
hold,0.002992
holiday,0.005847
honorari,0.038426
honour,0.029292
hope,0.008459
horror,0.014208
hospit,0.004898
hour,0.004365
howev,0.006048
httpwwwinternationaluclaeduarticleaspparentid,0.012567
hubri,0.009803
human,0.004786
hundr,0.003774
hungari,0.055364
hybrid,0.005190
ian,0.018400
idea,0.005576
ident,0.003557
ideolog,0.004692
ignatieff,0.024336
ii,0.016255
iith,0.011605
il,0.006926
illinoi,0.006080
illus,0.012267
imagin,0.004966
impact,0.003404
imposs,0.004200
includ,0.003448
incompet,0.007328
inde,0.004223
independ,0.002551
indepth,0.007206
indirect,0.005397
individu,0.002562
induc,0.004919
indulg,0.007558
industri,0.008506
inevit,0.005473
influenti,0.003995
insid,0.004166
insist,0.005068
intellectu,0.004261
intellig,0.007752
intens,0.004125
inter,0.006983
intern,0.002176
internet,0.004075
intervent,0.004507
interview,0.035956
introduc,0.002914
intrud,0.007622
invas,0.022649
invent,0.008084
irrat,0.006399
isbn,0.008809
island,0.007363
isnt,0.013483
isol,0.004033
israel,0.005087
issu,0.010662
italian,0.009322
ivori,0.006748
j,0.012478
jaggi,0.011392
jame,0.003503
januari,0.007090
januaryfebruari,0.009741
japan,0.012324
japanes,0.004734
jazz,0.017064
jewish,0.015449
joachim,0.007803
john,0.005651
join,0.003694
joll,0.010765
jone,0.005515
joshua,0.006991
journal,0.012780
journalist,0.005847
judgement,0.006279
judt,0.069635
julia,0.022281
june,0.014476
just,0.008689
justic,0.004198
justif,0.005804
justifieda,0.012567
karl,0.004672
kegan,0.008238
kershaw,0.019607
key,0.003161
khrushchev,0.024838
kind,0.003308
king,0.011869
kinnock,0.040981
knew,0.017553
know,0.011291
knowabl,0.025305
knowledg,0.012497
known,0.002075
koestler,0.009571
kremlinologist,0.012567
la,0.004145
labour,0.029433
land,0.003143
laps,0.007393
larg,0.002188
larger,0.003278
laszlo,0.009009
late,0.005750
later,0.009880
lead,0.002478
leagu,0.005095
learn,0.003488
leav,0.007034
lectur,0.009031
lectureship,0.009468
led,0.010721
left,0.019863
leftw,0.006849
leipzig,0.007349
leopold,0.006975
lesson,0.005829
let,0.009306
letter,0.016398
leukemia,0.016241
liber,0.007488
liberaldemocrat,0.010430
life,0.005339
lifelong,0.007276
light,0.003423
like,0.008873
line,0.008917
link,0.003611
lionel,0.008030
list,0.002309
literari,0.041666
literatur,0.011131
live,0.021482
logic,0.004019
london,0.041962
long,0.008089
longer,0.006850
longterm,0.004370
lose,0.004367
lover,0.007328
lunbeck,0.012567
macfarlan,0.009082
magazin,0.014424
maintain,0.002895
major,0.002203
make,0.004223
man,0.007348
manag,0.002974
manchest,0.013035
manhattan,0.007149
mani,0.009385
manufactur,0.003960
march,0.010331
market,0.009325
marlen,0.010765
marri,0.005205
marriag,0.010500
marseillais,0.011043
marx,0.023217
marxism,0.025977
marxist,0.056383
marylebon,0.011043
mass,0.010216
massacr,0.006055
massiv,0.013256
masterli,0.010765
match,0.004416
matern,0.006634
matter,0.003123
matthew,0.006021
maya,0.007461
mccarthyism,0.010081
mckibblin,0.012567
meanwhil,0.005021
measur,0.002719
media,0.003809
member,0.010598
membership,0.009148
memoir,0.040203
memori,0.017279
men,0.007701
mendac,0.011858
mention,0.008040
merchant,0.004932
metropoli,0.007961
michael,0.011465
middleclass,0.007104
miliband,0.010245
milit,0.006488
million,0.019570
mind,0.003916
mingay,0.012567
minist,0.007493
miscellan,0.007216
mixtur,0.005192
model,0.002586
moder,0.004757
modern,0.004981
molotovribbentrop,0.009241
monarchi,0.005323
montevideo,0.009120
moscow,0.011898
mostli,0.003543
mother,0.004785
motto,0.007382
movement,0.008590
movi,0.005679
moynihan,0.010644
mr,0.016749
murder,0.010548
muriel,0.009420
music,0.004564
mutual,0.004380
myth,0.005303
mytholog,0.005807
n,0.003729
nagasaki,0.008217
nanci,0.014067
narr,0.005464
nation,0.006903
nationalist,0.005384
natur,0.002216
nay,0.009803
nazi,0.034742
ne,0.007016
neal,0.007731
nearer,0.007759
need,0.002496
neil,0.019790
nelli,0.009373
new,0.029678
news,0.004532
newspap,0.005141
newton,0.010486
niall,0.018840
nineteenth,0.015583
nobodi,0.007266
nonmilitari,0.008637
norah,0.011858
normal,0.003310
norman,0.005851
notabl,0.006373
note,0.004788
noth,0.017577
notic,0.004713
novemb,0.011173
nuclear,0.008486
number,0.006232
numer,0.003076
obeyedif,0.012567
object,0.005677
oblig,0.004585
obliter,0.009082
observ,0.002740
obstacl,0.005993
obstbaum,0.012567
obtain,0.003227
obviou,0.005245
occupi,0.004163
octob,0.021545
oeuvr,0.010081
offshoot,0.007382
omiss,0.008012
omnibu,0.009420
onli,0.011469
onslaught,0.009082
opin,0.008435
opinion,0.004413
opposit,0.003416
oppress,0.006124
opt,0.006459
optimist,0.006999
order,0.018307
origin,0.002301
orwel,0.034335
ought,0.005961
outcom,0.004245
outlook,0.011906
outofwedlock,0.012567
outsid,0.003167
overwhelm,0.005568
ownership,0.004876
p,0.031700
pact,0.024942
page,0.004071
paid,0.004234
pair,0.004425
palliat,0.011043
parent,0.009353
pari,0.004541
parti,0.060974
partial,0.003820
particip,0.003275
partyliber,0.012567
pass,0.003226
passag,0.004947
passion,0.006235
past,0.013177
patern,0.007438
paul,0.007219
peak,0.004556
penguin,0.006245
peopl,0.013737
perci,0.007609
perhap,0.007720
period,0.009869
pernici,0.008411
person,0.005434
peter,0.003786
phd,0.010732
philip,0.005104
physic,0.002717
pimlott,0.012567
place,0.002348
plan,0.003144
player,0.005434
plead,0.007978
pleas,0.006289
plenti,0.006529
plural,0.005340
pluto,0.008322
pneumonia,0.008217
point,0.004817
polar,0.005087
polici,0.003073
polish,0.005610
polit,0.031276
politician,0.004982
polyglot,0.010644
polymath,0.007122
poor,0.004105
popul,0.002954
popular,0.008913
portugues,0.005286
posit,0.002461
possess,0.003731
possibl,0.004857
post,0.008060
postwar,0.005647
power,0.009747
pp,0.031057
pragu,0.029988
prais,0.022272
precis,0.003922
predomin,0.005432
prepar,0.003862
present,0.004762
presid,0.010331
presoviet,0.011605
press,0.011210
prevent,0.006808
price,0.003525
primit,0.005269
prinz,0.009803
prior,0.003596
prioriti,0.005021
prize,0.031088
pro,0.006918
probabl,0.003416
problem,0.005093
produc,0.002477
product,0.002481
profession,0.007717
professor,0.034047
professorship,0.007689
profil,0.005141
profound,0.010864
promin,0.003734
promis,0.004474
promot,0.003396
propaganda,0.006366
propos,0.002937
prose,0.006734
protest,0.008850
prove,0.003675
provincialis,0.011392
prowess,0.008065
prycejon,0.037702
pseudonym,0.007717
public,0.016787
publish,0.013215
pupil,0.006211
purport,0.006734
quartet,0.008637
queen,0.004997
question,0.014951
quickli,0.003876
quietli,0.007662
quot,0.009964
r,0.006373
radic,0.004365
radio,0.004875
rajk,0.011206
raphael,0.008139
rapid,0.004126
reach,0.003069
react,0.005276
reaction,0.003872
read,0.013114
reader,0.005149
real,0.006269
realis,0.006089
realisein,0.012567
realiti,0.008216
realli,0.004858
realm,0.004770
reason,0.010841
rebel,0.005487
rebellion,0.005392
receiv,0.008656
recent,0.002615
recept,0.005977
recipi,0.005829
recognis,0.004662
recruit,0.005568
recur,0.006448
red,0.004186
refer,0.009061
refus,0.008814
regard,0.017384
regim,0.017603
regret,0.007833
regular,0.004137
reject,0.003723
rejoic,0.009519
relat,0.002036
reliabl,0.018063
relinquish,0.006934
reluct,0.006156
remain,0.007493
remak,0.008411
rememb,0.011088
repblica,0.008810
repeat,0.004359
repli,0.006211
reprint,0.005807
republ,0.003544
research,0.007350
result,0.004207
retir,0.005138
retort,0.009626
retriev,0.004486
reveal,0.003972
review,0.081213
revis,0.004513
revolut,0.041153
rhetor,0.005847
ribbentrop,0.010533
ribbentropmolotov,0.011206
rich,0.004330
richard,0.003730
right,0.005420
rightlean,0.010644
rise,0.006078
robert,0.003320
role,0.005291
rough,0.005989
routledg,0.005381
royal,0.019924
russia,0.013315
russian,0.008842
s,0.023561
sacrific,0.012303
said,0.021720
samuel,0.005368
sassoon,0.012567
saw,0.011087
say,0.016359
scale,0.006836
schism,0.007349
schlerbund,0.012567
scholarship,0.005400
school,0.015363
schwarz,0.008458
scienc,0.005080
scorn,0.008178
seaman,0.009241
second,0.014871
sens,0.009322
septemb,0.010926
serv,0.006012
servic,0.002942
setonwatson,0.011858
seven,0.003963
sex,0.005229
shame,0.007849
share,0.002942
sheer,0.021891
shock,0.015489
short,0.003220
shouldnt,0.008637
sidney,0.007276
sign,0.006921
silenc,0.006734
similar,0.002509
simpli,0.010288
sinc,0.004203
sister,0.005800
skip,0.007745
slatta,0.012567
slipperi,0.009009
small,0.002571
smith,0.004453
snowman,0.011206
snyder,0.033202
social,0.037432
socialisma,0.011206
socialist,0.051722
societi,0.021066
somewher,0.006437
son,0.008048
sone,0.012567
soon,0.003854
sourc,0.002643
soviet,0.081945
sozialistisch,0.011206
spain,0.022332
spanish,0.013636
spartacu,0.010430
speak,0.004030
specif,0.005029
spectat,0.008047
spent,0.009291
spirit,0.004749
spoke,0.023203
spontan,0.005432
spous,0.007731
spring,0.018963
squander,0.009159
st,0.003388
stack,0.006553
staff,0.004954
stalin,0.062198
stalinist,0.008258
standard,0.002830
stanford,0.004882
start,0.010642
state,0.007356
statement,0.003858
statesman,0.007276
stay,0.004600
steadili,0.005544
stedman,0.011043
stood,0.005654
store,0.004258
straitjacket,0.010334
street,0.004796
strength,0.004262
stress,0.004310
strike,0.004675
strongli,0.004051
student,0.007154
studi,0.008884
subject,0.005347
subsequ,0.003279
subtler,0.010245
successor,0.004721
suffer,0.015344
sum,0.004192
supplement,0.031222
support,0.009996
suppress,0.004813
sure,0.005759
surnam,0.007609
surrend,0.005790
surround,0.003849
suspicion,0.006755
swing,0.006934
sympath,0.016204
sympathet,0.006720
sympathi,0.020663
taken,0.003077
talent,0.006211
tank,0.005822
tear,0.006741
televis,0.004737
temporari,0.004956
tendenc,0.004719
tender,0.007008
term,0.001986
terror,0.016785
th,0.019280
thane,0.022413
thank,0.005779
thatcher,0.007675
thcenturi,0.004846
theme,0.004655
themselv,0.003117
theori,0.002463
therefor,0.002856
thi,0.012576
thing,0.006688
think,0.021869
thought,0.005957
threat,0.004408
threw,0.007703
thu,0.007370
thuggeri,0.012567
time,0.024175
timesnoth,0.012567
timothi,0.013428
tito,0.008664
today,0.018116
toni,0.039071
took,0.009850
tool,0.003365
tortur,0.006325
total,0.005865
touch,0.005547
tough,0.006999
tower,0.006147
trade,0.003040
tradit,0.013233
tragic,0.007759
transform,0.003440
transport,0.003603
traumat,0.007266
treat,0.003680
trend,0.004014
tri,0.009924
trial,0.019184
trilog,0.014742
troop,0.004744
troubl,0.011036
trouser,0.009868
true,0.003403
trumpet,0.009045
truth,0.004471
turn,0.005811
tutor,0.007307
twenti,0.005169
twentieth,0.009917
type,0.002526
ubiquit,0.006628
unapologet,0.010644
uncl,0.006720
undeniablehi,0.012567
understand,0.005706
understandingnot,0.012567
unfortun,0.006059
union,0.012493
uniqu,0.003583
univers,0.021768
universidad,0.008158
unlik,0.003500
unpredict,0.006615
unqualifi,0.008873
untrustworthi,0.010081
upperclass,0.008939
uruguay,0.007187
use,0.003434
ussr,0.060722
usual,0.002529
variou,0.002392
vast,0.008441
vein,0.006259
veri,0.004800
version,0.007353
verstndigung,0.025134
victim,0.005647
video,0.004626
vienna,0.024054
view,0.013469
viewer,0.007371
viewpoint,0.005532
vision,0.009105
visit,0.016891
voic,0.005009
vol,0.004517
volga,0.008810
volum,0.050404
von,0.004262
vote,0.003985
vulgar,0.008102
w,0.003526
wa,0.110294
wall,0.004450
walther,0.015637
war,0.041800
wast,0.010083
way,0.004494
weaken,0.005264
weaker,0.005985
webb,0.007896
weber,0.006426
weekli,0.006235
weimarsupport,0.012567
west,0.007217
western,0.003161
whatev,0.010348
wheen,0.025134
wherea,0.007294
whi,0.007734
wide,0.002707
wish,0.004647
withdraw,0.005052
wolfson,0.010081
women,0.004000
wont,0.007307
wood,0.004849
word,0.002917
work,0.020605
worker,0.003912
world,0.036225
worst,0.005797
worth,0.023794
wrigley,0.010533
write,0.026290
writer,0.004313
written,0.006655
wrong,0.004943
wrote,0.046552
yale,0.005745
year,0.029696
york,0.009711
young,0.003902
younger,0.005222
zigzag,0.009199
zur,0.015018
