abil,0.007778
abov,0.007238
access,0.007822
accord,0.005653
achiev,0.014224
action,0.020840
activ,0.011282
activist,0.013487
addit,0.005719
address,0.008513
adequ,0.011860
adopt,0.007801
affect,0.022163
afford,0.012141
agenc,0.008894
aid,0.008481
ailment,0.017977
allow,0.011244
alon,0.009668
alreadi,0.008236
alsoedit,0.017800
american,0.019217
analog,0.010102
analyz,0.009503
angel,0.013558
anger,0.029514
annual,0.008684
anoth,0.005437
answer,0.010066
appli,0.012087
appreci,0.012545
approach,0.019519
appropri,0.009062
approxim,0.007704
aqol,0.029742
area,0.016845
argu,0.007521
argument,0.009498
ask,0.037384
aspect,0.022291
aspir,0.014212
assert,0.009712
assess,0.028889
associ,0.005522
attempt,0.032244
austin,0.016264
australia,0.009827
avail,0.013822
bank,0.016352
base,0.014778
basi,0.007042
basic,0.048007
becom,0.005457
begun,0.010839
belief,0.018251
believ,0.014101
belong,0.009632
belongingaccord,0.029742
better,0.015375
betweenperson,0.029742
bhutan,0.036866
brief,0.010804
broader,0.011366
broadli,0.011411
broken,0.034099
built,0.008498
busi,0.007684
buy,0.010784
calcul,0.008699
canadian,0.011589
capita,0.024783
care,0.025731
case,0.005708
casebycas,0.022915
categori,0.008711
catherin,0.016264
centr,0.009388
certain,0.012498
chang,0.015442
child,0.043863
choic,0.008971
chronic,0.014033
circl,0.011002
citi,0.047311
citizen,0.028031
closest,0.013821
coin,0.009455
colombia,0.015567
columbia,0.012699
combin,0.019533
comfort,0.014505
commit,0.009570
commonli,0.007583
commun,0.029854
comparison,0.009575
complex,0.006682
concept,0.038236
concern,0.012822
conclud,0.009339
confirm,0.009844
confus,0.010278
connect,0.007518
consid,0.015977
constitut,0.007210
content,0.009010
context,0.016066
correspondingli,0.017244
corrupt,0.011211
costa,0.016175
costanza,0.023514
count,0.010415
countri,0.060096
creativ,0.011799
crime,0.067981
crimesedit,0.029742
critic,0.006717
cultur,0.020599
d,0.014785
daili,0.021361
data,0.014576
databas,0.010683
david,0.008112
day,0.006891
dayreconstruct,0.029742
debilit,0.019218
declar,0.008820
declin,0.026011
decreas,0.008992
deem,0.012358
defens,0.010023
defin,0.037602
definit,0.028883
degre,0.007478
denmark,0.013396
deriv,0.006943
describ,0.005620
design,0.013415
desir,0.017189
determin,0.012683
develop,0.077641
developmentedit,0.026522
did,0.006778
differ,0.024206
difficult,0.015929
dilapid,0.022294
diplomaci,0.015369
disciplin,0.008414
diseas,0.019132
disord,0.011772
distinguish,0.008107
divers,0.008783
divorc,0.030684
doe,0.012358
domain,0.017837
domin,0.007514
dure,0.005256
ecolog,0.031082
econom,0.012033
economist,0.030966
educ,0.042025
effect,0.005341
effort,0.007614
elder,0.013510
electron,0.009200
elus,0.018398
emot,0.033882
employ,0.029171
enabl,0.008719
encapsul,0.017293
end,0.006011
endstag,0.024458
engag,0.017412
enjoy,0.020934
enquiri,0.016646
ensur,0.009032
environ,0.029152
environment,0.008839
eortc,0.026960
eric,0.013059
essenti,0.007642
ethic,0.020237
eudaimonia,0.021001
europ,0.014910
european,0.034239
evalu,0.057158
eventu,0.007814
everi,0.006879
everyday,0.011744
everyth,0.010948
exacerb,0.015221
examin,0.008605
exampl,0.019695
expect,0.022661
experi,0.040565
experienc,0.019741
experiencesth,0.029742
explain,0.007401
explicit,0.012005
express,0.007123
extend,0.007251
extent,0.009046
extern,0.004299
ezechi,0.029742
face,0.008252
famili,0.015225
fatalist,0.023682
featur,0.007435
field,0.018012
fiftytwo,0.022915
fight,0.010316
final,0.007111
financ,0.008711
financi,0.007848
fitt,0.025191
flourish,0.012100
focu,0.008065
focus,0.007653
follow,0.004702
food,0.016922
footprint,0.017742
foreign,0.015306
foundat,0.007666
franc,0.008416
francisco,0.012687
free,0.006715
freedom,0.046404
frequenc,0.011235
frequent,0.008264
fulfil,0.011888
gallup,0.039599
gavin,0.019448
gdp,0.022113
gender,0.012603
gener,0.029571
generos,0.019644
giuliani,0.026135
given,0.017689
global,0.062459
gnh,0.053921
goal,0.040433
good,0.006536
goodman,0.019218
govern,0.022362
graffiti,0.043740
grassroot,0.036330
greater,0.014809
gross,0.034484
group,0.005303
grow,0.022616
guid,0.016624
ha,0.031361
hamper,0.015582
hand,0.007284
happi,0.389979
happiest,0.022182
harder,0.014612
harsh,0.013988
hdi,0.061517
health,0.106548
healthcar,0.041542
healthcareedit,0.029742
healthi,0.012836
help,0.006436
hi,0.005271
high,0.005953
histori,0.004896
home,0.016790
homeless,0.018038
hope,0.010009
howev,0.023856
hrqol,0.056130
human,0.073633
idea,0.013196
ideal,0.009566
ident,0.008418
ignor,0.010197
ill,0.022831
imag,0.009052
impact,0.008056
implement,0.008185
impli,0.009139
implicit,0.013770
import,0.025070
improv,0.057876
includ,0.028568
incom,0.054028
incorpor,0.008073
increas,0.016960
index,0.093395
indexedit,0.059484
indic,0.079555
indicesedit,0.029742
individu,0.060640
inequ,0.012369
infant,0.014126
influenc,0.006475
infrastructur,0.009953
instead,0.006886
institut,0.012442
instrument,0.027157
integr,0.007094
intellig,0.009173
intens,0.009764
intern,0.046366
interpret,0.007843
intersector,0.029742
introduc,0.006896
investig,0.008238
involv,0.005699
issu,0.012617
jame,0.008291
java,0.014885
journal,0.045369
journalsedit,0.028065
joy,0.031876
justifi,0.011822
kingdom,0.007819
known,0.004912
la,0.009811
lack,0.007494
landmark,0.013838
languag,0.007387
lead,0.017593
learn,0.016513
leav,0.008323
leednd,0.029742
left,0.015670
leisur,0.016122
level,0.041141
life,0.398046
lifehecht,0.029742
lifesatisfact,0.029742
lifethreaten,0.038992
linksedit,0.017947
list,0.016395
literaci,0.014421
litter,0.018962
livabilityedit,0.029742
livabl,0.119299
live,0.076260
lo,0.013479
local,0.006656
locat,0.007226
long,0.012763
longterm,0.010342
look,0.008115
love,0.011668
low,0.007532
lower,0.007262
make,0.009995
mani,0.017768
market,0.007356
materi,0.013774
materialist,0.015889
matter,0.007392
mayor,0.014909
mean,0.005412
meaning,0.012773
measur,0.180241
measurementedit,0.029742
measuresedit,0.029742
memori,0.020446
mental,0.021269
mentalphys,0.026960
mercer,0.041559
messag,0.012213
metaanalysi,0.018841
method,0.036520
metric,0.026397
mind,0.009268
minor,0.018189
misfortun,0.020075
mix,0.009453
model,0.006121
moment,0.011181
monash,0.022182
mood,0.015495
morri,0.031223
morriss,0.026960
mortal,0.013135
ms,0.014768
multipl,0.007559
nation,0.059903
natur,0.005246
nearest,0.015841
necess,0.012315
necessarili,0.009620
need,0.017724
neg,0.017044
neglect,0.012855
neighborhood,0.014885
new,0.004389
newsom,0.025476
ngo,0.032315
normal,0.007833
north,0.007803
notabl,0.015082
numer,0.007281
object,0.026873
observ,0.006486
occur,0.006244
oecd,0.029095
offici,0.022383
onli,0.009047
opportun,0.009283
option,0.010254
order,0.005415
organ,0.026803
organis,0.009304
oscar,0.017125
outcom,0.030142
outlin,0.019132
overarch,0.016626
overview,0.009847
overviewedit,0.024246
pain,0.024895
particular,0.006151
patient,0.023941
penalti,0.014700
peopl,0.032511
perceiv,0.009883
percentag,0.010951
percept,0.010460
perform,0.006908
perhap,0.009135
period,0.005839
person,0.025723
pessimist,0.018263
physic,0.025723
pictur,0.011002
planet,0.011150
platform,0.012034
polici,0.080001
polit,0.018504
politician,0.011790
popsicl,0.084196
popul,0.006992
posit,0.023297
possibl,0.022992
potenti,0.007296
poverti,0.081153
pqli,0.056130
precis,0.009283
preconfigur,0.029742
predict,0.008245
presenc,0.008522
previou,0.008498
primarili,0.007703
princeton,0.012442
prioriti,0.011884
problem,0.012054
process,0.005327
product,0.011747
professor,0.010072
program,0.007089
programm,0.010832
progress,0.015361
project,0.013946
promin,0.017674
properti,0.006823
propon,0.011809
propoor,0.025789
protect,0.007511
provid,0.020000
proxi,0.015794
psycholog,0.019212
public,0.011351
publish,0.012510
purpos,0.007330
q,0.012621
qlqc,0.029742
qol,0.178454
qualiti,0.461819
qualityoflif,0.079566
qualityofliferecord,0.029742
quantifi,0.012663
quantit,0.010470
questionnair,0.017977
randomli,0.015235
rang,0.013529
rank,0.019029
rate,0.006983
readingedit,0.020075
real,0.007418
realis,0.014411
recal,0.027643
recent,0.030945
recognis,0.011034
record,0.015067
recreat,0.027608
reduc,0.006770
refer,0.003574
referencesedit,0.017604
reflect,0.007680
refocus,0.021971
refus,0.010430
regard,0.006857
region,0.006400
rehabilit,0.015411
rel,0.012816
relat,0.033733
relationship,0.013644
relief,0.012742
religi,0.009129
renew,0.010451
repeatedli,0.012718
replac,0.007283
report,0.068695
reportedit,0.029742
requir,0.005626
research,0.075378
resid,0.018938
resourc,0.007235
respect,0.013594
respond,0.028918
respons,0.006199
result,0.019914
review,0.008008
rica,0.017977
richard,0.008829
right,0.012827
riordan,0.022528
rise,0.007192
robert,0.007858
role,0.006261
rudolph,0.019308
s,0.005069
sad,0.052106
safe,0.011685
safeti,0.011520
sampl,0.010651
san,0.011350
satisfact,0.014803
scale,0.048537
scienc,0.006011
scope,0.010507
score,0.013100
secur,0.022780
select,0.007771
send,0.011177
seriou,0.010012
sf,0.018539
shape,0.017037
sharewarefreewar,0.029742
shelter,0.027869
shiel,0.025191
shown,0.008619
sick,0.014033
significantli,0.009173
similar,0.005939
simpl,0.015807
simpli,0.008116
sinc,0.019895
slightli,0.010780
social,0.025311
societi,0.043625
sociologist,0.028234
somat,0.017368
someon,0.011619
sometim,0.006475
specif,0.005951
sponsorship,0.018070
spur,0.013970
standard,0.053597
state,0.026114
steadili,0.013121
stress,0.051002
strongli,0.009587
studi,0.021027
subdomain,0.021494
subject,0.069605
sublimin,0.021584
substanti,0.027702
suffer,0.009078
suffici,0.009196
survey,0.057368
sustain,0.019459
swb,0.027466
symptom,0.014155
taken,0.007283
talk,0.010703
term,0.018801
termin,0.023309
theori,0.023321
therefor,0.006760
thi,0.037205
thing,0.007914
think,0.008626
thought,0.007049
time,0.022005
today,0.014291
toler,0.050344
toronto,0.015453
town,0.010214
tradit,0.006263
treatment,0.018300
tri,0.007829
type,0.005980
uk,0.009117
ultim,0.008603
unattend,0.023055
unforeseen,0.019218
unhappi,0.017445
uniqu,0.008481
unit,0.025085
univers,0.020607
unlik,0.008283
unreason,0.018468
uoft,0.029742
urban,0.010433
urin,0.016606
use,0.073159
uswitch,0.029742
valu,0.012750
vandal,0.018725
vari,0.007332
variabl,0.009092
variat,0.009324
variou,0.005662
vega,0.016449
victimless,0.026960
vietnam,0.013479
wa,0.008157
walk,0.025411
wangbak,0.029742
water,0.007672
way,0.015955
weak,0.009880
wealth,0.030730
wellb,0.199603
wheretobeborn,0.028797
wide,0.012813
wikivers,0.021078
wilson,0.024850
window,0.041938
withinperson,0.028065
word,0.006904
work,0.029258
world,0.065559
year,0.015060
york,0.007661
zero,0.020921
