access,0.013405
account,0.005705
accur,0.008253
acquir,0.007890
act,0.011073
action,0.005952
activ,0.009667
actor,0.022334
actual,0.006200
ad,0.006182
adam,0.009831
administr,0.006773
advocaci,0.013111
airplan,0.028424
aisl,0.020444
allow,0.009635
ambienc,0.021830
andor,0.016955
ani,0.008538
anoth,0.027956
appli,0.005178
apprehend,0.015917
appropri,0.007765
arbitr,0.013727
argu,0.006444
art,0.007411
assess,0.008251
assign,0.017403
assist,0.007223
attribut,0.007530
author,0.005454
autom,0.010840
avail,0.029609
balanc,0.007667
bank,0.007005
barter,0.014111
behalf,0.010774
behavior,0.006852
benefit,0.049619
big,0.008844
bodi,0.005861
book,0.005551
brokerag,0.015131
build,0.018544
burial,0.013277
busi,0.039509
buyer,0.023316
cab,0.018417
cadav,0.018269
care,0.036748
carpentri,0.019410
case,0.009783
caseload,0.022098
cash,0.009778
caus,0.005221
centuri,0.005023
chair,0.010988
characterist,0.006743
charg,0.007549
childcar,0.017805
circumst,0.008987
claimant,0.016078
classic,0.006464
classifi,0.007914
clean,0.045811
clear,0.007282
clearli,0.008797
client,0.045069
coin,0.008102
collect,0.005707
come,0.005595
commod,0.009496
common,0.004731
commonli,0.006497
commun,0.005116
complet,0.016212
comput,0.006483
concept,0.005460
concis,0.011917
condit,0.005475
configur,0.010809
conform,0.010799
congruenc,0.016876
consist,0.015711
constitut,0.006178
construct,0.006059
consult,0.009035
consum,0.210369
consumpt,0.027379
contact,0.016421
contend,0.011279
context,0.006883
continuum,0.023358
contribut,0.005743
control,0.005198
conveni,0.020895
coron,0.014714
corps,0.015277
correspondingli,0.014776
cost,0.006879
count,0.026772
countri,0.010298
court,0.007421
creat,0.009572
creation,0.007122
cremat,0.016876
crimin,0.019823
current,0.015028
custom,0.055386
cycl,0.008021
data,0.006244
databas,0.009154
day,0.005904
death,0.013706
decept,0.013756
decisionmak,0.011279
defin,0.005370
definit,0.006187
deliv,0.083009
deliver,0.017057
deliveri,0.237501
demand,0.006986
demonstr,0.007033
dental,0.014948
departur,0.012017
describ,0.024081
desk,0.062260
destin,0.010950
determin,0.010868
did,0.005808
differ,0.008296
difficult,0.006824
dimens,0.009003
diplomaci,0.013169
direct,0.005196
dishonest,0.017406
display,0.008893
disput,0.047225
distinct,0.006026
distinguish,0.006947
doe,0.005294
drama,0.013217
dramalurgi,0.024048
dramaturgi,0.019880
dri,0.010249
driver,0.011994
durat,0.022288
dure,0.009008
earlynineteenth,0.024048
econom,0.020622
economist,0.017689
educ,0.006001
effect,0.009153
electr,0.007789
electrician,0.018655
element,0.005991
employe,0.029614
empti,0.011231
enabl,0.007471
encount,0.018557
endur,0.011498
enforc,0.008832
english,0.006561
ensur,0.007739
entertain,0.011583
equip,0.008422
especi,0.005470
essenti,0.006548
establish,0.005027
estat,0.010028
exactli,0.009229
exampl,0.021096
exchang,0.026638
exclus,0.008048
exhaust,0.011015
expand,0.006664
experi,0.005793
express,0.006103
extent,0.007751
extrem,0.006757
fabric,0.023570
facil,0.018060
facilit,0.008493
factor,0.017919
fall,0.006430
famou,0.007782
financi,0.006725
fiscal,0.010680
flight,0.010320
focus,0.006557
follow,0.012089
food,0.007250
foodservic,0.021585
form,0.003850
french,0.006779
frequent,0.007081
fulfil,0.010187
function,0.010315
funer,0.013785
futur,0.006135
ga,0.008475
gambl,0.013951
garden,0.011267
gener,0.007239
gift,0.011371
given,0.005052
good,0.067209
groom,0.016664
group,0.009088
ha,0.010077
hair,0.011994
hairdress,0.094141
harmon,0.012245
health,0.021068
held,0.005956
heterogen,0.025329
highli,0.013263
home,0.021580
hospit,0.009933
howev,0.004088
human,0.014560
humor,0.013642
hygienist,0.021360
idea,0.005653
identifi,0.018268
immateri,0.015107
impair,0.026177
impli,0.007831
implic,0.008777
import,0.004296
incarcer,0.016283
includ,0.010491
inconsist,0.011707
increas,0.009688
independ,0.005174
indic,0.012394
individu,0.005196
induc,0.009975
industri,0.034499
inform,0.010016
ingenu,0.016213
input,0.009126
inquiri,0.010048
insepar,0.045047
institut,0.005330
insur,0.009711
intang,0.059190
intens,0.008366
interact,0.006285
interfac,0.010966
interpret,0.006720
interv,0.010613
inventori,0.013100
involv,0.029301
irrevers,0.013905
item,0.009240
janitor,0.020957
jeanbaptist,0.013656
key,0.012820
labor,0.008270
languag,0.012661
lateeighteenth,0.022098
laundri,0.018129
law,0.015220
lawyer,0.012219
legal,0.006949
lend,0.010820
lib,0.019520
librari,0.007912
like,0.008996
list,0.018731
litig,0.014904
live,0.005445
locat,0.024768
logic,0.008150
lost,0.007318
lovelock,0.018129
lowest,0.010017
maintain,0.005872
mainten,0.010102
make,0.012847
manag,0.024125
mani,0.015225
manicurist,0.024048
manifest,0.009687
manufactur,0.008031
market,0.012607
mass,0.006906
master,0.008987
matrix,0.010571
maximum,0.018314
mean,0.004637
meaning,0.010945
measur,0.005515
mece,0.024048
mechan,0.006218
mediat,0.010316
method,0.010431
militari,0.006812
modern,0.005051
modifi,0.008217
moment,0.028742
money,0.022578
monitor,0.009107
movi,0.023035
multipl,0.006477
museum,0.010154
mutual,0.008882
narrow,0.009509
nation,0.009332
natur,0.004495
necessari,0.013422
need,0.015187
negoti,0.017629
notat,0.011315
number,0.016851
object,0.028783
occup,0.008774
occur,0.010700
occurr,0.010892
offenc,0.014861
offer,0.025048
opportun,0.007954
oppos,0.007037
organ,0.009186
outcom,0.008609
outlet,0.013454
output,0.043948
owner,0.019050
ownership,0.019779
paid,0.008586
paramet,0.019417
parenthet,0.022394
park,0.009842
particip,0.006643
particular,0.010542
passeng,0.025060
patient,0.010257
pay,0.015723
pedicurist,0.024048
perform,0.029600
perhap,0.007828
period,0.020014
perish,0.043055
person,0.005510
perspect,0.015071
phone,0.012017
physic,0.033062
pilot,0.012474
plane,0.010142
play,0.006080
plumb,0.017251
point,0.043963
popular,0.006024
possess,0.015134
possibl,0.004925
power,0.009883
precis,0.007954
prepar,0.023495
prevent,0.013807
previou,0.007282
price,0.035744
principl,0.005796
probabl,0.006929
problem,0.005164
process,0.036520
produc,0.015073
product,0.040263
profess,0.010091
promis,0.009073
prop,0.016283
properli,0.010154
properti,0.005846
protect,0.006435
provid,0.098542
provis,0.028216
public,0.014589
publish,0.005359
purchas,0.008212
pure,0.015271
qualifi,0.010154
qualiti,0.028779
rate,0.005984
ratio,0.008708
reach,0.006223
readi,0.011255
real,0.006356
realli,0.009853
receiv,0.011702
refer,0.021437
regard,0.017627
region,0.005484
regist,0.009496
relat,0.004129
relationship,0.011691
remain,0.005065
remov,0.007070
render,0.040176
repair,0.010902
repeat,0.008839
request,0.046542
resolut,0.037475
resourc,0.037200
respect,0.011648
respons,0.005311
restaur,0.013601
resumpt,0.015825
retail,0.011076
right,0.005495
risk,0.007535
role,0.010729
room,0.009933
rout,0.009220
safekeep,0.019201
say,0.006635
school,0.006230
scopenumb,0.024048
screen,0.011309
script,0.036217
season,0.009896
seat,0.027188
sector,0.008029
secur,0.006506
seen,0.006009
selfservic,0.021360
seller,0.012236
sens,0.006301
sequenc,0.008502
serv,0.006096
servic,0.853273
servicecommod,0.024048
servicerelev,0.024048
servicescap,0.022394
serviceserv,0.024048
servicespecif,0.024048
set,0.009488
sexual,0.010063
shop,0.011427
simultan,0.008932
singl,0.022816
sit,0.022534
site,0.007179
skill,0.008210
smith,0.009030
social,0.016266
societi,0.016020
someon,0.009956
sometim,0.011097
special,0.005330
specif,0.020398
specifi,0.017331
spoc,0.022394
spoken,0.011465
sport,0.010122
stage,0.007443
standard,0.005740
state,0.018646
stock,0.016446
store,0.017270
subsequ,0.006651
success,0.011304
support,0.020270
tabl,0.008202
tangibl,0.027542
target,0.007936
tax,0.008010
taxi,0.049635
teach,0.008349
team,0.008900
technic,0.007580
telecommun,0.011110
televis,0.009607
term,0.024165
termin,0.009986
tertiari,0.012889
theatr,0.012040
theft,0.014229
theme,0.009439
theorist,0.019867
therefor,0.005793
thi,0.028692
tightli,0.013352
time,0.041482
topic,0.006762
total,0.005947
transact,0.009177
transfer,0.007293
translat,0.007337
transport,0.036534
treat,0.007463
trigger,0.010368
truth,0.009068
type,0.005124
typic,0.017154
union,0.006333
uniqu,0.007267
unit,0.004299
unless,0.009551
unproduct,0.015430
unus,0.014926
usag,0.008939
use,0.027861
usual,0.005129
util,0.038554
utiliz,0.024048
valu,0.010925
valuabl,0.010005
valuat,0.011792
vanish,0.012502
vari,0.006283
variabl,0.023374
vehicl,0.009677
view,0.005462
wa,0.003494
wait,0.011557
wast,0.010224
water,0.019724
wealth,0.026331
web,0.009240
willing,0.013020
window,0.011978
wire,0.012313
work,0.016713
worker,0.007934
x,0.007546
