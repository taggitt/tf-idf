academ,0.008741
accademia,0.021450
account,0.006873
activ,0.005823
advanc,0.014416
advic,0.012701
aforement,0.016520
aldershot,0.021679
allyn,0.021601
analysi,0.021105
analyz,0.009810
ancestor,0.012543
andersen,0.020384
anton,0.033844
antonio,0.015198
antoniu,0.022787
appli,0.006239
appoint,0.019187
apprentic,0.019064
apprenticeship,0.019329
approach,0.013433
arrow,0.014984
articl,0.027744
asset,0.010789
associ,0.011402
astrid,0.022188
autobiographi,0.017630
av,0.021377
avail,0.007134
award,0.010288
axiomat,0.016070
base,0.005085
becam,0.038594
becaus,0.005297
becom,0.005633
beekeep,0.023950
believ,0.007278
berg,0.018035
best,0.007756
bibliographi,0.010234
biographi,0.013574
bjerkholt,0.030702
book,0.006688
born,0.009940
bredtveit,0.030702
busi,0.047597
cambridg,0.009334
camp,0.038256
cardin,0.030905
career,0.023124
cassel,0.020970
celebr,0.012147
centr,0.009691
centuri,0.012104
child,0.011319
childhood,0.015061
chosen,0.010770
christiania,0.054757
clair,0.020493
classic,0.007787
coin,0.019521
collect,0.006875
concentr,0.018449
concis,0.014357
conjectur,0.014887
consum,0.009386
continu,0.011423
contribut,0.006918
corecipi,0.025733
correl,0.021976
cours,0.008216
credit,0.009122
cycl,0.028991
daughter,0.012938
david,0.008374
dconomi,0.050964
dean,0.015365
death,0.008256
decemb,0.008650
degre,0.015440
dei,0.018852
den,0.033805
detent,0.018089
develop,0.009429
die,0.018065
difficulti,0.010181
director,0.011334
disciplin,0.008686
dr,0.011696
dure,0.021706
dynam,0.044190
dynamikk,0.030702
earli,0.005676
easiest,0.019028
econom,0.161487
econometr,0.133579
econometrica,0.080503
econometrician,0.024273
economist,0.021310
ed,0.023835
editor,0.012081
educ,0.014460
edward,0.010324
elgar,0.018434
emigr,0.014267
empir,0.015838
emuseum,0.029727
enabl,0.009000
encyclopedia,0.009774
england,0.009848
especi,0.006590
essay,0.043993
estim,0.007986
everi,0.007101
expect,0.007797
explain,0.007640
extend,0.007486
extern,0.004437
faculti,0.026219
famili,0.062869
father,0.019956
fellowship,0.048864
feltrinelli,0.028971
field,0.006197
fisher,0.014973
fluctuat,0.012673
follow,0.019419
foren,0.027831
formal,0.007594
formul,0.019437
foundat,0.015827
founder,0.021274
franc,0.017376
frederick,0.041074
fredrikk,0.028353
friend,0.012054
frisch,0.789784
frischwaugh,0.030702
frischwaughlovel,0.030702
fund,0.008367
gener,0.004360
genet,0.010308
germani,0.018276
given,0.006086
gold,0.021106
goldsmith,0.018465
govern,0.005771
granddaught,0.022680
grandfath,0.017399
grini,0.030702
gustav,0.015528
handicraftsman,0.030702
hasnaoui,0.030702
help,0.013288
henri,0.009781
hi,0.141480
hobbi,0.019028
honor,0.012554
honour,0.028624
howev,0.004925
ideasrepec,0.023799
implement,0.008449
import,0.010352
imprison,0.014430
impuls,0.015554
impulsepropag,0.030702
includ,0.004212
institut,0.006421
introduc,0.042714
invest,0.017281
irv,0.016353
isbn,0.007174
itali,0.010921
j,0.007621
jan,0.026674
januari,0.008661
jewel,0.018374
jewelleri,0.020844
johannessen,0.030702
jointli,0.014641
journal,0.007805
jstor,0.015269
just,0.007076
kenneth,0.013617
kingincouncil,0.030702
kittil,0.028353
kittilsen,0.030702
known,0.005070
kongsberg,0.056707
konomikk,0.030702
konomisk,0.029727
kvantitativ,0.030702
later,0.006034
law,0.018336
lectur,0.022064
liberti,0.025757
librari,0.009532
like,0.005419
lincei,0.022474
linear,0.010795
link,0.004411
lover,0.017903
macroeconom,0.013706
macroeconomicsmicroeconom,0.030702
manag,0.007266
march,0.016826
mari,0.011720
marri,0.012718
matematisk,0.030702
mathemat,0.031902
medal,0.014898
mellem,0.028971
memori,0.021106
metal,0.010559
method,0.006283
methodolog,0.010738
mitchel,0.034001
model,0.018958
modern,0.030426
mother,0.011691
nadia,0.024630
nationalkonomisk,0.030702
nazi,0.014146
nazional,0.023132
nd,0.009439
ne,0.017142
neowalrasian,0.030702
new,0.022658
nobel,0.035429
nobelwinnerscom,0.028971
nordic,0.017513
norsk,0.024273
norway,0.057035
norwegian,0.031536
novemb,0.009099
number,0.010150
obtain,0.007885
occup,0.010571
octob,0.017545
offer,0.007543
og,0.041099
olav,0.022009
oligopoli,0.019329
ordin,0.014962
oslo,0.098744
outlin,0.009874
pair,0.010811
paper,0.041854
partner,0.011090
pass,0.015763
path,0.010286
paul,0.008818
perform,0.014263
philo,0.020331
physic,0.006638
plan,0.007683
popular,0.007258
pp,0.009484
preciou,0.015269
precis,0.009582
press,0.006846
previou,0.008772
primari,0.007679
primrinvest,0.030702
principl,0.006983
prize,0.032549
probabl,0.008347
problem,0.018665
problm,0.048215
process,0.005499
product,0.012126
professor,0.031191
programm,0.011181
propag,0.012962
public,0.005858
publish,0.038742
pure,0.027596
quantit,0.010808
quantiz,0.034494
ragna,0.092107
ragnar,0.325185
read,0.006408
real,0.007657
recapit,0.023516
receiv,0.021146
refer,0.007378
regress,0.014553
reinvest,0.036234
relationship,0.007042
remarri,0.022281
research,0.017956
result,0.005139
resum,0.014112
return,0.014933
rockefel,0.018345
rockefellerfund,0.030702
role,0.006463
royal,0.019469
sammenhengen,0.030702
samuelson,0.018526
scatter,0.027249
schultz,0.020723
scienc,0.024821
scientif,0.007424
seek,0.008279
select,0.024068
sell,0.010437
semin,0.014112
sens,0.007591
seri,0.022050
serv,0.007344
set,0.005715
shortest,0.018144
signific,0.006554
silver,0.025676
silversmith,0.027831
skrifter,0.025248
smedal,0.030702
societi,0.006433
sometim,0.006684
son,0.009831
specif,0.012287
spend,0.010658
spent,0.011349
start,0.025999
state,0.004492
static,0.026270
statikk,0.030702
statist,0.067393
statskonomisk,0.061405
steinar,0.027378
strm,0.026979
studi,0.021706
subject,0.006532
sur,0.032973
teach,0.010058
televis,0.011573
teoretisk,0.030702
teori,0.030702
term,0.009704
test,0.008394
th,0.011775
theorem,0.023021
theoret,0.016838
theori,0.060187
thesi,0.012707
thu,0.006002
tidsskrift,0.080938
time,0.009086
tinbergen,0.040251
today,0.007376
topic,0.008147
travel,0.009066
twenti,0.012629
uk,0.009411
unit,0.005179
univers,0.058499
use,0.004195
util,0.009289
v,0.009155
variabl,0.018773
variat,0.009625
view,0.006581
visit,0.010316
volum,0.008795
wa,0.050522
waugh,0.023950
wesley,0.035165
wide,0.006613
wife,0.013237
word,0.007127
work,0.035237
workshop,0.045596
write,0.008028
written,0.008129
wrote,0.008748
yale,0.014036
year,0.046639
york,0.007908
young,0.009534
