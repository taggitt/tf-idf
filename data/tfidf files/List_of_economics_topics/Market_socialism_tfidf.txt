abandon,0.006557
abba,0.021446
abolish,0.058685
absent,0.008556
accord,0.007021
account,0.004134
accru,0.010250
accumul,0.006734
achiev,0.026498
acquir,0.005718
activ,0.017514
ad,0.004480
adam,0.014249
adjust,0.006719
administr,0.004908
admit,0.007403
adopt,0.004844
advoc,0.036157
affect,0.004587
affin,0.017250
affirm,0.008473
agafonow,0.017427
agenc,0.011045
agricultur,0.005218
aim,0.020052
al,0.006021
alaska,0.021239
albeit,0.008873
alec,0.040740
alejandro,0.012502
alien,0.008226
alli,0.006484
alloc,0.042034
allow,0.017455
alter,0.012594
altern,0.004511
american,0.035798
anarch,0.143335
anarchist,0.217067
anarchisticsoci,0.036937
andrew,0.014259
andrewswilliam,0.017055
ani,0.012375
anoth,0.013506
anticapitalist,0.013088
anticorporatist,0.016228
antiequilibrium,0.017427
antihierarch,0.016228
antiimperi,0.013187
anyon,0.007653
appar,0.005993
apparentthat,0.016741
approach,0.008080
arbitr,0.009948
arbitrarili,0.009694
argu,0.023351
armand,0.012614
articl,0.008344
artifici,0.005928
ask,0.005803
assert,0.006030
associ,0.020576
attack,0.005660
attempt,0.024026
attent,0.011449
attract,0.011297
auckland,0.012361
austrian,0.008409
author,0.007905
autonom,0.014197
avoid,0.005243
b,0.009300
badli,0.009543
bank,0.010153
bardham,0.017882
bardhan,0.030958
baron,0.009340
barter,0.010226
base,0.030589
basi,0.021863
basic,0.008517
basqu,0.011800
beauti,0.008043
becam,0.007738
becaus,0.009559
becom,0.006777
befor,0.003615
begin,0.004127
belaru,0.010832
believ,0.021890
benjamin,0.024080
bent,0.010754
bertel,0.015187
bestknown,0.009228
better,0.004773
big,0.012819
blueprint,0.010931
board,0.024427
bockman,0.018468
book,0.012069
boss,0.012018
brad,0.011852
branko,0.014066
brooklyn,0.012774
built,0.005277
bureaucrat,0.018759
busi,0.019087
buy,0.006696
c,0.004051
calcul,0.027009
capabl,0.011165
capac,0.005876
capit,0.133717
capitalist,0.058625
carri,0.004570
carson,0.011826
carsonlongstyl,0.017882
case,0.003544
cast,0.007201
caus,0.007568
central,0.035768
centuri,0.029124
certif,0.008240
chang,0.003196
chapter,0.006607
character,0.005377
characterist,0.009773
charl,0.010859
chartier,0.032028
check,0.007350
chief,0.005903
china,0.010894
chines,0.048753
chri,0.009543
cincinnati,0.012576
circl,0.006831
cite,0.005929
citizen,0.005802
claim,0.004303
clarendon,0.010311
class,0.023512
classic,0.018737
clearli,0.006375
close,0.003793
collect,0.016543
coloni,0.005862
combin,0.004043
command,0.011897
commodityexchang,0.018468
common,0.003428
commun,0.018538
compani,0.004477
comparison,0.005945
compet,0.005846
complet,0.007832
compositionsautonomedia,0.018468
comprehens,0.006245
concentr,0.011098
concept,0.007914
concern,0.003980
conclud,0.005799
conclus,0.006318
condit,0.007935
consciou,0.016769
conscious,0.007223
consid,0.016534
consist,0.007590
constitut,0.004477
consum,0.011292
consumerownership,0.018468
contain,0.004067
contemporari,0.027733
continu,0.003435
contradict,0.007574
contrast,0.004790
contribut,0.004161
control,0.011302
coop,0.012733
cooper,0.054197
coordin,0.006011
corpor,0.033380
corporat,0.012692
correspond,0.005147
cost,0.024927
countri,0.003731
coupon,0.010931
cover,0.004668
cpb,0.032028
credit,0.005487
crise,0.008748
criterion,0.008514
critic,0.004171
croatborn,0.018468
cultur,0.008527
curtail,0.010286
czechborn,0.016468
czechoslovakia,0.011182
d,0.009181
date,0.004739
david,0.020148
deal,0.004568
debat,0.021116
decentr,0.018530
decentralis,0.011557
decis,0.009561
decisionmak,0.008173
decompos,0.009192
decre,0.016287
defenc,0.006902
defend,0.012689
degre,0.009287
dehomogen,0.017427
demand,0.010125
democraci,0.012632
democrat,0.022224
depend,0.007483
depriv,0.027323
describ,0.017451
desir,0.005336
determin,0.007875
develop,0.014179
dickinson,0.075918
did,0.021046
differ,0.009018
differenti,0.005741
direct,0.015061
directli,0.009112
disappear,0.021629
displac,0.007275
disput,0.005703
distinct,0.004367
distinguish,0.010068
distort,0.008006
distribut,0.018715
dividend,0.027343
divis,0.004914
doe,0.007673
domin,0.004666
driven,0.006601
droit,0.013774
drucker,0.012903
du,0.016029
dub,0.009121
dure,0.026113
dynam,0.005316
e,0.009190
earli,0.047804
earlier,0.005044
earliest,0.005706
earlytomid,0.015187
earn,0.012833
easi,0.006933
econom,0.141972
economi,0.228458
economist,0.083324
ed,0.009558
edit,0.016030
edward,0.006210
effect,0.009949
effici,0.016036
egalitarian,0.010493
elect,0.005035
element,0.013025
elimin,0.017711
els,0.007030
embodi,0.007893
emerg,0.017501
emil,0.009604
employe,0.021460
employeeown,0.015642
encompass,0.006391
encourag,0.011222
end,0.007465
energi,0.004779
engag,0.005406
enjoy,0.006499
enrico,0.011035
ensur,0.005608
entail,0.007966
enter,0.004990
enterpris,0.089965
enthusiast,0.009731
entrepreneuri,0.021239
entrepreneurship,0.010324
envis,0.018651
equal,0.041843
equat,0.022712
equilibrium,0.014270
equit,0.010179
equiti,0.023434
equival,0.005499
error,0.012983
establish,0.014573
et,0.011736
eunic,0.029864
everi,0.004272
everyon,0.015764
everyth,0.006798
exactli,0.006688
exchang,0.024129
exert,0.007833
exhaust,0.007982
exil,0.007797
exist,0.027159
expand,0.009658
expect,0.004690
experi,0.004198
experiment,0.005702
explicitli,0.006950
exploit,0.050842
exposit,0.009356
express,0.004423
expropri,0.011403
extern,0.002669
eye,0.006824
fabric,0.008540
facilit,0.006154
fact,0.008669
factor,0.012986
fall,0.004660
farmer,0.007226
fatal,0.008873
father,0.006002
favor,0.005638
favour,0.020140
feasibl,0.016589
feder,0.005435
federalist,0.011580
fellowlabour,0.018468
fight,0.012811
figur,0.005056
final,0.004415
financ,0.005409
financi,0.024368
firm,0.030486
firmli,0.008923
fiscal,0.007739
flat,0.007807
follow,0.005840
forbid,0.009817
foreign,0.004752
form,0.039065
format,0.004979
forward,0.006480
foundat,0.004760
fourpag,0.030375
framework,0.005230
fred,0.009468
free,0.045868
freed,0.009411
freedom,0.011526
freemarket,0.053770
freepric,0.017882
french,0.019651
frequent,0.005131
friendship,0.009066
function,0.007475
fund,0.025167
fuse,0.008987
gari,0.018215
gender,0.007826
genealog,0.009846
gener,0.013116
genuin,0.016937
gift,0.008240
gimenez,0.015642
global,0.004848
goal,0.005021
gold,0.006348
good,0.036528
gorbachev,0.011800
goulash,0.018468
gouvern,0.014229
govern,0.041658
gradual,0.005868
greater,0.004598
green,0.012556
ground,0.005312
groundwork,0.010311
h,0.010142
ha,0.021908
half,0.005025
hand,0.004523
happen,0.005769
harmoni,0.008514
harpercollin,0.011490
hayek,0.011299
hayekian,0.017055
heavi,0.006309
height,0.014557
held,0.004316
help,0.003996
hi,0.042552
high,0.003696
highli,0.004805
hillel,0.013642
histor,0.008201
histori,0.003040
historian,0.012303
hodgskin,0.015642
hold,0.004397
horvat,0.015819
hour,0.006415
howev,0.008888
human,0.003517
hungari,0.009040
idea,0.020486
identifi,0.004412
ideolog,0.006895
igualada,0.016014
iii,0.006981
illyrian,0.013843
implement,0.010165
improv,0.013476
incent,0.007600
includ,0.025341
incom,0.033549
increas,0.010531
increasingli,0.005381
independ,0.003749
indiana,0.009221
indic,0.013472
indirect,0.007931
individu,0.026358
individualist,0.105625
industri,0.012500
inequ,0.007680
influenc,0.008041
influenti,0.005871
infrastructur,0.012361
inher,0.006996
initi,0.003966
inquiri,0.007281
insist,0.007448
insofar,0.010110
institut,0.003862
integr,0.004405
intellectu,0.006262
intend,0.005651
intern,0.003199
introduc,0.021411
invest,0.031186
involv,0.014156
inwardli,0.014316
irrespons,0.012430
issu,0.003917
itali,0.006569
item,0.006696
jame,0.005148
jano,0.014501
jaroslav,0.013580
johanna,0.013774
john,0.020761
johnson,0.016683
join,0.005429
jointstock,0.012614
joseph,0.006015
josiah,0.053693
journal,0.009390
just,0.012769
justic,0.006169
karl,0.006865
kept,0.006577
kevin,0.009578
key,0.009290
kind,0.004862
konkin,0.016228
kornai,0.015642
la,0.006092
label,0.026270
labor,0.083903
labour,0.061792
lack,0.004653
land,0.009238
lang,0.060528
langedickinson,0.018468
langelern,0.035764
langetaylor,0.018468
lanham,0.011826
larg,0.006433
late,0.004225
later,0.018148
lawler,0.014932
lay,0.006868
le,0.007270
lead,0.010924
learn,0.005127
left,0.014595
leftlibertarian,0.118526
leftw,0.060395
legal,0.005036
legitim,0.007580
legitimaci,0.009340
lend,0.007841
leninist,0.013843
leon,0.009080
lerner,0.024094
liber,0.016508
libertarian,0.048289
liberti,0.015494
limit,0.011181
literatur,0.005452
littlefield,0.011651
live,0.007892
loan,0.007080
local,0.012400
logic,0.011813
long,0.015850
longterm,0.006422
lose,0.006418
loss,0.005337
lower,0.009019
lucid,0.012465
lysand,0.014066
m,0.004364
machineri,0.007526
macroeconom,0.024734
main,0.003863
maintain,0.008510
major,0.006475
make,0.015516
man,0.016198
manag,0.030595
mandatori,0.009379
mani,0.011033
manifest,0.007020
mankind,0.008807
manner,0.011779
margin,0.020064
markedli,0.010439
market,0.511633
marketbas,0.022402
marketdistort,0.018468
marketmechan,0.018468
marketori,0.035638
marshal,0.008088
marx,0.025589
marxian,0.021896
marxismlenin,0.012774
marxistleninist,0.011279
maryland,0.010133
mathemat,0.004797
matthew,0.008849
mean,0.026887
measur,0.003997
mechan,0.027040
member,0.003893
men,0.028294
met,0.006240
microeconom,0.009501
miguel,0.010493
mikhail,0.009613
miller,0.008519
minett,0.029864
minim,0.006440
minut,0.007284
mix,0.023479
mode,0.006870
model,0.125445
modern,0.007320
modif,0.007253
mondragon,0.015055
monetari,0.013084
monopoli,0.007818
monopolist,0.011052
moral,0.006105
mutual,0.064372
mutualcredit,0.015187
mutualist,0.036141
nation,0.003381
natur,0.019547
necess,0.022941
necessari,0.019453
need,0.011005
neg,0.005291
negat,0.019425
neoclass,0.083120
neoliber,0.010262
nep,0.014229
net,0.006415
new,0.013629
nicknam,0.009685
nonetheless,0.007633
nonexploit,0.018468
nonmarket,0.037616
norway,0.017154
notabl,0.014048
note,0.007036
nove,0.048686
number,0.003052
nyminor,0.018468
o,0.006435
object,0.004171
occasion,0.006884
occup,0.006358
occur,0.003877
odonnel,0.041745
offer,0.004537
offici,0.004633
oil,0.006091
ollman,0.016741
onli,0.016854
open,0.008355
oper,0.023099
oppos,0.015299
opposit,0.005021
oppress,0.009000
organ,0.003328
origin,0.010145
oskar,0.022070
ought,0.017520
outlin,0.011880
outlook,0.008748
output,0.006369
overlap,0.006782
owen,0.020243
owner,0.020708
ownership,0.114667
oxford,0.011467
p,0.009317
paid,0.006222
paper,0.010070
pareto,0.021477
pari,0.006673
parliament,0.006154
participatori,0.010999
particular,0.003819
pass,0.004740
pattern,0.005206
pay,0.017091
peac,0.011770
pearl,0.028259
pedest,0.013580
pension,0.033914
peopl,0.013458
perestroika,0.012465
perfect,0.006818
perform,0.004290
perhap,0.005672
period,0.014504
perman,0.005913
person,0.011979
perspect,0.005460
peter,0.005564
phase,0.005868
philosoph,0.032138
philosophi,0.020782
phrase,0.014706
pierr,0.007855
pierrejoseph,0.038981
plan,0.078567
planner,0.019370
plate,0.007924
pluralist,0.010453
pointless,0.012327
polici,0.027096
polit,0.034471
politic,0.012076
popul,0.008683
portion,0.006132
posit,0.003616
possess,0.016451
possibl,0.003569
potenti,0.004530
poverti,0.021596
power,0.007162
practic,0.014976
pranab,0.015055
predomin,0.015965
prefer,0.005358
present,0.010497
press,0.016474
pretend,0.010832
prevent,0.005002
preview,0.022070
price,0.088069
princip,0.005466
principl,0.029404
print,0.013143
prioriti,0.007379
privat,0.060212
privileg,0.023960
problem,0.014970
process,0.003308
produc,0.021846
product,0.091180
productthi,0.018468
profit,0.060461
program,0.004402
prolabor,0.016014
promis,0.013151
promot,0.019967
properti,0.046606
propon,0.036663
propos,0.069080
proprit,0.015328
prospect,0.007436
proudhon,0.114597
proudhonian,0.062569
prove,0.005400
provid,0.009314
provoc,0.011467
public,0.031718
publicli,0.007488
publish,0.027188
purest,0.024589
pursu,0.006364
push,0.006654
que,0.012692
questc,0.016228
question,0.004394
quota,0.009578
r,0.004682
race,0.006875
radic,0.019245
rais,0.010323
rate,0.008673
read,0.007709
receiv,0.016960
recent,0.007686
recherch,0.011651
reciproc,0.008337
recompens,0.014066
recoveri,0.007388
red,0.006151
refer,0.019973
reflect,0.004769
reform,0.016286
refractori,0.026081
regard,0.012773
regul,0.010508
reinvest,0.010898
relat,0.005984
relationship,0.004236
releg,0.011279
reli,0.016043
reliev,0.009287
remain,0.014682
remov,0.020496
remuner,0.011580
rent,0.042727
replac,0.009045
repres,0.014562
republ,0.020835
repugn,0.014229
requir,0.010481
resourc,0.022465
respect,0.004220
respond,0.011971
rest,0.005199
restrict,0.010287
result,0.006182
retain,0.005836
revenu,0.012949
review,0.004972
revis,0.006632
revolut,0.005497
revolutionist,0.028458
reward,0.007677
ricardian,0.038981
richard,0.005482
richman,0.015819
riddl,0.012395
right,0.019913
rightlibertarian,0.014932
robert,0.004879
roderick,0.011774
roemer,0.071580
role,0.019439
root,0.016818
rowman,0.011961
s,0.015738
said,0.018239
sale,0.006465
samuel,0.007889
satellit,0.007743
satisfi,0.006583
saw,0.005431
say,0.004808
scheme,0.013359
schewikart,0.018468
scholar,0.005636
school,0.004515
schuster,0.021416
schweickart,0.032028
sciabarra,0.017055
scienc,0.003732
scientist,0.005074
scrutini,0.010088
secondari,0.006319
sector,0.023273
selfmanag,0.047517
selfownership,0.015187
selfregul,0.010931
sell,0.018834
sens,0.009133
servic,0.012972
set,0.017189
sever,0.003220
sexual,0.007292
share,0.008649
sheldon,0.011512
short,0.004732
shortag,0.024178
signific,0.007885
similar,0.014753
simon,0.007421
simul,0.026845
simultan,0.006472
sinc,0.012354
singl,0.004133
sketch,0.009906
slaveri,0.008139
small,0.003779
smallscal,0.009543
smith,0.013088
social,0.306481
socialist,0.269484
socialistori,0.030111
sociallyown,0.015055
societi,0.015479
sold,0.006522
solut,0.010568
solv,0.011368
someth,0.011877
sometim,0.004020
sourc,0.011655
sovereign,0.007382
soviet,0.013380
soviettyp,0.028132
spangler,0.015642
spanish,0.006679
specif,0.003695
specul,0.006426
split,0.006348
spooner,0.014066
squar,0.006544
st,0.004979
stabil,0.005760
stage,0.010787
stanc,0.008514
standard,0.004160
stanford,0.014349
start,0.003909
state,0.056754
stateown,0.036376
staterun,0.011580
statesupport,0.015055
statist,0.015202
steinervallentyn,0.016741
stephen,0.020583
steven,0.015565
stock,0.029795
store,0.025030
stress,0.012667
strongli,0.005953
structur,0.007217
stuart,0.017133
subject,0.003929
subsidi,0.017207
substanti,0.005733
substitut,0.006863
success,0.012288
suggest,0.004324
suppli,0.005035
support,0.011017
suppress,0.014146
sur,0.009917
surplu,0.016252
surplus,0.021896
systemat,0.011802
t,0.005374
taken,0.004522
talk,0.006646
tape,0.010076
task,0.005729
tax,0.005804
taxat,0.015187
taylor,0.007771
teach,0.006050
technocraci,0.015479
tend,0.005072
term,0.023348
test,0.005049
text,0.005328
th,0.021250
thcenturi,0.007122
theft,0.010311
themselv,0.009163
theoret,0.025322
theori,0.032583
theorist,0.007198
therefor,0.012594
thi,0.030033
thing,0.014744
think,0.005356
thoma,0.005525
thoroughli,0.009788
thought,0.017510
thrive,0.008550
thu,0.003610
ticktin,0.018468
tie,0.006024
time,0.013664
titl,0.011088
titoism,0.036937
took,0.004825
tool,0.004945
trace,0.005922
trade,0.013404
tradit,0.007778
transform,0.015168
transit,0.016132
tree,0.006362
trial,0.014095
tucker,0.068419
twentieth,0.007287
type,0.011140
underli,0.005865
unearn,0.013402
union,0.013769
unit,0.009346
univers,0.015995
unlik,0.010287
unown,0.015328
unstabl,0.008240
unsurprisingli,0.013707
use,0.022714
ussr,0.017846
usuri,0.023758
util,0.027939
utopia,0.010881
valu,0.023752
vanek,0.014316
variat,0.005789
variou,0.003516
various,0.009552
version,0.005402
verso,0.012903
vertiginouslythat,0.017882
viabl,0.008499
vietnam,0.008370
vietnames,0.010336
view,0.015835
voic,0.007361
vol,0.006638
voluntari,0.008545
w,0.005182
wa,0.070911
wage,0.020361
wake,0.008240
walra,0.012361
want,0.005702
warren,0.090201
weaken,0.007736
wealth,0.019081
weekli,0.018327
welfar,0.007114
western,0.004646
whatev,0.007603
wherebi,0.013957
whereon,0.016741
wide,0.019890
william,0.009898
wing,0.016818
wolff,0.011163
work,0.036336
worker,0.051747
workpeopl,0.018468
wright,0.009163
write,0.004829
year,0.003117
yugoslavia,0.031319
zealand,0.007344
