absenc,0.015838
abstract,0.015159
acquisit,0.018627
action,0.010585
activ,0.017192
actual,0.011025
advent,0.018840
affect,0.022515
air,0.026468
allow,0.017134
alway,0.034754
amplif,0.026743
analyz,0.014481
ani,0.015185
anoth,0.008286
appear,0.019957
artifact,0.020412
ask,0.014241
aspect,0.011322
astronomi,0.019342
attach,0.016310
attent,0.014048
automobil,0.021584
avail,0.010531
away,0.026246
bad,0.035345
base,0.007506
beat,0.021852
becom,0.016631
befor,0.017743
begin,0.010129
begun,0.016516
behavior,0.048742
besid,0.017773
better,0.011714
bia,0.297127
bias,0.064579
big,0.015729
biotechnolog,0.022628
blind,0.042522
bring,0.012914
build,0.021985
built,0.012950
camcord,0.037986
camera,0.044975
canal,0.020635
care,0.013070
cargo,0.046430
caus,0.027858
centuri,0.008934
certain,0.009522
chain,0.015543
chang,0.031375
check,0.018037
child,0.016710
clock,0.021776
close,0.009309
cognit,0.016495
cold,0.016585
collaps,0.030654
collect,0.010149
come,0.019903
common,0.008413
compar,0.041167
comparison,0.029182
compet,0.014346
competit,0.013682
complex,0.010182
computer,0.051045
conclus,0.077523
confirm,0.045005
consciou,0.020576
conscious,0.017726
consequ,0.035498
consid,0.016230
consist,0.018627
construct,0.010775
copi,0.016776
correct,0.042039
count,0.031740
counter,0.018815
cult,0.045473
cultur,0.020926
data,0.122162
deal,0.022420
decad,0.012649
deduct,0.020134
defens,0.030547
definit,0.011003
deliber,0.037254
depend,0.036727
descript,0.012720
desir,0.026193
determin,0.009663
develop,0.013918
differ,0.088525
difficult,0.024273
digit,0.015539
direct,0.009240
disagre,0.020005
discoveri,0.065166
doubl,0.015406
draw,0.014265
drawn,0.016422
duplic,0.022828
dure,0.008010
dye,0.023834
earli,0.008379
easi,0.017014
effect,0.040694
emerg,0.010737
emphas,0.029407
employ,0.011112
encount,0.016500
end,0.009160
enhanc,0.031035
entir,0.021733
entropi,0.022736
equal,0.011409
erron,0.023996
error,0.031862
event,0.043610
everyday,0.017896
everyth,0.016682
exampl,0.045019
exist,0.016662
expect,0.023021
experi,0.061814
experiment,0.027987
explan,0.014747
extens,0.011313
extern,0.006551
eyewit,0.029492
fact,0.010637
factor,0.010622
falsif,0.028592
fast,0.017150
favor,0.013837
featur,0.011330
field,0.045745
fifth,0.017841
filter,0.020881
fit,0.030506
follow,0.007166
forgotten,0.023656
form,0.034238
formul,0.014346
frame,0.034240
function,0.045860
fusion,0.020881
gap,0.017439
gather,0.015617
geiger,0.031886
given,0.026955
goe,0.016121
good,0.019920
goodfaith,0.038386
great,0.010096
ha,0.023894
happen,0.014158
havent,0.031556
hear,0.018891
held,0.010591
home,0.025584
howev,0.021811
human,0.077679
hypothes,0.035628
hypothesi,0.062418
idea,0.010054
illus,0.022119
illustr,0.015277
imag,0.055177
imparti,0.027302
implic,0.015609
import,0.022922
impress,0.036572
incent,0.037303
incom,0.013721
increas,0.008614
independ,0.009201
indic,0.011020
individu,0.018481
inform,0.017812
infrar,0.023135
input,0.016230
insignific,0.025920
instrument,0.096561
interact,0.022355
interferomet,0.030862
intern,0.015700
introspect,0.027487
investig,0.012553
involv,0.017369
irrat,0.023076
irrevers,0.024728
isol,0.029089
issu,0.009613
journal,0.023044
judg,0.016543
kept,0.016141
laboratori,0.015200
larg,0.007893
later,0.008907
learn,0.012581
length,0.014859
let,0.016781
light,0.024695
like,0.015999
limit,0.009146
list,0.008328
live,0.019367
locat,0.011011
logic,0.014495
look,0.012365
loos,0.018619
love,0.017780
machin,0.028920
magnifi,0.027729
main,0.009481
make,0.038078
mani,0.013538
martian,0.029708
mass,0.012281
measur,0.078472
mechan,0.022118
memori,0.031156
method,0.037100
microscop,0.019333
mind,0.014122
minim,0.015806
misinterpret,0.027348
misjudg,0.036947
mistaken,0.024728
mixtur,0.037449
model,0.018657
modern,0.008982
moral,0.014982
motion,0.030223
myth,0.019125
n,0.013449
natur,0.023985
naturalist,0.020401
near,0.012313
need,0.009002
new,0.026757
normal,0.011936
note,0.008634
noth,0.015847
notic,0.016997
notori,0.023767
number,0.022475
numer,0.011094
object,0.092137
observ,0.691880
observatori,0.023155
occur,0.019029
onli,0.013787
optic,0.018015
order,0.008252
origin,0.008299
oscilloscop,0.035131
outcom,0.045931
overlook,0.023135
paper,0.012356
paradox,0.076607
parent,0.016865
particularli,0.010502
pass,0.011634
passag,0.017841
patholog,0.021288
peer,0.041763
peopl,0.016513
perceiv,0.015060
percept,0.063761
permit,0.015253
perpetu,0.020288
persuad,0.021642
pet,0.023744
phenomena,0.014298
phenomenon,0.092541
philosoph,0.013144
philosophi,0.012750
physic,0.039197
place,0.008469
plausibl,0.022384
play,0.021625
point,0.008686
polywat,0.041083
posit,0.008875
possibl,0.035036
power,0.008788
practic,0.009187
predict,0.025130
prefer,0.013150
presenc,0.012986
present,0.008586
preserv,0.014474
pressur,0.025853
preval,0.018908
primari,0.011335
principl,0.010308
problem,0.009184
process,0.146129
properti,0.031192
pseudosci,0.026204
psycholog,0.029275
psychologist,0.019678
publish,0.019063
qualifi,0.018058
qualit,0.039822
quantifi,0.019296
quantit,0.015954
quantiti,0.013987
quantum,0.051961
question,0.021568
racial,0.021087
racism,0.025243
radio,0.017580
rampant,0.027212
rate,0.021284
ration,0.014799
raw,0.035532
ray,0.018603
reach,0.011068
receiv,0.020810
recent,0.018862
reconstruct,0.017067
record,0.091839
reduc,0.020635
refer,0.016338
rel,0.019530
relat,0.007343
relationship,0.020792
relativist,0.044975
rememb,0.039989
repeat,0.015720
repetit,0.022217
repres,0.008934
represent,0.013739
reproduc,0.018477
requir,0.034294
research,0.114862
resolut,0.016660
respect,0.010357
rest,0.012760
result,0.113797
review,0.024406
revisednew,0.042766
reward,0.018840
role,0.009540
scale,0.012327
scandal,0.022999
schema,0.053569
scienc,0.073280
scientif,0.164394
second,0.008938
seen,0.021372
sens,0.089653
sensor,0.022575
sensori,0.021113
separ,0.009950
serum,0.028476
set,0.008436
sever,0.007901
share,0.010612
sight,0.020210
similarli,0.014360
simpl,0.012043
simul,0.016469
sinc,0.007579
singl,0.020287
slower,0.021776
small,0.009275
smell,0.024756
someth,0.014573
sometim,0.009867
sourc,0.009534
specif,0.009068
specifi,0.015410
spectromet,0.030955
speed,0.031000
standard,0.051045
state,0.033160
stay,0.016591
step,0.025705
store,0.015356
streetlight,0.041083
studi,0.032041
subject,0.019284
superior,0.016871
superposit,0.026241
support,0.027036
tape,0.024728
tast,0.021274
techniqu,0.046592
telescop,0.023296
term,0.014324
test,0.037173
testimoni,0.024387
th,0.008691
themselv,0.011243
theori,0.008884
therebi,0.015688
thermodynam,0.019455
thermomet,0.027394
thi,0.090710
think,0.026289
thought,0.021484
thrown,0.025243
thu,0.017721
time,0.026825
tire,0.026703
tissu,0.018316
togeth,0.010452
touch,0.020005
translat,0.013049
trip,0.020945
twin,0.066900
type,0.018226
ultim,0.013110
uncertainti,0.017639
unconsci,0.045328
unenhanc,0.038386
unobserv,0.051980
unreli,0.024286
use,0.037160
usual,0.009121
valu,0.029144
variou,0.008628
veloc,0.039864
veri,0.008656
version,0.013258
video,0.016682
view,0.029144
violat,0.016770
voltmet,0.035131
wa,0.006214
want,0.027987
wave,0.061143
way,0.016209
weigh,0.021357
whi,0.013946
wide,0.009762
world,0.023053
write,0.011851
xray,0.041344
younger,0.018832
