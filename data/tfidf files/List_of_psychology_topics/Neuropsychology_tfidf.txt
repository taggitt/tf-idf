aacn,0.021354
abil,0.012322
abnorm,0.033434
academ,0.006707
accept,0.005622
account,0.015822
accuraci,0.009711
acknowledg,0.008635
act,0.005118
action,0.011004
activ,0.040214
actual,0.017193
administ,0.008778
admit,0.009443
adult,0.008766
advanc,0.016592
advantag,0.006951
affect,0.017555
aim,0.006394
allow,0.004453
alon,0.015316
altern,0.005754
alway,0.012043
analyz,0.007527
anatom,0.023889
anatomi,0.010516
ancient,0.006488
anim,0.025071
anoth,0.012921
anterior,0.014388
apart,0.016412
apartheit,0.021354
app,0.014098
appear,0.005186
appli,0.009574
applic,0.010696
approach,0.061844
area,0.071161
argu,0.005957
arguabl,0.010913
argument,0.007523
arithmet,0.010933
arm,0.007140
articul,0.021630
artifici,0.007562
asid,0.010213
assess,0.030510
associ,0.004374
attempt,0.010215
attend,0.008544
attent,0.021906
autom,0.020041
automat,0.009141
autopsi,0.015767
away,0.006821
axial,0.014144
b,0.005931
background,0.007690
bacteriolog,0.015559
bad,0.018372
base,0.019509
batteri,0.024261
bc,0.007988
beauti,0.010259
becam,0.004935
becaus,0.008129
becom,0.004322
befor,0.009223
begin,0.010530
behavior,0.107676
belief,0.007228
believ,0.022338
benefici,0.009909
benton,0.017570
best,0.005951
better,0.006089
binari,0.010783
biolog,0.006403
blame,0.010726
bodi,0.081274
bodili,0.011799
book,0.005131
boston,0.009955
bouillaud,0.106773
brain,0.639404
brainbehavior,0.021007
braindamag,0.018758
break,0.007442
breath,0.011608
bring,0.006712
broca,0.083170
build,0.005713
burial,0.012273
cambridg,0.007162
came,0.023651
cantab,0.021354
capabl,0.014242
capac,0.007496
card,0.010275
carri,0.017489
case,0.009043
cast,0.018372
cat,0.011472
categor,0.009193
caus,0.009653
cell,0.015555
center,0.005767
centuri,0.013931
certain,0.004949
chicago,0.017628
children,0.014875
circul,0.009462
cite,0.022691
claim,0.021959
classic,0.011950
clinic,0.047159
clinicaltri,0.019953
close,0.004839
closer,0.008948
clue,0.013455
cn,0.027318
cnsv,0.021354
cognit,0.102892
coin,0.007489
colleagu,0.009775
collect,0.005275
come,0.005172
commit,0.007580
common,0.004373
commun,0.009458
compar,0.016048
complementari,0.010551
complet,0.014986
complex,0.026463
comput,0.011986
concept,0.010095
concern,0.015233
conclud,0.007397
concret,0.019818
conduct,0.006199
connect,0.005955
connection,0.015724
consequ,0.006150
consid,0.021091
consist,0.004841
constant,0.007190
constitut,0.005711
constrain,0.010516
consult,0.008352
contain,0.005189
continu,0.021913
contribut,0.010617
contributor,0.021161
control,0.033639
controversi,0.014255
convinc,0.009628
correl,0.008431
cortic,0.087602
countri,0.004760
credit,0.006999
crucial,0.008805
ct,0.012522
cut,0.007679
daili,0.008459
damag,0.015611
data,0.028863
day,0.005458
debat,0.026936
decad,0.006575
decid,0.007049
decis,0.006098
deeper,0.010808
deepli,0.010213
degre,0.005923
delv,0.015085
dementia,0.030660
demonstr,0.006501
depend,0.004772
descart,0.090553
deserv,0.012811
design,0.021251
despit,0.012266
determin,0.010046
determinist,0.023096
develop,0.018087
dewsburi,0.038409
diagnosi,0.021949
did,0.016108
dietari,0.013316
differ,0.026841
difficulti,0.007811
directli,0.005811
director,0.008697
discard,0.024197
disciplin,0.033325
discov,0.025479
discoveri,0.033873
diseas,0.007576
disord,0.027972
disput,0.007275
disregard,0.012485
dissatisfi,0.014656
distil,0.012954
distinct,0.005570
divorc,0.012152
doe,0.004894
donald,0.010197
door,0.011311
drew,0.009968
drive,0.007771
dualism,0.012585
dure,0.008327
dynasti,0.009405
dysfunct,0.038820
earlier,0.006434
earliest,0.007278
eeg,0.015600
effect,0.016922
effort,0.006030
egypt,0.009196
egyptian,0.019630
electr,0.014401
electroencephalographi,0.015901
electrophysiolog,0.031449
emerg,0.011162
emiss,0.010333
emot,0.008945
emphasi,0.007898
employ,0.005776
engram,0.038409
entir,0.005648
environment,0.007001
equipotenti,0.017658
eras,0.014053
error,0.008280
especi,0.015170
essenti,0.012107
establish,0.004647
eventu,0.006189
evid,0.011876
examin,0.006815
exampl,0.019500
exclus,0.007439
exercis,0.007482
expand,0.030801
experi,0.005355
experiment,0.050917
expert,0.008601
explain,0.011724
explan,0.007665
explanatori,0.012273
explor,0.006635
expos,0.017445
extern,0.003405
face,0.006536
factor,0.005521
fail,0.006547
fals,0.008832
famou,0.007194
father,0.007656
featur,0.017668
field,0.042800
final,0.005632
firm,0.007777
florida,0.011431
flourish,0.009584
fmri,0.030808
focu,0.006388
focus,0.012123
follow,0.003725
fooya,0.021354
forc,0.004667
forens,0.012473
forget,0.025998
forgot,0.017658
form,0.007118
format,0.006351
forward,0.008266
foundat,0.006072
franz,0.049185
fresh,0.010459
fruit,0.009227
fulli,0.007020
function,0.143027
futur,0.011343
gall,0.121500
gener,0.006692
genet,0.031637
german,0.006576
given,0.004670
gland,0.026211
global,0.006184
gloriou,0.014191
god,0.008150
got,0.010505
govern,0.004428
great,0.005248
greater,0.005865
greatest,0.008194
greek,0.006594
group,0.012601
growth,0.005894
ha,0.043470
habit,0.009872
harvard,0.009097
head,0.006011
healthi,0.010167
heart,0.017094
heartbeat,0.017169
height,0.009284
heil,0.019744
help,0.005097
hemispher,0.031813
hi,0.112731
high,0.004715
higher,0.016607
highli,0.012260
hippocr,0.054037
histori,0.015514
hitler,0.012689
hopkin,0.011472
hospit,0.009182
howev,0.026454
human,0.035891
hundr,0.014150
hysteria,0.016294
idea,0.057490
ill,0.036167
imag,0.021510
imagin,0.009309
imhotep,0.054783
immort,0.012330
impair,0.012098
import,0.003971
includ,0.012930
independ,0.014348
indepth,0.013509
indic,0.005728
individu,0.038425
industri,0.005315
influenc,0.041031
inform,0.018517
injuri,0.072259
input,0.008436
inspir,0.015408
institut,0.009855
intellig,0.036328
interact,0.005810
interdisciplinari,0.009572
intradisciplinari,0.021354
intric,0.038864
introduc,0.016387
invent,0.007576
investig,0.013050
involv,0.022571
item,0.017083
jeanbaptist,0.025248
john,0.010593
johnson,0.010640
joint,0.008199
joseph,0.015346
just,0.005429
juvenil,0.013678
karl,0.017515
know,0.014110
knowledg,0.005856
known,0.019453
laboratori,0.023702
lack,0.005936
laid,0.008787
larg,0.008205
larger,0.006145
lashley,0.199656
late,0.005390
layer,0.008961
learn,0.039239
leav,0.006592
led,0.015073
left,0.006205
lesion,0.043884
letter,0.007685
level,0.009310
life,0.005004
like,0.004158
limit,0.004754
line,0.011143
link,0.020310
littl,0.005958
live,0.005033
lobe,0.014168
local,0.021090
locu,0.012650
longlast,0.014337
look,0.070705
lower,0.011504
machinelik,0.020701
magic,0.010854
magnet,0.036553
magnetoencephalographi,0.017096
major,0.024779
make,0.011875
man,0.027550
manag,0.005575
mani,0.042222
manic,0.017169
map,0.021364
mass,0.012767
master,0.008307
matter,0.005855
maze,0.030660
mean,0.004287
measur,0.030592
mechan,0.005748
medic,0.021505
medicin,0.029687
medicoleg,0.019552
meg,0.017024
memori,0.032390
mental,0.025270
method,0.038568
methodolog,0.008239
midth,0.009604
mind,0.102773
mindbodi,0.028336
minnesota,0.012547
minor,0.014407
minut,0.009291
mobil,0.008268
model,0.024244
monkey,0.012770
mortal,0.020808
mostli,0.006641
motor,0.010039
mous,0.012784
mri,0.026982
multipl,0.011974
mutual,0.008211
mysteri,0.010275
nadin,0.019953
nan,0.016402
natur,0.004155
negro,0.014742
nelsondenni,0.021354
nervou,0.042634
network,0.013015
neural,0.041839
neuroanatomi,0.016190
neuroanatomist,0.018758
neurocognit,0.054133
neuroimag,0.029974
neurolog,0.082465
neuron,0.022749
neuropsychiatri,0.038409
neuropsycholog,0.335169
neuropsychologist,0.051967
neurosci,0.032197
new,0.013908
norm,0.009028
normal,0.012409
notabl,0.005973
noteworthi,0.013153
object,0.005321
observ,0.020550
obsolet,0.012141
occur,0.004945
offer,0.005788
onc,0.011633
onli,0.007166
open,0.005329
oper,0.004910
opinion,0.008273
opposit,0.006405
oral,0.010059
orang,0.011635
order,0.004289
organ,0.042460
outcom,0.007958
outlook,0.011159
overli,0.012461
oxford,0.007313
paper,0.006422
parallel,0.008065
park,0.009097
particular,0.024362
particularli,0.010918
past,0.006175
patholog,0.022131
patient,0.037926
pattern,0.006641
paul,0.020299
pay,0.007267
peopl,0.025750
perform,0.016416
perhap,0.014472
person,0.025468
perspect,0.006965
pet,0.012342
phd,0.020118
phenomena,0.007432
phenomenon,0.008017
philosoph,0.020497
phrenolog,0.049044
physic,0.005093
physician,0.029458
physiolog,0.027506
physiologist,0.027887
pictur,0.008714
pineal,0.034804
pioneer,0.024395
pittsburgh,0.014388
place,0.004402
plastic,0.010465
plausibl,0.011635
point,0.009030
popul,0.005538
portion,0.007822
posit,0.004613
positron,0.013316
possibl,0.009105
potenti,0.005779
power,0.009136
practic,0.014327
predict,0.006531
prefer,0.013670
preferenti,0.012401
present,0.013389
previous,0.013609
priest,0.010569
primarili,0.006101
primat,0.025326
principl,0.005358
privat,0.006400
problem,0.023869
proceed,0.008168
process,0.037978
produc,0.013933
product,0.009304
professor,0.007977
progress,0.006083
project,0.005523
prompt,0.009771
provid,0.003960
psychiatr,0.012637
psychiatri,0.012611
psychoeduc,0.018150
psycholog,0.098913
public,0.004495
quit,0.007543
racism,0.013121
racist,0.029037
random,0.008515
rat,0.059235
ration,0.015385
reach,0.005753
react,0.009890
reaction,0.014518
read,0.014750
realli,0.018216
reason,0.015242
receiv,0.005408
recent,0.004902
recogniz,0.013169
record,0.011934
refer,0.016985
regard,0.005431
region,0.025347
rehabilit,0.012206
reject,0.006978
rel,0.015227
relat,0.026719
relationship,0.005403
religi,0.007231
remain,0.009364
remedi,0.011196
remov,0.039217
ren,0.044376
represent,0.007141
requir,0.004456
research,0.055111
resist,0.007335
resolut,0.008660
reson,0.032503
respect,0.005383
respond,0.007635
respons,0.029460
rest,0.013265
result,0.007886
retent,0.013169
revers,0.015587
role,0.004959
root,0.007151
rule,0.005167
run,0.006221
said,0.005816
sampl,0.008436
say,0.006133
scale,0.012815
scan,0.033910
scienc,0.014283
scientif,0.039877
scientist,0.019420
se,0.011771
seat,0.041887
section,0.014029
seek,0.019057
seen,0.016663
select,0.006155
sens,0.005825
sent,0.007846
separ,0.010344
serv,0.005635
set,0.021926
shape,0.006747
share,0.011033
shown,0.006826
sign,0.006486
similar,0.004704
simpl,0.012520
simpli,0.019286
simplifi,0.009175
simul,0.008560
size,0.012271
skull,0.037756
societi,0.004936
softwar,0.008347
somewhat,0.008352
sort,0.008263
soul,0.050862
special,0.004927
specif,0.080136
spectacular,0.013717
speech,0.042442
spencer,0.012925
spirit,0.008902
spiritu,0.009078
spur,0.011065
standard,0.021226
start,0.004987
state,0.003447
stem,0.008856
store,0.007982
strength,0.007990
structur,0.041429
student,0.006705
studi,0.066619
subject,0.005012
suffer,0.021572
suffici,0.007283
summar,0.010035
switch,0.009674
systemat,0.007527
taken,0.005768
task,0.073089
taught,0.008784
team,0.008227
techniqu,0.006054
technolog,0.011361
tend,0.006469
term,0.003722
test,0.077290
th,0.004517
theoret,0.006460
theori,0.027708
theoriz,0.021440
thi,0.053044
thoma,0.014095
thought,0.033502
thu,0.004605
time,0.038345
tissu,0.028561
today,0.005660
togeth,0.005432
told,0.009992
tomographi,0.027595
took,0.024619
tool,0.006308
trace,0.015109
train,0.013606
trainingabl,0.042709
trauma,0.013014
treat,0.006898
treatment,0.021742
tri,0.006201
tropic,0.010054
true,0.006379
turn,0.005446
type,0.009473
typic,0.010571
ultim,0.006814
unabl,0.008187
uncov,0.011204
underli,0.007482
understand,0.090916
understood,0.014788
univers,0.028564
unment,0.018497
use,0.051508
useless,0.012911
usual,0.009482
valid,0.015047
variou,0.017940
vascular,0.012984
veri,0.008998
view,0.020198
viewpoint,0.010371
virginia,0.011350
visual,0.024938
vital,0.008799
wa,0.122757
wai,0.017658
watson,0.011743
way,0.037914
weak,0.007826
wechsler,0.037249
weidman,0.021354
wellthoughtout,0.020179
went,0.007226
west,0.006765
wherea,0.013673
whi,0.007249
wide,0.015223
willi,0.066086
wisconsin,0.013059
wm,0.015404
woodcock,0.018898
word,0.016407
work,0.061799
world,0.011983
write,0.012320
wrong,0.009266
wrote,0.013425
x,0.006975
y,0.025170
year,0.019881
yerk,0.018497
yield,0.015393
zoolog,0.011895
