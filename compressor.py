#Takes 2 arguments (1 is optional):
#	The first is the file containing the corpus,
#	The second can take 2 forms:
#	-f, an optional arg that stops the recreation of the tf_idf files from the corpus, saving hours when it is already constructed
#	Or an integer representing the number of digits you want to save of your tf_idf value (after the decimal) (default is 6)
#	The outputs are:
#	A file for each file in the corpus, in the format word_int_value,tf_idf_score\n
#	Four dictionaries containing:
#		String_to_int, a python dictionary of all the numerical values associated to each word
#		Int_to_string, the reverse dictionary mentioned above
#		Int_to_idf, a dictionary of all the numerical values associated with each words and their corresponding idf scores
#		word_base, a simple list of all the words in the cleaned corpus, useful for other parts of the sequence

import nltk
import string
import os
import sys
import numpy

from sklearn.feature_extraction.text import TfidfVectorizer
from nltk.stem.porter import PorterStemmer

stemmer = PorterStemmer

option = 'xx'
optionPrecision = 8
if len(sys.argv) == 2:
	path = sys.argv[1]
if len(sys.argv) == 3:
	path = sys.argv[1]
	if sys.argv[2] == '-f':
		option = sys.argv[2]
	elif (int(sys.argv[2]) in range(20)):
		optionPrecision = 2 + int(sys.argv[2])


#From a word base, create two files: from word -> integer and int -> word (in alphabetical order)
def dictionaryBuilder():
	counter = 0
	wordFile = open('./dictionaries/word_base', 'r')
	inverseMapFile = open('./dictionaries/string_to_int', 'w')
	mapFile = open('./dictionaries/string_to_int', 'w')
	dictStringToInt = {}
	dictIntToString = {}
	for x in wordFile:
		line = string.strip(x, '\n')
		dictStringToInt[line] = counter
		dictIntToString[counter] = line
		counter += 1
	mapFile.write(str(dictStringToInt))
	inverseMapFile.write(str(dictIntToString))
	wordFile.close()
	mapFile.close()


def stemTokens(tokens, stemmer):
	stemmed = []
	for items in tokens:
		stemmed.append(stemmer.stem(item))
	return stemmed


def tokenizeAndStem(text):
	tokens = nltk.word_tokenize(text)
	stems = stem_tokens(tokens, stemmer)
	return stems

def wordBaseBuilder(listOfFeatures):
	if not os.path.exists('./dictionaries'):
		os.makedirs('./dictionaries')
	wordBase = open('./dictionaries/word_base', 'w')
	for features in range(listOfFeatures.size):
		wordBase.write(str(features) + '\n')
	wordBase.close()

def tfidfMaker():
	tokenDictionary = {}
	numberOfFiles = 0
	for subdir, dirs, files in os.walk(path):
		for file in files:
			numberOfFiles += 1#To show progress
			filePath = subdir + os.path.sep + file
			subjectFile = open(filePath, 'r')
			text = subjectFile.read()
			text = text.lower()
			text = text.translate(None, string.punctuation)
			tokenDict[file] = text
			if numberOfFiles%10 == 0:
					print str(file)
	pageNames = tokenDict.keys()
	print('Creating tfidf database, this may take some time')
	tfidf = TfidfVectorizer(tokenizer = tokenize, stop_words='english')
	tfs = tfidf.fit_transform(token_dict.values())
	print('Tfidf sparse matrix created. Soon will be writing this matrix into files.')
	count = tfs[0].toarray()[0].size
	print('Creating word base')
	wordBaseBuilder(tfidf.get_feature_names())
	print('Creating map files')
	dictionaryBuilder()
	print('Writing the matrix to files')
	print('Number of tokens: ' + str(count))
	print('Number of files: ' + str(numberOfFiles))
	if not os.path.exists('./tfidf_text'):
    	os.makedirs('./tfidf_text')
    if option != '-f':
    	for col in range(numberOfFiles):
			tf_idf_page = open('./tfidf_text/' + 'tfidf_' + pageNames[col], 'w')
			print 'Doing the tf-idf for the file ' + pageNames[col]
			row = tfs[col].toarray()[0]
			for x in range(count):
				if row[x] != 0:
					tf_idf_page.write(str(x) + ',' + str(tfs[col, x])[:option2] + '\n') #change for number of sig figs in tfidf files
			tf_idf_page.close()
		print('Done writing the tf-idf values')
	print('Creting idf database, a list of the idf values of every word.')
	file = open('./dictionaries/int_to_idf', 'w')
	idf = tfidf.idf_
	for x in range(idf.size):
		file.write(str(x) + ',' + str(idf[x]) + '/n')
	file.close()