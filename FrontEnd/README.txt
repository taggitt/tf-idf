Copy and paste inside htdocs in XAMPP

Create 'nltk_data' folder in root (ie: C:\nltk_data)
Download with nltk.download()
(this is a quick fix cause for some reason when the import nltk is being done, it can't find that directory...)

For python:
inside the python file, you will have to change the first line:
#! ... to the path that it can take to access python.exe