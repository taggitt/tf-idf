#!C:\Users\Karely\AppData\Local\Programs\Python\Python36-32\python.exe

import cgi
import cgitb
import subprocess

print ("Content-Type: text/html\n\n")

cgitb.enable()

#Get the string from the form data
form = cgi.FieldStorage()
raw = form['inputTokens'].value

#Encode the string into bytes while adding the terminating
#term for the java program
byte = str.encode(raw+"exit\n")

#Instantiate a pipe between python and java program
pipe = subprocess.Popen(
    ['java','comparer'],
    stdout=subprocess.PIPE,
    stdin=subprocess.PIPE
    )

#Input the data to the java program
pipe.stdin.write(byte)
pipe.stdin.flush()

#Get the output of the java program and decode it
tag = pipe.stdout.readline().decode("utf-8")

#Print out to the response to the browser
print("#"+tag)

