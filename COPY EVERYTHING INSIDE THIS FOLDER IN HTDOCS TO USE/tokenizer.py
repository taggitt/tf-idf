#!C:\Users\Karely\AppData\Local\Programs\Python\Python36-32\python.exe

import collections
import cgi
import nltk
import cgitb

print ("Content-Type: text/html\n\n")

cgitb.enable()

form = cgi.FieldStorage()
fileitem = form['file']

if fileitem.file:
    #read the file and decode it to only text
    raw = fileitem.file.read()
    strRaw = raw.decode("utf-8")

    #generate tokens
    tokens = nltk.word_tokenize(strRaw)

    #removing numbers and punctuation
    tokensArray = []
    for w in tokens:
        if w[0].isalpha():
            tokensArray.append(w)

    #removing stop words and lowercasing tokens
    stop_words = set(nltk.corpus.stopwords.words("english"))
    lower_words = []
    for w in tokensArray:
        if w.lower() not in stop_words:
            lower_words.append(w.lower())
    
    #Stemming words
    stems = []
    for w in lower_words:
        stems.append(nltk.stem.porter.PorterStemmer().stem(w))

    #Calculating th term requency and printing on the web
    dict = sorted(collections.Counter(stems).items())
    totalCount = len(stems)
    
    print("<ul id='lstTokens' class='token-group'>")
    for key, value in dict:
        tf = round((value/totalCount),4)
        percent = int(tf*100)

        
        if(percent > 5 ):
            percent = 5
            
        #Only prints out words whose tf > 1%
        if(tf < 0.01):
            continue
            
        print("<li class='cloud"+str(percent)+"'>")
        print(key)
        print("</li>")
    print("</ul>")


    #creating a div containing the string needed to feed the java program
    print ('<textarea id="inputTokens" name="inputTokens" form="formTokenWrapper" style="display:none;">',end="")
    for key, value in dict:
        tf = round((value/totalCount),4)

        #Only keeping words whose tf > 0.01
        if (tf < 0.01):
            continue
        
        print(key + "," + str(tf))
    print('</textarea>')
    
else :
    print("<script> alert('No file uploaded'")



